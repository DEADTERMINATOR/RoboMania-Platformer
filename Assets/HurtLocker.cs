﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalData;

public class HurtLocker : MonoBehaviour , IDamageGiver{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {
        IDamageReceiver recever = collision.GetComponent<IDamageReceiver>();
        if (recever != null)
        {
            recever.TakeDamage(this, 17, DamageType.MELEE,Vector3.zero);
   
        }


    }
}
