﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;
using static GlobalData;

public class HurtPlayerOnTrigger : MonoBehaviour, IDamageGiver 
{
    public float DamageAmount;
    public DamageType TypeOfDamage = DamageType.ENVIROMENT;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        HandleCollision(collider);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        HandleCollision(collision.collider);
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        HandleCollision(collider);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        HandleCollision(collision.collider);
    }

    private void HandleCollision(Collider2D collider)
    {
        if (!Player.State.Invulnerable)
        {
            if (collider.gameObject.layer == PLAYER_LAYER)
            {
                Player.TakeDamage(this, DamageAmount, TypeOfDamage, Vector3.zero);
            }

        }
    }
}
