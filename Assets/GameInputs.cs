// GENERATED AUTOMATICALLY FROM 'Assets/GameInputs.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @GameInputs : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @GameInputs()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""GameInputs"",
    ""maps"": [
        {
            ""name"": ""In-Game"",
            ""id"": ""e6241421-3318-4250-8291-2fab7d29a5f8"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""29b2f4a8-99d5-4dae-983c-012bfbdaabc2"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""c877fb6e-ac33-4897-903f-46b5152042c6"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""794622c3-e069-47d8-9672-420cce1f6b18"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Wall Grip"",
                    ""type"": ""Button"",
                    ""id"": ""4de0ab37-78b2-4be0-836f-2aa893a3fa61"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Hover"",
                    ""type"": ""Button"",
                    ""id"": ""c001d15b-1f2c-43e3-b32b-4cf57c1356c4"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Raise"",
                    ""type"": ""Button"",
                    ""id"": ""b178cc09-2fdc-4109-bc7c-1aed8cfa16eb"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire"",
                    ""type"": ""Button"",
                    ""id"": ""2505e26e-ee48-4cf2-a1fb-8d23672cb53e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Precision Aim Trigger"",
                    ""type"": ""Button"",
                    ""id"": ""81f63701-6e8d-4e54-b659-db35a00cc118"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Precision Aim (Gamepad)"",
                    ""type"": ""Value"",
                    ""id"": ""df8f2e4f-40b9-45b1-87a9-5bdb42b9c4a5"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Precision Aim (Mouse)"",
                    ""type"": ""Button"",
                    ""id"": ""52f6b1b4-0aae-4fc3-8577-7b50bed583ba"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Repair"",
                    ""type"": ""Button"",
                    ""id"": ""2bcb37db-b59f-4757-95ec-2bd2f6b45aa6"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Action"",
                    ""type"": ""Button"",
                    ""id"": ""b1e747a9-48c8-49d8-ac27-cebd8efd7540"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""0072a811-1458-4edc-b286-0de24e739825"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Camera Shift"",
                    ""type"": ""Value"",
                    ""id"": ""990f3709-ee68-4cc0-8e72-d3726542e5c9"",
                    ""expectedControlType"": ""Digital"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pistol (Keyboard)"",
                    ""type"": ""Button"",
                    ""id"": ""fa566bdc-472a-43e5-8848-ca7636993484"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Machine Gun (Keyboard)"",
                    ""type"": ""Button"",
                    ""id"": ""1cbe49b0-937a-41c4-91b7-e450609aac2a"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Shotgun (Keyboard)"",
                    ""type"": ""Button"",
                    ""id"": ""876fb0a6-7db6-4765-a8bf-a40cc7f46b2f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Rocket Launcher (Keyboard)"",
                    ""type"": ""Button"",
                    ""id"": ""ad4029c0-8eb0-4e1a-8e06-6f76556fa549"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Charge Rifle (Keyboard)"",
                    ""type"": ""Button"",
                    ""id"": ""6b3f74c4-0dc8-452c-b303-32010e1052f4"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SwitchChipPickup"",
                    ""type"": ""Button"",
                    ""id"": ""18b76f85-aa1d-43ca-a61d-6d17495c701e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Next Weapon Form (Controller)"",
                    ""type"": ""Button"",
                    ""id"": ""e5df41c0-f383-48af-9012-1d785c25dbd4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Previous Weapon Form (Controller)"",
                    ""type"": ""Button"",
                    ""id"": ""87537cc5-3763-4dcc-b5fd-7982a4b536a5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cycle Weapon Form (Keyboard)"",
                    ""type"": ""Value"",
                    ""id"": ""ffc64332-0f1d-4ed0-b217-fae7c4a7bb43"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""64708050-3d30-45f8-9fd7-738ffebe4258"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""48069e69-aa35-4b52-9cb0-d0706ab35d3c"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8303aeba-7d18-4a6e-9233-e5ec78a7f6e6"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3f58e493-b296-40f8-9a05-9fc3cb514f92"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e99f680f-f99f-4faa-93ff-92d7cd89da93"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Wall Grip"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""432f5ef8-2ffc-411c-85c6-fc6990f0ea72"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Wall Grip"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e24fb039-5e85-488d-b5be-1f14fc20c1f9"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Hover"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""58972d07-955d-4de4-b431-fc72f8a20854"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Hover"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9d7300d2-b438-47be-8a6f-300cf35b29c6"",
                    ""path"": ""<Gamepad>/leftStick/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Raise"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""12dd662d-6600-45c4-97a9-c086b4eeabd6"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Raise"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e6bd6f34-a631-401b-b1ec-1a870f8a9463"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""235c40ae-3865-4b99-ad4a-3f493576c815"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""519334ac-15e2-45d8-9ee0-41c00c409262"",
                    ""path"": ""<Gamepad>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Repair"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""314274fd-ef69-4a20-ad75-c2e62a60184f"",
                    ""path"": ""<Keyboard>/h"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Repair"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6a582893-861d-4313-9c65-7050fe90a236"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Action"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7503ac59-53ce-4819-99de-e62d6072f666"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Action"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""aeb92f2e-3c0d-41d6-abe5-1345884bc5af"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Precision Aim (Gamepad)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""42d256e9-7f20-426e-abe7-4e1834f85f7d"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Precision Aim Trigger"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c4702557-7cd9-44d3-9e02-162b6cb956b8"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Precision Aim (Mouse)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""66afd7e7-f02a-44a9-92d6-46a2461db929"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b6774a11-f259-4f23-8171-3a7876ede980"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ca9d3135-9f67-4f5c-bf11-0894769cecd3"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pistol (Keyboard)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""87be1e0d-8a0f-4500-856a-45a0aca83127"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Machine Gun (Keyboard)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b4f85b91-330a-4a3a-a51b-59f64891cad8"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Shotgun (Keyboard)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c9009057-a4a6-4fa8-8746-a490dfc66882"",
                    ""path"": ""<Keyboard>/4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rocket Launcher (Keyboard)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""97a489d7-ca95-48d8-a753-17bd18ad2aba"",
                    ""path"": ""<Keyboard>/5"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Charge Rifle (Keyboard)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7bf29fdf-8471-468c-aa15-25dbdf5a9bfe"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SwitchChipPickup"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d0e76313-9185-4ffb-aa49-75125272daf1"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": ""Hold"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SwitchChipPickup"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b5439693-3a60-4f5e-9ad9-c403801e61f9"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Next Weapon Form (Controller)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c41fde5e-4870-45bf-9e95-e15e3a95ac4f"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Previous Weapon Form (Controller)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5f9f1eeb-fa47-4c4e-bff3-5818c4f3be47"",
                    ""path"": ""<Mouse>/scroll"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cycle Weapon Form (Keyboard)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Gamepad"",
                    ""id"": ""7b8ec321-1446-4d45-b381-27396443438d"",
                    ""path"": ""2DVector(mode=2)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""416ed2ee-a82b-4109-b414-a78b50b4077a"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""d645c218-29f2-42a1-bbf9-b9e83f76974f"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""c28107c2-241c-43c9-bd16-cfd1ace2701d"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""b2aae672-bd62-4f2f-86a9-83c8d0c865b3"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Keyboard"",
                    ""id"": ""28c81398-c61e-4cbe-96e8-ec14d872691b"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""0790362e-3ff1-4fd5-8cdd-662fcbe96aa4"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""9b4224de-d5f2-41c4-89d6-cd1fc7014572"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""1717aba8-3db1-476e-9676-0796e4bc79e2"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""42cb909f-c731-45e6-93c0-14a2007cda61"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Gamepad"",
                    ""id"": ""df87b3d3-916a-43a8-8c08-ee43b607d47d"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera Shift"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""4f4a55b9-7ebd-475a-ba03-329f20cace88"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera Shift"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""894e9ce7-db94-4a0f-a7eb-1c791ee1328e"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera Shift"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Keyboard"",
                    ""id"": ""52b37739-0feb-4bb5-8717-5c54411b7a1e"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera Shift"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""ec14f4ee-791f-41b1-ada7-511d88fcee62"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera Shift"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""a3b55e0e-74ca-4264-a93c-ee2517e6c982"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera Shift"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        },
        {
            ""name"": ""Menu"",
            ""id"": ""b6281b3a-e220-4e88-9372-71ad9c2153dc"",
            ""actions"": [
                {
                    ""name"": ""Select"",
                    ""type"": ""Button"",
                    ""id"": ""cc0c8fa7-3853-4274-a492-44284542702d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Back"",
                    ""type"": ""Button"",
                    ""id"": ""05feb285-1955-4775-b084-13a97658cc4c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Up"",
                    ""type"": ""Button"",
                    ""id"": ""fd196720-dbdb-4c7f-94aa-f81094d850e7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Down"",
                    ""type"": ""Button"",
                    ""id"": ""ac82cc39-e8fe-483c-9b1c-c50c13f2c0f7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Right"",
                    ""type"": ""Button"",
                    ""id"": ""8739470d-b4ff-4ef0-a574-eeb1c35e0d79"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Left"",
                    ""type"": ""Button"",
                    ""id"": ""d37fb170-01de-4402-aab9-f32c78e6fe00"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Unpause"",
                    ""type"": ""Button"",
                    ""id"": ""0090656b-cc77-4b2f-8444-2fbe86261599"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CloseWindow"",
                    ""type"": ""Button"",
                    ""id"": ""20e0f71a-5131-46e3-90aa-407ea3829f98"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""d6c527bd-11c4-4b40-a394-a3ec83619922"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""65312172-2245-4dcd-a9e4-20aa54fe15e4"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9833491c-bf61-4251-adf6-9d0cbae8c135"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""400be618-13f6-48a5-8963-d5b9be93e08a"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fff7dbb4-3d77-4620-91b2-773b85fe0c1b"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": ""HoldToPerformMultipleActions(SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6f0990f2-eaa7-46bd-80dd-620933cc3d76"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": ""ControlStickPerformSingleAction(RegisterFurtherInteractionsWithoutResettingControlStick=true,SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""472e71a6-9be7-485b-bd6e-3e0216a7a290"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""42bf4fa6-a2ca-43f5-9159-b963f5e0452b"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": ""HoldToPerformMultipleActions(SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0b776c8a-fec0-4a06-9a12-24549d021649"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": ""ControlStickPerformSingleAction(RegisterFurtherInteractionsWithoutResettingControlStick=true,SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b5ae6c4e-a522-4354-8658-2387a87c189b"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""25fe57fc-e9a6-4a95-ae96-d0757de4a67d"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0cd47f5f-010e-4cc9-a459-007618a967d4"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": ""ControlStickPerformSingleAction(RegisterFurtherInteractionsWithoutResettingControlStick=true,SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""60371792-6565-4ddf-b0e9-ff71cf24c034"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e51f4501-8af3-4089-91c5-3a6c17876c21"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e9654536-d645-4b4f-b789-c40b79c3f877"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": ""ControlStickPerformSingleAction(RegisterFurtherInteractionsWithoutResettingControlStick=true,SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3e128ffb-6ecc-4102-b3ce-a6ea1ac249ac"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""04105212-d18d-424f-802f-5e40524df573"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Unpause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""598f4f02-671b-4a2f-b50e-a8bfe441a5af"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Unpause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""37cfcb34-5b6d-4038-961b-263fcecacee2"",
                    ""path"": ""*/{Back}"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CloseWindow"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a90612e6-58c8-4f5e-9ab4-3c456e9e1e8c"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CloseWindow"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""PopUpUI"",
            ""id"": ""4a8be8da-ec68-4ac8-9307-4be0d741c3c6"",
            ""actions"": [
                {
                    ""name"": ""PopUpSelect"",
                    ""type"": ""Button"",
                    ""id"": ""cd4acd2f-cfc8-460c-9245-431c7a50fcfe"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PopUpBack"",
                    ""type"": ""Button"",
                    ""id"": ""657289c2-2c24-4159-93f3-3212e4fc74f2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PopUpUp"",
                    ""type"": ""Button"",
                    ""id"": ""9c68fcf6-316d-4b00-a416-d5323dd008ee"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PopUpDown"",
                    ""type"": ""Button"",
                    ""id"": ""107a0f77-1e04-4f1c-9095-f821e2acbfc2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PopUpRight"",
                    ""type"": ""Button"",
                    ""id"": ""c1b8b0e6-936f-4202-9116-6d22990607c2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PopUpLeft"",
                    ""type"": ""Button"",
                    ""id"": ""f35164d5-c0f1-4420-b548-f719c44c4f91"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""ae93d1f3-90a2-4ad0-801e-3c8f877dddf5"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpSelect"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ef481b87-a945-4ac6-8d39-5a9c022d8b33"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpSelect"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8835119e-e87c-41e1-ac77-7c2a5cc79692"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpBack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9f4bd46a-0998-469b-8f68-335a16765879"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpBack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""71d5a0bd-b777-4ce5-a723-acca78d289af"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": ""HoldToPerformMultipleActions(SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""02ec16a8-3c1f-403f-834d-7c936ccd17d4"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": ""ControlStickPerformSingleAction(RegisterFurtherInteractionsWithoutResettingControlStick=true,SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8fb44f0a-0385-4ed2-be93-a52d63f43d64"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""113f3e3b-087e-4f19-887c-40d0f592ce32"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": ""HoldToPerformMultipleActions(SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e9802bf2-8eae-46f6-b848-7024ab3c3649"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": ""ControlStickPerformSingleAction(RegisterFurtherInteractionsWithoutResettingControlStick=true,SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dea7d644-a0bb-4ea4-b359-711ec43e0a02"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e576e063-349d-4cde-b517-2fb6a3c05108"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4eff8e15-beae-489f-bd62-756fd0913be1"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": ""ControlStickPerformSingleAction(RegisterFurtherInteractionsWithoutResettingControlStick=true,SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6ae9a560-6cda-402e-bce1-266da5975d7f"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fa1f8c6f-9fc9-41ab-8ddf-719b562e1e29"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fd181605-b884-4c56-8131-41d2adca17be"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": ""ControlStickPerformSingleAction(RegisterFurtherInteractionsWithoutResettingControlStick=true,SpeedUpRepeatInteractionsAfterTheFirst=true)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""518860d5-e6f3-4e0a-8ca2-fe6fc8bb7dec"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": ""HoldToPerformMultipleActions"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PopUpLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // In-Game
        m_InGame = asset.FindActionMap("In-Game", throwIfNotFound: true);
        m_InGame_Move = m_InGame.FindAction("Move", throwIfNotFound: true);
        m_InGame_Jump = m_InGame.FindAction("Jump", throwIfNotFound: true);
        m_InGame_Dash = m_InGame.FindAction("Dash", throwIfNotFound: true);
        m_InGame_WallGrip = m_InGame.FindAction("Wall Grip", throwIfNotFound: true);
        m_InGame_Hover = m_InGame.FindAction("Hover", throwIfNotFound: true);
        m_InGame_Raise = m_InGame.FindAction("Raise", throwIfNotFound: true);
        m_InGame_Fire = m_InGame.FindAction("Fire", throwIfNotFound: true);
        m_InGame_PrecisionAimTrigger = m_InGame.FindAction("Precision Aim Trigger", throwIfNotFound: true);
        m_InGame_PrecisionAimGamepad = m_InGame.FindAction("Precision Aim (Gamepad)", throwIfNotFound: true);
        m_InGame_PrecisionAimMouse = m_InGame.FindAction("Precision Aim (Mouse)", throwIfNotFound: true);
        m_InGame_Repair = m_InGame.FindAction("Repair", throwIfNotFound: true);
        m_InGame_Action = m_InGame.FindAction("Action", throwIfNotFound: true);
        m_InGame_Pause = m_InGame.FindAction("Pause", throwIfNotFound: true);
        m_InGame_CameraShift = m_InGame.FindAction("Camera Shift", throwIfNotFound: true);
        m_InGame_PistolKeyboard = m_InGame.FindAction("Pistol (Keyboard)", throwIfNotFound: true);
        m_InGame_MachineGunKeyboard = m_InGame.FindAction("Machine Gun (Keyboard)", throwIfNotFound: true);
        m_InGame_ShotgunKeyboard = m_InGame.FindAction("Shotgun (Keyboard)", throwIfNotFound: true);
        m_InGame_RocketLauncherKeyboard = m_InGame.FindAction("Rocket Launcher (Keyboard)", throwIfNotFound: true);
        m_InGame_ChargeRifleKeyboard = m_InGame.FindAction("Charge Rifle (Keyboard)", throwIfNotFound: true);
        m_InGame_SwitchChipPickup = m_InGame.FindAction("SwitchChipPickup", throwIfNotFound: true);
        m_InGame_NextWeaponFormController = m_InGame.FindAction("Next Weapon Form (Controller)", throwIfNotFound: true);
        m_InGame_PreviousWeaponFormController = m_InGame.FindAction("Previous Weapon Form (Controller)", throwIfNotFound: true);
        m_InGame_CycleWeaponFormKeyboard = m_InGame.FindAction("Cycle Weapon Form (Keyboard)", throwIfNotFound: true);
        // Menu
        m_Menu = asset.FindActionMap("Menu", throwIfNotFound: true);
        m_Menu_Select = m_Menu.FindAction("Select", throwIfNotFound: true);
        m_Menu_Back = m_Menu.FindAction("Back", throwIfNotFound: true);
        m_Menu_Up = m_Menu.FindAction("Up", throwIfNotFound: true);
        m_Menu_Down = m_Menu.FindAction("Down", throwIfNotFound: true);
        m_Menu_Right = m_Menu.FindAction("Right", throwIfNotFound: true);
        m_Menu_Left = m_Menu.FindAction("Left", throwIfNotFound: true);
        m_Menu_Unpause = m_Menu.FindAction("Unpause", throwIfNotFound: true);
        m_Menu_CloseWindow = m_Menu.FindAction("CloseWindow", throwIfNotFound: true);
        // PopUpUI
        m_PopUpUI = asset.FindActionMap("PopUpUI", throwIfNotFound: true);
        m_PopUpUI_PopUpSelect = m_PopUpUI.FindAction("PopUpSelect", throwIfNotFound: true);
        m_PopUpUI_PopUpBack = m_PopUpUI.FindAction("PopUpBack", throwIfNotFound: true);
        m_PopUpUI_PopUpUp = m_PopUpUI.FindAction("PopUpUp", throwIfNotFound: true);
        m_PopUpUI_PopUpDown = m_PopUpUI.FindAction("PopUpDown", throwIfNotFound: true);
        m_PopUpUI_PopUpRight = m_PopUpUI.FindAction("PopUpRight", throwIfNotFound: true);
        m_PopUpUI_PopUpLeft = m_PopUpUI.FindAction("PopUpLeft", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // In-Game
    private readonly InputActionMap m_InGame;
    private IInGameActions m_InGameActionsCallbackInterface;
    private readonly InputAction m_InGame_Move;
    private readonly InputAction m_InGame_Jump;
    private readonly InputAction m_InGame_Dash;
    private readonly InputAction m_InGame_WallGrip;
    private readonly InputAction m_InGame_Hover;
    private readonly InputAction m_InGame_Raise;
    private readonly InputAction m_InGame_Fire;
    private readonly InputAction m_InGame_PrecisionAimTrigger;
    private readonly InputAction m_InGame_PrecisionAimGamepad;
    private readonly InputAction m_InGame_PrecisionAimMouse;
    private readonly InputAction m_InGame_Repair;
    private readonly InputAction m_InGame_Action;
    private readonly InputAction m_InGame_Pause;
    private readonly InputAction m_InGame_CameraShift;
    private readonly InputAction m_InGame_PistolKeyboard;
    private readonly InputAction m_InGame_MachineGunKeyboard;
    private readonly InputAction m_InGame_ShotgunKeyboard;
    private readonly InputAction m_InGame_RocketLauncherKeyboard;
    private readonly InputAction m_InGame_ChargeRifleKeyboard;
    private readonly InputAction m_InGame_SwitchChipPickup;
    private readonly InputAction m_InGame_NextWeaponFormController;
    private readonly InputAction m_InGame_PreviousWeaponFormController;
    private readonly InputAction m_InGame_CycleWeaponFormKeyboard;
    public struct InGameActions
    {
        private @GameInputs m_Wrapper;
        public InGameActions(@GameInputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_InGame_Move;
        public InputAction @Jump => m_Wrapper.m_InGame_Jump;
        public InputAction @Dash => m_Wrapper.m_InGame_Dash;
        public InputAction @WallGrip => m_Wrapper.m_InGame_WallGrip;
        public InputAction @Hover => m_Wrapper.m_InGame_Hover;
        public InputAction @Raise => m_Wrapper.m_InGame_Raise;
        public InputAction @Fire => m_Wrapper.m_InGame_Fire;
        public InputAction @PrecisionAimTrigger => m_Wrapper.m_InGame_PrecisionAimTrigger;
        public InputAction @PrecisionAimGamepad => m_Wrapper.m_InGame_PrecisionAimGamepad;
        public InputAction @PrecisionAimMouse => m_Wrapper.m_InGame_PrecisionAimMouse;
        public InputAction @Repair => m_Wrapper.m_InGame_Repair;
        public InputAction @Action => m_Wrapper.m_InGame_Action;
        public InputAction @Pause => m_Wrapper.m_InGame_Pause;
        public InputAction @CameraShift => m_Wrapper.m_InGame_CameraShift;
        public InputAction @PistolKeyboard => m_Wrapper.m_InGame_PistolKeyboard;
        public InputAction @MachineGunKeyboard => m_Wrapper.m_InGame_MachineGunKeyboard;
        public InputAction @ShotgunKeyboard => m_Wrapper.m_InGame_ShotgunKeyboard;
        public InputAction @RocketLauncherKeyboard => m_Wrapper.m_InGame_RocketLauncherKeyboard;
        public InputAction @ChargeRifleKeyboard => m_Wrapper.m_InGame_ChargeRifleKeyboard;
        public InputAction @SwitchChipPickup => m_Wrapper.m_InGame_SwitchChipPickup;
        public InputAction @NextWeaponFormController => m_Wrapper.m_InGame_NextWeaponFormController;
        public InputAction @PreviousWeaponFormController => m_Wrapper.m_InGame_PreviousWeaponFormController;
        public InputAction @CycleWeaponFormKeyboard => m_Wrapper.m_InGame_CycleWeaponFormKeyboard;
        public InputActionMap Get() { return m_Wrapper.m_InGame; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(InGameActions set) { return set.Get(); }
        public void SetCallbacks(IInGameActions instance)
        {
            if (m_Wrapper.m_InGameActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnMove;
                @Jump.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnJump;
                @Dash.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnDash;
                @WallGrip.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnWallGrip;
                @WallGrip.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnWallGrip;
                @WallGrip.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnWallGrip;
                @Hover.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnHover;
                @Hover.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnHover;
                @Hover.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnHover;
                @Raise.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnRaise;
                @Raise.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnRaise;
                @Raise.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnRaise;
                @Fire.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnFire;
                @Fire.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnFire;
                @Fire.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnFire;
                @PrecisionAimTrigger.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnPrecisionAimTrigger;
                @PrecisionAimTrigger.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnPrecisionAimTrigger;
                @PrecisionAimTrigger.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnPrecisionAimTrigger;
                @PrecisionAimGamepad.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnPrecisionAimGamepad;
                @PrecisionAimGamepad.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnPrecisionAimGamepad;
                @PrecisionAimGamepad.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnPrecisionAimGamepad;
                @PrecisionAimMouse.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnPrecisionAimMouse;
                @PrecisionAimMouse.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnPrecisionAimMouse;
                @PrecisionAimMouse.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnPrecisionAimMouse;
                @Repair.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnRepair;
                @Repair.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnRepair;
                @Repair.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnRepair;
                @Action.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnAction;
                @Action.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnAction;
                @Action.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnAction;
                @Pause.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnPause;
                @Pause.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnPause;
                @Pause.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnPause;
                @CameraShift.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnCameraShift;
                @CameraShift.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnCameraShift;
                @CameraShift.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnCameraShift;
                @PistolKeyboard.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnPistolKeyboard;
                @PistolKeyboard.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnPistolKeyboard;
                @PistolKeyboard.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnPistolKeyboard;
                @MachineGunKeyboard.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnMachineGunKeyboard;
                @MachineGunKeyboard.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnMachineGunKeyboard;
                @MachineGunKeyboard.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnMachineGunKeyboard;
                @ShotgunKeyboard.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnShotgunKeyboard;
                @ShotgunKeyboard.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnShotgunKeyboard;
                @ShotgunKeyboard.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnShotgunKeyboard;
                @RocketLauncherKeyboard.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnRocketLauncherKeyboard;
                @RocketLauncherKeyboard.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnRocketLauncherKeyboard;
                @RocketLauncherKeyboard.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnRocketLauncherKeyboard;
                @ChargeRifleKeyboard.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnChargeRifleKeyboard;
                @ChargeRifleKeyboard.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnChargeRifleKeyboard;
                @ChargeRifleKeyboard.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnChargeRifleKeyboard;
                @SwitchChipPickup.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnSwitchChipPickup;
                @SwitchChipPickup.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnSwitchChipPickup;
                @SwitchChipPickup.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnSwitchChipPickup;
                @NextWeaponFormController.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnNextWeaponFormController;
                @NextWeaponFormController.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnNextWeaponFormController;
                @NextWeaponFormController.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnNextWeaponFormController;
                @PreviousWeaponFormController.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnPreviousWeaponFormController;
                @PreviousWeaponFormController.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnPreviousWeaponFormController;
                @PreviousWeaponFormController.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnPreviousWeaponFormController;
                @CycleWeaponFormKeyboard.started -= m_Wrapper.m_InGameActionsCallbackInterface.OnCycleWeaponFormKeyboard;
                @CycleWeaponFormKeyboard.performed -= m_Wrapper.m_InGameActionsCallbackInterface.OnCycleWeaponFormKeyboard;
                @CycleWeaponFormKeyboard.canceled -= m_Wrapper.m_InGameActionsCallbackInterface.OnCycleWeaponFormKeyboard;
            }
            m_Wrapper.m_InGameActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
                @WallGrip.started += instance.OnWallGrip;
                @WallGrip.performed += instance.OnWallGrip;
                @WallGrip.canceled += instance.OnWallGrip;
                @Hover.started += instance.OnHover;
                @Hover.performed += instance.OnHover;
                @Hover.canceled += instance.OnHover;
                @Raise.started += instance.OnRaise;
                @Raise.performed += instance.OnRaise;
                @Raise.canceled += instance.OnRaise;
                @Fire.started += instance.OnFire;
                @Fire.performed += instance.OnFire;
                @Fire.canceled += instance.OnFire;
                @PrecisionAimTrigger.started += instance.OnPrecisionAimTrigger;
                @PrecisionAimTrigger.performed += instance.OnPrecisionAimTrigger;
                @PrecisionAimTrigger.canceled += instance.OnPrecisionAimTrigger;
                @PrecisionAimGamepad.started += instance.OnPrecisionAimGamepad;
                @PrecisionAimGamepad.performed += instance.OnPrecisionAimGamepad;
                @PrecisionAimGamepad.canceled += instance.OnPrecisionAimGamepad;
                @PrecisionAimMouse.started += instance.OnPrecisionAimMouse;
                @PrecisionAimMouse.performed += instance.OnPrecisionAimMouse;
                @PrecisionAimMouse.canceled += instance.OnPrecisionAimMouse;
                @Repair.started += instance.OnRepair;
                @Repair.performed += instance.OnRepair;
                @Repair.canceled += instance.OnRepair;
                @Action.started += instance.OnAction;
                @Action.performed += instance.OnAction;
                @Action.canceled += instance.OnAction;
                @Pause.started += instance.OnPause;
                @Pause.performed += instance.OnPause;
                @Pause.canceled += instance.OnPause;
                @CameraShift.started += instance.OnCameraShift;
                @CameraShift.performed += instance.OnCameraShift;
                @CameraShift.canceled += instance.OnCameraShift;
                @PistolKeyboard.started += instance.OnPistolKeyboard;
                @PistolKeyboard.performed += instance.OnPistolKeyboard;
                @PistolKeyboard.canceled += instance.OnPistolKeyboard;
                @MachineGunKeyboard.started += instance.OnMachineGunKeyboard;
                @MachineGunKeyboard.performed += instance.OnMachineGunKeyboard;
                @MachineGunKeyboard.canceled += instance.OnMachineGunKeyboard;
                @ShotgunKeyboard.started += instance.OnShotgunKeyboard;
                @ShotgunKeyboard.performed += instance.OnShotgunKeyboard;
                @ShotgunKeyboard.canceled += instance.OnShotgunKeyboard;
                @RocketLauncherKeyboard.started += instance.OnRocketLauncherKeyboard;
                @RocketLauncherKeyboard.performed += instance.OnRocketLauncherKeyboard;
                @RocketLauncherKeyboard.canceled += instance.OnRocketLauncherKeyboard;
                @ChargeRifleKeyboard.started += instance.OnChargeRifleKeyboard;
                @ChargeRifleKeyboard.performed += instance.OnChargeRifleKeyboard;
                @ChargeRifleKeyboard.canceled += instance.OnChargeRifleKeyboard;
                @SwitchChipPickup.started += instance.OnSwitchChipPickup;
                @SwitchChipPickup.performed += instance.OnSwitchChipPickup;
                @SwitchChipPickup.canceled += instance.OnSwitchChipPickup;
                @NextWeaponFormController.started += instance.OnNextWeaponFormController;
                @NextWeaponFormController.performed += instance.OnNextWeaponFormController;
                @NextWeaponFormController.canceled += instance.OnNextWeaponFormController;
                @PreviousWeaponFormController.started += instance.OnPreviousWeaponFormController;
                @PreviousWeaponFormController.performed += instance.OnPreviousWeaponFormController;
                @PreviousWeaponFormController.canceled += instance.OnPreviousWeaponFormController;
                @CycleWeaponFormKeyboard.started += instance.OnCycleWeaponFormKeyboard;
                @CycleWeaponFormKeyboard.performed += instance.OnCycleWeaponFormKeyboard;
                @CycleWeaponFormKeyboard.canceled += instance.OnCycleWeaponFormKeyboard;
            }
        }
    }
    public InGameActions @InGame => new InGameActions(this);

    // Menu
    private readonly InputActionMap m_Menu;
    private IMenuActions m_MenuActionsCallbackInterface;
    private readonly InputAction m_Menu_Select;
    private readonly InputAction m_Menu_Back;
    private readonly InputAction m_Menu_Up;
    private readonly InputAction m_Menu_Down;
    private readonly InputAction m_Menu_Right;
    private readonly InputAction m_Menu_Left;
    private readonly InputAction m_Menu_Unpause;
    private readonly InputAction m_Menu_CloseWindow;
    public struct MenuActions
    {
        private @GameInputs m_Wrapper;
        public MenuActions(@GameInputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @Select => m_Wrapper.m_Menu_Select;
        public InputAction @Back => m_Wrapper.m_Menu_Back;
        public InputAction @Up => m_Wrapper.m_Menu_Up;
        public InputAction @Down => m_Wrapper.m_Menu_Down;
        public InputAction @Right => m_Wrapper.m_Menu_Right;
        public InputAction @Left => m_Wrapper.m_Menu_Left;
        public InputAction @Unpause => m_Wrapper.m_Menu_Unpause;
        public InputAction @CloseWindow => m_Wrapper.m_Menu_CloseWindow;
        public InputActionMap Get() { return m_Wrapper.m_Menu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenuActions set) { return set.Get(); }
        public void SetCallbacks(IMenuActions instance)
        {
            if (m_Wrapper.m_MenuActionsCallbackInterface != null)
            {
                @Select.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnSelect;
                @Select.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnSelect;
                @Select.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnSelect;
                @Back.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnBack;
                @Back.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnBack;
                @Back.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnBack;
                @Up.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnUp;
                @Up.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnUp;
                @Up.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnUp;
                @Down.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnDown;
                @Down.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnDown;
                @Down.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnDown;
                @Right.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnRight;
                @Right.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnRight;
                @Right.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnRight;
                @Left.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnLeft;
                @Left.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnLeft;
                @Left.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnLeft;
                @Unpause.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnUnpause;
                @Unpause.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnUnpause;
                @Unpause.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnUnpause;
                @CloseWindow.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnCloseWindow;
                @CloseWindow.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnCloseWindow;
                @CloseWindow.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnCloseWindow;
            }
            m_Wrapper.m_MenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Select.started += instance.OnSelect;
                @Select.performed += instance.OnSelect;
                @Select.canceled += instance.OnSelect;
                @Back.started += instance.OnBack;
                @Back.performed += instance.OnBack;
                @Back.canceled += instance.OnBack;
                @Up.started += instance.OnUp;
                @Up.performed += instance.OnUp;
                @Up.canceled += instance.OnUp;
                @Down.started += instance.OnDown;
                @Down.performed += instance.OnDown;
                @Down.canceled += instance.OnDown;
                @Right.started += instance.OnRight;
                @Right.performed += instance.OnRight;
                @Right.canceled += instance.OnRight;
                @Left.started += instance.OnLeft;
                @Left.performed += instance.OnLeft;
                @Left.canceled += instance.OnLeft;
                @Unpause.started += instance.OnUnpause;
                @Unpause.performed += instance.OnUnpause;
                @Unpause.canceled += instance.OnUnpause;
                @CloseWindow.started += instance.OnCloseWindow;
                @CloseWindow.performed += instance.OnCloseWindow;
                @CloseWindow.canceled += instance.OnCloseWindow;
            }
        }
    }
    public MenuActions @Menu => new MenuActions(this);

    // PopUpUI
    private readonly InputActionMap m_PopUpUI;
    private IPopUpUIActions m_PopUpUIActionsCallbackInterface;
    private readonly InputAction m_PopUpUI_PopUpSelect;
    private readonly InputAction m_PopUpUI_PopUpBack;
    private readonly InputAction m_PopUpUI_PopUpUp;
    private readonly InputAction m_PopUpUI_PopUpDown;
    private readonly InputAction m_PopUpUI_PopUpRight;
    private readonly InputAction m_PopUpUI_PopUpLeft;
    public struct PopUpUIActions
    {
        private @GameInputs m_Wrapper;
        public PopUpUIActions(@GameInputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @PopUpSelect => m_Wrapper.m_PopUpUI_PopUpSelect;
        public InputAction @PopUpBack => m_Wrapper.m_PopUpUI_PopUpBack;
        public InputAction @PopUpUp => m_Wrapper.m_PopUpUI_PopUpUp;
        public InputAction @PopUpDown => m_Wrapper.m_PopUpUI_PopUpDown;
        public InputAction @PopUpRight => m_Wrapper.m_PopUpUI_PopUpRight;
        public InputAction @PopUpLeft => m_Wrapper.m_PopUpUI_PopUpLeft;
        public InputActionMap Get() { return m_Wrapper.m_PopUpUI; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PopUpUIActions set) { return set.Get(); }
        public void SetCallbacks(IPopUpUIActions instance)
        {
            if (m_Wrapper.m_PopUpUIActionsCallbackInterface != null)
            {
                @PopUpSelect.started -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpSelect;
                @PopUpSelect.performed -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpSelect;
                @PopUpSelect.canceled -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpSelect;
                @PopUpBack.started -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpBack;
                @PopUpBack.performed -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpBack;
                @PopUpBack.canceled -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpBack;
                @PopUpUp.started -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpUp;
                @PopUpUp.performed -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpUp;
                @PopUpUp.canceled -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpUp;
                @PopUpDown.started -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpDown;
                @PopUpDown.performed -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpDown;
                @PopUpDown.canceled -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpDown;
                @PopUpRight.started -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpRight;
                @PopUpRight.performed -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpRight;
                @PopUpRight.canceled -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpRight;
                @PopUpLeft.started -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpLeft;
                @PopUpLeft.performed -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpLeft;
                @PopUpLeft.canceled -= m_Wrapper.m_PopUpUIActionsCallbackInterface.OnPopUpLeft;
            }
            m_Wrapper.m_PopUpUIActionsCallbackInterface = instance;
            if (instance != null)
            {
                @PopUpSelect.started += instance.OnPopUpSelect;
                @PopUpSelect.performed += instance.OnPopUpSelect;
                @PopUpSelect.canceled += instance.OnPopUpSelect;
                @PopUpBack.started += instance.OnPopUpBack;
                @PopUpBack.performed += instance.OnPopUpBack;
                @PopUpBack.canceled += instance.OnPopUpBack;
                @PopUpUp.started += instance.OnPopUpUp;
                @PopUpUp.performed += instance.OnPopUpUp;
                @PopUpUp.canceled += instance.OnPopUpUp;
                @PopUpDown.started += instance.OnPopUpDown;
                @PopUpDown.performed += instance.OnPopUpDown;
                @PopUpDown.canceled += instance.OnPopUpDown;
                @PopUpRight.started += instance.OnPopUpRight;
                @PopUpRight.performed += instance.OnPopUpRight;
                @PopUpRight.canceled += instance.OnPopUpRight;
                @PopUpLeft.started += instance.OnPopUpLeft;
                @PopUpLeft.performed += instance.OnPopUpLeft;
                @PopUpLeft.canceled += instance.OnPopUpLeft;
            }
        }
    }
    public PopUpUIActions @PopUpUI => new PopUpUIActions(this);
    public interface IInGameActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnWallGrip(InputAction.CallbackContext context);
        void OnHover(InputAction.CallbackContext context);
        void OnRaise(InputAction.CallbackContext context);
        void OnFire(InputAction.CallbackContext context);
        void OnPrecisionAimTrigger(InputAction.CallbackContext context);
        void OnPrecisionAimGamepad(InputAction.CallbackContext context);
        void OnPrecisionAimMouse(InputAction.CallbackContext context);
        void OnRepair(InputAction.CallbackContext context);
        void OnAction(InputAction.CallbackContext context);
        void OnPause(InputAction.CallbackContext context);
        void OnCameraShift(InputAction.CallbackContext context);
        void OnPistolKeyboard(InputAction.CallbackContext context);
        void OnMachineGunKeyboard(InputAction.CallbackContext context);
        void OnShotgunKeyboard(InputAction.CallbackContext context);
        void OnRocketLauncherKeyboard(InputAction.CallbackContext context);
        void OnChargeRifleKeyboard(InputAction.CallbackContext context);
        void OnSwitchChipPickup(InputAction.CallbackContext context);
        void OnNextWeaponFormController(InputAction.CallbackContext context);
        void OnPreviousWeaponFormController(InputAction.CallbackContext context);
        void OnCycleWeaponFormKeyboard(InputAction.CallbackContext context);
    }
    public interface IMenuActions
    {
        void OnSelect(InputAction.CallbackContext context);
        void OnBack(InputAction.CallbackContext context);
        void OnUp(InputAction.CallbackContext context);
        void OnDown(InputAction.CallbackContext context);
        void OnRight(InputAction.CallbackContext context);
        void OnLeft(InputAction.CallbackContext context);
        void OnUnpause(InputAction.CallbackContext context);
        void OnCloseWindow(InputAction.CallbackContext context);
    }
    public interface IPopUpUIActions
    {
        void OnPopUpSelect(InputAction.CallbackContext context);
        void OnPopUpBack(InputAction.CallbackContext context);
        void OnPopUpUp(InputAction.CallbackContext context);
        void OnPopUpDown(InputAction.CallbackContext context);
        void OnPopUpRight(InputAction.CallbackContext context);
        void OnPopUpLeft(InputAction.CallbackContext context);
    }
}
