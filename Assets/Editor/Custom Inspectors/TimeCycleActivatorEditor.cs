﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using RoboMania;

[CustomEditor(typeof(TimeCycleActivator))]
[CanEditMultipleObjects]
public class TimeCycleActivatorEditor : ActivatorEditor
{
    private SerializedProperty _separateOnOffCycleDurationsProp;
    private SerializedProperty _timeCycleDurationProp;
    private SerializedProperty _cycleOnDurationProp;
    private SerializedProperty _cycleOffDurationProp;

    protected override void OnEnable()
    {
        base.OnEnable();
        _separateOnOffCycleDurationsProp = serializedObject.FindProperty("_separateOnOffCycleDurations");
        _timeCycleDurationProp = serializedObject.FindProperty("_timeCycleDuration");
        _cycleOnDurationProp = serializedObject.FindProperty("_cycleOnDuration");
        _cycleOffDurationProp = serializedObject.FindProperty("_cycleOffDuration");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.PropertyField(_separateOnOffCycleDurationsProp, new GUIContent("Separate On/Off Cycle Duration"));

        if (_separateOnOffCycleDurationsProp.boolValue)
        {
            EditorGUILayout.PropertyField(_cycleOnDurationProp, new GUIContent("Cycle On Duration (in seconds)"));
            EditorGUILayout.PropertyField(_cycleOffDurationProp, new GUIContent("Cycle Off Duration (in seconds)"));
        }
        else
        {
            EditorGUILayout.PropertyField(_timeCycleDurationProp, new GUIContent("Time Cycle Duration (in seconds)"));
        }

        serializedObject.ApplyModifiedProperties();
    }
}
