﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LevelComponents.Platforms;

[CustomEditor(typeof(PressurePlatform))]
public class PressurePlatformEditor : Editor
{
    private SerializedProperty _speedProp;
    private SerializedProperty _allowedToMoveEvenIfBlockedProp;

    private SerializedProperty _pressurePropertiesProp;
    private SerializedProperty _sinkingPropertiesProp;
    private SerializedProperty _rendererMasterObjectProp;

    protected void OnEnable()
    {
        _speedProp = serializedObject.FindProperty("Speed");
        _allowedToMoveEvenIfBlockedProp = serializedObject.FindProperty("allowedToMoveEvenIfBlocked");

        _pressurePropertiesProp = serializedObject.FindProperty("_pressureProperties");
        _sinkingPropertiesProp = serializedObject.FindProperty("_sinkingProperties");
        _rendererMasterObjectProp = serializedObject.FindProperty("RendererMasterObject");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_speedProp, new GUIContent("Speed"));
        EditorGUILayout.PropertyField(_allowedToMoveEvenIfBlockedProp, new GUIContent("Platform Allowed to Move Even if Blocked"));

        if (_speedProp.floatValue < 0)
        {
            EditorGUILayout.PropertyField(_sinkingPropertiesProp, new GUIContent("Sinking Platform Properties"));
        }
        else
        {
            EditorGUILayout.PropertyField(_pressurePropertiesProp, new GUIContent("Pressure Platform Properties"));
        }

        EditorGUILayout.PropertyField(_rendererMasterObjectProp, new GUIContent("Renderer Master Object"));

        serializedObject.ApplyModifiedProperties();
    }
}
