﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WarpGameObjectOnActivate))]
public class WarpGameObjectOnActivateEditor : Editor
{
    private SerializedProperty _objectToWarpProp;
    private SerializedProperty _localPositionToWarpToProp;
    private SerializedProperty _objectRotationAfterWarpProp;
    private SerializedProperty _oneTimeOnlyProp;
    private SerializedProperty _timeOutOnWarpProp;

    private void OnEnable()
    {
        _objectToWarpProp = serializedObject.FindProperty("_objectToWarp");
        _localPositionToWarpToProp = serializedObject.FindProperty("_localPositionToWarpTo");
        _objectRotationAfterWarpProp = serializedObject.FindProperty("_objectRotationAfterWarp");
        _oneTimeOnlyProp = serializedObject.FindProperty("_oneTimeWarpOnly");
        _timeOutOnWarpProp = serializedObject.FindProperty("_timeOutOnWarp");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_objectToWarpProp, new GUIContent("Object To Warp"));
        EditorGUILayout.PropertyField(_localPositionToWarpToProp, new GUIContent("Local Position To Warp To"));
        EditorGUILayout.PropertyField(_objectRotationAfterWarpProp, new GUIContent("Object Rotation After Warp"));
        EditorGUILayout.PropertyField(_oneTimeOnlyProp, new GUIContent("Warp One Time Only"));
        if (!_oneTimeOnlyProp.boolValue)
        {
            EditorGUILayout.PropertyField(_timeOutOnWarpProp, new GUIContent("Time Out On Warp"));
        }

        serializedObject.ApplyModifiedProperties();
    }
}
