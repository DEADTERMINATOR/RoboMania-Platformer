﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Projectile))]
[CanEditMultipleObjects]
public class ProjectileEditor : Editor
{
    private SerializedProperty _speedProp;
    private SerializedProperty _typeProp;
    private SerializedProperty _killOffScreenProp;
    private SerializedProperty _screenBufferProp;
    private SerializedProperty _killTimeProp;
    private SerializedProperty _particleElementsToModifyForDamageIndicationProp;
    //private SerializedProperty _soundManagerProp;
    private SerializedProperty _transformToOperateOnProp;
    private SerializedProperty _projectileVisualTransformProp;
    private SerializedProperty _projectileImpactEffectProp;
    private SerializedProperty _projectileMustRotateAroundX;

    protected virtual void OnEnable()
    {
        _speedProp = serializedObject.FindProperty("ProjectileSpeed");
        _typeProp = serializedObject.FindProperty("type");
        _killOffScreenProp = serializedObject.FindProperty("killOffScreen");
        _screenBufferProp = serializedObject.FindProperty("screenBuffer");
        _killTimeProp = serializedObject.FindProperty("killTime");
        _particleElementsToModifyForDamageIndicationProp = serializedObject.FindProperty("particleElementsToModifyForDamageIndication");
        //_soundManagerProp = serializedObject.FindProperty("soundManager");
        _transformToOperateOnProp = serializedObject.FindProperty("transformToOperateOn");
        _projectileVisualTransformProp = serializedObject.FindProperty("_projectileVisualTransform");
        _projectileImpactEffectProp = serializedObject.FindProperty("_projectileImpactEffect");
        _projectileMustRotateAroundX = serializedObject.FindProperty("projectileMustRotateAroundX");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_speedProp, new GUIContent("Speed"));
        EditorGUILayout.PropertyField(_typeProp, new GUIContent("Asset Type"));
        EditorGUILayout.PropertyField(_killOffScreenProp, new GUIContent("Destroy When Off Screen"));
        
        if (_killOffScreenProp.boolValue)
        {
            EditorGUILayout.PropertyField(_screenBufferProp, new GUIContent("Off Screen Buffer (in Pixels)"));
        }
        else
        {
            EditorGUILayout.PropertyField(_killTimeProp, new GUIContent("How Long Should the Projectile Stay Alive"));
        }

        EditorGUILayout.PropertyField(_particleElementsToModifyForDamageIndicationProp, new GUIContent("Elements to Tint to Indicate Projectile Has Taken Damage"), true);
        //EditorGUILayout.PropertyField(_soundManagerProp, new GUIContent("Sound Manager To Use"));
        EditorGUILayout.PropertyField(_transformToOperateOnProp, new GUIContent("Transform to Operate On"));
        EditorGUILayout.PropertyField(_projectileVisualTransformProp, new GUIContent("Projectile Visuals Transform"));
        EditorGUILayout.PropertyField(_projectileImpactEffectProp, new GUIContent("Projectile Impact Effect"));
        EditorGUILayout.PropertyField(_projectileMustRotateAroundX, new GUIContent("Projectile Must Rotate Around X-Axis"));

        serializedObject.ApplyModifiedProperties();
    }
}
