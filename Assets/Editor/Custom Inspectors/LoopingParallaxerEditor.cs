﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using _ShaderoShaderEditorFramework;

[CustomEditor(typeof(LoopingParallaxer))]
public class LoopingParallaxerEditor : Editor
{

    private bool _editbound;
    private LoopingParallaxer LoopingParallaxerTarget;
    private void OnEnable()
    {

    }

    public override void OnInspectorGUI()
    {
        bool update = false;
        LoopingParallaxerTarget = target as LoopingParallaxer;

        SpriteRenderer sprite = EditorGUILayout.ObjectField(new GUIContent("Sprite"), LoopingParallaxerTarget.Sprite, typeof(SpriteRenderer), true) as SpriteRenderer;

        Vector2 move = EditorGUILayout.Vector2Field(new GUIContent("MovmentFactor"), LoopingParallaxerTarget.MovementFactor, null);
        if (move != LoopingParallaxerTarget.MovementFactor)
        {
            LoopingParallaxerTarget.MovementFactor = move;
            update = true;
        }
        LoopingParallaxerTarget.Camera = EditorGUILayout.ObjectField(new GUIContent("Camera"), LoopingParallaxerTarget.Camera, typeof(Camera), true) as Camera;
        if (sprite != null && sprite != LoopingParallaxerTarget.Sprite)
        {
            Rect anewRect = new Rect();
            anewRect.size = sprite.bounds.size;
            anewRect.center = LoopingParallaxerTarget.transform.position;
            update = true;
            LoopingParallaxerTarget.MovmentBounds = anewRect;
        }
        else if (!_editbound)
        {
            Rect newRect = EditorGUILayout.RectField(new GUIContent("Movement Bounds"), LoopingParallaxerTarget.MovmentBounds, null);
            if (newRect != LoopingParallaxerTarget.MovmentBounds)
            {
                LoopingParallaxerTarget.MovmentBounds = newRect;
                update = true;
            }
        }
        LoopingParallaxerTarget.Sprite = sprite;
        if (update)
        {
            EditorUtility.SetDirty(LoopingParallaxerTarget);
        }
    }



    void OnSceneGUI()
    {


        LoopingParallaxer LoopingParallaxerTarget = target as LoopingParallaxer;
        BoxBoundsHandle _boundsHandle = new BoxBoundsHandle();
        _boundsHandle.center = LoopingParallaxerTarget.MovmentBounds.center;
        _boundsHandle.size = LoopingParallaxerTarget.MovmentBounds.size;
        _boundsHandle.SetColor(Color.cyan);
        EditorGUI.BeginChangeCheck();
        _boundsHandle.DrawHandle();
        if (EditorGUI.EndChangeCheck())
        {
            Debug.Log("Edit");
            // record the target object before setting new values so changes can be undone/redone
            Undo.RecordObject(LoopingParallaxerTarget, "Change Bounds");
            Rect anewRect = new Rect();
            anewRect.size = _boundsHandle.size;
            anewRect.center = _boundsHandle.center; ;
            LoopingParallaxerTarget.MovmentBounds = anewRect;
            EditorUtility.SetDirty(target);

        }

    }


}