using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MatchShadowCasterPointsForAllTerrain))]
public class MatchShadowCasterPointsForAllTerrainEditor : Editor
{
    private SerializedProperty _addMissingShadowCastersProp;

    private void OnEnable()
    {
        _addMissingShadowCastersProp = serializedObject.FindProperty("_addMissingShadowCasters");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_addMissingShadowCastersProp, new GUIContent("Add Missing Shadow Casters"));
        if (GUILayout.Button("Process All Ferr2D Terrains"))
        {
            var terrainProcessor = serializedObject.targetObject as MatchShadowCasterPointsForAllTerrain;
            if (terrainProcessor != null)
            {
                terrainProcessor.ProcessAllTerrain();
            }
        }

        serializedObject.ApplyModifiedProperties();
    }
}
