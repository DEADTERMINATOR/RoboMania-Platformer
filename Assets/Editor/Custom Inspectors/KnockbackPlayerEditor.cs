﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(KnockbackPlayer))]
[CanEditMultipleObjects]
public class KnockbackPlayerEditor : KnockbackEditor
{
    private SerializedProperty _applyKnockbackWhenPlayerInvulnerableProp;
    private SerializedProperty _isNPCProp;
    private SerializedProperty _owningNPCProp;
    private SerializedProperty _shouldDamageNPCsProp;
    private SerializedProperty _npcDamageMultiplierProp;

    private void OnEnable()
    {
        GetBaseProperties();

        _applyKnockbackWhenPlayerInvulnerableProp = serializedObject.FindProperty("_applyKnockbackWhenPlayerInvulnerable");
        _isNPCProp = serializedObject.FindProperty("_isNPC");
        _owningNPCProp = serializedObject.FindProperty("_owningNPC");
        _shouldDamageNPCsProp = serializedObject.FindProperty("_shouldDamageNPCs");
        _npcDamageMultiplierProp = serializedObject.FindProperty("_npcDamageMultiplier");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        DrawBaseProperties();

        EditorGUILayout.PropertyField(_applyKnockbackWhenPlayerInvulnerableProp, new GUIContent("Apply Knockback When Player Invulnerable"));
        EditorGUILayout.PropertyField(_isNPCProp, new GUIContent("Object Applying Knockback Is NPC"));
        if (_isNPCProp.boolValue)
        {
            EditorGUILayout.PropertyField(_owningNPCProp, new GUIContent("NPC Component"));
        }
        EditorGUILayout.PropertyField(_shouldDamageNPCsProp, new GUIContent("Should Damage NPCs"));
        if (_shouldDamageNPCsProp.boolValue)
        {
            EditorGUILayout.PropertyField(_npcDamageMultiplierProp, new GUIContent("NPC Damage Multiplier"));
        }

        serializedObject.ApplyModifiedProperties();
    }
}
