﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ProjectileSpawnAndFire))]
[CanEditMultipleObjects]
public class ProjectileSpawnAndFireEditor : Editor
{
    private SerializedProperty _projectileObjectProp;
    private SerializedProperty _delayProp;
    private SerializedProperty _firePositionOffsetProp;
    private SerializedProperty _overrideProjectileSpeedProp;
    private SerializedProperty _projectileSpeedProp;
    private SerializedProperty _damageProp;
    private SerializedProperty _forceProp;
    private SerializedProperty _angleProp;
    private SerializedProperty _typeProp;
    private SerializedProperty _startDelayProp;
    private SerializedProperty _run;
    private void OnEnable()
    {
        _projectileObjectProp = serializedObject.FindProperty("_projectileObject");
        _delayProp = serializedObject.FindProperty("_timeBetweenShots");
        _firePositionOffsetProp = serializedObject.FindProperty("_firePositionOffset");
        _projectileSpeedProp = serializedObject.FindProperty("_projectileSpeed");
        _overrideProjectileSpeedProp = serializedObject.FindProperty("_overrideProjectileSpeed");
        _damageProp = serializedObject.FindProperty("_damageAmount");
        _forceProp = serializedObject.FindProperty("_force");
        _angleProp = serializedObject.FindProperty("_angle");
        _typeProp = serializedObject.FindProperty("_type");
        _startDelayProp = serializedObject.FindProperty("_startDelay");
        _run = serializedObject.FindProperty("_run");
    }
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        ProjectileSpawnAndFire projectileSpawnAndFire = target as ProjectileSpawnAndFire;

        EditorGUILayout.PropertyField(_projectileObjectProp, new GUIContent("Projectile Object"));
        EditorGUILayout.PropertyField(_typeProp, new GUIContent("Projectile Type"));

        if (projectileSpawnAndFire.Type == ProjectileSpawnAndFire.ProjectileType.ArcedProjectile)
        {
            EditorGUILayout.PropertyField(_forceProp, new GUIContent("Force"));
        }
        else
        {
            EditorGUILayout.PropertyField(_overrideProjectileSpeedProp, new GUIContent("Override Projectile Speed"));
            if (projectileSpawnAndFire.OverrideProjectileSpeed)
            {
                EditorGUILayout.PropertyField(_projectileSpeedProp, new GUIContent("Projectile Speed"));
            }
        }

        EditorGUILayout.PropertyField(_angleProp, new GUIContent("Angle"));
        EditorGUILayout.PropertyField(_damageProp, new GUIContent("Damage"));
        EditorGUILayout.PropertyField(_startDelayProp, new GUIContent("Delay Start By (Seconds)"));
        EditorGUILayout.PropertyField(_delayProp, new GUIContent("Time Between Shots"));
        EditorGUILayout.PropertyField(_firePositionOffsetProp, new GUIContent("Fire Position Offset"));
        EditorGUILayout.PropertyField(_run, new GUIContent("Running"));
        serializedObject.ApplyModifiedProperties();
    }

}