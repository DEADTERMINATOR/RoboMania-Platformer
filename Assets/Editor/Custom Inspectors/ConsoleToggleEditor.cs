﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ConsoleToggle))]
public class ConsoleToggleEditor : Editor
{
    private SerializedProperty _activeProp;
    private SerializedProperty _isTurnedOnProp;

    private SerializedProperty _initiallyActivatedLinesProp;
    private SerializedProperty _initiallyDeactivatedLinesProp;

    private SerializedProperty _isTimeLimitedProp;
    private SerializedProperty _timeLimitProp;

    private SerializedProperty _hasDifferentAssetsPerStateProp;
    private SerializedProperty _consoleOnProp;
    private SerializedProperty _consoleOffProp;
    private SerializedProperty _consoleNoPowerProp;


    private void OnEnable()
    {
        _activeProp = serializedObject.FindProperty("_isPowered");
        _isTurnedOnProp = serializedObject.FindProperty("_isTurnedOn");

        _initiallyActivatedLinesProp = serializedObject.FindProperty("_initiallyActivatedLines");
        _initiallyDeactivatedLinesProp = serializedObject.FindProperty("_initiallyDeactivatedLines");

        _isTimeLimitedProp = serializedObject.FindProperty("_isTimeLimited");
        _timeLimitProp = serializedObject.FindProperty("_timeLimit");

        _hasDifferentAssetsPerStateProp = serializedObject.FindProperty("_hasDifferentAssetsPerState");
        _consoleOnProp = serializedObject.FindProperty("_consoleOn");
        _consoleOffProp = serializedObject.FindProperty("_consoleOff");
        _consoleNoPowerProp = serializedObject.FindProperty("_consoleNoPower");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_activeProp, new GUIContent("Console is Powered"));
        EditorGUILayout.PropertyField(_isTurnedOnProp, new GUIContent("Console is On"));

        EditorGUILayout.PropertyField(_initiallyActivatedLinesProp, new GUIContent("Initially Activated Power Lines"), true);
        EditorGUILayout.PropertyField(_initiallyDeactivatedLinesProp, new GUIContent("Initially Deactivated Power Lines"), true);

        EditorGUILayout.PropertyField(_isTimeLimitedProp, new GUIContent("Time Limited When Turned On"));
        if (_isTimeLimitedProp.boolValue)
        {
            EditorGUILayout.PropertyField(_timeLimitProp, new GUIContent("Time Limit"));
        }

        EditorGUILayout.PropertyField(_hasDifferentAssetsPerStateProp, new GUIContent("Has Different Assets For Different State"));
        if (_hasDifferentAssetsPerStateProp.boolValue)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("On and Off Are Required", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(_consoleOnProp, new GUIContent("Console On"));
            EditorGUILayout.PropertyField(_consoleOffProp, new GUIContent("Console Off"));

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("No Power is Not Required", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(_consoleNoPowerProp, new GUIContent("Console No Power"));

            EditorGUILayout.Space();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
