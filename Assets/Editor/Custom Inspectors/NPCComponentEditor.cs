﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Characters;

[CustomEditor(typeof(NPC))]
public class NPCComponentEditor : Editor
{
    private SerializedProperty _movementTypeProp;
    private SerializedProperty _baseMoveSpeedProp;
    private SerializedProperty _memberOfNPCGroupProp;
    private SerializedProperty _characterSpriteCollectionProp;
    private SerializedProperty _canFallOffPlatformsProp;
    private SerializedProperty _shouldAnalyzePlatformProp;
    private SerializedProperty _maxPlatformAnalysisDistanceProp;

    private void OnEnable()
    {
        _movementTypeProp = serializedObject.FindProperty("_movementType");
        _baseMoveSpeedProp = serializedObject.FindProperty("_baseMoveSpeed");
        _memberOfNPCGroupProp = serializedObject.FindProperty("_memberOfNPCGroup");
        _characterSpriteCollectionProp = serializedObject.FindProperty("_characterSpriteCollection");
        _canFallOffPlatformsProp = serializedObject.FindProperty("_canFallOffPlatforms");
        _shouldAnalyzePlatformProp = serializedObject.FindProperty("_shouldAnalyzePlatform");
        _maxPlatformAnalysisDistanceProp = serializedObject.FindProperty("_maxPlatformAnalysisDistance");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_movementTypeProp, new GUIContent("Movement Type"), true);
        EditorGUILayout.PropertyField(_baseMoveSpeedProp, new GUIContent("Base Move Speed"));
        EditorGUILayout.PropertyField(_memberOfNPCGroupProp, new GUIContent("Member of NPC Group"));
        EditorGUILayout.PropertyField(_characterSpriteCollectionProp, new GUIContent("Character Sprite Root"));
        EditorGUILayout.PropertyField(_canFallOffPlatformsProp, new GUIContent("Can Fall Off Platforms"));
        if (_movementTypeProp.enumValueIndex == 0)
        {
            EditorGUILayout.PropertyField(_shouldAnalyzePlatformProp, new GUIContent("Should Analyze Plaform"));
            if (_shouldAnalyzePlatformProp.boolValue)
            {
                EditorGUILayout.PropertyField(_maxPlatformAnalysisDistanceProp, new GUIContent("Maximum Platform Analysis Distance"));
            }
        }

        serializedObject.ApplyModifiedProperties();
    }
}
