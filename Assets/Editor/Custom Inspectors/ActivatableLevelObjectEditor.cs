﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ActivatableLevelObjectEditor : Editor
{
    private SerializedProperty _activatedProp;
    private SerializedProperty _activateWithActivatorProp;
    private SerializedProperty _neverDeactivateOnceActiveProp;
    private SerializedProperty _runBareBonesUpdateProp;
    private SerializedProperty _objectsToActivateWithLevelObjectProp;

    protected virtual void OnEnable()
    {
        _activatedProp = serializedObject.FindProperty("activated");
        _activateWithActivatorProp = serializedObject.FindProperty("activateWithActivator");
        _runBareBonesUpdateProp = serializedObject.FindProperty("runBareBonesUpdate");
        _neverDeactivateOnceActiveProp = serializedObject.FindProperty("neverDeactivateOnceActive");
        _objectsToActivateWithLevelObjectProp = serializedObject.FindProperty("objectsToActivateWithLevelObject");
    }

    public override void OnInspectorGUI()
    {
        bool currentActive = _activatedProp.boolValue;

        serializedObject.Update();

        if (!_activateWithActivatorProp.boolValue)
        {
            EditorGUILayout.PropertyField(_activatedProp, new GUIContent("Active"));
            if (!currentActive && _activatedProp.boolValue)
            {
                //If the Active box was just ticked, also automatically set runBareBonesUpdate to true to avoid issues with objects being deactivated when the player gets too far away from them, but the bare bones not being run in this case.
                _runBareBonesUpdateProp.boolValue = true;
            }
        }

        EditorGUILayout.PropertyField(_activateWithActivatorProp, new GUIContent("Activate With Activator"));
        EditorGUILayout.PropertyField(_neverDeactivateOnceActiveProp, new GUIContent("Never Deactivate Once Activated"));
        EditorGUILayout.PropertyField(_runBareBonesUpdateProp, new GUIContent("Should Run Bare Bones Update When Deactivated"));
        EditorGUILayout.PropertyField(_objectsToActivateWithLevelObjectProp, new GUIContent("Objects To Activate With This Object"));
    }
}
