﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RangeLimitedSnapRotatingTrap)), CanEditMultipleObjects]
public class RangeLimitedSnapRotatingTrapEditor : SnapRotatingTrapEditor
{
    private SerializedProperty _minAngleProp;
    private SerializedProperty _maxAngleProp;

    protected override void OnEnable()
    {
        base.OnEnable();

        _minAngleProp = serializedObject.FindProperty("_minAngle");
        _maxAngleProp = serializedObject.FindProperty("_maxAngle");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (!_synchronizeWithOtherTrapsProp.boolValue)
        {
            EditorGUILayout.PropertyField(_minAngleProp, new GUIContent("Minimum Angle"));
            EditorGUILayout.PropertyField(_maxAngleProp, new GUIContent("Maximum Angle"));
        }

        serializedObject.ApplyModifiedProperties();
    }
}
