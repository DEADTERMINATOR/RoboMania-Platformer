﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LevelComponents.Platforms;

[CustomEditor(typeof(AutomaticMovingPlatform))]
public class AutomaticMovingPlatformEditor : MovingPlatformEditor
{
    private SerializedProperty _platformMovedEventProp;

    protected override void OnEnable()
    {
        base.OnEnable();
        _platformMovedEventProp = serializedObject.FindProperty("PlatformMoved");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.PropertyField(_platformMovedEventProp, new GUIContent("Platform Moved"));
        serializedObject.ApplyModifiedProperties();
    }
}
