﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MovingPlatformPathComponent))]
public class MovingPlatformPathComponentEditor : Editor
{
    private SerializedProperty _waypointsProp;
    private SerializedProperty _endWaypointModeProp;

    private int _waypointsCount;


    private void OnEnable()
    {
        _waypointsProp = serializedObject.FindProperty("_waypoints");
        _endWaypointModeProp = serializedObject.FindProperty("_endWaypointMode");
    }

    public override void OnInspectorGUI()
    {
        _waypointsCount = _waypointsProp.arraySize;

        serializedObject.Update();

        EditorGUILayout.PropertyField(_waypointsProp, new GUIContent("Waypoints"), true);
        if (_waypointsProp.arraySize > _waypointsCount)
        {
            for (int i = _waypointsCount; i < _waypointsProp.arraySize; ++i)
            {
                var currentIndex = _waypointsProp.GetArrayElementAtIndex(i);
                var waypoint = currentIndex.FindPropertyRelative("Position");

                var targetGO = serializedObject.targetObject as MovingPlatformPathComponent;
                waypoint.vector3Value = targetGO.transform.position;
            }
        }
        EditorGUILayout.PropertyField(_endWaypointModeProp, new GUIContent("End Waypoint Mode"), true);

        serializedObject.ApplyModifiedProperties();
    }
}
