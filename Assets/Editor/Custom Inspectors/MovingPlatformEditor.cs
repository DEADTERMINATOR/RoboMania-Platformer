﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LevelComponents.Platforms;

[CustomEditor(typeof(MovingPlatform))]
public class MovingPlatformEditor : Editor
{
    private SerializedProperty _speedProp;
    private SerializedProperty _allowedToMoveEvenIfBlockedProp;

    private SerializedProperty _waitTimeProp;
    private SerializedProperty _easeFactorProp;
    private SerializedProperty _waypointsProp;
    private SerializedProperty _startingWaypointProp;
    private SerializedProperty _startFromStartingWaypointProp;
    private SerializedProperty _editorPathListProp;
    private SerializedProperty _endWaypointModeProp;
    private SerializedProperty _speedUpWhenOffScreenProp;
    private SerializedProperty _influencedPassengerAreaProp;
    private SerializedProperty _nonPassengerSideCollisionMaskProp;
    private SerializedProperty _checkForObstaclesProp;

    private int _waypointsCount;


    protected virtual void OnEnable()
    {
        //Platform properties
        _speedProp = serializedObject.FindProperty("Speed");
        _allowedToMoveEvenIfBlockedProp = serializedObject.FindProperty("allowedToMoveEvenIfBlocked");

        //MovingPlatform properties
        _waitTimeProp = serializedObject.FindProperty("waitTime");
        _easeFactorProp = serializedObject.FindProperty("easeFactor");
        _waypointsProp = serializedObject.FindProperty("waypoints");
        _startingWaypointProp = serializedObject.FindProperty("startingWaypoint");
        _startFromStartingWaypointProp = serializedObject.FindProperty("startFromStartingWaypoint");
        _editorPathListProp = serializedObject.FindProperty("editorPathList");
        _endWaypointModeProp = serializedObject.FindProperty("endWaypointMode");
        _speedUpWhenOffScreenProp = serializedObject.FindProperty("speedUpWhenOffScreen");
        _influencedPassengerAreaProp = serializedObject.FindProperty("influencedPassengerArea");
        _nonPassengerSideCollisionMaskProp = serializedObject.FindProperty("nonPassengerSideCollisionMask");
        _checkForObstaclesProp = serializedObject.FindProperty("_checkForObstacles");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        _waypointsCount = _waypointsProp.arraySize;

        EditorGUILayout.PropertyField(_speedProp, new GUIContent("Speed"));
        EditorGUILayout.PropertyField(_allowedToMoveEvenIfBlockedProp, new GUIContent("Platform Allowed to Move Even if Blocked"));
        EditorGUILayout.PropertyField(_waitTimeProp, new GUIContent("Wait Time"));

        EditorGUILayout.PropertyField(_easeFactorProp, new GUIContent("Ease Factor"));
        if (_easeFactorProp.floatValue < 1)
        {
            _easeFactorProp.floatValue = 1;
        }

        EditorGUILayout.PropertyField(_waypointsProp, new GUIContent("Waypoints"), true);
        if (_waypointsProp.arraySize > _waypointsCount)
        {
            for (int i = _waypointsCount; i < _waypointsProp.arraySize; ++i)
            {
                var currentIndex = _waypointsProp.GetArrayElementAtIndex(i);
                var waypoint = currentIndex.FindPropertyRelative("Position");

                var targetGO = serializedObject.targetObject as MovingPlatform;
                waypoint.vector3Value = targetGO.transform.position;
            }
        }

        EditorGUILayout.PropertyField(_startingWaypointProp, new GUIContent("Starting Waypoint"));
        EditorGUILayout.PropertyField(_startFromStartingWaypointProp, new GUIContent("Start From Starting Waypoint"));

        EditorGUILayout.PropertyField(_editorPathListProp, new GUIContent("Platform Paths"), true);
        EditorGUILayout.PropertyField(_endWaypointModeProp, new GUIContent("End Waypoint Mode"));
        EditorGUILayout.PropertyField(_speedUpWhenOffScreenProp, new GUIContent("Speed Up When Platform Off Screen"));
        EditorGUILayout.PropertyField(_influencedPassengerAreaProp, new GUIContent("Passenger Velocity Influence Trigger Area"));
        EditorGUILayout.PropertyField(_nonPassengerSideCollisionMaskProp, new GUIContent("Non Passenger Side Collision Mask"));
        EditorGUILayout.PropertyField(_checkForObstaclesProp, new GUIContent("Check for Obstacles"));

        serializedObject.ApplyModifiedProperties();
    }
}
