﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SnapRotatingTrap)), CanEditMultipleObjects]
public class SnapRotatingTrapEditor : Editor
{
    protected SerializedProperty _synchronizeWithOtherTrapsProp;

    private SerializedProperty _activeProp;
    private SerializedProperty _axisProp;
    private SerializedProperty _initialRotationProp;

    private SerializedProperty _snapRotationAmountProp;
    private SerializedProperty _timesBetweenRotationsProp;
    private SerializedProperty _startingTimeBetweenRotationsProp;
    private SerializedProperty _rotationTimeProp;
    private SerializedProperty _warnBeforeRotationProp;
    private SerializedProperty _warningSpriteProp;
    private SerializedProperty _singleSnapProp;
    private SerializedProperty _finishRotationBeforeDeactivationProp;
    private SerializedProperty _minSafeRotationProp;
    private SerializedProperty _maxSafeRotationProp;


    protected virtual void OnEnable()
    {
        _activeProp = serializedObject.FindProperty("active");
        _axisProp = serializedObject.FindProperty("axis");
        _initialRotationProp = serializedObject.FindProperty("initialRotation");

        _snapRotationAmountProp = serializedObject.FindProperty("snapRotationAmount");
        _timesBetweenRotationsProp = serializedObject.FindProperty("_timesBetweenRotations");
        _startingTimeBetweenRotationsProp = serializedObject.FindProperty("_startingTimeBetweenRotations");
        _rotationTimeProp = serializedObject.FindProperty("_rotationTime");
        _warnBeforeRotationProp = serializedObject.FindProperty("_warnBeforeRotation");
        _warningSpriteProp = serializedObject.FindProperty("_warningSprite");
        _singleSnapProp = serializedObject.FindProperty("_singleSnap");
        _synchronizeWithOtherTrapsProp = serializedObject.FindProperty("_synchronizeWithOtherTraps");
        _finishRotationBeforeDeactivationProp = serializedObject.FindProperty("_finishRotationBeforeDeactivation");
        _minSafeRotationProp = serializedObject.FindProperty("_minSafeRotation");
        _maxSafeRotationProp = serializedObject.FindProperty("_maxSafeRotation");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        if (!_synchronizeWithOtherTrapsProp.boolValue)
        {
            EditorGUILayout.PropertyField(_activeProp, new GUIContent("Active"));
        }

        EditorGUILayout.PropertyField(_axisProp, new GUIContent("Axis"));
        EditorGUILayout.PropertyField(_initialRotationProp, new GUIContent("Initial Rotation"));

        EditorGUILayout.PropertyField(_synchronizeWithOtherTrapsProp, new GUIContent("Part of Synchronized Trap"));
        EditorGUILayout.PropertyField(_finishRotationBeforeDeactivationProp, new GUIContent("Finish Rotation Before Deactivation (if Rotating)"));
        EditorGUILayout.PropertyField(_snapRotationAmountProp, new GUIContent("Snap Rotation Amount"));

        if (_synchronizeWithOtherTrapsProp.boolValue)
        {
            EditorGUILayout.PropertyField(_minSafeRotationProp, new GUIContent("Minimum Safe Rotation"));
            EditorGUILayout.PropertyField(_maxSafeRotationProp, new GUIContent("Maximum Safe Rotation"));
        }
        else
        {
            EditorGUILayout.LabelField(new GUIContent("If more than one time value is entered in this list, the time values will be cycled through with each rotation, starting with the first element in the list."), EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(_timesBetweenRotationsProp, new GUIContent("Times Between Rotations"), true);
            EditorGUILayout.PropertyField(_startingTimeBetweenRotationsProp, new GUIContent("Starting Time"));
            EditorGUILayout.PropertyField(_rotationTimeProp, new GUIContent("Rotation Time"));

            EditorGUILayout.PropertyField(_warnBeforeRotationProp, new GUIContent("Provide Warning Before Rotating"));
            if (_warnBeforeRotationProp.boolValue)
            {
                EditorGUILayout.PropertyField(_warningSpriteProp, new GUIContent("Sprite To Perform Warning Shake On"));
            }

            EditorGUILayout.PropertyField(_singleSnapProp, new GUIContent("Perform Only Single Snaps"));
        }

        serializedObject.ApplyModifiedProperties();
    }
}
