﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class KnockbackEditor : Editor
{
    private const string POSITIONAL_AWARENESS_TOOLTIP = "If set to true and if either X or Y knockback are set to EITHER, the knockback power is modified by the normalized collision direction vector. " +
                                                        "If set to false, the full amount of knockback power is applied either in the positive or negative direcion based on the direction of the collision, " +
                                                        "regardless of whether the collision direction occured perfectly along those axes.";
    private const string KNOCKBACK_BASE_VALUE_TOOLTIP = "Use knockback direction settings to set the intended knockback direction instead of negative values here. Negative values will be converted to positive ones.";
    private const string DESIRED_X_KNOCKBACK_TOOLTIP = "Selecting EITHER will cause the knockback direction to be calculated based on the point of collision.\nSelecting BACK or FORWARD will cause the knockback to move the player back or forward of the collider, respectively.";
    private const string DESIRED_Y_KNOCKBACK_TOOLTIP = "Selecting EITHER will cause the knockback direction to be calculated based on the point of collision.\nSelection UP or DOWN will cause the knockback to move the player up or down from the collider, respectively.";

    private SerializedProperty _usePositionalAwarenessProp;
    private SerializedProperty _knockbackBaseValueProp;
    private SerializedProperty _desiredXKnockbackDirectionProp;
    private SerializedProperty _desiredYKnockbackDirectionProp;
    private SerializedProperty _damageProp;

    protected void GetBaseProperties()
    {
        _usePositionalAwarenessProp = serializedObject.FindProperty("usePositionalAwareness");
        _knockbackBaseValueProp = serializedObject.FindProperty("knockbackBaseValue");
        _desiredXKnockbackDirectionProp = serializedObject.FindProperty("desiredXKnockbackDirection");
        _desiredYKnockbackDirectionProp = serializedObject.FindProperty("desiredYKnockbackDirection");
        _damageProp = serializedObject.FindProperty("damage");
    }

    protected void DrawBaseProperties()
    {
        EditorGUILayout.PropertyField(_knockbackBaseValueProp, new GUIContent("Knockback Amount", KNOCKBACK_BASE_VALUE_TOOLTIP));
        EditorGUILayout.PropertyField(_desiredXKnockbackDirectionProp, new GUIContent("Desired X Knockback Direction", DESIRED_X_KNOCKBACK_TOOLTIP));
        EditorGUILayout.PropertyField(_desiredYKnockbackDirectionProp, new GUIContent("Desired Y Knockback Direction", DESIRED_Y_KNOCKBACK_TOOLTIP));
        EditorGUILayout.PropertyField(_damageProp, new GUIContent("Damage"));
        EditorGUILayout.PropertyField(_usePositionalAwarenessProp, new GUIContent("Use Positional Awareness", POSITIONAL_AWARENESS_TOOLTIP));
    }
}
