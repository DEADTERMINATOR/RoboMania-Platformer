﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using RoboMania;

[CustomEditor(typeof(Activator))]
[CanEditMultipleObjects]
public class ActivatorEditor : Editor
{
    private SerializedProperty _activeProp;
    private SerializedProperty _objectsToActivateProp;
    private SerializedProperty _activateOverTimeProp;
    private SerializedProperty _deactivateOnExitProp;
    private SerializedProperty _onlyTriggerActivationOnceProp;
    private SerializedProperty _timeOutOnReactivationProp;
    private SerializedProperty _drawDebugLinesProp;
    private SerializedProperty _debugLinesColourProp;
    private SerializedProperty _tagsAllowedToActivateProp;
    private SerializedProperty _invertObjectActivationProp;

    protected virtual void OnEnable()
    {
        _activeProp = serializedObject.FindProperty("activeOnStart");
        _objectsToActivateProp = serializedObject.FindProperty("ObjectsToActivate");
        _activateOverTimeProp = serializedObject.FindProperty("ActivateOverTime");
        _deactivateOnExitProp = serializedObject.FindProperty("DeactivateOnExit");
        _onlyTriggerActivationOnceProp = serializedObject.FindProperty("OnlyTriggerActivationOnce");
        _timeOutOnReactivationProp = serializedObject.FindProperty("TimeOutOnReactivation");
        _drawDebugLinesProp = serializedObject.FindProperty("DrawDebugLines");
        _debugLinesColourProp = serializedObject.FindProperty("DebugLineColour");
        _tagsAllowedToActivateProp = serializedObject.FindProperty("_tagsAllowedToActivate");
        _invertObjectActivationProp = serializedObject.FindProperty("_invertObjectActivation");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_activeProp, new GUIContent("Active On Start"));
        EditorGUILayout.PropertyField(_objectsToActivateProp, new GUIContent("Objects To Activate"), true);
        EditorGUILayout.PropertyField(_invertObjectActivationProp, new GUIContent("Invert Object Activation"), true);
        _invertObjectActivationProp.arraySize = _objectsToActivateProp.arraySize;

        EditorGUILayout.Space();
        EditorGUILayout.LabelField(new GUIContent("Leaving the following list empty will default the activator to activate on contact with the player."), EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_tagsAllowedToActivateProp, new GUIContent("Tags Allowed to Activate"), true);
        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(_activateOverTimeProp, new GUIContent("Activate Over Time"));
        EditorGUILayout.PropertyField(_deactivateOnExitProp, new GUIContent("Deactivate On Exit"));

        EditorGUILayout.PropertyField(_onlyTriggerActivationOnceProp, new GUIContent("Only Trigger Activation Once"));
        if (!_onlyTriggerActivationOnceProp.boolValue)
        {
            EditorGUILayout.PropertyField(_timeOutOnReactivationProp, new GUIContent("Time Out On Reactivation"));
        }

        EditorGUILayout.PropertyField(_drawDebugLinesProp, new GUIContent("Draw Debug Lines"));
        if (_drawDebugLinesProp.boolValue)
        {
            EditorGUILayout.PropertyField(_debugLinesColourProp, new GUIContent("Debug Line Colour"));
        }

        serializedObject.ApplyModifiedProperties();
    }
}
