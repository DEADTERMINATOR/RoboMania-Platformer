﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TrapSynchronizationController)), CanEditMultipleObjects]
public class TrapSynchronizationControllerEditor : Editor
{
    private SerializedProperty _activeProp;
    private SerializedProperty _synchronizationModeProp;
    private SerializedProperty _trapGroupsProp;
    private SerializedProperty _initialSafeTrapGroupIndex;

    private SerializedProperty _overlapTimeProp;
    private SerializedProperty _alternateTimeProp;


    private void OnEnable()
    {
        _activeProp = serializedObject.FindProperty("_active");
        _synchronizationModeProp = serializedObject.FindProperty("_mode");
        _trapGroupsProp = serializedObject.FindProperty("_trapGroups");
        _initialSafeTrapGroupIndex = serializedObject.FindProperty("_initialSafeTrapGroupIndex");

        _overlapTimeProp = serializedObject.FindProperty("_overlapTime");
        _alternateTimeProp = serializedObject.FindProperty("_alternateTime");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_activeProp, new GUIContent("Active"));
        EditorGUILayout.PropertyField(_synchronizationModeProp, new GUIContent("Synchronization Mode"), true);
        EditorGUILayout.PropertyField(_trapGroupsProp, new GUIContent("Trap Groups"), true);
        EditorGUILayout.PropertyField(_initialSafeTrapGroupIndex, new GUIContent("Initial Safe Trap Group Index"));

        if (_synchronizationModeProp.enumValueIndex == 0) //Alternating Overlap mode
        {
            EditorGUILayout.PropertyField(_overlapTimeProp, new GUIContent("Overlap Time"));
            EditorGUILayout.PropertyField(_alternateTimeProp, new GUIContent("Alternate Time"));
        }

        serializedObject.ApplyModifiedProperties();
    }
}
