﻿using System.Collections;
using System.Collections.Generic;
using LevelComponents.Platforms;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RotateOnPressurePlatform))]
public class RotateOnPressurePlatformEditor : Editor
{
    private SerializedProperty _maxRotationProp;
    private SerializedProperty _maxRotationTimeProp;
    private SerializedProperty _timeUntilPlatformResetProp;
    private SerializedProperty _pivotPointIsNotAtCenterProp;
    private SerializedProperty _onlyRotateToDominantSideProp;
    private SerializedProperty _platformLogicIsChildOfRendererProp;


    private void OnEnable()
    {
        _maxRotationProp = serializedObject.FindProperty("_maxRotationInEitherDirection");
        _maxRotationTimeProp = serializedObject.FindProperty("_maxRotationTime");
        _timeUntilPlatformResetProp = serializedObject.FindProperty("_timeUntilPlatformReset");
        _pivotPointIsNotAtCenterProp = serializedObject.FindProperty("_pivotPointIsNotAtCenterOfObject");
        _onlyRotateToDominantSideProp = serializedObject.FindProperty("_onlyRotateToDominantSide");
        _platformLogicIsChildOfRendererProp = serializedObject.FindProperty("_platformLogicIsChildOfRenderer");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        RotateOnPressurePlatform pressurePlatform = target as RotateOnPressurePlatform;

        EditorGUILayout.PropertyField(_maxRotationProp, new GUIContent("Maximum Rotation In Either Direction"));
        EditorGUILayout.PropertyField(_maxRotationTimeProp, new GUIContent("Maximum Rotation Time"));
        EditorGUILayout.PropertyField(_timeUntilPlatformResetProp, new GUIContent("Time Until Platform Reset"));
        EditorGUILayout.PropertyField(_pivotPointIsNotAtCenterProp, new GUIContent("Pivot Point is Not at Center of Platform"));
        if (_pivotPointIsNotAtCenterProp.boolValue)
        {
            EditorGUILayout.PropertyField(_onlyRotateToDominantSideProp, new GUIContent("Only Rotate to Dominant Side"));
        }
        EditorGUILayout.PropertyField(_platformLogicIsChildOfRendererProp, new GUIContent("Platform Logic is Child of Renderer"));

        serializedObject.ApplyModifiedProperties();
    }
}
