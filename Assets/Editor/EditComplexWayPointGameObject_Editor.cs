﻿using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(EditComplexWayPointGameObject))]
public class EditComplexWayPointGameObject_Editor : Editor
{
    EditComplexWayPointGameObject editPoint;

    void OnSceneGUI()
    {
        EditComplexWayPointGameObject editPoint = (EditComplexWayPointGameObject)target;
        if (editPoint == null)
        {
            return;
        }
        GUIStyle style = new GUIStyle();
        style.fontSize = 25;
        style.fontStyle = FontStyle.Bold;
        style.normal.textColor = Color.white;



        Handles.color = Color.blue;
        Handles.Label(editPoint.transform.position + Vector3.up * 3, GetDisplyString(editPoint), style);
    
    }

    string GetDisplyString(EditComplexWayPointGameObject target)
    {
        string output = "Area" + target.OwningArea;
        if(target.IsPartrolPoint)
        {
            output += "\nPatrol Point #" + target.Index;
        }
        else
        {
            output += "\nConnection to "+ target.ConectedAreaID + " Point #" + target.Index ;
        }
        return output;
    }
   
}