﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(RequireInterfaceAttribute))]
public class RequireInterfaceDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Check if this is reference type property.
        if (property.propertyType == SerializedPropertyType.ObjectReference)
        {
            // Get attribute parameters.
            var requiredAttribute = attribute as RequireInterfaceAttribute;

            EditorGUI.BeginProperty(position, label, property);
            Object obj = EditorGUI.ObjectField(position, label, property.objectReferenceValue, typeof(Object), true);
            if (obj is GameObject go)
            {
                bool goPossessesAllInterfaces = true;
                var firstComponentWithInterface = go.GetComponent(requiredAttribute.InterfaceTypes[0]);

                if (firstComponentWithInterface != null)
                {
                    foreach (System.Type interfaceType in requiredAttribute.InterfaceTypes)
                    {
                        var goPossessingInterface = go.GetComponent(interfaceType);
                        if (goPossessingInterface == null || firstComponentWithInterface != goPossessingInterface)
                        {
                            EditorGUI.LabelField(position, label, new GUIContent("Game object does not possess interface of type " + interfaceType));
                            goPossessesAllInterfaces = false;
                            break;
                        }
                    }
                }

                if (goPossessesAllInterfaces)
                {
                    property.objectReferenceValue = go.GetComponent(requiredAttribute.InterfaceTypes[0]);
                }
            }
            EditorGUI.EndProperty();
        }
        else
        {
            // If field is not reference, show error message.
            var previousColor = GUI.color;

            GUI.color = Color.red;
            EditorGUI.LabelField(position, label, new GUIContent("Property is not a reference type"));
            GUI.color = previousColor;
        }
    }
}
