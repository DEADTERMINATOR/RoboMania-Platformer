﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(MovingPlatformPath.Waypoint))]
public class WaypointPropertyDrawer : PropertyDrawer
{
    private const float WHITE_SPACE_BETWEEN_FIELDS = 2.5f;
    private const float WHITE_SPACE_BETWEEN_INSTANCES = 15f;


    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 2;

        var positionProp = property.FindPropertyRelative("Position");
        var transferToBasePathProp = property.FindPropertyRelative("CanTransferToBasePath");
        var pathToTransferToProp = property.FindPropertyRelative("EditorComponentPathToTransferTo");

        positionProp.vector3Value = EditorGUI.Vector3Field(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight + WHITE_SPACE_BETWEEN_FIELDS), new GUIContent("Position"), positionProp.vector3Value);
        transferToBasePathProp.boolValue = EditorGUI.Toggle(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight + WHITE_SPACE_BETWEEN_FIELDS, position.width, EditorGUIUtility.singleLineHeight + WHITE_SPACE_BETWEEN_FIELDS), new GUIContent("Can Transfer to Platform's Base Path"), transferToBasePathProp.boolValue);

        if (!transferToBasePathProp.boolValue)
        {
            pathToTransferToProp.objectReferenceValue = EditorGUI.ObjectField(new Rect(position.x, position.y + (EditorGUIUtility.singleLineHeight + WHITE_SPACE_BETWEEN_FIELDS) * 2, position.width, EditorGUIUtility.singleLineHeight + WHITE_SPACE_BETWEEN_FIELDS / 2f), new GUIContent("Path to Transfer to"), pathToTransferToProp.objectReferenceValue, typeof(MovingPlatformPathComponent), true);
        }

        if (transferToBasePathProp.boolValue || pathToTransferToProp.objectReferenceValue != null)
        {
            float fieldOffset = 0;
            if (transferToBasePathProp.boolValue)
            {
                fieldOffset = (EditorGUIUtility.singleLineHeight + WHITE_SPACE_BETWEEN_FIELDS) * 2f;
            }
            else
            {
                fieldOffset = (EditorGUIUtility.singleLineHeight + WHITE_SPACE_BETWEEN_FIELDS) * 3f - WHITE_SPACE_BETWEEN_FIELDS / 2f;
            }

            var waypointIndexProp = property.FindPropertyRelative("_waypointIndexOnOtherPath");
            waypointIndexProp.intValue = EditorGUI.IntField(new Rect(position.x, position.y + fieldOffset, position.width, EditorGUIUtility.singleLineHeight + WHITE_SPACE_BETWEEN_FIELDS / 2f), new GUIContent("Waypoint Index on Other Path to Transfer to"), waypointIndexProp.intValue);
        }

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        var transferToBasePathProp = property.FindPropertyRelative("CanTransferToBasePath");
        var pathToTransferToProp = property.FindPropertyRelative("EditorComponentPathToTransferTo");
        var waypointIndexProp = property.FindPropertyRelative("_waypointIndexOnOtherPath");

        if (pathToTransferToProp.objectReferenceValue != null)
        {
            //We subtract at the end here to account for the fact that the object field is only given half the field white space.
            return (EditorGUIUtility.singleLineHeight + WHITE_SPACE_BETWEEN_FIELDS) * 4f + WHITE_SPACE_BETWEEN_INSTANCES - (WHITE_SPACE_BETWEEN_FIELDS / 2f) * 2f;
        }
        else
        {
            if (transferToBasePathProp.boolValue)
            {
                return (EditorGUIUtility.singleLineHeight + WHITE_SPACE_BETWEEN_FIELDS) * 3f + WHITE_SPACE_BETWEEN_INSTANCES - (WHITE_SPACE_BETWEEN_FIELDS / 2f);
            }
            else
            {
                return (EditorGUIUtility.singleLineHeight + WHITE_SPACE_BETWEEN_FIELDS) * 3f + WHITE_SPACE_BETWEEN_INSTANCES;
            }
        }
    }
}
