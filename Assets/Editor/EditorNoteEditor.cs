﻿using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(EditorNote))]
public class EditorNoteEditor : Editor
{
    SerializedProperty ResponseNote;
    SerializedProperty Note;
    SerializedProperty WrittenBy;
    protected void OnEnable()
    {

        Note = serializedObject.FindProperty("note");
        ResponseNote = serializedObject.FindProperty("ResponseNote");
        WrittenBy = serializedObject.FindProperty("WrittenBy");

    }

    public override void OnInspectorGUI()
    {

        EditorGUILayout.LabelField("Note From:");
        WrittenBy.stringValue = EditorGUILayout.TextArea(WrittenBy.stringValue);
        EditorGUILayout.LabelField("Note:");
        Note.stringValue =  EditorGUILayout.TextArea(Note.stringValue, GUILayout.Height(150));
        EditorGUILayout.LabelField("Response Note:");
        ResponseNote.stringValue = EditorGUILayout.TextArea(ResponseNote.stringValue, GUILayout.Height(100));
        serializedObject.ApplyModifiedProperties();
        serializedObject.Update();

    }
}