﻿using System;
using UnityEngine;

    public class MemProfiler : MonoBehaviour
    {

        private void LateUpdate()
        {
            Debug.Log(System.GC.GetTotalMemory(false));
        }

    }
