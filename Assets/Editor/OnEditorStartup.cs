﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEditor;
using GameMaster;

[InitializeOnLoad]
public class OnEditorStartup
{
    static OnEditorStartup()
    {
        GameMasterEditor.StartScene();
        InputSystem.RegisterInteraction<ControlStickPerformSingleAction>();
        InputSystem.RegisterInteraction<HoldToPerformMultipleActions>();
    }
}
