﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class WaypointsEditorTool : EditorWindow
{
    public static Object wayPointCompSource { get; private set; }
    public static Object activeAreasource { get; private set; }
    public static bool isSimple = true;
    public static SimpleWaypointsEditor SimpleWaypointsEditor = new SimpleWaypointsEditor();
    static EditableWayPoints editableWayPoints;
    static WayPointsComponent wayPointsComp;
    static ComplexWayPoints complexWayPoints;
    public static ComplexWaypointsEditor complexWaypointsEditor = new ComplexWaypointsEditor();


    private static List<EditWayPointGameObject> wayPointsEdit;

    static GameObject EditPoints;
    private int AreaPaddingX;
    private int AreaPaddingY;

    [MenuItem("GameObject/WayPoint Paths/Add Point Before &a", false, 10)]
    static void AddPoint(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }
        if (isSimple)
        {
            EditWayPointGameObject point = go.GetComponent<EditWayPointGameObject>();
            if (point == null)
            {
                Debug.LogError("Could not Delete Point");
                return; ///noting to work with
            }
            SimpleWaypointsEditor.AddPointBefor(point);
        }
        else
        {
            EditComplexWayPointGameObject point = go.GetComponent<EditComplexWayPointGameObject>();
            if (point == null)
            {
                Debug.LogError("Could not Delete Point");
                return; ///noting to work with
            }
            complexWaypointsEditor.AddPointBefor(point);
        }
    }


    [MenuItem("GameObject/WayPoint Paths/Remove Point &r", false, 10)]
    static void RemovePoint(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }

        if (isSimple)
        {
            EditWayPointGameObject point = go.GetComponent<EditWayPointGameObject>();
            if (point == null)
            {
                Debug.LogError("Could not Delet Point");
                return; ///noting to work with
            }
            SimpleWaypointsEditor.RemovePoint(point);
        }
        else
        {
            EditComplexWayPointGameObject point = go.GetComponent<EditComplexWayPointGameObject>();
            if (point == null)
            {
                Debug.LogError("Could not Delet Point");
                return; ///noting to work with
            }
            complexWaypointsEditor.RemovePoint(point);
        }
    }


    [MenuItem("GameObject/WayPoint Paths/Add Point After &b", false, 10)]
    static void AddPointAfter(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }
        if (isSimple)
        {
            EditWayPointGameObject point = go.GetComponent<EditWayPointGameObject>();
            if (point == null)
            {
                Debug.LogError("Selected Point was Null");
                return; ///noting to work with
            }
            SimpleWaypointsEditor.AddPointAfter(point);
        }
        else
        {
            EditComplexWayPointGameObject point = go.GetComponent<EditComplexWayPointGameObject>();
            if (point == null)
            {
                Debug.LogError("Selected Point was Null");
                return; ///noting to work with
            }
            complexWaypointsEditor.AddPointAfter(point);
        }

    }

    [MenuItem("GameObject/WayPoint Paths/Convert Connection &c", false, 10)]
    static void ConnectionConvert(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }

        if (isSimple)
        {
            Debug.LogError("Can not convert connections on non complex waypoint systems");
        }
        else
        {
            EditComplexWayPointGameObject point = go.GetComponent<EditComplexWayPointGameObject>();
            if (point == null || !point.IsConnectionPoint)
            {
                Debug.LogError("Invalid connection Point");
                return; ///noting to work with
            }
            complexWaypointsEditor.ConvertConnection(point);
        }
    }

    [MenuItem("GameObject/WayPoint Paths/Add Area &n", false, 10)]
    static void AddArea(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }

        if (isSimple)
        {
            Debug.LogError("Can not add area to non complex waypoint systems");
        }
        else
        {
            EditComplexWayPointGameObject point = go.GetComponent<EditComplexWayPointGameObject>();
            if (point == null)
            {
                Debug.LogError("Selected point is null");
                return; ///noting to work with
            }
            complexWaypointsEditor.AddAreaToEnd(point);
        }
    }


    [MenuItem("GameObject/WayPoint Paths/Set Area loops &n", false, 10)]
    static void SetLoops(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }

        if (isSimple)
        {
            Debug.LogError("Can not add area to non complex waypoint systems");
        }
        else
        {
            EditComplexWayPointGameObject point = go.GetComponent<EditComplexWayPointGameObject>();
            if (point == null)
            {
                Debug.LogError("Selected point is null");
                return; ///noting to work with
            }
            complexWaypointsEditor.SetAreaLoops(point);

        }
    }


    [MenuItem("GameObject/WayPoint Paths/Make Default Path &m", false, 10)]
    static void MakeDefaultPath(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }

        if (isSimple)
        {
            SimpleWaypointsEditor.MakeDefultPoints();
        }
        else
        {

            complexWaypointsEditor.MakeDefultPoints();
        }
    }


    [MenuItem("Path Tools/WayPoint Paths/Open Editor ")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(WaypointsEditorTool));
    }
    #region GUI
    void OnGUI()
    {
        // EditorGUILayout.BeginHorizontal();
        // The actual window code goes here
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        if (!SimpleWaypointsEditor.IsEditing)
            wayPointCompSource = EditorGUILayout.ObjectField(wayPointCompSource, typeof(EditableWayPoints), true);

        if (wayPointCompSource is ComplexWayPoints)
        {
            complexWayPoints = (ComplexWayPoints)wayPointCompSource;
            complexWaypointsEditor.ComplexWaypoints = complexWayPoints;
            isSimple = false;
        }
        else
        {
            wayPointsComp = (WayPointsComponent)wayPointCompSource;
            SimpleWaypointsEditor.wayPointsComp = wayPointsComp;
            isSimple = true;
        }
        string ButtonText = "Start Editing WayPoints";



        if (isSimple)
        {
            if (wayPointCompSource != null)
            {
                if (SimpleWaypointsEditor.IsEditing)
                {
                    ButtonText = "Stop Editing WayPoints";
                }

                if (GUILayout.Button(ButtonText))
                {
                    if (!SimpleWaypointsEditor.IsEditing)
                    {
                        SimpleWaypointsEditor.StartEditing();
                    }
                    else
                    {
                        SimpleWaypointsEditor.StopEditing();

                    }

                    SimpleWaypointsEditor.IsEditing = !SimpleWaypointsEditor.IsEditing;
                }
            }
            GUILayout.Label("Active Area", EditorStyles.boldLabel);
            activeAreasource = EditorGUILayout.ObjectField(activeAreasource, typeof(BoxCollider2D), true);
            if (activeAreasource != null && activeAreasource is BoxCollider2D)
            {
                SimpleWaypointsEditor.activeAreasource = activeAreasource;
                GUILayout.Label("Active Area Padding", EditorStyles.boldLabel);
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label("X", EditorStyles.boldLabel);
                SimpleWaypointsEditor.AreaPaddingX = EditorGUILayout.IntField(SimpleWaypointsEditor.AreaPaddingX);
                GUILayout.Label("Y", EditorStyles.boldLabel);
                SimpleWaypointsEditor.AreaPaddingY = EditorGUILayout.IntField(SimpleWaypointsEditor.AreaPaddingY);
                EditorGUILayout.EndHorizontal();
            }
        }
        else
        {
            if (complexWaypointsEditor.IsEditing)
            {
                ButtonText = "Stop Editing WayPoints";
            }
            if (GUILayout.Button(ButtonText))
            {
                if (!complexWaypointsEditor.IsEditing)
                {

                    complexWaypointsEditor.StartEditing();


                }
                else
                {
                    complexWaypointsEditor.StopEditing();
                }

            }
            GUILayout.Label("New Area Setting", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Offset", EditorStyles.boldLabel);
            complexWaypointsEditor.NewAreaOffset = EditorGUILayout.IntField(complexWaypointsEditor.NewAreaOffset);
            GUILayout.Label("Number Of areas to make", EditorStyles.boldLabel);
            complexWaypointsEditor.NumberOfNewAreasToAdd = EditorGUILayout.IntField(complexWaypointsEditor.NumberOfNewAreasToAdd);
            EditorGUILayout.EndHorizontal();
        }
    }

    private static void FixNames(List<EditWayPointGameObject> PatrolPoints)
    {
        int pId = 0;
        foreach (EditWayPointGameObject point in PatrolPoints)
        {
            point.name = ("Waypoint " + pId);
            ((EditWayPointGameObject)point.GetComponent<EditWayPointGameObject>()).Index = pId;
            point.transform.SetSiblingIndex(pId);
            pId++;
        }
    }
    void Update()
    {
        if (isSimple)
        {
            SimpleWaypointsEditor.Update();
        }
        else
        {
            complexWaypointsEditor.Update();
        }
    }


    #endregion
}
