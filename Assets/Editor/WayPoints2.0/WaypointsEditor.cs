﻿using UnityEngine;
using System.Collections;

public abstract class WaypointsEditor 
{
    protected GameObject EditPoints = null;
    public bool IsEditing;
    public abstract void Update();
    public abstract void StartEditing();
    public abstract void StopEditing();
    public abstract void AddPointAfter(EditWayPointGameObject point);
    public abstract void AddPointBefor(EditWayPointGameObject point);
    public abstract void RemovePoint(EditWayPointGameObject point);
    public abstract void MakeDefultPoints();
}
