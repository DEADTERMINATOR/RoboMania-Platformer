﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;


public class ComplexWaypointsEditor : WaypointsEditor
{
    public ComplexWayPoints ComplexWaypoints;
    private List<ComplexWayPointEdit> complexWaypointsEditOnjects;
    public int NewAreaOffset = 15;
    public int NumberOfNewAreasToAdd = 1;
   

    public void SetAreaLoops(EditComplexWayPointGameObject spoint)
    {

        IntInputPopup.Showpopup("How Many loops", (value) => { ComplexWaypoints.Areas[spoint.OwningArea].PatrolPoints.NumberOfLoops = value;  return 0; }, ComplexWaypoints.Areas[spoint.OwningArea].PatrolPoints.NumberOfLoops);

    }


    public void AddAreaToEnd(EditComplexWayPointGameObject spoint, int Xoffset = 15)
    {

        Xoffset = NewAreaOffset;
        for (int i = 1; i <= NumberOfNewAreasToAdd; i++)
        {
            Vector3 newPointBase = spoint.transform.position + Vector3.right * (Xoffset*i);
            ComplexWayPointArea newArea = new ComplexWayPointArea();
            newArea.PatrolPoints = new WayPoints();
            newArea.PatrolPoints.Add(newPointBase + Vector3.up * 2);
            newArea.PatrolPoints.Add(newPointBase + Vector3.left * 2);
            newArea.PatrolPoints.Add(newPointBase + Vector3.right * 2);
            //newArea.PathToNextPoints = new WayPoints();
            //newArea.PathToNextPoints.Add(newPointBase);
            ComplexWaypoints.AddArea(newArea);
        }
        StopEditing();
        StartEditing();
    }


    public override void AddPointAfter(EditWayPointGameObject spoint)
    {
        EditComplexWayPointGameObject point = (EditComplexWayPointGameObject)spoint;
        if (point.IsPartrolPoint)
        {
            AddPartollPointAfter(point);

        }
        else
        {
            if (point.IsConnectionPoint)
            {
                AddConnectionPointAfter(point);
            }
        }
    }

    public override void AddPointBefor(EditWayPointGameObject spoint)
    {
        EditComplexWayPointGameObject point = (EditComplexWayPointGameObject)spoint;
        if (point.IsPartrolPoint)
        {
            AddPatrolPointBefor(point);
        }
        else
        {
            if (point.IsConnectionPoint)
            {
                AddConnectionPointBefor(point);
            }
            // AddControllPointBefor(point);
        }
    }

    public override void RemovePoint(EditWayPointGameObject spoint)
    {
        EditComplexWayPointGameObject point = (EditComplexWayPointGameObject)spoint;
        GameObject go = point.gameObject;
        if (point.IsPartrolPoint)
        {
            ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points.RemoveAt(point.Index);
            GameObject.DestroyImmediate(go);
            complexWaypointsEditOnjects[point.OwningArea].PatrolPoints.RemoveAt(point.Index);
            FixNames(complexWaypointsEditOnjects[point.OwningArea].PatrolPoints);
        }
        else
        {
            ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path.Points.RemoveAt(point.Index);
            GameObject.DestroyImmediate(go);
            complexWaypointsEditOnjects[point.OwningArea].ConnectionPathsPoints[point.ConectedAreaID].RemoveAt(point.Index);
            FixConnectionNames(complexWaypointsEditOnjects[point.OwningArea].ConnectionPathsPoints[point.ConectedAreaID]);
        }
    }

    public override void StartEditing()
    {

        if (ComplexWaypoints == null || ComplexWaypoints.Areas == null || ComplexWaypoints.Areas.Count == 0)
        {
            if (EditorUtility.DisplayDialog("Empty Waypoints", "This WayPoint Sytem is empty would you like to add defult points", "Yes and Edit", "No and Stop Edit"))
            {
                
                MakeDefultPoints();
            }
            else
            {
                Debug.LogError("Can not Work on Empty WayPoint systems");
                return;
            }
        }
        complexWaypointsEditOnjects = new List<ComplexWayPointEdit>();
        EditPoints = new GameObject("WayPoint Edoitor Points");
        EditPoints.transform.parent = ComplexWaypoints.transform;
        int i = 0;
        if (ComplexWaypoints != null && ComplexWaypoints.Areas.Count >0)
        {

            foreach (ComplexWayPointArea area in ComplexWaypoints.Areas)
            {
                ComplexWayPointEdit editOBJ = new ComplexWayPointEdit();
                GameObject Areas = new GameObject("Area " + i);
                Areas.transform.parent = EditPoints.transform;
                GameObject PatrolPoints = new GameObject("Patrol Points");
                PatrolPoints.transform.parent = Areas.transform;
                Areas.transform.position = area.PatrolPoints.Points[0];
                editOBJ.AreaGameOBJ = Areas;
                editOBJ.PointsGameOBJ = PatrolPoints;
                int pId = 0;
                foreach (Vector3 point in area.PatrolPoints.Points)
                {
                    GameObject editPoint = new GameObject("Patrol Point " + pId);
                    EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
                    pathEditPoint.OwningArea = i;
                    pathEditPoint.Index = pId;
                    editPoint.transform.position = point;
                    editPoint.transform.parent = PatrolPoints.transform;
                    pathEditPoint.IsPartrolPoint = true;
                    pId++;
                    editOBJ.PatrolPoints.Add(editPoint);
                }

                GameObject PathToNexts = new GameObject("Conections");
                //editOBJ.ConnectionGameOBJ = PathToNexts;
                PathToNexts.transform.parent = Areas.transform;
                int id = 0;
                if (area.ComplexWaypointConnections != null)
                {
                    foreach (ComplexWaypointConnection conecctions in area.ComplexWaypointConnections)
                    {
                        if (conecctions != null && !conecctions.IsDummyNode)
                        {
                            GameObject connectionOBJ = new GameObject("Conection to" + id);
                            connectionOBJ.transform.parent = PathToNexts.transform;
                            int ccId = 0;
                            editOBJ.ConnectionPathsPoints.Add(id, new List<GameObject>());
                            editOBJ.ConnectionPaths.Add(id, connectionOBJ);
                            foreach (Vector3 point in conecctions.Path)
                            {
                                GameObject editPoint = new GameObject("Conection Point" + ccId);
                                EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
                                pathEditPoint.FillColour = Color.red;
                                pathEditPoint.OwningArea = i;
                                pathEditPoint.Index = ccId;
                                pathEditPoint.IsPartrolPoint = false;
                                pathEditPoint.IsConnectionPoint = true;
                                pathEditPoint.OwningArea = i;
                                pathEditPoint.ConectedAreaID = id;

                                editPoint.transform.position = point;
                                editPoint.transform.parent = connectionOBJ.transform;
                                ccId++;
                                editOBJ.ConnectionPathsPoints[id].Add(editPoint);
                            }
                        }
                        id++;
                    }

                }


                i++;
                complexWaypointsEditOnjects.Add(editOBJ);
            }
            //set the frst poin as selected
            Selection.activeObject = complexWaypointsEditOnjects[0].PatrolPoints[0];
            IsEditing = true;
        }
        else
        {
            Debug.Log("Complex waypoint system needs points");
        }
    }

    public override void StopEditing()
    {
        IsEditing = false;
        Object.DestroyImmediate(EditPoints);
        complexWaypointsEditOnjects = new List<ComplexWayPointEdit>();
        //ComplexWaypoints.FixConnections();
    }

    public override void Update()
    {
        if (!EditorApplication.isPlaying && !EditorApplication.isPaused)
        {
            if (IsEditing && complexWaypointsEditOnjects.Count > 0)
            {
                //Convert game object trans forms back to offsets
                int i = 0;
                foreach (ComplexWayPointEdit area in complexWaypointsEditOnjects)
                {
                    int pId = 0;
                    foreach (GameObject point in area.PatrolPoints)
                    {
                        ComplexWaypoints.Areas[i].PatrolPoints.Points[pId] = (Vector2)point.transform.position;

                        pId++;
                    }

                    int cId = 0;

                    for (int index = 0; index < area.ConnectionPathsPoints.Count; index++)
                    {
                        cId = 0;
                        if (area.ConnectionPathsPoints.ContainsKey(index))
                        {
                            foreach (GameObject point in area.ConnectionPathsPoints[index])
                            {
                                if (point != null)
                                {
                                    ComplexWaypoints.Areas[i].ComplexWaypointConnections[index].Path[cId] = (Vector2)point.transform.position;
                                }
                                cId++;
                            }
                        }
                    }
                    i++;
                }
            }
        }
    }
    private void FixNames(List<GameObject> PatrolPoints)
    {
        int pId = 0;
        foreach (GameObject point in PatrolPoints)
        {
            point.name = ("Patrol Point " + pId);
            ((EditComplexWayPointGameObject)point.GetComponent<EditComplexWayPointGameObject>()).Index = pId;
            point.transform.SetSiblingIndex(pId);
            pId++;
        }
    }
    private void FixConnectionNames(List<GameObject> Points)
    {
        int pId = 0;
        foreach (GameObject point in Points)
        {
            point.name = ("Conection Points " + pId);
            ((EditComplexWayPointGameObject)point.GetComponent<EditComplexWayPointGameObject>()).Index = pId;
            point.transform.SetSiblingIndex(pId);
            pId++;
        }
    }
    /// <summary>
    /// Convert a connction from 2 way to one way or revers
    /// </summary>
    /// <param name="point"></param>
    public void ConvertConnection(EditComplexWayPointGameObject point)
    {
        //Make sure this is a conection point of a complex wat point system
        if (!point.IsConnectionPoint)
        {
            Debug.LogError("No Connection point selected");
        }
        //if the point is in a state of single connection IE one each way 
        if (ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].IsOneDirection)
        {
            //mark this path as a tow direction path 
            ComplexWaypoints.Areas[point.ConectedAreaID].ComplexWaypointConnections[point.OwningArea].IsOneDirection  = false;
            //se the not to marked as a dummy node
            ComplexWaypoints.Areas[point.ConectedAreaID].ComplexWaypointConnections[point.OwningArea].IsDummyNode = true;
            // remove the path from the connected area
            ComplexWaypoints.Areas[point.ConectedAreaID].ComplexWaypointConnections[point.OwningArea].Path = null;
            //set the ref to the current path
            ComplexWaypoints.Areas[point.ConectedAreaID].ComplexWaypointConnections[point.OwningArea].actualyNode = ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID];
            // make the other path a two directional
            ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].IsOneDirection = false;
        }
        else
        {
            //mark this path as a one direction path 
            ComplexWaypoints.Areas[point.ConectedAreaID].ComplexWaypointConnections[point.OwningArea].IsOneDirection = true;
            // remove the path from the connected area 
            ComplexWaypoints.Areas[point.ConectedAreaID].ComplexWaypointConnections[point.OwningArea].IsDummyNode = false;
            //add the points in revers ored since the pathe is in the other dircetion
            for (int i = ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path.Count - 1; i > 0; i--)
            {
                Vector3 pathPoint = ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path[i];
                ComplexWaypoints.Areas[point.ConectedAreaID].ComplexWaypointConnections[point.OwningArea].Path.Add(pathPoint + Vector3.up);
            }

            ComplexWaypoints.Areas[point.ConectedAreaID].ComplexWaypointConnections[point.OwningArea].actualyNode = null;
            //set the ref to the current path
            ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].IsOneDirection = true;
        }
        //force the edit to stop()
        StopEditing();
        StartEditing();

    }


    /// <summary>
    /// Add A point to a coneection path afther the selcted point
    /// </summary>
    /// <param name="point"></param>
    private void AddConnectionPointAfter(EditComplexWayPointGameObject point)
    {
        Vector3 PointA, PointB, newPointPos = Vector3.zero;
        //is the point not the last in the arry
        if (point.Index > 0 && point.Index < ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path.Count - 1)//Not on the End of the array
        {
            PointA = ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path[point.Index];
            PointB = ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path[point.Index + 1];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        else if (point.Index == ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path.Count - 1)//is last index
        {
            // use the connection patroll area as the second point since this is the end of the array
            PointA = ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path.Last;
            PointB = ComplexWaypoints.Areas[point.ConectedAreaID].PatrolPoints.First;

            newPointPos = Vector3.Lerp(PointA, PointB, 0.5f);
        }
        else
        {
            Debug.LogError("Could not Detrimin point placment");
            return;
        }
        //if the point is still all zers then an error has happned
        if (newPointPos == Vector3.zero)
        {
            Debug.LogError("Could not make new point");
            return;
        }

        ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path.Insert(point.Index, newPointPos);


        GameObject editPoint = new GameObject("Patrol Point " + (point.Index));
        EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
        pathEditPoint.OwningArea = point.OwningArea;
        pathEditPoint.Index = point.Index + 1;
        pathEditPoint.FillColour = Color.red;
        editPoint.transform.position = newPointPos;
        editPoint.transform.parent = complexWaypointsEditOnjects[point.OwningArea].ConnectionPaths[point.ConectedAreaID].transform;
        //set the data of the edit point 
        pathEditPoint.IsPartrolPoint = false;
        pathEditPoint.IsConnectionPoint = true;
        pathEditPoint.OwningArea = point.OwningArea;
        pathEditPoint.ConectedAreaID = point.ConectedAreaID;
        complexWaypointsEditOnjects[point.OwningArea].ConnectionPathsPoints[point.ConectedAreaID].Insert(point.Index + 1, editPoint);
        FixConnectionNames(complexWaypointsEditOnjects[point.OwningArea].ConnectionPathsPoints[point.ConectedAreaID]);
        Selection.activeObject = editPoint;

    }

    private void AddConnectionPointBefor(EditComplexWayPointGameObject point)
    {
        Vector3 PointA, PointB, newPointPos = Vector3.zero;
        if (point.Index > 0 && point.Index < ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path.Count)//Not on the End of the array
        {
            PointA = ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path[point.Index - 1];
            PointB = ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path[point.Index];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        else if (point.Index == 0)//is first index
        {

            PointA = ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path.First;
            PointB = ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Last;

            newPointPos = Vector3.Lerp(PointA, PointB, 0.5f);
        }
        if (newPointPos == Vector3.zero)
        {
            Debug.LogError("Could not make new point");
            return;
        }

        ComplexWaypoints.Areas[point.OwningArea].ComplexWaypointConnections[point.ConectedAreaID].Path.Insert(point.Index, newPointPos);


        GameObject editPoint = new GameObject("Patrol Point " + (point.Index));
        EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
        pathEditPoint.OwningArea = point.OwningArea;
        if (point.Index > 0)
        {
            pathEditPoint.Index = point.Index - 1;
        }
        else
        {
            pathEditPoint.Index = 0;
        }
        pathEditPoint.FillColour = Color.red;
        editPoint.transform.position = newPointPos;
        editPoint.transform.parent = complexWaypointsEditOnjects[point.OwningArea].ConnectionPaths[point.ConectedAreaID].transform;
        pathEditPoint.IsPartrolPoint = false;
        pathEditPoint.IsConnectionPoint = true;
        pathEditPoint.OwningArea = point.OwningArea;
        pathEditPoint.ConectedAreaID = point.ConectedAreaID;

        complexWaypointsEditOnjects[point.OwningArea].ConnectionPathsPoints[point.ConectedAreaID].Insert(point.Index, editPoint);
        FixConnectionNames(complexWaypointsEditOnjects[point.OwningArea].ConnectionPathsPoints[point.ConectedAreaID]);
        Selection.activeObject = editPoint;
    }

    private void AddPartollPointAfter(EditComplexWayPointGameObject point)
    {
        Vector3 PointA, PointB, newPointPos = Vector3.zero;
        if (point.Index >= 0 && point.Index < ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points.Count - 1)//Not on the End of the array
        {
            PointA = ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points[point.Index];
            PointB = ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points[point.Index + 1];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        else if (point.Index == ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points.Count - 1)//is last index
        {
            PointA = ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points[point.Index];
            PointB = ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points[0];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        if (newPointPos == Vector3.zero)
        {
            Debug.LogError("Could not make new point");
            return;
        }

        ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points.Insert(point.Index + 1, newPointPos);


        GameObject editPoint = new GameObject("Patrol Point " + (point.Index));
        EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
        pathEditPoint.OwningArea = point.OwningArea;
        pathEditPoint.Index = point.Index - 1;
        editPoint.transform.position = newPointPos;
        editPoint.transform.parent = complexWaypointsEditOnjects[point.OwningArea].PointsGameOBJ.transform;
        pathEditPoint.IsPartrolPoint = true;
        complexWaypointsEditOnjects[point.OwningArea].PatrolPoints.Insert(point.Index + 1, editPoint);
        FixNames(complexWaypointsEditOnjects[point.OwningArea].PatrolPoints);
        Selection.activeObject = editPoint;
    }



    private void AddPatrolPointBefor(EditComplexWayPointGameObject point)
    {
        Vector3 PointA, PointB, newPointPos = Vector3.zero;
        if (point.Index > 0 && point.Index < ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points.Count)//Not on the End of the array
        {
            PointA = ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points[point.Index - 1];
            PointB = ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points[point.Index];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        else if (point.Index == 0)//is last index
        {
            PointA = ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points[0];
            PointB = ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points[point.Index];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);

        }
        else
        {
            Debug.LogError("Could not Detrimin point placment");
            return;
        }
        if (newPointPos == Vector3.zero)
        {
            Debug.LogError("Could not make new point");
            return;
        }

        ComplexWaypoints.Areas[point.OwningArea].PatrolPoints.Points.Insert(point.Index, newPointPos);


        GameObject editPoint = new GameObject("Patrol Point " + (point.Index));
        EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
        pathEditPoint.OwningArea = point.OwningArea;
        pathEditPoint.Index = point.Index - 1;
        editPoint.transform.position = newPointPos;
        editPoint.transform.parent = complexWaypointsEditOnjects[point.OwningArea].PointsGameOBJ.transform;
        pathEditPoint.IsPartrolPoint = true;
        complexWaypointsEditOnjects[point.OwningArea].PatrolPoints.Insert(point.Index, editPoint);
        FixNames(complexWaypointsEditOnjects[point.OwningArea].PatrolPoints);
        Selection.activeObject = editPoint;
    }

    public override void MakeDefultPoints()
    {
        ComplexWaypoints.Areas = new List<ComplexWayPointArea>();
        ComplexWayPointArea complexWayPoint = new ComplexWayPointArea();
        complexWayPoint.PatrolPoints.Add(ComplexWaypoints.transform.position + Vector3.up * 2);
        complexWayPoint.PatrolPoints.Add(ComplexWaypoints.transform.position + Vector3.left * 2);
        complexWayPoint.PatrolPoints.Add(ComplexWaypoints.transform.position + Vector3.right * 2);
        complexWayPoint.PatrolPoints.Add(ComplexWaypoints.transform.position + Vector3.down * 2);
        ComplexWaypoints.AddArea(complexWayPoint);
        //complexWayPoint.PathToNextPoints.Add(ComplexWaypoints.transform.position);
    }
}
public class IntInputPopup : EditorWindow
{
    public string DisplayText;
    public int InputText;
    System.Func< int,int> _callback;
   public static void Showpopup(string text, System.Func<int,int> callback,int defultValue = 0)
    {
        IntInputPopup window = ScriptableObject.CreateInstance<IntInputPopup>();
        window.InputText = defultValue;
        window._callback = callback;
        window.DisplayText = text;
        window.position = new Rect(Screen.width / 2, Screen.height / 2, 250, 150);
        window.ShowPopup();
    }
   
    void OnGUI()
    {
        EditorGUILayout.LabelField(DisplayText, EditorStyles.wordWrappedLabel);
        GUILayout.Space(1);
        InputText = EditorGUILayout.IntField(InputText);
        if (GUILayout.Button("Ok"))
        {
            _callback(InputText);
            this.Close();
        }
        if (GUILayout.Button("Cancel")) this.Close();
    }
}