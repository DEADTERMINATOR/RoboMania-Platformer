﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class SimpleWaypointsEditor : WaypointsEditor
{
    private List<EditWayPointGameObject> wayPointsEdit;
    public WayPointsComponent wayPointsComp;
    public object activeAreasource;
    public int AreaPaddingX = 2;
    public int AreaPaddingY = 2;

    public override void Update()
    {
        Vector3 min = new Vector3(float.MaxValue, float.MaxValue);
        Vector3 max = Vector3.zero;
        if (!EditorApplication.isPlaying && !EditorApplication.isPaused)
        {
            if (IsEditing && wayPointsEdit != null && wayPointsEdit.Count > 0)
            {
                //Convert game object trans forms back to offsets
                int i = 0;
                foreach (EditWayPointGameObject point in wayPointsEdit)
                {

                    wayPointsComp.WayPoints[i] = (Vector2)point.transform.position;
                    if (point.transform.position.x < min.x)
                    {
                        min.x = point.transform.position.x;
                    }
                    if (point.transform.position.y < min.y)
                    {
                        min.y = point.transform.position.y;
                    }
                    if (point.transform.position.x > max.x)
                    {
                        max.x = point.transform.position.x;
                    }
                    if (point.transform.position.y > max.y)
                    {
                        max.y = point.transform.position.y;
                    }

                    i++;
                }
                //Debug.DrawLine((min ), (max ), Color.red, 1);
                if (activeAreasource != null && activeAreasource is BoxCollider2D)
                {
                    ((BoxCollider2D)activeAreasource).size = new Vector2((max.x - min.x) + AreaPaddingX, (max.y - min.y) + AreaPaddingY);
                    ((BoxCollider2D)activeAreasource).offset = Vector2.Lerp(min, max, 0.5f)-(Vector2)wayPointsComp.transform.position; 
                }
            }
        }
    }

    public override void AddPointAfter(EditWayPointGameObject point)
    {
        Vector3 PointA, PointB, newPointPos = Vector3.zero;
        if (point.Index >= 0 && point.Index < wayPointsComp.WayPoints.Count - 1)//Not on the End of the array
        {
            PointA = wayPointsComp.WayPoints[point.Index + 1];
            PointB = wayPointsComp.WayPoints[point.Index];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        else

        {
            if (wayPointsComp.WayPoints.IsLooping)
            {
                PointA = wayPointsComp.WayPoints[0];
                PointB = wayPointsComp.WayPoints[point.Index];

                newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
            }
            else
            {
                newPointPos = wayPointsComp.WayPoints[point.Index] + (Vector3.right * 3);
            }
        }

        wayPointsComp.WayPoints.Insert(point.Index + 1, newPointPos);


        GameObject editPoint = new GameObject("WayPoint " + (point.Index));
        EditWayPointGameObject pathEditPoint = editPoint.AddComponent<EditWayPointGameObject>();
        pathEditPoint.Index = point.Index - 1;
        editPoint.transform.position = newPointPos ;
        editPoint.transform.parent = EditPoints.transform;

        wayPointsEdit.Insert(point.Index + 1, pathEditPoint);
        FixNames(wayPointsEdit);
        Selection.activeObject = editPoint;
    }
    private void FixNames(List<EditWayPointGameObject> PatrolPoints)
    {
        int pId = 0;
        foreach (EditWayPointGameObject point in PatrolPoints)
        {
            point.name = ("Waypoint " + pId);
            ((EditWayPointGameObject)point.GetComponent<EditWayPointGameObject>()).Index = pId;
            point.transform.SetSiblingIndex(pId);
            pId++;
        }
    }
    public override void AddPointBefor(EditWayPointGameObject point)
    {
        Vector3 PointA, PointB, newPointPos = Vector3.zero;
        if (point.Index > 0 && point.Index < wayPointsComp.WayPoints.Count)//Not on the End of the array
        {
            PointA = wayPointsComp.WayPoints[point.Index - 1];
            PointB = wayPointsComp.WayPoints[point.Index];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        else

        {
            if (wayPointsComp.WayPoints.IsLooping)
            {
                PointA = wayPointsComp.WayPoints[0];
                PointB = wayPointsComp.WayPoints.Last;

                newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
            }
            else
            {
                newPointPos = wayPointsComp.WayPoints[0] + (Vector3.left * 3);
            }
        }
        wayPointsComp.WayPoints.Insert(point.Index, newPointPos);


        GameObject editPoint = new GameObject("WayPoint " + (point.Index));
        EditWayPointGameObject pathEditPoint = editPoint.AddComponent<EditWayPointGameObject>();
        pathEditPoint.Index = point.Index - 1;
        editPoint.transform.position = newPointPos ;
        editPoint.transform.parent = EditPoints.transform;

        wayPointsEdit.Insert(point.Index, pathEditPoint);
        FixNames(wayPointsEdit);
        Selection.activeObject = editPoint;
    }

    public override void StartEditing()
    {

        if (wayPointsComp.WayPoints == null || wayPointsComp.WayPoints.Count == 0)
        {
            if (EditorUtility.DisplayDialog("Empty Waypoints","This WayPoint Sytem is empty would you like to add defult points", "Yes and Edit", "No and Stop Edit"))
            {
                MakeDefultPoints();
            }
            else
            {
                Debug.LogError("Can not Work on Empty WayPoint systems");
                return;
            }
        }
        wayPointsEdit = new List<EditWayPointGameObject>();
        EditPoints = new GameObject("WayPoint Edoitor Points");
        EditPoints.transform.parent = wayPointsComp.transform;

        if (wayPointsComp.WayPoints != null && wayPointsComp.WayPoints.Count > 0)
        {
            int pId = 0;
            foreach (Vector3 point in wayPointsComp.WayPoints)
            {
                GameObject editPoint = new GameObject("Waypoint " + pId);
                EditWayPointGameObject pathEditPoint = editPoint.AddComponent<EditWayPointGameObject>();
                pathEditPoint.Index = pId;
                editPoint.transform.position = point;
                editPoint.transform.parent = EditPoints.transform;
                pId++;
                wayPointsEdit.Add(pathEditPoint);
            }
            Selection.activeObject = wayPointsEdit[0];
        }
        else
        {
            Debug.Log("A Simple Way point system is empty. use the mnue to add defult points");
        }
    }

    public override void StopEditing()
    {
        EditorUtility.SetDirty(wayPointsComp);
        AssetDatabase.SaveAssets();
        Object.DestroyImmediate(EditPoints);
        wayPointsEdit = new List<EditWayPointGameObject>();
    }

    public override void RemovePoint(EditWayPointGameObject point)
    {
        GameObject go = point.gameObject;
        wayPointsComp.WayPoints.Points.RemoveAt(point.Index);
        GameObject.DestroyImmediate(go);
        wayPointsEdit.RemoveAt(point.Index);
        FixNames(wayPointsEdit);
    }

    public override void MakeDefultPoints( )
    {
        wayPointsComp.WayPoints.Add(wayPointsComp.transform.position + Vector3.right * 3);
        wayPointsComp.WayPoints.Add(wayPointsComp.transform.position);
        wayPointsComp.WayPoints.Add(wayPointsComp.transform.position + Vector3.left * 3);
        Debug.Log("New Waypoint system made");
    }
}
