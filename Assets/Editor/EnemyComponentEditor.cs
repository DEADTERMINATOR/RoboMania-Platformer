﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Characters.Enemies;

[CustomEditor(typeof(Enemy))]
public class EnemyComponentEditor : Editor
{
    private SerializedProperty _maxHealthProp;
    private SerializedProperty _knockbackResistanceProp;
    private SerializedProperty _gunChipSpawnChanceProp;
    private SerializedProperty _shieldSpawnChanceProp;
    private SerializedProperty _minScrapValueProp;
    private SerializedProperty _maxScrapValueProp;
    private SerializedProperty _enemyLookDistanceProp;

    private void OnEnable()
    {
        _maxHealthProp = serializedObject.FindProperty("_maxHealth");
        _knockbackResistanceProp = serializedObject.FindProperty("_knockbackResistance");
        _gunChipSpawnChanceProp = serializedObject.FindProperty("_gunChipSpawnChance");
        _shieldSpawnChanceProp = serializedObject.FindProperty("_shieldSpawnChance");
        _minScrapValueProp = serializedObject.FindProperty("_minScrapValue");
        _maxScrapValueProp = serializedObject.FindProperty("_maxScrapValue");
        _enemyLookDistanceProp = serializedObject.FindProperty("_enemyLookDistance");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_maxHealthProp, new GUIContent("Max Health"));
        EditorGUILayout.PropertyField(_knockbackResistanceProp, new GUIContent("Knockback Resistance"));
        EditorGUILayout.PropertyField(_gunChipSpawnChanceProp, new GUIContent("Gun Chip Spawn Chance"));
        EditorGUILayout.PropertyField(_shieldSpawnChanceProp, new GUIContent("Shield Spawn Chance"));
        EditorGUILayout.PropertyField(_minScrapValueProp, new GUIContent("Minimum Scrap Value"));
        EditorGUILayout.PropertyField(_maxScrapValueProp, new GUIContent("Maximum Scrap Value"));
        EditorGUILayout.PropertyField(_enemyLookDistanceProp, new GUIContent("Max Look Distance for Other Enemies"));

        serializedObject.ApplyModifiedProperties();
    }
}
