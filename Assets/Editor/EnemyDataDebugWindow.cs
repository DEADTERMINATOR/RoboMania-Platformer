﻿using UnityEngine;
using UnityEditor;
using Characters.Enemies;
using Characters.Enemies.Bosses.Handy;

public class EnemyDataDebugWindow : EditorWindow
{
    public static Object activeAreasource { get; private set; }

    [MenuItem("Tools/Debug/AI ")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(EnemyDataDebugWindow));
    }

    void OnGUI()
    {
        GUILayout.Label("Active Area", EditorStyles.boldLabel);
        activeAreasource = EditorGUILayout.ObjectField(activeAreasource, typeof(Enemy), true);
        Enemy theRef = (Enemy)activeAreasource;
        if (theRef != null)
        {
            if (theRef.BehaviourStateMachine != null)
            {
                GUILayout.Label(theRef.BehaviourStateMachine.DebugInfo());
            }
            else
            {
                GUILayout.Label("No State Machine");
            }
            if(theRef is HandyBoss)
            {
                HandyBoss handyMini = (HandyBoss)theRef;

                //GUILayout.TextArea(handyMini.DebugState());
            }
        }
        else
        {
            GUILayout.Label("Cast Error or Missing");
        }
    }

    private void Update()
    {
        
    }
}