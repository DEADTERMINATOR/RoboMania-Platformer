﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelStartData), true)]
public class LevelStartDataEditor : Editor
{
    protected SerializedProperty _fileToSaveAndLoadFromProp;
    protected SerializedProperty _startAtSpecifiedCheckpointProp;
    protected SerializedProperty _setParametersForReset;
    protected SerializedProperty _startingNumberOfEnergyCellsProp;

    protected virtual void OnEnable()
    {
        _fileToSaveAndLoadFromProp = serializedObject.FindProperty("FileToSaveAndLoadFrom");
        _startAtSpecifiedCheckpointProp = serializedObject.FindProperty("StartAtSpecifiedCheckpoint");
        _setParametersForReset = serializedObject.FindProperty("SetParametersForReset");
        _startingNumberOfEnergyCellsProp = serializedObject.FindProperty("StartingNumberOfEnergyCells");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        LevelStartData levelStartData = target as LevelStartData;

        EditorGUILayout.PropertyField(_fileToSaveAndLoadFromProp, new GUIContent("File to Save and Load From"));
        EditorGUILayout.PropertyField(_startAtSpecifiedCheckpointProp, new GUIContent("Start at Specified Checkpoint"));
        EditorGUILayout.PropertyField(_setParametersForReset, new GUIContent("Set Parameters for Reset"));

        if (_setParametersForReset.boolValue)
        {
            EditorGUILayout.PropertyField(_startingNumberOfEnergyCellsProp, new GUIContent("Starting Number of Energy Cells"));
        }

        EditorGUILayout.Space();
        if (GUILayout.Button(new GUIContent(_setParametersForReset.boolValue ? "Reset Checkpoint Progress and Parameters" : "Reset Checkpoint Progress")))
        {
            levelStartData.ResetLevelParameters();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
