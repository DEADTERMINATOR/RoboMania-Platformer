﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlatformingGauntletStartData), true)]
public class PlatformingGauntletStartDataEditor : LevelStartDataEditor
{
    protected SerializedProperty _resetSecondEnergyCellProp;
    protected SerializedProperty _resetThirdEnergyCellProp;

    protected override void OnEnable()
    {
        base.OnEnable();
        _resetSecondEnergyCellProp = serializedObject.FindProperty("ResetSecondEnergyCell");
        _resetThirdEnergyCellProp = serializedObject.FindProperty("ResetThirdEnergyCell");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        LevelStartData levelStartData = target as LevelStartData;

        EditorGUILayout.PropertyField(_fileToSaveAndLoadFromProp, new GUIContent("File to Save and Load From"));
        EditorGUILayout.PropertyField(_startAtSpecifiedCheckpointProp, new GUIContent("Start at Specified Checkpoint"));
        EditorGUILayout.PropertyField(_setParametersForReset, new GUIContent("Set Parameters for Reset"));

        if (_setParametersForReset.boolValue)
        {
            EditorGUILayout.PropertyField(_startingNumberOfEnergyCellsProp, new GUIContent("Starting Number of Energy Cells"));
            EditorGUILayout.PropertyField(_resetSecondEnergyCellProp, new GUIContent("Reset Pickup Status for 2nd Energy Cell"));
            EditorGUILayout.PropertyField(_resetThirdEnergyCellProp, new GUIContent("Reset Pickup Status for 3rd Energy Cell"));
        }

        EditorGUILayout.Space();
        if (GUILayout.Button(new GUIContent(_setParametersForReset.boolValue ? "Reset Checkpoint Progress and Parameters" : "Reset Checkpoint Progress")))
        {
            levelStartData.ResetLevelParameters();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
