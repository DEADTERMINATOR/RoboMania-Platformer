﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ClimbableObjectMountingPoint))]
public class ClimbableObjectMountingPointEditor : Editor
{
    private SerializedProperty _endProp;
    private SerializedProperty _associatedClimbableObjectProp;
    private SerializedProperty _stopClimbingOnContactProp;
    private SerializedProperty _startPlayerAtColliderProp;
    private SerializedProperty _playerStartingPositionProp;

    private void OnEnable()
    {
        _endProp = serializedObject.FindProperty("_end");
        _associatedClimbableObjectProp = serializedObject.FindProperty("_associatedClimbableObject");
        _stopClimbingOnContactProp = serializedObject.FindProperty("_stopClimbingOnContact");
        _startPlayerAtColliderProp = serializedObject.FindProperty("_startPlayerAtCollider");
        _playerStartingPositionProp = serializedObject.FindProperty("_playerStartingPosition");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_endProp, new GUIContent("End"));
        EditorGUILayout.PropertyField(_associatedClimbableObjectProp, new GUIContent("Associated Climbable Object"), true);
        EditorGUILayout.PropertyField(_stopClimbingOnContactProp, new GUIContent("Stop Climbing On Contact"));
        EditorGUILayout.PropertyField(_startPlayerAtColliderProp, new GUIContent("Start Player At Collider"));
        if (!_startPlayerAtColliderProp.boolValue)
        {
            EditorGUILayout.PropertyField(_playerStartingPositionProp, new GUIContent("Player Starting Position"));
        }

        serializedObject.ApplyModifiedProperties();
    }
}
