﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Characters;
using Characters.Enemies;

[CustomEditor(typeof(NPCSpawner))]
[CanEditMultipleObjects]
public class NPCSpawnerEditor : Editor
{
    private SerializedProperty _prefabProp;
    private SerializedProperty _reSapwnOnDeativateProp;
    private SerializedProperty _isUniqueSpawn;
    private SerializedProperty _defaultsAppliedProp;
    private SerializedProperty _spawnerID;
    private SerializedProperty _difficulty;
    private SerializedProperty _displaySpriteProp;
    private SerializedProperty _overrideDisplaySpriteScaleProp;
    private SerializedProperty _displaySpriteScaleProp;
    private SerializedProperty _forceBoundsRegenerationProp;


    private void OnEnable()
    {
        _prefabProp = serializedObject.FindProperty("PrefabToSpawn");
        _reSapwnOnDeativateProp = serializedObject.FindProperty("RespawnOnDeath");
        _isUniqueSpawn = serializedObject.FindProperty("IsAUniqueSpawn"); 
        _defaultsAppliedProp = serializedObject.FindProperty("SpawnerDefaultsApplied");
        _difficulty = serializedObject.FindProperty("Difficulty");
        _spawnerID = serializedObject.FindProperty("SPAWNERID");
        _displaySpriteProp = serializedObject.FindProperty("_displaySprite");
        _overrideDisplaySpriteScaleProp = serializedObject.FindProperty("_overrideDisplaySpriteScale");
        _displaySpriteScaleProp = serializedObject.FindProperty("_displaySpriteScale");
        _forceBoundsRegenerationProp = serializedObject.FindProperty("_forceBoundsRegeneration");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.LabelField("Spawner Parameters", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_prefabProp, new GUIContent("NPC Prefab"), false);
        
        EditorGUILayout.PropertyField(_difficulty, new GUIContent("Min Difficulty To Spawn At"), true);

        if (((NPCSpawner)serializedObject.targetObject).PrefabToSpawn != null)
        {
            if (!_isUniqueSpawn.boolValue)
            {
                EditorGUILayout.PropertyField(_reSapwnOnDeativateProp, new GUIContent("Respawn After Death"), false);
            }
            if (!_reSapwnOnDeativateProp.boolValue)
            {
                EditorGUILayout.PropertyField(_isUniqueSpawn, new GUIContent("Should only Be killed once"), false);
            }
            if (GUILayout.Button("Regenerate Spawner Bounds/Sprite"))
            {
                _forceBoundsRegenerationProp.boolValue = true;
            }
            if (GUILayout.Button("Apply " + ((GameObject)_prefabProp.objectReferenceValue).name + " Prefab Defaults"))
            {
                _defaultsAppliedProp.boolValue = false;
            }
            if (GUILayout.Button("Give New Spawner ID "))
            {
                _spawnerID.stringValue =  Guid.NewGuid().ToString();
            }
            EditorGUILayout.LabelField("SpawnnerID: " + _spawnerID.stringValue);
            EditorGUILayout.PropertyField(_displaySpriteProp, new GUIContent("Display Sprite"));

            EditorGUILayout.PropertyField(_overrideDisplaySpriteScaleProp, new GUIContent("Override Display Sprite Scale"));
            if (_overrideDisplaySpriteScaleProp.boolValue)
            {
                EditorGUILayout.PropertyField(_displaySpriteScaleProp, new GUIContent("Display Sprite Scale"));
            }

            NPC targetNPC = ((NPCSpawner)serializedObject.targetObject).PrefabToSpawn.GetComponentInChildren<NPC>();
            if (targetNPC != null)
            {
                SerializedProperty spawnerProp = serializedObject.FindProperty("SpawnerData");
                if (targetNPC is Sniper)
                {
                    if (Selection.activeGameObject.GetComponent<SniperSpawnerData>() == null)
                        ((NPCSpawner)serializedObject.targetObject).SpawnerData = Selection.activeGameObject.AddComponent<SniperSpawnerData>();

                    if (_defaultsAppliedProp.boolValue == false)
                    {
                        Sniper sniperPrefabComponent = ((GameObject)_prefabProp.objectReferenceValue).GetComponent<Sniper>();
                        SniperSpawnerData spawnerData = spawnerProp.objectReferenceValue as SniperSpawnerData;

                        if (sniperPrefabComponent != null && spawnerData != null)
                        {
                            spawnerData.ActiveZones = sniperPrefabComponent.RobotSniperActiveZones;

                            spawnerProp.serializedObject.ApplyModifiedProperties();
                            ApplyEnemyPrefabDefaults(ref spawnerProp, sniperPrefabComponent);
                        }
                    }
                }
                else if (targetNPC is Cockroach)
                {
                    if (Selection.activeGameObject.GetComponent<CockroachSpawnerData>() == null )
                        ((NPCSpawner)serializedObject.targetObject).SpawnerData = Selection.activeGameObject.AddComponent<CockroachSpawnerData>();

                    if (_defaultsAppliedProp.boolValue == false)
                    {
                        Cockroach cockroachPrefabComponent = ((GameObject)_prefabProp.objectReferenceValue).GetComponent<Cockroach>();
                        CockroachSpawnerData spawnerData = spawnerProp.objectReferenceValue as CockroachSpawnerData;

                        if (cockroachPrefabComponent != null && spawnerData != null)
                        {
                            spawnerData.StartUpsideDown = cockroachPrefabComponent.UpSideDown;
                            spawnerData.CanBeOnRoof = cockroachPrefabComponent.CanBeOnRoof;

                            spawnerProp.serializedObject.ApplyModifiedProperties();
                            ApplyEnemyPrefabDefaults(ref spawnerProp, cockroachPrefabComponent);
                        }                        
                    }
                }
                else if (targetNPC is Wasp)
                {
                    if (Selection.activeGameObject.GetComponent<WaspSpawnerData>() == null)
                        ((NPCSpawner)serializedObject.targetObject).SpawnerData = Selection.activeGameObject.AddComponent<WaspSpawnerData>();

                    if (_defaultsAppliedProp.boolValue == false)
                    {
                        Wasp waspPrefabComponent = ((GameObject)_prefabProp.objectReferenceValue).GetComponent<Wasp>();
                        WaspSpawnerData spawnerData = spawnerProp.objectReferenceValue as WaspSpawnerData;

                        if (waspPrefabComponent != null && spawnerData != null)
                        {
                            spawnerData.FaceStartingDirectionWheneverPossible = waspPrefabComponent.FaceStartingDirectionWheneverPossible;

                            spawnerProp.serializedObject.ApplyModifiedProperties();
                            ApplyEnemyPrefabDefaults(ref spawnerProp, waspPrefabComponent);
                        }
                    }
                }
                else if (targetNPC is Enemy)
                {
                    if (Selection.activeGameObject.GetComponent<EnemySpawnerData>() == null)
                    {
                        ((NPCSpawner)serializedObject.targetObject).SpawnerData = Selection.activeGameObject.AddComponent<EnemySpawnerData>();
                    }
                    else if (((NPCSpawner)serializedObject.targetObject).SpawnerData == null)
                    {
                        ((NPCSpawner)serializedObject.targetObject).SpawnerData = Selection.activeGameObject.GetComponent<EnemySpawnerData>();
                    }

                    if (_defaultsAppliedProp.boolValue == false)
                    {
                        Enemy enemyPrefabComponent = ((GameObject)_prefabProp.objectReferenceValue).GetComponent<Enemy>();
                        ApplyEnemyPrefabDefaults(ref spawnerProp, enemyPrefabComponent);
                    }
                }
                else
                {
                    if (Selection.activeGameObject.GetComponent<NPCSpawnerData>() == null)
                    {
                        ((NPCSpawner)serializedObject.targetObject).SpawnerData = Selection.activeGameObject.AddComponent<NPCSpawnerData>();
                    }
                    else if (((NPCSpawner)serializedObject.targetObject).SpawnerData == null)
                    {
                        ((NPCSpawner)serializedObject.targetObject).SpawnerData = Selection.activeGameObject.GetComponent<NPCSpawnerData>();
                    }

                    if (_defaultsAppliedProp.boolValue == false)
                    {
                        NPC npcPrefabComponent = ((GameObject)_prefabProp.objectReferenceValue).GetComponent<NPC>();
                        ApplyNPCPrefabDefaults(ref spawnerProp, npcPrefabComponent);
                    }
                }
              
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    private void ApplyEnemyPrefabDefaults(ref SerializedProperty spawnerProp, Enemy enemyPrefabComponent)
    {
        EnemySpawnerData spawnerData = spawnerProp.objectReferenceValue as EnemySpawnerData;

        if (enemyPrefabComponent != null && spawnerData != null)
        {
            spawnerData.MaxHealth = enemyPrefabComponent.MaxHealth;

            spawnerData.GunChipSpawnChance = enemyPrefabComponent.GunChipSpawnChance;
            spawnerData.ShieldSpawnChance = enemyPrefabComponent.ShieldSpawnChance;

            spawnerProp.serializedObject.ApplyModifiedProperties();
            ApplyNPCPrefabDefaults(ref spawnerProp, enemyPrefabComponent);
        }
    }

    private void ApplyNPCPrefabDefaults(ref SerializedProperty spawnerProp, NPC npcPrefabComponent)
    {
        NPCSpawnerData spawnerData = spawnerProp.objectReferenceValue as NPCSpawnerData;

        if (npcPrefabComponent != null && spawnerData != null)
        {
            spawnerData.GravityScale = npcPrefabComponent.GravityScale; 
            _defaultsAppliedProp.boolValue = true;
            spawnerProp.serializedObject.ApplyModifiedProperties();
        }
    }
}
