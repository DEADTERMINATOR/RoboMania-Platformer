﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NPCSpawnerData))]
[CanEditMultipleObjects]
public class NPCSpawnerDataEditor : Editor
{
    private SerializedProperty _initialGravityStateProp;
    private SerializedProperty _gravityScaleProp;

    private SerializedProperty _startFaceDirProp;
    
    private SerializedProperty _waypointsModeProp;
    private SerializedProperty _waypointsProp;
    private SerializedProperty _startingWaypointProp;

    private SerializedProperty _boundsModeProp;
    private SerializedProperty _boundsProp;
    private Collider2D _boundsCollider;

    private SerializedProperty _tileModeProp;
    private SerializedProperty _tilerProp;

    protected virtual void OnEnable()
    {
        _initialGravityStateProp = serializedObject.FindProperty("InitialGravityState");
        _gravityScaleProp = serializedObject.FindProperty("GravityScale");

        _startFaceDirProp = serializedObject.FindProperty("StartFaceDir");
        
        _waypointsModeProp = serializedObject.FindProperty("WaypointsMode");
        _waypointsProp = serializedObject.FindProperty("WaypointSystem");
        _startingWaypointProp = serializedObject.FindProperty("StartingWaypoint");

        _boundsModeProp = serializedObject.FindProperty("BoundsMode");
        _boundsProp = serializedObject.FindProperty("ActiveBounds");

        _tileModeProp = serializedObject.FindProperty("PathTileMode");
        _tilerProp = serializedObject.FindProperty("AreaTiler");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.LabelField("NPC Parameters", EditorStyles.boldLabel);

        EditorGUILayout.PropertyField(_initialGravityStateProp, new GUIContent("Initial Gravity State"));
        EditorGUILayout.PropertyField(_gravityScaleProp, new GUIContent("Gravity Scale"));

        EditorGUILayout.PropertyField(_startFaceDirProp, new GUIContent("Starting Facing Direction"));

        EditorGUILayout.PropertyField(_waypointsModeProp, new GUIContent("Waypoint System Selection Mode"));
        if (_waypointsModeProp.enumValueIndex == 2)
        {
            EditorGUILayout.PropertyField(_waypointsProp, new GUIContent("Waypoint System"));
            EditorGUILayout.PropertyField(_startingWaypointProp, new GUIContent("Starting Waypoint"));
        }

        EditorGUILayout.PropertyField(_boundsModeProp, new GUIContent("Bounds Selection Mode"));
        if (_boundsModeProp.enumValueIndex == 1)
        {
            EditorGUILayout.PropertyField(_boundsProp, new GUIContent("Bounds Rect"));
        }
        else if (_boundsModeProp.enumValueIndex == 3)
        {
            _boundsCollider = EditorGUILayout.ObjectField("Bounds Collider", _boundsCollider, typeof(Collider2D), true) as Collider2D;
            if (_boundsCollider != null)
            {
                _boundsProp.rectValue = new Rect(_boundsCollider.bounds.min.x, _boundsCollider.bounds.min.y, _boundsCollider.bounds.size.x, _boundsCollider.bounds.size.y);

                EditorGUILayout.RectField("Bounds Rect", _boundsProp.rectValue);
            }
        }
        EditorGUILayout.PropertyField(_tileModeProp, new GUIContent("Path Tile Selection Mode"));
        if (_tileModeProp.enumValueIndex == 1)
        {
            EditorGUILayout.PropertyField(_tilerProp, new GUIContent("Area Tiler"));
        }
        serializedObject.ApplyModifiedProperties();
    }
}
