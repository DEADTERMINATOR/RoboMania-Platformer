﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EnemySpawnerData))]
[CanEditMultipleObjects]
public class EnemySpawnerDataEditor : NPCSpawnerDataEditor
{
    private SerializedProperty _maxHealthProp;
    private SerializedProperty _stunReductionProp;

    private SerializedProperty _gunChipSpawnProp;
    private SerializedProperty _shieldSpawnProp;


    protected override void OnEnable()
    {
        base.OnEnable();

        _maxHealthProp = serializedObject.FindProperty("MaxHealth");
        _stunReductionProp = serializedObject.FindProperty("StunReductionPerSecond");

        _gunChipSpawnProp = serializedObject.FindProperty("GunChipSpawnChance");
        _shieldSpawnProp = serializedObject.FindProperty("ShieldSpawnChance");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Enemy Parameters", EditorStyles.boldLabel);

        EditorGUILayout.PropertyField(_maxHealthProp, new GUIContent("Max Health"));
       /// EditorGUILayout.PropertyField(_stunReductionProp, new GUIContent("Stun Reduction (Per Second)"));

        EditorGUILayout.PropertyField(_gunChipSpawnProp, new GUIContent("Gunchip Spawn Chance (% out of 100)"));
        EditorGUILayout.PropertyField(_shieldSpawnProp, new GUIContent("Shield Spawn Chance (% out of 100"));

        serializedObject.ApplyModifiedProperties();
    }
}
