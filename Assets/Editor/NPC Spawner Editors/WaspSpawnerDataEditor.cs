﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WaspSpawnerData))]
[CanEditMultipleObjects]
public class WaspSpawnerDataEditor : EnemySpawnerDataEditor
{
    private SerializedProperty _faceStartingDirectionProp;


    protected override void OnEnable()
    {
        base.OnEnable();
        _faceStartingDirectionProp = serializedObject.FindProperty("FaceStartingDirectionWheneverPossible");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Wasp Parameters", EditorStyles.boldLabel);

        EditorGUILayout.PropertyField(_faceStartingDirectionProp, new GUIContent("Face Starting Direction Whenever Possible"));

        serializedObject.ApplyModifiedProperties();
    }
}
