﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpinySpawnerData))]
[CanEditMultipleObjects]
public class SpinySpawnerDataEditor : EnemySpawnerDataEditor
{
    private SerializedProperty _platformColliderProp;
    private SerializedProperty _allowedMovementAreaProp;
    private SerializedProperty _startingPlatformSideProp;

    protected override void OnEnable()
    {
        base.OnEnable();
        _allowedMovementAreaProp = serializedObject.FindProperty("AllowedMovementArea");
        _startingPlatformSideProp = serializedObject.FindProperty("StartingPlatformSide");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Spiny Properties", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(_allowedMovementAreaProp, new GUIContent("Allowed Movement Area"));
        EditorGUILayout.PropertyField(_startingPlatformSideProp, new GUIContent("Starting Platform Side"));

        serializedObject.ApplyModifiedProperties();
    }
}
