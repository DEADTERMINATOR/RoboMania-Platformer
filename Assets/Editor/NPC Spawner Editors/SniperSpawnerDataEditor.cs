﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SniperSpawnerData))]
[CanEditMultipleObjects]
public class SniperSpawnerDataEditor : EnemySpawnerDataEditor
{

    private SerializedProperty _startingDirectionProp;


    protected override void OnEnable()
    {
        base.OnEnable();
        _startingDirectionProp = serializedObject.FindProperty("ActiveZones");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Sniper Parameters", EditorStyles.boldLabel);

        EditorGUILayout.PropertyField(_startingDirectionProp, new GUIContent("Active Zones"));

        serializedObject.ApplyModifiedProperties();

    }
}