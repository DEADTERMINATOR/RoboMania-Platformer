﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(CockroachSpawnerData))]
[CanEditMultipleObjects]
public class CockroachSpawnerDataEditor : EnemySpawnerDataEditor
{
    private SerializedProperty _startingDirectionProp;
    private SerializedProperty _canGoUpsideDown;

    protected override void OnEnable()
    {
        base.OnEnable();
        _startingDirectionProp = serializedObject.FindProperty("StartUpsideDown");
        _canGoUpsideDown = serializedObject.FindProperty("CanBeOnRoof");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Cockroach Parameters", EditorStyles.boldLabel);

        EditorGUILayout.PropertyField(_startingDirectionProp, new GUIContent("Starting Upside down Only use if can be upside down or it will just droop to the ground"));
        EditorGUILayout.PropertyField(_canGoUpsideDown, new GUIContent("Can Go on Roof"));
        serializedObject.ApplyModifiedProperties();
    }
}