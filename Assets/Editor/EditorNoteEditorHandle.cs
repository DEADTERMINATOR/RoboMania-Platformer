﻿//Create a folder and call it "Editor" if one doesn't already exist. Place this script in it.

using UnityEngine;
using System.Collections;
using UnityEditor;

// Create a 180 degrees wire arc with a ScaleValueHandle attached to the disc
// lets you visualize some info of the transform

[CustomEditor(typeof(EditorNote))]
class LabelHandle : Editor
{
    void OnSceneGUI()
    {
        EditorNote handleExample = (EditorNote)target;
        if (handleExample == null)
        {
            return;
        }

        Handles.color = Color.gray;
        GUIStyle style = new GUIStyle();
        style.fontSize = 13;
        style.onNormal.textColor = Color.white;
        style.onActive.textColor = Color.white;
        style.onHover.textColor = Color.white;
        style.onFocused.textColor = Color.white;
        Handles.DrawSolidRectangleWithOutline(new Rect(handleExample.transform.position,new Vector2(35,-3)),Color.white,new Color(125, 125, 125, 55));
        Handles.Label(handleExample.transform.position,
    handleExample.note.ToString(), style);




    }
}