﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ModifyRigidbodyForGravity))]
public class ModifyRigidbodyForGravityEditor : Editor
{
    /// <summary>
    /// A string array containing the possible gravity override options. To be passed
    /// into the mask field.
    /// </summary>
    private string[] _overrideOptions = { "Base", "Lava" };


    public override void OnInspectorGUI()
    {
        ModifyRigidbodyForGravity modifyRigidbodyForGravity = target as ModifyRigidbodyForGravity;
        modifyRigidbodyForGravity.OverrideGravityTypes = EditorGUILayout.MaskField("Override Gravity Types", modifyRigidbodyForGravity.OverrideGravityTypes, _overrideOptions);

        if ((modifyRigidbodyForGravity.OverrideGravityTypes & 1) != 0)
        {
            modifyRigidbodyForGravity.BaseLinearDrag = EditorGUILayout.FloatField("Base Linear Drag", modifyRigidbodyForGravity.BaseLinearDrag);
            modifyRigidbodyForGravity.BaseAngularDrag = EditorGUILayout.FloatField("Base Angular Drag", modifyRigidbodyForGravity.BaseAngularDrag);
            modifyRigidbodyForGravity.BaseGravityScale = EditorGUILayout.FloatField("Base Gravity Scale", modifyRigidbodyForGravity.BaseGravityScale);
        }
        if ((modifyRigidbodyForGravity.OverrideGravityTypes & 2) != 0)
        {
            modifyRigidbodyForGravity.LavaLinearDrag = EditorGUILayout.FloatField("Lava Linear Drag", modifyRigidbodyForGravity.LavaLinearDrag);
            modifyRigidbodyForGravity.LavaAngularDrag = EditorGUILayout.FloatField("Lava Angular Drag", modifyRigidbodyForGravity.LavaAngularDrag);
            modifyRigidbodyForGravity.LavaGravityScale = EditorGUILayout.FloatField("Lava Gravity Scale", modifyRigidbodyForGravity.LavaGravityScale);
        }
    }
}
