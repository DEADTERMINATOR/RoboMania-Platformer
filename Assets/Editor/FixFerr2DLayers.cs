﻿using UnityEngine;
using UnityEditor;

public class FixFerr2DLayers : ScriptableObject
{


    [MenuItem("Tools/Level Tools/Ferr2D/Fix Sorting Order (Un-Safe)")]
    static void DoIt2()
    {
        //EditorUtility.DisplayDialog("MyTool", "Do It in C# !", "OK", "");
        int i = 10;
        foreach (Ferr2DT_PathTerrain ferr2d in GameObject.FindObjectsOfType<Ferr2DT_PathTerrain>())
        {
            if (ferr2d.gameObject.GetComponent<MeshRenderer>().sortingOrder >= 0)
            {
                ferr2d.gameObject.GetComponent<MeshRenderer>().sortingOrder = i;
                ferr2d.WasSortingFixed = true;
                i++;
            }
            //AssetDatabase.SaveAssets();
        }
    }

    [MenuItem("Tools/Level Tools/Ferr2D/Fix Sorting Order (Safe)")]
    static void DoIt3()
    {
        //EditorUtility.DisplayDialog("MyTool", "Do It in C# !", "OK", "");
        int i = 10;
        foreach (Ferr2DT_PathTerrain ferr2d in GameObject.FindObjectsOfType<Ferr2DT_PathTerrain>())
        {
            if (ferr2d.gameObject.GetComponent<MeshRenderer>().sortingOrder >= 0)
            {
                if (!ferr2d.WasSortingFixed)
                {
                    ferr2d.gameObject.GetComponent<MeshRenderer>().sortingOrder = i;
                    ferr2d.WasSortingFixed = true;
                    i++;
                }
                else
                {
                    i = ferr2d.gameObject.GetComponent<MeshRenderer>().sortingOrder + 1;
                }
                
            }
            //AssetDatabase.SaveAssets();
        }
    }
    [MenuItem("Tools/Level Tools/Ferr2D/Flatten Sorting Order ")]
    static void DoIt4()
    {
        //EditorUtility.DisplayDialog("MyTool", "Do It in C# !", "OK", "");
        int i = 10;
        foreach (Ferr2DT_PathTerrain ferr2d in GameObject.FindObjectsOfType<Ferr2DT_PathTerrain>())
        {
            if (ferr2d.gameObject.GetComponent<MeshRenderer>().sortingOrder >= 0)
            {
                ferr2d.gameObject.GetComponent<MeshRenderer>().sortingOrder = 0;
                ferr2d.WasSortingFixed = false;
                i++;
            }
            //AssetDatabase.SaveAssets();
        }
    }

    [MenuItem("Tools/Level Tools/Ferr2D/Set Tangents")]
    static void DoIt()
    {
        //EditorUtility.DisplayDialog("MyTool", "Do It in C# !", "OK", "");

        foreach (Ferr2DT_PathTerrain ferr2d in GameObject.FindObjectsOfType<Ferr2DT_PathTerrain>())
        {
            if (ferr2d.gameObject.layer == GlobalData.OBSTACLE_LAYER)
            {
                ferr2d.createTangents = true;
                EditorUtility.SetDirty(ferr2d.gameObject);
                ferr2d.PathData.SetDirty();
                ferr2d.Build(true);
            }
        }
    }


    [MenuItem("Tools/Level Tools/Ferr2D/Set Z OFfset ")]
    static void SetZ()
    {
        //EditorUtility.DisplayDialog("MyTool", "Do It in C# !", "OK", "");

        foreach (Ferr2DT_PathTerrain ferr2d in GameObject.FindObjectsOfType<Ferr2DT_PathTerrain>())
        {
            if (ferr2d.gameObject.layer == GlobalData.OBSTACLE_LAYER)
            {
                ferr2d.fillZ = 0.25f;
                EditorUtility.SetDirty(ferr2d.gameObject);
                ferr2d.PathData.SetDirty();
                ferr2d.Build(true);
            }
        }
    }


    [MenuItem("Tools/Level Tools/Ferr2D/Set Z")]
    static void SetAllZ()
    {
        //EditorUtility.DisplayDialog("MyTool", "Do It in C# !", "OK", "");

        foreach (Ferr2DT_PathTerrain ferr2d in GameObject.FindObjectsOfType<Ferr2DT_PathTerrain>())
        {
            if (ferr2d.gameObject.layer == GlobalData.OBSTACLE_LAYER)
            {
                ferr2d.gameObject.transform.position = new Vector3(ferr2d.gameObject.transform.position.x, ferr2d.gameObject.transform.position.y,0);
                EditorUtility.SetDirty(ferr2d.gameObject);

            }
        }
    }

}