﻿using UnityEngine;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using System.Collections.Generic;

[CustomEditor(typeof(TillerSettings))]
public class TillerSettingsEditor : Editor
{
    private List<BoxBoundsHandle> m_BoundsHandle =  new List<BoxBoundsHandle>();

    private TillerSettings lastTraget = null;
    private int lastCount = 0;
    private void OnEnable()
    {
        RequiresConstantRepaint();
    }

    void OnSceneGUI()
    {
        TillerSettings t = target as TillerSettings;
        // Did the target change or the number of bounds change 
        if(lastTraget != t || lastCount != t.Bounds.Count)
        {
            m_BoundsHandle = new List<BoxBoundsHandle>();
            for(int i =0; i < t.Bounds.Count; i++ )
            {
                m_BoundsHandle.Add(new BoxBoundsHandle());

            }
            if(lastCount != t.Bounds.Count && t.Bounds[t.Bounds.Count - 1].position == Vector2Int.zero )
            {
                RectInt newRect = new RectInt();
                newRect.position = PathTilleMap.Vector2ToVector2Int(t.transform.position);
                newRect.size = new Vector2Int(10, 10);
                t.Bounds[t.Bounds.Count - 1] = newRect;
            }

            lastTraget = t;
            lastCount = t.Bounds.Count;
        }

        RequiresConstantRepaint();
        for (int i = 0; i < t.Bounds.Count; i++)
        {
            // copy the target object's data to the handle
            m_BoundsHandle[i].center = t.Bounds[i].center;
            m_BoundsHandle[i].size = (Vector2)t.Bounds[i].size;

            // draw the handle
            EditorGUI.BeginChangeCheck();
            m_BoundsHandle[i].DrawHandle();
            if (EditorGUI.EndChangeCheck())
            {
                Debug.Log("Edit");
                // record the target object before setting new values so changes can be undone/redone
                Undo.RecordObject(t, "Change Bounds");

                // copy the handle's updated data back to the target object
                RectInt newBounds = new RectInt();
                newBounds.min = PathTilleMap.Vector2ToVector2Int(m_BoundsHandle[i].center - (m_BoundsHandle[i].size / 2));
                newBounds.size = PathTilleMap.Vector2ToVector2Int(m_BoundsHandle[i].size);
                t.Bounds[i] = newBounds;
            }
        }
    }    
}