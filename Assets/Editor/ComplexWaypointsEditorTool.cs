﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class ComplexWaypointsEditorTool : EditorWindow
{
  /* string myString;
    public Object source { get; private set; }
    static ComplexWayPoints wayPoints;
    private static List<ComplexWayPointEdit> wayPointsEdit;
    private bool IsEditing = false;
    GameObject EditPoints = null;

    int MakeNAreas;

    #region MenuIitems
    [MenuItem("Path Tools/Complex WayPoint Paths/Remove Point #r", false, 10)]
    static void RemovePoint(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if(go == null)
        {
            go = Selection.activeGameObject;
        }
        EditComplexWayPointGameObject point = go.GetComponent<EditComplexWayPointGameObject>();
        if (point == null)
        {
            Debug.LogError("Could not Delet Point");
            return; ///noting to work with
        }
        if (point.IsPartrolPoint)
        {
            wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points.RemoveAt(point.Index);
            DestroyImmediate(go);
            wayPointsEdit[point.OwningArea].PatrolPoints.RemoveAt(point.Index);
            FixNames(wayPointsEdit[point.OwningArea].PatrolPoints);
        }
        else
        {
            wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.Points.RemoveAt(point.Index);
            DestroyImmediate(go);
            wayPointsEdit[point.OwningArea].PathToNextPoints.RemoveAt(point.Index);
            FixConnectionNames(wayPointsEdit[point.OwningArea].PathToNextPoints);
        }

    }
    [MenuItem("Path Tools/Complex WayPoint Paths/Remove Point #r", true,10)]
    static bool ValadateRemovePoint(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }
        if (go == null)
        {
            return false;
        }
        EditComplexWayPointGameObject it = go.GetComponent<EditComplexWayPointGameObject>();
        return (it != null);
    }

    [MenuItem("GameObject/Complex WayPoint Paths/Add Point After &a", false, 10)]
    static void AddPoint(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }
        EditComplexWayPointGameObject point = go.GetComponent<EditComplexWayPointGameObject>();
        if(point == null)
        {
            return; ///noting to work with
        }
        Debug.Log("Found point in Area" + point.OwningArea);
        if(point.IsPartrolPoint)
        {
            AddPartollPointAfter(point);

        }
        else
        {
            AddConnectionPointAfter(point);
        }
    }


    [MenuItem("GameObject/Complex WayPoint Paths/Add Point Befor &b", false, 10)]
    static void AddPointBefor(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }
        EditComplexWayPointGameObject point = go.GetComponent<EditComplexWayPointGameObject>();
        if (point == null)
        {
            return; ///noting to work with
        }
        Debug.Log("Found point in Area" + point.OwningArea);
        if(point.IsPartrolPoint)
        {
            AddPatrolPointBefor(point);
        }
        else
        {
            AddControllPointBefor(point);
        }

    }
    #endregion
    [MenuItem("Path Tools/Complex WayPoint Paths/Open Editor ")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(ComplexWaypointsEditorTool));
    }
    #region GUI
    void OnGUI()
    {
       // EditorGUILayout.BeginHorizontal();
        // The actual window code goes here
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        if (!IsEditing)
            source = EditorGUILayout.ObjectField(source, typeof(ComplexWayPoints), true);
        if (source != null)
        {
            wayPoints = (ComplexWayPoints)source;
        }
        string ButtonText = "Start Editing WayPoints";
        if (IsEditing)
        {
            ButtonText = "Stop Editing WayPoints";
        }

        if(wayPoints != null)
        {
            if (GUILayout.Button(ButtonText))
            {
                if (!IsEditing)
                {
                    wayPointsEdit = new List<ComplexWayPointEdit>();
                    EditPoints = new GameObject("WayPoint Edoitor Points");
                    EditPoints.transform.parent = wayPoints.transform;
                    int i = 0;
                    if (wayPoints != null)
                    {

                        foreach (ComplexWayPoint area in wayPoints.ActiveAreas)
                        {
                            ComplexWayPointEdit editOBJ = new ComplexWayPointEdit();
                            GameObject Areas = new GameObject("Area " + i);
                            Areas.transform.parent = EditPoints.transform;
                            GameObject PatrolPoints = new GameObject("Patrol Points");
                            PatrolPoints.transform.parent = Areas.transform;
                            Areas.transform.position = area.PatrolPoints.Points[0] + wayPoints.transform.position;
                            editOBJ.AreaGameOBJ = Areas;
                            editOBJ.PointsGameOBJ = PatrolPoints;
                            int pId = 0;
                            foreach (Vector3 point in area.PatrolPoints.Points)
                            {
                                GameObject editPoint = new GameObject("Patrol Point " + pId);
                                EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
                                pathEditPoint.OwningArea = i;
                                pathEditPoint.Index = pId;
                                editPoint.transform.position = point + wayPoints.transform.position;
                                editPoint.transform.parent = PatrolPoints.transform;
                                pathEditPoint.IsPartrolPoint = true;
                                pId++;
                                editOBJ.PatrolPoints.Add(editPoint);
                            }

                            GameObject PathToNext = new GameObject("Conection Points");
                            editOBJ.ConnectionGameOBJ = PathToNext;
                            PathToNext.transform.parent = Areas.transform;
                            int cId = 0;
                            foreach (Vector3 point in area.PathToNextPoints.Points)
                            {
                                GameObject editPoint = new GameObject("Conection Point" + cId);
                                EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
                                pathEditPoint.FillColour = Color.red;
                                pathEditPoint.OwningArea = i;
                                pathEditPoint.Index = cId;
                                pathEditPoint.IsPartrolPoint = false;
                                editPoint.transform.position = point + wayPoints.transform.position;
                                editPoint.transform.parent = PathToNext.transform;
                                cId++;
                                editOBJ.PathToNextPoints.Add(editPoint);
                            }

                            i++;
                            wayPointsEdit.Add(editOBJ);
                        }

                    }
                }
                else
                {
                    Object.DestroyImmediate(EditPoints);
                    wayPointsEdit = new List<ComplexWayPointEdit>();

                }

                IsEditing = !IsEditing;
            }
        }

       // EditorGUILayout.EndHorizontal();



    }
    #endregion
    void OnDestroy()
    {
        if (IsEditing)
        {
            Object.DestroyImmediate(EditPoints);
            wayPointsEdit = new List<ComplexWayPointEdit>();
            IsEditing = !IsEditing;
        }

    }
    #region HelperFunction
    private static void FixNames(List<GameObject> PatrolPoints)
    {
        int pId = 0;
        foreach (GameObject point in PatrolPoints)
        {
            point.name = ("Patrol Point " + pId);
            ((EditComplexWayPointGameObject)point.GetComponent<EditComplexWayPointGameObject>()).Index = pId;
            point.transform.SetSiblingIndex(pId);
            pId++;
        }
    }
    private static void FixConnectionNames(List<GameObject> Points)
    {
        int pId = 0;
        foreach (GameObject point in Points)
        {
            point.name = ("Conection Points " + pId);
            ((EditComplexWayPointGameObject)point.GetComponent<EditComplexWayPointGameObject>()).Index = pId;
            point.transform.SetSiblingIndex(pId);
            pId++;
        }
    }
    private static void AddConnectionPointAfter(EditComplexWayPointGameObject point)
    {
        Vector3 PointA, PointB, newPointPos = Vector3.zero;
        if (point.Index >= 0 && point.Index < wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.Points.Count - 1)//Not on the End of the array
        {
            PointA = wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.Points[point.Index];
            PointB = wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.Points[point.Index + 1];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        else if (point.Index == wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.Points.Count - 1)//is last index
        {
            PointA = wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.Points[point.Index];
            PointB = wayPoints.GetNextArea(point.OwningArea).PatrolPoints.First;
            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        if (newPointPos == Vector3.zero)
        {
            Debug.LogError("Could not make new point");
            return;
        }

        wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.Points.Insert(point.Index + 1, newPointPos);


        GameObject editPoint = new GameObject("Patrol Point " + (point.Index));
        EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
        pathEditPoint.OwningArea = point.OwningArea;
        pathEditPoint.Index = point.Index - 1;
        editPoint.transform.position = newPointPos + wayPoints.transform.position;
        editPoint.transform.parent = wayPointsEdit[point.OwningArea].ConnectionGameOBJ.transform;
        pathEditPoint.FillColour = Color.red;
        pathEditPoint.IsPartrolPoint = false;
        wayPointsEdit[point.OwningArea].PathToNextPoints.Insert(point.Index + 1, editPoint);
        FixConnectionNames(wayPointsEdit[point.OwningArea].PathToNextPoints);
        Selection.activeObject = editPoint;
    }
    public static void AddControllPointBefor(EditComplexWayPointGameObject point)
    {
        Vector3 PointA, PointB, newPointPos = Vector3.zero;
        if (point.Index > 0 && point.Index < wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.Points.Count)//Not on the End of the array
        {
            PointA = wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.Points[point.Index - 1];
            PointB = wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.Points[point.Index];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        else if (point.Index == 0)//is last index
        {

            PointA = wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.First;
            PointB = wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Last;

            newPointPos = Vector3.Lerp(PointA, PointB, 0.5f);
        }
        if (newPointPos == Vector3.zero)
        {
            Debug.LogError("Could not make new point");
            return;
        }

        wayPoints.ActiveAreas[point.OwningArea].PathToNextPoints.Points.Insert(point.Index, newPointPos);


        GameObject editPoint = new GameObject("Patrol Point " + (point.Index));
        EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
        pathEditPoint.OwningArea = point.OwningArea;
        pathEditPoint.Index = point.Index - 1;
        pathEditPoint.FillColour = Color.red;
        editPoint.transform.position = newPointPos + wayPoints.transform.position;
        editPoint.transform.parent = wayPointsEdit[point.OwningArea].ConnectionGameOBJ.transform;
        pathEditPoint.IsPartrolPoint = false;
        wayPointsEdit[point.OwningArea].PathToNextPoints.Insert(point.Index, editPoint);
        FixConnectionNames(wayPointsEdit[point.OwningArea].PathToNextPoints);
        Selection.activeObject = editPoint;
    }

    private static void AddPartollPointAfter(EditComplexWayPointGameObject point)
    {
        Vector3 PointA, PointB, newPointPos = Vector3.zero;
        if (point.Index >= 0 && point.Index < wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points.Count - 1)//Not on the End of the array
        {
            PointA = wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points[point.Index];
            PointB = wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points[point.Index + 1];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        else if (point.Index == wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points.Count - 1)//is last index
        {
            //newPointPos = wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points[wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points.Count - 1] + Vector3.left;
            PointA = wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points[point.Index];
            PointB = wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points[0];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        if (newPointPos == Vector3.zero)
        {
            Debug.LogError("Could not make new point");
            return;
        }

        wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points.Insert(point.Index + 1, newPointPos);


        GameObject editPoint = new GameObject("Patrol Point " + (point.Index));
        EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
        pathEditPoint.OwningArea = point.OwningArea;
        pathEditPoint.Index = point.Index - 1;
        editPoint.transform.position = newPointPos + wayPoints.transform.position;
        editPoint.transform.parent = wayPointsEdit[point.OwningArea].PointsGameOBJ.transform;
        pathEditPoint.IsPartrolPoint = true;
        wayPointsEdit[point.OwningArea].PatrolPoints.Insert(point.Index + 1, editPoint);
        FixNames(wayPointsEdit[point.OwningArea].PatrolPoints);
        Selection.activeObject = editPoint;
    }

    public static void AddPatrolPointBefor(EditComplexWayPointGameObject point)
    {
        Vector3 PointA, PointB, newPointPos = Vector3.zero;
        if (point.Index > 0 && point.Index < wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points.Count)//Not on the End of the array
        {
            PointA = wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points[point.Index - 1];
            PointB = wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points[point.Index];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        else if (point.Index == 0)//is last index
        {
            PointA = wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points[0];
            PointB = wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points[point.Index];

            newPointPos = Vector3.Lerp(PointB, PointA, 0.5f);
        }
        if (newPointPos == Vector3.zero)
        {
            Debug.LogError("Could not make new point");
            return;
        }

        wayPoints.ActiveAreas[point.OwningArea].PatrolPoints.Points.Insert(point.Index, newPointPos);


        GameObject editPoint = new GameObject("Patrol Point " + (point.Index));
        EditComplexWayPointGameObject pathEditPoint = editPoint.AddComponent<EditComplexWayPointGameObject>();
        pathEditPoint.OwningArea = point.OwningArea;
        pathEditPoint.Index = point.Index - 1;
        editPoint.transform.position = newPointPos + wayPoints.transform.position;
        editPoint.transform.parent = wayPointsEdit[point.OwningArea].PointsGameOBJ.transform;
        pathEditPoint.IsPartrolPoint = true;
        wayPointsEdit[point.OwningArea].PatrolPoints.Insert(point.Index, editPoint);
        FixNames(wayPointsEdit[point.OwningArea].PatrolPoints);
        Selection.activeObject = editPoint;
    }

    
    #endregion
    void Update()
    {

        if (!EditorApplication.isPlaying && !EditorApplication.isPaused)
        {
            if (IsEditing && wayPointsEdit.Count >0)
            {
                //Convert game object trans forms back to offsets
                int i = 0;
                foreach (ComplexWayPointEdit area in wayPointsEdit)
                {
                    int pId = 0;
                    foreach (GameObject point in area.PatrolPoints)
                    {
                        wayPoints.ActiveAreas[i].PatrolPoints.Points[pId] = (Vector2)point.transform.position - (Vector2)wayPoints.transform.position;

                        pId++;
                    }

                    int cId = 0;
                    foreach (GameObject point in area.PathToNextPoints)
                    {
                        wayPoints.ActiveAreas[i].PathToNextPoints.Points[cId] = (Vector2)point.transform.position - (Vector2)wayPoints.transform.position;
                        cId++;

                    }

                    i++;
                }

            }
        }
    }*/

}

