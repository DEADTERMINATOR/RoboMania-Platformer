﻿using UnityEngine;
using UnityEditor;

public class MoveGameObjectToNewParent : ScriptableObject
{

    static GameObject Parent = null;

    [MenuItem("GameObject/Level Tools/Set New Move parent &a", false, 10)]
    static void SetParentName(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }
        if(go != null)
        {
            Parent = go;
        }
        else
        {
            Debug.Log("Error Getting Game object");
        }
       
    }

    [MenuItem("GameObject/Level Tools/Move to New Parent &a", false, 10)]
    static void MoveToNewParnet(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }
        if (go != null)
        {
            if (Parent != null)
            {
                go.transform.parent = Parent.transform;
            }
            else
            {
                Debug.Log("No Parent Set");
            }
        }
        else
        {
            Debug.Log("Error Getting Game object");
        }

    }
}