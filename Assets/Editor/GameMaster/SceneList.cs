﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
namespace GameMaster
{
    public static class SceneList
    {
        public static List<string> _sceneList = new List<string>();
        private static float lastLookUp = Time.realtimeSinceStartup;
        private const int CHACHETIME = 10*60;
        /// <summary>
        /// Get A list of Scene assets 
        /// </summary>
        /// <returns>A list of paths </returns>
        public static List<String> LookUp()
        {
            if (_sceneList.Count >0 && lastLookUp + CHACHETIME >= Time.realtimeSinceStartup)
            {
                return _sceneList;
            }
            _sceneList.Clear();
            lastLookUp = Time.realtimeSinceStartup;
            string[] files = Directory.GetFiles(GameMasterSettings.SCENE_LOOKUP_ROOT, "*.unity", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                if (file != null && file != "")
                {
                    string finalName = file.Replace("\\","/");//.Replace("Assets/","")
                    _sceneList.Add(finalName);
                }

            }
            return _sceneList;
        }

    }



}
