﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System;
using UnityEditor.SceneManagement;

namespace GameMaster
{

    [CustomEditor(typeof(LevelSceneData))]
    public class LevelSceneDataEditor : Editor
    {
        private int _masterSelection;
        private bool[] _openedSection = new bool[1];
        private int[] _selectedList = new int[1];
        private int _lastSelectedIndex = 0;
        private int _loadingScriptSelection;
        private List<string> _levelScriptNames = new List<string>();
        private const int CHACHETIME = 10 * 60;
        private static float lastLookUp = DateTime.Now.ToFileTime();

        public override void OnInspectorGUI()
        {

            LevelSceneData levelSceneData = target as LevelSceneData;

            List<string> LevelScripts = LoookUpLevelScripts();
            List<string> sceneList = SceneList.LookUp();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Name");
            levelSceneData.Name = EditorGUILayout.TextField(levelSceneData.Name);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            SceneAsset oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(levelSceneData.MasterScene);
            SceneAsset newScene = EditorGUILayout.ObjectField("Master Scene", oldScene, typeof(SceneAsset), false) as SceneAsset;
            levelSceneData.MasterScene = AssetDatabase.GetAssetPath(newScene);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Level Script");
            _loadingScriptSelection = -1;
            if (levelSceneData.levelScriptType != null)
            {
                _loadingScriptSelection = _levelScriptNames.IndexOf(levelSceneData.levelScriptType.ToString());
            }
            _loadingScriptSelection = EditorGUILayout.Popup(_loadingScriptSelection, _levelScriptNames.ToArray());
            if (_loadingScriptSelection != -1)
            {
                int oldIndex = _levelScriptNames.IndexOf(levelSceneData.levelScriptType.ToString());
                levelSceneData.levelScriptType = _levelScriptNames[_loadingScriptSelection];
                if (_loadingScriptSelection != oldIndex)
                {
                    EditorUtility.SetDirty(levelSceneData);
                    AssetDatabase.SaveAssets();
                }
            }


            EditorGUILayout.EndHorizontal();
            levelSceneData.IsAseetBundle = EditorGUILayout.Toggle("Is Asset Bundle",levelSceneData.IsAseetBundle);
            if(levelSceneData.IsAseetBundle)
            {
                EditorGUILayout.BeginHorizontal("ProjectBrowserHeaderBgTop");
                EditorGUILayout.LabelField("Bundle Name");
                levelSceneData.AssestBundeName = EditorGUILayout.TextArea(levelSceneData.AssestBundeName);
                EditorGUILayout.EndHorizontal();
                levelSceneData.LoadAseetBundleOnStart = EditorGUILayout.Toggle("LoadAseetBundleOnStart", levelSceneData.LoadAseetBundleOnStart);
                levelSceneData.UnloadAseetBundleOnPakageUnload = EditorGUILayout.Toggle("UnloadAseetBundleOnPakageUnload", levelSceneData.UnloadAseetBundleOnPakageUnload);
                
                levelSceneData.BypassBundleInEditor = EditorGUILayout.Toggle(new GUIContent("Bypass Bundle InEditor", "The scene need to be added to the build index. Only work in editor not builds "), levelSceneData.BypassBundleInEditor);
            }
            levelSceneData.HasAdditionalAseetBundle = EditorGUILayout.Toggle("Has Additional Asset Bundle", levelSceneData.HasAdditionalAseetBundle);
            if (levelSceneData.HasAdditionalAseetBundle)
            {
                EditorGUILayout.BeginHorizontal("ProjectBrowserHeaderBgTop");
                EditorGUILayout.LabelField("Bundle Name");
                levelSceneData.AdditionalAseetBundleName = EditorGUILayout.TextArea(levelSceneData.AdditionalAseetBundleName);
                EditorGUILayout.EndHorizontal();
            }


            EditorGUILayout.BeginHorizontal("ProjectBrowserHeaderBgTop");
            EditorGUILayout.LabelField("Sub Scene");
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Size: " + levelSceneData.SceneList.Count);


            if (GUILayout.Button("Add"))
            {
                SceneData data = new SceneData();
                data.Path = "";
                data.QuickName = "New";
                levelSceneData.SceneList.Add(data);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.EndHorizontal();
            if (_openedSection.Length != levelSceneData.SceneList.Count)
            {
                _openedSection = new bool[levelSceneData.SceneList.Count];
            }
            if (_selectedList.Length != levelSceneData.SceneList.Count)
            {
                _selectedList = new int[levelSceneData.SceneList.Count];
            }
            SceneAsset oldSceneList;
            SceneAsset newSceneList;
            for (int i = 0; i < levelSceneData.SceneList.Count; i++)
            {

                EditorGUILayout.BeginVertical("CN Box");
                GUILayout.Label("Sub Scene " + i);
                GUILayout.EndVertical();
                SceneData data = levelSceneData.SceneList[i];
                EditorGUILayout.BeginVertical("BOX");
                GUILayout.BeginHorizontal("BOX");
                oldSceneList = AssetDatabase.LoadAssetAtPath<SceneAsset>(levelSceneData.SceneList[i].Path);
                newSceneList = EditorGUILayout.ObjectField("Scene", oldSceneList, typeof(SceneAsset), false) as SceneAsset;
                GUILayout.EndVertical();

                GUILayout.BeginVertical("BOX");
                GUILayout.BeginHorizontal();
                GUILayout.Label("Load With package");

                data.LoadOnPackageLoad = EditorGUILayout.Toggle(data.LoadOnPackageLoad);
                if (GUILayout.Button("Remove"))
                {
                    levelSceneData.SceneList.RemoveAt(i);
                    EditorUtility.SetDirty(levelSceneData);
                    AssetDatabase.SaveAssets();
                }
                if (newSceneList != oldSceneList)
                {

                    data.Path = AssetDatabase.GetAssetPath(newSceneList);
                    levelSceneData.SceneList[i] = data;
                    EditorUtility.SetDirty(levelSceneData);
                    AssetDatabase.SaveAssets();
                    _lastSelectedIndex = _selectedList[i];
                }

                EditorGUILayout.EndHorizontal();
                GUILayout.EndVertical();
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Space();

            }
            if(GUILayout.Button("Save Data"))
            {
                EditorUtility.SetDirty(levelSceneData);
                AssetDatabase.SaveAssets();
            }

        }

        public List<string> LoookUpLevelScripts()
        {
            _levelScriptNames.Clear();
            lastLookUp = Time.realtimeSinceStartup;
            foreach (Type type in
                Assembly.GetAssembly(typeof(LevelLoadingScript)).GetTypes()
                .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(LevelLoadingScript))))
            {
                _levelScriptNames.Add(type.FullName);
            }
            return _levelScriptNames;
        }


        public static T[] GetAllInstances<T>() where T : ScriptableObject
        {
            string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);  //FindAssets uses tags check documentation for more info
            T[] a = new T[guids.Length];
            for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                a[i] = AssetDatabase.LoadAssetAtPath<T>(path);
            }

            return a;

        }
    }
}