﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Linq;

namespace UnityTool.Editor
{
    public class EditorStaticTools
    {

        public static List<string> LoookUpListOfScripts<T>()
        {
            List<string> ScriptNames = new List<string>();
            //using reflection 
            foreach (Type type in
                Assembly.GetAssembly(typeof(T)).GetTypes()
                .Where(type => type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(T))))
            {
                ScriptNames.Add(type.FullName);
            }
            return ScriptNames;
        }

        public static T[] GetAllScriptableObjectAssestsOf<T>() where T : ScriptableObject
        {
            string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);  //FindAssets uses tags check documentation for more info
            T[] objects = new T[guids.Length];
            for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                objects[i] = AssetDatabase.LoadAssetAtPath<T>(path);
            }

            return objects;
        }

        public static T CreateScriptableObjectAsset<T>(string assetName = "") where T : ScriptableObject
        {
            T asset = ScriptableObject.CreateInstance<T>();
            if (String.IsNullOrEmpty(assetName))
            {
                assetName = "New " + typeof(T).Name;
            }
            ProjectWindowUtil.CreateAsset(asset, assetName + ".asset");
            return asset;
        }
        public static ScriptableObject CreateScriptableObjectAsset(Type type , string assetName = "") 
        {
            ScriptableObject asset = ScriptableObject.CreateInstance(type);
            if (String.IsNullOrWhiteSpace(assetName))
            {
                assetName = "New " + type.Name;
            }
            ProjectWindowUtil.CreateAsset(asset, assetName + ".asset");
            return asset;
        }
        public static ScriptableObject CreateScriptableObjectAsset(string type, string assetName = "")
        {
            ScriptableObject asset = ScriptableObject.CreateInstance(type);
            if (String.IsNullOrWhiteSpace(assetName))
            {
                assetName = "New " + type;
            }
            ProjectWindowUtil.CreateAsset(asset, assetName + ".asset");
            return asset;
        }
      
    }
}