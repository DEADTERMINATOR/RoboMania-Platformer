﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Weapons.Player.Upgrades;
using static Weapons.Player.WeaponFormProperties;
using System;
using UnityEditor.Rendering;

[CustomEditor(typeof(WeaponUpgradePropertySetter), true)]
public class WeaponUpgradePropertySetterEditor : Editor
{
    private SerializedProperty _weaponFormProp;
    private SerializedProperty _changedPropertyNamesListProp;
    private SerializedProperty _changedPropertiesEditorEnumValuesProp;
    private SerializedProperty _valueChangesListProp;
    private SerializedProperty _hasNewPropertyFieldProp;

    protected virtual void OnEnable()
    {
        _weaponFormProp = serializedObject.FindProperty("_applicableWeaponForm");
        _changedPropertyNamesListProp = serializedObject.FindProperty("_changedPropertyNamesList");
        _changedPropertiesEditorEnumValuesProp = serializedObject.FindProperty("_changedPropertiesEditorEnumValues");
        _valueChangesListProp = serializedObject.FindProperty("_valueChangesList");
        _hasNewPropertyFieldProp = serializedObject.FindProperty("_hasNewPropertyField");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(_weaponFormProp, new GUIContent("Weapon Form"), true);

        if (Enum.IsDefined(typeof(WeaponForms), _weaponFormProp.enumValueIndex))
        {
            WeaponForms chosenForm = (WeaponForms)_weaponFormProp.enumValueIndex;
            List<WeaponProperties> validWeaponProperties = GetValidPropertiesForWeaponForm(chosenForm);
            string[] validWeaponPropertiesNames = GetPropertyNames(validWeaponProperties);

            if (!EditorGUI.EndChangeCheck())
            {
                for (int i = 0; i < _changedPropertyNamesListProp.arraySize; ++i)
                {
                    DisplayAndChangeExistingProperties(i, validWeaponPropertiesNames);
                }

                if (_changedPropertyNamesListProp.arraySize == 0)
                {
                    _hasNewPropertyFieldProp.boolValue = true;
                }
            }
            else
            {
                _changedPropertyNamesListProp.ClearArray();
                _changedPropertiesEditorEnumValuesProp.ClearArray();
                _valueChangesListProp.ClearArray();
            }

            if (_hasNewPropertyFieldProp.boolValue)
            {
                bool newPropertySaved = DisplayAndInsertNewProperty(validWeaponPropertiesNames);
                if (newPropertySaved)
                {
                    _hasNewPropertyFieldProp.boolValue = false;
                }
            }

            EditorGUILayout.Space();
            if (GUILayout.Button(new GUIContent("Add New Field")))
            {
                _hasNewPropertyFieldProp.boolValue = true;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }

    private void DisplayAndChangeExistingProperties(int arrayElement, string[] enumNames)
    {
        EditorGUILayout.BeginHorizontal();

        int propertySelection = _changedPropertiesEditorEnumValuesProp.GetArrayElementAtIndex(arrayElement).intValue;
        float propertyImprovementAmount = _valueChangesListProp.GetArrayElementAtIndex(arrayElement).floatValue;

        EditorGUI.BeginChangeCheck();

        int newSelection = EditorGUILayout.Popup(new GUIContent("Changed Property"), propertySelection, enumNames);
        EditorGUILayout.Space();
        float newValue = EditorGUILayout.FloatField(new GUIContent("Amount Change (%)"), propertyImprovementAmount * 100f);

        if (EditorGUI.EndChangeCheck())
        {
            _changedPropertyNamesListProp.GetArrayElementAtIndex(arrayElement).stringValue = enumNames[newSelection];
            _changedPropertiesEditorEnumValuesProp.GetArrayElementAtIndex(arrayElement).intValue = newSelection;
            _valueChangesListProp.GetArrayElementAtIndex(arrayElement).floatValue = newValue / 100f;
        }

        EditorGUILayout.EndHorizontal();
    }

    private bool DisplayAndInsertNewProperty(string[] enumNames)
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUI.BeginChangeCheck();

        int selection = EditorGUILayout.Popup(new GUIContent("Changed Property"), 0, enumNames);
        EditorGUILayout.Space();
        float improvementValue = EditorGUILayout.FloatField(new GUIContent("Amount Change (%)"), 0);

        if (EditorGUI.EndChangeCheck())
        {
            _changedPropertyNamesListProp.InsertArrayElementAtIndex(_changedPropertyNamesListProp.arraySize);
            _changedPropertyNamesListProp.GetArrayElementAtIndex(_changedPropertyNamesListProp.arraySize - 1).stringValue = enumNames[selection];

            _changedPropertiesEditorEnumValuesProp.InsertArrayElementAtIndex(_changedPropertiesEditorEnumValuesProp.arraySize);
            _changedPropertiesEditorEnumValuesProp.GetArrayElementAtIndex(_changedPropertiesEditorEnumValuesProp.arraySize - 1).intValue = selection;

            _valueChangesListProp.InsertArrayElementAtIndex(_valueChangesListProp.arraySize);
            _valueChangesListProp.GetArrayElementAtIndex(_valueChangesListProp.arraySize - 1).floatValue = improvementValue / 100f;

            EditorGUILayout.EndHorizontal();
            return true;
        }

        EditorGUILayout.EndHorizontal();
        return false;
    }
}
