﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using UnityEditor;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Timeline;
using Weapons.Player.Upgrades;
using static Weapons.Player.WeaponFormProperties;

[CustomEditor(typeof(WeaponUpgrade), true)]
public class WeaponUpgradeEditor : Editor
{
    private SerializedProperty _upgradeNameProp;
    private SerializedProperty _applicableWeaponFormProp;
    private SerializedProperty _changedPropertiesProp;
    private SerializedProperty _weaponUpgradeTypeNameProp;
    private SerializedProperty _weaponUpgradeTypeNamePopupSelectionProp;

    private List<string> _allWeaponUpgradeScripts = new List<string>();

    protected void OnEnable()
    {
        _upgradeNameProp = serializedObject.FindProperty("_upgradeName");
        _applicableWeaponFormProp = serializedObject.FindProperty("_applicableWeaponForm");
        _changedPropertiesProp = serializedObject.FindProperty("ChangedProperties");
        _weaponUpgradeTypeNameProp = serializedObject.FindProperty("_weaponUpgradeTypeName");
        _weaponUpgradeTypeNamePopupSelectionProp = serializedObject.FindProperty("_weaponUpgradeTypeNamePopupSelection");

        FindWeaponUpgradeScripts();
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_upgradeNameProp, new GUIContent("Upgrade Name"));
        EditorGUILayout.PropertyField(_applicableWeaponFormProp, new GUIContent("Weapon Form"), true);

        if (_allWeaponUpgradeScripts.Count != 0)
        {
            _weaponUpgradeTypeNamePopupSelectionProp.intValue = EditorGUILayout.Popup(new GUIContent("Upgrade Type"), _weaponUpgradeTypeNamePopupSelectionProp.intValue, _allWeaponUpgradeScripts.ToArray());
            _weaponUpgradeTypeNameProp.stringValue = _allWeaponUpgradeScripts[_weaponUpgradeTypeNamePopupSelectionProp.intValue];
        }
        else
        {
            EditorGUILayout.HelpBox("No weapon upgrade scripts were found", MessageType.Warning, false);
        }

        EditorGUILayout.PropertyField(_changedPropertiesProp, new GUIContent("Changed Properties"));

        int chosenWeaponForm = _applicableWeaponFormProp.enumValueIndex;
        var changedProperties = _changedPropertiesProp.objectReferenceValue as WeaponUpgrade;

        if (changedProperties != null)
        {
            int changedPropertiesWeaponForm = (int)changedProperties.ApplicableWeaponForm;
            if (chosenWeaponForm != changedPropertiesWeaponForm)
            {
                EditorGUILayout.HelpBox("The chosen weapon forms for the upgrade and the changed properties do not match", MessageType.Error, true);
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    public void FindWeaponUpgradeScripts()
    {
        _allWeaponUpgradeScripts.Add("None");

        foreach (Type type in
            Assembly.GetAssembly(typeof(WeaponUpgrade)).GetTypes()
            .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(WeaponUpgrade))))
        {
            _allWeaponUpgradeScripts.Add(type.FullName.Replace("Weapons.Player.Upgrades.", ""));
        }
    }
}
