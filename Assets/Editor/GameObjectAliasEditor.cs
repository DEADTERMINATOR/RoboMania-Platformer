﻿using UnityEngine;
using UnityEditor;

public class GameObjectAliasEditor : ScriptableObject
{
    [MenuItem("Tools/Alias/Do It in C#")]
    static void DoIt()
    {
        EditorUtility.DisplayDialog("MyTool", "Do It in C# !", "OK", "");
    }

    [MenuItem("GameObject/Alias/Make Alias", false, 10)]
    static void AddPoint(MenuCommand command)
    {
        GameObject go = (GameObject)command.context;
        if (go == null)
        {
            go = Selection.activeGameObject;
        }

        GameObject Alias1 = new GameObject(go.name+" (Alias)");
        Alias1.AddComponent<GameObjectAlias>().RealGameObject = go;
        Alias1.transform.parent = go.transform.parent;
        
    }

}