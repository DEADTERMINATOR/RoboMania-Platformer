﻿using UnityEngine;
using UnityEditor;
using Characters.Enemies;
using Characters.Enemies.Bosses.Handy;
using LevelSystem;
using System.Collections.Generic;
using System.Linq;
using System;
using LevelSystem.SateData;
using System.Collections;

public class LevelStateDebugWindow : EditorWindow
{
    public static UnityEngine.Object activeAreasource { get; private set; }
    private string filtterString = "";
    [MenuItem("Tools/LevelState/DebugVarWindow ")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(LevelStateDebugWindow));
    }

    void OnGUI()
    {
        if (GameMaster.GameManager.Instance)
        {
            
            GUILayout.BeginHorizontal();
            GUILayout.Label("Loaded Data Package :", EditorStyles.boldLabel);
            GUILayout.Label((GameMaster.GameManager.Instance.LoadedDatatpackage.Name));
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Level Loaded", EditorStyles.boldLabel);
            GUILayout.Label((Level.CurentLevel) ? "True" : "False");
            GUILayout.EndHorizontal();
            filtterString = EditorGUILayout.TextField("Key Contains :",filtterString);
            if (Level.CurentLevel)
            {
                if (Level.CurentLevel.LevelState != null)
                {
                    GUILayoutOption[] nullops = new GUILayoutOption[0];
                    GUILayout.Label("Level State Loaded", EditorStyles.boldLabel);
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("State Was loaded From Disk", EditorStyles.boldLabel);
                    GUILayout.Label((Level.CurentLevel.LevelState.WasDataLoaded) ? "True" : "False");
                    GUILayout.EndHorizontal();
                    GUILayout.Label("<--Vars-->", EditorStyles.boldLabel);
                    GUILayout.BeginVertical("Vars");
                    if (string.IsNullOrEmpty(filtterString))
                    {
                        for (int i =0; i < Level.CurentLevel.LevelState.StateData.Count; i++)
                        {
                            KeyValuePair<string, int> var = Level.CurentLevel.LevelState.StateData.ElementAt < KeyValuePair<string, int> > (i);
                            GUILayout.BeginHorizontal();
                            GUILayout.Label(string.Format("{0} : ", var.Key), EditorStyles.boldLabel);
                            
                            int newvalue = EditorGUILayout.IntField(var.Value);                   
                            
                            if(newvalue != var.Value)
                            {
                                Level.CurentLevel.LevelState.SetState(var.Key, Convert.ToInt32(newvalue));
                            }
                            EditorGUILayout.Toggle(LevelState.IntToBool(var.Value), nullops);
                            GUILayout.EndHorizontal();
                        }
                    }else
                    {
                        KeyValuePair<string, int>[] list = Level.CurentLevel.LevelState.StateData.Select(i => i).Where(i => i.Key.Contains(filtterString)).ToArray<KeyValuePair<string, int>>();
                        for (int i = 0; i < list.Length; i++)
                        {
                            KeyValuePair<string, int> var = list.ElementAt<KeyValuePair<string, int>>(i);
                            GUILayout.BeginHorizontal();
                            GUILayout.Label(string.Format("{0} : ", var.Key), EditorStyles.boldLabel);

                            int newvalue = EditorGUILayout.IntField(var.Value);

                            if (newvalue != var.Value)
                            {
                                Level.CurentLevel.LevelState.SetState(var.Key, Convert.ToInt32(newvalue));
                            }
                            EditorGUILayout.Toggle(LevelState.IntToBool(var.Value), nullops);
                            GUILayout.EndHorizontal();

                        }
                    }
                    GUILayout.EndVertical();
                }

            }
        }
        else
        {
            GUILayout.Label("Game not Running or starting up", EditorStyles.boldLabel);
        }
    }

    private void Update()
    {

    }
}