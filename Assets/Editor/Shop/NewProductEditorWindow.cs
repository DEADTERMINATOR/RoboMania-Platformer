﻿using UnityEngine;
using UnityEditor;
using UnityEditor.PackageManager.UI;
using System.Collections.Generic;
using UnityTool.Editor;
using System;
using System.Reflection.Emit;

namespace ItemShop
{
    public class NewProductEditorWindow : EditorWindow
    {

        private int _lastIndex = 0;
        private string _name;

        [MenuItem("Assets/Create/Shop/New Store Product")]
        static void DoIt()
        {
            EditorWindow.GetWindow(typeof(NewProductEditorWindow));
        }
        

        void OnGUI()
        {
            List<string> buyscriptList = EditorStaticTools.LoookUpListOfScripts<ShopBuyScript>();
            EditorGUILayout.BeginVertical("CN Box");
            GUILayoutOption[] nullOptions = { };
            _name = EditorGUILayout.TextField(new GUIContent("Name"), _name, nullOptions);
            _lastIndex = EditorGUILayout.Popup(_lastIndex, buyscriptList.ToArray());
            
            if (GUILayout.Button("Make New Product"))
            {

                Product prod = EditorStaticTools.CreateScriptableObjectAsset<Product>(_name+ ".Product");
                prod.BuyScript = (ShopBuyScript)EditorStaticTools.CreateScriptableObjectAsset(buyscriptList[_lastIndex], _name + ".ShopBuyScript");
            }
            EditorGUILayout.EndVertical();
        }
    }
}