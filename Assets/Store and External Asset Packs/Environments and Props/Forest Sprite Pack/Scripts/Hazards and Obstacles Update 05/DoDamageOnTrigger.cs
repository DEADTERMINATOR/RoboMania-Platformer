﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoDamageOnTrigger : MonoBehaviour
{
#pragma warning disable CS0649
    [SerializeField]
    float damageValue;
#pragma warning restore CS0649

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            collision.gameObject.SendMessage("ReceiveDamage", damageValue);
        }
    }
}
