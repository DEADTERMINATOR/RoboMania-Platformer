﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Kill : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Spike")
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}