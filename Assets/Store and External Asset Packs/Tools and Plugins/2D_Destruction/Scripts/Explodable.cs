﻿using UnityEngine;
using Unity.Collections;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using MovementEffects;
using System;
using Characters.Enemies;

//[RequireComponent(typeof(Rigidbody2D))]
public class Explodable : MonoBehaviour
{
    public System.Action<List<GameObject>> OnFragmentsGenerated;
    /// <summary>
    /// This is the id of the folder this will be saved into 
    /// </summary>
    [Tooltip("This sets the name of the sub directory the pieces are saved in")]
    public int ExplodableID;

    public Enemy AssosaitedEnmay;
    // public bool allowRuntimeFragmentation = false;
    public static string piecesFilePath = "Assets/Resources/Prefabs/Enemies/Destroyed/";

    public int extraPoints = 0;
    public int subshatterSteps = 0;

    [Range(0, 31)]
    public int fragmentLayer;
    public string sortingLayerName = "Debris";
    public bool randomizeOrderInLayer = false;
    public int minOrderInLayer = 0;
    public int maxOrderInLayer = 0;
    public int orderInLayer = 0;

    public float explosionForce = 1250;
    public bool isGroundEnemy = true;

    public Material material = null;

    public enum ShatterType
    {
        //Triangle,
        Voronoi
    };
    public ShatterType shatterType;
    public List<GameObject> fragments = new List<GameObject>();
    private List<List<Vector2>> polygons = new List<List<Vector2>>();

    /// <summary>
    /// Creates fragments if necessary and destroys original gameobject
    /// </summary>
    public void explode(int totalScrap)
    {
        /* //if fragments were not created before runtime then create them now
          if (fragments.Count == 0 && allowRuntimeFragmentation)
         {
             generateFragments(true, false, false, false);
         }*/
        //otherwise unparent and activate them

        int perPieceScrapValue = 0;
        int remainder = 0;

        if (totalScrap > 0)
        {
            perPieceScrapValue = Mathf.FloorToInt(totalScrap / fragments.Count);
            remainder = totalScrap - (perPieceScrapValue * fragments.Count);
        }

        foreach (GameObject frag in fragments)
        {
            frag.transform.parent = null;
            frag.transform.Find("Sprite").localPosition = Vector3.zero;
            frag.SetActive(true);

            ScrapPickUp scrapComponent = frag.GetComponent<ScrapPickUp>();
            if (scrapComponent == null)
            {
                 scrapComponent = frag.AddComponent<ScrapPickUp>();
            }
            if (scrapComponent != null)
            {
                scrapComponent.ScrapAmount = remainder-- > 0 ? perPieceScrapValue + 1 : perPieceScrapValue;
            }
            frag.GetComponent<Explode>().ApplyExplosionProperties();
        }


        //if fragments exist destroy the original
        if (fragments.Count > 0)
        {
            Destroy(gameObject);
        }
    }
    /// <summary>
    /// Creates fragments and then disables them
    /// </summary>
    public void fragmentInEditor()
    {
       
        string name = "";
        if(AssosaitedEnmay != null)
        {
            name = AssosaitedEnmay.name;
        }
        if (fragments.Count > 0)
        {
            deleteFragments(name);
        }
        generateFragments(false, true, true, false,null,Guid.Empty, name);
        setPolygonsForDrawing();
        foreach (GameObject frag in fragments)
        {
            frag.transform.parent = transform;
            frag.transform.Find("Sprite").localPosition = Vector3.zero;
            frag.SetActive(false);
        }
    }
    public void deleteFragments(string Name = "")
    {
        if(String.IsNullOrEmpty(Name))
        {
            Name = transform.root.name;
        }
        foreach (GameObject frag in fragments)
        {
            if (Application.isEditor)
            {
                DestroyImmediate(frag);
            }
            else
            {
                Destroy(frag, 1);
            }
        }
        fragments.Clear();
        polygons.Clear();
#if UNITY_EDITOR
        if (System.IO.Directory.Exists(piecesFilePath + Name))
        {
            foreach (string file in System.IO.Directory.GetFiles(piecesFilePath + Name))
            {
                string extension = System.IO.Path.GetExtension(file);
                if (extension == "" || extension == ".meta")

                    FileUtil.DeleteFileOrDirectory(file);
            }
        }
#endif
    }

    /// <summary>
    /// Turns Gameobject into multiple fragments
    /// </summary>
    public void generateFragments(bool isRuntime, bool saveToDisk, bool inEditor, bool piecesAreScrap, System.Action finalizeFunction = null, Guid loadingUnlockID = new Guid() , string StorageName = "")
    {
        switch (shatterType)
        {
            case ShatterType.Voronoi:
                if (isRuntime)
                {
                    if (transform.root.lossyScale != new Vector3(1, 1, 1))
                    {
                        Debug.LogError("Can not generateFragments when a prefab root has been scaled " + transform.root);
                    }

                    Coroutine handle = StartCoroutine(SpriteExploder.GenerateVoronoiPiecesCoroutine((pieces, inEd, isScrap) => { SetAttributesAndParentageForPieces(pieces, inEditor, isScrap); finalizeFunction?.Invoke(); }, gameObject, piecesAreScrap, extraPoints, subshatterSteps, material, Guid.Empty, StorageName));
                }
                else
                {
                    List<GameObject> voronoiPieces = SpriteExploder.GenerateVoronoiPieces(gameObject, piecesAreScrap, ExplodableID, extraPoints, subshatterSteps, material, loadingUnlockID, inEditor, saveToDisk, StorageName);
                    SetAttributesAndParentageForPieces(voronoiPieces, inEditor, piecesAreScrap);
                }
                break;
            default:
                Debug.Log("invalid choice");
                break;
        }
    }
    private void SetAttributesAndParentageForPieces(List<GameObject> pieces, bool inEditor, bool piecesAreScrap)
    {
        fragments = new List<GameObject>(pieces);

        //sets additional aspects of the fragments
        foreach (GameObject p in fragments)
        {
            if (p != null)
            {
                p.layer = fragmentLayer;

                Renderer pieceRenderer = p.GetComponentInChildren<Renderer>();
                pieceRenderer.sortingLayerName = sortingLayerName;

                if (randomizeOrderInLayer)
                {
                    pieceRenderer.sortingOrder = UnityEngine.Random.Range(minOrderInLayer, maxOrderInLayer + 1);
                }
                else
                {
                    pieceRenderer.sortingOrder = orderInLayer;
                }
            }
        }

        foreach (ExplodableAddon addon in GetComponents<ExplodableAddon>())
        {
            if (addon.enabled)
            {
                addon.OnFragmentsGenerated(fragments);
            }
        }

        if (!inEditor)
        {
            foreach (GameObject piece in fragments)
            {
                piece.transform.parent = transform;
                piece.SetActive(false);

                piece.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                piece.GetComponentInChildren<Renderer>().enabled = true;

                Explode pieceExplode = piece.GetComponent<Explode>();
                pieceExplode.ExplosionForce = explosionForce;
                //TODO: Put direction in here instead pieceExplode.IsGroundSprite = isGroundEnemy;

                if (piecesAreScrap)
                {
                    piece.AddComponent<ScrapPickUp>();
                }
            }
        }

        //Debug.Log("Fragmentation Complete");
    }

    private void setPolygonsForDrawing()
    {
        polygons.Clear();
        List<Vector2> polygon;

        foreach (GameObject frag in fragments)
        {
            polygon = new List<Vector2>();
            foreach (Vector2 point in frag.GetComponent<PolygonCollider2D>().points)
            {
                Vector2 offset = rotateAroundPivot((Vector2)frag.transform.position, (Vector2)transform.position, Quaternion.Inverse(transform.rotation)) - (Vector2)transform.position;
                offset.x /= transform.localScale.x;
                offset.y /= transform.localScale.y;
                polygon.Add(point + offset);
            }
            polygons.Add(polygon);
        }
    }
    private Vector2 rotateAroundPivot(Vector2 point, Vector2 pivot, Quaternion angle)
    {
        Vector2 dir = point - pivot;
        dir = angle * dir;
        point = dir + pivot;
        return point;
    }

    void OnDrawGizmos()
    {
        if (Application.isEditor)
        {
            if (polygons.Count == 0 && fragments.Count != 0)
            {
                setPolygonsForDrawing();
            }

            Gizmos.color = Color.blue;
            Gizmos.matrix = transform.localToWorldMatrix;
            Vector2 offset = (Vector2)transform.position * 0;
            foreach (List<Vector2> polygon in polygons)
            {
                for (int i = 0; i < polygon.Count; i++)
                {
                    if (i + 1 == polygon.Count)
                    {
                        Gizmos.DrawLine(polygon[i] + offset, polygon[0] + offset);
                    }
                    else
                    {
                        Gizmos.DrawLine(polygon[i] + offset, polygon[i + 1] + offset);
                    }
                }
            }
        }
    }
}
