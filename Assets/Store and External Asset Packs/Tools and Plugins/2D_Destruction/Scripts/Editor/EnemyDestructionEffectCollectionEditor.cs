﻿using UnityEngine;
using UnityEditor;
using Characters.Enemies;

[CustomEditor(typeof(EnemyDestructionEffectCollection))]
public class EnemyDestructionEffectCollectionEditor : Editor
{
    private SerializedProperty _explodableOptions;
    private SerializedProperty _sprite;
    private SerializedProperty _scale;
    private SerializedProperty _enemtRef;
    private int _listSize = 0;
    protected void OnEnable()
    {

        _explodableOptions = serializedObject.FindProperty("ExplodableOptions");
        _listSize = _explodableOptions.arraySize;
        _sprite = serializedObject.FindProperty("RefeanceSprite");
        _scale = serializedObject.FindProperty("SpawnScale");
        _enemtRef = serializedObject.FindProperty("TheEnemy");
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.ObjectField(_enemtRef);
        EditorGUILayout.ObjectField(_sprite);
        _scale.vector3Value = EditorGUILayout.Vector3Field("Spawn Scale", _scale.vector3Value);

        _listSize = EditorGUILayout.IntField("List Size", _listSize);

        if (_listSize != _explodableOptions.arraySize)
        {
            while (_listSize > _explodableOptions.arraySize)
            {
                _explodableOptions.InsertArrayElementAtIndex(_explodableOptions.arraySize);
            }
            while (_listSize < _explodableOptions.arraySize)
            {
                _explodableOptions.DeleteArrayElementAtIndex(_explodableOptions.arraySize - 1);
            }
        }
        Enemy Enemyref = _enemtRef.objectReferenceValue as Enemy;
        for (int i = 0; i < _listSize; i++)
        {
            SerializedProperty theObject = _explodableOptions.GetArrayElementAtIndex(i);
            if (theObject.objectReferenceValue != null && ( ((Explodable)theObject.objectReferenceValue).ExplodableID != i || ((Explodable)theObject.objectReferenceValue).AssosaitedEnmay != Enemyref))
            {
                ((Explodable)theObject.objectReferenceValue).ExplodableID = i;
                if (Enemyref != null)
                {
                    ((Explodable)theObject.objectReferenceValue).AssosaitedEnmay = Enemyref;
                }
            }
            EditorGUILayout.ObjectField(theObject);
        }

        if (GUILayout.Button("Generate Fragments for all"))
        {

            for (int i = 0; i < _listSize; i++)
            {
                SerializedProperty theObject = _explodableOptions.GetArrayElementAtIndex(i);
                Explodable value = theObject.objectReferenceValue as Explodable;
                string aasetpath = AssetDatabase.GetAssetPath(value);
                GameObject contentsRoot =   PrefabUtility.LoadPrefabContents(aasetpath);
                contentsRoot.GetComponent<Explodable>()?.fragmentInEditor();
                EditorUtility.SetDirty(value);
                // Save contents back to Prefab Asset and unload contents.
                PrefabUtility.SaveAsPrefabAsset(contentsRoot, aasetpath);
                PrefabUtility.UnloadPrefabContents(contentsRoot);

            }

        }


        if (GUILayout.Button("Copy Elm 1 in all and make prefab"))
        {
            SerializedProperty theSObject = _explodableOptions.GetArrayElementAtIndex(0);
            Explodable orignal = theSObject.objectReferenceValue as Explodable; 
            string orignalAsetpath = AssetDatabase.GetAssetPath(orignal);
            Debug.Log(orignalAsetpath);

            for (int i = 1; i < _listSize; i++)
            {
                string newAssetPath = orignalAsetpath.Replace(".prefab",string.Format(" {0} .prefab",i));
                Debug.Log(newAssetPath);
                AssetDatabase.CopyAsset(orignalAsetpath, newAssetPath);
                AssetDatabase.Refresh();
                Explodable  newCopy = AssetDatabase.LoadAssetAtPath(newAssetPath,typeof(Explodable)) as Explodable;
                SerializedProperty theObject = _explodableOptions.GetArrayElementAtIndex(i);
                theObject.objectReferenceValue = newCopy;

  

            }

        }
        serializedObject.ApplyModifiedProperties();
        serializedObject.Update();
    }
}
