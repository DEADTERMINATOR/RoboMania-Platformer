﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Characters.Enemies;

[CustomEditor(typeof(Explodable))]
public class ExplodableEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        Explodable myTarget = (Explodable)target;
        myTarget.AssosaitedEnmay = EditorGUILayout.ObjectField("Associated Enmey", myTarget.AssosaitedEnmay,typeof(Enemy),true) as Enemy;
        myTarget.shatterType = (Explodable.ShatterType)EditorGUILayout.EnumPopup("Shatter Type", myTarget.shatterType);
        myTarget.extraPoints = EditorGUILayout.IntField("Extra Points", myTarget.extraPoints);
        myTarget.subshatterSteps = EditorGUILayout.IntField("Subshatter Steps",myTarget.subshatterSteps);
        if (myTarget.subshatterSteps > 1)
        {
            EditorGUILayout.HelpBox("Use subshatter steps with caution! Too many will break performance!!! Don't recommend more than 1", MessageType.Warning);
        }

        myTarget.fragmentLayer = EditorGUILayout.IntSlider("Fragment Layer", myTarget.fragmentLayer, 0, 31);
        myTarget.sortingLayerName = EditorGUILayout.TextField("Sorting Layer", myTarget.sortingLayerName);
        myTarget.randomizeOrderInLayer = EditorGUILayout.Toggle("Randomize Order In Layer", myTarget.randomizeOrderInLayer);
        if (myTarget.randomizeOrderInLayer)
        {
            myTarget.minOrderInLayer = EditorGUILayout.IntField("Minimum Order in Layer", myTarget.minOrderInLayer);
            myTarget.maxOrderInLayer = EditorGUILayout.IntField("Maximum Order in Layer", myTarget.maxOrderInLayer);
        }
        else
        {
            myTarget.orderInLayer = EditorGUILayout.IntField("Order In Layer", myTarget.orderInLayer);
        }

        myTarget.explosionForce = EditorGUILayout.FloatField("Explosion Force", myTarget.explosionForce);
        myTarget.isGroundEnemy = EditorGUILayout.Toggle("Is Ground Enemy", myTarget.isGroundEnemy);

        myTarget.material = EditorGUILayout.ObjectField("Material", myTarget.material, typeof(Material), false) as Material;
        myTarget.ExplodableID = EditorGUILayout.IntField("ExplodableID", myTarget.ExplodableID);
        if (myTarget.GetComponent<PolygonCollider2D>() == null && myTarget.GetComponent<BoxCollider2D>() == null)
        {
            EditorGUILayout.HelpBox("You must add a BoxCollider2D or PolygonCollider2D to explode this sprite", MessageType.Warning);
        }
        else if (myTarget.gameObject.transform.root.lossyScale != new Vector3(1,1,1) )
        {
            EditorGUILayout.HelpBox("You must not scale the prefab root to generate fragments", MessageType.Warning);
        }
        else
        {
            if (GUILayout.Button("Generate Fragments"))
            {
                myTarget.fragmentInEditor();
                EditorUtility.SetDirty(myTarget);
            }
            if (GUILayout.Button("Destroy Fragments"))
            {
                myTarget.deleteFragments();
                EditorUtility.SetDirty(myTarget);
            }
        }
        
    }
}
