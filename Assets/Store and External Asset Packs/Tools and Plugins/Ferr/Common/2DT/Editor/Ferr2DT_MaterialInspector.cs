﻿using UnityEngine;
using UnityEditor;

using System;
using System.Reflection;
using System.Collections;

[CustomEditor(typeof(Ferr2DT_Material))]
public class Ferr2DT_MaterialInspector : Editor {
	private SerializedProperty _materialTerrainSettingsProp;
	private SerializedProperty _terrainSettingsBehavioursProp;
	//private SerializedProperty _terrainSettingsTemplateBehaviours;

	//private int _behavioursCount;

    private void OnEnable() {
		_materialTerrainSettingsProp = serializedObject.FindProperty("_materialTerrainSettings");
		_terrainSettingsBehavioursProp = _materialTerrainSettingsProp.FindPropertyRelative("_behavioursToAdd");

		//_terrainSettingsTemplateBehaviours = _materialTerrainSettingsProp.FindPropertyRelative("_templateBehaviours");
    }

    public override void OnInspectorGUI() {
		Undo.RecordObject(target, "Modified Terrain Material");

		//_behavioursCount = _terrainSettingsBehavioursProp.arraySize;

		IFerr2DTMaterial mat = target as IFerr2DTMaterial;
		Material         newMat;
		
		newMat = mat.edgeMaterial = (Material)EditorGUILayout.ObjectField("Edge Material", mat.edgeMaterial, typeof(Material), true);
		if (mat.edgeMaterial != newMat) {
			mat.edgeMaterial  = newMat;
			CheckMaterialMode(mat.edgeMaterial, TextureWrapMode.Clamp);
		}
		
		newMat = (Material)EditorGUILayout.ObjectField("Fill Material", mat.fillMaterial, typeof(Material), true);
		if (mat.fillMaterial != newMat) {
			mat.fillMaterial  = newMat;
			CheckMaterialMode(mat.fillMaterial, TextureWrapMode.Repeat);
		}

		serializedObject.Update();
		//EditorGUILayout.PropertyField(_materialTerrainSettingsProp, new GUIContent("Material Terrain Settings"));
		/*
		var terrainType = (Ferr2DTTerrainSettings.TerrainType)EditorGUILayout.EnumPopup("Terrain Type", mat.materialTerrainType.Terrain);
		if (terrainType != mat.materialTerrainType.Terrain) {
			mat.materialTerrainType.Terrain = terrainType;
        }
		*/

		//EditorGUI.BeginChangeCheck();
		EditorGUILayout.PropertyField(_terrainSettingsBehavioursProp, new GUIContent("Behaviours To Add"));

		/* MAYBE TODO: Revisit this. It's already cost me more time than it would actually save in the long run, but I'm curious if I could get it to work.
		if (EditorGUI.EndChangeCheck() && _terrainSettingsBehavioursProp.arraySize > 0) {
			if (mat.materialTerrainSettings.TemplateBehaviours != null)
			{
				Destroy(mat.materialTerrainSettings.TemplateBehaviours);
			}

			var templateGameObject = new GameObject();
			templateGameObject.hideFlags = HideFlags.HideInHierarchy;
			templateGameObject.SetActive(false);

			for (int i = 0; i < _terrainSettingsBehavioursProp.arraySize; ++i)
			{
				var script = _terrainSettingsBehavioursProp.GetArrayElementAtIndex(i);
				var scriptAsMonoScript = script.objectReferenceValue as MonoScript;
				if (scriptAsMonoScript != null)
				{
					var scriptType = Type.GetType(scriptAsMonoScript.name + ", Assembly-CSharp");
					var component = templateGameObject.AddComponent(scriptType);
					var scriptEditor = Editor.CreateEditor(component, typeof(AutomaticMovingPlatformEditor));
					scriptEditor.OnInspectorGUI();
				}
			}

			mat.materialTerrainSettings.TemplateBehaviours = templateGameObject;
		}
		*/

		serializedObject.ApplyModifiedProperties();
			
		Type window = Type.GetType("Ferr2DT_TerrainMaterialWindow");
		if (window == null) {
			EditorGUILayout.HelpBox("Ferr2D was not detected in the project! You must have Ferr2D installed to edit or use this material.", MessageType.Error);
			if (GUILayout.Button("Get Ferr2D")) {
				Application.OpenURL("https://www.assetstore.unity3d.com/en/#!/content/11653");
			}
		} else {
			if (mat.edgeMaterial == null) EditorGUILayout.HelpBox("Please add an edge material to enable the material editor!", MessageType.Warning);
			else if (window != null){
				MethodInfo show = window.GetMethod("Show", BindingFlags.Static | BindingFlags.Public);
				if (show == null) {
					EditorGUILayout.HelpBox("No window show method found!", MessageType.Error);
				} else if (GUILayout.Button("Open Material Editor")) {
					show.Invoke(null, new object[] {mat});
				}
			}
		}
	}
	
	static void CheckMaterialMode(Material aMat, TextureWrapMode aDesiredMode) {
		if (aMat != null && aMat.mainTexture != null && aMat.mainTexture.wrapMode != aDesiredMode) {
			if (EditorUtility.DisplayDialog("Ferr2D Terrain", "The Material's texture 'Wrap Mode' generally works best when set to "+aDesiredMode+"! Would you like this texture to be updated?", "Yes", "No")) {
				string          path = AssetDatabase.GetAssetPath(aMat.mainTexture);
				TextureImporter imp  = AssetImporter.GetAtPath   (path) as TextureImporter;
				if (imp != null) {
					imp.wrapMode = aDesiredMode;
					AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
				}
			}
		}
	}
}
