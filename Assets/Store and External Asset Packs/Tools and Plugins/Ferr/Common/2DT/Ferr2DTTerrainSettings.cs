﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Ferr2DTTerrainSettings
{
#if UNITY_EDITOR
    [SerializeField] List<MonoScript>    _behavioursToAdd;
    //[SerializeField] GameObject          _templateBehaviours;

    public List<MonoScript> BehavioursToAdd { get{return _behavioursToAdd;} }
    //public GameObject TemplateBehaviours { get{return _templateBehaviours;} set{_templateBehaviours = value;} }
#endif
}
