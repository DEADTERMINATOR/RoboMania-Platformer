﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "RobotSFX/Shiled"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		[PerRendererData] _RingTex ("Ring Texture", 2D) = "white" {}
		_alphaSFXTex("Alpha SFX Texture", 2D) = "white" {}

		_MainColor ("Main Color", Color) = (1, 1, 1, 1)
		_DamageColor("Dmaged Colour", Color) = (1, 1, 1, 1)

		_AlphaLimit ("Max Alpha Value", Range(0, 1)) = 0.5


		_ringColor ("Ring Color", Color) = (1,1,1,1)
		_outterAlpha ("Ring  Max Alpha (Not In Ues ATM)", Range(0, 1)) = 0.5
		_alphaDistorstTile("Alpha SFX Tile Amount", Range(1, 100)) = 2
		_alphaDestortionInflinace("Alpha SFX  Amount", Range(0, 1)) = 0.5
		_colorLerp  ("Main -> Damage Lerp Amount", Range(0, 1)) = 0
		_damageLerp("Damage Lerp ", Range(0, 1)) = 0
		_DamagFxSeed("Seed", Range(0, 1)) = 0

    }
	SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Cull Off
			Lighting Off
			ZWrite Off
			Blend One OneMinusSrcAlpha    // I added this line

			Pass
			{
				CGPROGRAM
				#include "UnitySprites.cginc"

				#pragma vertex vert_vct
				#pragma fragment frag
				#pragma multi_compile _ PIXELSNAP_ON
				#pragma multi_compile _ ETC1_EXTERNAL_ALPHA

				sampler2D _RingTex;
				sampler2D _TextuerTex, _alphaSFXTex;
				fixed4 _MainColor, _DamageColor, _ringColor;
				float _colorLerp,_outterAlpha, _AlphaLimit, _alphaDistorstTile, _alphaDestortionInflinace, _damageLerp, _DamagFxSeed;
				float4 _alphaSFXTex_ST;
				float4 _MainTex_ST;
				float4 _RingTex_ST;
				float4 _textOut;


				 struct vin_vct
				 {
					 float4 vertex : POSITION;
					 float4 color : COLOR;
					 float2 texcoord : TEXCOORD0;
					 float2 texcoord2 : TEXCOORD1;
					 float2 texcoord3 : TEXCOORD2;
				 };

				 struct v2f_vct
				 {
					 float4 vertex : POSITION;
					 fixed4 color : COLOR;
					 float2 texcoord : TEXCOORD0;
					 float2 texcoord2 : TEXCOORD1;
					 float2 texcoord3 : TEXCOORD2;
				 };


				 float2 tile(float2 _st, float _zoom) {
					 _st *= _zoom;
					 // _st.xy = _st.yx;
					  return frac(_st);
				  }
				  float impulse(float k, float x)
				  {
					  const float h = k * x;
					  return h * exp(1.0 - h);
				  }

				  float2 MoveUV(float2 uv, float _dir)
				  {

					 return uv + _dir * _Time;

				  }
				  float3 Generate_Voronoi_hash3(float2 p, float seed)
				  {
					  float3 q = float3(dot(p, float2(127.1, 311.7)),
						  dot(p, float2(269.5, 183.3)),
						  dot(p, float2(419.2, 371.9)));
					  return frac(sin(q) * 43758.5453 * seed);
				  }
				  float4 Generate_Voronoi(float2 uv, float size, float seed, float black)
				  {
					  float2 p = floor(uv*size);
					  float2 f = frac(uv*size);
					  float k = 1.0 + 63.0*pow(1.0, 4.0);
					  float va = 0.0;
					  float wt = 0.0;
					  for (int j = -2; j <= 2; j++)
					  {
						  for (int i = -2; i <= 2; i++)
						  {
							  float2 g = float2(float(i), float(j));
							  float3 o = Generate_Voronoi_hash3(p + g, seed)*float3(1.0, 1.0, 1.0);
							  float2 r = g - f + o.xy;
							  float d = dot(r, r);
							  float ww = pow(1.0 - smoothstep(0.0, 1.414, sqrt(d)), k);
							  va += o.z*ww;
							  wt += ww;
						  }
					  }
					  float4 result = saturate(va / wt);
					  result.a = saturate(result.a + black);
					  return result;
				  }
				  inline float RBFXmod(float x, float modu)
				  {
					  return x - floor(x * (1.0 / modu)) * modu;
				  }

				  float3 RBFXrainbow(float t)
				  {
					  t = RBFXmod(t, 1.0);
					  float tx = t * 8;
					  float r = clamp(tx - 4.0, 0.0, 1.0) + clamp(2.0 - tx, 0.0, 1.0);
					  float g = tx < 2.0 ? clamp(tx, 0.0, 1.0) : clamp(4.0 - tx, 0.0, 1.0);
					  float b = tx < 4.0 ? clamp(tx - 2.0, 0.0, 1.0) : clamp(6.0 - tx, 0.0, 1.0);
					  return float3(r, g, b);
				  }

				  float4 Plasma(float4 txt, float2 uv, float _Fade, float speed)
				  {
					  float _TimeX = _Time.y * speed;
					  float a = 1.1 + _TimeX * 2.25;
					  float b = 0.5 + _TimeX * 1.77;
					  float c = 8.4 + _TimeX * 1.58;
					  float d = 610 + _TimeX * 2.03;
					  float x1 = 2.0 * uv.x;
					  float n = sin(a + x1) + sin(b - x1) + sin(c + 2.0 * uv.y) + sin(d + 5.0 * uv.y);
					  n = RBFXmod(((5.0 + n) / 5.0), 1.0);
					  float4 nx = txt;
					  n += nx.r * 0.2 + nx.g * 0.4 + nx.b * 0.2;
					  float4 ret = float4(RBFXrainbow(n), txt.a);
					  return lerp(txt, ret, _Fade);
				  }
				  float DGFXr(float2 c, float seed)
				  {
					  return frac(43.*sin(c.x + 7.*c.y)*seed);
				  }

				  float DGFXn(float2 p, float seed)
				  {
					  float2 i = floor(p), w = p - i, j = float2 (1., 0.);
					  w = w * w*(3. - w - w);
					  return lerp(lerp(DGFXr(i, seed), DGFXr(i + j, seed), w.x), lerp(DGFXr(i + j.yx, seed), DGFXr(i + 1., seed), w.x), w.y);
				  }

				  float DGFXa(float2 p, float seed)
				  {
					  float m = 0., f = 2.;
					  for (int i = 0; i<9; i++) { m += DGFXn(f*p, seed) / f; f += f; }
					  return m;
				  }

				  float4 DesintegrationFX(float4 txt, float2 uv, float value, float seed)
				  {

					  float t = frac(value*0.9999);
					  float4 c = smoothstep(t / 1.2, t + .1, DGFXa(3.5*uv, seed));
					  c = txt * c;
					  c.r = lerp(c.r, c.r*(1 - c.a), value);
					  c.g = lerp(c.g, c.g*(1 - c.a), value);
					  c.b = lerp(c.b, c.b*(1 - c.a), value);
					  return c;
				  }

				  inline float Holo1mod(float x, float modu)
				  {
					  return x - floor(x * (1.0 / modu)) * modu;
				  }

				  inline float Holo1noise(sampler2D source, float2 p)
				  {
					  float _TimeX = _Time.y;
					  float sample = tex2D(source, float2(.2, 0.2*cos(_TimeX))*_TimeX*8. + p * 1.).x;
					  sample *= sample;
					  return sample;
				  }

				  inline float Holo1onOff(float a, float b, float c)
				  {
					  float _TimeX = _Time.y;
					  return step(c, sin(_TimeX + a * cos(_TimeX*b)));
				  }

				  float4 Hologram(float2 uv, sampler2D source, float value, float speed)
				  {
					  float alpha = tex2D(source, uv).a;
					  float _TimeX = _Time.y * speed;
					  float2 look = uv;
					  float window = 1. / (1. + 20.*(look.y - Holo1mod(_TimeX / 4., 1.))*(look.y - Holo1mod(_TimeX / 4., 1.)));
					  look.x = look.x + sin(look.y*30. + _TimeX) / (50.*value)*Holo1onOff(4., 4., .3)*(1. + cos(_TimeX*80.))*window;
					  float vShift = .4*Holo1onOff(2., 3., .9)*(sin(_TimeX)*sin(_TimeX*20.) + (0.5 + 0.1*sin(_TimeX*20.)*cos(_TimeX)));
					  look.y = Holo1mod(look.y + vShift, 1.);
					  float4 video = float4(0, 0, 0, 0);
					  float4 videox = tex2D(source, look);
					  video.r = tex2D(source, look - float2(.05, 0.)*Holo1onOff(2., 1.5, .9)).r;
					  video.g = videox.g;
					  video.b = tex2D(source, look + float2(.05, 0.)*Holo1onOff(2., 1.5, .9)).b;
					  video.a = videox.a;
					  video = video;
					  float vigAmt = 3. + .3*sin(_TimeX + 5.*cos(_TimeX*5.));
					  float vignette = (1. - vigAmt * (uv.y - .5)*(uv.y - .5))*(1. - vigAmt * (uv.x - .5)*(uv.x - .5));
					  float noi = Holo1noise(source, uv*float2(0.5, 1.) + float2(6., 3.))*value * 3;
					  float y = Holo1mod(uv.y*4. + _TimeX / 2. + sin(_TimeX + sin(_TimeX*0.63)), 1.);
					  float start = .5;
					  float end = .6;
					  float inside = step(start, y) - step(end, y);
					  float fact = (y - start) / (end - start)*inside;
					  float f1 = (1. - fact) * inside;
					  video += f1 * noi;
					  video += Holo1noise(source, uv*2.) / 2.;
					  video.r *= vignette;
					  video *= (12. + Holo1mod(uv.y*30. + _TimeX, 1.)) / 13.;
					  video.a = video.a + (frac(sin(dot(uv.xy*_TimeX, float2(12.9898, 78.233))) * 43758.5453))*.5;
					  video.a = (video.a*.3)*alpha*vignette * 2;
					  video.a *= 1.2;
					  video.a *= 1.2;
					  video = lerp(tex2D(source, uv), video, value);
					  return video;
				  }
				  float4 OperationBlend(float4 origin, float4 overlay, float blend)
				  {
					  float4 o = origin;
					  o.a = overlay.a + origin.a * (1 - overlay.a);
					  o.rgb = (overlay.rgb * overlay.a + origin.rgb * origin.a * (1 - overlay.a)) * (o.a + 0.0000001);
					  o.a = saturate(o.a);
					  o = lerp(origin, o, blend);
					  return o;
				  }

				 v2f_vct vert_vct(vin_vct v)
				 {
					 v2f_vct o;
					 o.vertex = UnityObjectToClipPos(v.vertex);
					 o.color = v.color;
					 o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
					 o.texcoord2 = TRANSFORM_TEX(v.texcoord2, _alphaSFXTex);
					 o.texcoord3 = TRANSFORM_TEX(v.texcoord3, _RingTex);
					 return o;
				 }

				 fixed4 frag(v2f_vct IN) : SV_Target
				 {
					 ////No colour no alpha 
					 float4 trasparent = float4(0.,0.,0.,0.);
					 ///get the base textuer info
					 fixed4 mainTexData = SampleSpriteTexture(IN.texcoord);
					 ///
					 fixed4 ringTexData = tex2D(_RingTex, IN.texcoord);
					 float2 MovingUVs = MoveUV(IN.texcoord, float2(0.09, 0));
					 ///Tile the DestoirtionSFX
					 float2 TilesSFXUV = tile(MovingUVs, _alphaDistorstTile);
					 //smaple the textuer with tiltes  UVS
					 float4 distorDAta = tex2D(_alphaSFXTex, TilesSFXUV);

					 float Scanline = step(0.01,frac(MovingUVs.y)) + step(0.005, frac(MovingUVs.y)) - step(0.0066, frac(MovingUVs.y)) + step(0.5, frac(MovingUVs.y)) - step(0.495, frac(MovingUVs.y));

					 float4 mainBlend;
					 ///Get the damge coolour from var 
					 float4 blendColour = lerp(_MainColor, _DamageColor, _colorLerp + (_SinTime / 4));
					 //mainTexData = lerp(Plasma(mainTexData,IN.texcoord, _damageLerp,3), mainTexData, _damageLerp);
					 ///The Main Textuer Alpha - 1-other
					 float alphaComp = clamp(mainTexData.a - (((1 - distorDAta.a) / 4) * _alphaDestortionInflinace),0, _AlphaLimit);
					 mainBlend.a = lerp(alphaComp, trasparent, Scanline);
					// mainBlend = Plasma(mainTexData, IN.texcoord, _damageLerp, 3);
					 mainBlend.rgb = lerp(blendColour, Plasma(mainTexData, IN.texcoord, 1, 3), _damageLerp) * alphaComp;

					 mainBlend = DesintegrationFX(mainBlend, IN.texcoord, clamp(0.0,0.6,_damageLerp), smoothstep(1, 0.5, _DamagFxSeed));
					 //mainBlend = lerp(trasparent, blendColour, mainTexData.a);
					 if (ringTexData.a > 0.7)
					 {
						 mainBlend.rgb = _ringColor * _outterAlpha;//set the color and alpha
						 mainBlend.a = _outterAlpha;//set the alpha value
					 }
					 _textOut = mainBlend;
					 return mainBlend;




				 }

				 ENDCG
			 }
			 /*pass
			 {
				 CGPROGRAM
				#include "UnitySprites.cginc"

				#pragma vertex vert_vct
				#pragma fragment frag
				#pragma multi_compile _ PIXELSNAP_ON
			#pragma multi_compile _ ETC1_EXTERNAL_ALPHA

					 sampler2D _RingTex;
				 sampler2D _TextuerTex, _alphaSFXTex;
				 fixed4 _MainColor, _DamageColor, _ringColor;
				 float _colorLerp, _outterAlpha, _AlphaLimit, _alphaDistorstTile, _alphaDestortionInflinace;
				 float4 _alphaSFXTex_ST;
				 float4 _MainTex_ST;
				 float4 _RingTex_ST;
				 float4 _textOut;

				 struct vin_vct
				 {
					 float4 vertex : POSITION;
					 float4 color : COLOR;
					 float2 texcoord : TEXCOORD0;
					 float2 texcoord2 : TEXCOORD1;
					 float2 texcoord3 : TEXCOORD2;
				 };

				 struct v2f_vct
				 {
					 float4 vertex : POSITION;
					 fixed4 color : COLOR;
					 float2 texcoord : TEXCOORD0;
					 float2 texcoord2 : TEXCOORD1;
					 float2 texcoord3 : TEXCOORD2;
				 };


				 float2 tile(float2 _st, float _zoom) {
					 _st *= _zoom;
					 // _st.xy = _st.yx;
					 return frac(_st);
				 }
				 float impulse(float k, float x)
				 {
					 const float h = k * x;
					 return h * exp(1.0 - h);
				 }

				 float2 MoveUV(float2 uv, float _dir)
				 {

					 return uv + _dir * _Time;

				 }



				 v2f_vct vert_vct(vin_vct v)
				 {
					 v2f_vct o;
					 o.vertex = UnityObjectToClipPos(v.vertex);
					 o.color = v.color;
					 o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
					 o.texcoord2 = TRANSFORM_TEX(v.texcoord2, _alphaSFXTex);
					 o.texcoord3 = TRANSFORM_TEX(v.texcoord3, _RingTex);
					 return o;
				 }

				 fixed4 frag(v2f_vct IN) : SV_Target
				 {
					 return _textOut;//float4(1.,1.,1.,1.);
				 }
				 ENDCG
			 }*/
		}
}