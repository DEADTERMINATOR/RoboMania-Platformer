﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sprites/Shiled"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_TextuerTex ("Textuer Texture", 2D) = "white" {}
		_RingTex ("Ring Texture", 2D) = "white" {}
		_ColorA ("ColorA", Color) = (1, 1, 1, 1)
        _ColorB ("ColorB", Color) = (0, 0, 0, 1)
        _Slide ("Slide", Range(0, 1)) = 0.5
		_Blend ("Blend", Range(0, 1)) = 0.5
		_BlendTextuer ("BlendTextuer", Range(0, 1)) = 0.5
		_AlphaLimit ("AlphaLimit", Range(0, 1)) = 0.5
		_Color ("Color", Color) = (1,1,1,1)
		_outterAlpha ("Outer Alpha", Range(0, 1)) = 0.5
		_Mix ("Mix", Range(0, 1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha    // I added this line

		Pass
		{
			CGPROGRAM
			#include "UnitySprites.cginc"

			#pragma vertex vert_vct
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#pragma multi_compile _ ETC1_EXTERNAL_ALPHA

			sampler2D _RingTex;
			sampler2D _TextuerTex;
			fixed4 _ColorA, _ColorB;
			float _Slide,_BlendTextuer,_Blend,_AlphaLimit,_Mix,_outterAlpha;
			float4 _TextuerTex_ST;
			float4 _MainTex_ST;
			float4 _RingTex_ST;


			 struct vin_vct 
             {
                 float4 vertex : POSITION;
                 float4 color : COLOR;
                 float2 texcoord : TEXCOORD0;
				 float2 texcoord2 : TEXCOORD1;
				 float2 texcoord3 : TEXCOORD2;
             };
 
             struct v2f_vct
             {
                 float4 vertex : POSITION;
                 fixed4 color : COLOR;
                 float2 texcoord : TEXCOORD0;
				 float2 texcoord2 : TEXCOORD1;
				 float2 texcoord3 : TEXCOORD2;
             };





			v2f_vct vert_vct(vin_vct v)
            {
				v2f_vct o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX (v.texcoord, _MainTex);
				o.texcoord2 = TRANSFORM_TEX (v.texcoord2, _TextuerTex);
				o.texcoord3 = TRANSFORM_TEX (v.texcoord3, _RingTex);
				return o;
            }

			fixed4 frag(v2f_vct IN) : SV_Target
			{
				//get the base textuer info
				fixed4 c = SampleSpriteTexture (IN.texcoord);
				//get the textuer textuer info
				fixed4 c1 = tex2D(_TextuerTex,IN.texcoord2);
				//calc T for the ridial gradiant 
				float t = length(IN.texcoord - float2(0.5, 0.5)) * 1.41421356237; // 1.141... = sqrt(2)
				//lep color
				if(_Mix ==0)
				{
					c.rgb =lerp(c.rgb,c1.rgb,_BlendTextuer);
					//c.rgb = lerp(lerp(_ColorA.rgb, _ColorB.rgb, t + (_Slide - 0.5) * 2),c.rgb,_Blend);
					c.rgb = (lerp(_ColorA.rgb, _ColorB.rgb, t + (_Slide - 0.5) * 2)+ c.rgb)*0.5;
				}
				else if(_Mix ==0.5)
				{
					c.rgb = (c.rgb +c1.rgb) *0.5;
					c.rgb = lerp(lerp(_ColorA.rgb, _ColorB.rgb, t + (_Slide - 0.5) * 2),c.rgb,_Blend);
				}
				else
				{
					c.rgb = (c.rgb +c1.rgb) *0.5;
					c.rgb = (lerp(_ColorA.rgb, _ColorB.rgb, t + (_Slide - 0.5) * 2)+ c.rgb)*0.5;
				}
				fixed4 ringData = tex2D(_RingTex,IN.texcoord3);
				// and apply alpha
				c.a*=_AlphaLimit;
				c.rgb *=c.a;
				// is the ring alpha present at %50
				if(ringData.a > 0.5)
				{
					c.rgb = _Color *_outterAlpha;//set the color and alpha
					c.a= _outterAlpha;//set the alpha value
				}
				
					
				

				return c;
			}

			ENDCG
		}
	}
}