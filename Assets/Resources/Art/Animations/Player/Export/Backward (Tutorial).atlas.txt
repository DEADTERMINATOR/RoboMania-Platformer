
Backward (Tutorial).png
size: 512,256
format: RGBA8888
filter: Linear,Linear
repeat: none
Backward/Foot
  rotate: true
  xy: 130, 2
  size: 42, 37
  orig: 42, 37
  offset: 0, 0
  index: -1
Backward/Foot (Step)
  rotate: true
  xy: 74, 2
  size: 42, 54
  orig: 42, 54
  offset: 0, 0
  index: -1
Backward/Head
  rotate: false
  xy: 2, 91
  size: 154, 156
  orig: 154, 156
  offset: 0, 0
  index: -1
Backward/Leg
  rotate: true
  xy: 71, 46
  size: 43, 85
  orig: 43, 85
  offset: 0, 0
  index: -1
Backward/Link
  rotate: false
  xy: 2, 5
  size: 70, 15
  orig: 70, 15
  offset: 0, 0
  index: -1
Backward/Pelvis
  rotate: false
  xy: 2, 22
  size: 67, 67
  orig: 67, 67
  offset: 0, 0
  index: -1
Backward/Torso + Neck
  rotate: true
  xy: 158, 88
  size: 159, 107
  orig: 159, 107
  offset: 0, 0
  index: -1
