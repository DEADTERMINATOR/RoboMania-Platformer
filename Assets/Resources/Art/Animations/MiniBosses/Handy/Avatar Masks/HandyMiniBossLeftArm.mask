%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: HandyMiniBossLeftArm
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Body-top
    m_Weight: 0
  - m_Path: Body-top/Body-bottom
    m_Weight: 0
  - m_Path: Body-top/Body-bottom/Legs
    m_Weight: 1
  - m_Path: Body-top/Body-bottom/Legs/Left
    m_Weight: 1
  - m_Path: Body-top/Body-bottom/Legs/Left/RightFoot (1)
    m_Weight: 1
  - m_Path: Body-top/Body-bottom/Legs/Left/RightFoot (1)/RightFoot (2)
    m_Weight: 1
  - m_Path: Body-top/Body-bottom/Legs/Left/RightFoot (1)/RightFoot (2)/RightShoulder_Joint
    m_Weight: 1
  - m_Path: Body-top/Body-bottom/Legs/Left/RightFoot (1)/RightFoot (2)/RightShoulder_Joint/RightFoot
      (3)
    m_Weight: 1
  - m_Path: Body-top/Body-bottom/Legs/Right
    m_Weight: 1
  - m_Path: Body-top/Body-bottom/Legs/Right/LeftFoot (1)
    m_Weight: 1
  - m_Path: Body-top/Body-bottom/Legs/Right/LeftFoot (1)/LeftFoot (2)
    m_Weight: 1
  - m_Path: Body-top/Body-bottom/Legs/Right/LeftFoot (1)/LeftFoot (2)/RightShoulder_Joint_Manual
    m_Weight: 1
  - m_Path: Body-top/Body-bottom/Legs/Right/LeftFoot (1)/LeftFoot (2)/RightShoulder_Joint_Manual/LeftFoot
      (3)
    m_Weight: 1
  - m_Path: Body-top/Head
    m_Weight: 0
  - m_Path: Body-top/Head/Head-green-glow
    m_Weight: 1
  - m_Path: Body-top/Arms
    m_Weight: 0
  - m_Path: Body-top/Arms/Right
    m_Weight: 0
  - m_Path: Body-top/Arms/Right/RightShoulder_Part1_Manual
    m_Weight: 1
  - m_Path: Body-top/Arms/Right/RightShoulder_Part1_Manual/RightShoulder_Part2
    m_Weight: 1
  - m_Path: Body-top/Arms/Right/RightShoulder_Part1_Manual/RightForarem
    m_Weight: 1
  - m_Path: Body-top/Arms/Right/RightShoulder_Part1_Manual/RightForarem/Rightlowerarm
    m_Weight: 1
  - m_Path: Body-top/Arms/Right/RightShoulder_Part1_Manual/RightForarem/Rightlowerarm/right_hand
      (5)
    m_Weight: 1
  - m_Path: Body-top/Arms/Right/RightShoulder_Part1_Manual/RightForarem/Rightlowerarm/right_hand
      (5)/LeftHand (3)
    m_Weight: 1
  - m_Path: Body-top/Arms/Left
    m_Weight: 1
  - m_Path: Body-top/Arms/Left/LeftShoulder
    m_Weight: 1
  - m_Path: Body-top/Arms/Left/LeftShoulder/LeftShoulder_Joint
    m_Weight: 1
  - m_Path: Body-top/Arms/Left/LeftShoulder/LeftShoulder_Joint/LeftHand (2)
    m_Weight: 1
  - m_Path: Body-top/Arms/Left/LeftShoulder/LeftShoulder_Joint/LeftHand (2)/left_hand
    m_Weight: 0
  - m_Path: Body-top/Wings
    m_Weight: 0
  - m_Path: Body-top/Wings/WingR
    m_Weight: 1
  - m_Path: Body-top/Wings/Wing (1)
    m_Weight: 1
  - m_Path: Bones
    m_Weight: 0
  - m_Path: Bones/Body
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop/Head
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop/LeftShoulder
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop/LeftShoulder/New bone
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop/LeftShoulder/New bone/New bone
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop/LeftShoulder/New bone/New bone/New bone
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop/RightShoulder
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop/RightShoulder/New bone
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop/RightShoulder/New bone/New bone
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop/RightShoulder/New bone/New bone/LeftHand Bone
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop/WingR
    m_Weight: 1
  - m_Path: Bones/Body/BodyTop/Wingl
    m_Weight: 1
  - m_Path: Bones/Body/LeftHip
    m_Weight: 1
  - m_Path: Bones/Body/LeftHip/LeftKeen
    m_Weight: 1
  - m_Path: Bones/Body/LeftHip/LeftKeen/LeftFoot
    m_Weight: 1
  - m_Path: Bones/Body/RightHip
    m_Weight: 1
  - m_Path: Bones/Body/RightHip/RightKnee
    m_Weight: 1
  - m_Path: Bones/Body/RightHip/RightKnee/RightFoot
    m_Weight: 1
  - m_Path: Bones/IK
    m_Weight: 0
  - m_Path: Bones/IK/Left Arm Ik CCD
    m_Weight: 0
  - m_Path: Bones/IK/Right Arm Ik CCD
    m_Weight: 0
