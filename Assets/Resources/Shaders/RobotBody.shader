﻿Shader "robomania/RobotBody"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_BumpMap("Bump Map", 2D) = "bump" {}
		_Hue("Hue",  Range(0,5)) = 0
		_Saturation("Saturation",  Range(0,50)) = 0
		//_Metallic("Metallic",  Range(0,1)) = 0

	}
	SubShader
	{
		Tags { 
		"RenderType" = "Transparent"
		"Queue" = "Transparent"
		}
		LOD 100
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
	
			CGPROGRAM
	#pragma surface surf Lambert alpha:fade  
			struct Input {
			float2 uv_MainTex: TEXCOORD0;
			float2 uv_BumpMap: TEXCOORD1;
			float2 uv_Emission: TEXCOORD3;
		};
			//#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _BumpMap;
			sampler2D _Emission;
			float _Hue;
			float _Saturation;
			//float _Metallic;

			// Converts the rgb value to hsv, where H's range is -1 to 5
			float3 rgb_to_hsv(float3 RGB)
			{
				float r = RGB.x;
				float g = RGB.y;
				float b = RGB.z;

				float minChannel = min(r, min(g, b));
				float maxChannel = max(r, max(g, b));

				float h = 0;
				float s = 0;
				float v = maxChannel;

				float delta = maxChannel - minChannel;

				if (delta != 0)
				{
					s = delta / v;

					if (r == v) h = (g - b) / delta;
					else if (g == v) h = 2 + (b - r) / delta;
					else if (b == v) h = 4 + (r - g) / delta;
				}

				return float3(h, s, v);
			}

			float3 hsv_to_rgb(float3 HSV)
			{
				float3 RGB = HSV.z;

				float h = HSV.x;
				float s = HSV.y;
				float v = HSV.z;

				float i = floor(h);
				float f = h - i;

				float p = (1.0 - s);
				float q = (1.0 - s * f);
				float t = (1.0 - s * (1 - f));

				if (i == 0) { RGB = float3(1, t, p); }
				else if (i == 1) { RGB = float3(q, 1, p); }
				else if (i == 2) { RGB = float3(p, 1, t); }
				else if (i == 3) { RGB = float3(p, q, 1); }
				else if (i == 4) { RGB = float3(t, p, 1); }
				else /* i == -1 */ { RGB = float3(1, p, q); }

				RGB *= v;

				return RGB;
			}

			void surf(Input IN, inout SurfaceOutput o) {

				o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
				//o.Emission = tex2D(_Emission, IN.uv_Emission);
				float4 col = tex2D(_MainTex, IN.uv_MainTex);

				float3 hsv = rgb_to_hsv(col.xyz);

				hsv.x += _Hue;
				// Put the hue back to the -1 to 5 range
				if (hsv.x > 5) { hsv.x -= 6.0; }

				hsv = hsv_to_rgb(hsv);
				float4 newColor = float4(hsv, col.w);

				float4 colorWithBrightnessAndContrast = newColor;

				float greyscale = dot(colorWithBrightnessAndContrast.rgb, float3(0.3, 0.59, 0.11));

				colorWithBrightnessAndContrast.rgb = lerp(greyscale, colorWithBrightnessAndContrast.rgb, col.a * (_Saturation + 1.0));
				o.Albedo = colorWithBrightnessAndContrast;
				o.Alpha = colorWithBrightnessAndContrast.a;
				
			}


			ENDCG
		
	}
}
