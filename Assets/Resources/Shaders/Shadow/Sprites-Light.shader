// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Sprites/Light"
{
	Properties
	{
		[PerRendererData] _ShadowTex ("Texture", 2D) = "white" {}
		[PerRendererData] _Color ("Color", Color) = (1,1,1,1)
		[PerRendererData] _LightPosition("LightPosition", Vector) = (0,0,1,0)
		[PerRendererData] _ShadowMapParams("ShadowMapParams", Vector) = (0,0,0,0)
		[PerRendererData] _Params2("Params2", Vector) = (0,0,0,0)
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Geometry" 
			"IgnoreProjector"="True" 
			"RenderType"="Opaque" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One One

		Pass
		{
		CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "ShadowMap1D.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				float4 modelPos : TEXCOORD1;
				float4 worldPos : TEXCOORD2;
			};
			
			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.modelPos = IN.vertex;
				OUT.worldPos = mul(unity_ObjectToWorld, IN.vertex);
				return OUT;
			}

			sampler2D 	_ShadowTex;
			float4 		_LightPosition;
			float4 		_ShadowMapParams;
			float4 		_Params2;
			fixed4 		_Color;
			
			float sample(float2 coord, float r) {
				return step(r, tex2D(_ShadowTex, coord).r);
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = _Color;
				float2 norm = IN.modelPos.xy * 2.0 - 1.0;
				float2 polar = ToPolar(IN.worldPos.xy,_LightPosition.xy);
				float r = length(norm);
				float shadow = SampleShadowTexturePCF(_ShadowTex,polar,_ShadowMapParams.x);
				if (polar.x == 1 && polar.y == 1)
				{
					return float4(0, 0, 1, 0.5);
				}
				if (polar.x == 0 && polar.y == 0)
				{
					return float4(0, 1, 1, 0.5);
				}
				if (shadow < 0.5f) {
					//;
					//c = float4(0,0,0,1);
					return float4(0, 1, 0, 0.5);
				}
				
				float distFalloff = max(0.0f,length(IN.worldPos.xy-_LightPosition.xy) - _Params2.w) * _Params2.z;
				distFalloff = clamp(distFalloff,0.0f,1.0f);
				distFalloff = pow(1.0f - distFalloff,_LightPosition.z);

				float angleFalloff = AngleDiff(polar.x, _Params2.x) / _Params2.y;
				angleFalloff = clamp(angleFalloff, 0.0f, 1.0f);
				angleFalloff = pow(1.0f - angleFalloff, _LightPosition.w);

				float LightValue = (distFalloff * angleFalloff) / 2;
				if (LightValue > 0)
				{				

					c.rgb *= LightValue;
				}
				else
				{
					clip(-1.0);
					return float4(1, 0, 0, 0.5);
				}

					return c;
			}
		ENDCG
		}
	}
}
