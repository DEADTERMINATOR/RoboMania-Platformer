﻿Shader "Test/WriteTest"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_customStencilValue("Custom Stencil texture value",  Range(0.01, 1)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
		   Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
					Cull Off
		ZWrite Off
			 CGPROGRAM



		// Includes
		//#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
		//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

		#pragma vertex vert
		#pragma fragment frag


		#include "UnityCG.cginc"

		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};

		sampler2D _MainTex;
		float4 _MainTex_ST;
		float _customStencilValue;

		v2f vert(appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv = TRANSFORM_TEX(v.uv, _MainTex);
			return o;
		}

		fixed4 frag(v2f i) : SV_Target
		{

				float4 _SampleTexture2D_20A557E6_RGBA_0 = tex2D(_MainTex, i.uv);
				return fixed4(_customStencilValue, _customStencilValue, _customStencilValue, _SampleTexture2D_20A557E6_RGBA_0.a);
		}

	ENDCG
        }
    }
}
