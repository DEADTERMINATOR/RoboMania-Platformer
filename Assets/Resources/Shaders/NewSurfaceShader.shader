﻿Shader "Custom/ObsticalStencil"
{
	Properties
	{

		_MainTex("Albedo (RGB)", 2D) = "white" {}

	}

		SubShader{
	
			Tags{ "RenderType" = "Transparent" "Queue" = "Transparent"}

			//Sewtical Settings
			Stencil{
			Ref 2
			Comp Always
			Pass Replace
			}
			Pass{

				 Blend SrcAlpha OneMinusSrcAlpha
				ZWrite Off

				CGPROGRAM
				#include "UnityCG.cginc"

				#pragma vertex vert
				#pragma fragment frag

				fixed4 _Color;

				struct appdata {
					float4 vertex : POSITION;
				};

				struct v2f {
					float4 position : SV_POSITION;
				};

				v2f vert(appdata v) {
					v2f o;
					//calculate the position in clip space to render the object
					o.position = UnityObjectToClipPos(v.vertex);
					return o;
				}

				fixed4 frag(v2f i) : SV_TARGET{
					//Return the color the Object is rendered in
					return 0;
				}

				ENDCG
			}
		}
}
