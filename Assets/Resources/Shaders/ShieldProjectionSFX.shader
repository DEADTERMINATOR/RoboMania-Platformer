﻿Shader "RoboSFX/ShieldProjectionSFX"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_TintColor("Tint",Color)  = (1, 1, 1, 1)
		_TextMoveSpeed("Move Speed ", Float) = 0.4
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent"
				"Queue" = "Transparent" }
        LOD 100
		Blend SrcAlpha OneMinusSrcAlpha
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag


            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _TintColor;
			float _TextMoveSpeed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				///Move the UV with Cos(Time)
				i.uv += _TextMoveSpeed *_Time.x;
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
				//col.a = col.a *_SinTime;
				
				col.rgb =  _TintColor.rgb;
				col.a =  col.a - clamp((_SinTime/4),0,0.74);
				col.rgb *= col.a;
				
                
                return col;
            }
            ENDCG
        }
    }
}
