// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "RoboSFX/BlobShadow"
{
	Properties
	{
		_MainText("_MainText", 2D) = "white" {}
		_Alpha("Alpha", Float) = 0
		_Float0("Float 0", Range( 0 , 1)) = 0
		_Float1("Float 1", Range( 0 , 1)) = 0.5294118
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Unlit alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
			float4 screenPos;
		};

		uniform sampler2D _MainText;
		uniform float4 _MainText_ST;
		uniform float _Float0;
		uniform float _Float1;
		uniform sampler2D ObsticalPass;
		uniform float _Alpha;


		inline float4 ASE_ComputeGrabScreenPos( float4 pos )
		{
			#if UNITY_UV_STARTS_AT_TOP
			float scale = -1.0;
			#else
			float scale = 1.0;
			#endif
			float4 o = pos;
			o.y = pos.w * 0.5f;
			o.y = ( pos.y - o.y ) * _ProjectionParams.x * scale + o.y;
			return o;
		}


		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 uv0_MainText = i.uv_texcoord * _MainText_ST.xy + _MainText_ST.zw;
			float4 tex2DNode2 = tex2D( _MainText, uv0_MainText );
			clip( ( step( uv0_MainText.x , _Float0 ) - step( ( 1.0 - uv0_MainText.x ) , _Float1 ) ) - 0.5);
			o.Emission = tex2DNode2.rgb;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_grabScreenPos = ASE_ComputeGrabScreenPos( ase_screenPos );
			float4 ase_grabScreenPosNorm = ase_grabScreenPos / ase_grabScreenPos.w;
			float4 tex2DNode33 = tex2D( ObsticalPass, ase_grabScreenPosNorm.xy );
			float4 appendResult65 = (float4(tex2DNode33.r , tex2DNode33.g , tex2DNode33.b , 0.0));
			float4 color8 = IsGammaSpace() ? float4(0,0,0,0) : float4(0,0,0,0);
			float4 temp_cast_4 = (tex2DNode2.a).xxxx;
			float4 lerpResult5 = lerp( (( appendResult65 == float4( float3(1,1,1) , 0.0 ) ) ? color8 :  temp_cast_4 ) , color8 , _Alpha);
			o.Alpha = lerpResult5.r;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16400
-1913;212;1906;1051;874.4724;450.1645;1;True;True
Node;AmplifyShaderEditor.TexturePropertyNode;3;-1487.676,-270.1342;Float;True;Property;_MainText;_MainText;0;0;Create;True;0;0;False;0;None;6f17b19ef746f6443863641f6a1907e0;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;4;-1237.535,-116.207;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GrabScreenPosition;63;-1554.764,898.9886;Float;False;0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;32;-1308.982,715.3557;Float;True;Global;ObsticalPass;ObsticalPass;4;0;Create;True;0;0;False;0;None;None;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-880.3646,-711.9202;Float;False;Property;_Float1;Float 1;3;0;Create;True;0;0;False;0;0.5294118;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;15;-763.6616,-601.4128;Float;False;Property;_Float0;Float 0;2;0;Create;True;0;0;False;0;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;21;-571.8181,-370.6297;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;33;-963.5189,524.5284;Float;True;Property;_TextureSample1;Texture Sample 1;4;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector3Node;59;-378.4761,580.9264;Float;False;Constant;_Vector0;Vector 0;5;0;Create;True;0;0;False;0;1,1,1;1,1,1;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.StepOpNode;19;-119.8766,-707.185;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;65;-628.1318,494.7131;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;2;-942.2331,-279.9762;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;8;-1088.145,336.8216;Float;False;Constant;_AlphaFill;AlphaFill;2;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StepOpNode;18;-464.8183,-635.106;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;27;-172.8477,-458.4573;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareEqual;56;-256.6585,205.3311;Float;True;4;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;2;COLOR;0,0,0,0;False;3;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-1657.088,54.44695;Float;True;Property;_Alpha;Alpha;1;0;Create;True;0;0;False;0;0;0.001374817;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SurfaceDepthNode;28;-1237.489,-1161.815;Float;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;5;-111.4386,-146.2946;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ComputeScreenPosHlpNode;62;-1235.089,488.4224;Float;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;40;-1797.25,722.6855;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.UnityObjToClipPosHlpNode;61;-1453.973,460.3671;Float;False;1;0;FLOAT3;0,0,0;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClipNode;13;33.104,-353.9951;Float;False;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.PosVertexDataNode;60;-1743.973,461.3671;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;11;281.2645,-123.0532;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;RoboSFX/BlobShadow;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;6;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;4;2;3;0
WireConnection;21;0;4;1
WireConnection;33;0;32;0
WireConnection;33;1;63;0
WireConnection;19;0;21;0
WireConnection;19;1;20;0
WireConnection;65;0;33;1
WireConnection;65;1;33;2
WireConnection;65;2;33;3
WireConnection;2;0;3;0
WireConnection;2;1;4;0
WireConnection;18;0;4;1
WireConnection;18;1;15;0
WireConnection;27;0;18;0
WireConnection;27;1;19;0
WireConnection;56;0;65;0
WireConnection;56;1;59;0
WireConnection;56;2;8;0
WireConnection;56;3;2;4
WireConnection;5;0;56;0
WireConnection;5;1;8;0
WireConnection;5;2;6;0
WireConnection;62;0;61;0
WireConnection;61;0;60;0
WireConnection;13;0;2;0
WireConnection;13;1;27;0
WireConnection;11;2;13;0
WireConnection;11;9;5;0
ASEEND*/
//CHKSM=4820B17486CF7925CA3705576FAC0919A389B953