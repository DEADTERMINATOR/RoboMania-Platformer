// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ToonTest"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		_PosterizePower("Posterize Power", Range( 0 , 22)) = 2
		_Contrast("Contrast", Range( 0 , 3)) = 1
		_Texture0("Texture 0", 2D) = "bump" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Off
		CGPROGRAM
		#pragma target 4.6
		#pragma surface surf Lambert alpha:fade keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog nometa noforwardadd 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Texture0;
		uniform float4 _Texture0_ST;
		uniform float _Contrast;
		uniform float _PosterizePower;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;


		float4 CalculateContrast( float contrastValue, float4 colorTarget )
		{
			float t = 0.5 * ( 1.0 - contrastValue );
			return mul( float4x4( contrastValue,0,0,t, 0,contrastValue,0,t, 0,0,contrastValue,t, 0,0,0,1 ), colorTarget );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 uv_Texture0 = i.uv_texcoord * _Texture0_ST.xy + _Texture0_ST.zw;
			o.Normal = tex2D( _Texture0, uv_Texture0 ).rgb;
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 tex2DNode3 = tex2D( _MainTex, uv_MainTex );
			float div2=256.0/float((int)_PosterizePower);
			float4 posterize2 = ( floor( tex2DNode3 * div2 ) / div2 );
			o.Albedo = CalculateContrast(_Contrast,posterize2).rgb;
			o.Alpha = tex2DNode3.a;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16400
-1913;212;1906;1051;3277.968;2141.535;3.050978;True;True
Node;AmplifyShaderEditor.TexturePropertyNode;1;-1705.086,-126.3563;Float;True;Property;_MainTex;MainTex;0;0;Create;True;0;0;False;0;4657480014d71ef47909918d91b6ec73;0807d4d298b824140afb0953fe3e15f4;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-1040.608,-356.5517;Float;False;Property;_PosterizePower;Posterize Power;1;0;Create;True;0;0;False;0;2;15.71;0;22;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;3;-1229.258,-157.3476;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PosterizeNode;2;-561.6542,-266.6191;Float;False;75;2;1;COLOR;0,0,0,0;False;0;INT;75;False;1;COLOR;0
Node;AmplifyShaderEditor.TexturePropertyNode;15;-1762.656,176.5004;Float;True;Property;_Texture0;Texture 0;3;0;Create;True;0;0;False;0;None;1baad8fe21dd2fa47b3817c3736156dd;True;bump;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-947.778,-459.3335;Float;False;Property;_Contrast;Contrast;2;0;Create;True;0;0;False;0;1;0.95;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;13;-336.778,-295.3335;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;5;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;16;-1235.654,128.8325;Float;True;Property;_TextureSample1;Texture Sample 1;5;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;657.1247,-37.97109;Float;False;True;6;Float;ASEMaterialInspector;0;0;Lambert;ToonTest;False;False;False;False;True;True;True;True;True;True;True;True;False;False;True;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;1;15;10;25;False;0.28;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;5;0,0,0,0;VertexOffset;False;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;3;0;1;0
WireConnection;2;1;3;0
WireConnection;2;0;5;0
WireConnection;13;1;2;0
WireConnection;13;0;14;0
WireConnection;16;0;15;0
WireConnection;0;0;13;0
WireConnection;0;1;16;0
WireConnection;0;9;3;4
ASEEND*/
//CHKSM=E09FCF92EC8488E7C82FCACDDD247DC50939F055