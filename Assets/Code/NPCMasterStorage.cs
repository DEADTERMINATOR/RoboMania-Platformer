﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NPCMasterStorage 
{
    private Dictionary<string, GameObject> _data = new Dictionary<string, GameObject>();
  

    public void RegisterNPC(string id ,GameObject gameObject)
    {
        if (!String.IsNullOrEmpty(id) && !_data.ContainsKey(id))
        {
           // Debug.Log("Adding Key" + id );
            _data.Add(id, gameObject);
        }
        else
        {
           //Debug.Log("Bad add Key" + id + " OBJ :" + gameObject.name);
        }
    }
    public bool HasID(string id)
    {
        return _data.ContainsKey(id);
    }
    public GameObject GetNPC(string id )
    {
        if (!String.IsNullOrEmpty(id) &&_data.ContainsKey(id))
        {
            //Debug.Log("geting Key" + id);
            return _data[id];
        }
        return null;
    }

}
