﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MagicTileTest : MonoBehaviour {

    public Tilemap tilemap;
    public Vector3 data;
    public TileMapPointPicker tilePoint;
    public Tile testTile;
    // Use this for initialization
    void Start () {
		
	}

    
    private void OnTriggerEnter2D(Collider2D collision)
    {

        Vector3Int pos = tilemap.WorldToCell(tilePoint.wroldPoint);
        
        tilemap.SetTileFlags(pos, TileFlags.None);
        tilemap.SetTile(pos, null);
    }
    // Update is called once per frame
    void Update () {
		
	}
}
