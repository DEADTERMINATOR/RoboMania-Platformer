﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMapPointPicker : MonoBehaviour {
    public Vector3 wroldPoint { get { return gameObject.transform.position; } }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 0.5f);
        Gizmos.color = Color.red;
        float size = 0.3f;
        Vector3 globalWaypointPos = transform.position;
        Gizmos.DrawLine(globalWaypointPos - Vector3.up * size, gameObject.transform.position + Vector3.up * size);
        Gizmos.DrawLine(globalWaypointPos - Vector3.left * size, gameObject.transform.position + Vector3.left * size);


    }
}
