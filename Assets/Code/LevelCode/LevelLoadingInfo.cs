﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
///This holds the Info need to load a Level
/// </summary>
public class LevelLoadingInfo : MonoBehaviour
{

    /// <summary>
    /// The name of the level-to-load's master scene.
    /// </summary>
    public string MasterSceneName;
    /// <summary>
    /// A list of scenes in the Level for debug loading
    /// </summary>
    public List<string> SceneList;
    // Use this for initialization

    /// <summary>
    /// The name of the scene that represents the beginning of the level
    /// </summary>
    public string LevelStartingScene;
    /// <summary>
    /// The name used in the Menu
    /// </summary>
    public string MenuDisplayName;

    public bool LevelSystem2 = false;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
