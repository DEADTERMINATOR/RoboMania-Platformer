﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FallingRock : MonoBehaviour, IDamageGiver
{

    private bool loaded = true;
    private new Rigidbody2D rigidbody2D;
    /// <summary>
    /// How many seconds till till a low veclity check is alowed
    /// </summary>
    public float Timeout = 0.5f;

    /// <summary>
    /// The min y veclity used to stop dmage should be nagative
    /// </summary>
    public float yVelocityMin = -0.05f;
    /// <summary>
    /// How long the can be alive in the world
    /// </summary>
    public float aliveTime = 5.5f;
    /// <summary>
    /// The base amount for damage the aculty fdamage applied is the base+ ABS(velocity.y)
    /// </summary>
    public int BaseDamageAmount = 6;

    private float time = 0;
    

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (time >= Timeout && rigidbody2D.velocity.y >= yVelocityMin)
            loaded = false;
        else
        {
            if (loaded)
            {
                IDamageReceiver thing = collision.gameObject.GetComponent<IDamageReceiver>();
                if (thing != null)
                {
                    thing.TakeDamage(this, Mathf.Abs(BaseDamageAmount * rigidbody2D.velocity.y), GlobalData.DamageType.ENVIROMENT, transform.position);
                    loaded = false;
                }
            }
        }
    }
    // Use this for initialization
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        time += Time.deltaTime;
        if (time >= aliveTime)
        {
            StartCoroutine(this.RespawnPlatform());

        }
    }


    /// <summary>
    /// A co-routine that respawns the platform by re-scaling it from a scale of 0 to it's original scale.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> RespawnPlatform()
    {
        float percentScalingComplete = 0f;
        while (percentScalingComplete < 1f)
        {
            percentScalingComplete += Time.deltaTime / 0.5f;
            transform.localScale = new Vector3(Mathf.Lerp(1, 0, percentScalingComplete), Mathf.Lerp(1, 0, percentScalingComplete), Mathf.Lerp(1, 1, percentScalingComplete));
            yield return 0f;
        }

        //BUGFIX: Destroy 
        //dgameObject.SetActive(false);
        Destroy(gameObject);
        yield return 0f;
    }
}
