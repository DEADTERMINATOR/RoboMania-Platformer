﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LevelSystem.SateData;
using LevelSystem;
public class BackgroundManager : MonoBehaviour
{

    /// <summary>
    /// The String key of the active back ground;
    /// </summary>
    private  LevelStateInt _Index;

    public static int Activeindex
    {
        get
        {
            if (Instance)
            {
                return Instance._Index;
            }
            return -1;
        }
    }
    /// <summary>
    /// The collections of backgrounds index by a number
    /// </summary>
    public ParallaxerManager[] backgournds;

    /// <summary>
    /// a self managed ref 
    /// </summary>
    public static BackgroundManager Instance { get; private set; }

    public OverlayFader FadeOverFX;
    public static OverlayFader OverlayFader { get { return Instance.FadeOverFX; } }
    /// <summary>
    /// A static interface to get the self assigned ref and call ChnageBackGroundWithKey NOTE: ChnageBackGroundWithKey can be called if you have a ref to the manager
    /// </summary>
    /// <param name="key"></param>
    public static void TryAndChnageBackGroundWithKey(int key)
    {
        if (Instance)
        {
            Instance.ChnageBackGroundWithKey(key);
        }
        return;
    }

    public void ChnageBackGroundWithKey(int key)
    {
        if (key != _Index && key > -1 && key < backgournds.Length)
        {
            backgournds[_Index].gameObject.SetActive(false);
            backgournds[key].gameObject.SetActive(true);
            backgournds[key].ResetAll();
           _Index.Value = key;
        }
    }

    // Use this for initialization
    void Start()
    {
        _Index = LevelState.Instance.LoadLevelStateInt("BackgroundIndex", 0);
        //hold the latest ref to a background;
        Instance = this;

        for (int i = 0; i < backgournds.Length; i++)
        {
            backgournds[i].gameObject.SetActive(false);
        }

        if (_Index >= 0 && _Index < backgournds.Length)
        {
            backgournds[_Index].gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    ~BackgroundManager()
    {
        //cleanse up the ref if the OBJ is decontructed
        if (Instance == this)
            Instance = null;
    }
}
