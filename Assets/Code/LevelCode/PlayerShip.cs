﻿using UnityEngine;
using System.Collections;

public class PlayerShip : MonoBehaviour, IActivatable
{
    /// <summary>
    /// The name of the secen to sen the user to
    /// </summary>
    public string ReturnToScene;
    /// <summary>
    /// The 2d Collider that block the player from coliding with the ship
    /// </summary>
    public Collider2D ShipCollider2D;
    /// <summary>
    /// The activation zone 
    /// </summary>
    public HasTagInTrigger ActivationZoneCollider;
    /// <summary>
    /// The parent partical system that is the whole FX
    /// </summary>
    public ParticleSystem BeemParticleSystem;
    /// <summary>
    /// The been color
    /// </summary>
    public Color32 mainColor;
    /// <summary>
    /// The color of the active beem
    /// </summary>
    public Color32 activeColor;

    public bool Active { get { return _run; } }

    private bool _run = false;


    void Start()
    {
        SetColorOnParticalSystem(mainColor);
    }

    void Update()
    {
        if (ActivationZoneCollider.IsInTrigger)
        {
            if (BeemParticleSystem.isStopped)
                BeemParticleSystem.Play();
        }
        else
        {
            if (BeemParticleSystem.isPlaying)
                BeemParticleSystem.Stop();
        }
    }


    public void Activate(GameObject activator)
    {
        GlobalData.Player.NotifyEnteringShip();

        //Disable the ship coloider so the player can me moved behind the ship
        ShipCollider2D.enabled = false;

        //set the beem colour
        SetColorOnParticalSystem(activeColor);

        // play movment 
        StartCoroutine("MovePlayer");

        _run = true;
    }

    public void Deactivate(GameObject activator) { }

    private IEnumerator MovePlayer()
    {
        GlobalData.Player.ZeroOutAllVelocities();
        yield return new WaitForSeconds(0.5f);

        GlobalData.Player.GravityScale = 0;
        GlobalData.Player.SetVelocity(new Vector2(0, 3), MoveableObject.OBJECT_VELOCITY_COMPONENT);
        yield return new WaitForSeconds(1.35f);

        GameMaster.GameManager.Instance.LoadLevelByIndex(0);
    }
    

    private void SetColorOnParticalSystem(Color32 color)
    {
        foreach (ParticleSystem ps in BeemParticleSystem.gameObject.GetComponentsInChildren<ParticleSystem>())
        {
            var startColor = ps.main.startColor;
            startColor.color = color;
        }
    }
}
