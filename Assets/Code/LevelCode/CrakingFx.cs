﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrakingFx : MonoBehaviour {

    public List<Sprite> layers;
    private int activeFXLayer = -1;
    protected SpriteRenderer spriteRenderer;


	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();

    }
	public void IncrmentFX()
    {
        if(activeFXLayer <= layers.Count-1)
        {
            activeFXLayer++;
            spriteRenderer.sprite = layers[activeFXLayer];
        }
        
    }
	// Update is called once per frame
	void Update () {
		
	}
}
