﻿using UnityEngine;
using System.Collections;
public enum WayPointEditorIcons { NoIcon, skull, list, disk, unload }
public class EditWayPointGameObject : MonoBehaviour
{
    public WayPointEditorIcons icon = WayPointEditorIcons.NoIcon;
    public Color FillColour = Color.yellow;
    public int Index;
    private void OnDrawGizmos()
    {
        Gizmos.color = FillColour;
        Gizmos.DrawSphere(transform.position, 0.5f);
        Gizmos.color = Color.gray;
        //float size = 0.3f;
        Vector3 globalWaypointPos = transform.position;
        Gizmos.DrawSphere(transform.position, 0.2f);
        if (icon != WayPointEditorIcons.NoIcon)
        {
            Gizmos.DrawIcon(transform.position, icon + ".png", false);
        }

    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, 0.5f);
        Gizmos.color = Color.black;
       // float size = 0.3f;
        Vector3 globalWaypointPos = transform.position;
        /*Gizmos.DrawLine(globalWaypointPos - Vector3.up * size, globalWaypointPos + Vector3.up * size);
        Gizmos.DrawLine(globalWaypointPos - Vector3.left * size, globalWaypointPos + Vector3.left * size);*/
        Gizmos.DrawSphere(transform.position, 0.2f);
        if (icon != WayPointEditorIcons.NoIcon)
        {
            Gizmos.DrawIcon(transform.position, icon + ".png", false);
        }
    }

   
}
