﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IActivatable
{
    /// <summary>
    /// Whether the object is currently active.
    /// </summary>
    bool Active { get; }

    /// <summary>
    /// Activates the object.
    /// </summary>
    /// <param name="activator"></param>
    void Activate(GameObject activator);

    /// <summary>
    /// Activates the object.
    /// </summary>
    void Deactivate(GameObject activator);
}
