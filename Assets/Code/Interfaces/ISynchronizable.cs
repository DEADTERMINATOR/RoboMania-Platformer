﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISynchronizable
{
	bool Safe { get; }
	bool IsTransitioning { get; }

	void SynchronizableUpdate();

	void GoSafe();
	void GoUnsafe();
}
