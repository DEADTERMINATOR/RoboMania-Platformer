﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Used to indicate object types that are capable of giving damage.
/// </summary>
public interface IDamageGiver
{
  
}
