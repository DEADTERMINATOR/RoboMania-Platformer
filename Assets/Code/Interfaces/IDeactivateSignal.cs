﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateSignalArgs : EventArgs
{
    public IActivatable ObjToDeactivate;

    public DeactivateSignalArgs(IActivatable obj)
    {
        ObjToDeactivate = obj;
    }
}

public interface IDeactivateSignal
{
    event EventHandler SendDeactivationSignal;
}
