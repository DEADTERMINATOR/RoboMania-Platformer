﻿using UnityEngine;
using System.Collections;

public interface IPersistable
{
     SaveData Save();
     bool Load(SaveData data);
}
