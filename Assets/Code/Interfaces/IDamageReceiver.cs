﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Used to indicate object types that are capable of receiving damage.
/// </summary>
public interface IDamageReceiver
{
    
    void TakeDamage(IDamageGiver giver, float amount, GlobalData.DamageType type , Vector3 hitPoint );
}
