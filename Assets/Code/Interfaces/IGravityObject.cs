﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGravityObject
{
    void ChangeGravityType(GlobalData.GravityType gravityType);
}
