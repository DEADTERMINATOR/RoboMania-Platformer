﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IActivatableByMultiple : IActivatable
{
    int ActiveCount { get; }
}
