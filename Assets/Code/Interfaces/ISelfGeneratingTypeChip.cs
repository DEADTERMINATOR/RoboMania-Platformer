﻿
public interface ISelfGeneratingTypeChip
{
    void Generate(Rarity rarity);
}

