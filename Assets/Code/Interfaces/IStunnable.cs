﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStunnable
{
    bool IsStunned { get; }

    void SetStunned(float stunPower, IDamageGiver giver, GameObject owner);
}
