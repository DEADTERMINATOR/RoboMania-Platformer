﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void ActivateEvent();

public interface IScaledHazard
{
    event ActivateEvent AllScalersDeactivated;

    bool BeingScaled { get; set; }
}
