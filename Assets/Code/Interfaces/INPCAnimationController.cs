﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INPCAnimationController
{
    void SetIdleAnimation();
    void SetWalkAnimation();
   // void SetJumpAnimation();
    /// <summary>
    /// Performs the appropriate animation when the npc is alerted. This could be charging the player, firing a weapon, or nothing.
    /// </summary>
    void SetAlertAnimation();
    void SetShootAnimation();
    void SetStopShootingAnimation();
    void SetDeathAnimation();
    void SetStunnedAnimation(bool status);
}
