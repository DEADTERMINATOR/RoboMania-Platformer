﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWarpable
{
    void WarpToPosition(Vector2 position, Vector3 rotation);
}
