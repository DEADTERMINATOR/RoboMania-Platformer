﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Experimental.AI;
/// <summary>
/// A set of WayPonits
/// </summary>
[Serializable]
public class WayPoints : IEnumerable
{
    public struct ConnectedWaypoints
    {
        public struct WaypointData
        {
            public readonly Vector3 Point;
            public readonly int WaypointIndex;

            public WaypointData(Vector3 point, int waypointIndex)
            {
                Point = point;
                WaypointIndex = waypointIndex;
            }
        }

        public List<WaypointData> ConnectedPoints { get; private set; }

        public WaypointData First { get { return ConnectedPoints[0]; } }

        public Vector3 FirstPoint { get { return ConnectedPoints[0].Point; } }

        public WaypointData Last { get { return ConnectedPoints[ConnectedPoints.Count - 1]; } }

        public Vector3 LastPoint { get { return ConnectedPoints[ConnectedPoints.Count - 1].Point; } }

        public void AddConnectedPoint(Vector3 connectedPoint, int waypointIndex)
        {
            if (ConnectedPoints == null)
            {
                ConnectedPoints = new List<WaypointData>();
            }

            ConnectedPoints.Add(new WaypointData(connectedPoint, waypointIndex));
        }
    }

    /// <summary>
    /// The list of points in the set of waypoints
    /// </summary>
    public List<Vector3> Points = new List<Vector3>();

    public List<ConnectedWaypoints> ConnectedPoints = new List<ConnectedWaypoints>();

    public ConnectedWaypoints CurrentConnectedWaypoint { get { return _currentConnectedWaypoint; } }

    public int CurrentInex
    {
        get { return _ActiveWayPointIndex; }
    }
    public bool IsLooping = false;
    /// <summary>
    /// Number of loops -1 = INFINITE
    /// </summary>
    public int NumberOfLoops = -1;
    /// <summary>
    /// The index of the next waypoint
    /// </summary>
    public int IndexOfNextWayPoints { get { return _nextWayPointIndex; } }
    /// <summary>
    /// How many loops should be done
    /// </summary>
    public int GetNumberOfForwardLoops { get { return _numberOfForwardLoop; } }
    public float ComparitorThreshHould = 0.5f;
    /// <summary>
    /// First Point in the system
    /// </summary>
    public Vector3 First { get { return Points[0]; } }
    /// <summary>
    /// The Next Point In the system
    /// </summary>
    public Vector3 Next { get { return Points[_nextWayPointIndex]; } }
    /// <summary>
    /// The Final point in the system
    /// </summary>
    public Vector3 Last { get { return Points[Points.Count-1]; } }

    /// <summary>
    /// How many points are in the system
    /// </summary>
    public int Count { get { return Points.Count; } }
    private int _ActiveWayPointIndex = 0;
    private int _nextWayPointIndex = 1;
    private int _lastWayPointIndex;
    private int _numberOfForwardLoop = 0;
    ///private int _numberOfIncrements = 0;
    private bool _hitEnd = false;
    private bool _hasBeenReversed = false;

    private bool _pointsConnected = false;
    private ConnectedWaypoints _currentConnectedWaypoint;
    private int _currentConnectedWaypointIndex = 0;
    private int _currentPointInConnectedWaypoint = 0;

    public WayPoints()
    {
        List<Vector3> Points = new List<Vector3>();
        //  _lastWayPointINdex = Points.Count - 1;
    }

    public void Insert(int index, Vector3 newPointPos)
    {
        Points.Insert(index, newPointPos);
    }

    public void Add(Vector3 pos)
    {
        Points.Add(pos);
    }

    /// <summary>
    /// Clear all points and reset
    /// </summary>
    public void Clear()
    {
        Points.Clear();
        Reset();
    }

    /// <summary>
    ///  Re set the system
    /// </summary>
    public void Reset()
    {
         _ActiveWayPointIndex = 0;
         _nextWayPointIndex = 1;
         _lastWayPointIndex = Points.Count -1;
         _numberOfForwardLoop = 0;
         _hitEnd = false;
        _hasBeenReversed = false;
    }

    /// <summary>
    /// Add a base Vector 3 to all point (Ie from local space to wrold space)
    /// </summary>
    /// <param name="basPos"></param>
    public void AddToAll(Vector3 basPos)
    {
        for (int i = 0; i < Points.Count; i++)
        {
            Points[i] = Points[i] + basPos;
        }
    }

    /// <summary>
    /// Add a base Vector 3 to all point (Ie from local space to wrold space)
    /// </summary>
    /// <param name="basPos"></param>
    public void Subtract(Vector3 basPos)
    {
        for (int i = 0; i < Points.Count; i++)
        {
            Points[i] = Points[i] - basPos;
        }
    }

    /// <summary>
    /// Get the Target point
    /// </summary>
    /// <returns></returns>
    public Vector3 GetTarget()
    {
        if (_ActiveWayPointIndex >= 0 || _ActiveWayPointIndex < Points.Count)
        {
            try
            {
                return Points[_ActiveWayPointIndex];
            } 
            catch(ArgumentOutOfRangeException)
            {
                //Debug.LogError("Tryed to get index" + _ActiveWayPointIndex + "From an array of size " + Points.Count);
                return Vector3.zero;
            }
        }
        else
        {
            //Debug.LogError("Tryed to get index" + _ActiveWayPointIndex + "From an array of size " + Points.Count);
            throw new Exception("Invalid Next Traget index");
        }

    }

    public Vector3 SetTargetFromIndex(int waypointIndex)
    {
        _ActiveWayPointIndex = waypointIndex;

        if (waypointIndex <= 0)
        {
            _lastWayPointIndex = Points.Count - 1;
            _nextWayPointIndex = 1;
        }
        else if (waypointIndex == Points.Count - 1)
        {
            _lastWayPointIndex = waypointIndex-1;
            _nextWayPointIndex = 0;
        }
        else
        {
            _lastWayPointIndex = waypointIndex - 1;
            _nextWayPointIndex = waypointIndex + 1;
        }

        return Points[waypointIndex];
    }

    /// <summary>
    /// Get the point after the traget
    /// </summary>
    /// <returns></returns>
    public Vector3 GetNext()
    {
        return Points[_nextWayPointIndex];
    }

    /// <summary>
    /// Get the point comming From;
    /// </summary>
    /// <returns></returns>
    public Vector3 GetLast()
    {
        return Points[_lastWayPointIndex];
    }

    /// <summary>
    /// Will move to the next way point if not loping it will rever the traversal
    /// </summary>
    /// <returns></returns>
    public Vector3 Increment()
    {
        //if the revers flag is set then use decrment instreed
        if(_hasBeenReversed)
        {
            return Decrement();
        }
        _hitEnd = false;
        _lastWayPointIndex = _ActiveWayPointIndex;
        _ActiveWayPointIndex = (_ActiveWayPointIndex + 1) % Points.Count;
        _nextWayPointIndex = (_ActiveWayPointIndex + 1) % Points.Count;

        if (_ActiveWayPointIndex == 0 )// is this the end of the array
        {
            if (!IsLooping)// if we are not looping then rever the array
            {
                _ActiveWayPointIndex = Points.Count - 1;
                ReverseWayPoints();
            }
            _hitEnd = true; /// not that this is the end 
            _numberOfForwardLoop++;// incrment the number of loops
        }
        
        return GetTarget();
    }

    public Vector3 Decrement()
    {
        _hitEnd = false;
        if (_ActiveWayPointIndex == 0)
        {
            if (!IsLooping)// if we are not looping then rever the array
            {
                ReverseWayPoints();
                return GetTarget();
            }
            _hitEnd = true; /// not that this is the end 
            _numberOfForwardLoop++;// incrment the number of loops
            
        }

        _lastWayPointIndex = _ActiveWayPointIndex;
        _ActiveWayPointIndex = _ActiveWayPointIndex - 1;
        if(_ActiveWayPointIndex < 0)
        {
            _ActiveWayPointIndex = Points.Count - 1;
        }
        _nextWayPointIndex = (_ActiveWayPointIndex - 1) % Points.Count;
        
        return GetTarget();
    }

    public Vector3 IncrementConnectedPoints()
    {
        _currentConnectedWaypointIndex = (_currentConnectedWaypointIndex + 1) % ConnectedPoints.Count;
        _currentConnectedWaypoint = ConnectedPoints[_currentConnectedWaypointIndex];

        _ActiveWayPointIndex = _currentConnectedWaypoint.Last.WaypointIndex;
        _lastWayPointIndex = (_ActiveWayPointIndex - 1) % Points.Count;
        _nextWayPointIndex = (_ActiveWayPointIndex + 1) % Points.Count;

        if (_currentConnectedWaypointIndex == 0)// is this the end of the array
        {
            _hitEnd = true; /// not that this is the end 
            _numberOfForwardLoop++;// incrment the number of loops
        }

        return _currentConnectedWaypoint.LastPoint;
    }

    /// <summary>
    /// Set a flag that will cause the array to revers when inrementin only
    /// Sets the active point to the other end of the array;
    /// </summary>
    public void ReverseWayPoints()
    {
        _hasBeenReversed = !_hasBeenReversed;
        if (_hasBeenReversed)
        {
            _lastWayPointIndex = _ActiveWayPointIndex;
            _ActiveWayPointIndex = _ActiveWayPointIndex - 1 % Points.Count; 
        }else
        {
            _lastWayPointIndex = _ActiveWayPointIndex;
            _ActiveWayPointIndex = _ActiveWayPointIndex + 1 % Points.Count; 
        }
    }

    public bool IsMovingInDirOfIndex(int index)
    {
        if (_hasBeenReversed)
        {
            return index <= _nextWayPointIndex;
        }
        else
        {
            return index >= _nextWayPointIndex;
        }
    }

     public int GetIndexOfPointClosestTo(Vector3 thePoint)
    {
        float closestDistance = float.MaxValue;
        int closetIndex = 0;
        for(int i = 0; i < Points.Count; i++ )
        {
            float dist = Vector3.Distance( thePoint, Points[i]);
            if (dist < closestDistance)
            {
                closetIndex = i;
                closestDistance = dist;
            }
        }
        return closetIndex;
    }

    /// <summary>
    /// Gets the closest point in the direction may throw an CloseIndexNotFoundException if no point is infront in the index
    /// </summary>
    /// <param name="thePoint"></param>
    /// <param name="facingLeft"></param>
    /// <returns></returns>
    public int GetIndexOfClosestPointInDirection(Vector3 thePoint, bool facingLeft)
    {
        float closestDistance = float.MaxValue;
        int closetIndex = 0;
        int skippedPoints = 0;
        for (int i = 0; i < Points.Count; i++)
        {
            if ((facingLeft && thePoint.x > Points[i].x) || (!facingLeft && thePoint.x < Points[i].x))
            {
                float dist = Vector3.Distance(thePoint, Points[i]);
                if (dist < closestDistance)
                {
                    closetIndex = i;
                    closestDistance = dist;
                }
            }
            else
            {
                skippedPoints++;
            }
        }
        if (skippedPoints >= Points.Count)
        {
            throw new CloseIndexNotFoundException();
        }

        return closetIndex;
    }


    public bool IsOnLastPoint()
    {
        if (!IsLooping)
        {
            return _hitEnd;
        }
        else
        {
            return _hitEnd && NumberOfLoops <= _numberOfForwardLoop;

        }
    }

    public void ConnectPoints(Vector2? npcDimensions)
    {
        ConnectedPoints.Clear();
        ConnectedWaypoints currentConnectedPoints = new ConnectedWaypoints();
        currentConnectedPoints.AddConnectedPoint(First, 0);

        for (int i = 1; i < Points.Count; ++i)
        {
            var hit = Physics2D.CapsuleCast(currentConnectedPoints.FirstPoint, npcDimensions.GetValueOrDefault(), CapsuleDirection2D.Horizontal, 0, Points[i] - currentConnectedPoints.FirstPoint, Vector2.Distance(Points[i], currentConnectedPoints.FirstPoint), GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.TRIGGER_LAYER_SHIFTED);
            if (hit)
            {
                /* It's not possible to make a straight line from the first point in the current connected sequence to this point.
                   So the current sequence is complete, and we use this point as the first point of the next sequence. */
                var lastConnectedPoints = currentConnectedPoints;
                ConnectedPoints.Add(currentConnectedPoints);
                currentConnectedPoints = new ConnectedWaypoints();
                currentConnectedPoints.AddConnectedPoint(lastConnectedPoints.LastPoint, lastConnectedPoints.Last.WaypointIndex);

                /* We now decrement the i value so that we can verify that the current point does in fact connect to the new first point of the sequence, which is the previous point.
                 * Truthfully, I can't think of a case where it wouldn't connect, but let's guard against unforeseen edge cases. */
                hit = Physics2D.CapsuleCast(currentConnectedPoints.FirstPoint, npcDimensions.GetValueOrDefault(), CapsuleDirection2D.Horizontal, 0, Points[i] - currentConnectedPoints.FirstPoint, Vector2.Distance(Points[i], currentConnectedPoints.FirstPoint), GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.TRIGGER_LAYER_SHIFTED);
                if (hit)
                {
                    currentConnectedPoints.ConnectedPoints[0] = new ConnectedWaypoints.WaypointData(Points[i], i);
                }
            }
            else
            {
                //A staight line from the first point in the current connected sequence to this point is possible. So add it to the current sequence.
                currentConnectedPoints.AddConnectedPoint(Points[i], i);
            }
        }

        ConnectedPoints.Add(currentConnectedPoints);
        _pointsConnected = true;
    }

    public IEnumerator GetEnumerator()
    {
        return ((IEnumerable)Points).GetEnumerator();
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return base.ToString();
    }

    #region Operators
    public static WayPoints operator ++(WayPoints c1)
    {
        c1.Increment();
        return c1;
    }
    public static WayPoints operator --(WayPoints c1)
    {
        c1.Decrement();
        return c1;
    }
    public static WayPoints operator +(WayPoints c1, Vector3 c2)
    {
        c1.AddToAll(c2);
        return c1;
    }
    public static WayPoints operator -(WayPoints c1, Vector3 c2)
    {
        c1.Subtract(c2);
        return c1;
    }
    public static bool operator ==(WayPoints c1, Vector3 c2)
    {
        return StaticTools.ThresholdApproximately(c1.GetTarget(), c2, c1.ComparitorThreshHould);
    }
    public static bool operator !=(WayPoints c1, Vector3 c2)
    {
        return !StaticTools.ThresholdApproximately(c1.GetTarget(), c2, c1.ComparitorThreshHould);
    }

    public static bool operator ==(WayPoints c1, Vector2 c2)
    {
        return StaticTools.ThresholdApproximately((Vector2)c1.GetTarget(), (Vector2)c2, c1.ComparitorThreshHould);
    }
    public static bool operator !=(WayPoints c1, Vector2 c2)
    {
        return !StaticTools.ThresholdApproximately((Vector2)c1.GetTarget(), (Vector2)c2, c1.ComparitorThreshHould);
    }
    public Vector3 this[int key]
    {
        get
        {
            return Points[key];
        }
        set
        {
            Points[key] = value;
        }
    }

    #endregion
}

public class CloseIndexNotFoundException : Exception
{

}

