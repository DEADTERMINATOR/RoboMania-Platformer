﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InterfaceUtility
{
    /// <summary>
    /// Retrieves the first instance of a desired interface from the specified object. Use if you know
    /// that the specified object only has one behaviour that implements the desired interface.
    /// </summary>
    /// <typeparam name="T">The desired interface.</typeparam>
    /// <param name="result">The object to return as the desired interface.</param>
    /// <param name="objectToSearch">The game object to search for the interface.</param>
    public static void GetInterface<T>(out T result, GameObject objectToSearch) where T : class
    {
        MonoBehaviour[] behaviours = objectToSearch.GetComponents<MonoBehaviour>();
        result = null;
        foreach (MonoBehaviour behaviour in behaviours)
        {
            if (behaviour is T)
            {
                result = (T)((System.Object)behaviour);
                break;
            }
        }
    }

    /// <summary>
    /// Retrives all instances of a desired interface from the specified object. Use if the
    /// specified object has multiple behaviours that implement the desired interface.
    /// </summary>
    /// <typeparam name="T">The desired interface.</typeparam>
    /// <param name="results">The list of objects that implement the desired interface.</param>
    /// <param name="objectToSearch">The game object to search for the interface.</param>
    public static void GetInterfaces<T>(out List<T> results, GameObject objectToSearch) where T : class
    {
        MonoBehaviour[] behaviours = objectToSearch.GetComponents<MonoBehaviour>();
        results = new List<T>();
        foreach (MonoBehaviour behaviour in behaviours)
        {
            if (behaviour is T)
            {
                results.Add((T)((System.Object)behaviour));
            }
        }
    }
}
