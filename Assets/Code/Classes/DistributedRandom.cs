﻿using UnityEngine;
/// <summary>
/// References https://www.redblobgames.com/articles/probability/damage-rolls.html
/// https://www.alanzucconi.com/2015/09/09/understanding-the-gaussian-distribution/
/// https://www.alanzucconi.com/2015/09/16/how-to-sample-from-a-gaussian-distribution/
/// https://www.projectrhea.org/rhea/index.php/The_principles_for_how_to_generate_random_samples_from_a_Gaussian_distribution
/// </summary>

public class DistributedRandom
{
    /// <summary>
    /// Get a Gaussian Random Number From -1 - 1 Using Uniform distrabution  
    /// Using BoxMuller algo
    /// </summary>
    /// <returns></returns>
    public static float NextGaussian()
    {
        float v1, v2, s;
        do
        {
            v1 = 2.0f * Random.Range(0f, 1f) - 1.0f;
            v2 = 2.0f * Random.Range(0f, 1f) - 1.0f;
            s = v1 * v1 + v2 * v2;
        } while (s >= 1.0f || s == 0f);

        s = Mathf.Sqrt((-2.0f * Mathf.Log(s)) / s);

        return v1 * s;
    }

    /// <summary>
    /// get a Random Number using Gaussian  with supplied mean and standard deviation 
    /// </summary>
    /// <param name="mean"></param>
    /// <param name="standard_deviation"></param>
    /// <returns></returns>
    public static float NextGaussian(float mean, float standard_deviation)
    {
        return mean + NextGaussian() * standard_deviation;
    }

    /// <summary>
    ///  get a Random Number using Gaussian  with supplied mean and standard deviation With in the range Min - Max inclusive; 
    ///  https://www.hackmath.net/en/calculator/normal-distribution
    /// </summary>
    /// <param name="mean"></param>
    /// <param name="standard_deviation"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    public static float NextGaussian(float mean, float standard_deviation, float min, float max)
    {
        float x;
        do
        {
            x = NextGaussian(mean, standard_deviation);
        } while (x < min || x > max);
        return x;
    }

    /// <summary>
    /// Gen A random number using  N X Sided Dice 
    /// </summary>
    /// <param name="numberOfRolls"></param>
    /// <param name="numberOfSides"></param>
    /// <returns></returns>
    public static int NextRandomDiceRoll(int numberOfRolls, int numberOfSides)
    {
        int RandomValue = 0;
        for(int i =0; i < numberOfRolls;i++)
        {
            RandomValue += Random.Range(0,numberOfSides);        
        }
       
        return RandomValue;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="numberOfRolls"></param>
    /// <param name="Min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    public static int NextRandomDiceRoll(int numberOfRolls, int min , int max )
    {
       int sides =  (max-min) / numberOfRolls;
        return NextRandomDiceRoll(numberOfRolls, sides)+min;
    }

    public static int RerollBestTwoOutOfThreeDiceRoll(int numberOfRolls, int min, int max)
    {
        int sides = (max - min) / numberOfRolls;
        return RerollBestTwoOutOfThreeDiceRoll(numberOfRolls, sides) + min;
    }

    public static int RerollWorstTwoOutOfThreeDiceRoll(int numberOfRolls, int min, int max)
    {
        int sides = (max - min) / numberOfRolls;
        return RerollWorstTwoOutOfThreeDiceRoll(numberOfRolls, sides) + min;
    }



    public static int RerollBestTwoOutOfThreeDiceRoll(int numberOfRolls, int numberOfSides)
    {
        int[] rolls = { 0, 0, 0 };
        rolls[0] = NextRandomDiceRoll(numberOfRolls/2, numberOfSides+1);
        rolls[1] = NextRandomDiceRoll(numberOfRolls/2, numberOfSides+1);
        rolls[2] = NextRandomDiceRoll(numberOfRolls/2, numberOfSides+1);

        return (rolls[0] + rolls[1] + rolls[2]) - Mathf.Min(rolls[0], rolls[1], rolls[2]);
    }

    public static int RerollWorstTwoOutOfThreeDiceRoll(int numberOfRolls, int numberOfSides)
    {
        int[] rolls = { 0, 0, 0 };
        //Debug.Log(numberOfRolls + " " + numberOfSides);
        //Debug.Log(numberOfRolls/2 + " " + numberOfSides);
        rolls[0] = NextRandomDiceRoll(numberOfRolls/2, numberOfSides+1);
        rolls[1] = NextRandomDiceRoll(numberOfRolls/2, numberOfSides+1);
        rolls[2] = NextRandomDiceRoll(numberOfRolls/2, numberOfSides+1);

        return (rolls[0] + rolls[1] + rolls[2]) - Mathf.Max(rolls[0], rolls[1], rolls[2]);
    }

    
}

