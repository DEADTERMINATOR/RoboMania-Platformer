﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Inputs
{
    public class InputManager
    {
        #region Structs and Inner Classes
        /// <summary>
        /// A class that contains relevant information about any input devices that may be connected.
        /// </summary>
        public class Device
        {
            public DeviceType DeviceType;
            public string DeviceName;
            public InputDevice TheDevice;

            public Device(DeviceType deviceType, string deviceName, InputDevice device) => (DeviceType, DeviceName, TheDevice) = (deviceType, deviceName, device);
        }

        /// <summary>
        /// A struct that contains the information required to register or deregister an input callback.
        /// </summary>
        public struct RegisteredCallback
        {
            public string InputName;
            public InputActionChange InputPhase;
            public ActionCallback InputAction;

            public RegisteredCallback(string inputName, InputActionChange inputPhase, Action callbackAction, bool enable)
                => (InputName, InputPhase, InputAction) = (inputName, inputPhase, new ActionCallback(callbackAction, enable));
        }

        /// <summary>
        /// A struct that contains the information for a callback to be performed when a particular action is detected.
        /// </summary>
        public struct ActionCallback
        {
            public Action CallbackAction;
            public bool Enabled;

            public ActionCallback(Action callbackAction, bool enable = true) => (CallbackAction, Enabled) = (callbackAction, enable);

            public void SetEnabled(bool enabledState)
            {
                Enabled = enabledState;
            }
        }

        /// <summary>
        /// A class that contains the information required to process an input at a later time
        /// (used if the input can't be immediately processed because another callback related functionality is being performed).
        /// </summary>
        private class StoredInputAction
        {
            public InputAction Action;
            public InputActionChange Change;

            public StoredInputAction(InputAction action, InputActionChange change) => (Action, Change) = (action, change);
        }

        /// <summary>
        /// A struct that contains the information required to process the change of state for a registered callback at a later time
        /// (used if the change can't be immediately processed because another callback related functionality is being performed).
        /// </summary>
        private struct StoredCallbackStateChange
        {
            public RegisteredCallback Callback;
            public bool NewEnabledState;

            public StoredCallbackStateChange(RegisteredCallback callback, bool newEnabledState) => (Callback, NewEnabledState) = (callback, newEnabledState);
        }
        #endregion

        #region Delgates, Events, and Enums
        public delegate void ActionEvent(string actionName);
        /* Callback for when an input action is first initiated. */
        public event ActionEvent ActionStarted;
        /* Callback for when an input action is completed. */
        public event ActionEvent ActionPerformed;
        /* Callback for when an input action is cancelled. Will only happen if the action is not instantaneous (e.g. hold to interact, but the player released the button before the hold time has elapsed) */
        public event ActionEvent ActionCancelled;

        public delegate void DeviceChanged(Device newDevice);
        /* Callback for when the currently active device is changed. */
        public event DeviceChanged OnDeviceChanged;

        public delegate void RemapEvent(string msg);
        /* Callback for when the remap method has determined that the requested remap is valid and the system is waiting for a new input. */
        public event RemapEvent RemapWaiting;
        /* Callback for when the remap method has successfully remapped the action to the new input. */
        public event RemapEvent RemapSucceeded;
        /* Callback for when the remap method has determined that the requested remap is not valid (e.g. the requested action does not have a mapping for the supplied device type), so any remap attempt has been aborted. */
        public event RemapEvent RemapFailed;
        /* Callback for when a request to cancel the remap has occured. */
        public event RemapEvent RemapCancelled;

        /// <summary>
        /// The types of devices that we will handle.
        /// </summary>
        public enum DeviceType { KeyboardMouse, Controller, None }
        public enum GameInputsSets{ INGAME, MENU, POPUP }
        #endregion

        #region Public Properties
        public GameInputsSets ActiveInputActionSet = GameInputsSets.INGAME;
        public GameInputsSets LastActiveInputActionSet = GameInputsSets.INGAME;

        /// <summary>
        /// Getter for the single instance of the InputManager class.
        /// </summary>
        public static InputManager Instance { get { if (_instance == null) _instance = new InputManager(); return _instance; } }

        /// <summary>
        /// Reference to the In-Game action map.
        /// </summary>
        public GameInputs.InGameActions InGameActions { get { return _gameInputs.InGame; } }

        /// <summary>
        /// Reference to the Menu action map.
        /// </summary>
        public GameInputs.MenuActions MenuActions { get { return _gameInputs.Menu; } }


        /// <summary>
        /// Reference to the Menu action map.
        /// </summary>
        public GameInputs.PopUpUIActions PopUpActions { get { return _gameInputs.PopUpUI; } }

        /// <summary>
        /// A list of all currently connected devices.
        /// </summary>
        public List<Device> ConnectedDevices { get; private set; }

        /// <summary>
        /// The currently active device (i.e. the last device that was actuated).
        /// </summary>
        public Device CurrentDevice { get; private set; }

        /// <summary>
        /// The type of the currently active device.
        /// </summary>
        public DeviceType CurrentDeviceType { get { return CurrentDevice.DeviceType; } }

        /// <summary>
        /// Denotes whether there is a controller currently connected, even if it is not the type of the current device.
        /// </summary>
        public bool ControllerConnected { get; private set; }
        #endregion

        #region Private Properties
        /// <summary>
        /// Indicates that inputs were being processed at the time an attempted registration or deregistration of a callback occured.
        /// So the callback was stored to be handled at a later time.
        /// </summary>
        private bool _callbacksStoredForHandling = false;

        /// <summary>
        /// Indicated that a callback was being registered or deregistered at the time the processing of an input was attempted.
        /// So the input was stored to be handled at a later time.
        /// </summary>
        private bool _inputsStoredForHandling = false;

        private bool _stateChangesStoredForHandling = false;

        /// <summary>
        /// The list of callbacks to be registered at a later time.
        /// </summary>
        private List<RegisteredCallback> _callbacksToAdd = new List<RegisteredCallback>();

        /// <summary>
        /// The list of callbacks to be deregistered at a later time.
        /// </summary>
        private List<RegisteredCallback> _callbacksToRemove = new List<RegisteredCallback>();

        /// <summary>
        /// The list of inputs to be handled at a later time.
        /// </summary>
        private List<StoredInputAction> _inputsToHandle = new List<StoredInputAction>();

        private List<StoredCallbackStateChange> _stateChangesToHandle = new List<StoredCallbackStateChange>();


        private readonly Device _emptyDevice = new Device(DeviceType.None, "None", null);
        private static InputManager _instance = new InputManager();
        private GameInputs _gameInputs = new GameInputs();
        /* A mutex that ensures that the callback lists are not modified at the same time as they are accessed. */
        private bool _callbackListsLockActive = false;

        private Dictionary<string, List<ActionCallback>> _actionStartedCallbacks = new Dictionary<string, List<ActionCallback>>();
        private Dictionary<string, List<ActionCallback>> _actionPerformedCallbacks = new Dictionary<string, List<ActionCallback>>();
        private Dictionary<string, List<ActionCallback>> _actionCancelledCallbacks = new Dictionary<string, List<ActionCallback>>();
        #endregion

        #region Initialization and Update
        private InputManager()
        {
            ConnectedDevices = new List<Device>();
            foreach (InputDevice device in InputSystem.devices)
            {
                AddConnectedDevice(device);
            }

            if (ConnectedDevices.Count == 0)
            {
                Debug.LogWarning("No devices were connected at InputManagers instantiation");
                CurrentDevice = _emptyDevice;
            }

            InputSystem.onActionChange += OnActionChange;
            InputSystem.onDeviceChange += OnDeviceChange;
        }

        public void Update()
        {
            if (_inputsStoredForHandling)
            {
                var inputsListCopy = new List<StoredInputAction>(_inputsToHandle);

                foreach (StoredInputAction input in inputsListCopy)
                {
                    OnActionChange(input.Action, input.Change);
                    _inputsToHandle.Remove(input);
                }

                _inputsStoredForHandling = false;
            }

            if (_callbacksStoredForHandling)
            {
                var callbacksListCopy = new List<RegisteredCallback>(_callbacksToAdd);

                foreach (RegisteredCallback callback in callbacksListCopy)
                {
                    switch (callback.InputPhase)
                    {
                        case InputActionChange.ActionStarted:
                            RegisterSpecificActionStarted(callback.InputName, callback.InputAction.CallbackAction);
                            break;
                        case InputActionChange.ActionPerformed:
                            RegisterSpecificActionPerformed(callback.InputName, callback.InputAction.CallbackAction);
                            break;
                        case InputActionChange.ActionCanceled:
                            RegisterSpecificActionCancelled(callback.InputName, callback.InputAction.CallbackAction);
                            break;
                    }

                    _callbacksToAdd.Remove(callback);
                }

                callbacksListCopy = new List<RegisteredCallback>(_callbacksToRemove);

                foreach (RegisteredCallback callback in callbacksListCopy)
                {
                    if (callback.InputAction.CallbackAction == null)
                    {
                        RemoveRegisteredInputCallbackAll(callback.InputName, callback.InputPhase);
                    }
                    else
                    {
                        RemoveRegisteredInputCallback(callback);
                    }

                    _callbacksToRemove.Remove(callback);
                }

                _callbacksStoredForHandling = false;
            }

            if (_stateChangesStoredForHandling)
            {
                var stateChangesCopy = new List<StoredCallbackStateChange>(_stateChangesToHandle);

                foreach (StoredCallbackStateChange change in stateChangesCopy)
                {
                    SetEnabledRegisteredInputCallback(change.Callback, change.NewEnabledState);
                    _stateChangesToHandle.Remove(change);
                }

                _stateChangesStoredForHandling = false;
            }
        }
        #endregion

        #region Input Map Methods
        /// <summary>
        /// Enables the In-Game action map.
        /// </summary>
        public void EnableInGameActions()
        {
            LastActiveInputActionSet = ActiveInputActionSet;
            ActiveInputActionSet = GameInputsSets.INGAME;
            InGameActions.Enable();
        }

        /// <summary>
        /// Disabled the In-Game action map.
        /// </summary>
        public void DisableInGameActions()
        {
            InGameActions.Disable();
        }

        /// <summary>
        /// Enables the Menu action map.
        /// </summary>
        public void EnableMenuActions()
        {
            LastActiveInputActionSet = ActiveInputActionSet;
            ActiveInputActionSet = GameInputsSets.MENU;
            MenuActions.Enable();
        }

        /// <summary>
        /// Disables the Menu action map.
        /// </summary>
        public void DisableMenuActions()
        {
            MenuActions.Disable();
        }

        /// <summary>
        /// Enables the Menu action map.
        /// </summary>
        public void EnablePopUpActions()
        {
            LastActiveInputActionSet = ActiveInputActionSet;
            ActiveInputActionSet = GameInputsSets.POPUP;
            PopUpActions.Enable();
        }

        /// <summary>
        /// Remaps the input of the specified device type for the provided action.
        /// </summary>
        /// <param name="action">The action whose binding should be remapped.</param>
        /// <param name="deviceForRemapping">The type of device we want to remap</param>
        /// <param name="bindingIndex">Specifies the specific index of the binding that we want to remap. If this value is more than 0, then the method will attempt to target the binding attached to this index in the action's complete binding array.</param>
        public void RemapActionBinding(InputAction action, DeviceType deviceForRemapping, int bindingIndex = -1)
        {
            Debug.Log("Rebind Started");

            string desiredControlType = "";
            InputControl device = null;
            switch (deviceForRemapping)
            {
                case DeviceType.KeyboardMouse:
                    desiredControlType = "<Keyboard>";
                    device = Keyboard.current;
                    break;
                case DeviceType.Controller:
                    desiredControlType = "<Gamepad>";
                    device = Gamepad.current;
                    break;
            }

            var index = bindingIndex;
            if (bindingIndex > 0)
            {
                //Handle the case where we either return the part composite for remap, or handle remapping all elements of the composite one after another if the index leads to the composite parent.
            }
            else if (bindingIndex == -1)
            {
                foreach (var aBinding in action.bindings)
                {
                    if (InputControlPath.Matches(aBinding.effectivePath, device))
                    {
                        Debug.Log($"Binding for /{device}: {aBinding}");
                        index = action.GetBindingIndex(null, aBinding.effectivePath);
                    }
                }
            }
            else
            {
                RemapFailed?.Invoke("The binding index of " + bindingIndex + " is invalid for the action named " + action.name);
            }

            if (index != -1)
            {
                action.Disable();

                InputActionRebindingExtensions.RebindingOperation rebind = null;

                rebind = action.PerformInteractiveRebinding(index)
                    .WithControlsHavingToMatchPath(desiredControlType)
                    .OnMatchWaitForAnother(0.1f);

                rebind.OnComplete(
                    operation =>
                    {
                        Debug.Log("Rebind of action " + action.name + " complete");

                        action.Enable();
                        rebind.Dispose();

                        RemapSucceeded?.Invoke("Remap Successful");
                    });

                rebind.OnCancel(
                    operation =>
                    {
                        Debug.Log("Rebind of action " + action.name + " cancelled");

                        action.Enable();
                        rebind.Dispose();

                        RemapCancelled?.Invoke("Remap Cancelled");
                    });

                rebind.Start();
                RemapWaiting?.Invoke("Waiting for New Input...");
            }
            else
            {
                Debug.LogError("That action doesn't exist for the current device");
                RemapFailed?.Invoke("Could not find a binding for that action and device combination.");
            }
        }

        /// <summary>
        /// Disables the Menu action map.
        /// </summary>
        public void DisablePopUpActions()
        {
            PopUpActions.Disable();
        }

        /// <summary>
        /// Disables the Menu action map.
        /// </summary>
        public void EnableLastActions()
        {
            GameInputsSets newLast = ActiveInputActionSet;
            ActiveInputActionSet = LastActiveInputActionSet;
            switch (LastActiveInputActionSet)
            {
                case GameInputsSets.INGAME:
                    InGameActions.Enable();
                    break;
                case GameInputsSets.MENU:
                    MenuActions.Enable();
                    break;
                case GameInputsSets.POPUP:
                    PopUpActions.Enable();
                    break;

            }
            LastActiveInputActionSet = newLast;
        }

        /// <summary>
        /// Disables the Menu action map.
        /// </summary>
        public void DisableLastActions()
        {
            switch (LastActiveInputActionSet)
            {
                case GameInputsSets.INGAME:
                    InGameActions.Disable();
                    break;
                case GameInputsSets.MENU:
                    MenuActions.Disable();
                    break;
                case GameInputsSets.POPUP:
                    PopUpActions.Disable();
                    break;

            }
        }

        /// <summary>
        /// Disables the Menu action map.
        /// </summary>
        public void DisableCurrentActions()
        {
            switch (ActiveInputActionSet)
            {
                case GameInputsSets.INGAME:
                    InGameActions.Disable();
                    break;
                case GameInputsSets.MENU:
                    MenuActions.Disable();
                    break;
                case GameInputsSets.POPUP:
                    PopUpActions.Disable();
                    break;

            }
        }
        #endregion

        #region Callback Registration Methods
        /// <summary>
        /// Registers the provided Action to be called when a specific action is started.
        /// </summary>
        /// <param name="inputActionName">The name of the input action that will trigger the callback.</param>
        /// <param name="callbackFunction">The action to invoke on the callback.</param>
        public RegisteredCallback RegisterSpecificActionStarted(string inputActionName, Action callbackFunction, bool enableImmediately = true)
        {
            var newActionStartedCallback = new RegisteredCallback(inputActionName, InputActionChange.ActionStarted, callbackFunction, enableImmediately);
            RegisterCallback(ref _actionStartedCallbacks, newActionStartedCallback);
            return newActionStartedCallback;
        }

        /// <summary>
        /// Registers the provided Action to be called when a specific action is performed.
        /// </summary>
        /// <param name="inputActionName">The name of the input action that will trigger the callback.</param>
        /// <param name="callbackFunction">The function to invoke on the callback.</param>
        public RegisteredCallback RegisterSpecificActionPerformed(string inputActionName, Action callbackFunction, bool enableImmediately = true)
        {
            var newActionPerformedCallback = new RegisteredCallback(inputActionName, InputActionChange.ActionPerformed, callbackFunction, enableImmediately);
            RegisterCallback(ref _actionPerformedCallbacks, newActionPerformedCallback);
            return newActionPerformedCallback;
        }

        /// <summary>
        /// Registers the provided Action to be called when a specific action is cancelled.
        /// </summary>
        /// <param name="actionName">The name of the input action that will trigger the callback.</param>
        /// <param name="callbackFunction">The function to invoke on the callback.</param>
        public RegisteredCallback RegisterSpecificActionCancelled(string inputActionName, Action callbackFunction, bool enableImmediately = true)
        {
            var newActionCancelledCallback = new RegisteredCallback(inputActionName, InputActionChange.ActionDisabled, callbackFunction, enableImmediately);
            RegisterCallback(ref _actionCancelledCallbacks, newActionCancelledCallback);
            return newActionCancelledCallback;
        }

        /// <summary>
        /// Sets the enabled state for the specified callback.
        /// </summary>
        /// <param name="callback">The callback to modify the enabled state of.</param>
        /// <param name="enabledState">The new enabled state.</param>
        public void SetEnabledRegisteredInputCallback(RegisteredCallback callback, bool enabledState)
        {
            switch (callback.InputPhase)
            {
                case InputActionChange.ActionStarted:
                    SetEnabledRegisteredCallback(ref _actionStartedCallbacks, callback, enabledState);
                    break;
                case InputActionChange.ActionPerformed:
                    SetEnabledRegisteredCallback(ref _actionPerformedCallbacks, callback, enabledState);
                    break;
                case InputActionChange.ActionCanceled:
                    SetEnabledRegisteredCallback(ref _actionCancelledCallbacks, callback, enabledState);
                    break;
            }
        }

        /// <summary>
        /// Removes a registered action for a particular input.
        /// </summary>
        /// <param name="callbackToRemove">The struct that was returned upon the initial registration of the action.</param>
        public void RemoveRegisteredInputCallback(RegisteredCallback callbackToRemove)
        {
            if (!_callbackListsLockActive)
            {
                _callbackListsLockActive = true;

                switch (callbackToRemove.InputPhase)
                {
                    case InputActionChange.ActionStarted:
                        _actionStartedCallbacks[callbackToRemove.InputName].Remove(callbackToRemove.InputAction);
                        break;
                    case InputActionChange.ActionPerformed:
                        _actionPerformedCallbacks[callbackToRemove.InputName].Remove(callbackToRemove.InputAction);
                        break;
                    case InputActionChange.ActionDisabled:
                        _actionCancelledCallbacks[callbackToRemove.InputName].Remove(callbackToRemove.InputAction);
                        break;
                }

                _callbackListsLockActive = false;
            }
            else
            {
                _callbacksToRemove.Add(callbackToRemove);
                _callbacksStoredForHandling = true;
            }
        }

        /// <summary>
        /// Removes all registered actions for a particular input and input phase.
        /// </summary>
        /// <param name="inputActionName">The name of the input to remove callback actions from.</param>
        /// <param name="inputPhase">The input phase associated with the callback.</param>
        public void RemoveRegisteredInputCallbackAll(string inputActionName, InputActionChange inputPhase)
        {
            if (!_callbackListsLockActive)
            {
                _callbackListsLockActive = true;

                switch (inputPhase)
                {
                    case InputActionChange.ActionStarted:
                        _actionStartedCallbacks[inputActionName].Clear();
                        break;
                    case InputActionChange.ActionPerformed:
                        _actionPerformedCallbacks[inputActionName].Clear();
                        break;
                    case InputActionChange.ActionDisabled:
                        _actionCancelledCallbacks[inputActionName].Clear();
                        break;
                }

                _callbackListsLockActive = false;
            }
            else
            {
                _callbacksToRemove.Add(new RegisteredCallback(inputActionName, inputPhase, null, false));
                _callbacksStoredForHandling = true;
            }
        }

        /// <summary>
        /// Registers the callback into the provided dictionary.
        /// </summary>
        /// <param name="dictionaryToAddTo">The dictionary to register the callback into.</param>
        /// <param name="callbackToRegister">The callback to register.</param>
        private void RegisterCallback(ref Dictionary<string, List<ActionCallback>> dictionaryToAddTo, RegisteredCallback callbackToRegister)
        {
            if (!_callbackListsLockActive)
            {
                _callbackListsLockActive = true;

                if (!dictionaryToAddTo.ContainsKey(callbackToRegister.InputName))
                {
                    dictionaryToAddTo.Add(callbackToRegister.InputName, new List<ActionCallback>());
                }
                dictionaryToAddTo[callbackToRegister.InputName].Add(callbackToRegister.InputAction);

                _callbackListsLockActive = false;
            }
            else
            {
                _callbacksToAdd.Add(callbackToRegister);
                _callbacksStoredForHandling = true;
            }
        }

        /// <summary>
        /// Sets the enabled state for the specified callback in the appropriate dictionary.
        /// </summary>
        /// <param name="dictionary">The dictionary that has the callback.</param>
        /// <param name="callbackToSetEnabledState">The callback having their enabled state modified.</param>
        /// <param name="enabledState">The new enabled state.</param>
        private void SetEnabledRegisteredCallback(ref Dictionary<string, List<ActionCallback>> dictionary, RegisteredCallback callbackToSetEnabledState, bool enabledState)
        {
            if (!_callbackListsLockActive)
            {
                _callbackListsLockActive = true;

                int callbackToModifyIndex = dictionary[callbackToSetEnabledState.InputName].FindIndex(x => x.CallbackAction == callbackToSetEnabledState.InputAction.CallbackAction);
                if (callbackToModifyIndex != -1)
                {
                    ActionCallback callbackToModify = dictionary[callbackToSetEnabledState.InputName][callbackToModifyIndex];
                    callbackToModify.Enabled = enabledState;
                    dictionary[callbackToSetEnabledState.InputName][callbackToModifyIndex] = callbackToModify;
                }

                _callbackListsLockActive = false;
            }
            else
            {
                _stateChangesToHandle.Add(new StoredCallbackStateChange(callbackToSetEnabledState, enabledState));
                _stateChangesStoredForHandling = true;
            }
        }
        #endregion

        /// <summary>
        /// Callback to pick up and pass along when an action is started, performed, or cancelled. Also checks the device on the start of an action to determine if the currently active device has changed.
        /// </summary>
        /// <param name="obj">The type of action. This can be InputAction, InputActionMap, or InputActionAsset depending on the InputActionChange.</param>
        /// <param name="change">The type of change registered.</param>
        private void OnActionChange(object obj, InputActionChange change)
        {
            if (change == InputActionChange.ActionStarted || change == InputActionChange.ActionPerformed || change == InputActionChange.ActionCanceled)
            {
                /* obj can be InputAction, InputActionMap, or InputActionAsset depending on the InputActionChange. For the cases we care about, obj will be an InputAction. */
                InputAction action = obj as InputAction;

                if (!_callbackListsLockActive)
                {
                    _callbackListsLockActive = true;

                    switch (change)
                    {
                        case InputActionChange.ActionStarted:
                            if (action.activeControl.device != CurrentDevice.TheDevice)
                            {
                                CurrentDevice = FindConnectedDevice(action.activeControl.device);
                                OnDeviceChanged?.Invoke(CurrentDevice);
                            }

                            if (_actionStartedCallbacks.ContainsKey(action.name))
                            {
                                foreach (ActionCallback callback in _actionStartedCallbacks[action.name])
                                {
                                    if (callback.Enabled)
                                    {
                                        callback.CallbackAction.Invoke();
                                    }
                                }
                            }

                            ActionStarted?.Invoke(action.name);
                            break;
                        case InputActionChange.ActionPerformed:
                            if (_actionPerformedCallbacks.ContainsKey(action.name))
                            {
                                foreach (ActionCallback callback in _actionPerformedCallbacks[action.name])
                                {
                                    if (callback.Enabled)
                                    {
                                        callback.CallbackAction.Invoke();
                                    }
                                }
                            }

                            ActionPerformed?.Invoke(action.name);
                            break;
                        case InputActionChange.ActionCanceled:
                            if (_actionCancelledCallbacks.ContainsKey(action.name))
                            {
                                foreach (ActionCallback callback in _actionCancelledCallbacks[action.name])
                                {
                                    if (callback.Enabled)
                                    {
                                        callback.CallbackAction.Invoke();
                                    }
                                }
                            }

                            ActionCancelled?.Invoke(action.name);
                            break;

                    }

                    _callbackListsLockActive = false;
                }
                else
                {
                    _inputsToHandle.Add(new StoredInputAction(action, change));
                    _inputsStoredForHandling = true;
                }
            }
        }

        #region Device Methods
        /// <summary>
        /// Callback to pick up and pass along when a device as become active or inactive in some way. Also sets the currently active device as necessary.
        /// </summary>
        /// <param name="device">The device being changed.</param>
        /// <param name="change">The type of change registered.</param>
        private void OnDeviceChange(InputDevice device, InputDeviceChange change)
        {
            switch (change)
            {
                case InputDeviceChange.Added:
                    AddConnectedDevice(device);
                    break;
                case InputDeviceChange.Removed:
                    RemoveConnectedDevice(device);
                    break;
                case InputDeviceChange.Reconnected:
                    AddConnectedDevice(device);
                    break;
                case InputDeviceChange.Disconnected:
                    RemoveConnectedDevice(device);
                    break;
                case InputDeviceChange.Enabled:
                    AddConnectedDevice(device);
                    break;
                case InputDeviceChange.Disabled:
                    RemoveConnectedDevice(device);
                    break;
            }

            OnDeviceChanged?.Invoke(CurrentDevice);
        }

        /// <summary>
        /// Adds a new connected device to the list of connected devices and sets it to be the currently active device.
        /// </summary>
        /// <param name="device">The device to add.</param>
        private void AddConnectedDevice(InputDevice device)
        {
            Device newDevice = null;

            //We don't currently and probably won't care specifically what kind of controller we're dealing with, but just in case.
            switch (device.name)
            {
                case "Keyboard":
                    newDevice = new Device(DeviceType.KeyboardMouse, "Keyboard", device);
                    break;
                case "Mouse":
                    newDevice = new Device(DeviceType.KeyboardMouse, "Mouse", device);
                    break;
                case "XInputControllerWindows":
                    newDevice = new Device(DeviceType.Controller, "Xbox Controller", device);
                    ControllerConnected = true;
                    break;
                case "DualShock4GamepadHID":
                    newDevice = new Device(DeviceType.Controller, "PS4 Controller", device);
                    ControllerConnected = true;
                    break;
                case "SwitchProControllerHID":
                    /* NOTE: Switch controller is hilariously broken on PC. It constantly inputs random actions. Not sure if it's some sort of incompatibility between the controller and operating on PC, or something weird about the controller that needs to be handled. But it's not necessary to address at this time. */
                    newDevice = new Device(DeviceType.Controller, "Switch Pro Controller", device);
                    ControllerConnected = true;
                    break;
                default:
                    if (device.name.ToLower().Contains("controller"))
                    {
                        newDevice = new Device(DeviceType.Controller, "Generic Controller", device);
                        ControllerConnected = true;
                    }
                    break;
            }

            ConnectedDevices.Add(newDevice);
            CurrentDevice = newDevice;
        }

        /// <summary>
        /// Removes a newly disconnected device from the list of connected devices and sets the currently active device back to the first device in the list of connected devices.
        /// </summary>
        /// <param name="removedDevice">The device to remove.</param>
        private void RemoveConnectedDevice(InputDevice removedDevice)
        {
            for (int i = 0; i < ConnectedDevices.Count; ++i)
            {
                if (ConnectedDevices[i].TheDevice == removedDevice)
                {
                    ConnectedDevices.RemoveAt(i);

                    var controllerStillConnected = ConnectedDevices.Exists(x => x.DeviceType == DeviceType.Controller);
                    if (!controllerStillConnected)
                    {
                        ControllerConnected = false;
                    }

                    break;
                }
            }

            if (CurrentDevice.TheDevice == removedDevice)
            {
                if (ConnectedDevices.Count != 0)
                {
                    CurrentDevice = ConnectedDevices[0];
                }
                else
                {
                    Debug.LogError("InputManager just handled a device disconnection, but had no other connected devices to fall back on.");
                    CurrentDevice = _emptyDevice;
                }
            }
        }

        /// <summary>
        /// Finds a specified device in the list currently connected devices. The way this method is used, the device it is searching for should never not be in the list, or something has gone wrong.
        /// </summary>
        /// <param name="device">The device to find.</param>
        /// <returns></returns>
        private Device FindConnectedDevice(InputDevice device)
        {
            for (int i = 0; i < ConnectedDevices.Count; ++i)
            {
                if (ConnectedDevices[i].TheDevice == device)
                {
                    return ConnectedDevices[i];
                }
            }

            Debug.LogError("InputManager just tried to find a connected device that had not been added. How did this happen?");
            return _emptyDevice;
        }
        #endregion
    }
}
