﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEditor;

[Serializable]
public class ComplexWaypointConnection
{
    /// <summary>
    /// To Area ID;
    /// </summary>
    public int ToArea;
    /// <summary>
    /// The Path
    /// </summary>
    public WayPoints Path = new WayPoints();
    /// <summary>
    /// Can this connectiong be uses for both nodes it connects
    /// </summary>
    public bool IsOneDirection = false;

    public bool IsDummyNode = false;

    public ComplexWaypointConnection actualyNode;
}

[Serializable]
public class ComplexWayPointArea
{
    public int ID;
    /// <summary>
    /// The point the bat will fly in the active area
    /// </summary>
    public WayPoints PatrolPoints = new WayPoints();
    /// <summary>
    /// The point uses a the base for bombing
    /// </summary>
    public Vector3 focalPoint;
    /// <summary>
    /// Alt exit paths IE not a linierar exit in the array suck as from are 1 to 4 
    /// </summary>
    public ComplexWaypointConnection[] ComplexWaypointConnections;

}
[Serializable]
public class ComplexWayPointEdit
{
    /// <summary>
    /// The point the bat will fly in the active area
    /// </summary>
    public List<GameObject> PatrolPoints;
    /// <summary>
    /// The point uses a the base for bombing
    /// </summary>
    public Vector3 focalPoint;
    /// <summary>
    /// the PAths to all points
    /// </summary>
    public Dictionary<int, List<GameObject>> ConnectionPathsPoints = new Dictionary<int, List<GameObject>>();
    /// <summary>
    /// GameObject that holds the areas
    /// </summary>
    public GameObject AreaGameOBJ;

    public GameObject PointsGameOBJ;

    public GameObject ConnectionGameOBJ;

     public Dictionary<int, GameObject> ConnectionPaths = new Dictionary<int, GameObject>();


    public ComplexWayPointEdit()
    {
        PatrolPoints = new List<GameObject>();
    }

}
/// <summary>
/// Represents a set of waypoint seperated into partrol areas and paths to the next area
/// </summary>
public class ComplexWayPoints : EditableWayPoints
{
    /// <summary>
    /// The Areas that will be patrolled
    /// </summary>
    //[HideInInspector]
    public List<ComplexWayPointArea> Areas;
    /// <summary>
    /// is the system being editied
    /// </summary>
    [HideInInspector]
    public bool IsBeingEdited;
    #region Gizmo Colours
    /// <summary>
    /// Gizmo Colours 
    /// </summary>
    public Color PatrolPathColor = Color.blue;
    public Color PatrolAreaColor = Color.black;
    public Color OneWayConnectedPath = Color.red;
    public Color TwoWayConnectedPath = Color.green;
    #endregion
    public bool IsRandom = false;
    /// <summary>
    /// The id of the current area
    /// </summary>
    private int currentArea = 0;
    /// <summary>
    /// The Next Area;
    /// </summary>
    private int nextArea = 0;
    private bool inTrasition = false;
    private WayPoints activePath;

    public int CurrentArea { get { return currentArea; } }
    public WayPoints ActivePath { get
        {
            if(activePath == null)
            {
                activePath = Areas[0].PatrolPoints;
            }
            if (activePath.IsOnLastPoint())
            {
                ToNext();

            } return activePath; } }
    /// <summary>
    /// Stop the path from moving out of a patroll area;
    /// </summary>
    public bool CanMoveToNextArea = true;
    /// <summary>
    /// Override the next area if it can -1 is not in use and non vaild id will be ignored
    /// </summary>
    public int NextOverRide = -1;
 
    /// <summary>
    /// Add An area to the system
    /// </summary>
    /// <param name="newArea"></param>
    public void AddArea(ComplexWayPointArea newArea)
    {
        newArea.ID = Areas.Count;
        newArea.PatrolPoints.IsLooping = true;
        newArea.PatrolPoints.NumberOfLoops = 2;
        Areas.Add(newArea);
        FixConnections();
    }
    /// <summary>
    /// Add any new connection 
    /// </summary>
    public void FixConnections()
    {
        if(Areas.Count  >  1)
        {
            //look at each area 
            foreach (ComplexWayPointArea area in Areas)
            {
                ComplexWaypointConnection[] old = null;
                // are are theare any existing connection
                if (area.ComplexWaypointConnections != null)
                {
                    if (area.ComplexWaypointConnections.Length == Areas.Count)
                    {
                        continue;
                    }
                    old = area.ComplexWaypointConnections;
                }
                area.ComplexWaypointConnections = new ComplexWaypointConnection[Areas.Count];
                foreach (ComplexWayPointArea connctionArea in Areas)
                {
                    ///if the path is from self to self the set it as null
                    if(connctionArea.ID == area.ID)
                    {
                        area.ComplexWaypointConnections[connctionArea.ID] = null;
                        continue;
                    }
                    //id no old recored exist if the id is one that is new or the the data is null
                    if (old == null || old.Length <= connctionArea.ID || old[connctionArea.ID] == null)
                    {
                        ComplexWaypointConnection newConnect = new ComplexWaypointConnection();
                        newConnect.ToArea = connctionArea.ID;
                        newConnect.Path.IsLooping = true;
                        newConnect.Path.NumberOfLoops = 1;
                        newConnect.IsDummyNode = false;
                        newConnect.IsOneDirection = true;
                        newConnect.Path.Add(Vector3.Lerp(area.PatrolPoints.Last, connctionArea.PatrolPoints.First, 0.33f));
                        newConnect.Path.Add(Vector3.Lerp(area.PatrolPoints.Last, connctionArea.PatrolPoints.First, 0.66f));
                        area.ComplexWaypointConnections[connctionArea.ID] = newConnect;
                    }
                    else // copy old data 
                    {
                        area.ComplexWaypointConnections[connctionArea.ID] = old[connctionArea.ID];
                    }

                }
            }
        }
    }

    /// <summary>
    /// Will From A path from one area to the next at most crossing 2 areas
    /// </summary>
    /// <param name="curentArea"></param>
    /// <param name="Currentindx"></param>
    /// <param name="tragetArea"></param>
    /// <returns></returns>
    public WayPoints BuildAPathToArea(int curentArea, int Currentindx, int tragetArea)
    {
        WayPoints newPath = new WayPoints();
        newPath.IsLooping = false;
        newPath.NumberOfLoops = 1;
        if(curentArea == tragetArea)
        {
            throw new ComplexExitPathBuildingException("can not build a Path To self");
        }
        else if(Areas[curentArea].ComplexWaypointConnections[tragetArea] != null )// one hope
        {
            int disranceToEnd = Areas[curentArea].PatrolPoints.Count - Currentindx;
            if (disranceToEnd <= Currentindx)// is the End closed by moving in the normal Dir
            {
                while (Currentindx < Areas[curentArea].PatrolPoints.Count)//add all point from current to end of area 
                {
                    newPath.Add(Areas[curentArea].PatrolPoints[Currentindx]);
                    Currentindx++;
                }
            }
            else
            {
                while (Currentindx >= 0)//add all point from current to end of area 
                {
                    newPath.Add(Areas[curentArea].PatrolPoints[Currentindx]);
                    Currentindx--;
                }
                newPath.Add(Areas[curentArea].PatrolPoints.Last);
            }
            foreach(Vector3 point in Areas[curentArea].ComplexWaypointConnections[tragetArea].Path)//add all the conecting points 
            {
                newPath.Add(point);
            }
            newPath.Add(Areas[tragetArea].PatrolPoints[0]);//add first point of the traget area 
        }
        else
        {
            int ThrouAreaID = -1;
            foreach(ComplexWaypointConnection connect in  Areas[curentArea].ComplexWaypointConnections)//Look at all connected areas
            {
                if (Areas[connect.ToArea].ComplexWaypointConnections[tragetArea] != null)// an connecrting area is connected to the traget
                {
                    ThrouAreaID = connect.ToArea;
                    break;
                }
            }
            if(ThrouAreaID == -1)
            {
                throw new ComplexExitPathBuildingException("can not find a 2 hope or less path");
            }
            while (Currentindx < Areas[curentArea].PatrolPoints.Count)//add all point from current to end of area 
            {
                newPath.Add(Areas[curentArea].PatrolPoints[Currentindx]);
                Currentindx++;
            }
            foreach (Vector3 point in Areas[curentArea].ComplexWaypointConnections[ThrouAreaID].Path)//add all the conecting points to the next area 
            {
                newPath.Add(Areas[curentArea].PatrolPoints[Currentindx]);
            }
            //rest inddex to zero
            Currentindx = 0;
            ///Take A shorctcut from first to last (exit point)
            newPath.Add(Areas[ThrouAreaID].PatrolPoints[0]);
            newPath.Add(Areas[ThrouAreaID].PatrolPoints.Last);

            foreach (Vector3 point in Areas[ThrouAreaID].ComplexWaypointConnections[tragetArea].Path)//add all the conecting points 
            {
                newPath.Add(point);
            }

            newPath.Add(Areas[tragetArea].PatrolPoints[0]);//add first point of the traget area 

        }
        return newPath;
    }

    /// <summary>
    /// Get The next active area based on an index
    /// </summary>
    /// <param name="currentIndex"></param>
    /// <returns></returns>
    public ComplexWayPointArea GetNextArea(int currentIndex)
    {
        if (currentIndex < Areas.Count - 1)
        {
            return Areas[currentIndex + 1];
        }
        else
        {
            return Areas[0];
        }

    }

    //get a index to a connected area at random
    private int GetRandomArea()
    {
        if(Areas.Count == 2)// if we only have 2 elemnts the just return the other
        {
            if(currentArea == 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        ///Fail Safe only tries 10 times
        int failCount = 0;
        while(true)
        {
            //get a random numb in the index range
            int index = UnityEngine.Random.Range(0, Areas.Count);
            //make sure its not the same index
            if(index != CurrentArea && Areas[currentArea].ComplexWaypointConnections[index] != null)
            {
                return index;
            }
            else// is not valid;
            {
                failCount++;
                if(failCount == 10)// to many fails must be an issues with the system
                {             
                    throw new Exception("Random Path can not be found");
                }
            }
        }
        
    }
    /// <summary>
    /// Set the ActivePath to the next set of waypoints
    /// </summary>
    public void ToNext()
    {
        if (inTrasition)// was moving to next area
        {
            //set the active path to the patrol points
            activePath = Areas[nextArea].PatrolPoints;
            activePath.Reset();
            currentArea = nextArea;
            inTrasition = false;
        }
        else if(CanMoveToNextArea)
        {
            if(NextOverRide != -1)
            {
                if(NextOverRide == nextArea)
                {
                    activePath.Reset();
                    return;
                }
                nextArea = NextOverRide;
                if (nextArea >= Areas.Count || nextArea < 0)
                {
                    nextArea = 0;
                }

            }
            else if (IsRandom)
            {
                //Get an  random index
                nextArea = GetRandomArea();
                inTrasition = true;
            }
            else
            {
                inTrasition = true;
                nextArea++;
                if(nextArea >= Areas.Count)
                {
                    nextArea = 0;
                }
              
            }
            //is the node a dummy node
            if (Areas[currentArea].ComplexWaypointConnections[nextArea].IsDummyNode)
            {
                //get the actual node ref
                activePath = Areas[nextArea].ComplexWaypointConnections[currentArea].Path;
                //resert the valuse of the path including fixing the dirction
                activePath.Reset();
                //revers the waypoints as the path is in revers
                activePath.ReverseWayPoints();
            }
            else
            {
                //set the active path 
                activePath = Areas[currentArea].ComplexWaypointConnections[nextArea].Path;
                //resert the valuse of the path including fixing the dirction
                activePath.Reset();
            }
            inTrasition = true;
        }
       
    }
    /// <summary>
    /// Move backword in the areas
    /// </summary>
    /// <param name="currentIndex"></param>
    /// <returns></returns>
    public ComplexWayPointArea GetLastArea(int currentIndex)
    {
        if (currentIndex > 0)
        {
            return Areas[currentIndex - 1];
        }
        else
        {
            return Areas[Areas.Count - 1];
        }

    }


    private void Start()
    {
        if (Areas.Count >0)
        {
            activePath = Areas[0].PatrolPoints;
        }
    }
    /// <summary>
    /// Will fid the area the player is cloest to but will favor area that area Above the player if any are other wize it will pick the closet to the player
    /// </summary>
    /// <param name="thePoint"></param>
    /// <returns></returns>
    public int GetAreaIDClosestToPoint(Vector3 thePoint)
    {
        float smallestDistance = float.MaxValue;
        int id = -1;
        float smallestDistanceAbove = float.MaxValue;
        int idAbove = -1;

        foreach (ComplexWayPointArea area in Areas)
        {
            foreach (Vector3 point in area.PatrolPoints.Points)
            {
                if(Vector3.Distance(thePoint,point) < smallestDistance)
                {
                    id = area.ID;
                    smallestDistance = Vector3.Distance(thePoint, point);
                
                }
                if(point.y >= thePoint.y - 1.5 && Vector3.Distance(thePoint, point) < smallestDistanceAbove)
                {
                    idAbove = area.ID;
                    smallestDistanceAbove = Vector3.Distance(thePoint, point);

                }
            }
        }
        if(idAbove != -1)
        {
            return idAbove;
        }
        return id;
    }
    /// <summary>
    /// Get The point in the complexWaypoin system that is closed to the input point
    /// </summary>
    /// <param name="thePoint"></param>
    /// <returns></returns>
    public Vector3 GetClosestToPoint(Vector3 thePoint)
    {
        float smallestDistance = float.MaxValue;
        //int id = -1;
        Vector3 closestPoint = Vector3.zero;
        foreach (ComplexWayPointArea area in Areas)
        {
            foreach (Vector3 point in area.PatrolPoints.Points)
            {
                if (Vector3.Distance(thePoint, point) < smallestDistance)
                {
                    closestPoint = point;
                     smallestDistance = Vector3.Distance(thePoint, point);
                }
            }
        }
        return closestPoint;
    }
    /// <summary>
    /// Return a Zero vector on fail
    /// </summary>
    /// <param name="Areaid"></param>
    /// <param name="thePoint"></param>
    /// <returns></returns>
    public Vector3 GetClosestPointInAreaToPoint(int Areaid , Vector3 thePoint)
    {
        if(Areaid < 0 || Areaid >= Areas.Count)
        {
            return Vector3.zero; 
        }
        float smallestDistance = float.MaxValue;
        Vector3 closestPoint = Vector3.zero;
        
            foreach (Vector3 point in Areas[Areaid].PatrolPoints.Points)
            {
                if (Vector3.Distance(thePoint, point) < smallestDistance)
                {
                    closestPoint = point;
                    smallestDistance = Vector3.Distance(thePoint, point);
                }
            }
        
        return closestPoint;
    }

    private void OnDrawGizmos()
    {
        if(enabled ==false)
        {
            return;
        }

        if (Areas == null || Areas.Count == 0)
        {
            if (Application.isPlaying)
            {
                //Debug.LogWarning("No points in Complex Waypoints");
            }
            return;

        }



        ///The last point of the laster inner loop
        Vector3 lastEND = Vector3.zero;
        /// The First point
        Vector3 first = Vector3.zero;
        int i = 0;

        foreach (ComplexWayPointArea area in Areas)
        {
            Vector3 min = new Vector3(float.MaxValue, float.MaxValue);
            Vector3 max = Vector3.zero;
            Vector3 last = Vector3.zero;

 

            last = area.PatrolPoints.Last;
            int id = 0;
            if (area.ComplexWaypointConnections != null)
            {
                foreach (ComplexWaypointConnection coneect in area.ComplexWaypointConnections)
                {
                    
                    last = area.PatrolPoints.Last;
                    if (coneect != null && !coneect.IsDummyNode)
                    {
                        if (coneect.Path.Count > 0)
                        {
                            foreach (Vector3 point in coneect.Path)
                            {
                                // GizmoHelper.drawString("From "+area.ID + " to " + id, point, 0, 1, Color.red);
                                Gizmos.color = Color.green;
                                Gizmos.DrawSphere(point, 0.1f);


                                if (last != Vector3.zero && !coneect.IsDummyNode)
                                {

                                    Gizmos.color = OneWayConnectedPath;
                                    if (!coneect.IsOneDirection)
                                    {
                                        Gizmos.color = TwoWayConnectedPath;
                                    }

                                    Gizmos.DrawLine(last, point);

                                }
                                last = point;

                            }
                            Gizmos.color = OneWayConnectedPath;

                            if (!coneect.IsOneDirection)
                            {
                                Gizmos.color = TwoWayConnectedPath;

                                if (!coneect.IsDummyNode)
                                {
                                    // Handles.DrawDottedLine(last, Areas[id].PatrolPoints.Last, 0);
                                    Gizmos.DrawLine(last, Areas[id].PatrolPoints.Last);
                                    Gizmos.DrawLine(area.PatrolPoints.First, coneect.Path.First);

                                }
                                else
                                {
                                    Gizmos.DrawLine(coneect.actualyNode.Path.Last, Areas[coneect.actualyNode.ToArea].PatrolPoints.Last);
                                }
                            }

                            if (!coneect.IsDummyNode)
                            {
                                Gizmos.DrawLine(last, Areas[id].PatrolPoints.First);

                            }
                            else
                            {
                                Gizmos.color = TwoWayConnectedPath;
                                Gizmos.DrawSphere(last, 1f);
                            }
                        }
                    }
                    id++;
                }
            }

            last = Vector3.zero;
            foreach (Vector3 point in area.PatrolPoints.Points)
            {
                GizmoHelper.drawString(i.ToString(), point , 0, 1, Color.white);
                Gizmos.color = Color.black;
                Gizmos.DrawSphere(point , 0.1f);

               /* if (point.x < min.x)
                {
                    min.x = point.x;
                }
                if (point.y < min.y)
                {
                    min.y = point.y;
                }
                if (point.x > max.x)
                {
                    max.x = point.x;
                }
                if (point.y > max.y)
                {
                    max.y = point.y;
                }*/



                if (last != Vector3.zero)
                {
                    Gizmos.color = PatrolPathColor;
                    Gizmos.DrawLine(last, point);
                }
                last = point;
                i++;
            }

            /*Gizmos.color = PatrolAreaColor;
            Gizmos.DrawWireCube(Vector2.Lerp(min, max, 0.5f), new Vector2((max.x - min.x) + 2, (max.y - min.y) + 2));*/

            Gizmos.color = PatrolPathColor;
            Gizmos.DrawLine(last, area.PatrolPoints.Points[0]);

        }

        
        if (first != Vector3.zero)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, first);
        }
    }

    public IEnumerator GetEnumerator()
    {
        return ((IEnumerable)Areas).GetEnumerator();
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return base.ToString();
    }
      
}
/// <summary>
/// A custom exption thrown by the path building 
/// </summary>
public class ComplexExitPathBuildingException :Exception
  {
    public ComplexExitPathBuildingException(string text) : base(text)
    {
       
    }
}
