﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Inputs;
using System.Collections.Generic;
using static Inputs.InputManager;

namespace RoboMania.UI
{
    public class UIWindow : MonoBehaviour
    {
        /// <summary>
        /// The button to close the window can be null 
        /// </summary>
        public Button CloseButton;

        /// <summary>
        /// The button to open the window can be null
        /// </summary>
        public Button OpenButton;

        public GameObject VisibleWindowRootObject;

        public RectTrasfomAnimator openAnmation;
        public RectTrasfomAnimator CloseAnmation;
        public bool OverideHideWindow = false;

        public bool IsOpen { get { return _isOpen; } }

        protected static UIWindow _openWindow;
        protected bool _isOpen;

        #region input
        protected List<RegisteredCallback> _registeredInputCallbacks = new List<RegisteredCallback>();

        // <summary>
        /// Disables (but does not cleanup) any registered input callbacks so they are not processed. Use this if the callbacks
        /// only need to be ignored for a short period of time.
        /// </summary>
        protected void DisableRegisteredInputCallbacks()
        {
            foreach (RegisteredCallback callback in _registeredInputCallbacks)
            {
                InputManager.Instance.SetEnabledRegisteredInputCallback(callback, false);
            }
        }

        /// <summary>
        /// Re-enables any registered input callbacks so they are processed once again. Does nothing if they were already enabled.
        /// </summary>
        protected void EnableRegisteredInputCallbacks()
        {
            foreach (RegisteredCallback callback in _registeredInputCallbacks)
            {
                InputManager.Instance.SetEnabledRegisteredInputCallback(callback, true);
            }
        }

        /// <summary>
        /// Cleans up any registered input callbacks so they are not processed after the menu has been destroyed/deactivated.
        /// </summary>
        protected void CleanupRegisteredInputCallbacks()
        {
            foreach (RegisteredCallback callback in _registeredInputCallbacks)
            {
                InputManager.Instance.RemoveRegisteredInputCallback(callback);
            }
        }
        #endregion

















        // Use this for initialization
        public virtual void Start()
        {
            if (CloseButton && OpenButton)
            {
                CloseButton.onClick.AddListener(Close);
                OpenButton.onClick.AddListener(Show);
            }
            else if (OpenButton)
            {
                OpenButton.onClick.AddListener(Toggle);
            }
            else if (CloseButton)
            {
                CloseButton.onClick.AddListener(Close);
            }
        }

        public virtual void Close()
        {
            if (CloseAnmation)
            {
                CloseAnmation.SetFrameOne();
                CloseAnmation.Run();
                CloseAnmation.OnDone.AddListener(() => { VisibleWindowRootObject.SetActive(false); });
            }
            else
            {
                _isOpen = false;
                _openWindow = null;
                //Time.timeScale = 1;
                VisibleWindowRootObject.SetActive(false);
                Closed();
            }

        }

        public virtual void Closed()
        {

        }

        public virtual void Shown()
        {

        }
        /// <summary>
        /// Window should be populated in sub class brfoe calling base.show() 
        /// </summary>
        public virtual void Show()
        {

            if (_openWindow != null && _openWindow != this)
            {
                _openWindow.Close();
            }
            _openWindow = this;

            // Time.timeScale = 0;
            if (openAnmation)
            {
                openAnmation.SetFrameOne();
                VisibleWindowRootObject.SetActive(true);
                openAnmation.Run();
            }
            else
            {
                VisibleWindowRootObject.SetActive(true);
                transform.localScale.Set(1, 1, 1);
            }

            _isOpen = true;
            Shown();
        }


        public void Toggle()
        {
            if (_isOpen)
            {
                Close();
            }
            else
            {
                Show();
            }
        }
    }
}
