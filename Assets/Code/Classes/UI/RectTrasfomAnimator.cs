﻿using UnityEngine;
using System.Collections;
using static Easing;
using UnityEngine.Events;

public class RectTrasfomAnimator : MonoBehaviour
{
    public EaseType easeType;
    public float RunTime;
    public RectTransform RectTransform;
    public enum AnimationType {SCALE,POSITION}
    public Space Space;
    public AnimationType animationType;
    public Vector3 From;
    public Vector3 To;
    public bool Done => (_runningTime >= RunTime);
    private float _runningTime = 0;

    public UnityEvent OnDone = new UnityEvent();

    private bool _run = false;

    public void Start()
    {
       
    }

    public void Run()
    {
        _runningTime = 0;
        _run = true;
    }

    public void SetFrameOne()
    {
        if (animationType == AnimationType.SCALE)
        {
            RectTransform.localScale = From;
        }
        if (animationType == AnimationType.POSITION)
        {
            if (Space == Space.Self)
            {
                RectTransform.localPosition = From;
            }
            else
            {
                RectTransform.position = From;
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if(_run)
        {
            if (animationType == AnimationType.SCALE)
            {
                RectTransform.localScale = Vector3.Lerp(From, To, Easing.GetFuctionFromEnum(easeType)(_runningTime / RunTime));
            }
            if (animationType == AnimationType.POSITION)
            {
                if (Space == Space.Self)
                {
                    RectTransform.localPosition = Vector3.Lerp(From, To, Easing.GetFuctionFromEnum(easeType)(_runningTime / RunTime));
                }
                else
                {
                    RectTransform.position = Vector3.Lerp(From, To, Easing.GetFuctionFromEnum(easeType)(_runningTime / RunTime));
                }
            }

            _runningTime += Time.deltaTime;
            if (Done)
            {
                _run = false;
            }
        }
    }
}
