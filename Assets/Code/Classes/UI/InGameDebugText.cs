﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections.Generic;

public class InGameDebugText : MonoBehaviour
{
    public Text DebugTextElm;
    public static List<InGameDebugText> DebugText = new List<InGameDebugText>();


    public void Start()
    {
        DebugText.Add(this);
    }

    public void ShowText(string text , Vector3 worldPos)
    {
        DebugTextElm.gameObject.SetActive(true);
        DebugTextElm.text = text;
        DebugTextElm.rectTransform.position = Camera.main.WorldToScreenPoint(worldPos);
    }


    public void SetText(string text)
    {
        DebugTextElm.gameObject.SetActive(true);
        DebugTextElm.text = text;
    }
    public void HiddeText(string text)
    {
        DebugTextElm.gameObject.SetActive(false);
    }
    private void OnDestroy()
    {
        DebugText.Remove(this);
    }
}