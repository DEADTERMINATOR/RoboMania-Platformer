﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Inputs;

namespace RoboMania.UI
{
    public class PopUpHandle
    {
        internal string Title;
        internal string Text;
        internal string OkText;
        internal string CancelText;

        /// <summary>
        /// Fired when any text popuo is closed
        /// </summary>
        public UnityEvent OnOK = new UnityEvent();
        public UnityEvent OnCancel = new UnityEvent();
        public UnityEvent OnAny = new UnityEvent();

        public bool IsVisible { get; internal set; }
        public bool HasBeenShown { get; internal set; }
        public bool Ready;

        public InputManager.RegisteredCallback SelectCallback;
        public InputManager.RegisteredCallback BackCallback;

        private InputManager _inputManagerInstanceRef;

        public PopUpHandle(string title, string text, string ok = "OK", string cancel = "Cancel", bool ready = false)
        {
            Title = title;
            Text = text;
            OkText = ok;
            CancelText = cancel;
            Ready = ready;

            _inputManagerInstanceRef = InputManager.Instance;
            //_inputManagerInstanceRef.EnablePopUpActions();
            
             SelectCallback = _inputManagerInstanceRef.RegisterSpecificActionPerformed("PopUpSelect", () => 
            { 
                OnOK?.Invoke();
                OnAny?.Invoke();
                UIPopUp.OnOKGlobal?.Invoke(); 
                if (UIPopUp.CloseOnOk)
                {
                    UIPopUp._windowRef.Close();
                }
            }, false);
            BackCallback = _inputManagerInstanceRef.RegisterSpecificActionPerformed("PopUpBack", () => 
            { 
                OnCancel?.Invoke();
                OnAny?.Invoke();
                UIPopUp.OnCancelGlobal?.Invoke(); 
                if (UIPopUp.CloseOnCancel)
                {
                    UIPopUp._windowRef.Close();
                }
            }, false);
           // _inputManagerInstanceRef.DisablePopUpActions();
        }


        public void DisablePopUpHandle()
        {
            _inputManagerInstanceRef.SetEnabledRegisteredInputCallback(SelectCallback, false);
            _inputManagerInstanceRef.SetEnabledRegisteredInputCallback(BackCallback, false);
        }

        ~PopUpHandle()
        {
            _inputManagerInstanceRef.RemoveRegisteredInputCallback(SelectCallback);
            _inputManagerInstanceRef.RemoveRegisteredInputCallback(BackCallback);
        }
    }


    public class UIPopUp : MonoBehaviour
    {
        /// <summary>
        /// Static ref to last instantiated instance 
        /// </summary>
        internal static UIPopUp _windowRef;
        internal static Queue<PopUpHandle> Queue = new Queue<PopUpHandle>();
        internal static new  UIPopUp _openWindow;
        public static PopUpHandle CurrentHandle;
        public Animator Animator;
        public GameObject VisibleWindowRootObject;

        protected bool _isOpen;

        #region UIElements
        public TextMeshProUGUI MainTextUI;
        public Text OkButtonTextUI;
        public Text CancelButtonTextUI;
        public Text TitleTextUI;
        public Button OkButton;
        public Button CancelButton;

        #endregion

        #region Events
        public static UnityEvent OnOKGlobal;
        public static UnityEvent OnCancelGlobal;
        #endregion

        #region Settings
        public static bool CloseOnOk = true;
        public static bool CloseOnCancel = true;
        #endregion


        private void Awake()
        {
            OnOKGlobal = new UnityEvent();
            OnCancelGlobal = new UnityEvent();
            OkButton?.onClick.AddListener(OKClicked);
            CancelButton?.onClick.AddListener(CancelClicked);

            _windowRef = this;
        }

        /// <summary>
        /// tray to show a textpopup if one or more is showing/queued it will be added to the queue
        /// </summary>
        /// <param name="title">The Title of the popup</param>
        /// <param name="text">The main Text can use TextMesh pro rich markup</param>
        /// <param name="readyToShow"></param>
        /// <param name="ok"></param>
        /// <param name="cancel"></param>
        /// <returns></returns>
        public static PopUpHandle Show(string title, string text,  string ok = "OK", string cancel = "Cancel", bool readyToShow = true) //Basically a handle factory 
        {
            PopUpHandle handle =  new PopUpHandle(title, text, ok, cancel, readyToShow);
            Show(handle);

            return handle;
        }
        /// <summary>
        /// tray to show a textpopup if one or more is showing/queued it will be added to the queue
        /// </summary>
        /// <param name="title">The Title of the popup</param>
        /// <param name="text">The main Text can use TextMesh pro rich markup</param>
        /// <param name="readyToShow"></param>
        /// <param name="ok"></param>
        /// <param name="cancel"></param>
        /// <returns></returns>
        public static PopUpHandle Make(string title, string text, string ok = "OK", string cancel = "Cancel") //Basically a handle factory 
        {
            PopUpHandle handle = new PopUpHandle(title, text, ok, cancel, true);
            return handle;
        }
        /// <summary>
        /// try and show a window with your 
        /// </summary>
        /// <param name="handle"></param>
        public static void Show(PopUpHandle handle)
        {
            if (!Queue.Contains(handle) && CurrentHandle != handle)
            {
                if (_windowRef._isOpen || Queue.Count > 0)
                {
                    Queue.Enqueue(handle);
                }
                else
                {
                    var instance = InputManager.Instance;

                    instance.DisableCurrentActions();
                    instance.EnablePopUpActions();

                    instance.SetEnabledRegisteredInputCallback(handle.SelectCallback, true);
                    instance.SetEnabledRegisteredInputCallback(handle.BackCallback, true);
                    
                    _windowRef.ShowNow(handle);
                }
            }
        }

        protected void ShowNow(PopUpHandle handle)
        {
            _isOpen = true;
            CurrentHandle = handle;
            MainTextUI.text = handle.Text;
            OkButtonTextUI.text = handle.OkText;
            CancelButtonTextUI.text = handle.CancelText;
            TitleTextUI.text = handle.Title;
            if (_openWindow != null)
            {
                _openWindow.Close();
                
            }
            _openWindow = this;
            VisibleWindowRootObject.SetActive(true);
            
        }

        protected void CloseNow()
        {
            _openWindow = null;
            VisibleWindowRootObject.SetActive(false);
            Closed();
        }

        protected virtual void OKClicked()
        {
            CurrentHandle?.OnOK?.Invoke();
            CurrentHandle?.OnAny?.Invoke();
            OnOKGlobal?.Invoke();
            if (CloseOnOk)
            {
                Close();
            }
        }

        protected virtual void CancelClicked()
        {
            CurrentHandle?.OnCancel?.Invoke();
            CurrentHandle?.OnAny?.Invoke();
            OnCancelGlobal?.Invoke();
            if (CloseOnCancel)
            {
                Close();
            }
        }

        public  void Close()
        {
            CurrentHandle.DisablePopUpHandle();
            _isOpen = false;
            _openWindow = null;
            VisibleWindowRootObject.SetActive(false);
            var instance = InputManager.Instance;

            instance.DisablePopUpActions();
            instance.EnableLastActions();


            if (Queue.Count > 0)
            {
                ShowNow(Queue.Dequeue());
            }
            CurrentHandle = null;
        }

        public virtual  void Closed()
        {
            
        }
    }
}