﻿using UnityEngine;
using System.Collections;
using RoboMania.UI;

public class InGameUIWindowActivator : MonoBehaviour, IActivatable 
{
    public UIWindow window;
    public bool Active { get { return window.IsOpen; } }


    public void Activate(GameObject activator)
    {
        window.Show();
    }

    public void Deactivate(GameObject activator)
    {
        window.Close();
    }
}
