﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;

public static class GlobalData
{
    public delegate void WorldEvent();
    public static event WorldEvent SceneTransition;

    public enum DamageType : int { PROJECTILE, MELEE, ENVIROMENT, SHIELD, CONSTANT, REDUCED, INCREASED, GUARDIANS, NULL } //TODO It's debatable whether this belongs in a global class, but since the player, NPCs, and possibly even some objects could take damage, it should be outside a character class.
    public enum GravityType { Base, Lava, Mud } //TODO: Same as above

    public const int IGNORE_RAYCAST_LAYER = 2;
    public const int PLAYER_LAYER = 8;
    public const int OBSTACLE_LAYER = 9;
    public const int TRIGGER_LAYER = 11;
    public const int NPC_LAYER = 12;
    public const int ENEMY_LAYER = 13;
    public const int CAMERA_MOVEMENT = 14;
    public const int PROJECTILE_LAYER = 16;
    public const int SHIELD_LAYER = 19;

    public const int IGNORE_RAYCAST_LAYER_SHIFTED = 1 << IGNORE_RAYCAST_LAYER;
    public const int PLAYER_LAYER_SHIFTED = 1 << PLAYER_LAYER;
    public const int OBSTACLE_LAYER_SHIFTED = 1 << OBSTACLE_LAYER;
    public const int TRIGGER_LAYER_SHIFTED = 1 << TRIGGER_LAYER;
    public const int NPC_LAYER_SHIFTED = 1 << NPC_LAYER;
    public const int ENEMY_LAYER_SHIFTED = 1 << ENEMY_LAYER;
    public const int CAMERA_MOVEMENT_SHIFTED = 1 << CAMERA_MOVEMENT;
    public const int PROJECTILE_LAYER_SHIFTED = 1 << PROJECTILE_LAYER;
    public const int SHIELD_LAYER_SHIFTED = 1 << SHIELD_LAYER;

    public const string PREFAB_GUNCHIP_TEMPLATE_PATH = "Prefabs/Weapons/Chips/Gun Chip Template 1";
    
    public const string PLAYER_TAG = "Player";

    public const short FACING_LEFT = -1;
    public const short FACING_RIGHT = 1;

    public const float CONTROLLER_STICK_DEADZONE = 0.35f;

    public static GameObject PREFAB_SHIELD_PICKUP_TEMPLATE; //= Resources.Load("Prefabs/Shield/ShieldPickUpTemplate") as GameObject;
    public static Vector3 VEC3_OFFSCREEN = new Vector3(-999999, 999999, -99999);

    public static PlayerMaster Player;
    public static Transform PlayerTransfrom { get { return Player.transform; } }
    public static Timekeeper Timekeeper;
    public static CameraManager CameraManager;
    public static bool UseDebugSettings = true;
    public static bool IsANewGame = false;


    public static float DefultZoom = 15;

    public static string CURRENT_SAVE_FILE_NAME = "Save.es3";
    public static string CURRENT_PLAYER_SAVE_FILE_NAME = "Player.es3";
    public static string CURRENT_LEVEL_SAVE_FILE_NAME = "LevelData.es3";
    public static void Init()
    {
        GameObject TimeKeeperGO = GameObject.Find("Timekeeper");
        if (TimeKeeperGO != null)
        {
            Timekeeper = TimeKeeperGO.GetComponent<Timekeeper>();
        }
        GameObject PlayerGo  = GameObject.Find("Player");
        if (PlayerGo != null)
        {
            Player = PlayerGo.GetComponent<PlayerMaster>();
        }
        else
        {
            Debug.Log("Player GO not found");
        }
    }

    public static void NotifySceneTransition()
    {
        SceneTransition?.Invoke();
    }

    public static Color GetDisplayColorFromRarity(Rarity rarity)
    {
        Color color;
        //TODO: Move color hex to cons strings  
        switch(rarity)
        {
            case Rarity.UNCOMMON:   
                ColorUtility.TryParseHtmlString("#10ff10", out color);
                break;
            case Rarity.RARE:
                ColorUtility.TryParseHtmlString("#3333ff", out color);
                break;
            case Rarity.EPIC:
                ColorUtility.TryParseHtmlString("#551A8BCC", out color);
                break;
            default:
                ColorUtility.TryParseHtmlString("#f2f2f2", out color);
                break;
        }

        return color;
    }

    public static class PlayerDynamicModules
    {
        public const string JUMP = "Jump";
        public const string MULTI_JUMP = "Multi Jump";
        public const string DASH = "Dash";
        public const string WALL_SLIDE = "Wall Slide";
        public const string WALL_GRIP = "Wall Grip";
        public const string HOVER = "Hover";
        public const string HOVER_RAISE = "Hover Raise";
        public enum Enum { JUMP, MULTI_JUMP, DASH, WALL_SLIDE, WALL_GRIP, HOVER, HOVER_RAISE }
        public static string EnumToString(Enum value)
        {
            switch(value)
            {
                case Enum.JUMP: return JUMP;
                case Enum.MULTI_JUMP: return MULTI_JUMP;
                case Enum.DASH: return DASH;
                case Enum.WALL_SLIDE: return WALL_SLIDE;
                case Enum.WALL_GRIP: return WALL_GRIP;
                case Enum.HOVER: return HOVER;
                case Enum.HOVER_RAISE: return HOVER_RAISE;
                default: return JUMP;
            }
        }
             

    }
   
}

