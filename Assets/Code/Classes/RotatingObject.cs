﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RotatingObject : MonoBehaviour, IActivatable
{
    public enum Axis { X, Y, Z }

    public Axis RotationAxis { get { return axis; } }
    public bool Active { get { return active; } }


    [SerializeField]
    protected bool active;
    [SerializeField]
    protected Axis axis = Axis.Z;
    [SerializeField]
    protected Vector3 initialRotation;

    protected float currentX;
    protected float currentY;
    protected float currentZ;


    protected virtual void Awake()
    {
        currentX = initialRotation.x;
        currentY = initialRotation.y;
        currentZ = initialRotation.z;
    }

    public virtual void Activate(GameObject activator)
    {
        active = true;
    }

    public virtual void Deactivate(GameObject activator)
    {
        active = false;
    }
}
