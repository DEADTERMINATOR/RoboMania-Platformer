﻿using UnityEngine;
using System.Collections;
using Characters;

public enum Rarity : int { COMMON, UNCOMMON, RARE, EPIC, LEGENDARY, COUNT }

public class Shield : MonoBehaviour, IDamageGiver, IDamageReceiver
{

    public Character ShieldHolder;
    public float MaxPower;
    public float Power;
    public Collider2D Collider2D;
    public Rarity Rarity;
    /// <summary>
    /// Handel dame revived by the shield if the dame is grate then the power the extra is pass on to the player  
    /// </summary>
    /// <param name="giver"></param>
    /// <param name="amount"></param>
    /// <param name="type"></param>
    /// <param name="hitPoint"></param>
    public void TakeDamage(IDamageGiver giver, float amount, GlobalData.DamageType type, Vector3 hitPoint)
    {
        if (type != GlobalData.DamageType.ENVIROMENT)
        {
            if(giver is Projectile)
            {
                ((Projectile)giver).Kill();
            }
            Power -= amount;
            if(Power< -1 )
            {
                ShieldHolder?.TakeDamage(this, -Power, GlobalData.DamageType.SHIELD, hitPoint);
                Destroy(gameObject);
            }
        }
    }


    public void Init(float power,Rarity rarity)
    {
        MaxPower = power;
        Power = power;
        Rarity = rarity;
    }


    public void Load(float power, float maxPower, Rarity rarity)
    {
        MaxPower = maxPower;
        Power = power;
        Rarity = rarity;
    }
}