﻿using System;
using UnityEditor;
namespace LevelSystem
{
    [Serializable]
    public class Difficulty 
    {
        public enum Values { EASY, MEDIUM, HARD,ULTRA }
        /// <summary>
        /// Unity Editor interface
        /// </summary>
        public Values Setting;
        public Difficulty(int i)
        {
            Setting = (Values)i;
        }
        public Difficulty(Values E)
        {
            Setting = E;
        }

        #region Conversion Operators
        public static implicit operator int(Difficulty d)
        {
            return (int)d.Setting;
        }
        public static implicit operator Difficulty(Values e)
        {
            return new Difficulty(e);
        }
        #endregion
        #region Equality Operators
        public static bool operator == (Difficulty lhs , Difficulty rhs)
        {
            return (lhs.Setting == rhs.Setting);
        }
        public static bool operator !=(Difficulty lhs, Difficulty rhs)
        {
            return lhs.Setting != rhs.Setting;
        }
        public static bool operator >=(Difficulty lhs, Difficulty rhs)
        {
            return (lhs.Setting >= rhs.Setting);
        }
        public static bool operator <=(Difficulty lhs, Difficulty rhs)
        {
            return lhs.Setting >= rhs.Setting;
        }
        public static bool operator ==(Difficulty lhs, Values rhs)
        {
            return (lhs.Setting == rhs);
        }
        public static bool operator !=(Difficulty lhs, Values rhs)
        {
            return lhs.Setting != rhs;
        }
        public static bool operator >=(Difficulty lhs, Values rhs)
        {
            return (lhs.Setting >= rhs);
        }
        public static bool operator <=(Difficulty lhs, Values rhs)
        {
            return lhs.Setting >= rhs;
        }
        public override bool Equals(object obj)
        {
            return Setting.Equals(obj);
        }
        #endregion
        public override int GetHashCode()
        {
            return Setting.GetHashCode();
        }
        public override string ToString()
        {
            return Setting.ToString().ToLower();
        }
    }

    public class DifficultyUnitTest 
    {
        public DifficultyUnitTest()
        {
            Difficulty d = Difficulty.Values.EASY;
            if (d >= Difficulty.Values.EASY)
            {
                
            }
        }

    }
}
