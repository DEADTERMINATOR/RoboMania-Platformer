﻿using UnityEngine;
using System.Collections;

public class IntangerInWrappingRange
{
    private int _value;
    private int _maxValue;
    private int _minValue;
    public IntangerInWrappingRange(int max, int value =0 , int min = 0)
    {
        _maxValue = max;
        _minValue = min;
        _value = value;
    }

    private void Incrment()
    {
        _value++;
        if(_value > _maxValue)
        {
            _value = _minValue;
        }
    }
    private void Decrement()
    {
        _value--;
        if (_value < _minValue)
        {
            _value = _maxValue;
        }
    }
    public static IntangerInWrappingRange operator ++(IntangerInWrappingRange c1)
    {
        c1.Incrment();
        return c1;
    }
    public static IntangerInWrappingRange operator --(IntangerInWrappingRange c1)
    {
        c1.Decrement();
        return c1;
    }
    public static implicit operator int(IntangerInWrappingRange i)  
    {
        return i._value;  // implicit conversion
    }
    public static bool operator ==(int c1, IntangerInWrappingRange c2)
    {
        return c1 == c2._value;
    }
    public static bool operator !=(int c1, IntangerInWrappingRange c2)
    {
        return c1 != c2._value;
    }
    public static bool operator ==(IntangerInWrappingRange c1, int c2)
    {
        return c1._value ==c2;
    }
    public static bool operator !=(IntangerInWrappingRange c1, int c2)
    {
        return c1._value != c2;
    }
    public override bool Equals(object obj)
    {
        if ((obj == null) || !this.GetType().Equals(obj.GetType()))
        {
            return false;
        }
        else
        {
            var integer = (IntangerInWrappingRange)obj;
            return integer._value == _value;
        }
    }

    public override int GetHashCode()
    {
        return -1939223833 + _value.GetHashCode();
    }
}
