﻿using UnityEngine;
using System.Collections;
using Characters.Player;
using UnityEditor;

/// <summary>
/// A set of util methods that are static.
/// </summary>
public class StaticTools  
{
    public const string PLAYER_TAG = "Player";
    public const string SHEILD_TAG = "Shield";

    /// <summary>
    /// Get the player ref
    /// </summary>
    /// <returns>A reference to the player.</returns>
    public static PlayerMaster GetPlayerRef()
    {
        ///Updated to new global so legacy scripts will function
       return GlobalData.Player;
    }

    /// <summary>
    /// Force a string to be all lowecase with an upper case letter in the first pos
    /// </summary>
    /// <param name="str">The string to be modified.</param>
    /// <returns>The modified string.</returns>
    public static string FirstLetterToUpper(string str)
    {
        if (str == null)
            return null;
        str = str.ToLower();
        if (str.Length > 1)
            return char.ToUpper(str[0]) + str.Substring(1);

        return str.ToUpper();
    }

    public static float LinearToDecibel(float linear)
    {
        float dB;

        if (linear != 0)
            dB = 20.0f * Mathf.Log10(linear);
        else
            dB = -144.0f;

        return dB;
    }
    /// <summary>
    /// A check to see if the game obj is taged as a player
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static bool IsPlayerTagedObj(GameObject obj)
    {
        return obj.tag == PLAYER_TAG;
    }
    /// <summary>
    /// A check to see if the game obj is taged as a sheild
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static bool IsSheildTagedObj(GameObject obj)
    {
        return obj.tag == SHEILD_TAG;
    }

    /// <summary>
    /// A check to see if the game obj is taged as a player
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static bool IsPlayerOrSheildTagedObj(GameObject obj)
    {
        return (obj.tag == PLAYER_TAG || obj.tag == SHEILD_TAG) ;
    }

    /// <summary>
    /// Compares two float values to see if they are the same value within a specified threshold.
    /// </summary>
    /// <param name="a">The first float.</param>
    /// <param name="b">The second float.</param>
    /// <param name="threshold">The comparison threshold.</param>
    /// <returns>True if the values are the same within the threshold, false otherwise.</returns>
    public static bool ThresholdApproximately(float a, float b, float threshold)
    {
        return ((a - b) < 0 ? ((a - b) * -1) : (a - b)) <= threshold;
    }

    /// <summary>
    /// Compares two Vector2 values to see if they are the same value within a specified threshold.
    /// </summary>
    /// <param name="a">The first Vector2.</param>
    /// <param name="b">The second Vector2.</param>
    /// <param name="threshold">The comparison threshold.</param>
    /// <returns>True if the values are the same within the threshold, false otherwise.</returns>
    public static bool ThresholdApproximately(Vector2 a, Vector2 b, float threshold)
    {
        return (ThresholdApproximately(a.x,b.x,threshold) && ThresholdApproximately(a.y, b.y, threshold));
    }

    /// <summary>
    /// Compares two Vecter3 values to see if they are the same value within a specified threshold.
    /// </summary>
    /// <param name="a">The first Vector3.</param>
    /// <param name="b">The second Vector3.</param>
    /// <param name="threshold">The comparison threshold.</param>
    /// <returns>True if the values are the same within the threshold, false otherwise.</returns>
    public static bool ThresholdApproximately(Vector3 a, Vector3 b, float threshold)
    {
        return (ThresholdApproximately(a.x, b.x, threshold) && ThresholdApproximately(a.y, b.y, threshold) && ThresholdApproximately(a.z, b.z, threshold));
    }

    /// <summary>
    /// Produces a Vector2 from a specified angle.
    /// </summary>
    /// <param name="angle">The angle to convert to a Vector2.</param>
    /// <returns>The converted Vector2.</returns>
    public static Vector2 Vector2FromAngle(float angle)
    {
        angle *= Mathf.Deg2Rad;
        return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
    }

    /// <summary>
    /// Rotates a Vector2 by a specified angle.
    /// </summary>
    /// <param name="vector">The vector to rotate.</param>
    /// <param name="angle">The angle to rotate by.</param>
    /// <returns></returns>
    public static Vector2 RotateVector2(Vector2 vector, float angle)
    {
        angle *= Mathf.Deg2Rad;
        return new Vector2(vector.x * Mathf.Cos(angle) - vector.y * Mathf.Sin(angle), vector.x * Mathf.Sin(angle) + vector.y * Mathf.Cos(angle));
    }

    /// <summary>
    /// Finds the closest point on a provided collider given a specified point.
    /// </summary>
    /// <param name="col">The collider to find the closest point on.</param>
    /// <param name="point">The point to check against the collider.</param>
    /// <returns>A Vector2 representing the closes position on the collider.</returns>
    public static Vector2 ClosestPoint(Collider2D col, Vector2 point)
    {
        GameObject go = new GameObject("tempCollider");
        go.transform.position = point;
        CircleCollider2D c = go.AddComponent<CircleCollider2D>();
        c.radius = 0.1f;
        ColliderDistance2D dist = col.Distance(c);
        Object.Destroy(go);
        //Debug.DrawLine(dist.pointA - Vector2.right, dist.pointA + Vector2.right, Color.red, 5.0f);
        //Debug.DrawLine(dist.pointA - Vector2.up, dist.pointA + Vector2.up, Color.red, 5.0f);

        return dist.pointA;
    }

    public static float shortAngleDist(float a0, float a1)
    {
        var max = Mathf.PI * 2;
        var da = (a1 - a0) % max;
        return 2 * da % max - da;
    }

    public static float angleLerp(float a0, float a1, float t)
    {
        return a0 + shortAngleDist(a0, a1) * t;
    }

    public static Vector3 ExpoEaseIn(Vector3 startValue, Vector3 changeInValue, float currentTime, float totalTime)
    {
        return new Vector3(changeInValue.x * Mathf.Pow(2, 10 * (currentTime / totalTime - 1)) + startValue.x, changeInValue.y * Mathf.Pow(2, 10 * (currentTime / totalTime - 1)) + startValue.y, changeInValue.z * Mathf.Pow(2, 10 * (currentTime / totalTime - 1)) + startValue.z);
    }

    public static Vector2 ExpoEaseIn(Vector2 startValue, Vector2 changeInValue, float currentTime, float totalTime)
    {
        return new Vector2(changeInValue.x * Mathf.Pow(2, 10 * (currentTime / totalTime - 1)) + startValue.x, changeInValue.y * Mathf.Pow(2, 10 * (currentTime / totalTime - 1)) + startValue.y);
    }

    public static float ExpoEaseIn(float startValue, float changeInValue, float currentTime, float totalTime)
    {
        return changeInValue * Mathf.Pow(2, 10 * (currentTime / totalTime - 1)) + startValue;
    }

    public static float QuadEaseOut(float startValue, float changeInValue, float currentTime, float totalTime)
    {
        currentTime /= totalTime;
        return -changeInValue * currentTime * (currentTime - 2) + startValue;
    }

    public static float CubicEaseOut(float startValue, float changeInValue, float currentTime, float totalTime)
    {
        currentTime /= totalTime;
        --currentTime;
        return changeInValue * (Mathf.Pow(currentTime, 3) + 1) + startValue;
    }
    public static float EaseInSine(float start, float end, float currentTime, float totalTime)
    {
        end -= start;
        return -end * Mathf.Cos(currentTime/totalTime * (Mathf.PI * 0.5f)) + end + start;
    }
    public static Vector3 EaseInSine(Vector3 start, Vector3 end, float currentTime, float totalTime)
    {
 
        return new Vector3(EaseInSine(start.x,end.x,currentTime,totalTime), EaseInSine(start.y, end.y, currentTime, totalTime),EaseInSine(start.z, end.z, currentTime, totalTime));
    }

    public static float EaseInOutQuart(float start, float end, float currentTime, float totalTime)
    {
        float value = currentTime / totalTime;
        value /= .5f;
        end -= start;
        if (value < 1) return end * 0.5f * value * value * value * value + start;
        value -= 2;
        return -end * 0.5f * (value * value * value * value - 2) + start;
    }
    public static float EaseOutQuart(float start, float end, float currentTime, float totalTime)
    {
        float value = currentTime / totalTime;
        value--;
        end -= start;
        return -end * (value * value * value * value - 1) + start;
    }
    public static bool IsPointOnScreen(Vector3 point)
    {
        Vector3 foundpoint = Camera.main.WorldToViewportPoint(point);
        {
            return (foundpoint.x >= -0.05 && foundpoint.x <= 1.05) && (foundpoint.y >= -0.05 && foundpoint.y <= 1.05);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="point"></param>
    /// <param name="buffer"></param>
    /// <returns></returns>
    public static bool IsPointOnScreenWithBuffer(Vector3 point,float buffer = 0.1f)
    {
  
        Vector3 foundpoint = Camera.main.WorldToViewportPoint(point);
        {
            return (foundpoint.x >= -0-buffer && foundpoint.x <= 1+ buffer) && (foundpoint.y >= -0- buffer && foundpoint.y <= 1+ buffer);
        }
    }

    public static bool IsLayerInLayerMask(LayerMask mask, int layerToCheck)
    {
        return (mask & (1 << layerToCheck)) != 0;
    }

    /// <summary>
    /// Calculates the amount of time until two objects will collider, given the position for both objects and their respective velocities.
    /// </summary>
    /// <param name="initialPositionA">The initial position for the first object.</param>
    /// <param name="initialPositionB">The initial position for the second object.</param>
    /// <param name="aVelocity">The velocity for the first object.</param>
    /// <param name="bVelocity">The velocity for the second object.</param>
    /// <returns>The time until the collision. A positive time indicates that the collision will occur after that much time. A negative time indicates the collision has already occured.
    ///          And a non-zero time indicates that a collision will never happen.</returns>
    public static float CalculateTimeToCollisionOnOneAxis(float initialPositionA, float initialPositionB, float aVelocity, float bVelocity)
    {
        #region Collision Time Equation Explanation
        /* Calculate the position and time the two enemies will collide.
         * We only need to consider the X-axis in these calculations
         * Equation for finding an object position at a point in time - positionX(t) = initialPositionX + xVelocity * t
         * At the point of collision, the positions of both objects will be the same, so set positionX(t) for both objects
         * to be the same, and solve for t
         * initialPositionXA + aXVelocity * t = initialPositionXB + bXVelocity * t
         * => initialPositionXA + aXVelocity * t - bXVelocity * t = initialPositionXB + (bXVelocity * t) - (bXVelocity * t)
         * => initialPositionXA - initialPositionXA + aXVelocity * t - bXVelocity * t = initialPositionXB - initialPositionXA
         * => aXVelocity * t - bXVelocity * t = initialPositionXB - initialPositionXA
         * => t(aXVelocity - bXVelocity) = initialPositionXB - initialPositionXA
         * => t(aXVelocity - bXVelocity) / aXVelocity - bXVelocity = initialPositionXB - initialPositionXA / aXVelocity - bXVelocity
         * => t = initialPositionXB - initialPositionXA / aXVelocity - bXVelocity
         */
        #endregion

        return (initialPositionB - initialPositionA) / (aVelocity - bVelocity);
    }

    public static Vector2 InterceptTarget(Vector2 startingPosition, float interceptObjectSpeed, Vector2 targetPosition, Vector2 targetVelocity)
    {
        Vector2 totarget = targetPosition - startingPosition;

        float a = Vector2.Dot(targetVelocity, targetVelocity) - (interceptObjectSpeed * interceptObjectSpeed);
        float b = 2 * Vector2.Dot(targetVelocity, totarget);
        float c = Vector2.Dot(totarget, totarget);

        float p = -b / (2 * a);
        float q = Mathf.Sqrt((b * b) - 4 * a * c) / (2 * a);

        float t1 = p - q;
        float t2 = p + q;
        float t;

        if (t1 > t2 && t2 > 0)
        {
            t = t2;
        }
        else
        {
            t = t1;
        }

        Vector2 aimSpot = targetPosition + targetVelocity * t;
        return aimSpot;
        //Vector2 bulletPath = aimSpot - startingPosition;
        //float timeToImpact = bulletPath.Length() / bullet.speed; //speed must be in units per second
    }

    public static bool RectContains(Rect rect, Vector2 point)
    {
        return point.x >= rect.xMin && point.x <= rect.xMax && point.y >= rect.yMin && point.y <= rect.yMax;
    }

    public static void DrawPointGizmosOnly(Vector3 position, Color pointColor)
    {
        float size = 0.3f;

        Gizmos.color = pointColor;
        Gizmos.DrawLine(position - Vector3.up * size, position + Vector3.up * size);
        Gizmos.DrawLine(position - Vector3.left * size, position + Vector3.left * size);
    }

    public static void DrawPointGizmosOnly(Vector3 position, Color pointColor, string textToDraw, Color textColor)
    {
        DrawPointGizmosOnly(position, pointColor);

        #if UNITY_EDITOR
            Handles.color = textColor;
            Handles.Label(position, new GUIContent(textToDraw));
        #endif
    }
}
