﻿using UnityEngine;
using System.Collections;

public class NavPoint
{
    public Vector3 worldPos;
    public NavPoint Up;
    public NavPoint Down;
    public NavPoint Left;
    public NavPoint Right;
}


public class NavGrid : MonoBehaviour
{
    public Vector2[,] points;
    public bool Calc;
    public Vector2 AreaSize;
    public float PointSize =2;

    private void OnDrawGizmos()
    {
        //Gizmos.DrawWireCube(transform.position + new Vector3(AreaSize.x/2, AreaSize.y/2), AreaSize);
        if (Calc)
        {
            int Xcount = (int)(AreaSize.x / PointSize);
            int Ycount = (int)(AreaSize.y / PointSize);
            points = new Vector2[Xcount,Ycount];
            for(int X = 0; X < Xcount; X++ )
            {
                for (int Y = 0; Y < Ycount; Y++)
                {
                    points[X, Y] = (Vector2)transform.position + new Vector2(X * PointSize, Y * PointSize);
                    Gizmos.DrawSphere(points[X, Y], 0.03f);
                }
            }

        }
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
