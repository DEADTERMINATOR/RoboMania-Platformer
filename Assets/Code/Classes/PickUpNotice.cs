﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Contains The information of a pick up
/// </summary>
public class PickUpNotice
{
    public string Name;
    /// <summary>
    /// Discription of the item
    /// </summary>
    public string Discription;
}

