﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RigidbodyGravityDefaults
{
    /// <summary>
    /// The rigidbody's linear drag value when it does not have any gravity modifiers on it.
    /// </summary>
    public const float BASE_LINEAR_DRAG = 0;
    /// <summary>
    /// The rigidbody's angular drag value when it does not have any gravity modifies on it.
    /// </summary>
    public const float BASE_ANGULAR_DRAG = 0.05f;
    /// <summary>
    /// The rigidbody's gravity scale when it does not have any gravity modifiers on it.
    /// </summary>
    public const float BASE_GRAVITY_SCALE = 1;

    /// <summary>
    /// The rigidbody's linear drag value when it is in lava.
    /// </summary>
    public const float LAVA_LINEAR_DRAG = 2;
    /// <summary>
    /// The rigidbody's angular drag value when it is in lava.
    /// </summary>
    public const float LAVA_ANGULAR_DRAG = 0.05f;
    /// <summary>
    /// The rigidbody's gravity scale when it is in lava.
    /// </summary>
    public const float LAVA_GRAVITY_SCALE = 0.25F;
}
