﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Limb
{
    public string LimbName;
    public GameObject LimbObject;
    public ToggleLimbRendering LimbToggle;
    //public ToggleLimbRendering ManualLimbToggle;
    public Dictionary<string, GameObject> ManipulableElements;

    public Limb(string limbName, string limbPath, params Tuple<string, string>[] elementNamesAndPaths)
    {
        LimbName = limbName;

        LimbObject = GameObject.Find(limbPath);
        LimbToggle = LimbObject.GetComponent<ToggleLimbRendering>();

        /*
        if (manualLimbPath != "")
            ManualLimbToggle = GameObject.Find(manualLimbPath).GetComponent<ToggleLimbRendering>();
        */

        ManipulableElements = new Dictionary<string, GameObject>();
        for (int i = 0; i < elementNamesAndPaths.Length; ++i)
            ManipulableElements.Add(elementNamesAndPaths[i].Item1, GameObject.Find(elementNamesAndPaths[i].Item2));
    }
}
