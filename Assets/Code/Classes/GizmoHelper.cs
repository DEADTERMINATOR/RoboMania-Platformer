﻿using UnityEngine;
using System;
using System.Collections;

public class GizmoHelper
{
    static public void drawString(string text, Vector3 worldPos, float oX = 0, float oY = 0, Color? colour = null)
    {

#if UNITY_EDITOR
        try
        {
            UnityEditor.Handles.BeginGUI();

            var restoreColor = GUI.color;

            if (colour.HasValue) GUI.color = colour.Value;
            var view = UnityEditor.SceneView.currentDrawingSceneView;
            if (view == null)
            {
                return;
            }
            Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);

            if (screenPos.y < 0 || screenPos.y > Screen.height || screenPos.x < 0 || screenPos.x > Screen.width || screenPos.z < 0)
            {
                GUI.color = restoreColor;
                UnityEditor.Handles.EndGUI();
                return;
            }
            GUIStyle style = new GUIStyle();
            style.fontSize = 20;
            style.fontStyle = FontStyle.Bold;
            style.normal.textColor = Color.white;
            UnityEditor.Handles.Label(TransformByPixel(worldPos, oX, oY), text, style);

            GUI.color = restoreColor;
            UnityEditor.Handles.EndGUI();
        }
        catch (NullReferenceException)
        {
            /* If experience if any indication, we're here because Handles.BeginGUI threw the null reference exception. If I felt like being a good programmer,
             * I'd probably try and figure out why and recover, but since this is a helper script that only runs in the editor, and the error only seems to happen for one frame when the Scene view is moved,
             * we're just going to exit the method and silence the error being thrown.
             * */
            return;
        }
#endif
    }

#if UNITY_EDITOR
    static Vector3 TransformByPixel(Vector3 position, float x, float y)
    {
        return TransformByPixel(position, new Vector3(x, y));
    }
#endif

#if UNITY_EDITOR
    static Vector3 TransformByPixel(Vector3 position, Vector3 translateBy)
    {
        Camera cam = UnityEditor.SceneView.currentDrawingSceneView.camera;
        if (cam)
            return cam.ScreenToWorldPoint(cam.WorldToScreenPoint(position) + translateBy);
        else
            return position;
    }
#endif
}
