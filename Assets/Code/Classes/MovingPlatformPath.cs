﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformPath
{
    public enum EndWaypointMode { Reverse, Cyclic }


    [System.Serializable]
    public class Waypoint
    {
        public Vector3 Position;
        public bool CanTransferToBasePath;
        public MovingPlatformPath PathToTransferTo;

        [Rename("Path to Transfer To")]
        public MovingPlatformPathComponent EditorComponentPathToTransferTo;

        public int WaypointIndexOnOtherPath { get { return _waypointIndexOnOtherPath; } }


        [SerializeField]
        private int _waypointIndexOnOtherPath = -1;


        public Waypoint(Vector3 position, MovingPlatformPath pathToTransferTo = null)
        {
            Position = position;
            PathToTransferTo = pathToTransferTo;
        }
    }


    public Waypoint[] Waypoints { get; private set; }
    public EndWaypointMode EndMode { get; private set; }

    public Waypoint From { get { return Waypoints[_waypointIndex]; } }
    public Waypoint To
    {
        get
        {
            if (_waypointIndex + 1 >= Waypoints.Length)
            {
                if (EndMode == EndWaypointMode.Cyclic)
                {
                    return Waypoints[0];
                }
                else
                {
                    if (Waypoints.Length > 1)
                    {
                        return Waypoints[_waypointIndex - 1];
                    }
                    else
                    {
                        return Waypoints[_waypointIndex];
                    }
                }
            }
            else
            {
                return Waypoints[_waypointIndex + 1];
            }
        }
    }

    public Vector3 FirstPosition { get { return Waypoints[0].Position; } }
    public Vector3 LastPosition { get { return Waypoints[Waypoints.Length - 1].Position; } }

    public Vector3 FromPosition { get { return Waypoints[_waypointIndex].Position; } }
    public Vector3 ToPosition { get { return To.Position; } }

    public int FromWaypointIndex { get { return _waypointIndex; } }
    public int ToWaypointIndex
    {
        get
        {
            if (_waypointIndex + 1 >= Waypoints.Length)
            {
                if (EndMode == EndWaypointMode.Cyclic)
                {
                    return 0;
                }
                else
                {
                    if (Waypoints.Length > 1)
                    {
                        return _waypointIndex - 1;
                    }
                    else
                    {
                        return _waypointIndex;
                    }
                }
            }
            else
            {
                return _waypointIndex + 1;
            }
        }
    }


    private int _waypointIndex;


    public MovingPlatformPath(List<Waypoint> waypoints, EndWaypointMode endMode, int startingWaypoint = 0)
    {
        Waypoints = waypoints.ToArray();
        EndMode = endMode;

        if (_waypointIndex != 0 && _waypointIndex <= Waypoints.Length - 1)
        {
            _waypointIndex = startingWaypoint;
            if (_waypointIndex == Waypoints.Length - 1 && EndMode == EndWaypointMode.Reverse)
            {
                _waypointIndex = 0;
                System.Array.Reverse(Waypoints);
            }
        }
        else
        {
            _waypointIndex = 0;
        }
    }

    /// <summary>
    /// Increments the waypoint to the next one in the path, including handling of the waypoint reaching the end of the path, per the path's EndWaypointMode.
    /// </summary>
    /// <returns>True if the waypoint has reached the end of the path and the end case was handled, false otherwise.</returns>
    public bool IncrementPath()
    {
        ++_waypointIndex;

        if (_waypointIndex >= Waypoints.Length - 1)
        {
            _waypointIndex = 0;
            if (EndMode == EndWaypointMode.Reverse)
            {
                System.Array.Reverse(Waypoints);
            }

            return true;
        }

        return false;
    }

    /// <summary>
    /// Gets the position of the specified waypoint, provided that the index is a valid one.
    /// </summary>
    /// <param name="currentWaypoint">The index of the waypoint.</param>
    /// <returns>The position of the waypoint, as a Vector3.</returns>
    public Vector3 GetPathWaypointPosition(int currentWaypoint)
    {
        if (currentWaypoint < 0 || currentWaypoint >= Waypoints.Length)
        {
            Debug.LogWarning("Moving platform waypoint is out of bounds");
            return Vector3.zero;
        }

        return Waypoints[currentWaypoint].Position;
    }

    /// <summary>
    /// Sets the current waypoint to the specified waypoint index, provided that index is a valid one.
    /// </summary>
    /// <param name="newWaypointIndex">The index of the waypoint.</param>
    /// <param name="setWaypointAsTo">Whether the new path waypoint index should be set as the "To" waypoint instead of the default "From" waypoint.</param>
    /// <returns>True if the index was valid, false otherwise.</returns>
    public bool SetPathWaypoint(int newWaypointIndex, bool setWaypointAsTo)
    {
        if (newWaypointIndex < 0 || newWaypointIndex >= Waypoints.Length)
        {
            Debug.LogWarning("Moving platform waypoint is out of bounds");
            return false;
        }

        if (setWaypointAsTo)
        {
            if (newWaypointIndex > 0)
            {
                _waypointIndex = newWaypointIndex - 1;
            }
            else
            {
                //The new waypoint is the first one in the list, so we need to handle this.
                if (EndMode == EndWaypointMode.Cyclic)
                {
                    //The path is cyclic, so the previous waypoint would be the one at the end of the list.
                    _waypointIndex = Waypoints.Length - 1;
                }
                else
                {
                    //This case wouldn't really work, so in this case we'll just set the index as if we were setting From.
                    _waypointIndex = newWaypointIndex;
                }
            }
        }
        else
        {
            _waypointIndex = newWaypointIndex;
        }

        return true;
    }

    /// <summary>
    /// Sets the current waypoint to the waypoint index closest to the provided position.
    /// </summary>
    /// <param name="position">The position to compare against.</param>
    /// <returns>The position of the selected Waypoint, as a Vector3.</returns>
    public Vector3 SetPathWaypointClosestToPosition(Vector3 position)
    {
        int closestWaypoint = 0;
        float shortestDistance = Mathf.Infinity;

        for (int i = 0; i < Waypoints.Length; ++i)
        {
            float distance = Vector2.Distance(position, Waypoints[i].Position);
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                closestWaypoint = i;
            }
        }

        if (closestWaypoint >= Waypoints.Length - 1)
        {
            _waypointIndex = 0;
            System.Array.Reverse(Waypoints); //Reverse the array so the end becomes the start and the platform is simply travelling from start to finish again.
        }
        else
        {
            _waypointIndex = closestWaypoint;
        }

        return Waypoints[_waypointIndex].Position;
    }

    private void OnDrawGizmosSelected()
    {
        for (int i = 0; i < Waypoints.Length; ++i)
        {
            StaticTools.DrawPointGizmosOnly(Waypoints[i].Position, Color.green, "" + i, Color.white);
        }
    }
}
