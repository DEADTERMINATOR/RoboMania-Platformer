﻿using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// A point the play can be sent to if they die or load in the level part way 
/// </summary>
namespace LevelSystem.CheckpointSytem
{
    public class CheckpointEvent : UnityEvent<Checkpoint> { }
    public class Checkpoint : MonoBehaviour, IActivatable
    {
        /// <summary>
        /// The point the player will be spawned
        /// </summary>
        public Vector3 SpawnPoint;
        /// <summary>
        /// Disabled = cant be set
        /// Inactive = can be set 
        /// Active = is set
        /// </summary>
        public enum State {DISABLED,INACTIVE,ACTIVE}
        /// <summary>
        /// The abstraction of the visuals
        /// </summary>
        public CheckpointVisualManager CheckpointVisual;
        /// <summary>
        /// State Vars to load
        /// </summary>
        public LevelStateVars SetStateVarsComponet;
        /// <summary>
        /// Should state data be loaded
        /// </summary>
        public bool LoadLevelStateVars = false;
        /// <summary>
        /// The current state of the object
        /// </summary>
        public State CurrentState { get; private set; } = State.INACTIVE;
        
        [Tooltip("Used for Debug and spawn at check point menus ")]
        public string DebugName = "UnNamed Checkpoint"; 
        /// <summary>
        /// Used as part of the debug
        /// </summary>
        public string DebugID { get { return transform.position.x + "-" + transform.position.y; } }
        /// <summary>
        /// The ID of the check point Do not set
        /// </summary>
        public int ID { get; internal set; } = -1;
        public CheckpointEvent OnCheckpointSet = new CheckpointEvent();

        public bool Active { get { return CurrentState != State.DISABLED; } }


        //used for loading state
        internal void SetState(State state)
        {
            CurrentState = state;
            CheckpointVisual?.SetState(CurrentState);
            if (CurrentState == State.ACTIVE && LoadLevelStateVars && SetStateVarsComponet != null)
            {
                SetStateVarsComponet.SetValues();
            }
        }
 

        public void Start()
        {
            if (SpawnPoint == Vector3.zero)
            {
                SpawnPoint = transform.position;
                CheckpointVisual?.SetState(CurrentState);
            }
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if(collider.tag == GlobalData.PLAYER_TAG)
            {
               if(CurrentState == State.INACTIVE)
                {
                    CurrentState = State.ACTIVE;
                    CheckpointVisual?.SetState(CurrentState);
                    OnCheckpointSet?.Invoke(this);
                    if(LoadLevelStateVars && SetStateVarsComponet != null)
                    {
                        SetStateVarsComponet.SetValues();
                    }
                }
            }
        }
        /// <summary>
        /// Draw Gizmos and set some values if they are default
        /// </summary>
        private void OnDrawGizmosSelected()
        {
            if (SpawnPoint == Vector3.zero)
            {
                SpawnPoint = transform.position;
            }
            if(DebugName == "UnNamed Checkpoint")
            {
                DebugName = gameObject.name;
            }

            VisualDebug.DrawGizmoPointWithCircle(SpawnPoint, Color.green, Color.white);
        }

        public void Activate(GameObject activator)
        {
            CurrentState = State.INACTIVE;
            CheckpointVisual?.SetState(CurrentState);
        }

        public void Deactivate(GameObject activator)
        {
            CurrentState = State.DISABLED;
            CheckpointVisual?.SetState(CurrentState);
        }
    }
}

