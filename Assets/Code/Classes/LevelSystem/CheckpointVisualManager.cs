﻿using LevelSystem.CheckpointSytem;
using UnityEngine;
public class CheckpointVisualManager : MonoBehaviour
{
    public ParticleSystem IndicatorSystem;
    public Sprite ActiveSprite;
    public Sprite InActiveSprite;
    public Sprite DisabledSprite;
    public SpriteRenderer SpriteRenderer;
    private Checkpoint.State _curentState;
    public void SetState(Checkpoint.State state )
    {
        _curentState = state;
        switch(_curentState)
        {
            case Checkpoint.State.DISABLED:
                SpriteRenderer.sprite = DisabledSprite;
                IndicatorSystem.Stop();
                break;

            case Checkpoint.State.INACTIVE:
                SpriteRenderer.sprite = InActiveSprite;
                IndicatorSystem.Play();
                break;
            case Checkpoint.State.ACTIVE:
                SpriteRenderer.sprite = InActiveSprite;
                IndicatorSystem.Stop();
                break;
        }
    }

}

