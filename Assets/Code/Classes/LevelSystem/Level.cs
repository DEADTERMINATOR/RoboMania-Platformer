﻿using LevelSystem.CheckpointSytem;
using System;
using UnityEngine;
using GameMaster;
using UnityEngine.Events;
using Characters.Player;
using System.Collections;
using LevelSystem.SateData;
using LevelSystem;
using RoboMania.UI;
using UnityEngine.Playables;
using Inputs;

namespace LevelSystem
{
    /// <summary>
    /// The abstart representation of a level
    /// Must be low in the Script exaction order
    /// </summary>
    /// 
    [RequireComponent(typeof(Checkpoints))]
    public class Level : MonoBehaviour
    {
        /// <summary>
        /// The save file used for sate data
        /// </summary>
        public string StateDataSaveFileName = "LevelStateData.dat";
        /// <summary>
        /// The state of the level
        /// </summary>
        public LevelState LevelState { get; protected set; }
        /// <summary>
        /// The check points
        /// </summary>
        public Checkpoints Checkpoints { get; protected set; }

        public PlayableDirector LevelStartTimeLine;
        /// <summary>
        /// Should the level Run in debug 
        /// </summary>
        /// 
        [Tooltip("")]
        public bool RunInDebug = false;

        /// <summary>
        /// The Index to use to go to the main menu when exiting to main menu
        /// </summary>
        public int MainMenuDataPackageIndex = 0;
        /// <summary>
        /// The Index to use to go to exit to next
        /// </summary>
        public int NextDataPackageIndex = 2;
        /// <summary>
        /// A dictionary that will hold a string id representing an item spawner for non-unique item
        /// (such as a shield) and whether the item should be spawned.
        /// </summary>
        /// 

        public Difficulty CurentDifficulty;
        /// <summary>
        /// A starting g point for debug
        /// </summary>
        public Transform DebugStartingPoint;
        /// <summary>
        /// The point to spawn the player
        /// </summary>
        public Vector3 StartPoint;
        /// <summary>
        /// A starting g point for debug
        /// </summary>
        public Transform DebugCheckPoint;
        /// <summary>
        /// Design NOTE: Make Sure this is set erlay
        /// </summary>
        public static Level CurentLevel { get; protected set; }

        public bool UseDebugCheckPointOnlyOnFirstLoad;

        public Vector3 PlayerSpwnPoint { get; private set; }
        /// <summary>
        /// Fired when the level is loaded
        /// </summary>
        public UnityEvent OnLevelLoaded = new UnityEvent();
        public UnityEvent OnLevelLoading = new UnityEvent();
        public UnityEvent OnPause = new UnityEvent();
        public UnityEvent OnUnpause = new UnityEvent();
        private bool _wasLoaded = false;
        private bool _firstLoad = true;

        #region Unity Methods
        void Awake()
        {
#if !DEVELOPMENT_BUILD && !UNITY_EDITOR //no debug in full builds
            RunInDebug = false;
#endif

            PlayerSpwnPoint = Vector3.zero;
            CurentLevel = this;
            LevelState =  LevelState.Instance;
            if (!GlobalData.IsANewGame)
            {
                LevelState.LoadState(StateDataSaveFileName);
            }
            else
            {
                LevelState.LoadState("");
            }
        }

        /// <summary>
        /// Do Not Override this 
        /// </summary>
        void Start()
        {
            GameManager.OnSceneReloadedGameManager += GameManager_OnSceneReloadedGameManager;

        }


        #endregion

        #region Events 
        /// <summary>
        /// Fired when the level was reloaded
        /// </summary>
        public UnityEvent OnLevelReloaded = new UnityEvent();

        #endregion


        #region public Helpers
        /// <summary>
        /// Get the spawn point if a Checkpoint has been set then get that otherwise get the starting pos
        /// </summary>
        /// <returns></returns>
        public virtual Vector3 GetSpawnPoint()
        {

            if (Checkpoints.LastCheckPoint != null)
            {
                if (RunInDebug)
                {
                    if(DebugCheckPoint)
                    {
                        PlayerSpwnPoint = DebugCheckPoint.position;
                        return PlayerSpwnPoint;
                    }
                    PlayerSpwnPoint = DebugStartingPoint.position;
                    return PlayerSpwnPoint;
                }
                PlayerSpwnPoint = Checkpoints.LastCheckPoint.SpawnPoint;
                return PlayerSpwnPoint;
            }
            if (RunInDebug)
            {
                PlayerSpwnPoint = DebugStartingPoint.position;
                return PlayerSpwnPoint;
            }
            PlayerSpwnPoint = StartPoint;
            return PlayerSpwnPoint;
        }
        #endregion

        #region Polymorphic Events 
        /// <summary>
        ///  Set up Coded Called after the scene is loaded but before spawning  
        /// </summary>
        public virtual void SetUpLevel()
        {
            Checkpoints = GetComponent<Checkpoints>();
            if (Checkpoints)
            {
                Checkpoints.LevelWasLoaded();
               
            }
            OnLevelLoading?.Invoke();
        }

        public virtual void StartLevel()
        {
            Checkpoints.OnCheckpointSet.AddListener(CheckPointHit);
            if (!_wasLoaded)
            {
                GlobalData.PlayerTransfrom.position = GetSpawnPoint();
                GlobalData.Player.gameObject.SetActive(true);
            }
            if(GlobalData.IsANewGame)
            {
                LevelInstanceData.InstanceData.LevelStartPlayableDirector.Play();
            }
            _wasLoaded = true;
            OnLevelLoaded?.Invoke();
        }
        protected virtual void LevelExit()
        {

            GlobalData.Player.Save();
        }
        /// <summary>
        /// The level is about to reload
        /// </summary>
        protected virtual void LevelAboutToReload()
        {

            GlobalData.Player.Save();

            GlobalData.Player.gameObject.tag = "RemovedPlayer";
           

        }
        protected virtual void LevelReloaded()
        {

            GameObject.Destroy(GlobalData.Player.transform.root.gameObject);
            GameManager.Instance.SetMasterAsActive();
            GameObject newPlayer = GameObject.Instantiate(Resources.Load("Prefabs/Player") as GameObject);
            GlobalData.Player = newPlayer.GetComponentInChildren<PlayerMaster>();
            GlobalData.Player.Load();
            Checkpoints.LevelWasLoaded();
            GlobalData.PlayerTransfrom.position = GetSpawnPoint();
            GlobalData.Player.State.SetDead(false);

            OnLevelReloaded?.Invoke();

        }
        protected virtual void SaveLevelState() 
        {
            LevelState.SaveState(StateDataSaveFileName);
        }




        #endregion

        //public enum Difficulty {EASY,MEDIUM,HARD}
       
        /// <summary>
        /// Reloaded the level and set the correct events 
        /// </summary>
        public virtual void ReloadLevel()
        {
            LevelAboutToReload();
            StartCoroutine(ReloadLevelCorutine());
        }

        public virtual void ExitLevel(bool next = true)
        {
            if (next)
            {
                GameManager.Instance.LoadLevelByIndex(NextDataPackageIndex);
            }
            else
            {
                GameManager.Instance.LoadLevelByIndex(MainMenuDataPackageIndex);
            }

            LevelExit();
        }
        /// <summary>
        /// Sets the item associated with the id as picked up, so it doesn't spawn again on reloads.
        /// </summary>
        /// <param name="id"></param>
       /* public void SetItemPickedUp(string id)
        {
            NonUniqueItems[id] = false;
        }*/

        private void GameManager_OnSceneReloadedGameManager(UnityEngine.SceneManagement.Scene scene)
        {
            if (scene.path == GameManager.Instance.LoadedDatatpackage[0].Path)
            {
                LevelReloaded();
                OnLevelReloaded?.Invoke();
            }
        }

        private void OnDrawGizmos()
        {
            VisualDebug.DrawGizmoPoint(StartPoint, Color.cyan);
        }

        private void OnApplicationQuit()
        {
            ExitLevel();
        }
        /// <summary>
        /// Stall Loading by one frame 
        /// </summary>
        /// <returns></returns>
        IEnumerator ReloadLevelCorutine()
        {

            yield return null;
            GameManager.Instance.ReloadSceneInLoadedDataPackage(0);
        }

        public void CheckPointHit(Checkpoint point)
        {
            SaveLevelState();
           
        }
        /// <summary>
        /// Try and pause the game (currently will alsow return true)
        /// </summary>
        /// <returns></returns>
        public bool ReqestLevlePause()
        {
            InputManager.Instance.DisableInGameActions();
            Time.timeScale = 0;
            OnPause?.Invoke();
            return true;
        }
        public bool ReqestLevleUnpause()
        {
            InputManager.Instance.EnableInGameActions();
            Time.timeScale = 1;
            OnUnpause?.Invoke();
            return true;
        }
    }


}
