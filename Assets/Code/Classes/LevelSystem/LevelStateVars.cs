﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LevelSystem;
using LevelSystem.SateData;

[System.Serializable]
public class LevelStateVars : MonoBehaviour
{
    [Tooltip("Set all even if they did not exist  ")]
    public bool SetAll = true;

    [Tooltip("State var may not exist 1= true !0 = false")]
    ///Not a KeyValuePair Array Cuz unity 
    public LevelStartVarContainer[]StateVars; 
    // Use this for initialization
    
    public void SetValues()
    {
        Level.CurentLevel.LevelState.LoadVars(StateVars, !SetAll);
    }
}


