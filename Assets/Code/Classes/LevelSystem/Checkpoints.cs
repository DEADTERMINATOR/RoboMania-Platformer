﻿using LevelSystem.SateData;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// The collection of point in a level
/// </summary>
/// 
namespace LevelSystem.CheckpointSytem
{

    [Serializable]
    public class Checkpoints : MonoBehaviour
    {

        public Checkpoint[] Points;
        private LevelStateInt _lastCheckPoint ;
        public CheckpointEvent OnCheckpointSet = new CheckpointEvent();
        public Checkpoint LastCheckPoint { get { if (_lastCheckPoint > 0 && _lastCheckPoint < Points.Length) { return Points[_lastCheckPoint]; } return null; } }

        private void Awake()
        {
            _lastCheckPoint = Level.CurentLevel.LevelState.LoadLevelStateInt("LastCheckPoint", -1);
        }

        public void NewCheckPointHit(Checkpoint point)
        {
            if (LastCheckPoint)
            {
                LastCheckPoint.SetState(Checkpoint.State.INACTIVE);
            }
            _lastCheckPoint.Value = point.ID;
            OnCheckpointSet?.Invoke(point);
;        }

        public void LevelWasLoaded()
        {
            Points = FindObjectsOfType<Checkpoint>();
            for (int index = 0; index < Points.Length; index++)
            {
                Checkpoint point = Points[index];
                point.OnCheckpointSet.AddListener(NewCheckPointHit);
                point.ID = index;
            }
        }

    }  
}