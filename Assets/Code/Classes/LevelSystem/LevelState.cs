﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace LevelSystem.SateData
{
    public class LevelState
    {
        /// <summary>
        ///  Storage for state values they should not be set it editor
        /// </summary>
        public Dictionary<string, int> StateData { get; protected set; }

        private Dictionary<string, UnityEvent<int>> _OnChnagedEvents = new Dictionary<string, UnityEvent<int>>();

        public static UnityEvent OnStateDataLoaded = new UnityEvent();

        public bool WasDataLoaded { get; private set; } = false;
        /// <summary>
        /// LevelState needs to be loaded you can cheek WasLoadedData 
        /// </summary>
        public static LevelState Instance { get { if (_instance == null) { _instance = new LevelState(); } return _instance; } }

        private  static LevelState _instance;
        private LevelState()
        {

        }
            
        #region DataStorege
        public void LoadStateFromResources(string filename)
        {
            ES3Settings settings = new ES3Settings();
            settings.location = ES3.Location.Resources;
            StateData = ES3.Load<Dictionary<string, int>>("StateData", filename, settings);
        }
        public void SaveStateToResources(string filename)
        {
            ES3Settings settings = new ES3Settings();
            settings.location = ES3.Location.Resources;
            ES3.Save<Dictionary<string, int>>("StateData", filename, settings);
        }
        /// <summary>
        /// Try and load state data or make new empty store if no data loaded
        /// </summary>
        /// <param name="filename">Pass empty string for a new data</param>
        public void LoadState(string filename)
        {
            //save CPU time on File look up if no file is set
            if (!String.IsNullOrEmpty(filename)  && ES3.FileExists(filename))
            {
                WasDataLoaded = true;
                StateData = ES3.Load<Dictionary<string, int>>("StateData", filename);
                if (StateData == null)
                {
                    WasDataLoaded = false;
                    StateData = new Dictionary<string, int>();
                }
            }
            else
            {
                WasDataLoaded = false;
                StateData = new Dictionary<string, int>();
            }
        }
        public void SaveState(string filename)
        {
            ES3.Save<Dictionary<string, int>>("StateData", StateData, filename);
        }

        public void LoadVars(LevelStartVarContainer[] vars, bool updateOnly = true)
        {
            foreach (LevelStartVarContainer var in vars)
            {
                if (updateOnly || DoseStateExists(var.Key) )
                {
                    SetState(var.Key, var.Value, true);
                }
                else
                {
                    AddStateKey(var.Key, var.Value);
                }
            }
        }
        #endregion

        public void AddOnChangedEventListener(string keyname, UnityAction<int> callback)
        {
            if (!_OnChnagedEvents.ContainsKey(keyname))// is a valid key make new object with old key 
            {
                 _OnChnagedEvents.Add(keyname, new LevelStateValueChagedEvent()); 
            }
            _OnChnagedEvents[keyname].AddListener(callback);
        }
        public void RemoveOnChangedEventListener(string keyname, UnityAction<int> callback)
        {
            if (_OnChnagedEvents.ContainsKey(keyname))// is a valid key make new object with old key 
            {
                _OnChnagedEvents[keyname].RemoveListener(callback);
            }
        }

        #region StateManuplation
        /// <summary>
        /// Get The Int Value of a state
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public int GetState(string keyName)
        {
            if (StateData.ContainsKey(keyName))
            {
                return StateData[keyName];
            }
            throw new InvalidStateKeyException(keyName);
        }

        /// <summary>
        /// is state true will test int values as != 1
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public bool IsState(string keyName)
        {
            if (StateData.ContainsKey(keyName))
            {
                return IntToBool(StateData[keyName]);
            }
            throw new InvalidStateKeyException(keyName);
        }
        /// <summary>
        /// Dose the sate Exists 
        /// </summary>
        /// <param name="KeyName">The Key</param>
        /// <returns></returns>
        public bool DoseStateExists(string KeyName)
        {
            return StateData.ContainsKey(KeyName);
        }
        /// <summary>
        /// Add A new key with a bool vlaue
        /// </summary>
        /// <param name="KeyName"></param>
        /// <param name="state"></param>
        /// <returns>true if added a false</returns>
        public bool AddStateKey(string KeyName, bool state = false)
        {
            if (StateData.ContainsKey(KeyName))
            {
                return false;
            }
            else
            {
                StateData.Add(KeyName, BoolToInt(state));
                return true;
            }
        }
        /// <summary>
        /// Add A new key with a int value
        /// </summary>
        /// <param name="KeyName"></param>
        /// <param name="state"></param>
        /// <returns>true if added a false</returns>
        public bool AddStateKey(string keyName, int state)
        {
            if (StateData.ContainsKey(keyName))
            {
                return false;
            }
            else
            {
                StateData.Add(keyName, state);
                return true;
            }
        }

        /// <summary>
        /// Remove a state key
        /// </summary>
        /// <param name="keyName"></param>
        public void DeletStateKey(string keyName)
        {
            StateData.Remove(keyName);
        }
        /// <summary>
        /// Set the value of a state
        /// </summary>
        /// <param name="keyName"></param>
        /// <param name="value"></param>
        public void SetState(string keyName, bool value = true, bool SkipExceptionOnfail = false)
        {
            if (StateData.ContainsKey(keyName))
            {
                ChangeValue(keyName,BoolToInt(value));
            }
            else if (!SkipExceptionOnfail)
            {
                throw new InvalidStateKeyException(keyName);
            }

        }
        /// <summary>
        /// set the value of a state
        /// </summary>
        /// <param name="keyName"></param>
        /// <param name="value"></param>
        public void SetState(string keyName, int value, bool SkipExceptionOnfail = false)
        {
            if (StateData.ContainsKey(keyName))
            {
                ChangeValue(keyName,value);
            }
            else if (!SkipExceptionOnfail)
            {
                throw new InvalidStateKeyException(keyName);
            }


        }

        protected void ChangeValue(string keyName, int value)
        {
            if (StateData[keyName] != value)
            {
                StateData[keyName] = value;
                if (_OnChnagedEvents.ContainsKey(keyName))
                {
                    _OnChnagedEvents[keyName].Invoke(value);
                }
            }
        }

        #endregion

        #region StateObjetcFacoty
        /// <summary>
        /// Factory to get LevelStateInt object linked to a var will return null dose not exsits
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public LevelStateBool GetStateBoolObject(string keyName)
        {
            if (StateData.ContainsKey(keyName))// is a valid key make new object with old key 
            {
                LevelStateBool levelStateBool = new LevelStateBool();
                levelStateBool._levelState = this;
                levelStateBool._keyName = keyName;
                return levelStateBool;
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="getLoadedData"></param>
        /// <returns></returns>
        public LevelStateBool LoadLevelStateBool(string key, bool value, bool getLoadedData = true)
        {
            if (AddStateKey(key, value))// only fails if key exisited
            {
                return GetStateBoolObject(key);
            }
            else if (getLoadedData)
            {
                return GetStateBoolObject(key);
            }
            return null;
        }

        /// <summary>
        /// Factory to get LevelStateInt object linked to a var will make key if it dose not exsits
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public LevelStateInt GetStateIntObject(string keyName)
        {

            if (StateData.ContainsKey(keyName))// is a valid key make new object with old key 
            {
                LevelStateInt levelStateBool = new LevelStateInt();
                levelStateBool._levelState = this;
                levelStateBool._keyName = keyName;
                return levelStateBool;
            }
            return null;
        }
        public LevelStateInt LoadLevelStateInt(string key, int value, bool getLoadedData = true)
        {
            if (AddStateKey(key, value))// only fails if key exisited
            {
                return GetStateIntObject(key);
            }
            else if (getLoadedData)
            {
                return GetStateIntObject(key);
            }
            return null;
        }
        /// <summary>
        /// Get a object to be use by an object that dose not matter if the Statedata is avalabel 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="getLoadedData"></param>
        /// <returns></returns>
        public static LevelStateInt GetInternalLevelStateInt(string key, int value, bool getLoadedData = true)
        {
            if (Level.CurentLevel == null || Level.CurentLevel.LevelState == null)
            {
                LevelStateInt levelStateBool = new LevelStateInt();
                levelStateBool._useFakeStore = true;
                levelStateBool._keyName = key;
                OnStateDataLoaded.AddListener(levelStateBool.ReTryLink);
            }
            if (Level.CurentLevel.LevelState.AddStateKey(key, value))// only fails if key exisited
            {
                return Level.CurentLevel.LevelState.GetStateIntObject(key);
            }
            else if (getLoadedData)
            {
                return Level.CurentLevel.LevelState.GetStateIntObject(key);
            }
            return null;
        }

        /// <summary>
        /// Get a object to be use by an object that dose not matter if the Statedata is avalabel 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="getLoadedData"></param>
        /// <returns></returns>
        public static LevelStateBool GetInternalLevelStateBool(string key, bool value, bool getLoadedData = true)
        {
            if (Level.CurentLevel == null || Level.CurentLevel.LevelState == null)
            {
                LevelStateBool levelStateBool = new LevelStateBool();
                levelStateBool._useFakeStore = true;
                levelStateBool._keyName = key;
                OnStateDataLoaded.AddListener(levelStateBool.ReTryLink);
            }
            if (Level.CurentLevel.LevelState.AddStateKey(key, value))// only fails if key exisited
            {
                return Level.CurentLevel.LevelState.GetStateBoolObject(key);
            }
            else if (getLoadedData)
            {
                return Level.CurentLevel.LevelState.GetStateBoolObject(key);
            }
            return null;
        }



        #endregion
        #region Static Helpers

        /// <summary>
        /// True == 0 False !=0
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IntToBool(int value)
        {
            return (value == 0);
        }

        public static int BoolToInt(bool value)
        {
            return (value) ? 0 : 1;
        }
        #endregion
    }

    public class InvalidStateKeyException : Exception
    {
        public string InvalidKey;
        public InvalidStateKeyException(string key) : base(string.Format("The Key {0} could not be found in level state", key))
        {
            InvalidKey = key;
        }
    }
    public class DuplacateStateKeyException : Exception
    {
        public string InvalidKey;
        public DuplacateStateKeyException(string key) : base(string.Format("The Key {0} already exists in level state", key))
        {
            InvalidKey = key;
        }
    }
    public class InvalidStateLoadException : Exception
    {
        public string FileName;
        public InvalidStateLoadException(string fileName) : base(string.Format("The File {0} could not be loaded as level state", fileName))
        {
            FileName = fileName;
        }
    }

    public class LevelStateUnSyncException : Exception
    {

        public LevelStateUnSyncException() { }


    }

    public abstract class LevelStateObject<T>
    {

        internal string _keyName;
        internal LevelState _levelState;
        internal bool _useFakeStore = false;

        public bool IsDataStoreLinked { get { return _useFakeStore; } }

        protected T _value;

        protected abstract T GetValue();
        protected abstract void SetValue(T value);



        public T Value
        {
            get
            {
                if (_useFakeStore)
                {
                    return _value;
                }
                return GetValue();
            }
            set
            {
                if (_useFakeStore)
                {
                    _value = value;
                }
                SetValue(value);
            }
        }


        public void AddOnChangedEventListener(UnityAction<int> callback)
        {
            _levelState.AddOnChangedEventListener(_keyName, callback);
        }
        public void RemoveOnChangedEventListener(UnityAction<int> callback)
        {
            _levelState.RemoveOnChangedEventListener(_keyName, callback);
        }
        /// <summary>
        /// try and link data back to Level state
        /// </summary>
        internal void ReTryLink()
        {
            _levelState = Level.CurentLevel.LevelState;
            if (_levelState != null)
            {
                Value = _value;
                _useFakeStore = false;
                LevelState.OnStateDataLoaded.RemoveListener(this.ReTryLink);
            }
        }
    }

    /// <summary>
    /// Helper Class to update the value use .Value implicit conversion dose not hold value 
    /// </summary>
    public class LevelStateInt : LevelStateObject<int>
    {
        protected override int GetValue()
        {
            return _levelState.GetState(_keyName);
        }

        protected override void SetValue(int value)
        {
            _levelState.SetState(_keyName, value);
        }

        public static implicit operator int(LevelStateInt LSI) => LSI.Value;
    }

    /// <summary>
    /// Helper Class to update the value use .Value implicit conversion dose not hold value 
    /// </summary>
    public class LevelStateBool : LevelStateObject<bool>
    {
        protected override bool GetValue()
        {
            return _levelState.IsState(_keyName);
        }
        protected override void SetValue(bool value)
        {
            _levelState.SetState(_keyName, value);
        }
        public static implicit operator bool(LevelStateBool LSI) => LSI.Value;
    }
    [System.Serializable]
    //Cuz unity sucks KeyVaulePir and Dictionary do not show in editor 
    public class LevelStartVarContainer
    {
        public string Key;
        public int Value;
        public bool isBool;
    }
    /// <summary>
    /// Event for value chages
    /// </summary>
    public class LevelStateValueChagedEvent : UnityEvent<int>
    {

    }
}
