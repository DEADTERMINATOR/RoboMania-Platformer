﻿using UnityEngine;

public static class VisualDebug
 {
    #region Structs
    public struct Box
    {
        public Vector3 localFrontTopLeft { get; private set; }
        public Vector3 localFrontTopRight { get; private set; }
        public Vector3 localFrontBottomLeft { get; private set; }
        public Vector3 localFrontBottomRight { get; private set; }
        public Vector3 localBackTopLeft { get { return -localFrontBottomRight; } }
        public Vector3 localBackTopRight { get { return -localFrontBottomLeft; } }
        public Vector3 localBackBottomLeft { get { return -localFrontTopRight; } }
        public Vector3 localBackBottomRight { get { return -localFrontTopLeft; } }

        public Vector3 frontTopLeft { get { return localFrontTopLeft + origin; } }
        public Vector3 frontTopRight { get { return localFrontTopRight + origin; } }
        public Vector3 frontBottomLeft { get { return localFrontBottomLeft + origin; } }
        public Vector3 frontBottomRight { get { return localFrontBottomRight + origin; } }
        public Vector3 backTopLeft { get { return localBackTopLeft + origin; } }
        public Vector3 backTopRight { get { return localBackTopRight + origin; } }
        public Vector3 backBottomLeft { get { return localBackBottomLeft + origin; } }
        public Vector3 backBottomRight { get { return localBackBottomRight + origin; } }

        public Vector3 origin { get; private set; }

        public Box(Vector3 origin, Vector3 halfExtents, Quaternion orientation) : this(origin, halfExtents)
        {
            Rotate(orientation);
        }
        public Box(Vector3 origin, Vector3 halfExtents)
        {
            this.localFrontTopLeft = new Vector3(-halfExtents.x, halfExtents.y, 0);
            this.localFrontTopRight = new Vector3(halfExtents.x, halfExtents.y, 0);
            this.localFrontBottomLeft = new Vector3(-halfExtents.x, -halfExtents.y, 0);
            this.localFrontBottomRight = new Vector3(halfExtents.x, -halfExtents.y, 0);

            this.origin = origin;
        }


        public void Rotate(Quaternion orientation)
        {
            localFrontTopLeft = RotatePointAroundPivot(localFrontTopLeft, Vector3.zero, orientation);
            localFrontTopRight = RotatePointAroundPivot(localFrontTopRight, Vector3.zero, orientation);
            localFrontBottomLeft = RotatePointAroundPivot(localFrontBottomLeft, Vector3.zero, orientation);
            localFrontBottomRight = RotatePointAroundPivot(localFrontBottomRight, Vector3.zero, orientation);
        }
    }
    #endregion

    #region Draw Box Functions
    //Draws just the box at where it is currently hitting.
    public static void DrawBoxCastOnHit(Vector3 origin, Vector3 halfExtents, Quaternion orientation, Vector3 direction, float hitInfoDistance, Color color)
    {
        origin = CastCenterOnCollision(origin, direction, hitInfoDistance);
        DrawBox(origin, halfExtents, orientation, color);
    }

    //Draws the full box from start of cast to its end distance. Can also pass in hitInfoDistance instead of full distance
    public static void DrawBoxCastBox(Vector3 origin, Vector3 halfExtents, Quaternion orientation, Vector3 direction, float distance, Color color, float duration = 0)
    {
#if DEBUG
        direction.Normalize();
        Box bottomBox = new Box(origin, halfExtents, orientation);
        Box topBox = new Box(origin + (direction * distance), halfExtents, orientation);

        Debug.DrawLine(bottomBox.backBottomLeft, topBox.backBottomLeft, color, duration);
        Debug.DrawLine(bottomBox.backBottomRight, topBox.backBottomRight, color, duration);
        Debug.DrawLine(bottomBox.backTopLeft, topBox.backTopLeft, color, duration);
        Debug.DrawLine(bottomBox.backTopRight, topBox.backTopRight, color, duration);
        Debug.DrawLine(bottomBox.frontTopLeft, topBox.frontTopLeft, color, duration);
        Debug.DrawLine(bottomBox.frontTopRight, topBox.frontTopRight, color, duration);
        Debug.DrawLine(bottomBox.frontBottomLeft, topBox.frontBottomLeft, color, duration);
        Debug.DrawLine(bottomBox.frontBottomRight, topBox.frontBottomRight, color, duration);

        DrawBox(bottomBox, color);
        DrawBox(topBox, color);
#endif
    }

    public static void DrawBox(Vector3 center, Vector3 extents, Quaternion orientation, Color color, float duration = 0)
    {
        DrawBox(new Box(center, extents, orientation), color, duration);
    }

    public static void DrawBox(Box box, Color color, float duration = 0)
    {
#if DEBUG
        if (duration != 0)
        {
            Debug.DrawLine(box.frontTopLeft, box.frontTopRight, color, duration);
            Debug.DrawLine(box.frontTopRight, box.frontBottomRight, color, duration);
            Debug.DrawLine(box.frontBottomRight, box.frontBottomLeft, color, duration);
            Debug.DrawLine(box.frontBottomLeft, box.frontTopLeft, color, duration);

            Debug.DrawLine(box.backTopLeft, box.backTopRight, color, duration);
            Debug.DrawLine(box.backTopRight, box.backBottomRight, color, duration);
            Debug.DrawLine(box.backBottomRight, box.backBottomLeft, color, duration);
            Debug.DrawLine(box.backBottomLeft, box.backTopLeft, color, duration);

            Debug.DrawLine(box.frontTopLeft, box.backTopLeft, color, duration);
            Debug.DrawLine(box.frontTopRight, box.backTopRight, color, duration);
            Debug.DrawLine(box.frontBottomRight, box.backBottomRight, color, duration);
            Debug.DrawLine(box.frontBottomLeft, box.backBottomLeft, color, duration);
        }
        else //I've noticed that even passing a duration of 0 can result in the debug being drawn for more than one frame. So it appears we need the unfortunate code duplication to ensure we draw for only one frame when desired.
        {
            Debug.DrawLine(box.frontTopLeft, box.frontTopRight, color);
            Debug.DrawLine(box.frontTopRight, box.frontBottomRight, color);
            Debug.DrawLine(box.frontBottomRight, box.frontBottomLeft, color);
            Debug.DrawLine(box.frontBottomLeft, box.frontTopLeft, color);

            Debug.DrawLine(box.backTopLeft, box.backTopRight, color);
            Debug.DrawLine(box.backTopRight, box.backBottomRight, color);
            Debug.DrawLine(box.backBottomRight, box.backBottomLeft, color);
            Debug.DrawLine(box.backBottomLeft, box.backTopLeft, color);

            Debug.DrawLine(box.frontTopLeft, box.backTopLeft, color);
            Debug.DrawLine(box.frontTopRight, box.backTopRight, color);
            Debug.DrawLine(box.frontBottomRight, box.backBottomRight, color);
            Debug.DrawLine(box.frontBottomLeft, box.backBottomLeft, color);
        }
#endif
    }

    public static void DrawBoxGizmos(Vector3 center, Vector3 extents, Quaternion orientation)
    {
        DrawBoxGizmos(new Box(center, extents, orientation));
    }

    public static void DrawBoxGizmos(Box box)
    {
        Gizmos.DrawLine(box.frontTopLeft, box.frontTopRight);
        Gizmos.DrawLine(box.frontTopRight, box.frontBottomRight);
        Gizmos.DrawLine(box.frontBottomRight, box.frontBottomLeft);
        Gizmos.DrawLine(box.frontBottomLeft, box.frontTopLeft);

        Gizmos.DrawLine(box.backTopLeft, box.backTopRight);
        Gizmos.DrawLine(box.backTopRight, box.backBottomRight);
        Gizmos.DrawLine(box.backBottomRight, box.backBottomLeft);
        Gizmos.DrawLine(box.backBottomLeft, box.backTopLeft);

        Gizmos.DrawLine(box.frontTopLeft, box.backTopLeft);
        Gizmos.DrawLine(box.frontTopRight, box.backTopRight);
        Gizmos.DrawLine(box.frontBottomRight, box.backBottomRight);
        Gizmos.DrawLine(box.frontBottomLeft, box.backBottomLeft);
    }

    public static void DrawBoxCast2D(Vector2 origin, Vector2 size, float angle, Vector2 direction, float distance, Color color, float duration = 0)
    {
        Quaternion angle_z = Quaternion.Euler(0f, 0f, angle);
        DrawBoxCastBox(origin, size / 2f, angle_z, direction, distance, color, duration);
    }
    #endregion

    public static void DrawVector(Vector3 vector, Color color, float length = 1, float duration = 0)
    {
        DrawDebugPoint(vector, Color.black);
        Debug.DrawLine(vector, vector + vector.normalized, color, duration);
    }

    public static Color DebugRayNumber(int i)
    {
        switch (i)
        {
            case 0:
                return Color.white;
            case 1:
                return Color.cyan;
            case 2:
                return Color.black;
            case 3:
                return Color.blue;
            case 4:
                return Color.green;
            case 5:
                return Color.yellow;
            case 6:
                return Color.magenta;
        }
        return Color.red;
    }

    #region Draw Debug Point Functions
    public static void DrawDebugPoint(Vector3 point, Color lineColor, float duration = 10, float lineDistance = 1)
    {
#if DEBUG
        //Draw a Debug point if we complied debug
        Debug.DrawLine(point + Vector3.down * lineDistance, point + Vector3.up * lineDistance, lineColor, duration);
        Debug.DrawLine(point + Vector3.left * lineDistance, point + Vector3.right * lineDistance, lineColor, duration);
#endif
    }
    public static void DrawDebugPoint(Vector3 point)
    {
        DrawDebugPoint(point, Color.red);
    }

    public static void DrawDebugPointList(Vector3[] list)
    {
#if DEBUG
        DrawDebugPoint(list[0]);
        for (int i = 1; i< list.Length; i++)
        {
            DrawDebugPoint(list[i]);
            Debug.DrawLine(list[i - 1], list[i], Color.green);
        }
#endif

    }
    #endregion

    #region Draw Gizmo Point Functions
    public static void DrawGizmoPoint(Vector3 point, Color lineColor)
    {
        //Draw a Debug point if we complied debug
        Gizmos.color = lineColor;
        Gizmos.DrawLine(point + Vector3.down, point + Vector3.up);
        Gizmos.DrawLine(point + Vector3.left, point + Vector3.right);
    }

    public static void DrawGizmoPointWithCircle(Vector3 point, Color lineColor, Color cireleColor)
    {
        Gizmos.color = cireleColor;
        Gizmos.DrawSphere(point, 1.2f);
        //Draw a Debug point if we complied debug
        Gizmos.color = lineColor;
        Gizmos.DrawLine(point + Vector3.down, point + Vector3.up);
        Gizmos.DrawLine(point + Vector3.left, point + Vector3.right);
    }
    #endregion

    #region Support Functions
    //This should work for all cast types
    private static Vector3 CastCenterOnCollision(Vector3 origin, Vector3 direction, float hitInfoDistance)
    {
        return origin + (direction.normalized * hitInfoDistance);
    }

    private static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion rotation)
    {
        Vector3 direction = point - pivot;
        return pivot + rotation * direction;
    }
    #endregion
}


