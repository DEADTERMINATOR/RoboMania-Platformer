﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class FunctionPointerAction : StateMachineAction
{
    private Func<bool> _boolAction;
    private Func<object, bool> _boolParameterizedAction;
    private Func<object, object, bool> _boolDualParameterizedAction;

    private Func<ActionCompletionState> _action;
    private Func<object, ActionCompletionState> _parameterizedAction;
    private Func<object, object, ActionCompletionState> _dualParameterizedAction;

    private object[] _funcArgs;


    [Obsolete("Please use the version that works with ActionCompletionState. This remains for backwards compatibility only.")]
    public FunctionPointerAction(Func<bool> action)
    {
        _boolAction = action;
    }

    [Obsolete("Please use the version that works with ActionCompletionState. This remains for backwards compatibility only.")]
    public FunctionPointerAction(Func<object, bool> action, object arg)
    {
        _boolParameterizedAction = action;

        _funcArgs = new object[1];
        _funcArgs[0] = arg;
    }

    [Obsolete("Please use the version that works with ActionCompletionState. This remains for backwards compatibility only.")]
    public FunctionPointerAction(Func<object, object, bool> action, object arg1, object arg2)
    {
        _boolDualParameterizedAction = action;

        _funcArgs = new object[2];
        _funcArgs[0] = arg1;
        _funcArgs[1] = arg2;
    }

    public FunctionPointerAction(Func<ActionCompletionState> action, string name = "", params StateMachineRunCondition[] runConditions) : base(runConditions)
    {
        _action = action;
        ActionName = name;
    }

    public FunctionPointerAction(Func<object, ActionCompletionState> action, object arg, string name = "", params StateMachineRunCondition[] runConditions) : base(runConditions)
    {
        _parameterizedAction = action;

        _funcArgs = new object[1];
        _funcArgs[0] = arg;

        ActionName = name;
    }

    public FunctionPointerAction(Func<object, object, ActionCompletionState> action, object arg1, object arg2, string name = "", params StateMachineRunCondition[] runConditions) : base(runConditions)
    {
        _dualParameterizedAction = action;

        _funcArgs = new object[2];
        _funcArgs[0] = arg1;
        _funcArgs[1] = arg2;

        ActionName = name;
    }

    [Obsolete("Please use the version that returns an ActionCompletionState. This remains for backwards compatibility only.")]
    public override bool PerformAction()
    {
        if (_boolAction != null)
        {
            return _boolAction();
        }
        else if (_boolParameterizedAction != null)
        {
            return _boolParameterizedAction(_funcArgs[0]);
        }
        else
        {
            return _boolDualParameterizedAction(_funcArgs[0], _funcArgs[1]);
        }
    }

    public override ActionCompletionState DoAction()
    {
        if (EvaluateRunConditions())
        {
            if (_action != null)
            {
                return _action();
            }
            else if (_parameterizedAction != null)
            {
                return _parameterizedAction(_funcArgs[0]);
            }
            else
            {
                return _dualParameterizedAction(_funcArgs[0], _funcArgs[1]);
            }
        }
        return ActionCompletionState.NotRun;
    }
}
