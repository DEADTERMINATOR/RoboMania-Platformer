﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A state machine that handles groups of NPCs that require some kind of synchronization.
/// </summary>
public class TimedGroupNPCStateMachine : StateMachine
{
    /// <summary>
    /// The NPCs that this state machine will manage.
    /// </summary>
    public TimedNPCGroup[] Groups;

    /// <summary>
    /// Whether the group has been activated.
    /// </summary>
    public bool Activated;


    /// <summary>
    /// The index into the array of the current controlling group.
    /// </summary>
    private int _controllingGroupIndex;


    /// <summary>
    /// Instantiates an instance of state machine for a group of NPCs.
    /// </summary>
    /// <param name="initialState">The state the machine should begin in.</param>
    public TimedGroupNPCStateMachine(State initialState) : base(initialState, true)
    {
        if (Groups != null)
        {
            Groups[0].ControllingGroup = true;
            foreach (TimedNPCGroup group in Groups)
            {
                group.GroupFinished += new TimedNPCGroup.Finished(IncrementControllingIndex);
            }
        }
	}
	
    /// <summary>
    /// Handles the update loop for the state machine.
    /// </summary>
	public override void RunUpdate(bool printTransitionDebug = false)
    {
		if (Activated)
        {
            Groups[_controllingGroupIndex].AddTime();
            base.RunUpdate();
        }
	}


    /// <summary>
    /// A callback function that increments the controlling group index by one when a group reports that they have finished.
    /// </summary>
    private void IncrementControllingIndex()
    {
        _controllingGroupIndex = (_controllingGroupIndex + 1) % Groups.Length;
        Groups[_controllingGroupIndex].ControllingGroup = true;
        continuedAction = null;
        actionsToPerform.Clear();
    }
}
