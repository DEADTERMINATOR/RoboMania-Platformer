﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboMania;
using System;
using CompletionState = StateMachineAction.ActionCompletionState;

/// <summary>
/// Defines a state machine. A state machine is a method of controlling the behaviour of an object through the use of states
/// that contain defined behaviours. The state the object is currently in is controlled through the use of transitions that are 
/// triggered if specified conditions are met. This is the base type of state machine that will always select the first valid transition
/// the loop comes across, even if there are multiple valid transitions to choose from.
/// </summary>
public class StateMachine
{
    /// <summary>
    /// Whether the state machine is currently active.
    /// </summary>
    public bool Active;

    /// <summary>
    /// Public getter for the list of states that this state machine possesses.
    /// </summary>
    public List<State> States { get { return states; } }

    /// <summary>
    /// Public getter for the state machine's current state.
    /// </summary>
    public State CurrentState { get { return currentState; } }

    /// <summary>
    /// Public getter for the action completion state of the last executed action. Allows the following action to optionally execute differently based on what has just happened.
    /// </summary>
    public CompletionState LastActionCompletionState { get { return lastActionCompletionState; } }


    /// <summary>
    /// The list of states present in this state machine.
    /// </summary>
    protected List<State> states = new List<State>();

    /// <summary>
    /// The initial state for this state machine.
    /// </summary>
    protected State initialState;

    /// <summary>
    /// The current state for this state machine.
    /// </summary>
    protected State currentState;

    /// <summary>
    /// The list of global transitions that will be evaluated before any other transitions.
    /// </summary>
    protected List<GlobalTransition> highPriorityGlobalTransitions = new List<GlobalTransition>();

    /// <summary>
    /// The list of global transitions that will be evaluated after all other transitions.
    /// </summary>
    protected List<GlobalTransition> lowPriorityGlobalTransitions = new List<GlobalTransition>();

    /// <summary>
    /// An action that wasn't fully completed in the last update loop to be continued this loop.
    /// </summary>
    protected StateMachineAction continuedAction;

    /// <summary>
    /// The current list of actions that need to be performed.
    /// </summary>
    protected List<StateMachineAction> actionsToPerform;

    /// <summary>
    /// The list of run conditions to be evaluated that can determine whether a state machine update should be run at this time.
    /// </summary>
    protected List<StateMachineRunCondition> runConditions = new List<StateMachineRunCondition>();

    /// <summary>
    /// Whether start has been run. A precaution, as the state machine will be added a runtime to ensure that it's start will be run after
    /// the enemy has set up all the states, transitions, actions, and conditions.
    /// </summary>
    protected bool initialized = false;

    /// <summary>
    /// Whether or not the entry actions have run on the initial state 
    /// </summary>
    protected bool didRunEntryActionsOnStart = false;

    /// <summary>
    /// The action completion state of the last executed action.
    /// </summary>
    protected CompletionState lastActionCompletionState;


    /// <summary>
    /// Instantiates an instance of a state machine.
    /// </summary>
    /// <param name="initialState">The state the machine should begin in.</param>
	public StateMachine(State initialState, bool startActive, params StateMachineRunCondition[] runConditions)
    {
        this.initialState = currentState = initialState;
        states.Add(initialState);

        Active = startActive;

        actionsToPerform = new List<StateMachineAction>();
        this.runConditions = new List<StateMachineRunCondition>(runConditions);
	}
	
    /// <summary>
    /// Runs the update loop for the state machine.
    /// </summary>
    [Obsolete("Please use RunActionCompletionUpdate instead. This version of the methods is being kept for backwards compatibility reasons only.")]
	public virtual void RunUpdate(bool printTransitionDebug = false)
    {
        if (Active)
        {
            if(!didRunEntryActionsOnStart)
            {
                foreach(StateMachineAction ac in initialState.EntryActions)
                {
                    ac.PerformAction();
                }
                didRunEntryActionsOnStart = true;
            }
            Transition triggeredTransition = null;

            if (currentState != null)
            {
                foreach (GlobalTransition transition in highPriorityGlobalTransitions)
                {
                    if (!transition.ExcludedStates.Contains(currentState) && !transition.Disabled && transition.IsTriggered())
                    {
                        if (!transition.StateToTransitionTo.CompareStates(currentState) || (transition.StateToTransitionTo.CompareStates(currentState) && transition.StateToTransitionTo.CanTransitionToSelf))
                        {
                            if (printTransitionDebug)
                            {
                                Debug.Log(transition);
                            }

                            triggeredTransition = transition;
                            break;
                        }
                    }
                }
                if (triggeredTransition == null)
                {
                    foreach (Transition transition in currentState.Transitions)
                    {
                        if (!transition.Disabled && transition.IsTriggered())
                        {
                            if (printTransitionDebug)
                            {
                                Debug.Log(transition);
                            }

                            triggeredTransition = transition;
                            break;
                        }
                    }
                }
                if (triggeredTransition == null)
                {
                    foreach (GlobalTransition transition in lowPriorityGlobalTransitions)
                    {
                        if (!transition.ExcludedStates.Contains(currentState) && !transition.Disabled && transition.IsTriggered())
                        {
                            if (!transition.StateToTransitionTo.CompareStates(currentState) || (transition.StateToTransitionTo.CompareStates(currentState) && transition.StateToTransitionTo.CanTransitionToSelf))
                            {
                                if (printTransitionDebug)
                                {
                                    Debug.Log(transition);
                                }

                                triggeredTransition = transition;
                                break;
                            }
                        }
                    }
                }

                if (triggeredTransition != null)
                {
                    State targetState = triggeredTransition.StateToTransitionTo;
                    continuedAction = null;
                    actionsToPerform.Clear();

                    actionsToPerform.AddRange(currentState.ExitActions);
                    actionsToPerform.AddRange(triggeredTransition.ActionsToPerform);
                    actionsToPerform.AddRange(targetState.EntryActions);

                    currentState.LastTriggeredTransition = triggeredTransition;
                    currentState = targetState;
                }
                else if (actionsToPerform.Count == 0)
                {
                    actionsToPerform.AddRange(currentState.StateActions);
                }

                if (continuedAction != null)
                {
                    //If there is a continued action, we try to finish it here.
                    bool actionComplete = continuedAction.PerformAction();
                    if (actionComplete)
                    {
                        //The continued action was finished, so we can continue with the rest of the actions.
                        actionsToPerform.Remove(continuedAction);
                        continuedAction = null;
                    }
                    else
                    {
                        //The continued action was not finished, so we skip over performing the rest of the actions.
                        return;
                    }
                }

                StateMachineAction[] actionsToPerformCopy = new StateMachineAction[actionsToPerform.Count];
                actionsToPerform.CopyTo(actionsToPerformCopy);

                foreach (StateMachineAction action in actionsToPerformCopy)
                {
                    //We ensure the action is completed.
                    bool actionComplete = action.PerformAction();
                    if (!actionComplete)
                    {
                        //If the action is not completed, we store it to continue during the next update loop and skip the rest of the actions for now.
                        continuedAction = action;
                        return;
                    }
                    else
                    {
                        actionsToPerform.Remove(action);
                    }
                }
                actionsToPerform.Clear(); //If we hit this, then all actions were completed, so we clear the list for the next update.
            }
        }
	}

    /// <summary>
    /// Runs the update loop for the state machine.
    /// </summary>
    public virtual void RunActionCompletionUpdate(bool printTransitionDebug = false)
    {
        if (Active && EvaluateRunConditions())
        {
            if (!didRunEntryActionsOnStart)
            {
                foreach (StateMachineAction ac in initialState.EntryActions)
                {
                    lastActionCompletionState = ac.DoAction();
                }
                didRunEntryActionsOnStart = true;
            }
            Transition triggeredTransition = null;

            if (currentState != null)
            {
                foreach (GlobalTransition transition in highPriorityGlobalTransitions)
                {
                    if (!transition.ExcludedStates.Contains(currentState) && !transition.Disabled && transition.IsTriggered())
                    {
                        if (!transition.StateToTransitionTo.CompareStates(currentState) || (transition.StateToTransitionTo.CompareStates(currentState) && transition.StateToTransitionTo.CanTransitionToSelf))
                        {
                            if (printTransitionDebug)
                            {
                                Debug.Log(transition);
                            }

                            triggeredTransition = transition;
                            break;
                        }
                    }
                }
                if (triggeredTransition == null)
                {
                    foreach (Transition transition in currentState.Transitions)
                    {
                        if (!transition.Disabled && transition.IsTriggered())
                        {
                            if (printTransitionDebug)
                            {
                                Debug.Log(transition);
                            }

                            triggeredTransition = transition;
                            break;
                        }
                    }
                }
                if (triggeredTransition == null)
                {
                    foreach (GlobalTransition transition in lowPriorityGlobalTransitions)
                    {
                        if (!transition.ExcludedStates.Contains(currentState) && !transition.Disabled && transition.IsTriggered())
                        {
                            if (!transition.StateToTransitionTo.CompareStates(currentState) || (transition.StateToTransitionTo.CompareStates(currentState) && transition.StateToTransitionTo.CanTransitionToSelf))
                            {
                                if (printTransitionDebug)
                                {
                                    Debug.Log(transition);
                                }

                                triggeredTransition = transition;
                                break;
                            }
                        }
                    }
                }

                if (triggeredTransition != null)
                {
                    State targetState = triggeredTransition.StateToTransitionTo;
                    continuedAction = null;
                    actionsToPerform.Clear();

                    actionsToPerform.AddRange(currentState.ExitActions);
                    actionsToPerform.AddRange(triggeredTransition.ActionsToPerform);
                    actionsToPerform.AddRange(targetState.EntryActions);

                    currentState.LastTriggeredTransition = triggeredTransition;
                    currentState = targetState;
                }
                else if (actionsToPerform.Count == 0)
                {
                    actionsToPerform.AddRange(currentState.StateActions);
                }

                if (continuedAction != null)
                {
                    //If there is a continued action, we try to finish it here.
                    continuedAction.LastActionCompletionState = lastActionCompletionState = continuedAction.DoAction();
                    if (lastActionCompletionState != CompletionState.ToBeContinued)
                    {
                        if (lastActionCompletionState == CompletionState.RemoveOnSuccessful)
                        {
                            /* This is obviously wildly inefficient. However, under the current State structure there is no way to know which list the action belongs to without checking all the lists.
                               I don't expect RemoveOnSuccessful to be used often enough to justify a State refactor. If that assumption ends up being wrong, we can re-evaluate.
                               Meanwhile, the list checks have been ordered in the likeliness of that action belonging to that list. */
                            bool success = currentState.RemoveStateAction(continuedAction);
                            if (!success)
                            {
                                success = currentState.RemoveEntryAction(continuedAction);
                                if (!success)
                                {
                                    currentState.RemoveExitAction(continuedAction);
                                }
                            }
                        }

                        //The continued action was finished, so we can continue with the rest of the actions.
                        actionsToPerform.Remove(continuedAction);
                        continuedAction = null;
                    }
                    else
                    {
                        //The continued action was not finished, so we skip over performing the rest of the actions.
                        return;
                    }
                }

                StateMachineAction[] actionsToPerformCopy = new StateMachineAction[actionsToPerform.Count];
                actionsToPerform.CopyTo(actionsToPerformCopy);

                foreach (StateMachineAction action in actionsToPerformCopy)
                {
                    //We ensure the action is completed.
                    action.LastActionCompletionState = lastActionCompletionState = action.DoAction();
                    if (lastActionCompletionState == CompletionState.ToBeContinued)
                    {
                        //If the action is not completed, we store it to continue during the next update loop and skip the rest of the actions for now.
                        continuedAction = action;
                        return;
                    }
                    else
                    {
                        if (lastActionCompletionState == CompletionState.RemoveOnSuccessful)
                        {
                            bool success = currentState.RemoveStateAction(action);
                            if (!success)
                            {
                                success = currentState.RemoveEntryAction(action);
                                if (!success)
                                {
                                    currentState.RemoveExitAction(action);
                                }
                            }
                        }

                        actionsToPerform.Remove(action);
                    }
                }
                actionsToPerform.Clear(); //If we hit this, then all actions were completed, so we clear the list for the next update.
            }
        }
    }

    /// <summary>
    /// Adds a state to the list of states for this state machine.
    /// </summary>
    /// <param name="stateToAdd">The state to add.</param>
    public void AddState(State stateToAdd)
    {
        states.Add(stateToAdd);
    }

    /// <summary>
    /// Sets the specified state as the initial state for the state machine.
    /// </summary>
    /// <param name="initialState"></param>
    public void SetInitialState(State initialState)
    {
        this.initialState = initialState;
    }

    /// <summary>
    /// Adds a global transition to the appropriate global transition list, depending on its priority.
    /// </summary>
    /// <param name="transition">The global transition to add</param>
    /// <param name="isHighPriority">Whether the transition is high or low priority (high priority transitions are evaluated before any state specific transitions,
    ///                              low priority ones are evaluated after.</param>
    public void AddGlobalTransition(GlobalTransition transition, bool isHighPriority)
    {
        if (isHighPriority)
        {
            if (!highPriorityGlobalTransitions.Contains(transition))
            {
                highPriorityGlobalTransitions.Add(transition);
            }
        }
        else
        {
            if (!lowPriorityGlobalTransitions.Contains(transition))
            {
                lowPriorityGlobalTransitions.Add(transition);
            }
        }
    }

    protected bool EvaluateRunConditions()
    {
        foreach (StateMachineRunCondition condition in runConditions)
        {
            bool success = condition.CanRun();
            if (!success)
            {
                return false;
            }
        }
        return true;
    }

    public string DebugInfo()
    {
        return currentState.ToString();
    }
}
