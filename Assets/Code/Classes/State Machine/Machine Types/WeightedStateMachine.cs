﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A state machine for that better handles states that have multiple valid transitions. Where the base state machine will always trigger the first
/// available transition when multiple are available, this state machine will choose the highest weighted transition. In cases where there are multiple
/// transitions with the same highest weight, a transition will be chosen at random from the valid transitions.
/// </summary>
public class WeightedStateMachine : StateMachine
{
    /// <summary>
    /// Instantiates an instance of a weighted state machine.
    /// </summary>
    /// <param name="initialState">The state the machine should begin in.</param>
    public WeightedStateMachine(State initialState) : base(initialState, true)
    {

    }

    /// <summary>
    /// Runs the update loop for the weighted state machine.
    /// </summary>
	public override void RunUpdate(bool printTransitionDebug = false)
    {
        List<WeightedTransition> triggeredTransitions = new List<WeightedTransition>(); //We use a list in case there are multiple valid transitions that have the same weight. We want to randomly choose in that case.

        if (currentState != null)
        {
            float highestWeight = -1; //Will be used to keep track of the highest weight transition encountered so far.
            foreach (Transition transition in currentState.Transitions)
            {
                if (!transition.Disabled && transition.IsTriggered())
                {
                    WeightedTransition transitionToAdd = transition as WeightedTransition; //Cast the transition to a weighted transition.
                    if (transitionToAdd == null)
                    {
                        //If the cast came back null, it's not a weighted transition. So let's make it one.
                        transitionToAdd = new WeightedTransition(transition);
                    }

                    if (transitionToAdd != currentState.LastTriggeredTransition || (transitionToAdd == currentState.LastTriggeredTransition && transitionToAdd.ConsecutiveTriggersAllowed))
                    {
                        if (transitionToAdd.Weight > highestWeight)
                        {
                            //If the current transition has a greater weight than any previous transition, we need to clear the list of any transitions
                            //that might have been added earlier. Then we add the new transition.
                            triggeredTransitions.Clear();
                            triggeredTransitions.Add(transitionToAdd);
                            highestWeight = transitionToAdd.Weight;
                        }
                        else if (transitionToAdd.Weight == highestWeight)
                        {
                            //The current transition has the same weight as a previous highest weight transition, so add it to the list to be selected from.
                            triggeredTransitions.Add(transitionToAdd);
                        }
                    }
                }
            }

            if (triggeredTransitions.Count != 0)
            {
                WeightedTransition chosenTransition;
                if (triggeredTransitions.Count == 1)
                {
                    chosenTransition = triggeredTransitions[0];
                }
                else
                {
                    int randNum = Random.Range(0, triggeredTransitions.Count); //Remember the max value in the int version of Random.Range is exclusive. That's why we don't have to subtract one.
                    chosenTransition = triggeredTransitions[randNum];
                }

                State targetState = chosenTransition.StateToTransitionTo;
                actionsToPerform.Clear();

                actionsToPerform.AddRange(currentState.ExitActions);
                actionsToPerform.AddRange(chosenTransition.ActionsToPerform);
                actionsToPerform.AddRange(targetState.EntryActions);

                currentState.LastTriggeredTransition = chosenTransition;
                currentState = targetState;
            }
            else if (actionsToPerform.Count == 0)
            {
                actionsToPerform.AddRange(currentState.StateActions);
            }

            if (continuedAction != null)
            {
                //If there is a continued action, we try to finish it here.
                bool actionComplete = continuedAction.PerformAction();
                if (actionComplete)
                {
                    //The continued action was finished, so we can continue with the rest of the actions.
                    actionsToPerform.Remove(continuedAction);
                    continuedAction = null;
                }
                else
                {
                    //The continued action was not finished, so we skip over performing the rest of the actions.
                    return;
                }
            }

            StateMachineAction[] actionsToPerformCopy = new StateMachineAction[actionsToPerform.Count];
            actionsToPerform.CopyTo(actionsToPerformCopy);

            foreach (StateMachineAction action in actionsToPerformCopy)
            {
                //We ensure the action is completed.
                bool actionComplete = action.PerformAction();
                if (!actionComplete)
                {
                    //If the action is not completed, we store it to continue during the next update loop and skip the rest of the actions for now.
                    continuedAction = action;
                    return;
                }
                else
                {
                    actionsToPerform.Remove(action);
                }
            }
            actionsToPerform.Clear(); //If we hit this, then all actions were completed, so we clear the list for the next update.
        }
    }
}
