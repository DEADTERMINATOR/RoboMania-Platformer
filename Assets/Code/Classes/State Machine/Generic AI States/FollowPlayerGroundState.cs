﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using CompletionState = StateMachineAction.ActionCompletionState;

public class FollowPlayerGroundState : FollowPlayerState
{
    private bool _npcCanFallOffPlatforms;

    /// <summary>
    /// Constructs a follow player state focused on NPC's that can only move on the ground.
    /// </summary>
    /// <param name="npc">The NPC executing this state.</param>
    /// <param name="moveTowardsSpeed">The speed the NPC should follow the player at.</param>
    /// <param name="stopFollowingDistance">How far away from the NPC should the player be before it stops trying to follow the player.</param>
    /// <param name="canFallOffPlatforms">Whether the NPC is allowed to fall off the platform they spawn on.</param>
    /// <param name="continueToLastPlayerPositionWhenPlayerLost">Whether the NPC should still continue to move to the last position it knew the player was at when it stopped following the player.</param>
    public FollowPlayerGroundState(NPC npc, float moveTowardsSpeed, float stopFollowingDistance, bool canFallOffPlatforms, bool continueToLastPlayerPositionWhenPlayerLost) : base("Follow Player (Ground)", npc, 0, 0)
    {
        playerGO = GlobalData.Player.gameObject;

        this.stopFollowingDistance = stopFollowingDistance;
        this.continueToLastPlayerPositionWhenPlayerLost = continueToLastPlayerPositionWhenPlayerLost;

        _npcCanFallOffPlatforms = canFallOffPlatforms;

        AddEntryAction(new FunctionPointerAction(() => { isInAttackPosition = false; _npc.MoveSpeed = moveTowardsSpeed; _npc.TargetVector = playerGO.transform.position; return CompletionState.GuaranteedSuccess; }, "Set Initial State Parameters"));

        CanFollowPlayerAction = new FunctionPointerAction(() => { return CanKeepFollowingPlayer(true); }, "Can Keep Following Player");
        AddStateAction(CanFollowPlayerAction);
        AddStateAction(new FunctionPointerAction(FollowPlayer, "Follow Player"));

        AddExitAction(new FunctionPointerAction(() => { _npc.MoveSpeed = _npc.BaseMoveSpeed; return CompletionState.GuaranteedSuccess; }, "Reset Move Speed"));
    }

    public override CompletionState FollowPlayer()
    {
        bool canFollowPlayer = CanFollowPlayerAction.LastActionCompletionState == CompletionState.Successful;

        if ((canFollowPlayer || (!canFollowPlayer && continueToLastPlayerPositionWhenPlayerLost && !goneToLastPlayerPosition))
            && (_npcCanFallOffPlatforms || (!_npcCanFallOffPlatforms && !_npc.ShouldTurnAround())))
        {
            var moveSuccess = MoveTowards('X', true);
            if (moveSuccess == CompletionState.Successful)
            {
                if (canFollowPlayer)
                {
                    _npc.TargetVector = playerGO.transform.position;
                }
                else if (!canFollowPlayer && !goneToLastPlayerPosition)
                {
                    goneToLastPlayerPosition = true;
                }
            }

            return moveSuccess;
        }

        //We couldn't move towards the player's last position or set a new movement position for the NPC because the player has left the current platform and the NPC is not allowed or cannot follow.
        //Or the NPC has lost the player and gone to their last position and couldn't see them from there.
        return CompletionState.Failed;
    }
}
