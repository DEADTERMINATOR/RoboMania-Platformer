﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using CompletionState = StateMachineAction.ActionCompletionState;

public abstract class FollowPlayerState : GenericRaycastingState
{
    /* If the NPC is at least this distance below the player, they should raycast at the player to make sure that they still have sight on the player
    * The player may be on a higher platform that the NPC wouldn't be able to reach, so following them any further doesn't make sense. */
    private const float DISTANCE_BELOW_PLAYER_SHOULD_TEST_FOR_FOLLOW = 5.0f;

    public StateMachineAction CanFollowPlayerAction { get; protected set; }

    protected float stopFollowingDistance;
    protected bool continueToLastPlayerPositionWhenPlayerLost;
    protected bool goneToLastPlayerPosition = false;
    protected bool isInAttackPosition = false;
    protected GameObject playerGO;


    protected FollowPlayerState(string name, NPC npc, float raycastDistance, LayerMask layersToRaycastFor) : base(name, npc, raycastDistance, layersToRaycastFor) { }

    public abstract CompletionState FollowPlayer();

    /// <summary>
    /// Checks if the NPC can still follow the player by checking whether the player is still within the NPC's follow distance,
    /// and makes sure there is a clear line of sight to the player in the case where the player may be on a platform above the NPC (thus the NPC will never be able to reach them).
    /// </summary>
    /// <returns>A successful completion state if the NPC is cleared to continue following the player. Otherwise, a failed completion state.</returns>
    protected CompletionState CanKeepFollowingPlayer(bool npcOnGround)
    {
        if (Vector2.Distance(playerGO.transform.position, _npc.transform.position) < stopFollowingDistance)
        {
            if (!npcOnGround || (npcOnGround && playerGO.transform.position.y - _npc.transform.position.y > DISTANCE_BELOW_PLAYER_SHOULD_TEST_FOR_FOLLOW))
            {
                var hit = RaycastAtPlayer(stopFollowingDistance);
                if (hit.collider == null || hit.collider.tag != "Player")
                {
                    return CompletionState.Failed;
                }
            }
            return CompletionState.Successful;
        }

        return CompletionState.Failed;
    }

    /// <summary>
    /// A method to be used as a transition test to determine whether the NPC should leave this state because they can't follow the player anymore for whatever reason.
    /// </summary>
    /// <returns>True if the NPC should leave this state. Otherwise, false.</returns>
    public virtual bool StopFollowingPlayer()
    {
        if (CanFollowPlayerAction.LastActionCompletionState == CompletionState.Failed)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// A method to be used as a transition test to determine whether the NPC should leave this state because they are in a position to attack the player.
    /// </summary>
    /// <returns>True if the NPC should leave this state. Otherwise, false.</returns>
    public bool InPositionToAttack()
    {
        return isInAttackPosition;
    }
}
