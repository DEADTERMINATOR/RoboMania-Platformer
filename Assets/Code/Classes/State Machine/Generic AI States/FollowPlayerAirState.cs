﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using static MoveableObject;
using CompletionState = StateMachineAction.ActionCompletionState;

public class FollowPlayerAirState : FollowPlayerState
{
    private const float LOWER_BOUND_ATTACK_ANGLE_FOR_FLYING_ENEMIES = 40f;
    private const float UPPER_BOUND_ATTACK_ANGLE_FOR_FLYING_ENEMEIS = 140f;

    public class AirFollowInformation
    {
        public float MinAttackDistanceFromPlayer;
        public float MaxAttackDistanceFromPlayer;
        public float StopFollowingPlayerDistance;
        public Func<WayPoints> CustomFindAttackPositionMethod;

        public AirFollowInformation(float minDistanceFromPlayer, float maxDistanceFromPlayer, float stopFollowingPlayerDistance, Func<WayPoints> customFindAttackPositionMethod = null)
        {
            MinAttackDistanceFromPlayer = minDistanceFromPlayer;
            MaxAttackDistanceFromPlayer = maxDistanceFromPlayer;
            StopFollowingPlayerDistance = stopFollowingPlayerDistance;
            CustomFindAttackPositionMethod = customFindAttackPositionMethod;
        }
    }

    private AirFollowInformation _airFollowInformation;
    private AreaTiller _areaTiller;

    private WayPoints _currentPath;
    private bool _initialTargetSet = false;
    private StateMachineAction _followPlayerInAirAction;

    /// <summary>
    /// Constructs a follow player state focused on NPC's that can move in the air.
    /// </summary>
    /// <param name="npc">The NPC executing this state.</param>
    /// <param name="airFollowInformation"></param>
    /// <param name="areaTiller">The area tiller that the NPC should use for traversal.</param>
    /// <param name="moveTowardsSpeed">The speed the NPC should follow the player at.</param>
    /// <param name="continueToLastPlayerPositionWhenPlayerLost">Whether the NPC should still continue to move to the last position it knew the player was at when it stopped following the player.</param>
    public FollowPlayerAirState(NPC npc, ref AirFollowInformation airFollowInformation, AreaTiller areaTiller, float moveTowardsSpeed, bool continueToLastPlayerPositionWhenPlayerLost) : base("Follow Player (Air)", npc, 0, 0)
    {
        playerGO = GlobalData.Player.gameObject;

        _airFollowInformation = airFollowInformation;
        _areaTiller = areaTiller;
        stopFollowingDistance = airFollowInformation.StopFollowingPlayerDistance;
        this.continueToLastPlayerPositionWhenPlayerLost = continueToLastPlayerPositionWhenPlayerLost;

        AddEntryAction(new FunctionPointerAction(() => { isInAttackPosition = false; _initialTargetSet = false; _npc.MoveSpeed = moveTowardsSpeed; return CompletionState.GuaranteedSuccess; }, "Set Initial State Parameters"));
        AddEntryAction(new FunctionPointerAction(() =>
        {
            _initialTargetSet = CalculateAirPathToPlayerAttackPosition();
            if (_initialTargetSet)
            {
                _npc.TargetVector = _currentPath.GetTarget();
                return CompletionState.Successful;
            }

            return CompletionState.Failed;
        }, "Set Initial Path"));

        CanFollowPlayerAction = new FunctionPointerAction(() => { return CanKeepFollowingPlayer(false); }, "Can Keep Following Player");
        AddStateAction(CanFollowPlayerAction);

        _followPlayerInAirAction = new FunctionPointerAction(FollowPlayer, "Follow Player");
        AddStateAction(_followPlayerInAirAction);

        AddExitAction(new FunctionPointerAction(() => { _npc.MoveSpeed = _npc.BaseMoveSpeed; return CompletionState.GuaranteedSuccess; }, "Reset Move Speed"));
    }

    /// <summary>
    /// An action that handles the tracking and following of the player for NPC's that can move in the air.
    /// </summary>
    /// <returns>A successful completion state if the NPC is able to move towards the player and/or set a new position to travel to. Otherwise, a failed completion state.</returns>
    public override CompletionState FollowPlayer()
    {
        bool canFollowPlayer = CanFollowPlayerAction.LastActionCompletionState == CompletionState.Successful;
        if ((canFollowPlayer || (!canFollowPlayer && continueToLastPlayerPositionWhenPlayerLost && !goneToLastPlayerPosition)) && _currentPath != null)
        {
            var moveSuccess = MoveTowards('B', false);
            if (moveSuccess == CompletionState.Successful || moveSuccess == CompletionState.InProgress)
            {
                if (canFollowPlayer)
                {
                    var distanceToPlayer = Vector2.Distance(playerGO.transform.position, _npc.transform.position);
                    var distanceFromPlayerToTarget = Vector2.Distance(playerGO.transform.position, _currentPath.Last);
                    var directionToPlayer = playerGO.transform.position - _npc.transform.position;
                    var directionToTarget = _npc.TargetVector - _npc.transform.position;

                    var maxDistance = _airFollowInformation.MaxAttackDistanceFromPlayer * 2 < stopFollowingDistance ? _airFollowInformation.MaxAttackDistanceFromPlayer * 2 : stopFollowingDistance;

                    if (!_currentPath.IsOnLastPoint())
                    {
                        if (distanceToPlayer < _airFollowInformation.MinAttackDistanceFromPlayer)
                        {
                            //The player has moved closer to the enemy, and is now within attack distance. So check if the npc can see the player, and if so stop following the path and attack.
                            var hit = RaycastAtPlayer(_airFollowInformation.StopFollowingPlayerDistance);
                            if (hit && hit.collider.gameObject.layer == GlobalData.PLAYER_LAYER)
                            {
                                isInAttackPosition = true;
                                _npc.ZeroOutVelocity(OBJECT_VELOCITY_COMPONENT);

                                return CompletionState.Successful;
                            }
                            else
                            {
                                var success = CalculateAirPathToPlayerAttackPosition();
                                return success == true ? CompletionState.Successful : CompletionState.Failed;
                            }
                        }
                        if (distanceFromPlayerToTarget >= maxDistance
                            || ((Mathf.Sign(directionToTarget.x) != Mathf.Sign(directionToPlayer.x) || Mathf.Sign(directionToTarget.y) != Mathf.Sign(directionToPlayer.y)) && distanceFromPlayerToTarget >= _airFollowInformation.MaxAttackDistanceFromPlayer))
                        {
                            //The player has moved quite far away from where the NPC is headed, so calculate a new path to their current position.
                            //But keep the old path as a fallback in case the new path fails for any reason.
                            var oldPath = _currentPath;
                            bool success = CalculateAirPathToPlayerAttackPosition();
                            if (!success)
                            {
                                _currentPath = oldPath;
                                _npc.TargetVector = _currentPath.Increment();
                                //_npc.TargetVector = _currentPath.IncrementConnectedPoints(); //Needs further work, committing using the old incrementing so this class is more stable and usable in the meantime.
                            }
                        }
                        else if (moveSuccess == CompletionState.Successful)
                        {
                            //Continue moving towards the player.
                            _npc.TargetVector = _currentPath.Increment();
                            //_npc.TargetVector = _currentPath.IncrementConnectedPoints();
                        }
                    }
                    else if (moveSuccess == CompletionState.Successful)
                    {
                        //The NPC has arrived at the end of the calculated path.
                        if (distanceToPlayer < _airFollowInformation.MaxAttackDistanceFromPlayer)
                        {
                            //The end of the NPC's path is within attack distance of the player, so check if the npc can see the player and if so attack.
                            var hit = RaycastAtPlayer(_airFollowInformation.StopFollowingPlayerDistance);
                            if (hit && hit.collider.gameObject.layer == GlobalData.PLAYER_LAYER)
                            {
                                isInAttackPosition = true;
                                _npc.ZeroOutVelocity(OBJECT_VELOCITY_COMPONENT);

                                return CompletionState.Successful;
                            }
                            else
                            {
                                var success = CalculateAirPathToPlayerAttackPosition();
                                return success == true ? CompletionState.Successful : CompletionState.Failed;
                            }
                        }
                        else
                        {
                            //The end of the NPC's path is not close enough to the player to attack. So try and find a new path to follow.
                            bool success = CalculateAirPathToPlayerAttackPosition();
                            return success == true ? CompletionState.Successful : CompletionState.Failed;
                        }
                    }
                }
                else if (!canFollowPlayer && moveSuccess == CompletionState.Successful && !goneToLastPlayerPosition)
                {
                    goneToLastPlayerPosition = true;
                }
            }

            return moveSuccess;
        }

        return CompletionState.Failed;
    }

    public override bool StopFollowingPlayer()
    {
        if (CanFollowPlayerAction.LastActionCompletionState == CompletionState.Failed || _followPlayerInAirAction.LastActionCompletionState == CompletionState.Failed || !_initialTargetSet)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Chooses and calculates a path to an position from which to attack the player. If the player is within the bounds of npc's associated tilemap,
    /// this position will be decided by either the standard method belonging to this class, or a custom method if one was provided. If the player is
    /// not within the bounds, then the npc will simply travel to the edge of the bounds closest to the player.
    /// </summary>
    /// <returns>True if a path was successfully found. False otherwise.</returns>
    private bool CalculateAirPathToPlayerAttackPosition()
    {
        if (_areaTiller.Map.Bounds.Contains(PathTilleMap.Vector2ToVector2Int(playerGO.transform.position)))
        {
            _currentPath = _airFollowInformation.CustomFindAttackPositionMethod == null ? FindNewAttackPositionPath() : _airFollowInformation.CustomFindAttackPositionMethod.Invoke();
        }
        else
        {
            _currentPath = _areaTiller.Map.SortestOrClosestPathWaypoints(PathTilleMap.Vector2ToVector2Int(_npc.transform.position), PathTilleMap.Vector2ToVector2Int(playerGO.transform.position), true);
        }

        if (_currentPath == null)
        {
            return false;
        }

        _currentPath.ConnectPoints(_npc.MovementCollider.bounds.size);
        foreach (WayPoints.ConnectedWaypoints connectedWaypoint in _currentPath.ConnectedPoints)
        {
            VisualDebug.DrawDebugPoint(connectedWaypoint.FirstPoint, Color.green, 5, 0.25f);
            VisualDebug.DrawDebugPoint(connectedWaypoint.LastPoint, Color.green, 5, 0.25f);
        }

        return true;
    }

    /// <summary>
    /// Selects an attack position based at a random angle (within a constant lower and upper bound) and at a random distance (within the provided minimum and maximum attack distances).
    /// If this position is determined to be valid (i.e. it is marked as traversable in the npc's tile map, and it has line of sight to the player, a path is plotted. If within 10 random points,
    /// not one valid point or path is found, the method terminates.
    /// </summary>
    /// <returns>The found path as waypoints, or null if no path was found.</returns>
    private WayPoints FindNewAttackPositionPath()
    {
        WayPoints newPath = null;
        int count = 0;

        do
        {
            ++count;

            //Find a random point within attack range of the player to move to.
            float randomAngleAbovePlayer = UnityEngine.Random.Range(LOWER_BOUND_ATTACK_ANGLE_FOR_FLYING_ENEMIES, UPPER_BOUND_ATTACK_ANGLE_FOR_FLYING_ENEMEIS);
            var rotatedVector = StaticTools.RotateVector2(new Vector2(1, 0), randomAngleAbovePlayer);
            float randomDistanceFromPlayer = UnityEngine.Random.Range(_airFollowInformation.MinAttackDistanceFromPlayer, _airFollowInformation.MaxAttackDistanceFromPlayer);
            var travelPoint = rotatedVector * randomDistanceFromPlayer + (Vector2)playerGO.transform.position;

            //Verify that it's a valid point to both travel to and that it has line of sight to the player.
            var intPoint = PathTilleMap.Vector2ToVector2Int(travelPoint);
            bool isValid = _areaTiller.Map.IsNodeTraversable(intPoint);
            if (isValid)
            {
                var directionToPlayer = (Vector2)playerGO.transform.position - intPoint;
                var hit = Physics2D.Raycast(intPoint, directionToPlayer, _airFollowInformation.MaxAttackDistanceFromPlayer, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.PLAYER_LAYER_SHIFTED);
                if (hit && hit.collider.gameObject.layer == GlobalData.PLAYER_LAYER)
                {
                    newPath = _areaTiller.Map.SortestOrClosestPathWaypoints(PathTilleMap.Vector2ToVector2Int(_npc.transform.position), intPoint, true);
                    if (newPath == null)
                    {
                        continue;
                    }
                }
            }
        } while (newPath == null && count < 10);

        return newPath;
    }
}
