﻿using Characters;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CompletionState = StateMachineAction.ActionCompletionState;

public class PathBackToWaypoints : GenericRaycastingState
{
    private WayPoints _currentPath;
    private bool _arrivedAtWaypoints = false;

    /// <summary>
    /// Constructs an instance of PathBackToWaypoints state.
    /// </summary>
    /// <param name="npc">The NPC executing this state.</param>
    /// <param name="waypoints">The waypoints component that contains the waypoints the NPC cycles through.</param>
    /// <param name="areaTiler">The area tiler for the area the NPC is within so a path back to the waypoints can be constructed.</param>
    /// <param name="moveBackSpeed">The speed at which the NPC should travel back to their waypoints.</param>
    /// <param name="shouldRaycastForPlayer">Whether the NPC should look for the player along the way.</param>
    /// <param name="raycastInfo">A class containing further information about how the NPC should raycast for the player (if they should).</param>
    public PathBackToWaypoints(NPC npc, WayPointsComponent waypoints, AreaTiller areaTiler, float moveBackSpeed, bool shouldRaycastForPlayer, PlayerRaycastParameters raycastInfo = null) : base("Path Back to Waypoints", npc, 0, shouldRaycastForPlayer == true ? GlobalData.PLAYER_LAYER : 0, raycastInfo)
    {
        AddEntryAction(new FunctionPointerAction(new Func<CompletionState>(() => { _npc.MoveSpeed = moveBackSpeed; return CompletionState.GuaranteedSuccess; }), "Set Move Speed"));
        AddEntryAction(new FunctionPointerAction(new Func<CompletionState>(() => { _currentPath = null; _arrivedAtWaypoints = false; return CompletionState.GuaranteedSuccess; }), "Set Initial State Variables"));

        //Look for a path to the NPC's current target vector (which in this case would be a waypoint).
        AddStateActionAtIndex(new FunctionPointerAction(new Func<CompletionState>(() =>
        {
            if (_currentPath == null)
            {
                _currentPath = areaTiler.Map.SortestOrClosestPathWaypoints(PathTilleMap.Vector2ToVector2Int(_npc.transform.position), PathTilleMap.Vector2ToVector2Int(_npc.TargetVector), true);
                if (_currentPath == null)
                {
                    return CompletionState.Failed;
                }
                else
                {
                    _currentPath.ConnectPoints(npc.MovementCollider.bounds.size);
                    _npc.TargetVector = _currentPath.GetTarget();
                }
            }

            return CompletionState.Successful;
        }), "Find Path to Waypoint"), 0);

        //Travel along the waypoint path until the NPC arrives at the target waypoint.
        AddStateAction(new FunctionPointerAction(new Func<CompletionState>(() =>
        {
            if (_currentPath != null)
            {
                var success = MoveTowards('B', false);
                if (success == CompletionState.Successful)
                {
                    if (_currentPath.IsOnLastPoint())
                    {
                        _arrivedAtWaypoints = true;
                    }
                    else
                    {
                        //IncrementConnectedPoints needs further work, using the old incrementing so this class is more stable and usable in the meantime.
                        _npc.TargetVector = _currentPath.Increment();
                        //_npc.TargetVector = _currentPath.IncrementConnectedPoints();
                    }
                }

                return success;
            }
            else
            {
                //If for some reason don't have a path back to the waypoints, try and move directly towards it and perhaps a proper path can be found along the way.
                _npc.TargetVector = waypoints.WayPoints.First;

                var success = MoveTowards('B', false);
                if (success == CompletionState.Successful)
                {
                    //Somehow we made it back to the waypoints without ever finding a valid path. No clue what scenario would get us to land here.
                    _arrivedAtWaypoints = true;
                }

                return success;
            }
        })));
    }

    /// <summary>
    /// A condition that reports whether the NPC has successfully travelled back to their waypoints.
    /// </summary>
    /// <returns>True if they have arrived. False otherwise.</returns>
    public bool ArrivedAtWaypoints()
    {
        return _arrivedAtWaypoints;
    }
}
