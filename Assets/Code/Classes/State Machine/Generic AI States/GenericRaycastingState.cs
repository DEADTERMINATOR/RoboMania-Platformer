﻿using Characters;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CompletionState = StateMachineAction.ActionCompletionState;

public abstract class GenericRaycastingState : GenericAIState
{
    #region Raycast Info
    public class PlayerRaycastParameters
    {
        public enum RaycastFunctionType { Forward, TowardsPlayer, Custom }

        public readonly RaycastFunctionType FunctionType;
        public readonly bool MustFacePlayerToRaycast;
        public readonly Func<RaycastHit2D> CustomPlayerRaycastFunction;

        public PlayerRaycastParameters(bool raycastTowardsPlayer, bool mustFacePlayerToRaycast)
        {
            if (raycastTowardsPlayer)
            {
                FunctionType = RaycastFunctionType.TowardsPlayer;
            }
            else
            {
                FunctionType = RaycastFunctionType.Forward;
            }

            MustFacePlayerToRaycast = mustFacePlayerToRaycast;
            CustomPlayerRaycastFunction = null;
        }

        public PlayerRaycastParameters(Func<RaycastHit2D> customPlayerRaycastFunction)
        {
            if (customPlayerRaycastFunction == null)
            {
                Debug.LogError("A RaycastInfo that takes a custom raycast function was created, but the provided function is null.");
            }

            FunctionType = RaycastFunctionType.Custom;
            MustFacePlayerToRaycast = false;
            CustomPlayerRaycastFunction = customPlayerRaycastFunction;
        }
    }
    #endregion

    protected float raycastDistance;
    protected PlayerRaycastParameters raycastParameters;


    private StateMachineAction _playerRaycastAction;


    public GenericRaycastingState(string name, NPC npc, float raycastDistance, LayerMask layersToRaycastFor, PlayerRaycastParameters raycastParameters = null) : base(name, npc)
    {
        this.raycastDistance = raycastDistance;

        if (StaticTools.IsLayerInLayerMask(layersToRaycastFor, GlobalData.PLAYER_LAYER))
        {
            if (raycastParameters == null)
            {
                Debug.LogError("The NPC named " + npc.gameObject.name + " uses a raycasting state that raycasts for the player, but PlayerRaycastParameters were not provided");
            }
            else
            {
                this.raycastParameters = raycastParameters;
                _playerRaycastAction = AddStateAction(RaycastForPlayer, "Raycast for Player");
            }
        }
        if (StaticTools.IsLayerInLayerMask(layersToRaycastFor, GlobalData.OBSTACLE_LAYER))
        {
            AddStateAction(RaycastForObstacleInFront, "Raycast for Obstacle");
        }
    }

    /// <summary>
    /// Raycasts for the player according to the parameters defined by the state instance's RaycastInfo.
    /// </summary>
    /// <returns>A failed ActionCompletionState if the player was not hit by the raycast, or the player hit did not fulfill the requirements of the raycast function (e.g. the player was not within the NPC's platform bounding box).</returns>
    protected CompletionState RaycastForPlayer()
    {
        var hit = InvokeAppropriatePlayerRaycastFunction();

        if (hit.collider != null)
        {
            return CompletionState.Successful;
        }
        return CompletionState.Failed;
    }

    protected virtual CompletionState RaycastForObstacleInFront()
    {
        var hit = RaycastForwardForObstacle(raycastDistance);

        if (hit.collider != null)
        {
            return CompletionState.Successful;
        }
        return CompletionState.Failed;
    }

    #region Raycast Support Methods
    /// <summary>
    /// Performs a forward raycast check for any obstacles.
    /// </summary>
    /// <param name="distance">How far in front the raycast should check.</param>
    /// <returns>A RaycastHit2D with the results.</returns>
    protected RaycastHit2D RaycastForwardForObstacle(float distance)
    {
        return FireRaycastForward(distance, GlobalData.OBSTACLE_LAYER_SHIFTED);
    }

    /// <summary>
    /// Performs the appropriate forward raycast check for the player.
    /// </summary>
    /// <param name="distance">How far in front the raycast should check.</param>
    /// <returns>A RaycastHit2D with the results.</returns>
    protected RaycastHit2D RaycastForwardForPlayer(float distance)
    {
        var hit = FireRaycastForward(distance, GlobalData.PLAYER_LAYER_SHIFTED);

        //If the player isn't directly in front of the NPC, check if they are just slightly above but still in front.
        if (hit.collider == null)
        {
            FireRaycastArcInFront(ref hit, distance);
        }

        return hit;
    }

    /// <summary>
    /// Fires a raycast in the direction of the player, looking for both obstacles and the player to check if the player is both in range and a direct line of sight can be achieved.
    /// </summary>
    /// <returns>A RaycastHit2D containing the results.</returns>
    protected RaycastHit2D RaycastAtPlayer(float distance)//, bool mustBeFacingPlayer)
    {
        RaycastHit2D hit = new RaycastHit2D();
        Vector2 directionToPlayer = GlobalData.Player.transform.position - _npc.transform.position;

        //if (!mustBeFacingPlayer || Mathf.Sign(directionToPlayer.x) == _npc.Controller.Collisions.FaceDir)
        //{
        hit = Physics2D.Raycast(_npc.CharacterSpriteCollection.transform.position, directionToPlayer, distance, GlobalData.PLAYER_LAYER_SHIFTED | GlobalData.OBSTACLE_LAYER_SHIFTED);
        Debug.DrawRay(_npc.CharacterSpriteCollection.transform.position, directionToPlayer * distance, Color.green, 0.1f);
        //}

        return hit;
    }

    private RaycastHit2D FireRaycastForward(float distance, int layers)
    {
        RaycastHit2D hit = new RaycastHit2D();
        float baseAngle = _npc.CharacterSpriteCollection.transform.rotation.eulerAngles.z;

        if (_npc.Controller.Collisions.FaceDir == 1) //NPC facing right
        {
            hit = Physics2D.Raycast(_npc.CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle), distance, layers);
            Debug.DrawRay(_npc.CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle) * distance, Color.green, 0.1f);
        }
        else //NPC facing left
        {
            hit = Physics2D.Raycast(_npc.CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle + 180), distance, layers);
            Debug.DrawRay(_npc.CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle + 180) * distance, Color.green, 0.1f);
        }

        return hit;
    }

    /// <summary>
    /// Fires multiple raycasts in an arc from directly in front of the NPC. Intended to catch cases where the player isn't directly in front of the NPC, but they should still be able to be seen (e.g. the player is jumping).
    /// </summary>
    /// <param name="horizontalHit">A reference to RaycastHit2D to put the results into.</param>
    private void FireRaycastArcInFront(ref RaycastHit2D horizontalHit, float distance)
    {
        float baseAngle = _npc.CharacterSpriteCollection.transform.rotation.eulerAngles.z;
        Vector2 raycastDirection;
        float raycastDistance = distance;

        for (int i = 5; i < FRONT_RAYCAST_ARC_DEGREES; i += 5)
        {
            //Shrink the sight distance as the angle gets progressively more intense.
            raycastDistance -= distance * RAYCAST_ARC_DISTANCE_REDUCTION;

            //Rotate the vector to the desired angle.
            if (_npc.Controller.Collisions.FaceDir == -1)
            {
                raycastDirection = StaticTools.Vector2FromAngle(baseAngle + (180 - i)); //If the NPC is facing to the left, we add 180 degrees so we get the desired rotation on the other side.
            }
            else
            {
                raycastDirection = StaticTools.Vector2FromAngle(baseAngle + i);
            }

            horizontalHit = Physics2D.Raycast(_npc.transform.position, raycastDirection, raycastDistance, GlobalData.PLAYER_LAYER_SHIFTED);
            Debug.DrawRay(_npc.transform.position, raycastDirection * raycastDistance, Color.blue, 0.025f);

            if (horizontalHit.collider != null)
            {
                //If the NPC has seen the player, we don't need to continue with further raycasts
                return;
            }
        }
    }
    #endregion

    /// <summary>
    /// Fires a raycast looking for the player in the direction/way defined in the state instance's RaycastInfo.
    /// </summary>
    /// <returns>The raycast hit.</returns>
    protected RaycastHit2D InvokeAppropriatePlayerRaycastFunction()
    {
        RaycastHit2D hit = new RaycastHit2D();
        Vector2 directionToPlayer = GlobalData.Player.transform.position - _npc.transform.position;

        if (!raycastParameters.MustFacePlayerToRaycast || Mathf.Sign(directionToPlayer.x) == _npc.Controller.Collisions.FaceDir)
        {
            if (raycastParameters.FunctionType == PlayerRaycastParameters.RaycastFunctionType.Forward)
            {
                hit = RaycastForwardForPlayer(raycastDistance);
            }
            else if (raycastParameters.FunctionType == PlayerRaycastParameters.RaycastFunctionType.TowardsPlayer)
            {
                hit = RaycastAtPlayer(raycastDistance);
            }
            else
            {
                hit = raycastParameters.CustomPlayerRaycastFunction.Invoke();
            }
        }

        return hit;
    }

    public bool CanSeePlayer()
    {
        if (_playerRaycastAction != null && _playerRaycastAction.LastActionCompletionState == CompletionState.Successful)
        {
            return true;
        }
        return false;
    }
}
