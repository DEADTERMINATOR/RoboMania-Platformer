﻿using System;
using UnityEngine;
using Characters;
using Characters.Player;
using CompletionState = StateMachineAction.ActionCompletionState;

public class PatrolState : GenericRaycastingState
{
    private WayPoints _waypoints;
    private AreaTiller _pathTilemap;
    private RectInt _boundingBox;


    #region Constructors
    /// <summary>
    /// Constructs a patrol state that has the NPC simply move from one end of the platform they are on to the other and back while optionally raycasting for the player.
    /// </summary>
    /// <param name="npc">The NPC executing this state.</param>
    /// <param name="customRaycastFunction">An optional function that can be used define how raycasting should be performed should it differ from the default.</param>
    public PatrolState(NPC npc, float patrolSpeed, bool shouldRaycastForPlayer = false, float raycastDistance = 0, PlayerRaycastParameters raycastInfo = null) : base("Generic Platform Patrol", npc, raycastDistance, shouldRaycastForPlayer == true ? GlobalData.PLAYER_LAYER : 0, raycastInfo)
    {
        AddEntryAction(new FunctionPointerAction(() => { _npc.MoveSpeed = patrolSpeed; return CompletionState.GuaranteedSuccess; }));
        AddStateActionAtIndex(MoveOnPlatform, 0, "Move on Platform");
    }

    /// <summary>
    /// Constructs a patrol state that has the NPC travel through its waypoints, either reversing the waypoints or looping back when it reaches the end (depending on the provided parameter) while optionally raycasting for the player.
    /// </summary>
    /// <param name="npc">The NPC executing this state.</param>
    /// <param name="waypoints">Reference to the waypoints component that defines the NPC waypoints.</param>
    /// <param name="customRaycastFunction">An optional function that can be used define how raycasting should be performed should it differ from the default.</param>
    public PatrolState(NPC npc, MovementAxis axis, WayPointsComponent waypoints, float patrolSpeed, bool shouldRaycastForPlayer = false, float raycastDistance = 0, PlayerRaycastParameters raycastInfo = null) : base("Generic Waypoint Patrol", npc, raycastDistance, shouldRaycastForPlayer == true ? GlobalData.PLAYER_LAYER : 0, raycastInfo)
    {
        _waypoints = waypoints.WayPoints;
        _npc.TargetVector = _waypoints.GetTarget();

        AddEntryAction(new FunctionPointerAction(() => { _npc.MoveSpeed = patrolSpeed; return CompletionState.GuaranteedSuccess; }));
        AddStateActionAtIndex(new FunctionPointerAction(MoveByWaypoints, axis, "Move By Waypoints"), 0);
    }

    /// <summary>
    /// Constructs a patrol state that has the NPC travel between randomly selected positions within the provided bounds while optionally raycasting for the player.
    /// </summary>
    /// <param name="npc">The NPC executing this state.</param>
    /// <param name="pathTilemap">Reference to the AreaTiller that contains the navigation map for the bounding box.</param>
    /// <param name="boundingBox">The bounding box that the NPC can navigate within.</param>
    /// <param name="customRaycastFunction">An optional function that can be used define how raycasting should be performed should it differ from the default.</param>
    public PatrolState(NPC npc, MovementAxis axis, AreaTiller pathTilemap, float patrolSpeed, bool shouldRaycastForPlayer = false, float raycastDistance = 0, PlayerRaycastParameters raycastInfo = null) : base("Generic Bounding Box Patrol", npc, raycastDistance, shouldRaycastForPlayer == true ? GlobalData.PLAYER_LAYER : 0, raycastInfo)
    {
        _pathTilemap = pathTilemap;
        _pathTilemap.MakeMap();

        _boundingBox = pathTilemap.Bounds;

        Vector2 firstPosition = FindRandomPointInBoundingBox(axis);
        _waypoints = pathTilemap.Map.SortestOrClosestPathWaypoints(new Vector2Int((int)_npc.transform.position.x, (int)_npc.transform.position.y), new Vector2Int((int)firstPosition.x, (int)firstPosition.y));
        _npc.TargetVector = _waypoints.GetTarget();

        VisualDebug.DrawDebugPoint(firstPosition, Color.red, 5.0f);
        VisualDebug.DrawDebugPoint(_npc.TargetVector, Color.blue, 1f);

        AddEntryAction(new FunctionPointerAction(() => { _npc.MoveSpeed = patrolSpeed; return CompletionState.GuaranteedSuccess; }));
        AddStateActionAtIndex(new FunctionPointerAction(MoveInBoundingBox, axis, "Move in Bounding Box"), 0);
    }
    #endregion

    #region Move Methods
    /// <summary>
    /// Moves the NPC on a platform by the MoveSpeed and will turn around based on ShouldTurnAround.
    /// </summary>
    /// <returns>A successful ActionCompletionState.</returns>
    private CompletionState MoveOnPlatform()
    {
        if (_npc.ShouldTurnAround())
        {
            _npc.SetSpriteDirection(_npc.Controller.Collisions.FaceDir != 1, true);
        }

        if (_npc.Controller.Collisions.FaceDir == -1)
        {
            _npc.SetVelocityOnOneAxis('X', _npc.MoveSpeed * -1);
            _npc.SetInputOnOneAxis('X', -1);
        }
        else
        {
            _npc.SetVelocityOnOneAxis('X', _npc.MoveSpeed);
            _npc.SetInputOnOneAxis('X', 1);
        }

        return CompletionState.GuaranteedSuccess;
    }

    /// <summary>
    /// Moves the NPC through its waypoints.
    /// </summary>
    /// <param name="axis">The axis the NPC is travelling on as a char. 'X' for x-axis, 'Y' for y-axis, 'B' for both axes.</param>
    /// <returns>An in-progress ActionCompletionState if the NPC has not arrived at a waypoint, a successful ActionCompletionState if it has.</returns>
    private CompletionState MoveByWaypoints(object axis)
    {
        var moveTowardsState = MoveTowards(axis, false);
        if (moveTowardsState == CompletionState.Successful)
        {
            _npc.TargetVector = _waypoints.Increment();
        }
        return moveTowardsState;
    }

    /// <summary>
    /// Moves the NPC between random positions within its bounding box.
    /// </summary>
    /// <param name="axis">The axis the NPC is travelling. 'X' for x-axis, 'Y' for y-axis, 'Both' for both axes.</param>
    /// <returns>An in-progress ActionCompletionState if the NPC has not arrived at its chosen position, a successful ActionCompletionState if it has.</returns>
    private CompletionState MoveInBoundingBox(object axis)
    {
        CompletionState moveTowardsState = MoveTowards(axis, false);
        if (moveTowardsState == CompletionState.Successful)
        {
            if (_waypoints.IsOnLastPoint())
            {
                MovementAxis castedAxis = (MovementAxis)axis;
                Vector2 nextPosition = FindRandomPointInBoundingBox(castedAxis);

                _waypoints = _pathTilemap.Map.SortestOrClosestPathWaypoints(new Vector2Int((int)_npc.transform.position.x, (int)_npc.transform.position.y), new Vector2Int((int)nextPosition.x, (int)nextPosition.y));
                _npc.TargetVector = _waypoints.GetTarget();

                VisualDebug.DrawDebugPoint(nextPosition, Color.red, 5);
                VisualDebug.DrawDebugPoint(_npc.TargetVector, Color.blue, 1);
            }
            else
            {
                _npc.TargetVector = _waypoints.Increment();

                VisualDebug.DrawDebugPoint(_npc.TargetVector, Color.blue, 1);
            }
        }
        return moveTowardsState;
    }
    #endregion

    #region Support Methods
    /// <summary>
    /// Generates a random position within the bounding box along the desired axis.
    /// </summary>
    /// <param name="axis">The char representing the axis the move towards operates on. 'X' for x-axix, 'Y' for y-axis, 'B' for both axes.</param>
    /// <returns>The new random position as a Vector2.</returns>
    private Vector2 FindRandomPointInBoundingBox(MovementAxis axis)
    {
        Vector2 position = _npc.transform.position;
        int count = 0;

        if (axis == MovementAxis.X)
        {
            while (Vector2.Distance(position, _npc.transform.position) < 1 || position.x == _boundingBox.xMax)
            {
                position = new Vector2(UnityEngine.Random.Range(_boundingBox.min.x, _boundingBox.max.x), _npc.transform.position.y);

                ++count;
                if (count >= 10)
                {
                    position = new Vector2(_boundingBox.xMin, _npc.transform.position.y);
                    break;
                }
            }
        }
        else if (axis == MovementAxis.Y)
        {
            while (Vector2.Distance(position, _npc.transform.position) < 1 || position.y == _boundingBox.yMax)
            {
                position = new Vector2(_npc.transform.position.x, UnityEngine.Random.Range(_boundingBox.min.y, _boundingBox.max.y));

                ++count;
                if (count >= 10)
                {
                    position = new Vector2(_npc.transform.position.x, _boundingBox.yMin);
                    break;
                }
            }
        }
        else if (axis == MovementAxis.Both)
        {
            while (Vector2.Distance(position, _npc.transform.position) < 1 || position.x == _boundingBox.xMin || position.y == _boundingBox.yMax)
            {
                position = new Vector2(UnityEngine.Random.Range(_boundingBox.min.x, _boundingBox.max.x), UnityEngine.Random.Range(_boundingBox.min.y, _boundingBox.max.y));

                ++count;
                if (count >= 10)
                {
                    position = new Vector2(_boundingBox.xMin, _boundingBox.yMin);
                    break;
                }
            }
        }

        return position;
    }
    #endregion
}
