﻿using Characters;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CompletionState = StateMachineAction.ActionCompletionState;

public abstract class GenericAIState : State
{
    public enum MovementAxis { X, Y, Both }


    /// <summary>
    /// The desired minimum distance from a target vector to be to consider itself as having arrived at the target.
    /// </summary>
    protected const float ARRIVAL_DISTANCE = 0.25f;
    protected const float TURN_AROUND_VELOCITY_THRESHOLD = 0.25f;
    protected const float Y_DIRECTION_CHANGE_SMOOTH_STEP_TIME = 0.15f;

    protected const float FRONT_RAYCAST_ARC_DEGREES = 50;
    protected const float RAYCAST_ARC_DISTANCE_REDUCTION = 0.075f;


    public bool NPCAllowedToChangeFacingDirection = true;


    protected NPC _npc;


    protected GenericAIState(string name, NPC npc) : base(name) 
    {
        _npc = npc;
        AddEntryAction(ClearActionCompletionStates, "Clear Action Completion States");
    }

    /// <summary>
    /// Checks if the NPC has reached its target, and if it hasn't, moves it closer to it.
    /// </summary>
    /// <param name="axis">The char representing the axis the move towards operates on. 'X' for x-axix, 'Y' for y-axis, 'B' for both axes.</param>
    /// <param name="returnSuccessOnlyOnCompletion">If true, the function will only return true if the NPC has reached its target.</param>
    /// <returns>A successful ActionCompletionState if the NPC has reached its target or the NPC is on its way to the target but this is not treated as a continued action. A failed ActionCompletionState if it can't reach the target for any reason. Otherwise, an in-progress ActionCompletionState if the NPC hasn't yet reached its target.</returns>
    protected CompletionState MoveTowards(object axis, object returnSuccessOnlyOnCompletion)
    {
        var castedAxis = (MovementAxis)axis;
        bool castedReturnSuccess = (bool)returnSuccessOnlyOnCompletion;

        Vector3 targetDirection = Vector3.zero;
        Vector3 newVelocity = Vector3.zero;
        Vector3 frameVelocity = Vector3.zero;
        Vector3 positionAfterFrameVelocity = Vector3.zero;

        bool targetReached = false;
        bool cantReachTarget = false;

        //Check if we've reached the target.
        if (castedAxis == MovementAxis.X)
        {
            targetDirection.x = _npc.TargetVector.x - _npc.transform.position.x < 0 ? -1 : 1;
            newVelocity.x = targetDirection.normalized.x * _npc.MoveSpeed;

            frameVelocity.x = newVelocity.x * Time.fixedDeltaTime;
            positionAfterFrameVelocity.x = _npc.transform.position.x + frameVelocity.x;

            if (Mathf.Abs(_npc.transform.position.x - _npc.TargetVector.x) <= ARRIVAL_DISTANCE
                || (positionAfterFrameVelocity.x > _npc.TargetVector.x && targetDirection.x == 1)
                || (positionAfterFrameVelocity.x < _npc.TargetVector.x && targetDirection.x == -1))
            {
                targetReached = true;
            }
            else if ((targetDirection.x == 1 && _npc.Collisions.Right.IsColliding) || (targetDirection.x == -1 && _npc.Collisions.Left.IsColliding))
            {
                cantReachTarget = true;
            }
        }
        else if (castedAxis == MovementAxis.Y)
        {
            targetDirection.y = _npc.TargetVector.y - _npc.transform.position.y < 0 ? -1 : 1;
            newVelocity.y = targetDirection.normalized.y * _npc.MoveSpeed;

            frameVelocity.y = newVelocity.y * Time.fixedDeltaTime;
            positionAfterFrameVelocity.y = _npc.transform.position.y + frameVelocity.y;

            if (Mathf.Abs(_npc.transform.position.y - _npc.TargetVector.y) <= ARRIVAL_DISTANCE
                || (positionAfterFrameVelocity.y > _npc.TargetVector.y && targetDirection.y == 1)
                || (positionAfterFrameVelocity.y < _npc.TargetVector.y && targetDirection.y == -1))
            {
                targetReached = true;
            }
            else if ((targetDirection.y == 1 && _npc.Collisions.Above.IsColliding) || (targetDirection.y == -1 && _npc.Collisions.Below.IsColliding))
            {
                cantReachTarget = true;
            }
        }
        else
        {
            targetDirection = _npc.TargetVector - _npc.transform.position;
            newVelocity = targetDirection.normalized * _npc.MoveSpeed;

            frameVelocity = newVelocity * Time.fixedDeltaTime;
            positionAfterFrameVelocity = _npc.transform.position + frameVelocity;

            var targetDirectionFromNewPosition = _npc.TargetVector - positionAfterFrameVelocity;

            if (Vector3.Distance(_npc.transform.position, _npc.TargetVector) <= ARRIVAL_DISTANCE
                || (Mathf.Sign(targetDirection.x) != Mathf.Sign(targetDirectionFromNewPosition.x) && Mathf.Sign(targetDirection.y) != Mathf.Sign(targetDirectionFromNewPosition.y)))
            {
                targetReached = true;
            }
            else if ((targetDirection.x == 1 && _npc.Collisions.Right.IsColliding) || (targetDirection.x == -1 && _npc.Collisions.Left.IsColliding))
            {
                cantReachTarget = true;
            }
            else if ((targetDirection.y == 1 && _npc.Collisions.Above.IsColliding) || (targetDirection.y == -1 && _npc.Collisions.Below.IsColliding))
            {
                cantReachTarget = true;
            }
        }

        var currentNPCVelocity = _npc.GetVelocityComponentVelocity(MoveableObject.OBJECT_VELOCITY_COMPONENT);
        //We haven't reached the target, so keep moving towards it.
        //Set the NPC input and velocity based on the target direction and the selected axes of movement.
        if (castedAxis == MovementAxis.X || castedAxis == MovementAxis.Both)
        {
            if (newVelocity.x < 0)
            {
                _npc.SetInputOnOneAxis('X', -1);
            }
            else if (newVelocity.x > 0)
            {
                _npc.SetInputOnOneAxis('X', 1);
            }
            else
            {
                _npc.SetInputOnOneAxis('X', 0);
            }

            _npc.SetVelocityOnOneAxis('X', newVelocity.x);
        }

        if (castedAxis == MovementAxis.Y || castedAxis == MovementAxis.Both)
        {
            if (newVelocity.y < 0)
            {
                _npc.SetInputOnOneAxis('Y', -1);
            }
            else if (newVelocity.y > 0)
            {
                _npc.SetInputOnOneAxis('Y', 1);
            }
            else
            {
                _npc.SetInputOnOneAxis('Y', 0);
            }

            _npc.SetVelocityOnOneAxis('Y', newVelocity.y);
        }

        if (NPCAllowedToChangeFacingDirection)
        {
            ShouldTurnAroundInput();
        }

        if (cantReachTarget)
        {
            return CompletionState.Failed;
        }

        if (!targetReached)
        {
            if (castedReturnSuccess)
            {
                return CompletionState.ToBeContinued;
            }
            else
            {
                return CompletionState.InProgress;
            }
        }
        return CompletionState.Successful;
    }

    /// <summary>
    /// Clears the action completion states for every action in the state. Intended to be used as the state is entered so the state begins at a clean slate.
    /// </summary>
    /// <returns>A guaranteed success completion state.</returns>
    protected CompletionState ClearActionCompletionStates()
    {
        foreach (StateMachineAction action in _entryActions)
        {
            action.LastActionCompletionState = CompletionState.NotRun;
        }

        foreach (StateMachineAction action in _stateActions)
        {
            action.LastActionCompletionState = CompletionState.NotRun;
        }

        foreach (StateMachineAction action in _exitActions)
        {
            action.LastActionCompletionState = CompletionState.NotRun;
        }

        return CompletionState.GuaranteedSuccess;
    }


    /// <summary>
    /// Determines whether the NPC should turn around based on their movement input vector.
    /// </summary>
    protected void ShouldTurnAroundInput()
    {
        if (_npc.MovementInput.x < 0 && _npc.Velocity.x <= -TURN_AROUND_VELOCITY_THRESHOLD)
        {
            _npc.SetSpriteDirection(false);
        }
        else if (_npc.MovementInput.x > 0 && _npc.Velocity.x >= TURN_AROUND_VELOCITY_THRESHOLD)
        {
            _npc.SetSpriteDirection(true);
        }
    }
}
