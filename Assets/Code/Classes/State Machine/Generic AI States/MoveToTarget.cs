﻿using Characters;
using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CompletionState = StateMachineAction.ActionCompletionState;

public class MoveToTarget : GenericAIState
{
    private StateMachineAction _moveTowardsTargetAction;
    private Rect _moveableArea;


    /// <summary>
    /// Moves the NPC towards the position of the provided point. Future points will need to be set manually with the SetTargetPoint method or by setting the NPC's target vector directly.
    /// If a moveable area rect is provided, the NPC will only move towards the target position if the position is within that moveable area.
    /// </summary>
    /// <param name="npc">The NPC running this state.</param>
    /// <param name="moveAxis">The axis the NPC moves on. X for x-axis, Y for y-axis, or B for both axes.</param>
    /// <param name="moveShouldReturnInProgressState">Whether the move towards action should return a successful ActionCompletionState on arrival only, or every time the NPC moves closer to the target.</param>
    /// <param name="point">The target point.</param>
    /// <param name="moveableArea">An optional rect defining a space the NPC is allowed to move within.</param>
    public MoveToTarget(NPC npc, char moveAxis, bool moveShouldReturnInProgressState, Vector2 point, Rect? moveableArea = null) : base("Move to Target", npc)
    {
        AddEntryAction(new FunctionPointerAction(new Func<CompletionState>(() => { npc.TargetVector = point; return CompletionState.GuaranteedSuccess; })));

        if (moveableArea.HasValue)
        {
            _moveableArea = moveableArea.Value;
            _moveTowardsTargetAction = new FunctionPointerAction(MoveTowardsTargetWithinMoveableArea, moveAxis, moveShouldReturnInProgressState, "Move Towards Target");
        }
        else
        {
            _moveTowardsTargetAction = new FunctionPointerAction(MoveTowardsTarget, moveAxis, moveShouldReturnInProgressState, "Move Towards Target");
        }
        AddStateAction(_moveTowardsTargetAction);
    }

    /// <summary>
    /// Moves the NPC towards the position of the targetObject parameter. If a moveable area rect is provided, the NPC will only move towards the target position if the position is within that moveable area.
    /// </summary>
    /// <param name="npc">The NPC running this state.</param>
    /// <param name="moveAxis">The axis the NPC moves on. X for x-axis, Y for y-axis, or B for both axes.</param>
    /// <param name="moveShouldReturnInProgressState">Whether the move towards action should return a successful ActionCompletionState on arrival only, or every time the NPC moves closer to the target.</param>
    /// <param name="targetObject">The game object the NPC should be moving towards</param>
    /// <param name="moveableArea">An optional rect defining a space the NPC is allowed to move within.</param>
    public MoveToTarget(NPC npc, char moveAxis, bool moveShouldReturnInProgressState, GameObject targetObject, Rect? moveableArea = null) : base("Move to Target", npc) 
    {
        if (targetObject != null)
        {
            AddEntryAction(new FunctionPointerAction(new Func<CompletionState>(() => { npc.TargetVector = targetObject.transform.position; return CompletionState.GuaranteedSuccess; })));
        }

        if (moveableArea.HasValue)
        {
            _moveableArea = moveableArea.Value;
            _moveTowardsTargetAction = new FunctionPointerAction(MoveTowardsTargetWithinMoveableArea, moveAxis, moveShouldReturnInProgressState, "Move Towards Target");
        }
        else
        {
            _moveTowardsTargetAction = new FunctionPointerAction(MoveTowardsTarget, moveAxis, moveShouldReturnInProgressState, "Move Towards Target");
        }
        AddStateAction(_moveTowardsTargetAction);
    }

    private CompletionState MoveTowardsTarget(object axis, object moveShouldReturnInProgressState)
    {
        if (!_npc.ShouldTurnAround())
        {
            var moveSuccess = MoveTowards(axis, moveShouldReturnInProgressState);
            return moveSuccess;
        }
        return CompletionState.Failed;
    }

    private CompletionState MoveTowardsTargetWithinMoveableArea(object axis, object moveShouldReturnInProgressState)
    {
        if (_moveableArea.Contains(_npc.TargetVector))
        {
            var moveSuccess = MoveTowards(axis, moveShouldReturnInProgressState);
            return moveSuccess;
        }
        return CompletionState.Failed;
    }

    public void SetTargetPoint(Vector2 targetPoint)
    {
        _npc.TargetVector = targetPoint;
    }

    public void SetMoveableArea(Rect moveableArea)
    {
        _moveableArea = moveableArea;
    }

    public bool ArrivedAtTarget()
    {
        return _moveTowardsTargetAction.LastActionCompletionState == CompletionState.Successful ? true : false;
    }

    public bool CantGetToTarget()
    {
        return _moveTowardsTargetAction.LastActionCompletionState == CompletionState.Failed ? true : false;
    }
}
