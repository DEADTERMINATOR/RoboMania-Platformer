﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using static MoveableObject;
using CompletionState = StateMachineAction.ActionCompletionState;
using Direction = Controller2D.CollisionInfo.CollisionDirectionInfo.Direction;

public class MoveAroundPlatform : GenericAIState
{
    private const float ROTATION_TIME_PER_NPC_WIDTH_UNIT = 0.25f;
    private const float EDGE_DETECTION_DISTANCE = 1f;
    private const float MOVE_TO_SIDE_DISTANCE = 0.025f;

    /// <summary>
    /// The "side" of the platform, as would represented by the surface's normal vector (e.g. "Up" is the side where the normal vector points straight up).
    /// </summary>
    public enum PlatformSide { Left, Right, Up, Down }

    public PlatformSide CurrentSideDirection { get; private set; }
    public bool IsRotating { get; private set; }


    private float _currentRotationPercentage;
    private float _currentRotation;
    private float _targetRotation;

    private Rect _allowedMovementArea;

    private bool _hasCollidedWithPlatform = false;

    private OnScreenRunCondition _rotateNPCSpriteCheck;


    public MoveAroundPlatform(NPC npc, PlatformSide startingSide, float startingRotation, Rect allowedMovementArea, bool setInitialNPCRotation) : base("Move Around Platform", npc)
    {
        CurrentSideDirection = startingSide;
        _currentRotation = startingRotation;
        _allowedMovementArea = allowedMovementArea;

        if (setInitialNPCRotation)
        {
            npc.CharacterSpriteCollection.transform.rotation = Quaternion.Euler(npc.CharacterSpriteCollection.transform.eulerAngles.x, npc.CharacterSpriteCollection.transform.eulerAngles.y, startingRotation);
        }

        SetNPCGravity(startingSide);

        npc.Controller.Collisions.Above.CollisionOccurred += RespondToCollision;
        npc.Controller.Collisions.Below.CollisionOccurred += RespondToCollision;
        npc.Controller.Collisions.Left.CollisionOccurred += RespondToCollision;
        npc.Controller.Collisions.Right.CollisionOccurred += RespondToCollision;

        if (_allowedMovementArea != Rect.zero)
        {
            AddStateAction(CheckPositionInsideMovementArea, "Check Current Position Is Inside Movement Area");
        }
        AddStateAction(CheckForPossibleSideChange, "Check for Possible Side Change");
        AddStateAction(MoveOnCurrentSide, "Move On Current Side");
        AddStateAction(RotateNPCSprite, "Rotate NPC Sprite");
    }

    private CompletionState CheckPositionInsideMovementArea()
    {
        Vector2 checkPositionInsideMovementArea = Vector2.zero;
        switch (CurrentSideDirection)
        {
            case PlatformSide.Up:
                if (_npc.FaceDir == 1)
                {
                    checkPositionInsideMovementArea = new Vector2(_npc.MovementCollider.bounds.max.x, _npc.MovementCollider.bounds.center.y);
                }
                else
                {
                    checkPositionInsideMovementArea = new Vector2(_npc.MovementCollider.bounds.min.x, _npc.MovementCollider.bounds.center.y);
                }
                break;
            case PlatformSide.Down:
                if (_npc.FaceDir == 1)
                {
                    checkPositionInsideMovementArea = new Vector2(_npc.MovementCollider.bounds.min.x, _npc.MovementCollider.bounds.center.y);
                }
                else
                {
                    checkPositionInsideMovementArea = new Vector2(_npc.MovementCollider.bounds.max.x, _npc.MovementCollider.bounds.center.y);
                }
                break;
            case PlatformSide.Left:
                if (_npc.FaceDir == 1)
                {
                    checkPositionInsideMovementArea = new Vector2(_npc.MovementCollider.bounds.center.x, _npc.MovementCollider.bounds.max.y);
                }
                else
                {
                    checkPositionInsideMovementArea = new Vector2(_npc.MovementCollider.bounds.center.x, _npc.MovementCollider.bounds.min.y);
                }
                break;
            case PlatformSide.Right:
                if (_npc.FaceDir == 1)
                {
                    checkPositionInsideMovementArea = new Vector2(_npc.MovementCollider.bounds.center.x, _npc.MovementCollider.bounds.min.y);
                }
                else
                {
                    checkPositionInsideMovementArea = new Vector2(_npc.MovementCollider.bounds.center.x, _npc.MovementCollider.bounds.max.y);
                }
                break;
        }

        if (!_allowedMovementArea.Contains(checkPositionInsideMovementArea))
        {
            _npc.SetSpriteDirection(_npc.FaceDir == 1 ? false : true);
        }

        return CompletionState.Successful;
    }

    private CompletionState MoveOnCurrentSide()
    {
        if (CurrentSideDirection == PlatformSide.Left || CurrentSideDirection == PlatformSide.Right)
        {
            if (CurrentSideDirection == PlatformSide.Left)
            {
                _npc.SetVelocity(new Vector2(_npc.Velocity.x, _npc.MoveSpeed * _npc.FaceDir), OBJECT_VELOCITY_COMPONENT);
            }
            else
            {
                _npc.SetVelocity(new Vector2(_npc.Velocity.x, _npc.MoveSpeed * -_npc.FaceDir), OBJECT_VELOCITY_COMPONENT);
            }
            _npc.SetInput(new Vector2(0, _npc.FaceDir));
        }
        else
        {
            if (CurrentSideDirection == PlatformSide.Up)
            {
                _npc.SetVelocity(new Vector2(_npc.MoveSpeed * _npc.FaceDir, _npc.Velocity.y), OBJECT_VELOCITY_COMPONENT);                
            }
            else
            {
                _npc.SetVelocity(new Vector2(_npc.MoveSpeed * -_npc.FaceDir, _npc.Velocity.y), OBJECT_VELOCITY_COMPONENT);
            }
            _npc.SetInput(new Vector2(_npc.FaceDir, 0));
        }

        return CompletionState.Successful;
    }

    private CompletionState CheckForPossibleSideChange()
    {
        if (_hasCollidedWithPlatform && !IsRotating)
        {
            switch (CurrentSideDirection)
            {
                case PlatformSide.Left:
                    if (!_npc.Controller.Collisions.IsCollidingRight)
                    {
                        VerifySideChangeFromNoEdge();
                    }
                    break;
                case PlatformSide.Right:
                    if (!_npc.Controller.Collisions.IsCollidingLeft)
                    {
                        VerifySideChangeFromNoEdge();
                    }
                    break;
                case PlatformSide.Up:
                    if (!_npc.Controller.Collisions.IsCollidingBelow)
                    {
                        VerifySideChangeFromNoEdge();
                    }
                    break;
                case PlatformSide.Down:
                    if (!_npc.Controller.Collisions.Above.IsColliding)
                    {
                        VerifySideChangeFromNoEdge();
                    }
                    break;
            }
        }

        return CompletionState.Successful;
    }

    private void VerifySideChangeFromCollision(Direction collisionDirection)
    {
        switch (CurrentSideDirection)
        {
            case PlatformSide.Left:
                if (collisionDirection == Direction.Above)
                {
                    SetNewSide(PlatformSide.Down, false, _currentRotation + 90f);
                }
                else if (collisionDirection == Direction.Below)
                {
                    SetNewSide(PlatformSide.Up, false, _currentRotation - 90f);
                }
                break;
            case PlatformSide.Right:
                if (collisionDirection == Direction.Above)
                {
                    SetNewSide(PlatformSide.Down, false, _currentRotation - 90f);
                }
                else if (collisionDirection == Direction.Below)
                {
                    SetNewSide(PlatformSide.Up, false, _currentRotation + 90f);
                }
                break;
            case PlatformSide.Up:
                if (collisionDirection == Direction.Left)
                {
                    SetNewSide(PlatformSide.Right, false, _currentRotation - 90f);
                }
                else if (collisionDirection == Direction.Right)
                {
                    SetNewSide(PlatformSide.Left, false, _currentRotation + 90f);
                }
                break;
            case PlatformSide.Down:
                if (collisionDirection == Direction.Left)
                {
                    SetNewSide(PlatformSide.Right, false, _currentRotation - 90f);
                }
                else if (collisionDirection == Direction.Right)
                {
                    SetNewSide(PlatformSide.Left, false, _currentRotation + 90f);
                }
                break;
        }
    }

    private void VerifySideChangeFromNoEdge()
    {
        if (RaycastForPlatformEdge() == CompletionState.Successful)
        {
            switch (CurrentSideDirection)
            {
                case PlatformSide.Left:
                    if (_npc.FaceDir == 1)
                    {
                        SetNewSide(PlatformSide.Up, true, _currentRotation - 90f);
                    }
                    else if (_npc.FaceDir == -1)
                    {
                        SetNewSide(PlatformSide.Down, true, _currentRotation + 90f);
                    }
                    break;
                case PlatformSide.Right:
                    if (_npc.FaceDir == 1)
                    {
                        SetNewSide(PlatformSide.Down, true, _currentRotation - 90f);
                    }
                    else if (_npc.FaceDir == -1)
                    {
                        SetNewSide(PlatformSide.Up, true, _currentRotation + 90f);
                    }
                    break;
                case PlatformSide.Up:
                    if (_npc.FaceDir == 1)
                    {
                        SetNewSide(PlatformSide.Right, true, _currentRotation - 90f);
                    }
                    else if (_npc.FaceDir == -1)
                    {
                        SetNewSide(PlatformSide.Left, true, _currentRotation + 90f);
                    }
                    break;
                case PlatformSide.Down:
                    if (_npc.FaceDir == 1)
                    {
                        SetNewSide(PlatformSide.Left, true, _currentRotation - 90f);
                    }
                    else if (_npc.FaceDir == -1)
                    {
                        SetNewSide(PlatformSide.Right, true, _currentRotation + 90f);
                    }
                    break;
            }
        }
    }

    private void SetNewSide(PlatformSide newSide, bool moveNPCToSide, float targetRotation)
    {
        CurrentSideDirection = newSide;

        if (moveNPCToSide)
        {
            Vector3 moveToPlatformSide = Vector3.zero;
            switch (CurrentSideDirection)
            {
                case PlatformSide.Left:
                    moveToPlatformSide = new Vector3(0, _npc.Controller.Collisions.FaceDir == 1 ? MOVE_TO_SIDE_DISTANCE : -MOVE_TO_SIDE_DISTANCE, 0);
                    _npc.ZeroOutVelocityOnOneAxis('X');
                    break;
                case PlatformSide.Right:
                    moveToPlatformSide = new Vector3(0, _npc.Controller.Collisions.FaceDir == 1 ? -MOVE_TO_SIDE_DISTANCE : MOVE_TO_SIDE_DISTANCE, 0);
                    _npc.ZeroOutVelocityOnOneAxis('X');
                    break;
                case PlatformSide.Up:
                    moveToPlatformSide = new Vector3(_npc.Controller.Collisions.FaceDir == 1 ? MOVE_TO_SIDE_DISTANCE : -MOVE_TO_SIDE_DISTANCE, 0, 0);
                    _npc.ZeroOutVelocityOnOneAxis('Y');
                    break;
                case PlatformSide.Down:
                    moveToPlatformSide = new Vector3(_npc.Controller.Collisions.FaceDir == 1 ? -MOVE_TO_SIDE_DISTANCE : MOVE_TO_SIDE_DISTANCE, 0, 0);
                    _npc.ZeroOutVelocityOnOneAxis('Y');
                    break;
            }

            _npc.Controller.Move(ref moveToPlatformSide, 'N', 'N', true);
        }

        _targetRotation = targetRotation;

        SetNPCGravity(CurrentSideDirection);
        IsRotating = true;

        /* A micro-optimization using a StateMachineRunCondition that caused too many problems for the near nil performance gains it provided.
         * Still, leaving it here in case I decided to revisit and to try and solve the problems.
        bool rotateCanRun = _rotateNPCSpriteCheck.CanRun();
        if (rotateCanRun)
        {
            //The rotation will happen overtime in the RotateNPCSprite actions.
            IsRotating = true;
        }
        else
        {
            //Warp the rotation now, since the player can't see it. So there is no point in performing the whole rotation procedure.
            _npc.CharacterSpriteCollection.transform.rotation = Quaternion.Euler(_npc.CharacterSpriteCollection.transform.eulerAngles.x, _npc.CharacterSpriteCollection.transform.eulerAngles.y, _targetRotation);
            _currentRotation = _targetRotation;
        }
        */
    }

    private CompletionState RotateNPCSprite()
    {
        if (IsRotating)
        {
            float totalRotationTime = _npc.MovementCollider.bounds.size.x * ROTATION_TIME_PER_NPC_WIDTH_UNIT;
            _currentRotationPercentage += Time.deltaTime / totalRotationTime;
            if (_currentRotationPercentage > 1f)
            {
                _currentRotationPercentage = 1f;
            }

            _npc.CharacterSpriteCollection.transform.rotation = Quaternion.Euler(_npc.CharacterSpriteCollection.transform.eulerAngles.x, _npc.CharacterSpriteCollection.transform.eulerAngles.y, Mathf.LerpAngle(_currentRotation, _targetRotation, _currentRotationPercentage));

            if (_currentRotationPercentage >= 1f)
            {
                IsRotating = false;

                _currentRotation = _targetRotation;
                _currentRotationPercentage = 0f;

                return CompletionState.Successful;
            }

            return CompletionState.InProgress;
        }
        return CompletionState.EarlyTermination;
    }

    private CompletionState RaycastForPlatformEdge()
    {
        var groundController = (GroundController2D)_npc.Controller;
        groundController.Raycaster.SetCastLength(EDGE_DETECTION_DISTANCE);
        RaycastHit2D hit = new RaycastHit2D();

        switch (CurrentSideDirection)
        {
            case PlatformSide.Left:
                groundController.Raycaster.SetCastOrigin(Mathf.Sign(_npc.Velocity.y) == 1 ? groundController.Raycaster.Origins.TopRight : groundController.Raycaster.Origins.BottomRight);
                hit = groundController.Raycaster.FireCastAndReturnFirstResult('X', 1, GlobalData.OBSTACLE_LAYER_SHIFTED);
                break;
            case PlatformSide.Right:
                groundController.Raycaster.SetCastOrigin(Mathf.Sign(_npc.Velocity.y) == 1 ? groundController.Raycaster.Origins.TopLeft : groundController.Raycaster.Origins.BottomLeft);
                hit = groundController.Raycaster.FireCastAndReturnFirstResult('X', -1, GlobalData.OBSTACLE_LAYER_SHIFTED);
                break;
            case PlatformSide.Up:
                groundController.Raycaster.SetCastOrigin(Mathf.Sign(_npc.Velocity.x) == 1 ? groundController.Raycaster.Origins.BottomRight : groundController.Raycaster.Origins.BottomLeft);
                hit = groundController.Raycaster.FireCastAndReturnFirstResult('Y', -1, GlobalData.OBSTACLE_LAYER_SHIFTED);
                break;
            case PlatformSide.Down:
                groundController.Raycaster.SetCastOrigin(Mathf.Sign(_npc.Velocity.x) == 1 ? groundController.Raycaster.Origins.TopRight : groundController.Raycaster.Origins.TopLeft);
                hit = groundController.Raycaster.FireCastAndReturnFirstResult('Y', 1, GlobalData.OBSTACLE_LAYER_SHIFTED);
                break;
        }

        if (!hit)
        {
            return CompletionState.Successful;
        }
        return CompletionState.Failed;
    }

    private void SetNPCGravity(PlatformSide platformSide)
    {
        switch (platformSide)
        {
            case PlatformSide.Left:
                _npc.CurrentActiveGravityState.ChangeGravityDirection(Vector2.right);
                break;
            case PlatformSide.Right:
                _npc.CurrentActiveGravityState.ChangeGravityDirection(Vector2.left);
                break;
            case PlatformSide.Up:
                _npc.CurrentActiveGravityState.ChangeGravityDirection(Vector2.down);
                break;
            case PlatformSide.Down:
                _npc.CurrentActiveGravityState.ChangeGravityDirection(Vector2.up);
                break;
        }
        _npc.GravityScale = 1;
    }

    private void RespondToCollision(Controller2D.CollisionInfo.CollisionDirectionInfo collisionInfo)
    {
        if (!_hasCollidedWithPlatform)
        {
            _hasCollidedWithPlatform = true;
        }

        if (collisionInfo.CollidingLayer == GlobalData.OBSTACLE_LAYER && !IsRotating)
        {
            VerifySideChangeFromCollision(collisionInfo.CollisionDirection);
        }
    }
}
