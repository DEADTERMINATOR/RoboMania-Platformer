﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightedTransition : Transition
{
    /// <summary>
    /// How much weight this transition should have. Measured on a scale of 1 (lowest priority, likely only called if there is no other valid transition)
    /// to 10 (highest priority, guaranteed to be called if valid and there are no other 10 priority transitions). When used in a weighted state machine,
    /// the transition with the highest weight will be triggered. If there are multiple valid transitions with the same weight, then the next transition 
    /// will be randomly chosen from the valid list.
    /// </summary>
    public byte Weight { get; private set; }

    /// <summary>
    /// Whether this transition is allowed to be triggered consecutively.
    /// </summary>
    public bool ConsecutiveTriggersAllowed { get; private set; }


    /// <summary>
    /// Constructor for a weighted transition.
    /// </summary>
    /// <param name="stateToTransitionTo">The state this transition leads to.</param>
    /// <param name="transitionCondition">The condition that must be met for this transition to trigger.</param>
    /// <param name="weight">The weight of this transition.</param>
    public WeightedTransition(State stateToTransitionTo, Condition transitionCondition, byte weight = 5, bool consecutiveTriggersAllowed = true) : base(stateToTransitionTo, transitionCondition)
    {
        Weight = weight;
        ConsecutiveTriggersAllowed = consecutiveTriggersAllowed;
    }

    /// <summary>
    /// Consturctor for a weighted transition. This version constructs a weighted transition from a regular transition.
    /// These transitions are given a default weight of 5.
    /// </summary>
    /// <param name="transition">The transition to construct a weighted transition from.</param>
    public WeightedTransition(Transition transition, byte weight = 5, bool consecutiveTriggersAllowed = true) : base(transition.StateToTransitionTo, transition.TransitionCondition)
    {
        Weight = weight;
        ConsecutiveTriggersAllowed = consecutiveTriggersAllowed;
    }
}
