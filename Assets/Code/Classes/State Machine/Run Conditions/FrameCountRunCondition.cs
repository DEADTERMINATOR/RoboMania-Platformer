﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameCountRunCondition : StateMachineRunCondition
{
    private int _numFramesToSkipBeforeRun;
    private int _currentFrameCount;


    public FrameCountRunCondition(int numFramesToSkipBeforeRun, int frameCountToStartAt) => (_numFramesToSkipBeforeRun, _currentFrameCount) = (numFramesToSkipBeforeRun, frameCountToStartAt);

    public override bool CanRun()
    {
        ++_currentFrameCount;
        if (_currentFrameCount >= _numFramesToSkipBeforeRun)
        {
            _currentFrameCount = 0;
            return true;
        }
        return false;
    }
}
