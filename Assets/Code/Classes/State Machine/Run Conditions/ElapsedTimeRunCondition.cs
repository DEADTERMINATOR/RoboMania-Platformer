﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElapsedTimeRunCondition : StateMachineRunCondition
{
    private float _timeBetweenRuns;
    private float _currentTime;


    public ElapsedTimeRunCondition(float timeBetweenRuns, float startingTime) => (_timeBetweenRuns, _currentTime) = (timeBetweenRuns, startingTime);

    public override bool CanRun()
    {
        _currentTime += Time.deltaTime;
        if (_currentTime >= _timeBetweenRuns)
        {
            _currentTime = 0;
            return true;
        }
        return false;
    }
}
