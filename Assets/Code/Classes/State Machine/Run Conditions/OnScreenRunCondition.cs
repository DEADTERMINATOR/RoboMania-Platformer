﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnScreenRunCondition : StateMachineRunCondition
{
    private Bounds _objectBounds;


    public OnScreenRunCondition(Bounds objectBounds) => _objectBounds = objectBounds;

    public override bool CanRun()
    {
        Vector3 minBoundsScreen = Camera.main.WorldToViewportPoint(_objectBounds.min);
        Vector3 maxBoundsScreen = Camera.main.WorldToViewportPoint(_objectBounds.max);
        minBoundsScreen.z = 0;
        maxBoundsScreen.z = 0;
        Debug.Log("Min: " + minBoundsScreen + " Max: " + maxBoundsScreen);
        if ((minBoundsScreen.x <= 1.0f && minBoundsScreen.y <= 1.0f) || (maxBoundsScreen.x <= 1.0f && maxBoundsScreen.y <= 1.0f))
        {
            return true;
        }
        return false;
    }
}
