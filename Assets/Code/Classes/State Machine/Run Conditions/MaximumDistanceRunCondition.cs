﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaximumDistanceRunCondition : StateMachineRunCondition
{
    private float _maxDistance;
    private GameObject _stateMachineOwner;
    private GameObject _objectToCheckDistanceAgainst;


    public MaximumDistanceRunCondition(float maxDistance, GameObject stateMachineOwner, GameObject objectToCheckDistanceAgainst) => (_maxDistance, _stateMachineOwner, _objectToCheckDistanceAgainst) = (maxDistance, stateMachineOwner, objectToCheckDistanceAgainst);

    public override bool CanRun()
    {
        return Vector2.Distance(_stateMachineOwner.transform.position, _objectToCheckDistanceAgainst.transform.position) <= _maxDistance;
    }
}
