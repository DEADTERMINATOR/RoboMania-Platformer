﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrCondition : Condition
{
    /// <summary>
    /// Contains the conditions of which any can be met for this condition to pass.
    /// </summary>
    private Condition[] _conditions;


    /// <summary>
    /// Creates an instance of this condition which is a composite condition that can take a variable number of conditions and
    /// ensures that at least one of them pass before signaling that this condition has passed.
    /// </summary>
    /// <param name="conditions">The conditions of which any can be met in order for this condition to pass.</param>
    public OrCondition (params Condition[] conditions)
    {
        _conditions = conditions;
    }


    /// <summary>
    /// Tests each of the provided conditions.
    /// </summary>
    /// <returns>True if any of the conditions return true, false if they all return false.</returns>
    public override bool TestCondition()
    {
        foreach (Condition condition in _conditions)
        {
            bool success = condition.TestCondition();
            if (success)
            {
                return true;
            }
        }
        return false;
    }
}
