﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a condition in a state machine. A condition is a test or series of tests that can either pass or fail. 
/// If they all pass, the condition is met and the state transition is approved.
/// </summary>
public abstract class Condition
{
    /// <summary>
    /// Tests to see whether the condition has been met.
    /// </summary>
    /// <returns>True if the condition has been met, false otherwise.</returns>
    public abstract bool TestCondition();
}
