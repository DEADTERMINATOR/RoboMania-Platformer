﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndCondition : Condition
{
    /// <summary>
    /// Contains the conditions that need to be met in order for this condition to pass.
    /// </summary>
    private Condition[] _conditions;


    /// <summary>
    /// Creates an instance of this condition which is a composite condition that takes a varible number of conditions
    /// and ensures that they all pass before signaling the condition has passed.
    /// </summary>
    /// <param name="conditions">The conditions that need to be met in order for the and condition to pass.</param>
    public AndCondition(params Condition[] conditions)
    {
        _conditions = conditions;
    }


    /// <summary>
    /// Tests each of the provided conditions.
    /// </summary>
    /// <returns>True if all conditions return true, false if even one did not.</returns>
    public override bool TestCondition()
    {
        foreach (Condition condition in _conditions)
        {
            bool success = condition.TestCondition();
            if (!success)
            {
                return false;
            }
        }
        return true;
    }
}
