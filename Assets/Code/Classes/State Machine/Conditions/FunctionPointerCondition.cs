﻿using UnityEngine;
using System.Collections;
using System;

public class FunctionPointerCondition : Condition
{
    private Func<bool> _function;


    public FunctionPointerCondition(Func<bool> function)
    {
        _function = function;
    }

    public override bool TestCondition()
    {
        return _function();
    }
}
