﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotCondition : Condition
{
    /// <summary>
    /// The condition that will be tested.
    /// </summary>
    private Condition _condition;


    /// <summary>
    /// Instantiates an instance of this condition which returns the inverse of the condition test result.
    /// </summary>
    /// <param name="condition">The condition to test.</param>
    public NotCondition(Condition condition)
    {
        _condition = condition;
    }


    /// <summary>
    /// Tests the condition and returns the inverse.
    /// </summary>
    /// <returns>True if the condition fails, false if it passes.</returns>
    public override bool TestCondition()
    {
        return !_condition.TestCondition();
    }
}
