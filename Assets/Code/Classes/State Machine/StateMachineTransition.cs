﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a transition between state machines. To be used with a hierarchical state machine.
/// </summary>
public class StateMachineTransition
{
    /// <summary>
    /// The state machine this transition comes from.
    /// </summary>
    public StateMachine MachineToTransitionFrom { get; private set; }

    /// <summary>
    /// The state machine this transition should go to if the condition is met.
    /// </summary>
    public StateMachine MachineToTransitionTo { get; private set; }

    /// <summary>
    /// The condition that needs to be met in order for the transition to occur.
    /// </summary>
    public Condition TransitionCondition { get; private set; }

    /// <summary>
    /// Public getter for the action the transition should perform.
    /// </summary>
    public List<StateMachineAction> ActionsToPerform { get { return _actionsToPerform; } }

    /// <summary>
    /// Whether this transition has been disabled.
    /// </summary>
    public bool Disabled;


    /// <summary>
    /// The actions that the transition should perform before handing control over to the next state.
    /// </summary>
    protected List<StateMachineAction> _actionsToPerform;


    /// <summary>
    /// Instantiates an instance of a state machine transition.
    /// </summary>
    /// <param name="stateMachineToTransitionTo">The state machine this transition leads to.</param>
    /// <param name="transitionCondition">The condition that needs to be met for the transition to occur.</param>
    public StateMachineTransition(StateMachine machineToTransitionFrom, StateMachine machineToTransitionTo, Condition transitionCondition)
    {
        MachineToTransitionFrom = machineToTransitionFrom;
        MachineToTransitionTo = machineToTransitionTo;
        TransitionCondition = transitionCondition;

        _actionsToPerform = new List<StateMachineAction>();
    }


    /// <summary>
    /// Tests whether the condition required for this transition to trigger has been met.
    /// </summary>
    /// <returns>True if the condition is met, false if it is not.</returns>
    public bool IsTriggered()
    {
        bool transfer = TransitionCondition.TestCondition();
        if (transfer)
        {
            ////Debug.Log("Entering " + StateMachineToTransitionTo.ToString());
        }
        return transfer;
        //return _transitionCondition.TestCondition();
    }

    /// <summary>
    /// Adds an action to the list of actions the transition needs to perform before handing control over to the next state.
    /// </summary>
    /// <param name="actionToPerform">The action to perform during the transition.</param>
    public void AddActionToPerform(StateMachineAction actionToPerform)
    {
        _actionsToPerform.Add(actionToPerform);
    }
}
