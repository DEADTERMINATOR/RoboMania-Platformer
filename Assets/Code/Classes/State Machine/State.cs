﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CompletionState = StateMachineAction.ActionCompletionState;

/// <summary>
/// Represents a state for a state machine. A state defines the current behaviour that the state machine owner adhere to.
/// </summary>
public class State
{
    /// <summary>
    /// Whether this state should be allowed to transition to itself (thus restarting the state, including entry actions).
    /// </summary>
    public bool CanTransitionToSelf;

    /// <summary>
    /// The transition that was triggered last time the state machine was in this state.
    /// </summary>
    public Transition LastTriggeredTransition;

    /// <summary>
    /// Public getter for the entry action.
    /// </summary>
    public List<StateMachineAction> EntryActions { get { return _entryActions; } }

    /// <summary>
    /// Public getter for the state action.
    /// </summary>
    public List<StateMachineAction> StateActions { get { return _stateActions; } }

    /// <summary>
    /// Public getter for the exit action.
    /// </summary>
    public List<StateMachineAction> ExitActions { get { return _exitActions; } }

    /// <summary>
    /// Public getter for the possible transitions from this state.
    /// </summary>
    public List<Transition> Transitions { get { return _transitions; } }


    /// <summary>
    /// The name of the state as a string.
    /// </summary>
    protected string _name;

    /// <summary>
    /// The actions that should be performed when the state is first entered.
    /// </summary>
    protected List<StateMachineAction> _entryActions;

    /// <summary>
    /// The actions that should be performed every update while this is the current state.
    /// </summary>
    protected List<StateMachineAction> _stateActions;

    /// <summary>
    /// The actions that should be performed when the state is exited.
    /// </summary>
    protected List<StateMachineAction> _exitActions;

    /// <summary>
    /// The list of transitions that this state possesses.
    /// </summary>
    protected List<Transition> _transitions;


    /// <summary>
    /// State class constructor
    /// </summary>
    public State(string name, bool canTransitionToSelf = false)
    {
        _name = name;
        _entryActions = new List<StateMachineAction>();
        _stateActions = new List<StateMachineAction>();
        _exitActions = new List<StateMachineAction>();
        _transitions = new List<Transition>();
        CanTransitionToSelf = canTransitionToSelf;
    }

    #region Entry Actions
    /// <summary>
    /// Adds an entry action to the state's list of entry actions.
    /// </summary>
    /// <param name="entryAction">The entry action to add.</param>
    public void AddEntryAction(StateMachineAction entryAction)
    {
        _entryActions.Add(entryAction);
    }

    /// <summary>
    /// Shotcut version of AddEntryAction if you're adding a func that takes no parameters.
    /// </summary>
    /// <param name="entryAction">The entry action to add.</param>
    /// <param name="name">The name of the entry action.</param>
    /// <returns>The created StateMachineAction.</returns>
    public StateMachineAction AddEntryAction(Func<CompletionState> entryAction, string name = "")
    {
        var newAction = new FunctionPointerAction(entryAction, name);
        _entryActions.Add(newAction);
        return newAction;
    }

    /// <summary>
    /// Adds an entry action to the state's list of entry actions at the specified index.
    /// </summary>
    /// <param name="entryAction">The entry action to add.</param>
    /// <param name="index">The index to add the action at.</param>
    public void AddEntryActionAtIndex(StateMachineAction entryAction, int index)
    {
        _entryActions.Insert(index, entryAction);
    }

    /// <summary>
    /// Shortcut version of AddEntryActionAtIndex if you're adding a func that takes no parameters.
    /// </summary>
    /// <param name="entryAction">The entry action to add.</param>
    /// <param name="index">The index to add the action at.</param>
    /// <param name="name">The name of the entry action.</param>
    /// <returns>The created StateMachineAction.</returns>
    public StateMachineAction AddEntryActionAtIndex(Func<CompletionState> entryAction, int index, string name = "")
    {
        var newAction = new FunctionPointerAction(entryAction, name);
        _entryActions.Insert(index, newAction);
        return newAction;
    }

    /// <summary>
    /// Removes the state action if the action exists in the entry actions list.
    /// </summary>
    /// <param name="action">The action to remove</param>
    /// <returns>True if the action is found and removed, false otherwise.</returns>
    public bool RemoveEntryAction(StateMachineAction action)
    {
        return _entryActions.Remove(action);
    }

    /// <summary>
    /// Removes the entry action at the specified index of the state's list of entry actions.
    /// </summary>
    /// <param name="index">The index of the action to remove.</param>
    public void RemoveEntryActionAtIndex(int index)
    {
        if (index >= 0 && index < _entryActions.Count)
        {
            _entryActions.RemoveAt(index);
        }
    }

    /// <summary>
    /// Removes the entry action by the name of the action (if it can find it).
    /// </summary>
    /// <param name="name">The name of the action to remove.</param>
    /// <returns>True if the removal was successful. False otherwise.</returns>
    public bool RemoveEntryActionByName(string name)
    {
        var actionToRemove = _entryActions.Find(x => x.ActionName == name);
        if (actionToRemove != null)
        {
            _entryActions.Remove(actionToRemove);
            return true;
        }

        return false;
    }
    #endregion

    #region State Actions
    /// <summary>
    /// Adds a state action to the state's list of in-state actions.
    /// </summary>
    /// <param name="stateAction">The state action to add.</param>
    public void AddStateAction(StateMachineAction stateAction)
    {
        _stateActions.Add(stateAction);
    }

    /// <summary>
    /// Shortcut version of AddStateAction if you're adding a func that takes no parameters.
    /// </summary>
    /// <param name="stateAction">The state action to add.</param>
    /// <param name="name">The name of the state action.</param>
    /// <returns>The created StateMachineAction.</returns>
    public StateMachineAction AddStateAction(Func<CompletionState> stateAction, string name = "")
    {
        var newAction = new FunctionPointerAction(stateAction, name);
        _stateActions.Add(newAction);
        return newAction;
    }

    /// <summary>
    /// Adds a state action to the state's list of in-state actions at the specified index.
    /// </summary>
    /// <param name="stateAction">The state action to add.</param>
    /// <param name="index">The index to add the action at.</param>
    public void AddStateActionAtIndex(StateMachineAction stateAction, int index)
    {
        _stateActions.Insert(index, stateAction);
    }

    /// <summary>
    /// Shortcut version of AddStateActionAtIndex if you're adding a func that takes no parameters.
    /// </summary>
    /// <param name="stateAction">The state action to add.</param>
    /// <param name="index">The index to add the action at.</param>
    /// <param name="name">The name of the state action.</param>
    /// <returns>The created StateMachineAction.</returns>
    public StateMachineAction AddStateActionAtIndex(Func<CompletionState> stateAction, int index, string name = "")
    {
        var newAction = new FunctionPointerAction(stateAction, name);
        _stateActions.Insert(index, newAction);
        return newAction;
    }

    /// <summary>
    /// Removes the state action if the action exists in the state actions list.
    /// </summary>
    /// <param name="action">The action to remove</param>
    /// <returns>True if the action is found and removed, false otherwise.</returns>
    public bool RemoveStateAction(StateMachineAction action)
    {
        return _stateActions.Remove(action);
    }

    /// <summary>
    /// Removes the state action at the specified index of the state's list of in-state actions.
    /// </summary>
    /// <param name="index">The index of the action to remove.</param>
    public void RemoveStateActionAtIndex(int index)
    {
        if (index >= 0 && index < _stateActions.Count)
        {
            _stateActions.RemoveAt(index);
        }
    }

    /// <summary>
    /// Removes the state action by the name of the action (if it can find it).
    /// </summary>
    /// <param name="name">The name of the action to remove.</param>
    /// <returns>True if the removal was successful. False otherwise.</returns>
    public bool RemoveStateActionByName(string name)
    {
        var actionToRemove = _stateActions.Find(x => x.ActionName == name);
        if (actionToRemove != null)
        {
            _stateActions.Remove(actionToRemove);
            return true;
        }

        return false;
    }
    #endregion

    #region Exit Actions
    /// <summary>
    /// Adds an exit action to the state's list of exit actions.
    /// </summary>
    /// <param name="exitAction">The exit action to add.</param>
    public void AddExitAction(StateMachineAction exitAction)
    {
        _exitActions.Add(exitAction);
    }

    /// <summary>
    /// Shortcut version of AddExitAction if you're adding a func that takes no parameters.
    /// </summary>
    /// <param name="exitAction">The exit action to add.</param>
    /// <param name="name">The name of the exit action.</param>
    /// <returns>The created StateMachineAction.</returns>
    public StateMachineAction AddExitAction(Func<CompletionState> exitAction, string name = "")
    {
        var newAction = new FunctionPointerAction(exitAction, name);
        _exitActions.Add(newAction);
        return newAction;
    }

    /// <summary>
    /// Adds an exit action to the state's list of exit actions at the specified index.
    /// </summary>
    /// <param name="exitAction">The exit action to add.</param>
    /// <param name="index">The index to add the action at.</param>
    public void AddExitActionAtIndex(StateMachineAction exitAction, int index)
    {
        _exitActions.Insert(index, exitAction);
    }

    /// <summary>
    /// Shortcut version of AddExitActionAtIndex if you're adding a func that takes no parameters.
    /// </summary>
    /// <param name="exitAction">The exit action to add.</param>
    /// <param name="index">The index to add the action at.</param>
    /// <param name="name">The name of the exit action.</param>
    /// <returns>The created StateMachineAction.</returns>
    public StateMachineAction AddExitActionAtIndex(Func<CompletionState> exitAction, int index, string name = "")
    {
        var newAction = new FunctionPointerAction(exitAction, name);
        _exitActions.Insert(index, newAction);
        return newAction;
    }

    /// <summary>
    /// Removes the state action if the action exists in the exit actions list.
    /// </summary>
    /// <param name="action">The action to remove</param>
    /// <returns>True if the action is found and removed, false otherwise.</returns>
    public bool RemoveExitAction(StateMachineAction action)
    {
        return _exitActions.Remove(action);
    }

    /// <summary>
    /// Removes the exit action at the specified index of the state's list of exit actions.
    /// </summary>
    /// <param name="index">The index of the action to remove.</param>
    public void RemoveExitActionAtIndex(int index)
    {
        if (index >= 0 && index < _exitActions.Count)
        {
            _exitActions.RemoveAt(index);
        }
    }

    /// <summary>
    /// Removes the exit action by the name of the action (if it can find it).
    /// </summary>
    /// <param name="name">The name of the action to remove.</param>
    /// <returns>True if the removal was successful. False otherwise.</returns>
    public bool RemoveExitActionByName(string name)
    {
        var actionToRemove = _exitActions.Find(x => x.ActionName == name);
        if (actionToRemove != null)
        {
            _exitActions.Remove(actionToRemove);
            return true;
        }

        return false;
    }
    #endregion

    /// <summary>
    /// Add a transition to this state's list of possible transitions.
    /// </summary>
    /// <param name="transitionToAdd">The transitions to add.</param>
    public void AddTransition(Transition transitionToAdd)
    {
        _transitions.Add(transitionToAdd);
    }

    /// <summary>
    /// Finds an action by its name in the provided list of actions if it exists.
    /// </summary>
    /// <param name="actionList">A reference to the list to search.</param>
    /// <param name="name">The name of the action.</param>
    /// <returns>The action if it can be found. Null otherwise.</returns>
    public StateMachineAction FindActionByName(ref List<StateMachineAction> actionList, string name)
    {
        return actionList.Find(x => x.ActionName == name);
    }

    public static bool CompareStates(State stateA, State stateB)
    {
        return stateA.ToString() == stateB.ToString();
    }

    public bool CompareStates(State otherState)
    {
        return ToString() == otherState.ToString();
    }

    public bool CompareStates(string otherStateName)
    {
        return ToString() == otherStateName;
    }

    public override string ToString()
    {
        return _name;
    } 
}
