﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents an action in a state machine. An action is a behaviour that the state machine owner should perform either during a state, 
/// or when transferring from one state to another.
/// </summary>

public abstract class StateMachineAction
{
    /* Note: InProgress should be returned when the action is not yet complete, but other actions should continue to run.
     * ToBeContinued should be returned when the action is not yet complete, and other actions should be blocked from running until it is complete. */
    public enum ActionCompletionState { NotRun, EarlyTermination, GuaranteedSuccess, InProgress, ToBeContinued, Successful, Failed, RemoveOnSuccessful }

    public ActionCompletionState LastActionCompletionState = ActionCompletionState.NotRun;
    public string ActionName { get; protected set; }

    protected List<StateMachineRunCondition> runConditions = new List<StateMachineRunCondition>();

    public StateMachineAction(params StateMachineRunCondition[] runConditions)
    {
        this.runConditions = new List<StateMachineRunCondition>(runConditions);
    }

    /// <summary>
    /// Performs the action. Now obsolete. Please use the version that returns an ActionCompletionState.
    /// </summary>
    /// <returns>True if the action was deemed complete. False otherwise.</returns>
    [Obsolete("Please use the version that returns an ActionCompletionState. This remains for backwards compatibility only.")]
    public abstract bool PerformAction();

    /// <summary>
    /// Performs the action.
    /// </summary>
    /// <returns>An ActionCompletionState representing the progress the action made.</returns>
    public abstract ActionCompletionState DoAction();

    protected bool EvaluateRunConditions()
    {
        foreach (StateMachineRunCondition condition in runConditions)
        {
            bool success = condition.CanRun();
            if (!success)
            {
                return false;
            }
        }
        return true;
    }
}