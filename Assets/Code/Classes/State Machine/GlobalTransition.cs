﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Described transitions that should be evaluated regardless of the current state.
/// </summary>
public class GlobalTransition : Transition
{
    /// <summary>
    /// A list of states that should not evaluate this condition. If this list is empty, all states will evaluate this transition.
    /// </summary>
    public List<State> ExcludedStates { get; protected set; }

    /// <summary>
    /// Constuctor for a global transition.
    /// </summary>
    /// <param name="stateToTransitionTo">The state this transition leads to.</param>
    /// <param name="transitionCondition">The condition that must be met for this transition to occur.</param>
    /// <param name="excludedStates">The list of states that should not evalute this transition.</param>
    public GlobalTransition(State stateToTransitionTo, Condition transitionCondition, params State[] excludedStates) : base(stateToTransitionTo, transitionCondition)
    {
        ExcludedStates = new List<State>();
        foreach (State state in excludedStates)
            ExcludedStates.Add(state);
    }

    public void AddExcludedState(State excludedState)
    {
        if (!ExcludedStates.Contains(excludedState))
            ExcludedStates.Add(excludedState);
    }
}
