﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a transition in a state machine. A transition transfers control from one state to another.
/// </summary>
public class Transition
{
    /// <summary>
    /// The state this transition goes to.
    /// </summary>
    public State StateToTransitionTo { get; private set; }

    /// <summary>
    /// The condition that needs to be met in order for the transition to occur.
    /// </summary>
    public Condition TransitionCondition { get; private set; }

    /// <summary>
    /// Public getter for the action the transition should perform.
    /// </summary>
    public List<StateMachineAction> ActionsToPerform { get { return _actionsToPerform; } }

    /// <summary>
    /// Whether this transition has been disabled.
    /// </summary>
    public bool Disabled;

    /// <summary>
    /// The actions that the transition should perform before handing control over to the next state.
    /// </summary>
    protected List<StateMachineAction> _actionsToPerform;

    /// <summary>
    /// Constructor for a transition.
    /// </summary>
    /// <param name="stateToTransitionTo">The state this transition leads to.</param>
    /// <param name="transitionCondition">The condition that must be met for this transition to occur.</param>
    /// <param name="actionsToPerform">This is a list of action to be preformed on the transition use (new StateMachineAction[]{ Actions,Actions } )</param>
    public Transition(State stateToTransitionTo, Condition transitionCondition , StateMachineAction[] actionsToPerform = null)
    {
        StateToTransitionTo = stateToTransitionTo;
        TransitionCondition = transitionCondition;
        if(actionsToPerform != null)
        {
            _actionsToPerform = new List<StateMachineAction>(actionsToPerform);
        }
        else
        {
            _actionsToPerform = new List<StateMachineAction>();
        }
    }


    /// <summary>
    /// Tests whether the condition required for this transition to trigger has been met.
    /// </summary>
    /// <returns>True if the condition is met, false if it is not.</returns>
    public bool IsTriggered()
    {
        return TransitionCondition.TestCondition();
    }

    /// <summary>
    /// Adds an action to the list of actions the transition needs to perform before handing control over to the next state.
    /// </summary>
    /// <param name="actionToPerform">The action to perform during the transition.</param>
    public void AddActionToPerform(StateMachineAction actionToPerform)
    {
        _actionsToPerform.Add(actionToPerform);
    }

    public override string ToString()
    {
        return "Entering " + StateToTransitionTo.ToString();
    }
}
