﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerConstants
{
    /// <summary>
    /// How long does it take the player to turn around when they are in the air.
    /// </summary>
    public const float ACCELERATION_TIME_AIRBORNE = 0f; //0.05f

    /// <summary>
    /// How long does it take the player to turn around when they are on the ground.
    /// </summary>
    public const float ACCELERATION_TIME_GROUNDED = 0.05f;

    /// <summary>
    /// The amount of time the full effect of gravity pauses when an air dash is used.
    /// </summary>
    public const float AIR_DASH_PAUSE_TIME = 0.5f;

    /// <summary>
    /// The amount of time the player can be in the air and still jump. Intended to subtly give the player a little bit of leeway
    /// (that they hopefully don't notice) and not be so strict on jump timing.
    /// </summary>
    public const float AIR_TIME_JUMP_LEEWAY = 0.15f;

    /// <summary>
    /// The amount of time the player will be considered as "bouncing" when they jump on a platform that bounces them. This might not encompass
    /// the entire time that they are effected by the velocity.
    /// </summary>
    public const float BOUNCE_TIME = 0.2f;

    /// <summary>
    /// The height multiplier that will be applied to the player if they jump right around colliding with the bouncy platform (if the bouncy platform supports this).
    /// </summary>
    public const float BOUNCE_MULTIPLIER = 1.5f;

    /// <summary>
    /// The length of the raycast that is fired to check if the player is within wall sliding distance.
    /// </summary>
    public const float CHECK_FOR_WALL_SLIDE_DISTANCE = 0.25f;

    /// <summary>
    /// The multiplier applied to the player's input when climbing to determine their climbing speed.
    /// </summary>
    public const float CLIMBING_SPEED_MULTIPLIER = 5;

    /// <summary>
    /// How long after taking damage will the player be visibly immune to further damage (or damage knockback)
    /// </summary>
    public const float DAMAGE_INVULNERABILITY_FLASH_TIME = 1.5f;

    /// <summary>
    /// The total amount of time after taking damage will the player be immune to further damage (or damage knockback).
    /// Includes a brief grace period after DAMAGE_INVULNERABILITY_FLASH_TIME has elapsed to give the player a warning that they need to get out of the way of any danger.
    /// </summary>
    public const float DAMAGE_INVULNERABILITY_TIME = 2f;

    /// <summary>
    /// The velocity the gets applied in the X-direction when the player dashes.
    /// </summary>
    public const float DASH_VELOCITY = 1200f;

    /// <summary>
    /// The percentage of the player's dash that should be resolved before gravity and player input is restored.
    /// </summary>
    public const float DASH_RESOLVED_PERCENTAGE_TO_RESTORE_GRAVITY_AND_INPUT = 0.75f;

    /// <summary>
    /// The amount of time the player has to wait between performing dashes.
    /// </summary>
    public const float DASH_TIMEOUT = 0.5f;

    /// <summary>
    /// The amount of time the velocity of a dash should ease out over when the player is providing no movement input.
    /// </summary>
    public const float DESIRED_TIME_TO_EASE_OUT_DASH_NO_INPUT = 0.4f;

    /// <summary>
    /// The amount of time the velocity of a dash should ease out over when the player is providing movement input.
    /// </summary>
    public const float DESIRED_TIME_TO_EASE_OUT_DASH_WITH_INPUT = 0.35f;

    /// <summary>
    /// The amount of time a dash should take to fully resolve.
    /// </summary>
    public const float DESIRED_TIME_TO_RESOLVE_DASH = 0.4f;

    /// <summary>
    /// The amount of time the full effect of gravity pauses when a double jump is used.
    /// </summary>
    public const float DOUBLE_JUMP_PAUSE_TIME = 0.2f;

    /// <summary>
    /// The divisor for the gravity reduction if an energy cell move has been used recently.
    /// </summary>
    public const float ENERGY_CELL_MOVE_GRAVITY_DIVISOR = 3;

    /// <summary>
    /// The amount of time that has to pass since last using an energy cell move before they recharge.
    /// </summary>
    public const float ENERGY_CELL_RECHARGE_TIME_DELAY = 0.33f;

    /// <summary>
    /// The upward velocity to be applied (over a second) when the player is raising while hovering.
    /// </summary>
    public const float HOVER_RAISE_AMOUNT_PER_SECOND = 200f;

    /// <summary>
    /// The multiplier applied to the energy drain of a hover when the player is raising while hovering.
    /// </summary>
    public const float HOVER_RAISE_ENERGY_DRAIN_MULTIPLIER = 3f;

    /// <summary>
    /// The amount of time the player's input will be locked for after performing a jump.
    /// </summary>
    public const float JUMP_INPUT_LOCK_TIME = 0.05f;

    /// <summary>
    /// The maximum amount of time the player can provide movement input in the opposite direction of the wall they're sliding down before the wall slide is ended.
    /// </summary>
    public const float MAXIMUM_WALL_PUSHOFF_TIME = 0.15f;

    /// <summary>
    /// The maximum Y-velocity speed the player can achieve when wall sliding.
    /// </summary>
    public const float MAXIMUM_WALL_SLIDE_SPEED = 2.5f;

    /// <summary>
    /// The maximum amount of time the player will stay stuck to the wall if they have reached a gap in the wall (or the edge of the wall).
    /// </summary>
    public const float MAXIMUM_WALL_STICK_TIME = 0.25f;

    /// <summary>
    /// The maximum negative Y-velocity that the player is allowed to achieve.
    /// </summary>
    public const float MAXIMUM_Y_VELOCITY = -98.0f;

    /// <summary>
    /// The minimum amount of time the player must be on the ground in between jumps.
    /// </summary>
    public const float MINIMUM_GROUND_TIME_BETWEEN_JUMP = 0.1f;

    /// <summary>
    /// The minimum horizontal collision percentage required to stop regular knockback.
    /// </summary>
    public const float MINIMUM_HORIZONTAL_COLLISION_PERCENTAGE_FOR_STOPPING_KNOCKBACK = 0.25f;

    /// <summary>
    /// The minimum horizontal collision percentage required for wall sliding to trigger.
    /// </summary>
    public const float MINIMUM_HORIZONTAL_COLLISION_PERCENTAGE_FOR_WALL_SLIDE = 0.8f;

    /// <summary>
    /// The amount of time the player's input will be locked for after performing a multi jump.
    /// </summary>
    public const float MULTI_JUMP_INPUT_LOCK_TIME = 0.25f;

    /// <summary>
    /// The amount of energy a multi jump uses.
    /// </summary>
    public const float MULTI_JUMP_ENERGY_USAGE = 80f;

    /// <summary>
    /// The gravity scale applied when the player performs a multi jump.
    /// </summary>
    public const float MULTI_JUMP_GRAVITY_SCALE = 1;

    /// <summary>
    /// The amount the player's base jump velocity is divided by when they perform a multi jump. 
    /// </summary>
    public const float MULTI_JUMP_POWER_REDUCTION = 1.1f;

    /// <summary>
    /// The maximum percentage of non-damage knockback that can be remaining before the input lock is removed.
    /// </summary>
    public const float PERCENTAGE_OF_KNOCKBACK_REMAINING_BEFORE_INPUT_LOCK_REMOVAL = 0.6f;

    /// <summary>
    /// The amount of time the player's special input will be locked for after performing a wall climb jump.
    /// </summary>
    public const float WALL_CLIMB_SPECIAL_INPUT_LOCK_TIME = 0.5f;

    /// <summary>
    /// The velocity vector for a wall jump (used when jumping off of a wall).
    /// </summary>
    public static readonly Vector2 WALL_JUMP = new Vector2(12.5f, 27.5f);//new Vector2(25f, 20f);

    /// <summary>
    /// The amount of time the player will be considered wall jumping off a wall after they perform the action.
    /// </summary>
    public const float WALL_JUMP_OFF_WALL_TIME = 0.25f;

    /// <summary>
    /// The amount of energy the player will recharge per second of wall sliding.
    /// </summary>
    public const float WALL_SLIDING_ENERGY_CELL_RECHARGE_RATE = 10f;

    /// <summary>
    /// The multipler by which the caluclated X-damage knockback for the frame is multiplied and subtracted from the total knockback
    /// to cause the knockback to resolve more quickly. Allows knockback values to be set high (for the feeling of an intense hit)
    /// while not flinging the player extreme distances.
    /// </summary>
    public const float X_DAMAGE_KNOCKBACK_DISSIPATION_MULTIPLIER = 16;

    /// <summary>
    /// The multipler by which the caluclated Y-damage knockback for the frame is multiplied and subtracted from the total knockback
    /// to cause the knockback to resolve more quickly. Allows knockback values to be set high (for the feeling of an intense hit)
    /// while not flinging the player extreme distances.
    /// </summary>
    public const float Y_DAMAGE_KNOCKBACK_DISSIPATION_MULTIPLIER = 16;

}
