﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;

public class BulletProjectileType : ProjectileType
{
    /// <summary>
    /// An enum of the allowed fire types (straight shot etc) when this projectile is selected for a common chip.
    /// </summary>
    public new List<string> AllowedFireTypesCommon = new List<string> { "RANDOMSHOT" };

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for an uncommon chip
    /// </summary>
    public new List<string> AllowedFireTypesUncommon = new List<string> { "RANDOMSHOT" };

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for a rare chip.
    /// </summary>
    public new List<string> AllowedFireTypesRare = new List<string> { "RANDOMSHOT" };

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for an epic chip.
    /// </summary>
    public new List<string> AllowedFireTypesEpic = new List<string> { "STRAIGHTSHOT", "RANDOMSHOT" };


    /// <summary>
    /// Public getter for the single instance of the projectile type's data.
    /// </summary>
    public static BulletProjectileType Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new BulletProjectileType();
            }
            return _instance;
        }
    }


    /// <summary>
    /// The single instance of the projectile type data class.
    /// </summary>
    protected static BulletProjectileType _instance;


    /// <summary>
    /// Private constructor that sets the limits for this projectile type.
    /// </summary>
    private BulletProjectileType()
    {
        MinDamage = 2;
        MaxDamage = 6;

        MinFireRate = 0.1f;
        MaxFireRate = 0.05f;

        AmmoProvided = 200;
        ammoType = PlayerData.Ammo.AmmoTypes.Bullet;
        projectileType = ProjectileTypes.Bullet;
    }

    /// <summary>
    /// Returns the appropriate list of allowed fire types based on the provided rarity.
    /// </summary>
    /// <param name="rarity">The rarity of the gun chip.</param>
    /// <returns>The list with the allowed fire types.</returns>
    public override List<string> GetAllowedFireTypes(int rarity)
    {
        if (rarity == 0)
        {
            return AllowedFireTypesCommon;
        }
        else if (rarity == 1)
        {
            return AllowedFireTypesUncommon;
        }
        else if (rarity == 2)
        {
            return AllowedFireTypesRare;
        }
        else if (rarity == 3)
        {
            return AllowedFireTypesEpic;
        }
        return null;
    }
}
