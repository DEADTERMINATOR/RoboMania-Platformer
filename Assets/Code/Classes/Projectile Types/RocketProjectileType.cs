﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;

public class RocketProjectileType : ProjectileType
{
    /// <summary>
    /// An enum of the allowed fire types (straight shot etc) when this projectile is selected for a common chip.
    /// </summary>
    public new List<string> AllowedFireTypesCommon = new List<string> { "STRAIGHTSHOT" };

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for an uncommon chip
    /// </summary>
    public new List<string> AllowedFireTypesUncommon = new List<string> { "STRAIGHTSHOT" };

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for a rare chip.
    /// </summary>
    public new List<string> AllowedFireTypesRare = new List<string> { "STRAIGHTSHOT" };

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for an epic chip.
    /// </summary>
    public new List<string> AllowedFireTypesEpic = new List<string> { "STRAIGHTSHOT", "SPREADSHOT" };


    /// <summary>
    /// Public getter for the single instance of the projectile type's data.
    /// </summary>
    public static RocketProjectileType Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new RocketProjectileType();
            }
            return _instance;
        }
    }


    /// <summary>
    /// The single instance of the projectile type data class.
    /// </summary>
    protected static RocketProjectileType _instance;


    /// <summary>
    /// Private constructor that sets the limits for this projectile type.
    /// </summary>
    private RocketProjectileType()
    {
        MinDamage = 30;
        MaxDamage = 75;

        MinFireRate = 1.5f;
        MaxFireRate = 0.75f;

        AmmoProvided = 10;

        ammoType = PlayerData.Ammo.AmmoTypes.Bullet;
        projectileType = ProjectileTypes.Rocket;
    }

    /// <summary>
    /// Returns the appropriate list of allowed fire types based on the provided rarity.
    /// </summary>
    /// <param name="rarity">The rarity of the gun chip.</param>
    /// <returns>The list with the allowed fire types.</returns>
    public override List<string> GetAllowedFireTypes(int rarity)
    {
        if (rarity == 0)
        {
            return AllowedFireTypesCommon;
        }
        else if (rarity == 1)
        {
            return AllowedFireTypesUncommon;
        }
        else if (rarity == 2)
        {
            return AllowedFireTypesRare;
        }
        else if (rarity == 3)
        {
            return AllowedFireTypesEpic;
        }
        return null;
    }
}
