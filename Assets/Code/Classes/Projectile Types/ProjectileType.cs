﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;

public enum ProjectileTypes { Energy, Electric, Rocket, Bullet, Penetration }
public abstract class ProjectileType
{
    /// <summary>
    /// An enum of the allowed fire types (straight shot etc) when this projectile is selected for a common chip.
    /// </summary>
    public List<string> AllowedFireTypesCommon = new List<string>();

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for an uncommon chip
    /// </summary>
    public List<string> AllowedFireTypesUncommon = new List<string>();

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for a rare chip.
    /// </summary>
    public List<string> AllowedFireTypesRare = new List<string>();

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for an epic chip.
    /// </summary>
    public List<string> AllowedFireTypesEpic = new List<string>();


    /// <summary>
    /// The minimum amount of damage a projectile of this type can do.
    /// </summary>
    public float MinDamage;

    /// <summary>
    /// The maximum (barring a boost from an epic gun chip) amount of damage a projectile of this type can do.
    /// </summary>
    public float MaxDamage;

    /// <summary>
    /// The minimum fire rate (i.e. pause between projectiles, barring a boost from an epic gun chip) this type of projectile can have.
    /// </summary>
    public float MinFireRate;

    /// <summary>
    /// The maximum fire rate this type of projectile can have.
    /// </summary>
    public float MaxFireRate;

    /// <summary>
    /// How much ammo does picking up a chip of this projectile type provide.
    /// </summary>
    public int AmmoProvided;

    /// <summary>
    /// The type of ammo used by the projectile (Bullet is the default)
    /// </summary>
    public PlayerData.Ammo.AmmoTypes ammoType = PlayerData.Ammo.AmmoTypes.Bullet;

    /// <summary>
    /// The type of prjectile used by saveing and loading system
    /// </summary>
    public ProjectileTypes projectileType;

    /// <summary>
    /// Returns the appropriate list of allowed fire types based on the provided rarity.
    /// </summary>
    /// <param name="rarity">The rarity of the gun chip.</param>
    /// <returns>The list with the allowed fire types.</returns>
    public virtual List<string> GetAllowedFireTypes(int rarity)
    {
        if (rarity == 0)
        {
            return AllowedFireTypesCommon;
        }
        else if (rarity == 1)
        {
            return AllowedFireTypesUncommon;
        }
        else if (rarity == 2)
        {
            return AllowedFireTypesRare;
        }
        else if (rarity == 3)
        {
            return AllowedFireTypesEpic;
        }
        return null;
    }
}
