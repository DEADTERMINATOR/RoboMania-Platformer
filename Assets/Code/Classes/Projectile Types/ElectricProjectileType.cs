﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;

public class ElectricProjectileType : ProjectileType
{
    /// <summary>
    /// An enum of the allowed fire types (straight shot etc) when this projectile is selected for a common chip.
    /// </summary>
    public new List<string> AllowedFireTypesCommon = new List<string> { "STRAIGHTSHOT" };

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for an uncommon chip
    /// </summary>
    public new List<string> AllowedFireTypesUncommon = new List<string> { "STRAIGHTSHOT" };

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for a rare chip.
    /// </summary>
    public new List<string> AllowedFireTypesRare = new List<string> { "STRAIGHTSHOT", "SPREADSHOT" };

    /// <summary>
    /// An enum of the allowed fire types when this projectile is selected for an epic chip.
    /// </summary>
    public new List<string> AllowedFireTypesEpic = new List<string> { "STRAIGHTSHOT", "SPREADSHOT" };

    /// <summary>
    /// The minimum amount of stun power that can be applied by an electric shock.
    /// </summary>
    public float MinStunPower;

    /// <summary>
    /// The maximum amount of stun power that can be applied by an electric shock.
    /// </summary>
    public float MaxStunPower;

    /// <summary>
    /// The minimum amount of time a stunned enemy can remained stunned for.
    /// </summary>
    public float MinStunTime;

    /// <summary>
    /// The maximum amount of time a stunned enemy can remained stunned for.
    /// </summary>
    public float MaxStunTime;


    /// <summary>
    /// Public getter for the single instance of the projectile type's data.
    /// </summary>
    public static ElectricProjectileType Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ElectricProjectileType();
            }
            return _instance;
        }
    }


    /// <summary>
    /// The single instance of the projectile type data class.
    /// </summary>
    protected static ElectricProjectileType _instance;


    /// <summary>
    /// Private constructor that sets the limits for this projectile type.
    /// </summary>
    private ElectricProjectileType()
    {
        MinDamage = 0;
        MaxDamage = 0;

        MinFireRate = 0.5f;
        MaxFireRate = 0.25f;

        AmmoProvided = 50;

        MinStunPower = 40;
        MaxStunPower = 150;

        MinStunTime = 2;
        MaxStunTime = 5;

        ammoType = PlayerData.Ammo.AmmoTypes.Bullet;
        projectileType = ProjectileTypes.Electric;
    }

    /// <summary>
    /// Returns the appropriate list of allowed fire types based on the provided rarity.
    /// </summary>
    /// <param name="rarity">The rarity of the gun chip.</param>
    /// <returns>The list with the allowed fire types.</returns>
    public override List<string> GetAllowedFireTypes(int rarity)
    {
        if (rarity == 0)
        {
            return AllowedFireTypesCommon;
        }
        else if (rarity == 1)
        {
            return AllowedFireTypesUncommon;
        }
        else if (rarity == 2)
        {
            return AllowedFireTypesRare;
        }
        else if (rarity == 3)
        {
            return AllowedFireTypesEpic;
        }
        return null;
    }
}
