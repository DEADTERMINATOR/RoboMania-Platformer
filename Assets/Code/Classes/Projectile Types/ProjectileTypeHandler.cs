﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// A singleton-esque class that returns an instance of the data class for the desired projectile type to be used
/// as a guide when generating weapon chips.
/// </summary>
public class ProjectileTypeHandler
{
    /// <summary>
    /// Public getter for the handler instance.
    /// </summary>
    public static ProjectileTypeHandler Handler
    {
        get
        {
            if (_handler == null)
            {
                _handler = new ProjectileTypeHandler();
            }
            return _handler;
        }
    }


    /// <summary>
    /// Reference to the data class for the blue bullet projectile type.
    /// </summary>
    private EnergyProjectileType _energy;

    /// <summary>
    /// Reference to the data class for the electric shock projectile type.
    /// </summary>
    private ElectricProjectileType _electric;

    /// <summary>
    /// Reference to the data class for the rocket projectile type.
    /// </summary>
    private RocketProjectileType _rocket;

    /// <summary>
    /// Reference to the data class for the bullet projectile type.
    /// </summary>
    private BulletProjectileType _bullet;

    /// <summary>
    /// The single instance of this projectile handler.
    /// </summary>
    private static ProjectileTypeHandler _handler;


    /// <summary>
    /// Private constructor for the handler.
    /// </summary>
    private ProjectileTypeHandler()
    {
        _energy = EnergyProjectileType.Instance;
        _electric = ElectricProjectileType.Instance;
        _rocket = RocketProjectileType.Instance;
        _bullet = BulletProjectileType.Instance;
    }


    /// <summary>
    /// Returns the projectile type data for the specified projectile type.
    /// </summary>
    /// <param name="projectileType">The specified projectile type whose data is desired.</param>
    /// <returns>The projectile type's data.</returns>
    public ProjectileType GetProjectileTypeData(string projectileType)
    {
        switch (projectileType)
        {
            case "Energy":
                return _energy;
            case "Electric":
                return _electric;
            case "Rocket":
                return _rocket;
            case "Bullet":
                return _bullet;
            default:
                return null;
        }
    }

    /// <summary>
    /// Returns the projectile type data for the specified projectile type.
    /// </summary>
    /// <param name="projectileType">The specified projectile type whose data is desired.</param>
    /// <returns>The projectile type's data.</returns>
    public ProjectileType GetProjectileTypeData(ProjectileTypes projectileType)
    {
        switch (projectileType)
        {
            case ProjectileTypes.Energy:
                return _energy;
            case ProjectileTypes.Electric:
                return _electric;
            case ProjectileTypes.Rocket:
                return _rocket;
            case ProjectileTypes.Bullet:
                return _bullet;
            default:
                return null;
        }
    }
}
