﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class DistributedRandomTest : MonoBehaviour
{
    public int Min;
    public int Max;
    public int NumberOfRolls;
    public int NumberOfTests;
    public int[] NumberBuket;
    public Text OutPut;
    public Text LowLable;
    public Text hightLable;
    public Text MidLable;
    public LineRenderer line;
    public Button Test;

    public void OutputNumberBuket()
    {
        OutPut.text = "";
        Vector3[] points = new Vector3[Max + 1];
        line.positionCount = Max + 1;
        for (int i = 0; i < NumberBuket.Length; i++)
        {
            points[i] = new Vector3(i*5, NumberBuket[i], 0);
            OutPut.text += string.Format("{0:6} ", "[" + i + "] =>" + NumberBuket[i] + "     ");
            if(i % 5 ==0 )
            {
                OutPut.text += "\n"; 
            }
        }
        line.SetPositions(points);
    }

    public void TestDiceRoll()
    {
        NumberBuket = new int[(int)((Max+1))];
        for (int i = 0; i < NumberBuket.Length; i++)
        {
            NumberBuket[i] = 0;
        }
        for (int i = 0; i < NumberOfTests; i++)
        {
            int Number = (DistributedRandom.NextRandomDiceRoll(Min, Max,NumberOfRolls));
            try
            {
                NumberBuket[Number]++;
            }
            catch (System.IndexOutOfRangeException)
            {
                //Debug.Log("Index out "+Number);
            }
        }
        OutputNumberBuket();
    }


    public void TestGaussian()
    {
       /* NumberBuket = new int[Max - Min];
        for (int i = 0; i < NumberBuket.Length; i++)
        {
            NumberBuket[i] = 0;
        }
        for (int i = 0; i < NumberOfTests; i++)
        {
            int Number = (int)DistributedRandom.NextGaussian(Max / 2, 0.5f, Min, Max);

        }*/
    }

    // Use this for initialization
    void Start()
    {
        Test.onClick.AddListener(TestDiceRoll);
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
