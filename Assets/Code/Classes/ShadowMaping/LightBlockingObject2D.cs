﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// An object that has the ability to block light in 2D space
/// </summary>
public class LightBlockingObject2D : MonoBehaviour
{

    public enum EDGEMODE : int { BOXCOLLIDER, POLYGONCOLLIDER, MESH }

    public EDGEMODE edgeMode = EDGEMODE.BOXCOLLIDER;

    public Collider2D BlockingCollider;

    public Mesh mesh;

    private void Start()
    {
      mesh =   GetComponent<MeshFilter>().mesh;
    }

    // Get the outline of the object for shadow map rendering
    public void GetEdges(ref List<Vector2> edges)
    {
        switch(edgeMode)
        {
            case EDGEMODE.BOXCOLLIDER:
                GetEdgesFromBox(ref edges);

                return;
            case EDGEMODE.POLYGONCOLLIDER:
                GetEdgesFromPolygon(ref edges);
                return;
            case EDGEMODE.MESH:
                GetEdgesFromMesh(ref edges);
                return;
        }
    }

    private void GetEdgesFromPolygon(ref List<Vector2> edges)
    {
        //Find the Collider to get the data from 
        PolygonCollider2D polygonCollider2D = null;
        if (BlockingCollider)
        {
            polygonCollider2D = BlockingCollider as PolygonCollider2D; /// was pased so cast
        }
        else
        {
            polygonCollider2D = GetComponent<PolygonCollider2D>(); // is attached so look 
        }
        if (polygonCollider2D == null)// if one was not found error out 
        {
            throw new System.Exception("Can not build Edge list from Polygon ");
        }
        // look at all pahts
        for (int i = 0; i < polygonCollider2D.pathCount; i++)
        {
            Vector2[] points =  polygonCollider2D.GetPath(i);
            for (int ii = 1 ;  ii < points.Length; ii++)
            {
                edges.Add(points[ii - 1]);
                edges.Add(points[ii]);
            }
        }


    }
    private void GetEdgesFromMesh(ref List<Vector2> edges)
    {
      
        for (int i = 1; i < mesh.vertexCount; i++)
        {
           // edges.Add(mesh.vertices[i-1]);
            edges.Add(mesh.vertices[i]);          
        }

    }
        private void GetEdgesFromBox(ref List<Vector2> edges)
    {
        //Find the Collider to get the data from 
        BoxCollider2D boxCollider2D = null;
        if (BlockingCollider)
        {
            boxCollider2D = BlockingCollider as BoxCollider2D; /// was pased so cast
        }
        else
        {
            boxCollider2D = GetComponent<BoxCollider2D>(); // is attached so look 
        }
        if (boxCollider2D == null)// if one was not found error out 
        {
            return;
           // throw new System.Exception("Can not build Edge list from box ");
        }

        edges.Add(boxCollider2D.bounds.min);
        edges.Add(new Vector2(boxCollider2D.bounds.max.x, boxCollider2D.bounds.min.y));

        edges.Add(new Vector2(boxCollider2D.bounds.max.x, boxCollider2D.bounds.min.y));
        edges.Add(boxCollider2D.bounds.max);

        edges.Add(boxCollider2D.bounds.max);
        edges.Add(new Vector2(boxCollider2D.bounds.min.x, boxCollider2D.bounds.max.y));

        edges.Add(new Vector2(boxCollider2D.bounds.min.x, boxCollider2D.bounds.max.y));
        edges.Add(boxCollider2D.bounds.min);
                               
    }
}
