﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering;

namespace RoboMania.Graphics.Lighting
{
    public class BasicLight2D : ShadowCaster2D
    {

        public Color mColour;
        private float mAngle = 0;
        private float mSpread = 360;
        public float mFalloffExponent = 1.0f;
        public float mAngleFalloffExponent = 1.0f;
        public float mFullBrightRadius = 0.0f;
        private float mRadius = 0.0f;
        private CommandBuffer commandBuffer;

        public float StartAngle;
        public float StartSpreed;

        public float Angle
        {
            get
            {
                return mAngle;
            }
            set
            {
                if (mAngle != value)
                {
                    mAngle = value;

                    transform.localRotation = Quaternion.Euler(0.0f, 0.0f, mAngle);
                }
            }
        }
        public float Spread
        {
            get
            {
                return mSpread;
            }
            set
            {
                if (mSpread != value)
                {
                    mSpread = value;
                    RebuildQuad();
                }
            }
        }

        public void Start()
        {

            GetComponent<MeshFilter>().mesh = MakeAQuad(2, 2);


            Angle = StartAngle;
            Spread = StartSpreed;
            mRadius = Mathf.Max(transform.localScale.x, transform.localScale.y) * 2;

            transform.localScale = Vector3.one;

            RebuildQuad();
        }
        private void Update()
        {
            Angle = StartAngle;
            Spread = StartSpreed;
            SetUpMaterial();

        }

        /// <summary>
        /// Build the light's quad mesh. This aims to fit the light cone as best as possible.
        /// 
        /// </summary>
        public void RebuildQuad()
        {
            Mesh m = GetComponent<MeshFilter>().mesh;
            m.MarkDynamic();
            //Mesh m = new Mesh();
            Vector3[] verts = new Vector3[6];

            if (mSpread > 180.0f)
            {
                GetComponent<MeshFilter>().mesh = MakeAQuad(+mRadius, +mRadius);
            }

            else
            {
                float radius = mRadius;

                float minAngle = -mSpread * 0.5f;
                float maxAngle = +mSpread * 0.5f;

                Bounds aabb = new Bounds(Vector3.zero, Vector3.zero);
                aabb.Encapsulate(new Vector3(radius, 0.0f));
                aabb.Encapsulate(new Vector3(Mathf.Cos(Mathf.Deg2Rad * minAngle), Mathf.Sin(Mathf.Deg2Rad * minAngle)) * radius);
                aabb.Encapsulate(new Vector3(Mathf.Cos(Mathf.Deg2Rad * maxAngle), Mathf.Sin(Mathf.Deg2Rad * maxAngle)) * radius);


                GetComponent<MeshFilter>().mesh = MakeAQuad(aabb.max.x, aabb.max.y);
            }

        }

        /// <summary>
        /// This function sets up the parameters needed to DRAW the shadow map, plus the parameters to USE the shadow map when this object is rendered.
        /// Returns null if wanting to skip shadow rendering.
        /// </summary>
        /// <param name="shadowMapTexture"></param>
        /// <returns></returns>
        public override MaterialPropertyBlock BindShadowMap(Texture shadowMapTexture)
        {
            Vector4 shadowMapParams = GetShadowMapParams(mShadowMapSlot);

            mMaterialPropertyBlock.SetVector("_LightPosition", new Vector4(transform.position.x, transform.position.y, mAngle * Mathf.Deg2Rad, mSpread * Mathf.Deg2Rad * 0.5f));
            mMaterialPropertyBlock.SetVector("_ShadowMapParams", shadowMapParams);

            MeshRenderer mr = GetComponent<MeshRenderer>();

            Material mat = mr.materials[0];

            float radius = mRadius;

            mat.SetVector("_Color", mColour);
            mat.SetVector("_LightPosition", new Vector4(transform.position.x, transform.position.y, mFalloffExponent, mAngleFalloffExponent));
            mat.SetVector("_Params2", new Vector4(mAngle * Mathf.Deg2Rad, mSpread * Mathf.Deg2Rad * 0.5f, 1.0f / ((1.0f - mFullBrightRadius) * radius), mFullBrightRadius * radius));
            mat.SetVector("_ShadowMapParams", shadowMapParams);
            //mat.SetTexture("_ShadowTex", shadowMapTexture);

            return mMaterialPropertyBlock;
        }

        /// <summary>
        /// This function sets up the parameters needed to DRAW the shadow map, plus the parameters to USE the shadow map when this object is rendered.
        /// 
        public void SetUpMaterial()
        {
            Vector4 shadowMapParams = GetShadowMapParams(mShadowMapSlot);

            mMaterialPropertyBlock.SetVector("_LightPosition", new Vector4(transform.position.x, transform.position.y, mAngle * Mathf.Deg2Rad, mSpread * Mathf.Deg2Rad * 0.5f));
            mMaterialPropertyBlock.SetVector("_ShadowMapParams", shadowMapParams);

            MeshRenderer mr = GetComponent<MeshRenderer>();

            Material mat = mr.materials[0];

            float radius = mRadius;

            mat.SetVector("_Color", mColour);
            mat.SetVector("_LightPosition", new Vector4(transform.position.x, transform.position.y, mFalloffExponent, mAngleFalloffExponent));
            mat.SetVector("_Params2", new Vector4(mAngle * Mathf.Deg2Rad, mSpread * Mathf.Deg2Rad * 0.5f, 1.0f / ((1.0f - mFullBrightRadius) * radius), mFullBrightRadius * radius));
            mat.SetVector("_ShadowMapParams", shadowMapParams);
            //mat.SetTexture("_ShadowTex", shadowMapTexture);
        }


        private Mesh MakeAQuad(float width, float height)
        {
            Mesh mesh = new Mesh();

            // Setup vertices
            Vector3[] newVertices = new Vector3[4];
            float halfHeight = height * 0.5f;
            float halfWidth = width * 0.5f;
            newVertices[0] = new Vector3(-halfWidth, -halfHeight, 0);
            newVertices[1] = new Vector3(-halfWidth, halfHeight, 0);
            newVertices[2] = new Vector3(halfWidth, -halfHeight, 0);
            newVertices[3] = new Vector3(halfWidth, halfHeight, 0);

            // Setup UVs
            Vector2[] newUVs = new Vector2[newVertices.Length];
            newUVs[0] = new Vector2(0, 0);
            newUVs[1] = new Vector2(0, 1);
            newUVs[2] = new Vector2(1, 0);
            newUVs[3] = new Vector2(1, 1);

            // Setup triangles
            int[] newTriangles = new int[] { 0, 1, 2, 3, 2, 1 };

            // Setup normals
            Vector3[] newNormals = new Vector3[newVertices.Length];
            for (int i = 0; i < newNormals.Length; i++)
            {
                newNormals[i] = Vector3.forward;
            }

            // Create quad
            mesh.vertices = newVertices;
            mesh.uv = newUVs;
            mesh.triangles = newTriangles;
            mesh.normals = newNormals;

            return mesh;
        }
    }
}

