﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightBlockingMesh : MonoBehaviour
{
    public List<LightBlockingObject2D> staticLightBloker;
    public List<LightBlockingObject2D> DynamicLightBlokers;
    public MeshFilter meshFilter;

    // Use this for initialization
    void Start()
    {
        staticLightBloker = new List<LightBlockingObject2D>();
        staticLightBloker.AddRange(GameObject.FindObjectsOfType<LightBlockingObject2D>());
    }

    // Update is called once per frame
    void Update()
    {
        //meshFilter.mesh = GetLightBlockerMesh();
    }


    // Create a mesh containing all the light blocker edges
    public Mesh GetLightBlockerMesh()
    {
        

        List<Vector2> edges = new List<Vector2>();
        foreach (LightBlockingObject2D b in staticLightBloker)
        {
            b.GetEdges(ref edges);
        }

        List<Vector3> verts = new List<Vector3>();
        List<Vector2> normals = new List<Vector2>();
        for (int i = 0; i < edges.Count; i += 2)
        {
            verts.Add(edges[i + 0]);
            verts.Add(edges[i + 1]);
            normals.Add(edges[i + 1]);
            normals.Add(edges[i + 0]);
        }

        // Simple 1:1 index buffer
        int[] incides = new int[edges.Count];
        for (int i = 0; i < edges.Count; i++)
        {
            incides[i] = i;
        }

        Mesh mesh = new Mesh();
        mesh.SetVertices(verts);
        mesh.SetUVs(0, normals);
        mesh.SetIndices(incides, MeshTopology.Triangles, 0);
        return mesh;
    }
}
