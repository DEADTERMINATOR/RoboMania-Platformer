﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightBlocker2D : MonoBehaviour
{
    public Mesh mesh;

    public void Awake()
    {
        LightBlockers2D.Blockers.Add(this);
    }

    public Mesh GetMesh()
    {
        //Debug.Assert(GetComponent<MeshFilter>().mesh.vertexCount % 3 == 0);
        return GetComponent<MeshFilter>().mesh;
    }
    public Material GetMaterial()
    {
        return GetComponent<MeshRenderer>().material;
    }

}
public class LightBlockers2D
{
    public static List<LightBlocker2D> Blockers = new List<LightBlocker2D>();
}