﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;

public class ShadowMapRendere : MonoBehaviour
{
    CommandBuffer commandBuffer;
    Camera _camera;
    public Material BlackAndWhiteShader;
    public RenderTexture OcclusionMapText;



    public static ShadowMapRendere Instance
    {
        get;
        private set;
    }


    // Use this for initialization
    void Start()
    {
        _camera = GetComponent<Camera>();
    }



    private void OnPreRender()
    {
        if(commandBuffer == null)
        {
            commandBuffer = new CommandBuffer();
            _camera.AddCommandBuffer(CameraEvent.AfterForwardOpaque, commandBuffer = new CommandBuffer());
        }
        RenderTargetIdentifier rtID = new RenderTargetIdentifier(OcclusionMapText);

        MaterialPropertyBlock propertyBlock = new MaterialPropertyBlock();
        commandBuffer.Clear();
        commandBuffer.SetRenderTarget(rtID);
        commandBuffer.ClearRenderTarget(true, true, new Color(1, 1, 1, 1), 1.0f);
        foreach (LightBlocker2D blocker in LightBlockers2D.Blockers)
        {
            Mesh mesh = blocker.GetMesh();
            BlackAndWhiteShader.SetTexture("_MainTex", blocker.GetMaterial().GetTexture("_MainTex"));
            ///commandBuffer.DrawProcedural(camera.cameraToWorldMatrix, BlackAndWhiteShader, -1, mesh.GetTopology(0), mesh.vertexCount);

            commandBuffer.DrawMesh(blocker.GetMesh(), _camera.cameraToWorldMatrix, BlackAndWhiteShader, 0, -1);
            for (int i = 1; i < mesh.subMeshCount; i++)
            {
                commandBuffer.DrawMesh(blocker.GetMesh(), _camera.cameraToWorldMatrix, BlackAndWhiteShader, i, -1);
            }
        }
        Graphics.ExecuteCommandBufferAsync(commandBuffer, ComputeQueueType.Default);
        
    }
    // Update is called once per frame
    void Update()
    {
        
        
    }
}
