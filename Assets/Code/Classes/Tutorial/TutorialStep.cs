﻿public class TutorialStep
{
    public string Title;
    public string Deceription;
    public bool Active;
    public bool _isComplet;


    /// <summary>
    /// Return the Sate of the tutorial 
    /// </summary>
    /// <returns></returns>
    public virtual bool IsComplet()
    {
        return _isComplet;
    }
    public virtual bool CanStart()
    {
        return true;
    }
    // non mon behavieor
    public virtual void Update()
    {

    }

    public virtual void StartStep()
    {

    }

     public virtual void CleanUp()
    {

    }

    public virtual void CancleCliked()
    {

    }
}

