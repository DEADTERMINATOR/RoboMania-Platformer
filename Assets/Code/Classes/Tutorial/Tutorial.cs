﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tutorial : Object 
{

    public int SetepIndex;
    public TutorialStep CurrentStep {get; private set;}
    public List<TutorialStep> Steps = new List<TutorialStep>();
    public bool Running { get; private set; }
    public  Tutorial()
    {
        SetepIndex = 0;
        CurrentStep = Steps[SetepIndex];
        CurrentStep.StartStep();
        Running = true;
    }


   public  void Update()
    {
        if (Running)
        {
            CurrentStep.Update();
            if (CurrentStep.IsComplet())
            {
                MoveToNextStep();
            }
        }
    }

    private void MoveToNextStep()
    {
        SetepIndex++;
        CurrentStep.CleanUp();
        if (SetepIndex < Steps.Count)
        {
            CurrentStep = Steps[SetepIndex];
            CurrentStep.StartStep();
        }
        else
        {
            Running = false;
        }

    }
}
