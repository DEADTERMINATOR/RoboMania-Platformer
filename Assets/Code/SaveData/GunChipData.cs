﻿using UnityEngine;
using System.Collections;
using Characters.Player;

public class GunChipData : SaveData
{
    /// <summary>
    /// The rarity of the chip. More rare chips might do more damage, have more modifiers, or fire more projectiles at once.
    /// </summary>
    public string ChipRarity;

    /// <summary>
    /// Total amount of ammo
    /// </summary>
    public int OldAmmoAmount = 100;

    /// <summary>
    /// The type of ammom useed by the chip (Bullet Is the defult)
    /// </summary>
    public PlayerData.Ammo.AmmoTypes ammoType = PlayerData.Ammo.AmmoTypes.Bullet;

    /// <summary>
    /// Max ammo the gun can have
    /// </summary>
    public int PickUpAmmo;

    /// <summary>
    /// How much damage each projectile does for this chip.
    /// </summary>
    public float DamagePerProjectile;

    /// <summary>
    /// How many projectiles this chip fires at once. 
    /// </summary>
    public int NumberOfProjectiles;

    /// <summary>
    /// How long the cooldown between shots is.
    /// </summary>
    public float CoolDownTime;

    public bool CurrentlyHeldByPlayer;

    #region BuilderData
    //The type name of the chip IE Class name
    public string ChipType;
    /// <summary>
    /// 
    /// </summary>
    public ProjectileTypes ProjectileType;

    /// <summary>
    /// The rarity of the chip in numaric form. More rare chips might do more damage, have more modifiers, or fire more projectiles at once.
    /// </summary>
    public int ChipRarityNumber;
    #endregion

    #region RandomShotGunChip
    /// <summary>
    /// The maximum range of degree randomness a shot can have.
    /// </summary>
    public static float MaxDegreeRangeOfRandomness;


    /// <summary>
    /// What degree range, given a direction of aim, should the projectiles travel in.
    /// e.g. a range of randomness of 20 degrees means that a shot fired at 45 degrees may travel
    /// at any degree from 35 to 55 degrees.
    /// </summary>
    public float DegreeRangeOfRandomness;
    #endregion

    #region SpreadShotGunChip
    /// <summary>
    /// How many degrees should each projectile fired at once spread out from each other.
    /// </summary>
    public float DegreeOfSpread;


    /// <summary>
    /// The maximum degree of spread a spread shot gun chip can have.
    /// </summary>
    [HideInInspector]
    public float MaxDegreeOfSpread = 20;

    /// <summary>
    /// The minimum degree of spready a spread shot gun chip can have.
    /// </summary>
    [HideInInspector]
    public float MimDegreeOfSpread = 5;
    #endregion;

    #region StraightShotGunChip
    /// <summary>
    /// How far apart should each projectile be from each other (if this chip fires more than one projectile at a time).
    /// </summary>
    public float ProjectileSpacing;

    /// <summary>
    /// How long does it take the projectiles to apply the complete spacing (if this chip fires more than one projectile at a time).
    /// </summary>
    public float SpacingTime = 0.05f;

    #endregion

}
