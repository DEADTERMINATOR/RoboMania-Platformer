﻿using UnityEngine;
using System.Collections;
using static Characters.Player.PlayerData;

public class PlayerInventorySaveData : SaveData
{
    public Ammo ammo = null;
    public float totalEnergyCellEnerg;
    public int totalScrap;
}
