﻿using UnityEngine;
using System.Collections;

public class ShieldData : SaveData
{
    /// <summary>
    /// The file path for the sprite.
    /// </summary>
    public string TheSpritePath;


    /// <summary>
    /// The maximum amount of power the shield can have.
    /// </summary>
    public float MaxPower;

    /// <summary>
    /// The current amount of power the shield has.
    /// </summary>
    public float CurrentPower;

    /// <summary>
    /// Whether the shield is currently active or not.
    /// </summary>
    public bool Active;

    public Rarity _rarity = Rarity.COUNT;

    /// <summary>
    /// Meta data for the UI
    /// </summary>
   // public ShieldReflectionType ReflectionType = ShieldReflectionType.ABSOURB;

    public Color32 color = Color.cyan;

    public float[] _absorbAmount = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };


    public bool reflectProjectiles;

    public string ClassType;
}
