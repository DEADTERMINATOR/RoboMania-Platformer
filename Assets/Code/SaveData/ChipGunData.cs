﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChipGunData : SaveData
{
    public GunChipData ActiveChip;
    public int CurrentChipID;
    public List<GunChipData> listOfChips = new List<GunChipData>();
}
