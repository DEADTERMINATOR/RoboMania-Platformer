﻿using UnityEngine;
using System.Collections;
using System;
using static Characters.Player.PlayerData;

[Serializable]
public class PlayerSaveData : SaveData
{
    public Ammo PlayerData;
    public ChipGunData ChipGunData;
    public ShieldData shieldData = new ShieldData();
}
