﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformSide = MoveAroundPlatform.PlatformSide;

public class SpinySpawnerData : EnemySpawnerData
{
    public BoxCollider2D AllowedMovementArea;
    public PlatformSide StartingPlatformSide;
}
