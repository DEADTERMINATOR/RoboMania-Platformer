﻿using UnityEngine;
using Characters;
using StartingFacingDirection = Characters.NPC.StartingFacingDirection;

public class NPCSpawnerData : MonoBehaviour
{
    public enum WaypointsSelectionMode { NoWaypoints = 0, AttachedSystem = 1, AssignSystem = 2 }
    public enum BoundsSelectionMode { NoBounds = 0, RectangleField = 1, AttachedCollider = 2, AssignCollider = 3 }
    public enum PathTileSelectionMode { NoTiles = 0, AssignTiller = 1 }

    public GlobalData.GravityType InitialGravityState;
    public float GravityScale;

    public StartingFacingDirection StartFaceDir;
    public int StartingWaypoint;

    public WaypointsSelectionMode WaypointsMode;
    public EditableWayPoints WaypointSystem;

    public BoundsSelectionMode BoundsMode;
    public Rect ActiveBounds;

    public PathTileSelectionMode PathTileMode;
    public AreaTiller AreaTiler;
}
