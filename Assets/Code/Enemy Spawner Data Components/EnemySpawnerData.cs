﻿
public class EnemySpawnerData : NPCSpawnerData
{
    public float MaxHealth;

    public int GunChipSpawnChance;
    public int ShieldSpawnChance;
}
