﻿using UnityEngine;
using System.Collections;

public class PathTileMovingObjectTracker : MonoBehaviour
{
    public Transform Object;
    public int Height = 2;
    public int Width = 1;
    public AreaTiller areaTiller;
    public bool TrackPlayer = false;
    private Vector3 _lastPos;

    // Use this for initialization
    void Start()
    {
        if(TrackPlayer)
        {
            Object = GlobalData.Player.transform;
            _lastPos = Object.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Object.position != _lastPos)
        {
           PathTille tille = areaTiller.Map[PathTilleMap.Vector2ToVector2Int((Vector2)_lastPos)];
            if(tille != null)
            {
                tille.Passable = true;
            }
             tille = areaTiller.Map[PathTilleMap.Vector2ToVector2Int((Vector2)Object.position)];
            if (tille != null && tille.Passable )
            {
                tille.Passable = false;
                _lastPos = Object.position;
            }
           
        }
    }
}
