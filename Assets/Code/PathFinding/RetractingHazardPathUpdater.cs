﻿using LevelComponents.Hazards;
using UnityEngine;
using static LevelComponents.Hazards.RetractingHazard;

public class RetractingHazardPathUpdater : MonoBehaviour
 {
    /// <summary>
    /// The hazard being tracked 
    /// </summary>
    public RetractingHazard trackedHazard;
    /// <summary>
    /// The area to be blokced
    /// </summary>
    public Rect BlockArea;

    public AreaTiller Tiller;
    private HazardState _lastSate;
    private bool _PassableSet = true;

    private void Start()
    {
        _lastSate = trackedHazard.CurrentState;
    }
    void Update()
    {
       if(_lastSate != trackedHazard.CurrentState)
        {
            switch(trackedHazard.CurrentState)
            {
                case HazardState.Extending:
                    if(_PassableSet)
                    {
                        SetNotPassable();
                    }
                    break;
                case HazardState.Retracted:
                    if (!_PassableSet)
                    {
                        SetPassable();
                    }
                    break;
            }
        }
    }

    public void SetNotPassable()
    {
        RectInt roundedRec = new RectInt((int)Mathf.FloorToInt(BlockArea.min.x), Mathf.FloorToInt(BlockArea.min.y), Mathf.CeilToInt(BlockArea.max.x - BlockArea.min.x) + 1, (int)Mathf.CeilToInt(BlockArea.max.y - BlockArea.min.y) + 1);
        for (int x = roundedRec.min.x; x < roundedRec.max.x; x++)
        {
            for (int y = roundedRec.min.y; y < roundedRec.max.y; y++)
            {
                if (Tiller.Bounds.Contains(new Vector2Int(x, y)))
                {
                    Tiller.Map.Data[x + Tiller.Offset.x, y + Tiller.Offset.y].Passable = false;
                }
            }
        }
        _PassableSet = false;
    }

    public void SetPassable()
    {

        RectInt roundedRec = new RectInt((int)Mathf.FloorToInt(BlockArea.min.x), Mathf.FloorToInt(BlockArea.min.y), Mathf.CeilToInt(BlockArea.max.x - BlockArea.min.x) + 1, (int)Mathf.CeilToInt(BlockArea.max.y - BlockArea.min.y) + 1);
        for (int x = roundedRec.min.x; x < roundedRec.max.x; x++)
        {
            for (int y = roundedRec.min.y; y < roundedRec.max.y; y++)
            {
                if (Tiller.Bounds.Contains(new Vector2Int(x, y)))
                {
                    Tiller.Map.Data[x + Tiller.Offset.x, y + Tiller.Offset.y].Passable = true;
                }
            }
        }
        _PassableSet = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(BlockArea.center, new Vector3(BlockArea.size.x, BlockArea.size.y));
    }
}

