﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AreaTiller : MonoBehaviour
{
    public string AreaName = "";
    public PathTilleMap Map;
    public RectInt Bounds;
    public GameObject ObsticalRoot;
    public LayerMask ObsticalLayer;
    public Vector2Int Offset { get { return _startingOffset; }  }
    private Vector2Int _startingOffset;
    /// <summary>
    /// Render - The Render bounds is used
    /// Collider - The attacked Collider
    /// AttachedRec - The Attacked Rec
    /// Complex - use TillerSettings
    /// </summary>
    public enum BoundsMode{Render,Collider,AttachedRec,Complex }
    public BoundsMode DefaultBoundsMode;
    public bool CreateImpassableBuffer = false;
    public Vector2Int ImpassableBufferSize = Vector2Int.zero;
    public bool CreateTriggersAroundImpassableTiles = false;

    private Guid _lockID;

    void Awake()
    {
        _startingOffset = new Vector2Int(-Bounds.min.x, -Bounds.min.y);
        Map = new PathTilleMap(Bounds,AreaName);
        MakeMap();
    }


    public void MakeMap()
    {
        _lockID = GameMaster.LoadingLocks.Lock();
        for (int x = 0; x < Map.Data.GetLength(0); x++)
        {
            for (int y = 0; y < Map.Data.GetLength(1); y++)
            {
                Map.Data[x, y] = new PathTille
                {
                    Passable = true,
                    WorldPoint = new Vector2(x - _startingOffset.x, y - _startingOffset.y),
                    Point = new Vector2(x, y)
                };
            }
        }
        foreach (GameObject go in GetObjectsInLayer(ObsticalRoot, ObsticalLayer.value) )
        {
            if (go.activeInHierarchy)
            {
                BoxCollider2D triggerCollider;

                TillerSettings settings = go.GetComponentInChildren<TillerSettings>();
                if (!settings)
                {
                    settings = go.GetComponentInChildren<TillerSettings>();
                }
                BoundsMode boundsMode = DefaultBoundsMode;
                if (settings)
                {
                    boundsMode = settings.boundsMode;
                }
                if (boundsMode == BoundsMode.Render)
                {
                    Renderer render = go.GetComponent<Renderer>();
                    if (render)
                    {
                        Bounds renderBounds = render.bounds;
                        if (CreateImpassableBuffer)
                        {
                            renderBounds.Expand(new Vector2(ImpassableBufferSize.x, ImpassableBufferSize.y));
                        }
                        RectInt roundedRec = new RectInt((int)Mathf.FloorToInt(renderBounds.min.x), Mathf.FloorToInt(renderBounds.min.y), Mathf.CeilToInt(renderBounds.max.x - renderBounds.min.x) + 1, (int)Mathf.CeilToInt(renderBounds.max.y - renderBounds.min.y) + 1);

                        if (CreateTriggersAroundImpassableTiles)
                        {
                            triggerCollider = gameObject.AddComponent<BoxCollider2D>();

                            triggerCollider.offset = roundedRec.center - (Vector2)transform.position;
                            triggerCollider.size = roundedRec.size;

                            triggerCollider.isTrigger = true;
                            gameObject.layer = GlobalData.TRIGGER_LAYER;
                        }

                        for (int x = roundedRec.min.x; x < roundedRec.max.x; x++)
                        {
                            for (int y = roundedRec.min.y; y < roundedRec.max.y; y++)
                            {
                                if (Bounds.Contains(new Vector2Int(x, y)))
                                {
                                    Map.Data[x + _startingOffset.x, y + _startingOffset.y].Passable = false;
                                }
                            }
                        }
                    }
                }
                if (boundsMode == BoundsMode.Collider)
                {
                    Collider2D collider = go.GetComponent<Collider2D>();
                    if (collider)
                    {
                        Bounds colliderBounds = collider.bounds;
                        if (CreateImpassableBuffer)
                        {
                            colliderBounds.Expand(new Vector2(ImpassableBufferSize.x, ImpassableBufferSize.y));
                        }
                        RectInt roundedRec = new RectInt((int)Mathf.RoundToInt(colliderBounds.min.x), Mathf.RoundToInt(colliderBounds.min.y), Mathf.CeilToInt(colliderBounds.max.x - colliderBounds.min.x), (int)Mathf.CeilToInt(colliderBounds.max.y - colliderBounds.min.y));

                        if (CreateTriggersAroundImpassableTiles)
                        {
                            triggerCollider = gameObject.AddComponent<BoxCollider2D>();

                            triggerCollider.offset = roundedRec.center - (Vector2)transform.position;
                            triggerCollider.size = roundedRec.size;

                            triggerCollider.isTrigger = true;
                            gameObject.layer = GlobalData.TRIGGER_LAYER;
                        }

                        for (int x = roundedRec.min.x; x < roundedRec.max.x; x++)
                        {
                            for (int y = roundedRec.min.y; y < roundedRec.max.y; y++)
                            {
                                if (Bounds.Contains(new Vector2Int(x, y)))
                                {
                                    try
                                    {
                                        Map.Data[x + _startingOffset.x, y + _startingOffset.y].Passable = false;
                                    }
                                    catch (System.Exception)
                                    {
                                        Debug.Log(new Vector2(x + _startingOffset.x, y + _startingOffset.y));
                                        Debug.Log(new Vector2(x, y));
                                    }
                                }

                            }
                        }
                    }
                }
                if (boundsMode == BoundsMode.AttachedRec)
                {
                    RectInt Rec = go.GetComponent<RectInt>();
                    if (Rec.size != Vector2Int.zero)
                    {
                        RectInt roundedRec = Rec;
                        if (CreateImpassableBuffer)
                        {
                            roundedRec.width += ImpassableBufferSize.x;
                            roundedRec.height += ImpassableBufferSize.y;
                        }

                        if (CreateTriggersAroundImpassableTiles)
                        {
                            triggerCollider = gameObject.AddComponent<BoxCollider2D>();

                            triggerCollider.offset = roundedRec.center - (Vector2)transform.position;
                            triggerCollider.size = roundedRec.size;

                            triggerCollider.isTrigger = true;
                            gameObject.layer = GlobalData.TRIGGER_LAYER;
                        }

                        for (int x = roundedRec.min.x; x < roundedRec.max.x; x++)
                        {
                            for (int y = roundedRec.min.y; y < roundedRec.max.y; y++)
                            {
                                Map.Data[x + _startingOffset.x, y + _startingOffset.y].Passable = false;

                            }
                        }
                    }
                }
                if (boundsMode == BoundsMode.Complex && settings != null)
                {
                    foreach (RectInt Rec in settings.Bounds)
                    {
                        RectInt recCopy = Rec;
                        if (recCopy.size != Vector2Int.zero)
                        {
                            if (CreateImpassableBuffer)
                            {
                                recCopy.width += ImpassableBufferSize.x;
                                recCopy.height += ImpassableBufferSize.y;
                            }

                            if (CreateTriggersAroundImpassableTiles)
                            {
                                triggerCollider = gameObject.AddComponent<BoxCollider2D>();

                                triggerCollider.offset = recCopy.center - (Vector2)transform.position;
                                triggerCollider.size = recCopy.size;

                                triggerCollider.isTrigger = true;
                                gameObject.layer = GlobalData.TRIGGER_LAYER;
                            }

                            for (int x = recCopy.min.x; x < recCopy.max.x; x++)
                            {
                                for (int y = recCopy.min.y; y < recCopy.max.y; y++)
                                {
                                    if (Bounds.Contains(new Vector2Int(x, y)))
                                    {
                                        Map.Data[x + _startingOffset.x, y + _startingOffset.y].Passable = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        GameMaster.LoadingLocks.Unlock(_lockID);
    }

   
 
    private  List<GameObject> GetObjectsInLayer(GameObject root, LayerMask layerMask)
    {
        var objList = new List<GameObject>();
        for (int i = 0; i < root.transform.childCount; i++)
        {
            Transform t = root.transform.GetChild(i);

            if (layerMask == (layerMask | 1 << t.gameObject.layer))
            {
                objList.Add(t.gameObject);
            }
            if (t.childCount > 0)
            {
                GetObjectsInChildInLayer(t.gameObject, layerMask, ref objList);
            }
        }
        return objList;
    }
    private void GetObjectsInChildInLayer(GameObject root, LayerMask layerMask, ref List<GameObject> listRef )
    {

        for (int i = 0; i < root.transform.childCount; i++)
        {
            Transform t = root.transform.GetChild(i);
            if (layerMask == (layerMask | 1 << t.gameObject.layer))
            {
                listRef.Add(t.gameObject);
            }
            if(t.childCount >0)
            {
                GetObjectsInChildInLayer(t.gameObject, layerMask, ref listRef);// reersave 
            }
        }
        return ;
    }
    private void OnDrawGizmosSelected()
    {

        if(string.IsNullOrEmpty(AreaName))
        {
            AreaName = this.gameObject.name;
        }
#if UNITY_EDITOR
        Vector2Int pos = PathTilleMap.Vector2ToVector2Int(transform.position);
        Bounds.x = pos.x;
        Bounds.y = pos.y;
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(Bounds.center, new Vector3(Bounds.size.x, Bounds.size.y));
#endif

        if (Map != null && Map.Data.Length >0)
        {
            for(int X =0; X < Map.Data.GetLength(0);X++)
            {
                for (int Y = 0; Y < Map.Data.GetLength(1); Y++)
                {
                    
                    Vector2 point = new Vector2(X - _startingOffset.x, Y - _startingOffset.y);
                    //Gizmos.DrawWireCube(new Vector2(X - _startingOffset.x, Y - _startingOffset.y), rect.size);                  
                    PathTille up = null;
                    if (Y+1 < Map.Data.GetLength(1))
                    {
                        up = Map.Data[X, Y + 1];
                    }
                    PathTille right = null;
                    if (X+1 != Map.Data.GetLength(0))
                    {
                        right = Map.Data[X + 1, Y];
                    }
                    if (up != null)
                    {
                        if(up.Passable)
                        {
                            Gizmos.color = Color.green;
                        }
                        else
                        {
                            Gizmos.color = Color.red;
                        }
                        Gizmos.DrawLine(point, point + Vector2.up);
                    }
                    if (right != null)
                    {
                        if (right.Passable)
                        {
                            Gizmos.color = Color.green;
                        }
                        else
                        {
                            Gizmos.color = Color.red;
                        }
                        Gizmos.DrawLine(point, point + Vector2.right);
                    }

                    if (Map.Data[X, Y].Passable)
                    {
                        Gizmos.color = Color.green;
                    }
                    else
                    {
                        Gizmos.color = Color.red;
                    }
                    Gizmos.DrawSphere(point, 0.1f);
                }
            }
        }

    }
}
