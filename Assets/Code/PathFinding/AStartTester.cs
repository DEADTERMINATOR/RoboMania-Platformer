﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AStartTester : MonoBehaviour
{
     public AreaTiller area;
    public Vector2Int Start;
    public Vector2Int End;
    private Vector2Int _start;
    private Vector2Int _end;
    public List<PathTille> path;
    public Transform StartGo;

   

    // Update is called once per frame
    void Update()
    {
        Start = PathTilleMap.Vector2ToVector2Int(StartGo.position);
        if (_start !=  Start || _end != End)
        {
            path = area.Map.SortestOrCloestsPath(Start, End);
            _start = Start;
            _end = End;
        }
    }

    private void OnDrawGizmos()
    {
        VisualDebug.DrawGizmoPointWithCircle(new Vector3(Start.x, Start.y), Color.blue,Color.yellow);
        VisualDebug.DrawGizmoPointWithCircle(new Vector3(End.x, End.y), Color.white, Color.yellow);
        Gizmos.color = Color.cyan;
        if (path != null && path.Count > 1)
        {
            GizmoHelper.drawString("0", path[0].WorldPoint);
            Vector2 last = path[0].WorldPoint;
            for(int i = 1; i < path.Count;i++)
            {
                Gizmos.DrawLine(last, path[i].WorldPoint);
                last = path[i].WorldPoint;
                GizmoHelper.drawString(i.ToString(), path[i].WorldPoint);

            }

        }
    }
}
