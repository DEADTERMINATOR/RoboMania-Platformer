﻿using UnityEngine;
using System.Collections;
using static AreaTiller;
using System.Collections.Generic;

public class TillerSettings : MonoBehaviour
{
    public BoundsMode boundsMode;
    [SerializeField]
    public List<RectInt> Bounds;
    private void OnDrawGizmosSelected()
    {
#if UNITY_EDITOR
        if(GetComponent<Ferr2DT_PathTerrain>() != null)
        {
            Debug.LogWarning("TillerSettings on the same gameobject as a Ferr2DT_PathTerrain ");
        }
#endif
        if (Bounds != null)
        {
            foreach (RectInt bound in Bounds)
            {
                Gizmos.DrawWireCube(bound.center, new Vector3(bound.size.x, bound.size.y));
            }
        }
    }

}
