﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PathTilleMap
{

    public string Key { get; private set; } 
    public PathTille[,] Data { get; private set; }
    public RectInt Bounds { get; private set; }
    private Vector2Int _startingOffset;
    public PathTille this[Vector2Int pos]
    {
        get
        {
            return this[pos.x, pos.y];
        }
    }
    public PathTille this[int x, int y]
    {
        get
        {
            if (x + _startingOffset.x >= 0 && x + _startingOffset.x < Data.GetLength(0) && y + _startingOffset.y >= 0 && y + _startingOffset.y < Data.GetLength(1))
            {
                return this.Data[x + _startingOffset.x, y + _startingOffset.y];
            }
            return null;
        }
    }

    public PathTilleMap(RectInt bounds , string keyName)
    {
        Bounds = bounds;
        _startingOffset = new Vector2Int(-Bounds.min.x, -Bounds.min.y);
        Data = new PathTille[Bounds.size.x, Bounds.size.y];
    }

    public WayPoints SortestPathWaypoints(Vector2Int From, Vector2Int To, bool rethrowExceptions = true)
    {
        //VisualDebug.DrawDebugPoint((Vector2)From, Color.red);
        // VisualDebug.DrawDebugPoint((Vector2)To, Color.green);
        WayPoints wayPoints = null;
        try
        {
            List<PathTille> path = SortestPath(From, To);
            if (path != null && path.Count > 0)
            {
                wayPoints = new WayPoints();
                foreach (PathTille point in path)
                {
                    wayPoints.Add(point.WorldPoint);
                }
            }
        }
        catch (PathOutOfBoundsException e)
        {
            Debug.LogError(e.Message + "\n" + e.StackTrace);
            if (rethrowExceptions)
            {
                throw e;
            }
        }
        catch (PathStartIsEndException e)
        {
            Debug.LogError(e.Message + "\n" + e.StackTrace);
            if (rethrowExceptions)
            {
                throw e;
            }
        }
        return wayPoints;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="From"></param>
    /// <param name="To"></param>
    /// <param name="isFlying"></param>
    /// <returns></returns>
    public WayPoints SortestOrClosestPathWaypoints(Vector2Int From, Vector2Int To, bool isFlying = false, bool rethrowExceptions = true)
    {
        //VisualDebug.DrawDebugPoint((Vector2)From, Color.red);
        //VisualDebug.DrawDebugPoint((Vector2)To, Color.green);
        WayPoints wayPoints = null;
        try
        {
            List<PathTille> path = SortestOrCloestsPath(From, To);
            if (path != null && path.Count > 0)
            {
                wayPoints = new WayPoints();
                foreach (PathTille point in path)
                {
                    wayPoints.Add(point.WorldPoint);
                }
            }
        }
        catch (PathOutOfBoundsException e)
        {
            Debug.LogError(e.Message + "\n" + e.StackTrace);
            if (rethrowExceptions)
            {
                throw e;
            }
        }
        catch (PathStartIsEndException e)
        {
            Debug.LogError(e.Message + "\n" + e.StackTrace);
            if (rethrowExceptions)
            {
                throw e;
            }
        }
        return wayPoints;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="From">The World X And Y Of the Point</param>
    /// <param name="To"></param>
    /// <returns></returns>
    public List<PathTille> SortestPath(Vector2Int From, Vector2Int To, bool isFlying = false)
    {
        Dictionary<Vector2, PathTille> Open = new Dictionary<Vector2, PathTille>();
        Dictionary<Vector2, PathTille> Closed = new Dictionary<Vector2, PathTille>();
        List<PathTille> Path = new List<PathTille>();
        List<PathTille> Neighbors = new List<PathTille>();
        PathTille Current;
        PathTille Goal = this[To.x, To.y];
        PathTille Start = this[From.x, From.y];
       
        if(this[To.x, To.y] == null || this[From.x, From.y] == null)
        {
            throw new PathOutOfBoundsException("Null Point ");
        }
        if (From.x > Bounds.xMax || From.y > Bounds.yMax || To.x > Bounds.xMax || To.y > Bounds.yMax || From.x < 0 || From.y < 0 || To.x < 0 || To.y < 0)
        {
            throw new PathStartIsEndException("Path was out of bounds");
        }
        if(To == From)
        {
            throw new PathStartIsEndException("Start and End points are the same");
        }
        Start.PathFindData = new PathFindData(Heuristic(Start, this[To.x, To.y]), 0, null);
        Open.Add(Start.Point, Start);
        while (Open.Count > 0)
        {
            Current = Open.OrderBy(x => x.Value.PathFindData.FScore).First().Value;
            if (Current == Goal)
            {
                while (Current.PathFindData.Parent != null)
                {
                    Path.Add(Current);
                    Current = (PathTille)Current.PathFindData.Parent;
                }
                Path.Reverse();
                return Path;
            }

            Open.Remove(Current.Point);
            Closed.Add(Current.Point, Current);
            Neighbors.Clear();
            //leftUp
            if (isFlying)
            {
                //leftUp

                TryAndGetNaborFlying((int)Current.Point.x - 1, (int)Current.Point.y + 1, ref Neighbors, 14);
                //RightUp
                TryAndGetNaborFlying((int)Current.Point.x + 1, (int)Current.Point.y + 1, ref Neighbors, 14);
                //RightDown
                TryAndGetNaborFlying((int)Current.Point.x + 1, (int)Current.Point.y - 1, ref Neighbors, 14);
                //LeftDown
                TryAndGetNaborFlying((int)Current.Point.x - 1, (int)Current.Point.y - 1, ref Neighbors, 14);
                //Up
                TryAndGetNaborFlying((int)Current.Point.x, (int)Current.Point.y + 1, ref Neighbors, 10);
                //Right
                TryAndGetNaborFlying((int)Current.Point.x + 1, (int)Current.Point.y, ref Neighbors, 10);
                //Down
                TryAndGetNaborFlying((int)Current.Point.x, (int)Current.Point.y - 1, ref Neighbors, 10);
                //Left
                TryAndGetNaborFlying((int)Current.Point.x - 1, (int)Current.Point.y, ref Neighbors, 10);
            }

            else
            {
                //leftUp

                TryAndGetNabor((int)Current.Point.x - 1, (int)Current.Point.y + 1, ref Neighbors, 14);
                //RightUp
                TryAndGetNabor((int)Current.Point.x + 1, (int)Current.Point.y + 1, ref Neighbors, 14);
                //RightDown
                TryAndGetNabor((int)Current.Point.x + 1, (int)Current.Point.y - 1, ref Neighbors, 14);
                //LeftDown
                TryAndGetNabor((int)Current.Point.x - 1, (int)Current.Point.y - 1, ref Neighbors, 14);
                //Up
                TryAndGetNabor((int)Current.Point.x, (int)Current.Point.y + 1, ref Neighbors, 10);
                //Right
                TryAndGetNabor((int)Current.Point.x + 1, (int)Current.Point.y, ref Neighbors, 10);
                //Down
                TryAndGetNabor((int)Current.Point.x, (int)Current.Point.y - 1, ref Neighbors, 10);
                //Left
                TryAndGetNabor((int)Current.Point.x - 1, (int)Current.Point.y, ref Neighbors, 10);
            }

            float tentative_gScore;
            for (int i = 0; i < Neighbors.Count; i++)
            {
                PathTille Neighbor = Neighbors[i];
                if (!Neighbor.Passable)//Not a valid nabor
                {
                    //Neighbors.Remove(Neighbor);
                    continue;
                }
                if (Neighbor.PathFindData == null)
                {
                    Neighbor.PathFindData = new PathFindData();
                }
                tentative_gScore = Neighbor.GcostTemp + Current.PathFindData.GCost;
                if (!Closed.ContainsKey(Neighbor.Point))
                {
                    if (tentative_gScore < Neighbor.PathFindData.GCost)
                    {
                        if (Open.ContainsKey(Neighbor.Point))
                        {

                            Open.Remove(Neighbor.Point);

                            //Open.Add(Neighbor);

                        }
                        if (Closed.ContainsKey(Neighbor.Point))
                        {
                            Closed.Remove(Neighbor.Point);
                        }

                    }
                    if (!Open.ContainsKey(Neighbor.Point) && !Closed.ContainsKey(Neighbor.Point))
                    {
                        Neighbor.PathFindData.Parent = Current;
                        Neighbor.PathFindData.GCost = tentative_gScore;
                        Neighbor.PathFindData.Hcost = Heuristic(Neighbor, Goal);
                        Open.Add(Neighbor.Point, Neighbor);
                    }

                }

            }
        }
        return null;
    }


    public List<PathTille> SortestOrCloestsPath(Vector2Int From, Vector2Int To,bool isFlying =false)
    {
        Dictionary<Vector2, PathTille> Open = new Dictionary<Vector2, PathTille>();
        Dictionary<Vector2, PathTille> Closed = new Dictionary<Vector2, PathTille>();
        List<PathTille> Path = new List<PathTille>();
        List<PathTille> Neighbors = new List<PathTille>();
        PathTille Current;
        Vector2Int toVector = GetPointOnMap(To.x, To.y);
        PathTille Goal = this[toVector];
        PathTille Start = this[From.x, From.y];
        if (Goal == null || this[From.x, From.y] == null)
        {
            throw new PathOutOfBoundsException("Null Point");
        }
        if (To == From)
        {
            throw new PathStartIsEndException("Start and End points are the same");
        }
        Start.PathFindData = new PathFindData(Heuristic(Start, this[toVector]), 0, null);
        Open.Add(Start.Point, Start);
        while (Open.Count > 0)
        {
            Current = Open.OrderBy(x => x.Value.PathFindData.FScore).First().Value;
            if (Current == Goal)
            {
                while (Current.PathFindData.Parent != null)
                {
                    Path.Add(Current);
                    Current = (PathTille)Current.PathFindData.Parent;
                }
                Path.Reverse();
                return Path;
            }

            Open.Remove(Current.Point);
            Closed.Add(Current.Point, Current);
            Neighbors.Clear();
            if (isFlying)
            {
                //leftUp

                TryAndGetNaborFlying((int)Current.Point.x - 1, (int)Current.Point.y + 1, ref Neighbors, 14);
                //RightUp
                TryAndGetNaborFlying((int)Current.Point.x + 1, (int)Current.Point.y + 1, ref Neighbors, 14);
                //RightDown
                TryAndGetNaborFlying((int)Current.Point.x + 1, (int)Current.Point.y - 1, ref Neighbors, 14);
                //LeftDown
                TryAndGetNaborFlying((int)Current.Point.x - 1, (int)Current.Point.y - 1, ref Neighbors, 14);
                //Up
                TryAndGetNaborFlying((int)Current.Point.x, (int)Current.Point.y + 1, ref Neighbors, 10);
                //Right
                TryAndGetNaborFlying((int)Current.Point.x + 1, (int)Current.Point.y, ref Neighbors, 10);
                //Down
                TryAndGetNaborFlying((int)Current.Point.x, (int)Current.Point.y - 1, ref Neighbors, 10);
                //Left
                TryAndGetNaborFlying((int)Current.Point.x - 1, (int)Current.Point.y, ref Neighbors, 10);
            }

            else
            {
                //leftUp

                TryAndGetNabor((int)Current.Point.x - 1, (int)Current.Point.y + 1, ref Neighbors, 10);
                //RightUp
                TryAndGetNabor((int)Current.Point.x + 1, (int)Current.Point.y + 1, ref Neighbors, 10);
                //RightDown
                TryAndGetNabor((int)Current.Point.x + 1, (int)Current.Point.y - 1, ref Neighbors, 10);
                //LeftDown
                TryAndGetNabor((int)Current.Point.x - 1, (int)Current.Point.y - 1, ref Neighbors, 10);
                //Up
                TryAndGetNabor((int)Current.Point.x, (int)Current.Point.y + 1, ref Neighbors, 14);
                //Right
                TryAndGetNabor((int)Current.Point.x + 1, (int)Current.Point.y, ref Neighbors, 14);
                //Down
                TryAndGetNabor((int)Current.Point.x, (int)Current.Point.y - 1, ref Neighbors, 14);
                //Left
                TryAndGetNabor((int)Current.Point.x - 1, (int)Current.Point.y, ref Neighbors, 14);
            }


            float tentative_gScore;
            for (int i = 0; i < Neighbors.Count; i++)
            {
                PathTille Neighbor = Neighbors[i];
                if(Neighbor == null)
                {
                    Debug.LogError("NULL Neighbor in path");
                }
                if (!Neighbor.Passable)//Not a valid nabor
                {
                    //Neighbors.Remove(Neighbor);
                    continue;
                }
                if (Neighbor.PathFindData == null)
                {
                    Neighbor.PathFindData = new PathFindData();
                }
                tentative_gScore = Neighbor.GcostTemp + Current.PathFindData.GCost; ;
                if (!Closed.ContainsKey(Neighbor.Point))
                {
                    if (tentative_gScore < Neighbor.PathFindData.GCost)
                    {
                        if (Open.ContainsKey(Neighbor.Point))
                        {

                            Open.Remove(Neighbor.Point);

                            //Open.Add(Neighbor);

                        }
                        if (Closed.ContainsKey(Neighbor.Point))
                        {
                            Closed.Remove(Neighbor.Point);
                        }

                     }
                    if (!Open.ContainsKey(Neighbor.Point) && !Closed.ContainsKey(Neighbor.Point))
                    {
                        Neighbor.PathFindData.Parent = Current;
                        Neighbor.PathFindData.GCost = tentative_gScore;
                        Neighbor.PathFindData.Hcost = Heuristic(Neighbor, Goal);
                        Open.Add(Neighbor.Point, Neighbor);
                    }

                }

            }
        }
        Current = Open.OrderBy(x => x.Value.PathFindData.Hcost).FirstOrDefault().Value;
        if (Current != null)
        {
            while (Current.PathFindData.Parent != null)
            {
                Path.Add(Current);
                Current = (PathTille)Current.PathFindData.Parent;
            }
            Path.Reverse();
            return Path;
        }

        return null;
    }


    public static Vector2Int Vector2ToVector2Int(Vector2 point)
    {
        return new Vector2Int(Mathf.RoundToInt(point.x), Mathf.RoundToInt(point.y));
    }

    public bool IsNodeTraversable(Vector2Int point)
    {
        if (this[point] != null)
        {
            return this[point].Passable;
        }
        return false;
    }

    public bool IsNodeTraversable(Vector2 point)
    {
        var intPoint = Vector2ToVector2Int(point);
        if (this[intPoint] != null)
        {
            return this[intPoint].Passable;
        }
        return false;
    }

    private void TryAndGetNabor(int x, int y, ref List<PathTille> List, int cost)
    {
        if (x >= 0 && y >= 0 && x < Data.GetLength(0) && y < Data.GetLength(1))
        {

            PathTille tille = Data[x, y];
            tille.GcostTemp = cost;
            List.Add(tille);
        }
    }

    private void TryAndGetNaborFlying(int x, int y, ref List<PathTille> List, int cost)
    {
        if (x >= 0 && y >= 0 && x < Data.GetLength(0) && y < Data.GetLength(1))
        {

            PathTille tille = Data[x, y];
            if(ISNaborCollider(x,y))
            {
                cost += 8;
            }
            tille.GcostTemp = cost;
            List.Add(tille);
        }
    }


    private bool ISNaborCollider(int x, int y)
    {
        if (x >= 0 && y >= 0 && x < Data.GetLength(0) && y < Data.GetLength(1))
        {
            int i = 0;
            int j = 0;
            var row_limit = Data.GetLength(0);
            if (row_limit > 0)
            {
                var column_limit = Data.GetLength(1);
                for (x = Mathf.Max(0, i - 1); x <= Mathf.Min(i + 1, row_limit); x++)
                {
                    for (y = Mathf.Max(0, j - 1); y <= Mathf.Min(j + 1, column_limit); y++)
                    {
                        if (x != i || y != j)
                        {
                            if (this[x,y] == null || !this[x,y].Passable)
                            {
                                return false;
                            }
                        }
                    }
                }
            }

        }
        return true;
    }

    private float Heuristic(PathTille inStart, PathTille inEnd)
    {
        return Math.Abs(inStart.WorldPoint.x - inEnd.WorldPoint.x) + Math.Abs(inStart.WorldPoint.x - inEnd.WorldPoint.y);
    }

    private Vector2Int GetPointOnMap(int x, int y)
    {
        if(x > Bounds.xMax )
        {
            x = Bounds.xMax;
        }
        else if(x < Bounds.xMin)
        {
            x = Bounds.xMin;
        }
        if(y > Bounds.yMax)
        {
            y = Bounds.yMax;
        }
        else if(y < Bounds.yMin)
        {
            y = Bounds.yMin;
        }
       return new Vector2Int(x, y);
    }
}
[Serializable]
public class PathTille
{
    public bool Passable;
    public Vector2 WorldPoint;
    public Vector2 Point;
    [IgnoreDataMember]
    public PathFindData PathFindData;
    [IgnoreDataMember]
    public float GcostTemp;

}

public class PathFindData
{
    public float Hcost;
    public float GCost;
    public float FScore { get { return GCost + Hcost; } }
    public PathTille Parent;
    public PathFindData(float H, float G, PathTille parent)
    {
        Hcost = H;
        GCost = G;
        Parent = parent;
    }
    public PathFindData()
    {
        Hcost = float.MaxValue;
        GCost = float.MaxValue;
    }
}

public class PathOutOfBoundsException : Exception
{
    public PathOutOfBoundsException(string msg) : base(msg)
    {
        
    }
}

public class PathNUllInpoutBoundsException : Exception
{
    public PathNUllInpoutBoundsException(string msg) : base(msg)
    {

    }
}

public class PathStartIsEndException : Exception
{
    public PathStartIsEndException(string msg) : base(msg)
    {

    }
}