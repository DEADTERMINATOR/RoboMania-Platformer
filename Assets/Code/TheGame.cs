﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class TheGame
{
    /// <summary>
    /// Singalton;
    /// </summary>
    public static TheGame GetRef { get { if (_internalRef == null) { _internalRef = new TheGame(); } return _internalRef; } }
    /// <summary>
    /// The potential states of the overal game
    /// </summary>
    public enum GameState { LOADING, TRANSITION, RESPAWNING, RUNNING, PAUSED, MENU }
    /// <summary>
    /// The curent state as set;
    /// </summary>
    public GameState gameState = GameState.MENU;
    /// <summary>
    /// Sigelaton Ref
    /// </summary>
    private static TheGame _internalRef;

    public Dictionary<string, object> GlobalDataStore = new Dictionary<string, object>();
    
   

    private TheGame()
    {

    }

    public object GetFromDataStore(string key)
    {
        if (GlobalDataStore.ContainsKey(key))
        {
            return GlobalDataStore[key];
        }
        return null;
    }
}

