﻿using LevelSystem;
using System;
using UnityEngine;

namespace GameMaster
{
    public class RobomaniaLevelScript : LevelLoadingScript
    {
        private Guid _lockID;

        /// <summary>
        /// Reference to the level information in the master scene to set the starting current scene correctly
        /// </summary>
        //protected LevelInformation levelInfo;

        public override void OnLoading()
        { 
            if (Level.CurentLevel != null)
            {
                Level.CurentLevel.SetUpLevel();
            }

            if (_lockID != Guid.Empty)
            {
                LoadingLocks.Unlock(_lockID);
                _lockID = Guid.Empty;
            }
        }

        public override void OnLoaded()
        {  
            Level.CurentLevel?.StartLevel();
        }

        public override void OnMasterLoaded()
        {
            GlobalData.Init();
            _lockID = LoadingLocks.Lock();
            //GlobalData.Player.gameObject.SetActive(false);
            ///Level Script will Run on awake
        }
    }
}
