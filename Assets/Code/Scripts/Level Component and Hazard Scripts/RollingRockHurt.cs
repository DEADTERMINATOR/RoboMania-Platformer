﻿using UnityEngine;
using System.Collections;
using static GlobalData;

public class RollingRockHurt : MonoBehaviour, IDamageGiver, IDamageReceiver
{
    /// <summary>
    /// Damage per hit
    /// 
    /// </summary>
    public int Damage = 15;

    private RollingRock  _rollingRock;

    // Use this for initialization
    void Start()
    {
        _rollingRock = GetComponentInParent<RollingRock>();
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        IDamageReceiver charRef = collision.GetComponent<IDamageReceiver>();
        if (charRef != null)
        {
            charRef.TakeDamage(this, Damage, DamageType.ENVIROMENT, transform.position);

            if (StaticTools.IsPlayerOrSheildTagedObj(collision.gameObject))
            {
                _rollingRock.HitSomething();
            }
        }
    }


    public void TakeDamage(IDamageGiver giver, float amount, DamageType type, Vector3 hitPoint)
    {
        if (type == DamageType.PROJECTILE)
        {
            _rollingRock.HitSomething();
        }
    }
}
