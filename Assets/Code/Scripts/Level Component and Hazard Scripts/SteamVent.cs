﻿using UnityEngine;
using System.Collections;
using Characters.Player;

namespace LevelComponents.Hazards
{
    public class SteamVent : ActivatableLevelObject
    {
        enum Angle { D0, D45, D90, D180, D270 }
        enum State { START, RESTING, WARNNING, RUNNING }


        public ParticleSystem SteamParticleSystem;
        public ParticleSystem WarningSteamParticleSystem;

        public AudioSource SteamSound;
        public AudioSource WarnningSound;
        /// <summary>
        /// How long the steam should run
        /// </summary>
        public float SteamTime;
        public float WarningTime;
        public float WaitTime;
        public float StartTimeOffset;

        private DamageOverTimeTrigger _damageOverTimeTrigger;
        private float _internalTime = 0;
        private State _state = State.RUNNING;
        private float _lastStateStart;
        private PlayerMaster _player;


        // Use this for initialization
        protected override void Start()
        {
            base.Start();

            _player = GlobalData.Player;
          

            //Stop the sytem to modify them
            SteamParticleSystem.Stop();
            WarningSteamParticleSystem.Stop();
            //Set all pratical times to match the set state times
            var system = SteamParticleSystem.main;
            system.duration = SteamTime;
            system.loop = false;
            system = WarningSteamParticleSystem.main;
            system.duration = WarningTime + SteamTime;
            system.loop = false;
        }

        // Update is called once per frame
        protected override void FullUpdate()
        {
            if (_player == null)
            {
                return;
            }

            ///Using Distance as performance on windows in the same as using sqrMagnatud Mobile is not the same  
            //visible = 

            BareBonesUpdate();

        }

        protected override void BareBonesUpdate()
        {
            _internalTime += Time.deltaTime;
            if (_internalTime >= StartTimeOffset)
            {
                RunHazard();
            }
        }
        protected override bool CheckIfPlayerIsNearby()
        {
            if (_player != null)
            {
                return Vector3.Distance(transform.position, _player.transform.position) < 30; 
            }
            return false;
        }

        private void RunHazard()
        {
            switch (_state)
            {
                case State.START:
                    if (WarnningSound)
                    {
                        WarnningSound?.Play();
                    }
                    _state = State.WARNNING;
                    _lastStateStart = _internalTime;

                    break;
                case State.RESTING:
                    if (_internalTime - _lastStateStart >= WaitTime)
                    {//End of Resting state
                        if (WarnningSound)
                        {
                            WarnningSound?.Play();
                        }
                        _state = State.WARNNING;
                        _lastStateStart = _internalTime;
                        if (Active) // only when visible
                        {
                            WarningSteamParticleSystem.Play();
                        }
                    }
                    break;
                case State.WARNNING:
                      if (_internalTime - _lastStateStart >= WarningTime)
                    {//End of Warnning state
                        if (SteamSound)
                        {
                            SteamSound?.Play();
                        }
                        _state = State.RUNNING;
                        _lastStateStart = _internalTime;

                        if (Active)
                        {
                            SteamParticleSystem.Play();
                        }
                    }
                    break;
                case State.RUNNING:
                    if (_internalTime - _lastStateStart >= SteamTime)
                    {//End of Running state              
                        _state = State.RESTING;
                        _lastStateStart = _internalTime;
                    }
                    break;
            }
        }

        public void Reset()
        {
            _internalTime = 0;
        }
    }
}
