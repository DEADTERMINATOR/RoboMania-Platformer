﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class FalseCover : MonoBehaviour, IActivatable
{
    private const float REVEAL_TIME = 0.5f;


    public bool IsFerr2D = false;
    public bool FadeBackInOnExit = false;

    [Tooltip("Standard functionality is to reveal the false nature on activation and, optionally, hide it upon deactivation. Check this if you wish to invert this.")]
    public bool InvertFunctionality = false;

    public bool Active { get { return _running; } }


    private List<Renderer> _coverItems = new List<Renderer>();
    private bool _running = false;


    // Start is called before the first frame update
    private void Start()
    {
        if (IsFerr2D)
        {
            Ferr2DT_PathTerrain terrain = GetComponent<Ferr2DT_PathTerrain>();
            if (terrain == null)
            {
                Debug.LogError("False cover " + gameObject.name + " is marked as a Ferr2D object, but no PathTerrain component was found.");
            }
        }
        else
        {
            for (int i = 0; i < transform.childCount; ++i)
            {
                _coverItems.Add(transform.GetChild(i).GetComponent<Renderer>());
            }

            if (_coverItems.Count == 0)
            {
                _coverItems.Add(GetComponent<Renderer>());
            }

            if (_coverItems.Count == 0)
            {
                Debug.LogError("False cover " + gameObject.name + " does not have any renderers attached to turn off/on");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            if (InvertFunctionality)
            {
                HandleFade(false);
            }
            else
            {
                HandleFade(true);
            }

            _running = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER && FadeBackInOnExit)
        {
            if (InvertFunctionality)
            {
                HandleFade(true);
            }
            else
            {
                HandleFade(false);
            }

            _running = false;
        }
    }

    public void Activate(GameObject activator)
    {
        if (_coverItems.Count != 0)
        {
            if (InvertFunctionality)
            {
                HandleFade(false);
            }
            else
            {
                HandleFade(true);
            }

            _running = true;
        }
    }

    public void Deactivate(GameObject activator)
    {
        if (_coverItems.Count != 0)
        {
            if (InvertFunctionality)
            {
                HandleFade(true);
            }
            else
            {
                HandleFade(false);
            }

            _running = false;
        }
    }

    private void HandleFade(bool isFadingOut)
    {
        if (isFadingOut)
        {
            Timing.KillCoroutines("Fade Cover In");
            Timing.RunCoroutine(FadeCoverOut(), "Fade Cover Out");
        }
        else
        {
            Timing.KillCoroutines("Fade Cover Out");
            Timing.RunCoroutine(FadeCoverIn(), "Fade Cover In");
        }
    }

    private IEnumerator<float> FadeCoverOut()
    {
        float progress = 1f;
        Ferr2DT_PathTerrain terrain = null;

        if (IsFerr2D)
        {
            terrain = GetComponent<Ferr2DT_PathTerrain>();
            progress = terrain.vertexColor.a;
        }
        else if (_coverItems[0] is SpriteRenderer)
        {
            SpriteRenderer firstSprite = _coverItems[0] as SpriteRenderer;
            progress = firstSprite.color.a;
        }
        else
        {
            progress = _coverItems[0].material.color.a;
        }

        while (progress > 0f)
        {
            float change = Time.deltaTime / REVEAL_TIME;

            if (IsFerr2D)
            {
                terrain.vertexColor = new Color(terrain.vertexColor.r, terrain.vertexColor.g, terrain.vertexColor.b, progress - change);
                terrain.Build();
            }
            else
            {
                foreach (Renderer item in _coverItems)
                {
                    if (item is SpriteRenderer)
                    {
                        SpriteRenderer spriteItem = item as SpriteRenderer;
                        spriteItem.color = new Color(spriteItem.color.r, spriteItem.color.g, spriteItem.color.b, progress - change);
                    }
                    else
                    {
                        Material[] materials = item.materials;
                        foreach (Material material in materials)
                            material.color = new Color(item.material.color.r, item.material.color.g, item.material.color.b, progress - change);
                    }
                }
            }

            progress -= change;
            yield return 0f;
        }
    }

    private IEnumerator<float> FadeCoverIn()
    {
        float progress = 0f;
        Ferr2DT_PathTerrain terrain = null;

        if (IsFerr2D)
        {
            terrain = GetComponent<Ferr2DT_PathTerrain>();
            progress = terrain.vertexColor.a;
        }
        else if (_coverItems[0] is SpriteRenderer)
        {
            SpriteRenderer firstSprite = _coverItems[0] as SpriteRenderer;
            progress = firstSprite.color.a;
        }
        else
        {
            progress = _coverItems[0].material.color.a;
        }

        while (progress < 1f)
        {
            float change = Time.deltaTime / REVEAL_TIME;

            if (IsFerr2D)
            {
                terrain.vertexColor = new Color(terrain.vertexColor.r, terrain.vertexColor.g, terrain.vertexColor.b, progress + change);
                terrain.Build();
            }
            else
            {
                foreach (Renderer item in _coverItems)
                {
                    if (item is SpriteRenderer)
                    {
                        SpriteRenderer spriteItem = item as SpriteRenderer;
                        spriteItem.color = new Color(spriteItem.color.r, spriteItem.color.g, spriteItem.color.b, progress + change);
                    }
                    else
                    {
                        Material[] materials = item.materials;
                        foreach (Material material in materials)
                            material.color = new Color(item.material.color.r, item.material.color.g, item.material.color.b, progress + change);
                    }
                }
            }

            progress += change;
            yield return 0f;
        }
    }
}
