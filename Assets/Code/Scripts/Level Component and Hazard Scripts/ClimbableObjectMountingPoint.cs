﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;
using Characters.Player;

public class ClimbableObjectMountingPoint : MonoBehaviour
{
    /// <summary>
    /// The offset on the X-axis applied to the player's mount position when the sprite is facing to the left.
    /// </summary>
    private float X_MOUNT_OFFSET_WHEN_FACING_LEFT = 0.25f;


    /// <summary>
    /// An enum representing the possible ends of the climbable object.
    /// </summary>
    public enum ClimbableObjectEnd { Top, Bottom, Left, Right };


    public bool PlayerInsideMountingPoint { get { return _playerInsideMountingPoint; } }


    /// <summary>
    /// Which end of the climbable object this mounting point is positioned at.
    /// </summary>
    [SerializeField]
    private ClimbableObjectEnd _end;

    /// <summary>
    /// The climbable object this mount point is associated with.
    /// </summary>
    [SerializeField]
    private ClimbableObject _associatedClimbableObject;

    /// <summary>
    /// Whether the player should immediately stop climbing if they come into contact with this mounting point.
    /// </summary>
    [SerializeField]
    private bool _stopClimbingOnContact;

    /// <summary>
    /// Whether the player should mount the climbable object using the mounting point trigger as reference.
    /// If it is the top mounting point, the player will be positioned right below it, if it is the
    /// bottom mounting point, right above it.
    /// </summary>
    [SerializeField]
    private bool _startPlayerAtCollider;

    /// <summary>
    /// The position relative to the mounting point that the player should start at if they shouldn't 
    /// start at the collider (disabled if StartPlayerAtCollider is true).
    /// </summary>
    [SerializeField]
    private Vector3 _playerStartingPosition;

    /// <summary>
    /// The player's starting position on the climbable object converted to global coordinates
    /// (not used if the player starts at the collider).
    /// </summary>
    private Vector3 _globalPlayerStartingPoint;

    /// <summary>
    /// Whether the player is currently inside the mounting point trigger.
    /// </summary>
    private bool _playerInsideMountingPoint;

    /// <summary>
    /// The trigger collider for the mounting point.
    /// </summary>
    private Collider2D _mountPoint;

    /// <summary>
    /// Reference to the player.
    /// </summary>
    private PlayerMaster _playerRef;



	// Use this for initialization
	private void Start()
    {
        if (_associatedClimbableObject == null)
        {
            _associatedClimbableObject = GetComponentInParent<ClimbableObject>();
            if (_associatedClimbableObject == null)
            {
                Debug.LogWarning("Climbable object mounting point " + gameObject.name + " doesn't have a climbable object associated with it, and one could not be found.");
            }
        }
        _globalPlayerStartingPoint = _playerStartingPosition + transform.position;

        _mountPoint = GetComponent<Collider2D>();

        _playerRef = GlobalData.Player;
	}
	

    public void SetOnClimbableObject()
    {
        if (_startPlayerAtCollider)
        {
            if (_end == ClimbableObjectEnd.Bottom)
            {
                if (_playerRef.Controller.Collisions.FaceDir == -1)
                {
                    //The player is slightly off center when mounting a ladder if they are facing left, so give them a nudge to align them.
                    _playerRef.transform.position = new Vector3(_mountPoint.bounds.center.x + X_MOUNT_OFFSET_WHEN_FACING_LEFT, _mountPoint.bounds.max.y + PlayerMaster.PLAYER_VERTICAL_EXTENTS, _playerRef.transform.position.z);
                }
                else
                {
                    _playerRef.transform.position = new Vector3(_mountPoint.bounds.center.x, _mountPoint.bounds.max.y + PlayerMaster.PLAYER_VERTICAL_EXTENTS, _playerRef.transform.position.z);
                }
            }
            else if (_end == ClimbableObjectEnd.Top)
            {
                if (_playerRef.Controller.Collisions.FaceDir == -1)
                {
                    _playerRef.transform.position = new Vector3(_mountPoint.bounds.center.x + X_MOUNT_OFFSET_WHEN_FACING_LEFT, _mountPoint.bounds.min.y - PlayerMaster.PLAYER_VERTICAL_EXTENTS - 0.1f, _playerRef.transform.position.z);
                }
                else
                {
                    _playerRef.transform.position = new Vector3(_mountPoint.bounds.center.x, _mountPoint.bounds.min.y - PlayerMaster.PLAYER_VERTICAL_EXTENTS - 0.1f, _playerRef.transform.position.z);
                }
            }
        }
        else
        {
            Timing.RunCoroutine(MoveToStartingPoint(), "Move to Ladder Starting Point");
        }
    }


    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (_associatedClimbableObject.MountedObjectPoint != this) //If this is not the mounting point used to enter
        {
            if (_associatedClimbableObject.Climbing && _stopClimbingOnContact)
            {
                _associatedClimbableObject.SetOffClimbableObject();
            }
        }
        else
        {
            if (_end == ClimbableObjectEnd.Bottom && GlobalData.Player.Velocity.y >= -0.01)
            {
                _playerRef.Movement.SetVelocity('y', 0);
                
            }
            else if (_end == ClimbableObjectEnd.Top && GlobalData.Player.Velocity.y <= 0.01)
            {
                _playerRef.Movement.SetVelocity('y', 0);
            }
        }
      
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        _playerInsideMountingPoint = true;     
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        _playerInsideMountingPoint = false;
    }


    private IEnumerator<float> MoveToStartingPoint()
    {
        float percentScalingComplete = 0f;

        Vector3 start = _playerRef.transform.position;
        Vector3 end = _globalPlayerStartingPoint;
        if (_playerRef.Controller.Collisions.FaceDir == -1)
        {
            end.x += X_MOUNT_OFFSET_WHEN_FACING_LEFT;
        }

        while (percentScalingComplete < 1f)
        {
            percentScalingComplete += Time.deltaTime / 0.334f;
            _playerRef.transform.position = Vector3.Lerp(start, end, percentScalingComplete);
            yield return 0f;
        }
        yield return 0f;
    }

    private void OnDrawGizmos()
    {
        Collider2D exitCollider = GetComponent<Collider2D>();
        Gizmos.color = new Color32(0, 0, 255, 125);
        Gizmos.DrawCube(exitCollider.bounds.center, exitCollider.bounds.size);

        if (!_startPlayerAtCollider)
        {
            Gizmos.color = Color.magenta;
            float size = 0.3f;
            Gizmos.DrawLine(_playerStartingPosition + transform.position - Vector3.up * size, _playerStartingPosition + transform.position + Vector3.up * size);
            Gizmos.DrawLine(_playerStartingPosition + transform.position - Vector3.left * size, _playerStartingPosition + transform.position + Vector3.left * size);
        }
    }
}
