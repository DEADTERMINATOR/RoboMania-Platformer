﻿using UnityEngine;
using System.Collections;

public class FallingSpike : MonoBehaviour , IDamageGiver
{
    public float DamageAmount = 25;

    public ParticleSystem brakeSFX; 


    void Start()
    {
        brakeSFX.Pause();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (StaticTools.IsPlayerTagedObj(collision.gameObject))
        {
            GlobalData.Player.TakeDamage(this, DamageAmount, GlobalData.DamageType.ENVIROMENT, transform.position);
        }
        brakeSFX.Play();
        gameObject.SetActive(false);
        Destroy(this, 1);
    }
}
