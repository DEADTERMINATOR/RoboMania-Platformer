﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;
using Characters.Player;

public class AirGust : MonoBehaviour
{
    private const float STOP_MOVEMENT_THRESHOLD = 0.95f;

    /// <summary>
    /// The velocity applied to anything caught in the gust.
    /// </summary>
    [SerializeField]
    private Vector2 _velocity;

    /// <summary>
    /// Whether gravity should be disabled for anything caught in the gust.
    /// </summary>
    [SerializeField]
    private bool _disableGravity;

    /// <summary>
    /// Whether the gust should stop moving the player near the edge of the collider (so they don't exit the gust collider without intention).
    /// </summary>
    [SerializeField]
    private bool _stopMovementNearEdge;

    /// <summary>
    /// Whether the air gust should raycast for obstacles (for cases where the gust may be momentarily blocked by some obstacle, preventing it from pushing the player up).
    /// </summary>
    [SerializeField]
    private bool _checkForObstacles = false;

    private PlayerMaster _playerRef;
    private float _timeSinceLastEnergyCellRecharged = 0f;
    private BoxCollider2D _boxCollider;


    private void Start()
    {
        _playerRef = GlobalData.Player;
        _boxCollider = GetComponentInChildren<BoxCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (!_playerRef.State.Dead)
        {
            if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
            {
                _playerRef.State.SetInsideAirGust(true);

                var currentPlayerInputVelocity = _playerRef.GetVelocityComponentVelocity(MoveableObject.OBJECT_VELOCITY_COMPONENT);
                _playerRef.SetVelocity(new Vector2(currentPlayerInputVelocity.x, 0), MoveableObject.OBJECT_VELOCITY_COMPONENT);
                _playerRef.SetVelocity(_velocity, PlayerMaster.AIR_GUST_VELOCITY_COMPONENT);

                if (_disableGravity)
                {
                    _playerRef.GravityScale = 0;
                }

                _playerRef.Data.InstantlyRechargeEnergyCells();
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (_boxCollider == null)
        {
            Debug.LogWarning("Air gust hazard at " + transform.position + " is lacking a box collider 2D. It will not function without one");
            return;
        }

        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER || collider.gameObject.layer == GlobalData.SHIELD_LAYER)
        {
            if (!_playerRef.State.Dead)
            {
                if (_checkForObstacles)
                {
                    RaycastHit2D result = Physics2D.Linecast(_boxCollider.bounds.min, collider.transform.position, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.PLAYER_LAYER_SHIFTED);
                    Debug.DrawLine(_boxCollider.bounds.min, collider.transform.position, Color.red, 0.25f);
                    if (result.collider != null && result.collider.tag != "Player" && result.collider.tag != "Shield")
                    {
                        _playerRef.GravityScale = 1;
                        return; //If the linecast hits something that isn't the player along the way, that something is blocking the gust, so the player doesn't get the lift.
                    }
                }

                if (_stopMovementNearEdge)
                {
                    if (_velocity.x == 0 && _velocity.y != 0)
                    {
                        float yDistanceFromMinEdgeToPlayer = Mathf.Abs(_boxCollider.bounds.min.y - _playerRef.transform.position.y);
                        if (yDistanceFromMinEdgeToPlayer > _boxCollider.size.y * transform.localScale.y * STOP_MOVEMENT_THRESHOLD)
                        {
                            return;
                        }
                    }
                    else if (_velocity.x != 0 && _velocity.y == 0)
                    {
                        float xDistanceFromMinEdgeToPlayer = Mathf.Abs(_boxCollider.bounds.min.x - _playerRef.transform.position.x);
                        if (xDistanceFromMinEdgeToPlayer > _boxCollider.size.x * transform.localScale.x * STOP_MOVEMENT_THRESHOLD)
                        {
                            return;
                        }
                    }
                    else
                    {
                        float distanceFromMinEdgeToPlayer = Vector2.Distance(_boxCollider.bounds.min, _playerRef.transform.position);
                        float distanceFromMinEdgeToMaxEdge = Vector2.Distance(transform.position, _boxCollider.bounds.max);
                        if (distanceFromMinEdgeToPlayer > distanceFromMinEdgeToMaxEdge * STOP_MOVEMENT_THRESHOLD)
                        {
                            return;
                        }
                    }

                    if (_playerRef.Data.FreeCellEnergy != _playerRef.Data.TotalEnergyCellEnergy)
                    {
                        _timeSinceLastEnergyCellRecharged += Time.deltaTime;
                        if (_timeSinceLastEnergyCellRecharged >= PlayerConstants.ENERGY_CELL_RECHARGE_TIME_DELAY)
                        {
                            _playerRef.Data.InstantlyRechargeEnergyCells();
                            _timeSinceLastEnergyCellRecharged = 0;
                        }
                    }
                }
                else
                {
                    var currentPlayerVelocity = _playerRef.GetVelocityComponentVelocity(MoveableObject.OBJECT_VELOCITY_COMPONENT);
                    if (_velocity.y != 0)
                    {
                        _playerRef.SetVelocity(new Vector2(currentPlayerVelocity.x, 0)); //TODO: Why am I doing this???
                    }
                    _playerRef.SetVelocity(new Vector2(currentPlayerVelocity.x, _velocity.y));
                }
            }
            else
            {
                OnTriggerExit2D(collider);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            _playerRef.State.SetInsideAirGust(false);
            _playerRef.GravityScale = 1;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "WindGust", false);
    }
}
