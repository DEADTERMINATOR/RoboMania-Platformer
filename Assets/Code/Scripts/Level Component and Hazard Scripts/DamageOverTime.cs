﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Characters;

/// <summary>
/// Dose dame on a set interval to all tracked Character 
/// </summary>
public class DamageOverTime : MonoBehaviour, IDamageGiver ,IActivatable 
{
    public struct TrackedDamageReceiver 
    {
        public IDamageReceiver Character;
        public int TickCount;

        public void Update()
        {
            TickCount++;
        }
        public TrackedDamageReceiver(IDamageReceiver damageReceiver)
        {
            Character = damageReceiver;
            TickCount = 0;
        }
    }

    public float BaseDamage = 10;
    public float DameIncreaseRate = 0.02f;
    public float TickInterval = 0.5F;
    public bool PlayerOnly;
    /// <summary>
    /// If not exponential then growth  is liner  
    /// </summary>
    public bool ExponentialIncrees;
    public bool NoDameRamp;

    public bool Active { get { return _active; } }

    public UnityEvent OnPlayerEnter = new UnityEvent();
    public UnityEvent OnAnyEnter = new UnityEvent();
    public UnityEvent OnPlayerExit = new UnityEvent();
    public UnityEvent OnAnyExit = new UnityEvent();
    public UnityEvent OnAnyTick = new UnityEvent();
    private List<TrackedDamageReceiver> _trackedCharacters = new List<TrackedDamageReceiver>();
    private float _internalTime;
    private bool _active = true;


    private void OnTriggerEnter2D(Collider2D other)
    {
        var character = other.gameObject.GetComponent<IDamageReceiver>(); ;
        bool isPlayer = other.tag == GlobalData.PLAYER_TAG;
        if (((PlayerOnly && isPlayer) ||!PlayerOnly) && character != null )
        {
            if (!IsInList(character))
            {
                _trackedCharacters.Add(new TrackedDamageReceiver(character));
                if(OnPlayerEnter != null && isPlayer)
                {
                    OnPlayerEnter.Invoke();
                }
                OnAnyEnter?.Invoke();
            }
        }
        
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        var character = other.gameObject.GetComponent<IDamageReceiver>(); ;
        bool isPlayer = other.collider.tag == GlobalData.PLAYER_TAG;
        if (((PlayerOnly && isPlayer) || !PlayerOnly) && character != null)
        {
            if (!IsInList(character))
            {
                _trackedCharacters.Add(new TrackedDamageReceiver(character));
                if (OnPlayerEnter != null && isPlayer)
                {
                    OnPlayerEnter.Invoke();
                }
                OnAnyEnter?.Invoke();
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        var character = collision.gameObject.GetComponent<IDamageReceiver>(); ;
        bool isPlayer = collision.tag == GlobalData.PLAYER_TAG;
        if (((PlayerOnly && isPlayer) || !PlayerOnly) && character != null)
        {
            if (RemoveFromList(character))
            {
                _trackedCharacters.Remove(new TrackedDamageReceiver(character));
                if (OnPlayerEnter != null && isPlayer)
                {
                    OnPlayerExit.Invoke();
                }
                OnAnyExit?.Invoke();
            }
        }
    }

    private void Update()
    {
        if (_active)
        {
            _internalTime += Time.deltaTime;
            if(_internalTime >= TickInterval)
            {
                for (int i = 0; i < _trackedCharacters.Count; i++)
                {
                    _trackedCharacters[i].Update();
                    _trackedCharacters[i].Character.TakeDamage(this,GetDmageWithRamp(_trackedCharacters[i].TickCount), GlobalData.DamageType.ENVIROMENT,Vector3.zero);
                }
                OnAnyTick?.Invoke();
                _internalTime = 0;
            }
        }

    }

    private float GetDmageWithRamp(int TickCount)
    {
        if(NoDameRamp)
        {
            return BaseDamage;
        }
        else
        {
            if(ExponentialIncrees)
            {
                return Mathf.Pow(DameIncreaseRate, TickCount)*BaseDamage;
            }
            else
            {
                return BaseDamage + (DameIncreaseRate * (TickCount-1));///Tick start at 1 for Expo
            }
        }
    }

    private bool IsInList(IDamageReceiver damageReceiver)
    {
        for (int i = 0; i < _trackedCharacters.Count; i++)
        {
            if (_trackedCharacters[i].Character == damageReceiver)
            {
                return true;
            }
        }
        return false;
    }


    private bool RemoveFromList(IDamageReceiver damageReceiver)
    {
        for (int i = 0; i < _trackedCharacters.Count; i++)
        {
            if (_trackedCharacters[i].Character == damageReceiver)
            {
                _trackedCharacters.Remove(_trackedCharacters[i]);
                return true;
            }
        }
        return false;
    }

    public void Activate(GameObject activator)
    {
        _active = true;
    }

    public void Deactivate(GameObject activator)
    {
        _active = true;
    }

}

