﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FallingRocksArea : MonoBehaviour, IActivatable
{
    /// <summary>
    /// The Time To wait after an attack
    /// </summary>
    public float WaitTime = 10;
    /// <summary>
    /// How long is warring of droops
    /// </summary>
    public float WarringTime = 3;
    /// <summary>
    /// How long are rocks droop fro
    /// </summary>
    public float DroopTime =10;
    /// <summary>
    /// The point that the rocks will fall from
    /// </summary>
    public List<Vector3> SpawnPoints;
    /// <summary>
    /// The Prefab used to spawn the rocks(may be turned in to a Collection)
    /// </summary>
    public GameObject RockPref;
    /// <summary>
    /// How Much Time in seconds between each droop during the Droop
    /// </summary>
    public float SpawnDelay = 0.25f;

    public bool Active { get { return _isActive; } }


    private PrefabPool pool;

    private bool _isActive;
    private enum State { WAIT, WARN, DROOP }
    private State _theState;
    private float _stateTime;
    private CameraShakeSFX _shakeSFX;
    private float _timeOflastDroop = 0;


    private void Start()
    {
        pool = gameObject.AddComponent<PrefabPool>();
        pool.PoolPrefab = RockPref;

        _shakeSFX = Camera.main.GetComponent<CameraShakeSFX>();
        Debug.Assert(_shakeSFX != null);
    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive)
        {
            _stateTime += Time.deltaTime;
            if (_theState == State.DROOP)
            {
                Droop();
            }
            AdvanceState();
        }
    }

    private void Droop()
    {
        if (Time.realtimeSinceStartup - _timeOflastDroop > SpawnDelay)
        {
            Vector3 XOffeset = new Vector3(Random.Range(-0.5f, 0.5f),0,0);
            int spawnCount = Random.Range(1, 4);
            for(int s =0; s < spawnCount; s++)
            {
                GameObject go = pool.Spawn();
                RollingRock rock = go.GetComponentInChildren<RollingRock>();
                rock.OnRockDead.AddListener(RockDied);
                go.transform.position = transform.position + SpawnPoints[Random.Range(0, SpawnPoints.Count)] + XOffeset;
                go.SetActive(true);
            }
            //GameObject go =   Instantiate(RockPref,, Quaternion.identity);         
            _timeOflastDroop = Time.realtimeSinceStartup;
        }
    }

    private void Warn()
    {
        _shakeSFX.Play(WarringTime);
    }
    private void AdvanceState()
    {

        if (_theState == State.WAIT && _stateTime >= WaitTime)
        {
            _theState = State.WARN;
            Warn();
            _stateTime = 0;
        }
        else if (_theState == State.WARN && _stateTime >= WarringTime)
        {
            _theState = State.DROOP;
            _shakeSFX.Play(DroopTime);
            _stateTime = 0;
        }
        else if (_theState == State.DROOP && _stateTime >= DroopTime)
        {
            _theState = State.WAIT;
            _stateTime = 0;
        }

    }

    private void RockDied(GameObject go)
    {
        pool.NotifyOfObjectDeath(go);
    }

    private void OnDrawGizmos()
    {
        foreach (Vector3 pos in SpawnPoints)
        {
            VisualDebug.DrawGizmoPoint(transform.position + pos, Color.red);
        }
    }

    public void Activate(GameObject activator)
    {
        _stateTime = 0;
        _theState = State.WAIT;
        _isActive = true;
    }

    public void Deactivate(GameObject activator)
    {
        _isActive = false;
    }
}
