﻿using Characters.Player;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class PathFollowingClimbableObject : ClimbableObject
{
    private enum AxisTravelDirections { Negative, Positive }


    /// <summary>
    /// The line renderer that acts as the climbing path guide.
    /// </summary>
    private LineRenderer ClimbingPath;

    /// <summary>
    /// The index of the current line renderer point the player is travelling towards.
    /// </summary>
    private int CurrentClimbingPathIndex = 0;

    /// <summary>
    /// The index of the line renderer point the player should travel towards if they are moving towards the last point on the line renderer.
    /// </summary>
    private int NextClimbingPathIndex = 0;

    /// <summary>
    /// The index of the line renderer point the player should travel towards if they are moving towards the first point on the line renderer.
    /// </summary>
    private int PreviousClimbingPathIndex = 0;


    /* The direction of travel on the relevant axis. For example, if the climbable object is vertical, will moving in a positive direction
     * on the Y-axis increment the climbing path index, or will moving in a negative direction increment it? */
    private AxisTravelDirections _axisTravelDirection;

    /* The rigidbody that is attached to the game object that represents the part of the path climbing is currently occuring on. */
    private Rigidbody2D _currentPathGameObjectRigidbody;
    private HaveLineRendererFollowGameObjects _lineRendererFollow;


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        ClimbingPath = GetComponent<LineRenderer>();
        if (ClimbingPath == null)
        {
            Debug.LogError("No line renderer was found on path following climbable object " + gameObject.name);
        }
        
        if (travelDirection == TravelDirections.Vertical)
        {
            if (ClimbingPath.GetPosition(0).y < ClimbingPath.GetPosition(1).y)
            {
                _axisTravelDirection = AxisTravelDirections.Positive;
            }
            else
            {
                _axisTravelDirection = AxisTravelDirections.Negative;
            }
        }
        else
        {
            if (ClimbingPath.GetPosition(0).x < ClimbingPath.GetPosition(1).x)
            {
                _axisTravelDirection = AxisTravelDirections.Positive;
            }
            else
            {
                _axisTravelDirection = AxisTravelDirections.Negative;
            }
        }
    }

    protected override void Update()
    {
        if (Climbing)
        {
            Vector2 currentPlayerMovementDirection = playerRef.Input.MovementInput;

            bool canContinue = true;
            if (travelDirection == TravelDirections.Vertical && currentPlayerMovementDirection.y != 0)
            {
                canContinue = DetermineIncrementOrDecrementClimbingPathIndex(playerRef.State.NextClimbingPathTarget.y, playerRef.State.PreviousClimbingPathTarget.y, playerRef.TopOfPlayerTransform.y);
            }
            else if (travelDirection == TravelDirections.Horizontal && currentPlayerMovementDirection.x != 0)
            {
                canContinue = DetermineIncrementOrDecrementClimbingPathIndex(playerRef.State.NextClimbingPathTarget.x, playerRef.State.PreviousClimbingPathTarget.x, playerRef.transform.position.x);
            }

            if (canContinue)
            {
                playerRef.State.SetClimbingPathTargets(ClimbingPath.GetPosition(NextClimbingPathIndex), ClimbingPath.GetPosition(PreviousClimbingPathIndex)); //Even if the next and previous climbing path indices have not been incremented or decremented, physics may have moved the points, so we need to refresh the positions the player should be travelling towards.

                VisualDebug.DrawDebugPoint(playerRef.State.NextClimbingPathTarget, Color.red, 0.25f);
                VisualDebug.DrawDebugPoint(ClimbingPath.GetPosition(CurrentClimbingPathIndex), Color.white, 0.25f);
                VisualDebug.DrawDebugPoint(playerRef.State.PreviousClimbingPathTarget, Color.blue, 0.25f);
            }
        }

        base.Update();
    }

    /// <summary>
    /// Compares the Player's position to the positions of the next and previous path indices to determine whether the Player has moved past either of them
    /// and the indices need to be shifted as a result. If the indices cannot be shifted (because the Player is at either end of the path), a call to
    /// get off the climbable object is made.
    /// </summary>
    /// <param name="nextClimbingPathTargetValue">The relevant axis value for the position of the next climbing path index (should be an X-axis value for horizontal climbing or a Y-axis value for vertical climbing).</param>
    /// <param name="previousClimbingPathTargetValue">The relevant axis value for the position of the previous climbing path index (should be an X-axis value for horizontal climbing or a Y-axis value for vertical climbing).</param>
    /// <param name="playerTransformValue">The relevant axis value for the Player's current position (should be an X-axis value for horizontal climbing or a Y-axis value for vertical climbing).</param>
    /// <returns>Whether the indices were safely changed (or no change was necessary) and the Player can continue climbing, of if the Player has gone beyond the bounds of the climbable object.</returns>
    private bool DetermineIncrementOrDecrementClimbingPathIndex(float nextClimbingPathTargetValue, float previousClimbingPathTargetValue, float playerTransformValue)
    {
        if (_axisTravelDirection == AxisTravelDirections.Positive)
        {
            if (playerTransformValue >= nextClimbingPathTargetValue)
            {
                if (NextClimbingPathIndex == ClimbingPath.positionCount - 1)
                {
                    SetOffClimbableObject();
                    return false;
                }

                IncrementClimbingPathIndex();
            }
            else if (playerTransformValue <= previousClimbingPathTargetValue)
            {
                if (PreviousClimbingPathIndex == 0)
                {
                    SetOffClimbableObject();
                    return false;
                }

                DecrementClimbingPathIndex();
            }
        }
        else
        {
            if (playerTransformValue >= nextClimbingPathTargetValue)
            {
                if (NextClimbingPathIndex == 0)
                {
                    SetOffClimbableObject();
                    return false;
                }

                DecrementClimbingPathIndex();
            }
            else if (playerTransformValue <= previousClimbingPathTargetValue)
            {
                if (PreviousClimbingPathIndex == ClimbingPath.positionCount - 1)
                {
                    SetOffClimbableObject();
                    return false;
                }

                IncrementClimbingPathIndex();
            }
        }

        return true;
    }

    private void IncrementClimbingPathIndex()
    {
        CurrentClimbingPathIndex = NextClimbingPathIndex;
        ++NextClimbingPathIndex;
        ++PreviousClimbingPathIndex;
    }

    private void DecrementClimbingPathIndex()
    {
        CurrentClimbingPathIndex = PreviousClimbingPathIndex;
        --NextClimbingPathIndex;
        --PreviousClimbingPathIndex;
    }

    /// <summary>
    /// Finds the index of the point on the climbable object's climbing path that is closest to where the object (likely the Player) initiated climbing.
    /// </summary>
    /// <returns>The index into the ClimbingPath of the closest</returns>
    protected int BinarySearchClosestStartPointIndex()
    {
        if (Vector2.Distance(playerRef.TopOfPlayerTransform, ClimbingPath.GetPosition(0)) < Vector2.Distance(playerRef.TopOfPlayerTransform, ClimbingPath.GetPosition(1)))
        {
            return 0;
        }

        if (Vector2.Distance(playerRef.TopOfPlayerTransform, ClimbingPath.GetPosition(ClimbingPath.positionCount - 1)) < Vector2.Distance(playerRef.TopOfPlayerTransform, ClimbingPath.GetPosition(ClimbingPath.positionCount - 2)))
        {
            return ClimbingPath.positionCount - 1;
        }

        int min = 0;
        int max = ClimbingPath.positionCount - 1;

        do
        {
            int middle = Mathf.CeilToInt((min + max) / 2);
            if (Vector2.Distance(playerRef.TopOfPlayerTransform, ClimbingPath.GetPosition(min)) <= Vector2.Distance(playerRef.TopOfPlayerTransform, ClimbingPath.GetPosition(max)))
            {
                max = middle - 1;
            }
            else
            {
                min = middle + 1;
            }
        } while (min <= max);

        return Vector2.Distance(playerRef.TopOfPlayerTransform, ClimbingPath.GetPosition(min)) < Vector2.Distance(playerRef.TopOfPlayerTransform, ClimbingPath.GetPosition(max)) ? min : max;
    }

    public override void SetOnClimbableObject(bool setPlayerPosition)
    {
        int closestIndex = BinarySearchClosestStartPointIndex();

        if (closestIndex == 0)
        {
            CurrentClimbingPathIndex = 1;
        }
        else if (closestIndex == ClimbingPath.positionCount - 1)
        {
            CurrentClimbingPathIndex = ClimbingPath.positionCount - 2;
        }
        else
        {
            CurrentClimbingPathIndex = closestIndex;
        }

        NextClimbingPathIndex = _axisTravelDirection == AxisTravelDirections.Positive ? CurrentClimbingPathIndex + 1 : CurrentClimbingPathIndex - 1;
        PreviousClimbingPathIndex = _axisTravelDirection == AxisTravelDirections.Positive ? CurrentClimbingPathIndex - 1 : CurrentClimbingPathIndex + 1;

        if (travelDirection == TravelDirections.Vertical)
        {
            float startingYPosition = playerRef.TopOfPlayerTransform.y;
            if (closestIndex == 0 || closestIndex == ClimbingPath.positionCount - 1)
            {
                startingYPosition = ClimbingPath.GetPosition(CurrentClimbingPathIndex).y - PlayerMaster.PLAYER_VERTICAL_EXTENTS * 2;
            }

            playerRef.transform.position = new Vector3(ClimbingPath.GetPosition(CurrentClimbingPathIndex).x, startingYPosition, playerRef.transform.position.z);
        }
        else
        {
            float startingXPosition = playerRef.transform.position.x;
            if (closestIndex == 0 || closestIndex == ClimbingPath.positionCount - 1)
            {
                startingXPosition = ClimbingPath.GetPosition(CurrentClimbingPathIndex).x;
            }

            playerRef.transform.position = new Vector3(startingXPosition, ClimbingPath.GetPosition(CurrentClimbingPathIndex).y - PlayerMaster.PLAYER_VERTICAL_EXTENTS * 2.5f, playerRef.transform.position.z);
        }

        Climbing = true;
        playerRef.State.SetClimbing(true, this, true, ClimbingPath.GetPosition(NextClimbingPathIndex), ClimbingPath.GetPosition(PreviousClimbingPathIndex));

        playerController.Raycaster.RemoveLayerFromCollisionMask(LayerMask.NameToLayer("Obstacle"));
        InvokeOnClimbableObject();
    }

    public override void SetOffClimbableObject()
    {
        base.SetOffClimbableObject();

        if (playerRef.transform.parent != null)
        {
            playerRef.transform.parent = null;
            playerRef.Rigidbody2D.bodyType = RigidbodyType2D.Kinematic;
            SceneManager.MoveGameObjectToScene(playerRef.transform.gameObject, SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage.MasterScene));
        }
    }
}
