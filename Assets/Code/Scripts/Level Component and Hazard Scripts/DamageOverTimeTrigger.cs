﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalData;

/// <summary>
/// Deal dmage over time based on a tag with in a trigger
/// </summary>
public class DamageOverTimeTrigger : MonoBehaviour, IDamageGiver
{
    /// <summary>
    /// The tag the Dame ofer time will look for to dmage (defult is player)
    /// </summary>
    public LayerMask TargetTag = GlobalData.PLAYER_LAYER;

    public bool DamgeAll = false;
    /// <summary>
    /// Will only look for the set tag and not the player or shield
    /// </summary>
    public bool UseTag = false;

    /// <summary>
    /// The amount of damage per damage tick
    /// </summary>
    public float DamagePerTick;

    /// <summary>
    /// How often damage is applied in seconds
    /// </summary>
    public float DamageTickTime = 0.5f;

    /// <summary>
    /// Should the damage ramp up the longer the object is inside the trigger.
    /// </summary>
    public bool BuffDamageOvertime = false;

    /// <summary>
    /// How much of a buff is added on each hit (BuffDamageOvertime must be true)
    /// </summary>
    public float DamageBuffPerHit = 0.01f;



    /// <summary>
    /// Is the tag inside the trigger
    /// </summary>
    protected bool _tagInTrigger = false;

    /// <summary>
    /// The current amount of damage buff
    /// </summary>
    protected float _damageBuff = 1.0f;

    /// <summary>
    /// Time since last damage applied
    /// </summary>
    protected float _lastDamageTime;

    protected IDamageReceiver _damageReceiver;

    protected List<IDamageReceiver> _damageReceivers;
    
    private float _timeTillRest = 5;

    private float _timeLeavingTrigger = 0;

    private float _internalTime = 0;

    protected virtual void Update()
    {
        _internalTime += Time.deltaTime;
        if (_damageBuff > 1 && (_timeLeavingTrigger >0 &&  _timeLeavingTrigger+ _timeTillRest <= _internalTime))
             _damageBuff = 1;
    }


    public virtual void HandleOnTriggerEnter2D(Collider2D other)
    {
        _tagInTrigger = true;
        _timeLeavingTrigger = 0;
        _damageReceiver.TakeDamage(this, DamagePerTick * _damageBuff, DamageType.ENVIROMENT,Vector3.zero);
        _lastDamageTime = _internalTime;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (enabled)
        {
            _damageReceiver = other.gameObject.GetComponent<IDamageReceiver>();
            if ((!UseTag && _damageReceiver != null) || (UseTag && other.gameObject.layer == TargetTag))
            {
                HandleOnTriggerEnter2D(other);
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (enabled)
        {
            _damageReceiver = other.gameObject.GetComponent<IDamageReceiver>();
            if ((!UseTag && _damageReceiver != null) || (UseTag && other.gameObject.layer == TargetTag))
            {
                HandleOnTriggerExit2D(other);
            }
        }
    }

    public virtual void HandleOnTriggerExit2D(Collider2D other)
    {
        _tagInTrigger = false;
        _timeLeavingTrigger = _internalTime; 
    }

    public virtual void OnTriggerStay2D(Collider2D other)
    {
        if (enabled)
        {
            if (_internalTime >= _lastDamageTime+DamageTickTime)
            {
                _damageReceiver = other.gameObject.GetComponent<IDamageReceiver>();
                if ((!UseTag && _damageReceiver != null) || (UseTag && other.gameObject.layer == TargetTag))
                {
                    _damageReceiver.TakeDamage(this, DamagePerTick * _damageBuff, DamageType.ENVIROMENT, Vector3.zero);

                    _lastDamageTime = _internalTime;
                    if (BuffDamageOvertime)
                        _damageBuff += DamageBuffPerHit;
                }
            }
         
        }
    }
    
}
