﻿using System;
using UnityEngine;

public class SpinningTrap : RotatingObject, IScaledHazard
{
    public Vector3 CurrentRotation { get { return new Vector3(currentX, currentY, currentZ); } }
    public bool BeingScaled { get; set; }


    [SerializeField]
    private int _degreesPerSecond;
    [SerializeField]
    private bool _accelerationOnActivation = false;
    [SerializeField]
    private float _secondsToReachMaxRotationSpeed = 1;
    [SerializeField]
    private bool _deaccelerationOnDeactivation = false;
    [SerializeField]
    private float _secondsToFullyStopRotating = 1;
    [SerializeField]
    private bool _roundToNearestNinetyDegrees = false;

    private bool _isAccelerating = false;
    private bool _isDeaccelerating = false;
    private float _currentDegreesPerSecond;
    private float _currentAccelerationPercentage = 0;


    event ActivateEvent IScaledHazard.AllScalersDeactivated
    {
        add
        {
            BeingScaled = false;
            Deactivate(gameObject);
        }

        remove
        {
            BeingScaled = true;
        }
    }


    protected override void Awake()
    {
        base.Awake();

        if (_accelerationOnActivation)
        {
            if (active)
            {
                _isAccelerating = true;
            }
            _currentDegreesPerSecond = 0;
        }
        else
        {
            _currentDegreesPerSecond = _degreesPerSecond;
        }
    }

    private void Update()
    {
        if (!BeingScaled)
        {
            SpinUpdate();
        }
    }

    public void RunOnceUpdate() { }

    public void ScaledUpdate()
    {
        SpinUpdate();
    }

    private void SpinUpdate()
    {
        if (_isAccelerating)
        {
            _currentAccelerationPercentage += Time.deltaTime / _secondsToReachMaxRotationSpeed;
            _currentDegreesPerSecond = Mathf.Lerp(0, _degreesPerSecond, _currentAccelerationPercentage);
            if (_currentAccelerationPercentage >= 1)
            {
                _currentDegreesPerSecond = _degreesPerSecond;
                _isAccelerating = false;
            }
        }
        else if (_isDeaccelerating)
        {
            _currentAccelerationPercentage -= Time.deltaTime / _secondsToFullyStopRotating;

            if (_currentAccelerationPercentage <= 0)
            {
                _currentDegreesPerSecond = 0;
                _isDeaccelerating = false;
            }
            else
            {
                _currentDegreesPerSecond = Mathf.Lerp(0, _degreesPerSecond, _currentAccelerationPercentage);
            }

            if (_roundToNearestNinetyDegrees)
            {
                bool roundSuccessful = false;

                float totalDeaccelerationRotation = _degreesPerSecond / 2 * _secondsToFullyStopRotating; //d = ((vi + vf) / 2) * t --- where vf is 0
                float remainingDeaccelerationRotation = totalDeaccelerationRotation * _currentAccelerationPercentage;

                switch (axis)
                {
                    case Axis.X:
                        if (Mathf.Round(currentX) % 90 == 0 && remainingDeaccelerationRotation <= 90)
                        {
                            currentX = Mathf.Round(currentX);
                            roundSuccessful = true;
                        }
                        break;
                    case Axis.Y:
                        if (Mathf.Round(currentY) % 90 == 0 && remainingDeaccelerationRotation <= 90)
                        {
                            currentY = Mathf.Round(currentY);
                            roundSuccessful = true;
                        }
                        break;
                    case Axis.Z:
                        if (Mathf.Round(currentZ) % 90 == 0 && remainingDeaccelerationRotation <= 90)
                        {
                            currentZ = Mathf.Round(currentZ);
                            roundSuccessful = true;
                        }
                        break;
                }

                if (roundSuccessful)
                {
                    transform.localRotation = Quaternion.Euler(currentX, currentY, currentZ);
                    _currentDegreesPerSecond = 0;
                    _isDeaccelerating = false;
                }
            }
        }

        if (active || _isDeaccelerating)
        {
            switch (axis)
            {
                case Axis.X:
                    currentX += _currentDegreesPerSecond * Time.deltaTime;
                    if (currentX >= 360 || currentX <= -360)
                    {
                        currentX = 0;
                    }
                    break;
                case Axis.Y:
                    currentY += _currentDegreesPerSecond * Time.deltaTime;
                    if (currentY >= 360 || currentY <= -360)
                    {
                        currentY = 0;
                    }
                    break;
                case Axis.Z:
                    currentZ += _currentDegreesPerSecond * Time.deltaTime;
                    if (currentZ >= 360 || currentZ <= -360)
                    {
                        currentZ = 0;
                    }
                    break;
            }

            transform.localRotation = Quaternion.Euler(currentX, currentY, currentZ);
        }
    }

    public override void Activate(GameObject activator)
    {
        if (!active && _accelerationOnActivation)
        {
            _isAccelerating = true;
            _currentAccelerationPercentage = Mathf.Abs(_currentDegreesPerSecond) / Mathf.Abs(_degreesPerSecond);
        }
        else
        {
            _currentDegreesPerSecond = _degreesPerSecond;
        }

        base.Activate(activator);
    }

    public override void Deactivate(GameObject deactivator)
    {
        if (!BeingScaled)
        {
            if (active && _deaccelerationOnDeactivation)
            {
                _isDeaccelerating = true;
                _currentAccelerationPercentage = Mathf.Abs(_currentDegreesPerSecond) / Mathf.Abs(_degreesPerSecond);
            }
            else
            {
                _currentDegreesPerSecond = 0;
            }

            base.Deactivate(deactivator);
        }
    }
}

