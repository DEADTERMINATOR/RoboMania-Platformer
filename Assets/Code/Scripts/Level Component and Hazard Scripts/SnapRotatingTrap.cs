﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MovementEffects;
using Object = UnityEngine.Object;

public class SnapRotatingTrap : RotatingObject, IScaledHazard, IDeactivateSignal, ISynchronizable
{
    private const float PERCENTAGE_BEFORE_ROTATION_TO_SIGNAL = 0.75f;


    public bool BeingScaled { get; set; }
    public bool Safe { get; private set; }
    public bool IsTransitioning { get { return _isRotating; } }


    [SerializeField]
    protected float snapRotationAmount = 90f;

    protected float lastSnapRotationTarget = 0f;
    protected float nextSnapRotationTarget = 0f;


    [SerializeField]
    private List<float> _timesBetweenRotations = new List<float>();
    [SerializeField]
    private float _startingTimeBetweenRotations;
    [SerializeField]
    private float _rotationTime;

    [SerializeField]
    private bool _warnBeforeRotation;
    [SerializeField]
    private GameObject _warningSprite;

    [SerializeField]
    [Tooltip("If this is enabled, the trap will automatically deactivate itself after performing one snap rotation, and will need to be activated again to perform another snap (and so on).")]
    private bool _singleSnap;

    [SerializeField]
    private bool _synchronizeWithOtherTraps;
    [SerializeField]
    private bool _finishRotationBeforeDeactivation;
    [SerializeField]
    [Range(-360f, 360f)]
    private float _minSafeRotation;
    [SerializeField]
    [Range(-360f, 360f)]
    private float _maxSafeRotation;

    private float _timeSinceLastRotation = 0f;
    private int _timeListIndex = 0;

    private bool _isRotating = false;
    private float _rotationPercentComplete = 0f;
    private bool _finishCurrentRotationProcedure = false;
    private bool _upcomingRotationWarningStarted = false;

    //Re-usable DeactivateSignalArgs instance, to prevent new-ing one every time a deactivate signal needs to be sent.
    private DeactivateSignalArgs _deactivateArgs;


    event ActivateEvent IScaledHazard.AllScalersDeactivated
    {
        add
        {
            if (_isRotating)
            {
                _finishCurrentRotationProcedure = true;
            }
            BeingScaled = false;
        }

        remove
        {
            BeingScaled = true;
        }
    }
    // Used when the trap operates on single snaps. In these cases, after the trap has snap rotated once, it uses this event to sent a signal to its activator that the activator should deactivate this trap.
    // Yes, this object could just deactivate itself, but it's possible the activator itself has actions to perform on a deactivation (e.g. a console switching its visuals from on to off), so this signal ensures that those actions are performed as well.
    public event EventHandler SendDeactivationSignal;


    protected override void Awake()
    {
        base.Awake();
        if (_singleSnap)
        {
            _deactivateArgs = new DeactivateSignalArgs(this);
        }

        _timeSinceLastRotation = _startingTimeBetweenRotations;
    }

    private void FixedUpdate()
    {
        if (_timesBetweenRotations.Count > 0 && ((active && !BeingScaled && !_synchronizeWithOtherTraps) || _finishCurrentRotationProcedure))
        {
            SnapRotateUpdate(_timesBetweenRotations[_timeListIndex]);
        }
    }

    public void RunOnceUpdate() { }

    public void ScaledUpdate()
    {
        if (_timesBetweenRotations.Count > 0)
        {
            SnapRotateUpdate(_timesBetweenRotations[_timeListIndex]);
        }
    }

    public void SynchronizableUpdate()
    {
        if (_isRotating)
        {
            HandleRotation();
        }
    }

    public void GoSafe()
    {
        SetNewRotationTarget(_minSafeRotation);
        _isRotating = true;
    }

    public void GoUnsafe()
    {
        SetNewRotationTarget(_maxSafeRotation + snapRotationAmount);
        _isRotating = true;
    }

    public override void Deactivate(GameObject activator)
    {
        base.Deactivate(activator);
        if (_isRotating && _finishRotationBeforeDeactivation)
        {
            _finishCurrentRotationProcedure = true;
        }
    }

    private void SnapRotateUpdate(float timeBetweenRotation)
    {
        if (!_isRotating && (_timeSinceLastRotation >= timeBetweenRotation || _singleSnap))
        {
            ++_timeListIndex;
            _timeListIndex %= _timesBetweenRotations.Count; //This will automatically loop the index back to 0 when the list count is hit.

            SetNewRotationTarget();

            _isRotating = true;
            _timeSinceLastRotation = 0f;
        }

        if (_isRotating)
        {
            HandleRotation();
        }
        else
        {
            _timeSinceLastRotation += Time.deltaTime;

            if (_warnBeforeRotation && _timeSinceLastRotation >= timeBetweenRotation * PERCENTAGE_BEFORE_ROTATION_TO_SIGNAL && !_upcomingRotationWarningStarted)
            {
                Timing.RunCoroutine(UpcomingTrapRotationWarning());
            }
        }
    }

    protected virtual void SetNewRotationTarget()
    {
        switch (axis)
        {
            case Axis.X:
                lastSnapRotationTarget = currentX;
                nextSnapRotationTarget = currentX + snapRotationAmount;
                break;
            case Axis.Y:
                lastSnapRotationTarget = currentY;
                nextSnapRotationTarget = currentY + snapRotationAmount;
                break;
            case Axis.Z:
                lastSnapRotationTarget = currentZ;
                nextSnapRotationTarget = currentZ + snapRotationAmount;
                break;
        }
    }

    protected void SetNewRotationTarget(float newRotationTarget)
    {
        switch (axis)
        {
            case Axis.X:
                lastSnapRotationTarget = currentX;
                break;
            case Axis.Y:
                lastSnapRotationTarget = currentY;
                break;
            case Axis.Z:
                lastSnapRotationTarget = currentZ;
                break;
        }
        nextSnapRotationTarget = newRotationTarget;
    }

    private void HandleRotation()
    {
        float additionalPercentage = Time.deltaTime / _rotationTime;
        _rotationPercentComplete += additionalPercentage;

        switch (axis)
        {
            case Axis.X:
                currentX = Mathf.Lerp(lastSnapRotationTarget, nextSnapRotationTarget, _rotationPercentComplete >= 1f ? 1f : _rotationPercentComplete);
                if (currentX >= 360 || currentX <= -360)
                {
                    currentX = 0;
                }
                break;
            case Axis.Y:
                currentY = Mathf.Lerp(lastSnapRotationTarget, nextSnapRotationTarget, _rotationPercentComplete >= 1f ? 1f : _rotationPercentComplete);
                if (currentY >= 360 || currentY <= -360)
                {
                    currentY = 0;
                }
                break;
            case Axis.Z:
                currentZ = Mathf.Lerp(lastSnapRotationTarget, nextSnapRotationTarget, _rotationPercentComplete >= 1f ? 1f : _rotationPercentComplete);
                if (currentZ >= 360 || currentZ <= -360)
                {
                    currentZ = 0;
                }
                break;
        }

        transform.localRotation = Quaternion.Euler(currentX, currentY, currentZ);

        if (_rotationPercentComplete >= 1f)
        {
            _rotationPercentComplete = 0f;
            _isRotating = false;

            if (_finishCurrentRotationProcedure)
            {
                _finishCurrentRotationProcedure = false;

                //We don't want to go into the singleSnap specific conditional even if this is a singleSnap object because if we're here then the deactivation has already been triggered.
                //So we don't need the automatic deactivation signal to be sent out. 
                return;
            }

            if (_singleSnap)
            {
                SendDeactivationSignal?.Invoke(this, _deactivateArgs);
            }
        }
    }

    /// <summary>
    /// A co-routine that performs a shake of the tra[ to indicate to the player that indicates the platform is about to snap rotate.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> UpcomingTrapRotationWarning()
    {
        _upcomingRotationWarningStarted = true;

        Transform transformToRotate = _warningSprite != null ? _warningSprite.transform : transform;

        float totalTimeUntilRotation = _timesBetweenRotations[_timeListIndex];
        float totalRotationSignalTime = totalTimeUntilRotation - totalTimeUntilRotation * PERCENTAGE_BEFORE_ROTATION_TO_SIGNAL;

        for (int i = 0; i < 10; ++i)
        {
            float percentRotationComplete = 0f;
            Vector3 startingRotation = transformToRotate.localRotation.eulerAngles;

            while (percentRotationComplete < 1f)
            {
                percentRotationComplete += Time.deltaTime / (totalRotationSignalTime / 10f);
                if (i == 0) //i = 0, rotation = 0 to -5 degrees
                {
                    transformToRotate.localRotation = Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z - 2.5f, percentRotationComplete));
                }
                else if (i % 2 == 0) //i == 2, 4, 6, 8, rotation = 5 to -5 degrees
                {
                    transformToRotate.localRotation = Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z - 5f, percentRotationComplete));
                }
                else if (i % 2 != 0 && i != 9) //i == 1, 3, 5, 7, rotation = -5 to 5 degrees
                {
                    transformToRotate.localRotation = Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z + 5f, percentRotationComplete));
                }
                else //i == 9, rotation = -5 to 0 degrees
                {
                    transformToRotate.localRotation = Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z + 2.5f, percentRotationComplete));
                }
                yield return 0f;
            }
        }

        _upcomingRotationWarningStarted = false;
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
#if UNITY_EDITOR
            Handles.Label(transform.position, new GUIContent("" + _timeSinceLastRotation));
#endif
        }
    }
}
