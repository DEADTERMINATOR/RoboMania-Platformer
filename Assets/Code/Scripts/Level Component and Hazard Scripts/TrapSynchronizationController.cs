﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class TrapSynchronizationController : MonoBehaviour, IActivatable
{
	public enum SynchronizationMode { AlternatingOverlap }


	[Serializable]
	private class TrapGroup
	{
		public ISynchronizable LeadTrap { get { return _leadTrap as ISynchronizable; } }
		public List<ISynchronizable> FollowerTraps { get { return _followerTrapsCasted; } }

		[SerializeField]
		[RequireInterface(typeof(ISynchronizable))]
		private Object _leadTrap;
		[SerializeField]
		[RequireInterface(typeof(ISynchronizable))]
		private List<Object> _followerTraps;

		private List<ISynchronizable> _followerTrapsCasted = new List<ISynchronizable>();

		public void ConvertFollowerTraps()
        {
			foreach (Object trap in _followerTraps)
            {
				_followerTrapsCasted.Add(trap as ISynchronizable);
            }
        }
	}


	[SerializeField]
	private SynchronizationMode _mode;

	[SerializeField]
	private bool _active;
	[SerializeField]
	private List<TrapGroup> _trapGroups;
	[SerializeField]
	private int _initialSafeTrapGroupIndex;

	/* Alternating Overlap Mode Exposed Properties */
	[SerializeField]
	private float _overlapTime;
	[SerializeField]
	private float _alternateTime;

	private int _activeTrapGroupIndex;
	private float _currentTime;
	private float _totalAlternatingOverlapTime;

    public bool Active { get { return _active; } }

    private void Start()
	{
		if (_mode == SynchronizationMode.AlternatingOverlap)
		{
			for (int i = 0; i < _trapGroups.Count; ++i)
			{
				_trapGroups[i].ConvertFollowerTraps();

				if (i == _initialSafeTrapGroupIndex)
				{
					SetTrapGroupAsSafe(_trapGroups[i]);
					_activeTrapGroupIndex = i;
				}
				else
				{
					SetTrapGroupAsUnsafe(_trapGroups[i]);
				}
			}

			_totalAlternatingOverlapTime = _alternateTime + _overlapTime;
		}
	}

	private void FixedUpdate()
	{
		if (_active)
		{
			bool groupInTransition = false;
			foreach (TrapGroup group in _trapGroups)
			{
				group.LeadTrap.SynchronizableUpdate();
				foreach (ISynchronizable trap in group.FollowerTraps)
				{
					trap.SynchronizableUpdate();
				}

				if (group.LeadTrap.IsTransitioning)
				{
					groupInTransition = true;
				}
			}

			if (groupInTransition)
			{
				return;
			}

			if (_mode == SynchronizationMode.AlternatingOverlap)
			{
				AlternatingOverlapUpdate();
			}
        }
	}

	private void AlternatingOverlapUpdate()
	{
		if (_currentTime < _alternateTime)
		{
			_currentTime += Time.deltaTime;
			if (_currentTime >= _alternateTime)
			{
				SetTrapGroupAsSafe(_trapGroups[_activeTrapGroupIndex]);
			}
		}
		else if (_currentTime > _alternateTime)
		{
			_currentTime += Time.deltaTime;
			if (_currentTime >= _totalAlternatingOverlapTime)
			{
				_activeTrapGroupIndex = (_activeTrapGroupIndex + 1) % _trapGroups.Count;
				for (int i = 0; i < _trapGroups.Count; ++i)
				{
					if (i == _activeTrapGroupIndex)
					{
						SetTrapGroupAsUnsafe(_trapGroups[i]);
					}
					else
					{
						SetTrapGroupAsSafe(_trapGroups[i]);
					}
				}

				_currentTime = 0;
			}
		}
		else
		{
			_currentTime += Time.deltaTime;
		}
	}

	private void SetTrapGroupAsSafe(TrapGroup group)
	{
		group.LeadTrap.GoSafe();
		foreach (ISynchronizable trap in group.FollowerTraps)
		{
			trap.GoSafe();
		}
	}

	private void SetTrapGroupAsUnsafe(TrapGroup group)
	{
		group.LeadTrap.GoUnsafe();
		foreach (ISynchronizable trap in group.FollowerTraps)
		{
			trap.GoUnsafe();
		}
	}

    public void Activate(GameObject activator)
    {
		_active = true;
    }

    public void Deactivate(GameObject activator)
    {
		_active = false;
    }
}