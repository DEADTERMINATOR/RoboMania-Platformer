﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D.Animation;
using Characters.Player;

public class ClimbableObject : MonoBehaviour
{
    /* If the player tries to get off a climbable object while there are within an obstacle collider, this is the maximum percentage of the player's Y collider size 
     * that can be within that collider and have the player automatically ejected from the collider. If a larger percentage of the player's collider is within the other collider,
     * we wait for the player to fully exit the collider before re-enabling collision with obstacles. */
    private const float MAXIMUM_COLLIDER_EJECT_DISTANCE_MULTIPLIER = 0.4f;
    private const float BUTTON_PRESS_TIME_OUT = 0.025f;


    public delegate void ClimbableObjectStateChange();
    public event ClimbableObjectStateChange OnClimbableObject;
    public event ClimbableObjectStateChange OffClimbableObject;


    /// <summary>
    /// The possible travel directions for climbable objects.
    /// </summary>
    public enum TravelDirections { Vertical, Horizontal }

    #region Public Variables/Attributes
    /// <summary>
    /// Whether the player is on the object climbing
    /// </summary>
    public bool Climbing { get; protected set; }

    /// <summary>
    /// Public getter for the travel direction for this climbable object.
    /// </summary>
    public TravelDirections TravelDirection { get { return travelDirection; } }

    /// <summary>
    /// The mounting point used by the player to get on the ladder 
    /// </summary>
    public ClimbableObjectMountingPoint MountedObjectPoint { get { return mountedObjectPoint; } }
    #endregion

    #region Protected + Private Variables
    /// <summary>
    /// The direction of travel for this climbable object.
    /// </summary>
    [SerializeField]
    protected TravelDirections travelDirection;

    /// <summary>
    /// A list that contains all the defined zones where the player should go through the mounting procedure if they begin climbing while inside them.
    /// </summary>
    [SerializeField]
    protected List<ClimbableObjectMountingPoint> mountingPoints;

    /// <summary>
    /// Colliders that represent area that, should the player try to get off the ladder while within them,
    /// they will not collide with anything within.
    /// </summary>
    [SerializeField]
    protected List<Collider2D> noCollisionZones;

    /// <summary>
    /// Reference to the player master object that contains the main player components.
    /// </summary>
    protected PlayerMaster playerRef;

    /// <summary>
    /// The player's collider.
    /// </summary>
    protected Collider2D playerCollider;

    /// <summary>
    /// Reference to the player's controller casted to its actual class.
    /// </summary>
    protected PlayerController2D playerController;

    /// <summary>
    /// Indicates that the player got off the ladder within a collider, so their collision was disabled, and we need to
    /// keep testing to see when we can safely re-enable it.
    /// </summary>
    protected bool testForCollisionReenable;

    /// <summary>
    /// The mounting point used by the player to get on to the ladder 
    /// The player will not be ejected if the player is on the point that they mounted
    /// </summary>
    protected ClimbableObjectMountingPoint mountedObjectPoint;


    /// <summary>
    /// The collider for the ladder.
    /// </summary>
    private Collider2D _climbableObjectCollider;

    /// <summary>
    /// Whether the player is inside the trigger collider for the ladder.
    /// </summary>
    private bool _insideCollider;
    #endregion

    #region Unity Methods
    protected virtual void Start()
    {
        _climbableObjectCollider = GetComponent<Collider2D>();

        playerRef = GlobalData.Player;
        playerCollider = playerRef.gameObject.GetComponent<Collider2D>();
        playerController = playerRef.Controller as PlayerController2D;
    }

    protected virtual void Update()
    {
        if (_insideCollider && playerRef.Input.InputDictionary["Action"].IsDown) //Player wants on or off
        {
            if (Climbing)
            {
                SetOffClimbableObject();
            }
            else
            {
                SetOnClimbableObject(true);
            }
        }

        if (testForCollisionReenable)
        {
            bool inNoCollisionZone = PlayerIsInNoCollisionZone();

            if (!inNoCollisionZone)
            {
                testForCollisionReenable = playerController.CheckInternalCollision() == -999 ? false : true;
                if (!testForCollisionReenable)
                {
                    playerRef.Movement.CurrentCollisionCheck = PlayerMovementController.CollisionCheck.Both;
                }
            }
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            _insideCollider = true;
        }
    }

    protected virtual void OnTriggerStay2D(Collider2D collider)
    {
        if (!_insideCollider && collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            _insideCollider = true;
        }
    }

    protected virtual void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            _insideCollider = false;
            if (Climbing)
            {
                SetOffClimbableObject();
            }
        }
    }
    #endregion

    #region Set On/Off Climbable Object
    public virtual void SetOnClimbableObject(bool setPlayerPosition)
    {
        if (!Climbing)
        {
            playerRef.State.SetClimbing(true, this);
            Climbing = true;

            playerController.Raycaster.RemoveLayerFromCollisionMask(LayerMask.NameToLayer("Obstacle"));
            OnClimbableObject?.Invoke();

            if (setPlayerPosition)
            {
                //Check if the player is within the trigger for a mounting point, let that handle the mount if so.
                foreach (ClimbableObjectMountingPoint mountPoint in mountingPoints)
                {
                    if (mountPoint.PlayerInsideMountingPoint)
                    {
                        mountPoint.SetOnClimbableObject();
                        mountedObjectPoint = mountPoint;
                        return;
                    }
                }

                //Otherwise, the player is probably trying to mount the ladder in the middle (e.g. after a jump), so handle it the default way internally.
                if (travelDirection == TravelDirections.Vertical)
                {
                    playerRef.transform.position = new Vector3(_climbableObjectCollider.bounds.min.x + _climbableObjectCollider.bounds.extents.x, playerRef.transform.position.y, playerRef.transform.position.z);
                }
                else
                {
                    playerRef.transform.position = new Vector3(playerRef.transform.position.x, _climbableObjectCollider.bounds.min.y - _climbableObjectCollider.bounds.extents.y, playerRef.transform.position.z);
                }
                mountedObjectPoint = null;
            }
        }
    }

    public void SetOnClimbableObject(Vector2 position)
    {
        SetOnClimbableObject(false);
        playerRef.transform.position = position;
    }

    public virtual void SetOffClimbableObject()
    {
        playerRef.State.SetClimbing(false, null);

        playerController.Raycaster.AddLayerToCollisionMask(LayerMask.NameToLayer("Obstacle"));
        Climbing = false;

        playerRef.transform.rotation = Quaternion.Euler(playerRef.transform.eulerAngles.x, playerRef.transform.eulerAngles.y, 0);

        bool inNoCollisionZone = PlayerIsInNoCollisionZone();
        if (inNoCollisionZone)
        {
            testForCollisionReenable = true;
            playerRef.Movement.CurrentCollisionCheck = PlayerMovementController.CollisionCheck.None;
        }

        if (!inNoCollisionZone)
        {
            float insideColliderEjectDistance = playerController.CheckInternalCollision();

            if (insideColliderEjectDistance != -999)
            {
                if (insideColliderEjectDistance <= playerCollider.bounds.size.y * MAXIMUM_COLLIDER_EJECT_DISTANCE_MULTIPLIER)
                {
                    Vector3 ejectVelocity = new Vector3(0, insideColliderEjectDistance + playerController.Raycaster.HorizontalRaySpacing, 0);
                    playerRef.Controller.Move(ref ejectVelocity, 'N');
                }
                else
                {
                    testForCollisionReenable = true;
                    playerRef.Movement.CurrentCollisionCheck = PlayerMovementController.CollisionCheck.None;
                }
            }
        }

        OffClimbableObject?.Invoke();
    }
    #endregion

    #region Utility Methods
    private bool PlayerIsInNoCollisionZone()
    {
        foreach (Collider2D collider in noCollisionZones)
        {
            bool doesOverlap = collider.OverlapPoint(playerRef.transform.position);
            if (doesOverlap)
                return true;
        }
        return false;
    }

    protected void InvokeOnClimbableObject()
    {
        OnClimbableObject?.Invoke();
    }
    #endregion
}
