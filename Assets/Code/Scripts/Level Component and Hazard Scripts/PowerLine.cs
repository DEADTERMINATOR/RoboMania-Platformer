﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class PowerLine : MonoBehaviour, IActivatable
{
    private readonly Vector4 WIRE_ON = new Color(0, 1, 0, 1);
    private readonly Vector4 WIRE_OFF = new Color(1, 0, 0, 1);
    private readonly Vector4 WIRE_NO_POWER = new Color(0, 0, 0, 1);


    /// <summary>
    /// Whether the initial state of the line is to be turned on.
    /// </summary>
    [HideInInspector]
    public bool InitiallyTurnedOn;

    public bool Active { get { return _isPowered; } }
    public bool IsTurnedOn { get { return _isTurnedOn; } }


    [SerializeField]
    private bool _isPowered;
    [SerializeField]
    private bool _isTurnedOn;

    [RequireInterface(typeof(IActivatable))]
    [SerializeField]
    private Object _objectToPower;

    [SerializeField]
    private Ferr2DT_PathTerrain _wireTerrain;
    [SerializeField]
    private PowerIndicatorManager _associatedPowerIndicator;

    private IActivatable _objAsIActivatable;


    private void Awake()
    {
        InitiallyTurnedOn = _isTurnedOn;
        _objAsIActivatable = _objectToPower as IActivatable;
    }

    private void Start()
    {
        var asIDeactivateSignal = _objectToPower as IDeactivateSignal; //We don't know if this cast will work, so we need to check the cast;
        if (asIDeactivateSignal != null)
        {
            asIDeactivateSignal.SendDeactivationSignal += HandleDeactivationCall;
        }
    }

    public void Activate(GameObject activator)
    {
        if (_isPowered)
        {
            _isTurnedOn = true;
            _objAsIActivatable.Activate(gameObject);

            _wireTerrain.vertexColor = WIRE_ON;
            _wireTerrain.Build(false);

            _associatedPowerIndicator?.Activate(gameObject);
        }
    }

    public void Deactivate(GameObject deactivator)
    {
        _isTurnedOn = false;

        if (_objAsIActivatable.Active)
        {
            _objAsIActivatable.Deactivate(gameObject);
        }

        _wireTerrain.vertexColor = WIRE_OFF;
        _wireTerrain.Build(false);

        _associatedPowerIndicator?.Deactivate(gameObject);
    }

    public void SupplyPower()
    {
        _isPowered = true;

        if (InitiallyTurnedOn)
        {
            _wireTerrain.vertexColor = WIRE_ON;
        }
        else
        {
            _wireTerrain.vertexColor = WIRE_OFF;
        }
        _wireTerrain.Build(false);

        _associatedPowerIndicator?.SupplyPower();
    }

    public void KillPower()
    {
        _isPowered = false;
        _isTurnedOn = false;

        _wireTerrain.vertexColor = WIRE_NO_POWER;
        _wireTerrain.Build(false);

        if (_objAsIActivatable.Active)
        {
            _objAsIActivatable.Deactivate(gameObject);
        }

        _associatedPowerIndicator?.KillPower();
    }

    /// <summary>
    /// Handles a potential callback from the object this line is powering that the line should deactivate it.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    private void HandleDeactivationCall(object sender, EventArgs args)
    {
        if (_isTurnedOn)
        {
            var deactivateArgs = args as DeactivateSignalArgs;
            if (deactivateArgs != null)
            {
                deactivateArgs?.ObjToDeactivate.Deactivate(gameObject);
                _wireTerrain.vertexColor = WIRE_OFF;
            }
        }
    }
}
