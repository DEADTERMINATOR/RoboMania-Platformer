﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeLimitedSnapRotatingTrap : SnapRotatingTrap
{
    [SerializeField]
    [Range(-360, 360)]
    private float _minAngle;

    [SerializeField]
    [Range(-360, 360)]
    private float _maxAngle;

    private float _currentRotationDirection;


    protected override void Awake()
    {
        base.Awake();

        if (_minAngle % snapRotationAmount != 0)
        {
            Debug.LogWarning("The minimum angle of " + _minAngle + " for the RangeLimitedSnapRotatingTarget component attached to " + gameObject.name + " is not equally divisible by the provided snap rotation value of " + snapRotationAmount + ". The object will adhere to the provided snap rotation value, and may go below the desired minimum angle.");
        }
        if (_maxAngle % snapRotationAmount != 0)
        {
            Debug.LogWarning("The maximum angle of " + _maxAngle + " for the RangeLimitedSnapRotatingTarget component attached to " + gameObject.name + " is not equally divisible by the provided snap rotation value of " + snapRotationAmount + ". The object will adhere to the provided snap rotation value, and may go above the desired maximum angle.");
        }

        if (_minAngle > _maxAngle)
        {
            Debug.LogWarning("The minimum angle of " + _minAngle + " for the RangeLimitedSnapRotatingTarget component attached to " + gameObject.name + " was determined to be greater than the maximum angle of " + _maxAngle + ". The values will be swapped.");
            var temp = _minAngle;
            _minAngle = _maxAngle;
            _maxAngle = temp;
        }

        if (Mathf.Abs(_maxAngle - _minAngle) < Mathf.Abs(snapRotationAmount))
        {
            Debug.LogWarning("The difference between the minimum angle of " + _minAngle + " and the maximum angle of " + _maxAngle + " for the RangeLimitedSnapRotatingTarget component attached to " + gameObject.name + " is smaller than the provided snap rotation value of " + snapRotationAmount + ". The object will adhere to the provided snap rotation value, and may not stay within the desired range.");
        }

        switch (axis)
        {
            case Axis.X:
                currentX = Mathf.Clamp(currentX, _minAngle, _maxAngle);
                break;
            case Axis.Y:
                currentY = Mathf.Clamp(currentY, _minAngle, _maxAngle);
                break;
            case Axis.Z:
                currentZ = Mathf.Clamp(currentZ, _minAngle, _maxAngle);
                break;
        }

        transform.localRotation = Quaternion.Euler(currentX, currentY, currentZ);
        _currentRotationDirection = Mathf.Sign(snapRotationAmount);
    }

    protected override void SetNewRotationTarget()
    {
        switch (axis)
        {
            case Axis.X:
                UpdateRotation(currentX);
                break;
            case Axis.Y:
                UpdateRotation(currentY);
                break;
            case Axis.Z:
                UpdateRotation(currentZ);
                break;
        }
    }

    private void UpdateRotation(float currentAxisRotation)
    {
        lastSnapRotationTarget = currentAxisRotation;
        if (currentAxisRotation >= _maxAngle)
        {
            if (snapRotationAmount < 0)
            {
                nextSnapRotationTarget = currentAxisRotation + snapRotationAmount;
                _currentRotationDirection = 1f;
            }
            else
            {
                nextSnapRotationTarget = currentAxisRotation - snapRotationAmount;
                _currentRotationDirection = -1f;
            }
        }
        else if (currentAxisRotation <= _minAngle)
        {
            if (snapRotationAmount < 0)
            {
                nextSnapRotationTarget = currentAxisRotation - snapRotationAmount;
                _currentRotationDirection = -1f;
            }
            else
            {
                nextSnapRotationTarget = currentAxisRotation + snapRotationAmount;
                _currentRotationDirection = 1f;
            }
        }
        else
        {
            nextSnapRotationTarget = currentAxisRotation + snapRotationAmount * _currentRotationDirection;
        }
    }
}
