﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;

public class MudTrigger : MonoBehaviour, IDamageGiver
{
    private Collider2D _trigger;


    private void Start()
    {
        _trigger = GetComponent<Collider2D>();
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            if (_trigger.bounds.max.y - GlobalData.Player.transform.position.y > PlayerMaster.PLAYER_VERTICAL_EXTENTS + 1)
            {
                GlobalData.Player.TakeDamage(this, 5000, GlobalData.DamageType.ENVIROMENT, GlobalData.Player.transform.position);
                //Add some effect on the mud here indicating what happened. Alternatively, set up a callback and pass it off to the player's animator or something.
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            if (_trigger.bounds.max.y - GlobalData.Player.transform.position.y > PlayerMaster.PLAYER_VERTICAL_EXTENTS + 1)
            {
                GlobalData.Player.TakeDamage(this, 5000, GlobalData.DamageType.ENVIROMENT, GlobalData.Player.transform.position);
                //Add some effect on the mud here indicating what happened. Alternatively, set up a callback and pass it off to the player's animator or something.
            }
        }
    }
}
