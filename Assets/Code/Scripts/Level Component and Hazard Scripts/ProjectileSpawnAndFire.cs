﻿using UnityEngine;
using System.Collections;

public class ProjectileSpawnAndFire : MonoBehaviour, IActivatable
{
    public enum ProjectileType { Projectile, ArcedProjectile }

    public ProjectileType Type { get { return _type; } }
    public bool OverrideProjectileSpeed { get { return _overrideProjectileSpeed; } }
    public bool Active { get { return _run; } }


    [SerializeField]
    private Projectile _projectileObject;

    [SerializeField]
    private float _timeBetweenShots;

    [SerializeField]
    private Vector3 _firePositionOffset;

    [SerializeField]
    private bool _overrideProjectileSpeed;

    [SerializeField]
    private float _projectileSpeed;

    [SerializeField]
    private float _damageAmount;

    [SerializeField]
    private float _force;

    [SerializeField]
    private ProjectileType _type;

    [SerializeField]
    private float _angle;

    [SerializeField]
    private float _startDelay = 0;

    [SerializeField]
    private bool _run = false;

    private float _lastShoot;
    private float _totalTime;
    private PrefabPool _pool;


    private void Awake()
    {
        _pool = gameObject.AddComponent<PrefabPool>();
        _pool.PoolPrefab = _projectileObject.gameObject;

        _lastShoot = 0;
        _totalTime = _startDelay * -1f;
    }

    private void FixedUpdate()
    {
        if (_run)
        {
            _totalTime += Time.deltaTime;
            if (_totalTime - _lastShoot >= _timeBetweenShots)
            {
                Shoot();
            }
        }
    }

    private void OnDrawGizmos()
    {
        VisualDebug.DrawGizmoPoint(transform.position + _firePositionOffset, Color.green);
    }

    private void Shoot()
    {
        Projectile pro = null;
        _lastShoot = _totalTime;

        pro = _pool.Spawn().GetComponent<Projectile>();
        var proRigidbody = pro.GetComponent<Rigidbody2D>();
        if (proRigidbody)
        {
            proRigidbody.MovePosition(transform.position + _firePositionOffset);
        }
        else
        {
            pro.transform.position = transform.position + _firePositionOffset;
        }

        if (_type == ProjectileType.ArcedProjectile)
        {
            ((ArcedProjectile)pro).Force = _force;
            ((ArcedProjectile)pro).Fire(new Vector3(0, 0, _angle), _damageAmount, gameObject);
        }
        else
        {
            if (_overrideProjectileSpeed)
            {
                pro.ProjectileSpeed = _projectileSpeed;
            }
            pro.Fire(new Vector3(0, 0, _angle), _damageAmount, gameObject);
        }
    }

    public void Activate(GameObject activator)
    {
        _run = true;
    }

    public void Deactivate(GameObject activator)
    {
        _run = false;
    }
}
