﻿using UnityEngine;
using System.Collections;

public class SwingingTrap : MonoBehaviour, IActivatable
{
    /// <summary>
    /// The Max Angle Of The Swing (Degrees from the top of the swing from the Center Ange or one half the total angler distance )
    /// Example if the desired swing is a 180 swing back and forth set the max at 90
    /// </summary>
    public float MaxAngle = 90;
    /// <summary>
    /// How Long should the swing from MaxAngle to - MaxAngle
    /// </summary>
    public float SwingTime = 5;
    /// <summary>
    /// Start the Swing during the swing time if set to half of the swing time the swing will start on the opposite side  
    /// </summary>
    public float StartTime = 0;

    public bool Active { get { return _isActive; } }


    private Vector3 _currentAngle;
    private float _time = 0;
    private bool _isActive = true;
    private const float MAXANGLE = 100;
    private bool _isOffScreen = false;
    private float _timeWhentOffScreen = 0;



    // Use this for initialization
    void Start()
    {
        _time = Mathf.Min(StartTime, SwingTime);
        if (MaxAngle > 100)
        {
            MaxAngle = 100;
        }
        if (MaxAngle < -100)
        {
            MaxAngle = -100;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive)
        {
            _currentAngle.z = AngleOscillation(_time);
            transform.eulerAngles = _currentAngle;
            _time += Time.deltaTime;
            if (_time > SwingTime)
            {
                _time = 0;
            }
        }
        /*
        if (!_isOffScreen && !StaticTools.IsPointOnScreenWithBuffer(transform.position,0.5f))
        {
            _isOffScreen = true;
            _timeWhentOffScreen = Time.realtimeSinceStartup;
        }
        if (_isOffScreen && StaticTools.IsPointOnScreenWithBuffer(transform.position, 0.5f))
        {
            _isOffScreen = false;
            _time = (Time.realtimeSinceStartup - _timeWhentOffScreen) % SwingTime;
        }
        if (!_isOffScreen && _isActive)
        {
            _currentAngle.z = AngleOscillation(_time);
            transform.eulerAngles = _currentAngle;
            _time += Time.deltaTime;
            if (_time > SwingTime)
            {
                _time = 0;
            }
        }
        */
    }

    private float AngleOscillation(float time)
    {
        return MaxAngle * Mathf.Cos((2 * Mathf.PI / SwingTime) * time);
    }

    public void Activate(GameObject activator)
    {
        _isActive = true;
    }

    public void Deactivate(GameObject activator)
    {
        _isActive = false;
    }
}
