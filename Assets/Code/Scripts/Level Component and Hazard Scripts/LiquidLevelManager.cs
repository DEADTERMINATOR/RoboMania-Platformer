﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using LevelSystem;
using LevelSystem.SateData;
using UnityEngine.Experimental.Rendering.Universal;

[System.Serializable]
public class LiquidLevelChahgedEvent : UnityEvent<float>
{
}
[System.Serializable]
public class LiquidLevel
{
    public float SinkAmount;
    public float MaxZRotationOnSink = 0;
    public Color DebugColour;

}
//Liquid Level Manage
public class LiquidLevelManager : MonoBehaviour, IActivatable
{

    public string StateName;
    /// <summary>
    /// The Transform of the of the game object holding the lava mesh
    /// </summary>
    public Transform LiquidTrasform;
    /// <summary>
    /// The Lava Trigger
    /// </summary>
    public BoxCollider2D LiquidTrigger;
    /// <summary>
    /// ALl the leveles this will fill/drain to need at least
    /// </summary>
    public LiquidLevel[] Levels;

    public int LevelIndex = 0;
    /// <summary>
    /// How far doe it move
    /// </summary>
    private float _sinkAmount = 10;
    /// <summary>
    /// IF in dummy mode during update 
    /// </summary>
    public bool DummyMode = false;
    /// <summary>
    /// Time for the easing to run if selected
    /// </summary>
    private float _moveRunTime = 5;
    /// <summary>
    /// Set the easign in editor
    /// </summary>
    public Easing.EaseType EasingAlgo = Easing.EaseType.Linear;

    /// <summary>
    /// The easing function ref
    /// </summary>
    private Easing.EasingFunction _easeFunction;

    bool _active = true;

    public float GetYHeightOfFluid { get { return LiquidTrigger.bounds.max.y; } }

    public bool Active { get { return _active; } }

    public bool PrintDebug = false;



#if UNITY_EDITOR

    public bool DrawGizmo = false;

    public bool MoveMode = false;
#endif

    public enum States { INIT, MOVING, ATLEVLE }
    private States _currentState = States.INIT;
    public float MaxZRotationOnSink = 0.1f;


    public UnityEvent<float> OnLevelChahged = new LiquidLevelChahgedEvent();
    public UnityEvent OnStarted = new UnityEvent();
    public UnityEvent OnComplete = new UnityEvent();
    private int _nextLevelIndex;
    private float _fxRunTime = 0;
    private Vector3 _meshTarget;
    private Vector3 _triggerTarget;
    private float _boundYOffset = 0;
    private Vector3 _lavaStart;
    private Vector3 _lavaMeshZeroPos;
    private Vector3 _lavaTriggerZeroPos;
    private Vector3 _triggerStart;
    private float _currentSinkAmount = 0;


    /// <summary>
    /// Hoisted vars to save per frame me allocations
    /// </summary>
    private Vector3 _NewPos;
    private Vector3 _NewTrasFromPos;

    private LevelStateInt LSBIsAtMinLevel;

    // Use this for initialization
    void Start()
    {
        _easeFunction = Easing.GetFuctionFromEnum(EasingAlgo);
        _lavaMeshZeroPos = LiquidTrasform.position;
        _lavaTriggerZeroPos = LiquidTrigger.transform.position;
        if (string.IsNullOrEmpty(StateName))
        {
            StateName = gameObject.name;
        }
  
        LSBIsAtMinLevel = Level.CurentLevel.LevelState.LoadLevelStateInt(StateName, LevelIndex);
        LSBIsAtMinLevel.AddOnChangedEventListener(OnLevelStateChanged);
        _boundYOffset = LiquidTrasform.position.y - LiquidTrigger.transform.position.y;
        _fxRunTime = 300 + _moveRunTime;

        if (!DummyMode)
        {
            ChangeLevelInstant(LevelIndex);
        }
        _currentState = States.ATLEVLE;
    }

    // Update is called once per frame
    void Update()
    {
        if (!DummyMode)
        {
            //VisualDebug.DrawBox(_lavaMeshZeroPos, LiquidTrigger.bounds.extents, Quaternion.identity, Color.yellow, 1);
            if (_active && _currentState == States.MOVING)
            {
                LevelUpdate();
            }
        }

    }


    /// <summary>
    /// This is used to allow other script to move the level
    /// </summary>
    public void ExternalLevelMove(float amount)
    {
        _NewPos = _lavaStart + Vector3.up * amount;
        float deltaY = (_NewPos - LiquidTrasform.transform.position).y;
        if (OnLevelChahged != null && deltaY != 0)
        {
            OnLevelChahged?.Invoke(deltaY);
        }
        LiquidTrasform.transform.position = _NewPos;
        
       //VisualDebug.DrawBox(LiquidTrigger.bounds.center, LiquidTrigger.bounds.extents, Quaternion.identity, Color.red);
    }


    /// <summary>
    /// if a the level index is valid Start a level change. Retrun true if it was started and false if it could not 
    /// </summary>
    /// <param name="level"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    public bool  StartChangeLevel(int level, float time =30)
    {
        if(_nextLevelIndex == level)//we are allredy doin this
        {
            return false;
        }
        _nextLevelIndex = level;
        //is not moving and is not the same index the we are at and is valid
        if (_currentState != States.MOVING && _nextLevelIndex != LevelIndex && _nextLevelIndex >= 0 && _nextLevelIndex < Levels.Length)
        {   
            if (PrintDebug)
            {
                Debug.Log("LNUmb:" + level);
            }
            if (Levels[_nextLevelIndex].SinkAmount != 0)
            {

                _sinkAmount = Levels[_nextLevelIndex].SinkAmount ;
                if (PrintDebug)
                {
                    Debug.Log(name);
                    Debug.Log(_sinkAmount);
                    Debug.Log("_____________________________________________________________________");
                }
                _meshTarget = _lavaMeshZeroPos - new Vector3(0, _sinkAmount, 0);
                _triggerTarget = _lavaTriggerZeroPos - new Vector3(0, _sinkAmount, 0);
            }
            else// zero is just undo all skink
            {
                _sinkAmount = 0;
                _meshTarget = _lavaMeshZeroPos;
                _triggerTarget = _lavaTriggerZeroPos;
            }

           
            _moveRunTime = time;
            MaxZRotationOnSink = Levels[_nextLevelIndex].MaxZRotationOnSink;
            _lavaStart = LiquidTrasform.position;
            _triggerStart = LiquidTrigger.transform.position;

          //  VisualDebug.DrawBox(LiquidTrigger.bounds.center - new Vector3(0, _sinkAmount, 0), LiquidTrigger.bounds.extents, Quaternion.identity, Color.green, 100);

            _currentState = States.MOVING;
            _fxRunTime = 0;

            return true;
        }
        return false;
    }
  

    // the update function only run in he moving state
    private void LevelUpdate()
    {
          if (_fxRunTime <= _moveRunTime)
        {

            //calac now mesh pos 
            float EasedTime = _easeFunction(_fxRunTime / _moveRunTime);
            Vector3 last = LiquidTrasform.transform.position;
           
            LiquidTrasform.transform.position = new Vector3(LiquidTrasform.position.x, Mathf.Lerp(_lavaStart.y, _meshTarget.y, EasedTime), LiquidTrasform.position.z); 
            float deltaY = ( LiquidTrasform.transform.position.y - last.y);
            if (OnLevelChahged != null)
            {
                OnLevelChahged?.Invoke(deltaY);
            }
            //update fx time
            _fxRunTime += Time.fixedDeltaTime;
            if (_fxRunTime >= _moveRunTime)
            {
                _currentState = States.ATLEVLE;
                LevelIndex = _nextLevelIndex;
                OnComplete?.Invoke();
            }
        }
    }


    /// <summary>
    /// Set to a level immediately 
    /// </summary>
    /// <param name="index"></param>
    public void ChangeLevelInstant(int index)
    {
        if (index >= 0 && index < Levels.Length && LevelIndex != index)
        {

            if (PrintDebug)
            {
                Debug.Log("ISTA Level set to :" + index);
            }
            _nextLevelIndex = index;
            //get data
            LevelIndex = index;
            _sinkAmount = Levels[_nextLevelIndex].SinkAmount;
            //Set targets
            _lavaStart = LiquidTrasform.position;
            _triggerStart = LiquidTrigger.transform.position;
            _meshTarget = _lavaMeshZeroPos - new Vector3(0, _sinkAmount, 0);
            _triggerTarget = _lavaTriggerZeroPos - new Vector3(0, _sinkAmount, 0);
            //move now
            float deltaY = (LiquidTrasform.transform.position - _meshTarget).y;
            Debug.Log(deltaY);
            // LiquidTrigger.size = new Vector2(LiquidTrigger.size.x, LiquidTrigger.size.y - (_sinkAmount/2));
           // LiquidTrigger.transform.position = _triggerTarget;
            LiquidTrasform.transform.position = _meshTarget;
            //update state
            LevelIndex = _nextLevelIndex;

            _currentSinkAmount = _sinkAmount;
            _currentState = States.ATLEVLE;
            OnLevelChahged?.Invoke(_sinkAmount);
        }

    }
    private void OnLevelStateChanged(int value)
    {

        if (value != LevelIndex && value >= 0 && value < Levels.Length)
        {
            ChangeLevelInstant(value);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Vector3 leftPoint = new Vector3(LiquidTrigger.bounds.min.x, LiquidTrigger.bounds.max.y);
        Vector3 rightPoint = new Vector3(LiquidTrigger.bounds.max.x, LiquidTrigger.bounds.max.y);
        for (int i = 0; i < Levels.Length; i++)
        {
            Vector3 sinkAmount = new Vector3(0, Levels[i].SinkAmount, 0);
            Gizmos.color = Levels[i].DebugColour;
            Gizmos.DrawLine(leftPoint - sinkAmount, rightPoint - sinkAmount);
        }

    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "LiquidManager.png", true);
#if UNITY_EDITOR
        if (DrawGizmo)
        {
            if (!DummyMode)
            {

                Vector3 leftPoint = new Vector3(LiquidTrigger.bounds.min.x, LiquidTrigger.bounds.max.y);
                Vector3 rightPoint = new Vector3(LiquidTrigger.bounds.max.x, LiquidTrigger.bounds.max.y);
                for (int i = 0; i < Levels.Length; i++)
                {
                    Vector3 sinkAmount = new Vector3(0, Levels[i].SinkAmount, 0);
                    Gizmos.color = Levels[i].DebugColour;
                    Gizmos.DrawLine(leftPoint - sinkAmount, rightPoint - sinkAmount);
                }
            }
        }
#endif
    }



    public void Activate(GameObject activator)
    {
        _active = true;
    }

    public void Deactivate(GameObject activator)
    {
        _active = false;
    }



    private float FlipFlopLrep(float min, float max, float time)
    {
        return (time < 0.5f) ? Mathf.Lerp(min, max, time * 2) : Mathf.Lerp(max, min, (time - 0.5f) * 2);
    }

}
