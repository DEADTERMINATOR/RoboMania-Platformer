﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalData;

/// <summary>
/// An trigger volume that instantly kills the player when they enter it.
/// </summary>
public class DeathZone : MonoBehaviour, IDamageGiver
{
    /// <summary>
    /// Handle a colider entering the deth zone colider
    /// </summary>
    /// <param name="collision"></param>
    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        //Make sure we are not moving the player to a new zone
        if (TheGame.GetRef.gameState != TheGame.GameState.TRANSITION)
        {
            //if the colider is the player or the players curent shield
            if (StaticTools.IsPlayerOrSheildTagedObj(collision.gameObject))
            {
                if (!GlobalData.Player.State.Dead)
                    collision.gameObject.GetComponent<IDamageReceiver>().TakeDamage(this, 5000, DamageType.ENVIROMENT, Vector3.zero);
            }   
        }
    }
}
