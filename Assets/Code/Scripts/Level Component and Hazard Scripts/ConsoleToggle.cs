﻿using Characters.Player;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleToggle : MonoBehaviour, IActivatable
{
    public bool Active { get { return _isPowered; } }


    /// <summary>
    /// Whether the console is receiving power. Will only be relevant to consoles that are powered by some external system (e.g. another console), otherwise this should be set to true (or the console won't function at all).
    /// </summary>
    [SerializeField]
    private bool _isPowered = true;

    /// <summary>
    /// Whether the console is turned on. This is separate from a console being powered. It's possible for a console to be powered, but turned off and thus not powering all or some of the lines connected to it.
    /// </summary>
    [SerializeField]
    private bool _isTurnedOn;

    [SerializeField]
    private List<PowerLine> _initiallyActivatedLines = new List<PowerLine>();
    [SerializeField]
    private List<PowerLine> _initiallyDeactivatedLines = new List<PowerLine>();

    [SerializeField]
    private bool _isTimeLimited;
    [SerializeField]
    private float _timeLimit;

    [SerializeField]
    private bool _hasDifferentAssetsPerState;
    [SerializeField]
    private GameObject _consoleOn;
    [SerializeField]
    private GameObject _consoleOff;
    [SerializeField]
    private GameObject _consoleNoPower;

    private List<PowerLine> _powerLines = new List<PowerLine>();

    private float _currentTime;
    private bool _alreadyRespondedToInput = false;

    private PlayerInput _inputRef;


    private void Awake()
    {
        _inputRef = GlobalData.Player.Input;
    }

    private void Start()
    {
        foreach (PowerLine line in _initiallyActivatedLines)
        {
            if (_isPowered)
            {
                if (!line.Active)
                {
                    line.SupplyPower();
                }
                line.Activate(gameObject);
            }
            else if (!_isPowered)
            {
                line.KillPower(); //Despite not being initially powered, the line may be coloured to its initial active state in the editor for clarity, so we need to make sure this isn't reflected when the game is running.
            }

            line.InitiallyTurnedOn = true;
            _powerLines.Add(line);
        }

        foreach (PowerLine line in _initiallyDeactivatedLines)
        {
            if (_isPowered && !line.Active)
            {
                line.SupplyPower();
            }
            else if (!_isPowered)
            {
                line.KillPower();
            }

            line.InitiallyTurnedOn = false;
            _powerLines.Add(line);
        }

        if (_isTurnedOn)
        {
            Activate(gameObject);
        }
    }

    private void Update()
    {
        if (_isTimeLimited && _isTurnedOn)
        {
            _currentTime += Time.deltaTime;
            if (_currentTime >= _timeLimit)
            {
                _isTurnedOn = false;
                Deactivate(gameObject);
            }
        }
    }

    private void OnTriggerStay2D()
    {
        if (_inputRef.InputDictionary["Action"].IsDownOrHeld && !_alreadyRespondedToInput)
        {
            _isTurnedOn = !_isTurnedOn;
            if (_isTurnedOn)
            {
                Activate(gameObject);
            }
            else
            {
                Deactivate(gameObject);
            }

            _alreadyRespondedToInput = true;
        }

        if (_inputRef.InputDictionary["Action"].IsUpOrReleased && _alreadyRespondedToInput)
        {
            _alreadyRespondedToInput = false;
        }
    }

    public void Activate(GameObject activator)
    {
        if (activator != gameObject)
        {
            _isPowered = true;
            _isTurnedOn = false;

            ResetPowerLinesToInitialStates();

            if (_hasDifferentAssetsPerState)
            {
                SetCorrectConsoleAsset();
            }
        }
        else if (activator == gameObject && _isPowered)
        {
            TogglePowerLines();

            if (_hasDifferentAssetsPerState)
            {
                SetCorrectConsoleAsset();
            }
        }
    }

    public void Deactivate(GameObject deactivator)
    {
        if (deactivator != gameObject)
        {
            _isPowered = false;
            _isTurnedOn = false;

            KillPowerToAllLines();

            if (_hasDifferentAssetsPerState)
            {
                SetCorrectConsoleAsset();
            }
        }
        else if (deactivator == gameObject && _isPowered)
        {
            TogglePowerLines();
            _currentTime = 0;

            if (_hasDifferentAssetsPerState)
            {
                SetCorrectConsoleAsset();
            }
        }
    }

    private void KillPowerToAllLines()
    {
        for (int i = 0; i < _powerLines.Count; ++i)
        {
            _powerLines[i].KillPower();
        }
    }

    private void ResetPowerLinesToInitialStates()
    {
        for (int i = 0; i < _powerLines.Count; ++i)
        {
            var currentLine = _powerLines[i];

            if (!currentLine.Active)
            {
                currentLine.SupplyPower();
            }

            if (currentLine.InitiallyTurnedOn && !currentLine.IsTurnedOn)
            {
                currentLine.Activate(gameObject);
            }
        }
    }

    /// <summary>
    /// Toggles the attached power lines on or off, based on their current state.
    /// </summary>
    private void TogglePowerLines()
    {
        for (int i = 0; i < _powerLines.Count; ++i)
        {
            if (!_powerLines[i].IsTurnedOn)
            {
                _powerLines[i].Activate(gameObject);
            }
            else
            {
                _powerLines[i].Deactivate(gameObject);
            }
        }
    }

    /// <summary>
    /// Sets the currently active console asset to the one that represents the current state of the console (on, off, or optionally, no power).
    /// </summary>
    private void SetCorrectConsoleAsset()
    {
        if (_isPowered)
        {
            if (_isTurnedOn)
            {
                _consoleOn.SetActive(true);
                _consoleOff.SetActive(false);
                if (_consoleNoPower != null)
                {
                    _consoleNoPower.SetActive(false);
                }
            }
            else
            {
                _consoleOn.SetActive(false);
                _consoleOff.SetActive(true);
                if (_consoleNoPower != null)
                {
                    _consoleNoPower.SetActive(false);
                }
            }
        }
        else
        {
            _consoleOn.SetActive(false);
            _consoleOff.SetActive(false);
            if (_consoleNoPower != null)
            {
                _consoleNoPower.SetActive(true);
            }
        }
    }
}
