﻿using UnityEngine;

public class HazardMove : MonoBehaviour , IActivatable
{
    public Vector3 Start;
    public Vector3 End;
    public float Speed;
    public float StartPrecentage = 0f;

    public bool Active { get { return _active; } }
    

    [SerializeField]
    private bool _active;
    private Vector3 _zeroPos;
    private bool movingToEnd = true;


    public void Activate(GameObject activator)
    {
        _active = true;
    }

    public void Deactivate(GameObject activator)
    {
        _active = false;
    }

    private void Awake()
    {
        _zeroPos = transform.position;
        End = _zeroPos + End;
        Start = _zeroPos + Start;
        if(StartPrecentage >0 && StartPrecentage <1)
        {
            float XDistance = End.x - Start.x;
            float YDistance = End.y - Start.y;
            transform.position = Start + new Vector3(Mathf.Abs(XDistance * StartPrecentage), Mathf.Abs(YDistance * StartPrecentage));
        }
        else if(StartPrecentage == 1)
        {
            movingToEnd = false;
            transform.position = End;
        }

    }
    private void Update()
    {

        Vector3 target = End;
        if(!movingToEnd)
        {
            target = Start;
        }
        transform.position = Vector3.MoveTowards(transform.position, target, Speed * Time.deltaTime);
        if(transform.position == target)
        {
            movingToEnd = !movingToEnd;
        }
    }

    private void OnDrawGizmos()
    {
        VisualDebug.DrawGizmoPoint(transform.position+Start, Color.green);
        VisualDebug.DrawGizmoPoint(transform.position+End, Color.red);
    }
}