﻿using System;
using System.Collections;
using UnityEngine;

 public class BasicDoor : MonoBehaviour , IActivatable
 {
    public Vector3 OpenPosition;
    public GameObject Door;
    public float ActionTime = 10;
    public bool StartOpen = false;
    
    public bool Active { get { return isOpening || isOpen; } }

    protected bool isOpen = false;
    protected bool isOpening = false;

    private Vector3 ClosedPosition;


    protected IEnumerator Open()
    {
        isOpening = true;
        float t = 0;
        while(t < ActionTime)
        {
            Door.transform.position = Vector3.Lerp(ClosedPosition,OpenPosition, Easing.Quadratic.In(t / ActionTime));
            yield return new WaitForEndOfFrame();
            t += Time.deltaTime;
        }
        isOpen = true;
        isOpening = false;
    }
    public void OpenNow()
    {
        if (!isOpening)
        {
            StartCoroutine(Open());
        }
    }
    public void CloseNow()
    {
        if (!isOpening)
        {
            StartCoroutine(Open());
        }
    }
    protected IEnumerator Close()
    {
        isOpening = true;
        float t = 0;
        while (t < ActionTime)
        {
            Door.transform.position = Vector3.Lerp(OpenPosition, ClosedPosition, Easing.Quadratic.In(t / ActionTime));
            yield return new WaitForEndOfFrame();
            t += Time.deltaTime;
        }
        isOpen = false;
        isOpening = false;
    }

    public void Awake()
    {
        ClosedPosition = Door.transform.position;
        OpenPosition += ClosedPosition;
        if(StartOpen)
        {
            OpenNow();
        }
    }


    public virtual void Activate(GameObject activator)
    {

        OpenNow();
    }

    public virtual void Deactivate(GameObject activator)
    {
        CloseNow();
    }
}

