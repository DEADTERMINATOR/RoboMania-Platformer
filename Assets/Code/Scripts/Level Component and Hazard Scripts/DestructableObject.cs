﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;

public class DestructableObject : MonoBehaviour, IDamageReceiver, IActivatable
{
    private const float PRESSURE_HEALTH_DRAIN_PER_SECOND = 25f;
    private const float PRESSURE_HEALTH_DRAIN_PER_VELOCITY_UNIT = 10f;
    private const float GRAVITY_SCALE_MULTIPLIER_WHEN_DESTROYED_BY_YVELOCITY = 2f;

    public delegate void DestructionTriggered(DestructableObject destructabeObj);
    public event DestructionTriggered Destroyed;

    public enum DamageTypes { Projectile, Pressure, LevelEvent };

    public bool Active { get { return _trigger; } }

    #region Serialized Properties
    [SerializeField]
    private List<DamageTypes> _acceptableDamageTypes = new List<DamageTypes>();

    [SerializeField]
    private bool _yVelocityCanDealExtraPressureDamage;

    [SerializeField]
    private LayerMask _layersThatCanApplyDamage;

    [SerializeField]
    private float _health;

    [SerializeField]
    private Collider2D _collider;

    [Tooltip("You can leave this empty if the obstacle collider (the one the Player collides with) and the collider that accepts damage are the same")]
    [SerializeField]
    private Collider2D _obstacleCollider;

    [SerializeField]
    private bool _shouldExplodeOnDestruction;

    [SerializeField]
    private List<GameObject> _deactivateOnDestruction = new List<GameObject>();

    [Tooltip("Any objects assigned to this list will receive the same damage as this object.")]
    [SerializeField]
    private List<DestructableObject> _mirroredDestructableObjects = new List<DestructableObject>();

    [SerializeField]
    private GameObject _platform;

    [SerializeField]
    private GameObject _cracks;

    [SerializeField]
    private GameObject _pieces;

    [SerializeField]
    private GameObject _particles;

    [Header("How Long Until It Collapses")]
    [SerializeField]
    private float _cracksTimer;
    #endregion

    #region Private Variables
    private bool _trigger = false;
    private float _currentHealth;
    private WaitForSeconds _waitCracksTimer;
    private bool _brokenViaYVelocity = false;

    private bool _pressureCanDamage = false;
    private bool _projectilesCanDamage = false;
    private bool _levelEventCanDamage = false;
    #endregion

    #region Unity Methods
    private void Start()
    {
        _currentHealth = _health;
        _waitCracksTimer = new WaitForSeconds(_cracksTimer);

        foreach (DamageTypes type in _acceptableDamageTypes)
        {
            if (type == DamageTypes.Pressure)
            {
                _pressureCanDamage = true;
            }
            else if (type == DamageTypes.Projectile)
            {
                _projectilesCanDamage = true;
            }
            else if (type == DamageTypes.LevelEvent)
            {
                _levelEventCanDamage = true;
            }
        }
    }

    private void Update()
    {
        if (_trigger == true)
        {
            StartCoroutine(TriggerObject());
            _trigger = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        DamageOnPressure(collision.gameObject);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        DamageOnPressure(collision.gameObject);
    }
    #endregion

    public void Activate(GameObject activator)
    {
        if (_levelEventCanDamage)
        {
            _currentHealth = 0;
            _trigger = true;
        }
    }

    public void Deactivate(GameObject activator) { }

    public void TakeDamage(IDamageGiver giver, float amount, GlobalData.DamageType type, Vector3 hitPoint)
    {
        if (_projectilesCanDamage && (type == GlobalData.DamageType.PROJECTILE || type == GlobalData.DamageType.MELEE))
        {
            _currentHealth -= amount;
            if (_currentHealth <= 0)
            {
                _trigger = true;
            }

            foreach (DestructableObject destructableObject in _mirroredDestructableObjects)
            {
                destructableObject.DamageOnProjectileHit(amount);
            }
        }
    }

    private void DamageOnPressure(GameObject collidingGameObject)
    {
        if (_pressureCanDamage && StaticTools.IsLayerInLayerMask(_layersThatCanApplyDamage, collidingGameObject.layer))
        {
            if (_yVelocityCanDealExtraPressureDamage &&
                ((StaticTools.IsLayerInLayerMask(_layersThatCanApplyDamage, GlobalData.PLAYER_LAYER) && collidingGameObject.layer == GlobalData.PLAYER_LAYER)
                || (StaticTools.IsLayerInLayerMask(_layersThatCanApplyDamage, GlobalData.ENEMY_LAYER) && collidingGameObject.layer == GlobalData.ENEMY_LAYER)))
            {
                var character = collidingGameObject.GetComponent<Character>();

                //We want to do extra damage if the player or enemy landed on this pressure destructable object with some Y-velocity (i.e. they landed on it from a height).
                float damageFromYVelocity = Mathf.Abs(character.Velocity.y) * PRESSURE_HEALTH_DRAIN_PER_VELOCITY_UNIT;

                _currentHealth -= damageFromYVelocity;
                foreach (DestructableObject destructableObject in _mirroredDestructableObjects)
                {
                    destructableObject.DamageOnPressure(damageFromYVelocity, true);
                }

                if (_currentHealth <= 0)
                {
                    _brokenViaYVelocity = true;
                    _trigger = true;
                    return;
                }
            }

            float damage = PRESSURE_HEALTH_DRAIN_PER_SECOND * Time.deltaTime;
            _currentHealth -= damage;

            if (_currentHealth <= 0)
            {
                _trigger = true;
            }

            foreach (DestructableObject destructableObject in _mirroredDestructableObjects)
            {
                destructableObject.DamageOnPressure(damage, false);
            }
        }
    }

    private void DamageOnPressure(float amount, bool damageFromYVelocity)
    {
        if (_pressureCanDamage)
        {
            _currentHealth -= amount;
            if (_currentHealth <= 0)
            {
                if (damageFromYVelocity)
                {
                    _brokenViaYVelocity = true;
                }
                _trigger = true;
            }
        }
    }

    private void DamageOnProjectileHit(float amount)
    {
        if (_projectilesCanDamage)
        {
            _currentHealth -= amount;
            if (_currentHealth <= 0)
            {
                _trigger = true;
            }
        }
    }

    private IEnumerator TriggerObject()
    {
        _cracks?.SetActive(true);
        _particles?.SetActive(true);

        if (!_brokenViaYVelocity)
        {
            yield return _waitCracksTimer;
        }

        _cracks?.SetActive(false);
        if (_collider != null)
        {
            _collider.enabled = false;
        }

        _platform?.SetActive(false);

        _pieces?.SetActive(true);
        if (_brokenViaYVelocity)
        {
            for (int i = 0; i < _pieces.transform.childCount; ++i)
            {
                _pieces.transform.GetChild(i).GetComponent<Rigidbody2D>().gravityScale *= GRAVITY_SCALE_MULTIPLIER_WHEN_DESTROYED_BY_YVELOCITY;
            }
        }

        if (_shouldExplodeOnDestruction)
        {
            Explode[] explodePieces = _pieces.GetComponentsInChildren<Explode>();
            foreach (Explode explode in explodePieces)
            {
                explode.ApplyExplosionProperties();
            }
        }

        _collider.enabled = false;
        if (_obstacleCollider != null)
        {
            _obstacleCollider.enabled = false;
        }

        foreach (GameObject obj in _deactivateOnDestruction)
        {
            obj.SetActive(false);
        }

        Destroyed?.Invoke(this);
        //TODO: show particles
    }
}
