﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteSpawner : MonoBehaviour, IActivatable
{
    public GameObject ObjectToSpawn;
    public float TimeBetweenSpawns;

    public bool Active { get { return _active; } }


    [SerializeField]
    private bool _active;
    private List<GameObject> _spawnedObjects = new List<GameObject>();
    private float _timeSinceLastSpawn = 0;


    private void Start()
    {
        _timeSinceLastSpawn = TimeBetweenSpawns;
    }

    private void Update()
    {
        if (_active)
        {
            _timeSinceLastSpawn += Time.deltaTime;
            if (_timeSinceLastSpawn >= TimeBetweenSpawns)
            {
                GameObject spawnedObject = Instantiate(ObjectToSpawn, transform.position, Quaternion.identity);
                _spawnedObjects.Add(spawnedObject);
                _timeSinceLastSpawn = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (_active && _spawnedObjects.Contains(collider.gameObject))
        {
            _spawnedObjects.Remove(collider.gameObject);
            Destroy(collider.gameObject);
        }
    }

    public void Activate(GameObject activator)
    {
        _active = true;
    }

    public void Deactivate(GameObject activator)
    {
        _active = false;
    }
}
