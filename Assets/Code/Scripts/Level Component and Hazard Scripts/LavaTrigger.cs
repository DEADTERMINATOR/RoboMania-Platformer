﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(DamageOverTime))]
public class LavaTrigger : MonoBehaviour
{
    /// <summary>
    /// THE SFX played when somthing hits the lava 
    /// </summary>
    public ParticleSystem splashFX;
    /// <summary>
    /// The SFX that is played while somtinh is in the lava 
    /// </summary>
    public GameObject MovingFX;
    public ParticleSystem BubblesFX;

    /// <summary>
    /// How long the splash FX is Played for;
    /// </summary>
    public  float SplashFxPlayTime = 3;
    public MeshRenderer LavaMeshRendere;
    public bool IsMovingLava = false;
    public DamageOverTime DamageOverTime;
    public bool DoPlayerDisplaceSFX = false;
    public LavaShaderController LavaShaderController;

    private int _DisplamentXPointId = Shader.PropertyToID("_DisplamentXPoint");
    private int _DisplacmentAmountID = Shader.PropertyToID("_DisplacmentAmount");
    private int _waveScaleID = Shader.PropertyToID("_waveScale");
    private bool _displamentCoroutineRunning = false;
    // Use this for initialization
    private void Start ()
    {
        if (splashFX != null)
        {
            splashFX.Stop();
        }
        MovingFX.SetActive(false);
        if(DamageOverTime == null)
        {
            Debug.Log(gameObject.name + "Is missing DamageOverTime script");
        }
        DamageOverTime.OnPlayerEnter.AddListener(OnPlayerEnter);
        DamageOverTime.OnPlayerEnter.AddListener(OnPlayerExit);
    }

    protected void Update()
    {
        if(IsMovingLava)
        {
            if(BubblesFX)
            { 
            }
        }

        if (GlobalData.Player.State != null &&  GlobalData.Player.State.InLava)
        {
            if (MovingFX)
                MovingFX.transform.position = GlobalData.Player.gameObject.transform.position; 
        }
       
    }


    public void OnPlayerEnter()
    {

        if (splashFX != null)
        {
            splashFX.transform.position = GlobalData.Player.gameObject.transform.position;
            splashFX.Play();
        }
        BubblesFX.transform.position = new Vector3(GlobalData.Player.gameObject.transform.position.x + 0.5f, splashFX.transform.position.y, splashFX.transform.position.z);
        BubblesFX.Play();
        if (DoPlayerDisplaceSFX)
        {
            StartPlayerSplash();
        }
        MovingFX.SetActive(true);
        if (splashFX != null)
        {
            splashFX.transform.position = new Vector3(GlobalData.Player.gameObject.transform.position.x + 0.5f, splashFX.transform.position.y, splashFX.transform.position.z);

        }
    }
  

    public  void OnPlayerExit()
    {
        MovingFX.SetActive(false);
    }

    private void StartPlayerSplash()
    {
        // Vector3 screenpos = Camera.current.WorldToScreenPoint(GlobalData.PlayerTransfrom.position);
        

        float displamentPoint =  Mathf.Clamp01((GlobalData.PlayerTransfrom.position.x - LavaMeshRendere.bounds.min.x) / (LavaMeshRendere.bounds.max.x - LavaMeshRendere.bounds.min.x));
        LavaShaderController.MaterialPropertyBlock.SetFloat(_DisplamentXPointId, displamentPoint);
        //Debug.Log(displamentPoint);
        if (!_displamentCoroutineRunning)
        {
            _displamentCoroutineRunning = true;
            StartCoroutine(RunDisplace());
        }
    }


    IEnumerator RunDisplace()
    {
        float frames = 2;
        float baseWaveSalce = LavaShaderController.MaterialPropertyBlock.GetFloat(_waveScaleID);
          for (float i = 0; i <= frames; i++)
          {
              yield return new WaitForEndOfFrame();
              LavaShaderController.MaterialPropertyBlock.SetFloat(_DisplacmentAmountID, -(0.15f *(i/ frames)));
            LavaShaderController.MaterialPropertyBlock.SetFloat(_waveScaleID, Mathf.Lerp(baseWaveSalce, baseWaveSalce+5, (i / frames)) );

        }
       
        frames =4;
        for (float ii = 0; ii <= frames; ii++)
          {
              yield return new WaitForEndOfFrame();
             // Debug.Log (-(0.1f * (1f - (ii / frames))));
              LavaShaderController.MaterialPropertyBlock.SetFloat(_DisplacmentAmountID, (-0.15f * (1f- Easing.Bounce.Out(ii / frames))) );
          } 
        frames = 3;
        for (float ii = 0; ii <= frames; ii++)
        {
            yield return new WaitForEndOfFrame();
           // Debug.Log(-(0.1f * (1f - (ii / frames))));
            LavaShaderController.MaterialPropertyBlock.SetFloat(_DisplacmentAmountID, (0.1f * (Easing.Bounce.Out(ii / frames))));
        }
        for (float ii = 0; ii <= frames; ii++)
        {
            yield return new WaitForEndOfFrame();
           // Debug.Log(-(0.1f * (1f - (ii / frames))));
            LavaShaderController.MaterialPropertyBlock.SetFloat(_DisplacmentAmountID, (0.1f * (1f - Easing.Bounce.In(ii / frames))));
            LavaShaderController.MaterialPropertyBlock.SetFloat(_waveScaleID, Mathf.Lerp(baseWaveSalce, baseWaveSalce + 5, (ii / frames)));
        }
        _displamentCoroutineRunning = false;
    }
}
