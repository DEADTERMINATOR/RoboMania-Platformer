﻿using System;
using UnityEngine;
[RequireComponent(typeof(Collider2D))]
public class AutoDoor : BasicDoor
{
    public bool Locked;


    public override void Activate(GameObject activator)
    {
        Locked = !Locked;
    }
    public override void Deactivate(GameObject activator)
    {
        Locked = !Locked;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
         if (!Locked)
        {
            if (isOpen)
            {
                CloseNow();
            }
            else
            {
                OpenNow();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!Locked)
        {
            if (isOpen)
            {
                CloseNow();
            }
            else
            {
                OpenNow();
            }
        }
    }
}

