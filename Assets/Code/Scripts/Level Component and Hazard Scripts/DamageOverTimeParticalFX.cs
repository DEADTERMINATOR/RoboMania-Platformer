﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Characters;
using static GlobalData;

public class DamageOverTimeParticalFX : MonoBehaviour, IDamageGiver
{
    /// <summary>
    /// The tag the Dame ofer time will look for to dmage (defult is player)
    /// </summary>
    public LayerMask TragetsLarers = GlobalData.PLAYER_LAYER;

    public bool DamgeAll = false;
    /// <summary>
    /// Will only look for the set tag and not the player or shield
    /// </summary>
    public bool UseTag = false;

    /// <summary>
    /// The amount of damage per damage tick
    /// </summary>
    public float DamagePerTick;

    /// <summary>
    /// How often damage is applied in seconds
    /// </summary>
    public float DamageTickTime = 0.5f;

    /// <summary>
    /// Should the damage ramp up the longer the object is inside the trigger.
    /// </summary>
    public bool BuffDamageOvertime = false;

    /// <summary>
    /// How much of a buff is added on each hit (BuffDamageOvertime must be true)
    /// </summary>
    public float DamageBuffPerHit = 0.01f;


    /// <summary>
    /// The current amount of damage buff
    /// </summary>
    protected float _damageBuff = 1.0f;

    /// <summary>
    /// Time since last damage applied
    /// </summary>
    protected float _lastDamageTime;

    protected IDamageReceiver _damageReceiver;
    public struct DamageReciverData
    {
        public Character _Character;
        public float TimeLastSeen; 

      //  public DamageReciverData()

    }
    /// <summary>
    /// the resistred damageReceivers expting mosly look up so in frame 
    /// </summary>
    protected Dictionary<IDamageReceiver,float> _damageReceivers = new Dictionary<IDamageReceiver, float>();

    private float timeLastSeen = 0;
    private float _timeTillRest = 3;

    private float _timeLeavingTrigger = 0;

    private float _internalTime = 0;

    protected virtual void Update()
    {
        _internalTime += Time.deltaTime;
        if (_damageBuff > 1 && (_timeLeavingTrigger > 0 && _timeLeavingTrigger + _timeTillRest <= _internalTime))
        {
            _damageBuff = 1;
        }


        for (int i =0; i < _damageReceivers.Keys.Count; i++ )
        {
            if (_damageReceivers.ElementAt(i).Value + _timeTillRest < _internalTime)
            {
                _damageReceivers.Remove(_damageReceivers.Keys.ElementAt(i));
            }
        }
       
    }


    private void OnParticleCollision(GameObject other)
    {
        _damageReceiver = other.gameObject.GetComponent<IDamageReceiver>();

        if ((!UseTag && _damageReceiver != null) || (UseTag && TragetsLarers == (TragetsLarers | (1 << other.gameObject.layer)) ))
        {
            if (_damageReceivers.ContainsKey(_damageReceiver))
            {
                if (_internalTime >= _lastDamageTime + DamageTickTime)
                {
                    _damageReceiver.TakeDamage(this, DamagePerTick * _damageBuff, DamageType.ENVIROMENT, Vector3.zero);
                    _lastDamageTime = _internalTime;
                    if (BuffDamageOvertime)
                        _damageBuff += DamageBuffPerHit;
                }
                _damageReceivers[_damageReceiver] = _internalTime;
            }
            else
            {
                _damageReceivers.Add(_damageReceiver,_internalTime);
                _timeLeavingTrigger = 0;
                _damageReceiver.TakeDamage(this, DamagePerTick * _damageBuff, DamageType.ENVIROMENT, Vector3.zero);
                _lastDamageTime = _internalTime;
            }
        }
    }
}

