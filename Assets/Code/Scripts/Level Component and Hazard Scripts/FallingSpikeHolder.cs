﻿using UnityEngine;
using System.Collections;

public class FallingSpikeHolder : MonoBehaviour
{
    public GameObject SpikeGameObject;


    private Rigidbody2D SpikeRigidbody;


    // Use this for initialization
    void Start()
    {
        SpikeRigidbody = SpikeGameObject.GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SpikeRigidbody.simulated = true;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
