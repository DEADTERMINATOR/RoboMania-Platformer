﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalData;

public class GravityTrigger : MonoBehaviour
{
    /// <summary>
    /// The type of gravity this trigger activates for an object.
    /// </summary>
    public GravityType ActivateGravityType;

    /// <summary>
    /// The type of gravity this trigger returns to when the object exits.
    /// </summary>
    public GravityType DeactivateGravityType;


    /// <summary>
    /// Checks if what collided is something that is affected by gravity, and if it is, makes the necessary gravity modifications.
    /// </summary>
    /// <param name="collider">The collider that triggered the collision.</param>
    private void OnTriggerEnter2D(Collider2D collider)
    {
        HandleGravitySwitch(collider.gameObject);
    }

    /// <summary>
    /// Checks if what is exiting the trigger is affected by gravity, and if it is, restores gravity to what it was before.
    /// </summary>
    /// <param name="collider">The collider that is exiting.</param>
    private void OnTriggerExit2D(Collider2D collider)
    {
        HandleGravitySwitch(collider.gameObject);
    }

    /// <summary>
    /// Checks if what collided is something that is affected by gravity, and if it is, makes the necessary gravity modifications.
    /// The version for non-trigger colliders.
    /// </summary>
    /// <param name="collider">The collision that triggered the call.</param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        HandleGravitySwitch(collision.gameObject);
    }

    /// <summary>
    /// Checks if what is exiting the trigger is affected by gravity, and if it is, restores gravity to what it was before.
    /// The version for non-trigger colliders.
    /// </summary>
    /// <param name="collider">The exiting collision.</param>
    private void OnCollisionExit2D(Collision2D collision)
    {
        HandleGravitySwitch(collision.gameObject);
    }

    private void HandleGravitySwitch(GameObject collidedGameObject)
    {
        //Try to get a character component to see if a character hit the modified gravity trigger.
        IGravityObject gravityObject = collidedGameObject.GetComponent<IGravityObject>();

        if (gravityObject != null)
        {
            gravityObject.ChangeGravityType(DeactivateGravityType);
        }
    }
}
