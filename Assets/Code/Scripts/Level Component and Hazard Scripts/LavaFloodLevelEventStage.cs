﻿using UnityEngine;
using System.Collections;
using LevelEventStageSystem;
using System.Collections.Generic;
using System;

[Serializable]
public struct LiquidLevelSettings
{
    public LiquidLevelManager LiquidLevelManager;
    public float StartingOffset;
    public float EndingOffset;
}


public class LavaFloodLevelEventStage : LevelEventStage , IActivatable
{
    public List<LiquidLevelSettings> Settings;

    public bool Active { get { return !IsDone(); } }

    public override void RunStage(float EasedTime)
    {
        base.RunStage(EasedTime);
        foreach(LiquidLevelSettings setting in Settings)
        {
            setting.LiquidLevelManager.ExternalLevelMove(Mathf.Lerp(setting.StartingOffset, setting.EndingOffset, EasedTime));
        }
    }

    /// <summary>
    /// Go to the end of the stage for warping
    /// </summary>
    public void QuickEnd()
    {
        foreach (LiquidLevelSettings setting in Settings)
        {
            setting.LiquidLevelManager.ExternalLevelMove( setting.EndingOffset);
        }
        _intrenalTime = StateTime;
    }
    public override bool IsDone()
    {
        return base.IsDone();
    }

    public void Activate(GameObject activator)
    {
        if (!IsDone())
        {
            QuickEnd();
        }
    }

    public void Deactivate(GameObject activator)
    {
        
    }
}
