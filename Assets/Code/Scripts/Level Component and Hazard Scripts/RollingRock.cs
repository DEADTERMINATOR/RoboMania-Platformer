﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class RollingRock : MonoBehaviour, IWorldKillable
{
    public ParticleSystem BlowUp;
    public UnityEvent<GameObject> OnRockDead = new GameObjectUnityEvent();
    public bool BlowUpOnObsticals = true;
    public bool BlowUpOnHazard = false;
    private new Rigidbody2D rigidbody;


    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        transform.root.gameObject.SetActive(true);
        BlowUp.Stop();

    }
    public void PreKillLogic()
    {
       
    }

    private void Update()
    {
        if (StaticTools.ThresholdApproximately(Vector2.zero, rigidbody.velocity, 0.1f))// if the rock is moving let it 
        {
             HitSomething();

        }
        if (BlowUpOnObsticals && StaticTools.IsPointOnScreen(transform.position))
        {
            if (rigidbody.IsTouchingLayers(GlobalData.OBSTACLE_LAYER_SHIFTED))
            {
                HitSomething();
            }
        }
    }

    public void HitSomething()
    {
        GetComponent<Rigidbody2D>().Sleep();
        if(OnRockDead != null)
        {
            OnRockDead.Invoke(gameObject);
        }
        ///BugFix: 
        transform.root.gameObject.SetActive(false);
        Destroy(transform.root.gameObject);
    }


}

public class GameObjectUnityEvent : UnityEvent<GameObject>
{

}