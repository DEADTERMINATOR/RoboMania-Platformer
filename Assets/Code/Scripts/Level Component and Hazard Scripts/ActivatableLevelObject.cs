﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelComponents
{
    public abstract class ActivatableLevelObject : MonoBehaviour, IActivatable
    {
        /// <summary>
        /// The vector2 that defines the size of the search rectangle that looks to determine if the player is close enough to the platform to operate normal functions.
        /// </summary>
        protected readonly Vector2 ACTIVATE_WHEN_PLAYER_IS_WITHIN_NUM_UNITS = new Vector2(250, 250);


        /// <summary>
        /// Public getter for whether the object is currently active and performing all its intended functionality.
        /// </summary>
        public bool Active { get { return activated; } }


        /// <summary>
        /// Whether the object is currently activated and performing all of its intended functionality.
        /// </summary>
        [SerializeField]
        protected bool activated = false;

        /// <summary>
        /// Whether this object should be activated and deactivated by an activator. If this is false, the object will be activated and deactivated based on how far away the player is.
        /// </summary>
        [SerializeField]
        protected bool activateWithActivator = false;

        /// <summary>
        /// Whether this object should be allowed to run its bare bones update (the update that runs when the object is deactivated).
        /// </summary>
        [SerializeField]
        protected bool runBareBonesUpdate = true;

        /// <summary>
        /// Whether this object shouldn't be deactivated once it's been activated (or if it starts active).
        /// </summary>
        [SerializeField]
        protected bool neverDeactivateOnceActive = false;

        /// <summary>
        /// A list of IActivatable objects that are to be activated when this level object is activated.
        /// </summary>
        [SerializeField]
        [RequireInterface(typeof(IActivatable))]
        protected List<Object> objectsToActivateWithLevelObject = new List<Object>();

        /// <summary>
        /// Whether the platform's functionality is currently "frozen". Level objects that are frozen perform none of their functions, unlike inactive objects which may still perform some bare bones functions.
        /// </summary>
        protected bool frozen = false;

        /// <summary>
        /// The rectangle that represents the search area for the player.
        /// </summary>
        private Rect _playerSearchRect;

        /// <summary>
        /// A single element array to hold the results of whether the overlap test found the player.
        /// </summary>
        private Collider2D[] _overlapTestResult = new Collider2D[1];


        protected virtual void Start()
        {
            _playerSearchRect = new Rect(transform.position, ACTIVATE_WHEN_PLAYER_IS_WITHIN_NUM_UNITS);
            GlobalData.Player.Dead += OnPlayerDead;
        }

        //The player could move several times per PhyicsUpdate (FixedUpdate) but we only need to tie this to visual updates
        protected virtual void Update()
        {
            if (!frozen)
            {
                if (!activateWithActivator)
                {
                    bool playerNearby = CheckIfPlayerIsNearby();
                    if (!activated && playerNearby)
                    {
                        ActivateImmediately();
                    }
                    else if (!neverDeactivateOnceActive && activated && !playerNearby)
                    {
                        DeactivateImmediately();
                    }
                }

                if (Active)
                {
                    FullUpdate();
                }
                else if (runBareBonesUpdate)
                {
                    BareBonesUpdate();
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            if (!frozen)
            {
                if (Active)
                {
                    FullFixedUpdate();
                }
                else if (runBareBonesUpdate)
                {
                    BareBonesFixedUpdate();
                }
            }
        }

        public void AddObjectToActivate(IActivatable objectToActivate)
        {
            var objectExists = objectsToActivateWithLevelObject.Find(x => x == (Object)objectToActivate);
            if (objectExists == null)
            {
                objectsToActivateWithLevelObject.Add((Object)objectToActivate);
            }
        }

        public bool RemoveObjectToActivate(IActivatable objectToActivate)
        {
            var removalSuccessful = objectsToActivateWithLevelObject.Remove((Object)objectToActivate);
            return removalSuccessful;
        }

        public virtual void Activate(GameObject activator)
        {
            if (activateWithActivator)
            {
                ActivateImmediately();
            }
        }

        public virtual void Deactivate(GameObject activator)
        {
            if (!neverDeactivateOnceActive && activateWithActivator)
            {
                DeactivateImmediately();
            }
        }

        /// <summary>
        /// Activates the level object immediately, regardless of how far away the player is or whether it has an associated activator that has not been hit.
        /// </summary>
        public void ActivateImmediately()
        {
            activated = true;

            foreach (IActivatable obj in objectsToActivateWithLevelObject)
            {
                obj.Activate(gameObject);
            }
        }

        /// <summary>
        /// Deactivates the level object immediately.
        /// </summary>
        public void DeactivateImmediately()
        {
            activated = false;

            foreach (IActivatable obj in objectsToActivateWithLevelObject)
            {
                obj.Deactivate(gameObject);
            }
        }

        protected virtual void SetFrozen(bool frozenStatus)
        {
            frozen = frozenStatus;
        }

        /// <summary>
        /// Searches the area within the defined player search rectangle for the player. If the player is not found,
        /// then we only need to do the bare bones in terms of updating this object.
        /// </summary>
        /// <returns></returns>
        protected virtual bool CheckIfPlayerIsNearby()
        {
            // We search an area around the object for the player. If he isn't found, don't waste the resources performing the full update.
            int numberOfResults = Physics2D.OverlapBoxNonAlloc(transform.position, _playerSearchRect.size, 0, _overlapTestResult, GlobalData.PLAYER_LAYER_SHIFTED);

            if (numberOfResults != 0)
            {
                return true;
            }
            return false;
        }

        protected virtual void OnPlayerDead()
        {
            SetFrozen(true);
            GlobalData.Player.Dead -= OnPlayerDead;
        }

        /// <summary>
        /// The minimum update that should be done regardless of whether the object has been activated.
        /// </summary>
        protected virtual void BareBonesUpdate() { }

        /// <summary>
        /// The minimum fixed update that should be done regardless of whether the object has been activated.
        /// </summary>
        protected virtual void BareBonesFixedUpdate() { }

        /// <summary>
        /// The full update that should be done only if the object has been activated.
        /// </summary>
        protected virtual void FullUpdate() { }

        /// <summary>
        /// The full fixed update that should be done only if the object has been activated.
        /// </summary>
        protected virtual void FullFixedUpdate() { }
    }
}
