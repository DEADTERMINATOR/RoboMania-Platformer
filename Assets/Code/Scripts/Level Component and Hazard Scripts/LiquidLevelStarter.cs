﻿using UnityEngine;

public class LiquidLevelStarter : MonoBehaviour, IActivatable
{
    public bool Active { get; set; }
    public int LevelIndex = 0;
    public float Time = 20;
    public LiquidLevelManager _manager;
    public bool Instant = false;
    public Easing Easing;
    public void Activate(GameObject activator)
    {
        if (!Instant)
        {
            _manager.StartChangeLevel(LevelIndex,Time);
        }
        else
        {
            _manager.ChangeLevelInstant(LevelIndex);
        }
    }


    public void Deactivate(GameObject activator)
    {
      //  throw new System.NotImplementedException();
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "LiquidManagerActivator.png", false);
    }
}

