﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;
using UnityEngine.Rendering;
using MovementEffects;
using System;

[RequireComponent(typeof(CollisionRaycaster2D))]
public class PlayerMoveableObject : MonoBehaviour, IGrabbable, IActivatable, IWarpable
{
    /* How much time should elapse before the player is allowed to switch from pushing to pulling, or vice versa.
     * This pause is intended to both better convey the weight of a moveable object,
     * as well as to prevent animation/functionality issues that can arise from swapping between the pushing and pulling to quickly. */
    private const float DESIRED_TIME_BETWEEN_MOVE_TYPE_SWITCH = 0.35f;

    /// <summary>
    /// The types of movement possible with a moveable object.
    /// </summary>
    public enum MoveType { Push, Pull, None };

    
    /// <summary>
    /// Whether the object is currently moveable.
    /// </summary>
    public bool CanMove = true;

    /// <summary>
    /// The last type of movement attempted against this object.
    /// </summary>
    public MoveType LastMoveType { get; private set; } = MoveType.None;

    public bool Active { get { return CanMove; } }


    /// <summary>
    /// The speed (in units per second) in which the object should be pushed or pulled.
    /// </summary>
    [SerializeField]
    private float _moveSpeed;

    private PlayerMaster _playerRef;
    private CollisionRaycaster2D _raycaster;
    private Rigidbody2D _rigidbody;

    /* The elapsed time since the last SUCCESSFUL movement */
    private float _timeSinceLastMovement = 0;


    // Start is called before the first frame update
    private void Start()
    {
        _playerRef = GlobalData.Player;

        _raycaster = GetComponent<CollisionRaycaster2D>();
        _raycaster.CalculateRaySpacing(true);

        _rigidbody = GetComponent<Rigidbody2D>();
        if (_rigidbody == null)
        {
            Debug.LogWarning("Player moveable object named " + gameObject.name + " does not have a Rigidbody2D attached to the object.");
        }
    }

    private void Update()
    {
        if (_playerRef.State.Grabbing)
        {
            _timeSinceLastMovement += Time.deltaTime;
        }
    }

    public void Grab()
    {
        _playerRef.MoveSpeed = _moveSpeed;
        _timeSinceLastMovement = 0;
    }

    public void Release()
    {
        _playerRef.MoveSpeed = _playerRef.Movement.CurrentActiveGravityState.BaseMoveSpeed;
        _rigidbody.velocity = Vector2.zero;
        LastMoveType = MoveType.None;
    }

    public bool Push(Vector2 direction)
    {
        LastMoveType = MoveType.Push;

        if (CanMove && (LastMoveType == MoveType.Push || LastMoveType == MoveType.None || (LastMoveType == MoveType.Pull && _timeSinceLastMovement >= DESIRED_TIME_BETWEEN_MOVE_TYPE_SWITCH)))
        {
            if (!Mathf.Approximately(_playerRef.MovementInput.x, 0))
            {
                float xDirection = Mathf.Sign(direction.x);
                _rigidbody.velocity = new Vector2(_moveSpeed * xDirection, 0);
                _timeSinceLastMovement = 0;

                return true;
            }
            else
            {
                _rigidbody.velocity = Vector2.zero;
            }
        }

        return false;
    }

    public bool Pull(Vector2 direction)
    {
        LastMoveType = MoveType.Pull;

        if (CanMove && (LastMoveType == MoveType.Pull || LastMoveType == MoveType.None || (LastMoveType == MoveType.Push && _timeSinceLastMovement >= DESIRED_TIME_BETWEEN_MOVE_TYPE_SWITCH)))
        {
            _raycaster.UpdateCastOrigins(true);

            float xDirection = Mathf.Sign(direction.x);
            float moveDistance = _moveSpeed * Time.deltaTime * xDirection;
            float shortestHitDistance = float.MaxValue;

            for (int i = 0; i < _raycaster.HorizontalRayCount; ++i)
            {
                _raycaster.SetCastOrigin('X', xDirection);
                _raycaster.SetCastLength(_moveSpeed * Time.deltaTime);
                _raycaster.OffsetRayOrigin('X', _raycaster.HorizontalRaySpacing * i);

                var hit = _raycaster.FireAndDrawCastAndReturnFirstResult('X', xDirection, Color.green, 0.25f);

                if (hit && hit.distance < shortestHitDistance)
                {
                    shortestHitDistance = hit.distance;
                    moveDistance = hit.distance * xDirection;
                }
            }

            transform.Translate(new Vector2(moveDistance, 0));
            _timeSinceLastMovement = 0;

            return true;
        }

        return false;
    }

    public void Activate(GameObject activator)
    {
        CanMove = true;
    }

    public void Deactivate(GameObject activator)
    {
        CanMove = false;
    }

    public void WarpToPosition(Vector2 position, Vector3 rotation)
    {
        transform.position = position;
        transform.rotation = Quaternion.Euler(rotation);
    }
}
