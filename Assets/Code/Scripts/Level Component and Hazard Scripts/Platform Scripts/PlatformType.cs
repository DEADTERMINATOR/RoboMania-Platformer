﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformType : MonoBehaviour
{
    public enum PlatformTypes { Normal, NoWallSlide, Bouncy }
    

    public PlatformTypes Type { get { return _type; } }

    [SerializeField]
    private PlatformTypes _type;
}
