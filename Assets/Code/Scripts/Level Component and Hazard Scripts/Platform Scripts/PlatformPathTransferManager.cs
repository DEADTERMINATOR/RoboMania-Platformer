﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelComponents.Platforms;

namespace LevelComponents.Platforms.Tools
{
    public class PlatformPathTransferManager : MonoBehaviour, IActivatable
    {
        public bool Active { get; private set; }


        [SerializeField]
        private MovingPlatform _platform;
        [SerializeField]
        private bool _transferToBasePath;
        [SerializeField]
        private MovingPlatformPathComponent _pathToTransferTo;


        private void OnTriggerEnter2D(Collider2D collider)
        {
            Activate(gameObject);
        }

        public void Activate(GameObject activator)
        {
            if (_transferToBasePath)
            {
                _platform.GoBackToBasePath();
            }
            else
            {
                _platform.SetPathTransfer(_pathToTransferTo.Path);
            }

            Active = true;
        }

        public void Deactivate(GameObject activator) { }
    }
}
