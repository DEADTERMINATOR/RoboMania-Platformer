﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using Characters.Player;

namespace LevelComponents.Platforms
{
    [RequireComponent(typeof(CollisionBoxcaster2D))]
    public class BouncyPlatform : Platform
    {
        private const float BLOCK_INPUT_TIME_WHEN_BUMPED = 0.15f;

        public float JumpVelocity;
        public Vector2 JumpDirectionUnitVector;
        public bool PlayerCanBoostVelocityWithTimedJump = false;

        private Animator _anim;


        protected override void Start()
        {
            base.Start();

            JumpVelocity = Mathf.Abs(JumpVelocity);
            JumpDirectionUnitVector.Normalize();

            _anim = GetComponent<Animator>();
        }

        protected void Update()
        {
            CalculatePassengerMovement(Vector3.zero);
        }

        protected override void CalculatePassengerMovement(Vector3 velocity)
        {
            foreach (MoveableObject obj in currentPassengers)
            {
                if (obj is PlayerMaster)
                {
                    var player = obj as PlayerMaster;

                    _anim.SetTrigger("boing");

                    if (PlayerCanBoostVelocityWithTimedJump)
                    {
                        player.State.SetBouncing(true);
                    }
                    else
                    {
                        player.State.SetBumped(true);
                        player.Input.BlockInput = true;
                        GlobalData.Timekeeper.StartTimer(() => GlobalData.Player.Input.BlockInput = false, BLOCK_INPUT_TIME_WHEN_BUMPED);
                    }

                    player.GravityScale = 1;
                    player.Data.InstantlyRechargeEnergyCells();

                    player.SetVelocity(JumpVelocity * JumpDirectionUnitVector, OBJECT_VELOCITY_COMPONENT);
                }
            }

            currentPassengers.Clear();
        }
    }
}
