﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LevelComponents.Platforms
{
    public class InfinitelyCyclingMovingPlatformSpawner : ActivatableLevelObject
    {
        /// <summary>
        /// Reference to the moving platform that should be spawned.
        /// </summary>
        public GameObject MovingPlatform;

        /// <summary>
        /// The speed the platforms should move at. If left at 0, the speed of the moving platform prefab will be used.
        /// </summary>
        public float PlatformSpeed = 0;

        /// <summary>
        /// The offset (in local coordinates) from the position of the spawner that represents the start of the platform path.
        /// </summary>
        public Vector2 BeginPositionOffset;

        /// <summary>
        /// The offset (in local coordinates) from the position of the spawner that represents the end of the platform path.
        /// </summary>
        public Vector2 EndPositionOffset;

        /// <summary>
        /// The offset (in local coordinates) applied to each platform after they spawn in their initial positions.
        /// </summary>
        public Vector2 StartingPositionOffset;

        /// <summary>
        /// The number of platforms that should be active along the path at a given time.
        /// </summary>
        public int NumberOfActivePlatforms;


        /// <summary>
        /// The positions (in global coordinates) of the waypoints along the platform path.
        /// </summary>
        private Vector3[] _waypoints;


        protected override void Start()
        {
            /* TODO: Rework this whole thing to work with the new moving platform paths should infinite cycling platforms ever be needed again.
            base.Start();

            Vector2 beginPosition = (Vector2)transform.position + BeginPositionOffset;
            Vector2 endPosition = (Vector2)transform.position + EndPositionOffset;

            float endPositionDistance = Vector2.Distance(beginPosition, endPosition);
            Vector2 offsetFromBeginToEnd = new Vector2(Mathf.Abs(BeginPositionOffset.x) + Mathf.Abs(EndPositionOffset.x), Mathf.Abs(BeginPositionOffset.y) + Mathf.Abs(EndPositionOffset.y));

            float pathPercentBetweenEachPlatform = 1.0f / NumberOfActivePlatforms;
            Vector2 directionFromBeginToEnd = endPosition - beginPosition;

            _waypoints = new Vector3[NumberOfActivePlatforms + 1];

            _waypoints[0] = new Vector3(beginPosition.x, beginPosition.y, 0);
            for (int i = 1; i < _waypoints.Length - 1; ++i)
            {
                 * Some explaination so I don't forget.
                 * offsetFromBeginToEnd is an absolute offset vector.
                 * So we needed to calculate the direction of platform travel and apply that signage to the offset.
                 * This way, we can offset from the beginning position in the correct direction.
                _waypoints[i] = new Vector2(beginPosition.x + offsetFromBeginToEnd.x * Mathf.Sign(directionFromBeginToEnd.x) * (pathPercentBetweenEachPlatform * i), beginPosition.y + offsetFromBeginToEnd.y * Mathf.Sign(directionFromBeginToEnd.y) * (pathPercentBetweenEachPlatform * i));
            }
            _waypoints[_waypoints.Length - 1] = (Vector2)transform.position + EndPositionOffset;

            Scene previousScene = SceneManager.GetActiveScene();
            SceneManager.SetActiveScene(SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage[0].Path));

            float distanceOffsetPercentage = 0;
            float offsetDistance = Vector3.Distance(Vector3.zero, StartingPositionOffset);
            if (_waypoints.Length > 1)
            {
                float distanceBetweenPlatforms = Vector3.Distance(_waypoints[0], _waypoints[1]);
                distanceOffsetPercentage = Mathf.Clamp01(offsetDistance / distanceBetweenPlatforms);
            }
            else if (_waypoints.Length == 1)
            {
                distanceOffsetPercentage = Mathf.Clamp01(offsetDistance / endPositionDistance);
            }

            for (int i = 0; i < _waypoints.Length - 1; ++i)
            {
                SceneManager.SetActiveScene(SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage[0].Path));
                AutomaticMovingPlatform instantiatedPlatform = Instantiate(MovingPlatform, (Vector2)_waypoints[i], Quaternion.Euler(MovingPlatform.transform.eulerAngles.x, MovingPlatform.transform.eulerAngles.y, MovingPlatform.transform.eulerAngles.z)).GetComponent<AutomaticMovingPlatform>();

                instantiatedPlatform.name += i;
                //instantiatedPlatform.Cyclic = true;
                instantiatedPlatform.Speed = PlatformSpeed != 0 ? PlatformSpeed : instantiatedPlatform.Speed;

                instantiatedPlatform.LateWaypointSetup(_waypoints, i + 1, distanceOffsetPercentage);
                instantiatedPlatform.EndWaypointReached += new AutomaticMovingPlatform.WaypointReached(OnEndWaypointReached);

                AddObjectToActivate(instantiatedPlatform);
            }
            SceneManager.SetActiveScene(previousScene);
            */
        }

        protected override void FullUpdate() { }

        private void OnEndWaypointReached(ref AutomaticMovingPlatform movingPlatform)
        {
            movingPlatform.SetCurrentWaypoint(0);
        }

        private void OnDrawGizmos()
        {
            float size = 0.3f;

            if (Application.isPlaying)
            {
                Gizmos.color = Color.yellow;

                for (int i = 0; i < _waypoints.Length; ++i)
                {
                    Gizmos.DrawLine(_waypoints[i] - Vector3.up * size, _waypoints[i] + Vector3.up * size);
                    Gizmos.DrawLine(_waypoints[i] - Vector3.left * size, _waypoints[i] + Vector3.left * size);
                }
            }
            else
            {
                Gizmos.color = Color.yellow;

                Vector3 beginPosition = transform.position + (Vector3)BeginPositionOffset;
                Vector3 endPosition = transform.position + (Vector3)EndPositionOffset;

                Gizmos.DrawLine(beginPosition - Vector3.up * size, beginPosition + Vector3.up * size);
                Gizmos.DrawLine(beginPosition - Vector3.left * size, beginPosition + Vector3.left * size);

                Gizmos.color = Color.blue;
                float distanceBetweenPlatforms = Vector3.Distance(beginPosition, endPosition) / NumberOfActivePlatforms;
                Vector3 platformDirection = (endPosition - beginPosition).normalized;
                for (int i = 1; i < NumberOfActivePlatforms; ++i)
                {
                    Gizmos.DrawLine(beginPosition + distanceBetweenPlatforms * i * platformDirection - Vector3.up * size, beginPosition + distanceBetweenPlatforms * i * platformDirection + Vector3.up * size);
                    Gizmos.DrawLine(beginPosition + distanceBetweenPlatforms * i * platformDirection - Vector3.left * size, beginPosition + distanceBetweenPlatforms * i * platformDirection + Vector3.left * size);
                }

                Gizmos.DrawLine(endPosition - Vector3.up * size, endPosition + Vector3.up * size);
                Gizmos.DrawLine(endPosition - Vector3.left * size, endPosition + Vector3.left * size);

                Gizmos.color = Color.green;

                Gizmos.DrawLine(transform.position - Vector3.up * size, transform.position + Vector3.up * size);
                Gizmos.DrawLine(transform.position - Vector3.left * size, transform.position + Vector3.left * size);
            }
        }
    }
}
