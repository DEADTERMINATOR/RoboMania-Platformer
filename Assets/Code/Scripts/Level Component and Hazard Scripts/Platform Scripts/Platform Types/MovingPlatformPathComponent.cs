﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformPathComponent : MonoBehaviour
{
    public MovingPlatformPath Path { get; private set; }
    public List<MovingPlatformPath.Waypoint> Waypoints { get { return _waypoints; } }

    [SerializeField]
    private List<MovingPlatformPath.Waypoint> _waypoints;
    [SerializeField]
    private MovingPlatformPath.EndWaypointMode _endWaypointMode;
    

    private void Awake()
    {
        Path = new MovingPlatformPath(_waypoints, _endWaypointMode);
    }

    private void Start()
    {
        for (int i = 0; i < _waypoints.Count; ++i)
        {
            if (_waypoints[i].EditorComponentPathToTransferTo != null)
            {
                _waypoints[i] = new MovingPlatformPath.Waypoint(_waypoints[i].Position, _waypoints[i].EditorComponentPathToTransferTo.Path);
            }
            else
            {
                _waypoints[i] = new MovingPlatformPath.Waypoint(_waypoints[i].Position);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (_waypoints != null)
        {
            Gizmos.color = Color.green;
            float size = 0.3f;

            for (int i = 0; i < _waypoints.Count; ++i)
            {
                Gizmos.DrawLine(_waypoints[i].Position - Vector3.up * size, _waypoints[i].Position + Vector3.up * size);
                Gizmos.DrawLine(_waypoints[i].Position - Vector3.left * size, _waypoints[i].Position + Vector3.left * size);
            }
        }
    }
}
