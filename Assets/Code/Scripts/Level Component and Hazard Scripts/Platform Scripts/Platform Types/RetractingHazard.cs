﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelComponents.Platforms;

namespace LevelComponents.Hazards
{
    public class RetractingHazard : ActivatableLevelObject
    {
        public enum HazardState { Extended, Retracted, Extending, Retracting, Warning }

        private const float WARNING_DISTANCE_PERCENTAGE = 0.3f;

        /// <summary>
        /// The distance the hazard should travel from its spawn point.
        /// </summary>
        /// For some reason this doesn't seem to correspond to the number of units the object actually moves. Can't even begin to fucking explain why.
        public Vector2 ExtensionDistance;

        /// <summary>
        /// The speed the hazard should extend out at.
        /// </summary>
        public float ExtensionSpeed;

        /// <summary>
        /// How long the hazard should remain fully extended.
        /// </summary>
        public float TimeToRemainExtended;

        /// <summary>
        /// The speed the hazard should retract back in at.
        /// </summary>
        public float RetractionSpeed;

        /// <summary>
        /// How long the hazard should remain fully retracted.
        /// </summary>
        public float TimeToRemainRetracted;

        /// <summary>
        /// Whether the hazard should give a warning that its about to extend by extending a portion of the way and waiting for a bit.
        /// </summary>
        public bool HazardShouldGiveWarning;

        /// <summary>
        /// How long the warning that the hazard is about to extend should last.
        /// </summary>
        public float WarningTime;

        /// <summary>
        /// Whether the initial state of the hazard is extended.
        /// </summary>
        public bool BeginExtended = false;

        /// <summary>
        /// The amount of time against the hazard retracted (or extended) time the hazard begins with. Used to stagger successive hazards.
        /// </summary>
        public float StartingTime = 0f;

        [HideInInspector]
        public Vector2 Velocity;

        public HazardState CurrentState { get { return _currentState; } }

        /// <summary>
        /// Whether the hazard is part of a moving platform.
        /// </summary>
        public bool PartOfMovingPlatform = false;



        /// <summary>
        /// The current state of the hazard.
        /// </summary>
        private HazardState _currentState = HazardState.Retracted;

        /// <summary>
        /// The elapsed time for the current state (if the state is time based).
        /// </summary>
        private float _currentElapsedStateTime = 0;

        private float _desiredTotalTravelDistance = 0;
        private float _desiredWarningTravelDistance = 0;
        private float _currentTravelledDistance = 0;

        private Vector2 _desiredLocalPosition;

        private KnockbackPlayer _knockbackEffect;
        private Rigidbody2D _transformRigidbody;


        protected override void Start()
        {
            base.Start();

            _knockbackEffect = GetComponentInChildren<KnockbackPlayer>();
            _transformRigidbody = GetComponent<Rigidbody2D>();

            _desiredTotalTravelDistance = Vector2.Distance(transform.localPosition, new Vector2(transform.localPosition.x + ExtensionDistance.x, transform.localPosition.y + ExtensionDistance.y));
            _desiredWarningTravelDistance = _desiredTotalTravelDistance * WARNING_DISTANCE_PERCENTAGE;

            if (BeginExtended)
            {
                _currentState = HazardState.Extended;

                if (_knockbackEffect != null)
                {
                    _knockbackEffect.enabled = true;
                }
            }
            else
            {
                if (_knockbackEffect != null)
                {
                    _knockbackEffect.enabled = false;
                }
            }

            _currentElapsedStateTime = StartingTime;
        }

        protected override void FullUpdate()
        {
            CheckHazardState(Time.deltaTime);
        }

        protected override void BareBonesUpdate()
        {
            CheckHazardState(Time.deltaTime);
        }

        protected override void FullFixedUpdate()
        {
            HazardUpdate();
        }

        protected override void BareBonesFixedUpdate()
        {
            HazardUpdate();
        }

        private void HazardUpdate()
        {
            Velocity = CalculateHazardMovement(Time.fixedDeltaTime);

            if (!PartOfMovingPlatform)
            {
                if (_transformRigidbody != null)
                {
                    _transformRigidbody.MovePosition((Vector2)transform.position + Velocity);
                }
                else
                {
                    transform.Translate(Velocity);
                }
            }
            else
            {
                transform.localPosition += (Vector3)Velocity;
            }
        }

        private void CheckHazardState(float timeSinceLastUpdate)
        {
            if (_currentState == HazardState.Extended)
            {
                _currentElapsedStateTime += timeSinceLastUpdate;
                if (_currentElapsedStateTime >= TimeToRemainExtended)
                {
                    _currentState = HazardState.Retracting;
                    _currentElapsedStateTime = 0;

                    _desiredLocalPosition = (Vector2)transform.localPosition - ExtensionDistance;

                    if (_knockbackEffect != null)
                    {
                        _knockbackEffect.enabled = false;
                    }
                }
            }
            /*
            else if (_currentState == HazardState.Retracting)
            {

            }
            */
            else if (_currentState == HazardState.Retracted)
            {
                _currentElapsedStateTime += timeSinceLastUpdate;
                if (_currentElapsedStateTime >= TimeToRemainRetracted)
                {
                    if (HazardShouldGiveWarning)
                    {
                        _currentState = HazardState.Warning;
                        _desiredLocalPosition = (Vector2)transform.localPosition + ExtensionDistance * WARNING_DISTANCE_PERCENTAGE;
                    }
                    else
                    {
                        _currentState = HazardState.Extending;
                        _desiredLocalPosition = (Vector2)transform.localPosition + ExtensionDistance;
                    }

                    _currentElapsedStateTime = 0;
                }
            }
            /*
            else if (_currentState == HazardState.Extending)
            {

            }
            */
            else if (_currentState == HazardState.Warning)
            {
                if (_currentTravelledDistance >= _desiredWarningTravelDistance)
                {
                    _currentElapsedStateTime += timeSinceLastUpdate;
                    if (_currentElapsedStateTime >= WarningTime)
                    {
                        _currentState = HazardState.Extending;
                        _currentElapsedStateTime = 0;

                        _desiredLocalPosition = (Vector2)transform.localPosition + ExtensionDistance * (1.0f - WARNING_DISTANCE_PERCENTAGE);
                    }
                }
            }
        }

        /// <summary>
        /// Calculates the velocity of this hazard this frame based on its current state.
        /// </summary>
        /// <returns>The hazard's velocity for this frame.</returns>
        private Vector2 CalculateHazardMovement(float timeSinceLastUpdate)
        {
            Vector2 velocity = Vector2.zero;

            /*
            if (_currentState == HazardState.Extended)
            {
                _currentElapsedStateTime += timeSinceLastUpdate;
                if (_currentElapsedStateTime >= TimeToRemainExtended)
                {
                    _currentState = HazardState.Retracting;
                    _currentElapsedStateTime = 0;

                    _desiredLocalPosition = (Vector2)transform.localPosition - ExtensionDistance;

                    if (_knockbackEffect != null)
                    {
                        _knockbackEffect.enabled = false;
                    }
                }
            }
            */
            if (_currentState == HazardState.Retracting)
            {
                if (_currentTravelledDistance >= _desiredTotalTravelDistance)
                {
                    _currentState = HazardState.Retracted;
                    _currentTravelledDistance = 0;
                }
                else
                {
                    velocity = Vector2.Lerp(Vector2.zero, ExtensionDistance, ExtensionSpeed * timeSinceLastUpdate);
                    velocity = -velocity;
                    _currentTravelledDistance += Vector2.Distance(Vector2.zero, velocity);

                    if (_currentTravelledDistance > _desiredTotalTravelDistance)
                    {
                        Vector2 correctionVelocity = (Vector2)transform.localPosition + velocity - _desiredLocalPosition;
                        velocity -= correctionVelocity;
                    }
                }
            }
            /*
            else if (_currentState == HazardState.Retracted)
            {
                _currentElapsedStateTime += timeSinceLastUpdate;
                if (_currentElapsedStateTime >= TimeToRemainRetracted)
                {
                    if (HazardShouldGiveWarning)
                    {
                        _currentState = HazardState.Warning;
                        _desiredLocalPosition = (Vector2)transform.localPosition + ExtensionDistance * WARNING_DISTANCE_PERCENTAGE;
                    }
                    else
                    {
                        _currentState = HazardState.Extending;
                        _desiredLocalPosition = (Vector2)transform.localPosition + ExtensionDistance;
                    }

                    _currentElapsedStateTime = 0;
                }
            }
            */
            else if (_currentState == HazardState.Extending)
            {
                if (_currentTravelledDistance >= _desiredTotalTravelDistance)
                {
                    _currentState = HazardState.Extended;
                    _currentTravelledDistance = 0;

                    if (_knockbackEffect != null)
                    {
                        _knockbackEffect.enabled = true;
                    }
                }
                else
                {
                    velocity = Vector2.Lerp(Vector2.zero, ExtensionDistance, ExtensionSpeed * timeSinceLastUpdate);
                    _currentTravelledDistance += Vector2.Distance(Vector2.zero, velocity);

                    if (_currentTravelledDistance > _desiredTotalTravelDistance)
                    {
                        Vector2 correctionVelocity = (Vector2)transform.localPosition + velocity - _desiredLocalPosition;
                        velocity -= correctionVelocity;
                    }
                }
            }
            else if (_currentState == HazardState.Warning)
            {
                if (_currentTravelledDistance < _desiredWarningTravelDistance)
                {
                    velocity = Vector2.Lerp(Vector2.zero, ExtensionDistance, ExtensionSpeed * timeSinceLastUpdate);
                    _currentTravelledDistance += Vector2.Distance(Vector2.zero, velocity);

                    if (_currentTravelledDistance > _desiredWarningTravelDistance)
                    {
                        Vector2 correctionVelocity = (Vector2)transform.localPosition + velocity - _desiredLocalPosition;
                        velocity -= correctionVelocity;
                    }
                }
                /*
                else
                {
                    _currentElapsedStateTime += timeSinceLastUpdate;
                    if (_currentElapsedStateTime >= WarningTime)
                    {
                        _currentState = HazardState.Extending;
                        _currentElapsedStateTime = 0;

                        _desiredLocalPosition = (Vector2)transform.localPosition + ExtensionDistance * (1.0f - WARNING_DISTANCE_PERCENTAGE);
                    }
                }
                */
            }

            return velocity;
        }
    }
}
