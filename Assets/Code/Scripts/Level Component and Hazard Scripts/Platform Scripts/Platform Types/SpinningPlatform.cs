﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelComponents.Platforms;

/*
namespace LevelComponents.Platforms
{
    public class SpinningPlatform : Platform
    {
        private struct TrackedHits
        {
            public enum SideHit { Top, Bottom, Left, Right }

            public SideHit SideHitOccuredOn;
            public int CastIndex;
            public Vector2 CastPosition;

            public TrackedHits(SideHit sideHitOccuredOn, int castIndex, Vector2 castPosition)
            {
                SideHitOccuredOn = sideHitOccuredOn;
                CastIndex = castIndex;
                CastPosition = castPosition;
            }
        }


        [SerializeField]
        private SpinningTrap _spinningTrap;

        private Dictionary<Transform, TrackedHits> _trackedHits = new Dictionary<Transform, TrackedHits>();
        private Vector3 _previousRotation;
        private CollisionRaycaster2D _raycaster;


        protected override void Start()
        {
            base.Start();
            _previousRotation = _spinningTrap.CurrentRotation;
            _raycaster = _collisionCaster as CollisionRaycaster2D;
        }

        protected override void CalculatePassengerMovement(Vector3 velocity)
        {
            float changeInAngle = 0;
            switch (_spinningTrap.RotationAxis)
            {
                case SpinningTrap.Axis.X:
                    changeInAngle = _spinningTrap.CurrentRotation.x - _previousRotation.x;
                    break;
                case SpinningTrap.Axis.Y:
                    changeInAngle = _spinningTrap.CurrentRotation.y - _previousRotation.y;
                    break;
                case SpinningTrap.Axis.Z:
                    changeInAngle = _spinningTrap.CurrentRotation.z - _previousRotation.z;
                    break;
            }
            Debug.Log("Angle Change: " + changeInAngle);

            RaycastHit2D hit;
            Vector3 moveVector = Vector3.zero;
            _raycaster.SetCastLength(CollisionRaycaster2D.SKIN_WIDTH * 2f);

            for (int i = 0; i < _raycaster.VerticalRayCount; ++i)
            {
                _raycaster.SetCastOrigin('Y', POSITIVE_AXIS);
                _raycaster.OffsetRayOrigin('Y', _raycaster.VerticalRaySpacing * i + velocity.x);
                hit = _raycaster.FireAndDrawCastAndReturnFirstResult('Y', POSITIVE_AXIS, Color.red, 0.05f);

                if (hit && !_trackedHits.ContainsKey(hit.transform))
                {
                    moveVector = new Vector3(Mathf.Cos(changeInAngle * Mathf.Deg2Rad), Mathf.Sin(changeInAngle * Mathf.Deg2Rad), 0);
                    Debug.Log("X: " + moveVector.x + " Y: " + moveVector.y);
                    hit.transform.GetComponent<Controller2D>()?.Move(ref moveVector, true);
                    _trackedHits.Add(hit.transform, new TrackedHits(TrackedHits.SideHit.Top, i, _raycaster.CastOrigin));
                }

                _raycaster.SetCastOrigin('Y', NEGATIVE_AXIS);
                _raycaster.OffsetRayOrigin('Y', _raycaster.VerticalRaySpacing * i + velocity.x);
                hit = _raycaster.FireAndDrawCastAndReturnFirstResult('Y', NEGATIVE_AXIS, Color.red, 0.05f);

                if (hit && !_trackedHits.ContainsKey(hit.transform))
                {
                    moveVector = new Vector3(Mathf.Cos(changeInAngle * Mathf.Deg2Rad), Mathf.Sin(changeInAngle * Mathf.Deg2Rad), 0);
                    Debug.Log("X: " + moveVector.x + " Y: " + moveVector.y);
                    hit.transform.GetComponent<Controller2D>()?.Move(ref moveVector, true);
                    _trackedHits.Add(hit.transform, new TrackedHits(TrackedHits.SideHit.Bottom, i, _raycaster.CastOrigin));
                }
            }

            for (int i = 0; i < _raycaster.HorizontalRayCount; ++i)
            {
                _raycaster.SetCastOrigin('X', POSITIVE_AXIS);
                _raycaster.OffsetRayOrigin('X', _raycaster.HorizontalRaySpacing * i + velocity.y);
                hit = _raycaster.FireAndDrawCastAndReturnFirstResult('X', POSITIVE_AXIS, Color.red, 0.05f);

                if (hit && !_trackedHits.ContainsKey(hit.transform))
                {
                    moveVector = new Vector3(Mathf.Cos(changeInAngle * Mathf.Deg2Rad), Mathf.Sin(changeInAngle * Mathf.Deg2Rad), 0);
                    Debug.Log("X: " + moveVector.x + " Y: " + moveVector.y);
                    hit.transform.GetComponent<Controller2D>()?.Move(ref moveVector, true);
                    _trackedHits.Add(hit.transform, new TrackedHits(TrackedHits.SideHit.Right, i, _raycaster.CastOrigin));
                }

                _raycaster.SetCastOrigin('X', NEGATIVE_AXIS);
                _raycaster.OffsetRayOrigin('X', _raycaster.HorizontalRaySpacing * i + velocity.y);
                hit = _raycaster.FireAndDrawCastAndReturnFirstResult('X', NEGATIVE_AXIS, Color.red, 0.05f);

                if (hit && !_trackedHits.ContainsKey(hit.transform))
                {
                    moveVector = new Vector3(Mathf.Cos(changeInAngle * Mathf.Deg2Rad), Mathf.Sin(changeInAngle * Mathf.Deg2Rad), 0);
                    Debug.Log("X: " + moveVector.x + " Y: " + moveVector.y);
                    hit.transform.GetComponent<Controller2D>()?.Move(ref moveVector, true);
                    _trackedHits.Add(hit.transform, new TrackedHits(TrackedHits.SideHit.Left, i, _raycaster.CastOrigin));
                }
            }
        }

        protected override void FullUpdate()
        {
            //ProcessTrackedHits(Vector3.zero);
            _trackedHits.Clear();
            _raycaster.UpdateCastOrigins();
            CalculatePassengerMovement(Vector3.zero);
            _previousRotation = _spinningTrap.CurrentRotation;
        }

        private void ProcessTrackedHits(Vector3 velocity)
        {
            Vector2 newCastPosition = Vector2.zero;

            Transform[] _trackedHitKeys = new Transform[_trackedHits.Keys.Count];
            _trackedHits.Keys.CopyTo(_trackedHitKeys, 0);

            TrackedHits[] _trackedHitValues = new TrackedHits[_trackedHits.Values.Count];
            _trackedHits.Values.CopyTo(_trackedHitValues, 0);

            for (int i = 0; i < _trackedHitKeys.Length; ++i)
            {
                switch (_trackedHitValues[i].SideHitOccuredOn)
                {
                    case TrackedHits.SideHit.Top:
                        _raycaster.SetCastOrigin('Y', POSITIVE_AXIS);
                        _raycaster.OffsetRayOrigin('Y', _raycaster.VerticalRaySpacing * _trackedHitValues[i].CastIndex + velocity.x);
                        newCastPosition = _raycaster.CastOrigin;
                        break;
                    case TrackedHits.SideHit.Bottom:
                        _raycaster.SetCastOrigin('Y', NEGATIVE_AXIS);
                        _raycaster.OffsetRayOrigin('Y', _raycaster.VerticalRaySpacing * _trackedHitValues[i].CastIndex + velocity.x);
                        newCastPosition = _raycaster.CastOrigin;
                        break;
                    case TrackedHits.SideHit.Right:
                        _raycaster.SetCastOrigin('X', POSITIVE_AXIS);
                        _raycaster.OffsetRayOrigin('X', _raycaster.HorizontalRaySpacing * _trackedHitValues[i].CastIndex + velocity.y);
                        newCastPosition = _raycaster.CastOrigin;
                        break;
                    case TrackedHits.SideHit.Left:
                        _raycaster.SetCastOrigin('X', NEGATIVE_AXIS);
                        _raycaster.OffsetRayOrigin('X', _raycaster.HorizontalRaySpacing * _trackedHitValues[i].CastIndex + velocity.y);
                        newCastPosition = _raycaster.CastOrigin;
                        break;
                }

                Vector3 changeInPosition = newCastPosition - _trackedHitValues[i].CastPosition;
                Debug.Log("X: " + changeInPosition.x + " Y: " + changeInPosition.y);
                _trackedHitKeys[i].GetComponent<Controller2D>()?.Move(ref changeInPosition, true);
            }

            _trackedHits.Clear();

            /*
            foreach (TrackedHits hit in _trackedHits.Values)
            {
                switch (hit.SideHitOccuredOn)
                {
                    case TrackedHits.SideHit.Top:
                        _raycaster.SetCastOrigin('Y', POSITIVE_AXIS);
                        _raycaster.OffsetRayOrigin('Y', _raycaster.VerticalRaySpacing * hit.CastIndex + velocity.x);
                        newCastPosition = _raycaster.CastOrigin;
                        break;
                    case TrackedHits.SideHit.Bottom:
                        _raycaster.SetCastOrigin('Y', NEGATIVE_AXIS);
                        _raycaster.OffsetRayOrigin('Y', _raycaster.VerticalRaySpacing * hit.CastIndex + velocity.x);
                        newCastPosition = _raycaster.CastOrigin;
                        break;
                    case TrackedHits.SideHit.Right:
                        _raycaster.SetCastOrigin('X', POSITIVE_AXIS);
                        _raycaster.OffsetRayOrigin('X', _raycaster.HorizontalRaySpacing * hit.CastIndex + velocity.y);
                        newCastPosition = _raycaster.CastOrigin;
                        break;
                    case TrackedHits.SideHit.Left:
                        _raycaster.SetCastOrigin('X', NEGATIVE_AXIS);
                        _raycaster.OffsetRayOrigin('X', _raycaster.HorizontalRaySpacing * hit.CastIndex + velocity.y);
                        newCastPosition = _raycaster.CastOrigin;
                        break;
                }

                var changeInPosition = newCastPosition - hit.CastPosition;
                _trackedHits.V
            }
        }
    }
}
*/