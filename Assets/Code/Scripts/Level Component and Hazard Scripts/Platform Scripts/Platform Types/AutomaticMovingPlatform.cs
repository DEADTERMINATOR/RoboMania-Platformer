﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelComponents.Hazards;
using UnityEngine.Events;
using UnityEditor;

namespace LevelComponents.Platforms
{
    /// <summary>
    /// A component that allows for platforms to have moving capabilities.
    /// </summary>
    public class AutomaticMovingPlatform : MovingPlatform, IWarpable //IActivatableByMultiple, IScaledHazard
    {
        public delegate void WaypointReached(ref AutomaticMovingPlatform movingPlatform);
        public event WaypointReached EndWaypointReached;

        public override Vector2 CurrentVelocityDirection { get { return Velocity.normalized; } }


        private float _maxSpeed;


        public MovedPlatformEvent PlatformMoved = new MovedPlatformEvent();


        protected override void Start()
        {
            base.Start();

            _maxSpeed = Speed * MAX_SPEED_MULTIPLIER;
            timeOffScreen = 0.01f;
        }

        protected void OnBecameVisible()
        {
            if (speedUpWhenOffScreen)
            {
                timeOffScreen = 0;
                CurrentSpeed = Speed;
            }
        }

        protected void OnBecameInvisible()
        {
            if (speedUpWhenOffScreen)
            {
                timeOffScreen = 0.01f; //Setting the time to something above 0 here saves me having to do a separate bool to see if the platform has gone invisible.
            }
        }

        protected void Update()
        {
            if (speedUpWhenOffScreen && timeOffScreen > 0)
            {
                timeOffScreen += Time.deltaTime;
                if (timeOffScreen >= MIN_TIME_OFFSCREEN_BEFORE_SPEEDUP && CurrentSpeed < _maxSpeed)
                {
                    CurrentSpeed = Speed * (BASE_OFFSCREEN_SPEED_MULTIPLIER + ((Mathf.Floor(timeOffScreen / THRESHOLD_TIME_OFFSCREEN_BEFORE_FURTHER_SPEEDUP) - THRESHOLD_TIME_FLOORED_MULTIPLIER) * FURTHER_SPEEDUP_PERCENTAGE_MULTIPLIER));
                }
            }

            if (objectBlockingMovement)
            {
                platformWaitTimer += Time.deltaTime;
                if (platformWaitTimer >= PLATFORM_WAIT_ON_BLOCK)
                {
                    objectBlockingMovement = false;
                    platformWaitTimer = 0;
                }
            }
        }

        protected override void FixedUpdate()
        {
            PerformPlatformMovement(Time.fixedDeltaTime);
            base.FixedUpdate();
        }

        private void PerformPlatformMovement(float time)
        {
            if (!objectBlockingMovement && currentPath != null)
            {
                CalculatePlatformMovement(time);
            }
            else
            {
                ZeroOutVelocity();
            }
        }

        /// <summary>
        /// Calculates the movement to the platforms next waypoint for this frame. 
        /// </summary>
        /// <returns>The amount the platform needs to move this frame.</returns>
        private void CalculatePlatformMovement(float time)
        {
            if (Time.time < nextMoveTime)
            {
                return;
            }

            Vector3 newPosition = Vector3.zero;

            if (moveToNewPath)
            {
                newPosition = Vector3.Lerp(transform.position, currentPath.FromPosition, CurrentSpeed * time);

                if (Vector2.Distance(currentPath.FromPosition, newPosition) <= 0.05f)
                {
                    if (transformRigidbody != null)
                    {
                        transformRigidbody.position = newPosition;
                    }
                    else
                    {
                        transform.position = newPosition;
                    }

                    moveToNewPath = false;
                }

                previousFrameTravelDirection = GetDirectionToNextWaypoint();
            }
            else
            {
                if (percentBetweenWaypoints == 0)
                {
                    //If the platform has just hit a waypoint, make sure the plaform is exactly lined up with the waypoint position (handles possible floating point errors in the position).
                    transform.position = currentPath.FromPosition;
                }

                bool waypointHit = false;

                float distanceBetweenWaypoints = Vector3.Distance(currentPath.FromPosition, currentPath.ToPosition);

                lastPercentBetweenWaypoints = percentBetweenWaypoints;
                percentBetweenWaypoints += time * CurrentSpeed / distanceBetweenWaypoints;
                //Debug.Log("Before: " + percentBetweenWaypoints);
                if (percentBetweenWaypoints >= 1.0f)
                {
                    waypointHit = true;
                    Mathf.Clamp01(percentBetweenWaypoints);
                }
                //TODO: Re-implement easing with the new platform velocity system.
                //percentBetweenWaypoints = Ease(percentBetweenWaypoints, easeFactor);
                //Debug.Log("After: " + percentBetweenWaypoints);

                var percentChangeThisFrame = percentBetweenWaypoints - lastPercentBetweenWaypoints;
                var distanceChangeThisFrame = distanceBetweenWaypoints * percentChangeThisFrame;
                var speedThisFrame = distanceChangeThisFrame / time;

                var newVelocity = speedThisFrame * GetDirectionToNextWaypoint();
                ApplyPassengerVelocityDifference(newVelocity);
                CheckForSetStandingOnUpwardMovingPlatform(newVelocity);

                previousFrameTravelDirection = GetDirectionToNextWaypoint();
                SetVelocity(newVelocity, OBJECT_VELOCITY_COMPONENT);

                if (waypointHit)
                {
                    var temp = (Vector2)transform.position + newVelocity * Time.fixedDeltaTime;
                    //Debug.Log("Waypoint #" + currentPath.ToWaypointIndex + " reached at position " + "(" + temp.x + " , " + temp.y + ")");

                    percentBetweenWaypoints = 0;
                    bool isAtEnd = currentPath.IncrementPath();

                    if (transferToNewPath && currentPath.From.PathToTransferTo != null && currentPath.From.PathToTransferTo == pathToTransferTo)
                    {
                        HandlePathTransfer();
                    }
                    else if (isAtEnd)
                    {
                        var thisPlatform = this;
                        EndWaypointReached?.Invoke(ref thisPlatform);
                    }

                    nextMoveTime = Time.time + waitTime;
                }
            }
        }

        /// <summary>
        /// Sets the desired waypoint for the platform.
        /// </summary>
        /// <param name="currentWaypoint">The waypoint the platform should go to.</param>
        public void SetCurrentWaypoint(int currentWaypoint)
        {
            currentPath.SetPathWaypoint(currentWaypoint, false);
            transform.position = currentPath.FromPosition;
            percentBetweenWaypoints = 0;
        }

        /// <summary>
        /// Offsets the position of each waypoint by the supplied amount.
        /// </summary>
        /// <param name="offsetAmount">The amount to offset the waypoints by.</param>
        public override void SetBasePositionOffset(Vector3 offsetAmount)
        {
            transform.position = transform.position + offsetAmount;
            for (int i = 0; i < platformPaths.Count; ++i)
            {
                for (int j = 0; j < platformPaths[i].Waypoints.Length; ++j)
                {
                    var oldPosition = platformPaths[i].Waypoints[j].Position;
                    platformPaths[i].Waypoints[j].Position = new Vector3(oldPosition.x + offsetAmount.x, oldPosition.y + offsetAmount.y, oldPosition.z + offsetAmount.z);
                }
            }
        }

        /// <summary>
        /// Warps the platform to the waypoint closest to the specified position.
        /// </summary>
        /// <param name="position">The position to use to find the nearest waypoint.</param>
        /// <param name="rotation">Not used.</param>
        public void WarpToPosition(Vector2 position, Vector3 rotation)
        {
            transform.position = currentPath.SetPathWaypointClosestToPosition(position);
            percentBetweenWaypoints = 0;
        }

        private void OnDrawGizmos()
        {
            #if UNITY_EDITOR
            if (Application.isPlaying)
            {
                Handles.Label(transform.position, new GUIContent("" + CurrentSpeed));
            }
            #endif
        }

        protected override void OnDrawGizmosSelected()
        {
            base.OnDrawGizmosSelected();
            Color[] colors = new Color[] { Color.green, Color.red, Color.blue, Color.magenta, Color.cyan, Color.yellow, Color.grey, Color.black };

            if (Application.isPlaying)
            {
                for (int i = 0; i < platformPaths.Count; ++i)
                {
                    int colorIndex = i;
                    if (i >= colors.Length)
                    {
                        colorIndex = i % colors.Length - 1;
                    }
                    Gizmos.color = colors[colorIndex];

                    for (int j = 0; j < platformPaths[i].Waypoints.Length - 1; ++j)
                    {
                        StaticTools.DrawPointGizmosOnly(platformPaths[i].Waypoints[j].Position, Gizmos.color, "" + i, Color.white);
                    }
                }
            }
            else
            {
                bool localWaypointsDrawn = false;
                if (waypoints != null)
                {
                    Gizmos.color = colors[0];
                    for (int i = 0; i < waypoints.Count; ++i)
                    {
                        StaticTools.DrawPointGizmosOnly(waypoints[i].Position, Gizmos.color);
                    }

                    localWaypointsDrawn = true;
                }

                for (int i = 0; i < editorPathList.Count; ++i)
                {
                    if (editorPathList[i] != null)
                    {
                        int colorIndex = localWaypointsDrawn ? i + 1 : i; //We've already used the first color if we needed to draw local waypoints, we don't want to use it again unless we need to loop back around.
                        if (i >= colors.Length)
                        {
                            colorIndex = i % colors.Length - 1;
                        }
                        Gizmos.color = colors[colorIndex];

                        for (int j = 0; j < editorPathList[i].Waypoints.Count - 1; ++j)
                        {
                            StaticTools.DrawPointGizmosOnly(editorPathList[i].Waypoints[j].Position, Gizmos.color);
                        }
                    }
                }
            }
        }
    }

    [System.Serializable]
    public class MovedPlatformEvent : UnityEvent<Vector3>
    {
    }

}
