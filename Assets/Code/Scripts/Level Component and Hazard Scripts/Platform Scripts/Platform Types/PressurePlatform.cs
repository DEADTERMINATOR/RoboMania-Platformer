﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using Unity.Jobs;
using MovementEffects;
using Characters;

namespace LevelComponents.Platforms
{
    [RequireComponent(typeof(CollisionBoxcaster2D))]
    public class PressurePlatform : Platform
    {
        public const float PLATFORM_COLLAPSE_SPEED = -25f;
        public const float PLATFORM_RESPAWN_DISTANCE = 25f;


        [System.Serializable]
        public class PressurePlatformData
        {
            /// <summary>
            /// The maximum distance the pressure platform can travel.
            /// </summary>
            public float Distance;

            /// <summary>
            /// How long the platform must be free of any passengers before it can begin returning to its base position.
            /// </summary>
            public float TimeUntilPlatformRecoveryBegins;

            /// <summary>
            /// The multiplier applied to the platform's speed when it's moving back to its base position.
            /// </summary>
            [Min(0.01f)]
            public float RecoverySpeedMultiplier;
        }

        [System.Serializable]
        public class SinkingPlatformData : PressurePlatformData
        {
            /// <summary>
            /// Whether this platform drops when it reaches its maximum sink distance.
            /// </summary>
            public bool PlatformDrops;

            /// <summary>
            /// How long will the platform stay gone before the respawn process begins (if the platform drops).
            /// </summary>
            public float TimeUntilRespawn;

            /// <summary>
            /// The distance the platform will fall (if the platform drops) before the respawn process begins.
            /// </summary>
            [HideInInspector]
            public float RespawnDistance;
        }

        public class PressurePlatformState
        {
            /// <summary>
            /// Hold the platforms starting position so we know when the platform is at its max height.
            /// </summary>
            public Vector3 StartingPosition;

            /// <summary>
            /// The initial scale of the platform, so the correct scale can be restored on respawn.
            /// </summary>
            public Vector3 StartingScale;

            /// <summary>
            /// Whether the player is colliding with the platform and the platform should be dropping.
            /// </summary>
            public bool Moving;

            /// <summary>
            /// Whether the platform has achieved the maximum drop distance it is allowed.
            /// </summary>
            public bool MaxDistanceAchieved;

            /// <summary>
            /// Holds how long the platform has been free of all passengers. We wait a short period of time before we begin moving the platorm again.
            /// </summary>
            public float HowLongPlatformPassengerFree;

            /// <summary>
            /// Whether the platform had at least one passenger this frame.
            /// </summary>
            public bool PlatformHasPassenger;
        }

        public class SinkingPlatformState : PressurePlatformState
        {
            /// <summary>
            /// Whether the platform shaking indicating the platform is about to drop is active.
            /// </summary>
            public bool CollapseWarningActive;

            /// <summary>
            /// Whether the platform needs to collapse.
            /// </summary>
            public bool CollapsePlatform;

            /// <summary>
            /// Whether the platform needs to begin the respawn process.
            /// </summary>
            public bool NeedsRespawn;

            /// <summary>
            /// Whether the platform is currently in the respawn process.
            /// </summary>
            public bool InRespawnProcess;

            /// <summary>
            /// How much longer until the platform respawns.
            /// </summary>
            public float HowLongUntilRespawn;
        }


        /// <summary>
        /// The game object that contains all the renderers for this object underneath it.
        /// </summary>
        public GameObject RendererMasterObject;


        /// <summary>
        /// Instance of the properties that a pressure platform possesses.
        /// </summary>
        [SerializeField]
        private PressurePlatformData _pressureProperties;

        /// <summary>
        /// If the platform is determined to be a sinking one based on the set speed, this holds an instance of the properties a sinking pressure platform possesses.
        /// </summary>
        [SerializeField]
        private SinkingPlatformData _sinkingProperties;

        /// <summary>
        /// Instance of the state variables for this pressure platform.
        /// </summary>
        private PressurePlatformState _state;

        /// <summary>
        /// If the platform is determined to be a sinking one based on the set speed, this holds an easy to access reference to the state cast to SinkingPlatformState.
        /// </summary>
        private SinkingPlatformState _sinkingState;

        /// <summary>
        /// The collider on the platform that defines the collision for the platforn.
        /// </summary>
        private Collider2D _collisionCollider;


        //Use this for initialization
        protected override void Start()
        {
            base.Start();

            if (Speed < 0)
            {
                _state = _sinkingState = new SinkingPlatformState();
                _pressureProperties = _sinkingProperties;

                currentVelocityDirection = new Vector2(0, -1);
            }
            else
            {
                _state = new PressurePlatformState();
                currentVelocityDirection = new Vector2(0, 1);
            }

            if (Speed < 0)
            {
                _sinkingProperties.TimeUntilRespawn = Mathf.Abs(_sinkingProperties.TimeUntilRespawn); //Ensure the time value is positive.
                _sinkingProperties.RespawnDistance = _pressureProperties.Distance + PLATFORM_RESPAWN_DISTANCE;
                _sinkingState.HowLongUntilRespawn = _sinkingProperties.TimeUntilRespawn;
            }

            _pressureProperties.TimeUntilPlatformRecoveryBegins = Mathf.Abs(_pressureProperties.TimeUntilPlatformRecoveryBegins); //Ensure the time value is positive.
            _pressureProperties.RecoverySpeedMultiplier = Mathf.Abs(_pressureProperties.RecoverySpeedMultiplier); //Ensure the multiplier is positive.

            _state.StartingPosition = transform.position;
            _state.StartingScale = transform.localScale;

            _collisionCollider = transform.GetComponentInChildren<Collider2D>();

            if (_collisionCollider == null)
            {
                Debug.LogError("Sinking platform " + gameObject.name + " is missing a Collider2D for its collision");
            }
        }

        protected void Update()
        {
            if (((Speed < 0 && !_sinkingState.InRespawnProcess) || Speed > 0) && !_state.Moving)
            {
                if (Speed < 0)
                {
                    if (!_sinkingState.NeedsRespawn)
                    {
                        if (!_sinkingState.CollapseWarningActive && !_sinkingState.CollapsePlatform)
                        {
                            _state.HowLongPlatformPassengerFree += Time.deltaTime;
                        }

                        if (_state.MaxDistanceAchieved && _sinkingProperties.PlatformDrops && !_sinkingState.CollapseWarningActive)
                        {
                            Timing.RunCoroutine(CollapsePlatform(), "Collapse Platform");
                        }
                    }
                    else
                    {
                        _sinkingState.HowLongUntilRespawn -= Time.deltaTime;
                        if (_sinkingState.HowLongUntilRespawn <= 0f)
                        {
                            Timing.RunCoroutine(RespawnPlatform(), "Respawn Platform");
                        }
                    }
                }
                else
                {
                    //This is a raising platform, which doesn't have to deal with collapsing or respawning.
                    _state.HowLongPlatformPassengerFree += Time.deltaTime;
                }
            }
            else if (Speed > 0 && allowedToMoveEvenIfBlocked && PlatformHasPassengers())
            {
                CheckObjectBlockingMovement();
            }
        }

        protected override void FixedUpdate()
        {
            bool platformRecovering = false;
            var newVelocity = Vector2.zero;

            if (PlatformHasPassengers())
            {
                _state.PlatformHasPassenger = true;
            }
            else
            {
                _state.PlatformHasPassenger = false;
            }

            if ((Speed < 0 && !_sinkingState.NeedsRespawn && !_sinkingState.InRespawnProcess) || Speed > 0)
            {
                if (!PlatformHasPassengers() && !objectBlockingMovement && (_sinkingState == null || (!_sinkingState.CollapseWarningActive && !_sinkingState.CollapsePlatform))
                    && _state.HowLongPlatformPassengerFree >= _pressureProperties.TimeUntilPlatformRecoveryBegins
                    && ((Speed < 0 && transform.position.y < _state.StartingPosition.y) || (Speed > 0 && transform.position.y > _state.StartingPosition.y)))
                {
                    //Sometimes, fast moving platforms may overshoot their base position while returning to it. This is intended to correct that.
                    if ((Speed < 0 && transform.position.y > _state.StartingPosition.y) || (Speed > 0 && transform.position.y < _state.StartingPosition.y))
                    {
                        var correctionDistance = Vector3.Distance(_state.StartingPosition, transform.position);
                        var maxStepDistance = Mathf.Abs(Speed * _pressureProperties.RecoverySpeedMultiplier) * Time.deltaTime;
                        if (correctionDistance > maxStepDistance)
                        {
                            //Travel the maximum distance, in the direction of the base position, allowed in one step based on the recovery speed.
                            newVelocity = maxStepDistance * (_state.StartingPosition - transform.position).normalized;
                        }
                        else
                        {
                            //Travel the entire correction distance.
                            newVelocity = _state.StartingPosition - transform.position;
                        }
                    }
                    else
                    {
                        newVelocity = new Vector2(0, -(Speed * _pressureProperties.RecoverySpeedMultiplier));
                    }
                    platformRecovering = true;
                    _state.MaxDistanceAchieved = false;
                }

                if (!_state.MaxDistanceAchieved && !objectBlockingMovement)
                {
                    if (Vector3.Distance(_state.StartingPosition, transform.position) >= _pressureProperties.Distance && !platformRecovering)
                    {
                        _state.MaxDistanceAchieved = true;
                        _state.Moving = false;

                        newVelocity = Vector2.zero;
                    }

                    if (_state.Moving)
                    {
                        newVelocity = new Vector2(0, Speed);
                    }
                    else if (!platformRecovering)
                    {
                        newVelocity = Vector2.zero;
                    }

                    ApplyPassengerVelocityDifference(newVelocity);
                    SetVelocity(newVelocity, OBJECT_VELOCITY_COMPONENT);
                }
                else
                {
                    if (Speed < 0)
                    {
                        var newPosition = transform.position;
                        if (_sinkingProperties.PlatformDrops && _sinkingState.CollapsePlatform)
                        {
                            SetVelocity(new Vector2(0, PLATFORM_COLLAPSE_SPEED), OBJECT_VELOCITY_COMPONENT);
                            ZeroOutAllPassengerVelocity();
                        }

                        if (Vector2.Distance(newPosition, _state.StartingPosition) >= _sinkingProperties.RespawnDistance)
                        {
                            DespawnPlatform();
                        }
                    }
                }
            }

            base.FixedUpdate();
        }

        public override bool RegisterPassenger(MoveableObject passenger)
        {
            bool registrationOccurred = false;
            if (!currentPassengers.Contains(passenger))
            {
                currentPassengers.Add(passenger);

                var currentPlatformVelocity = passenger.GetVelocityComponentVelocity(PLATFORM_VELOCITY_COMPONENT);
                passenger.AddToVelocity(Velocity - currentPlatformVelocity, PLATFORM_VELOCITY_COMPONENT);

                if (Speed > 0)
                {
                    passenger.Controller.StandingOnUpwardMovingPlatform = true;
                }

                _state.Moving = true;
                _state.HowLongPlatformPassengerFree = 0;

                passenger.HasBeenRegistered(this);
                registrationOccurred = true;
            }

            return registrationOccurred;
        }

        public override void DeregisterPassenger(MoveableObject passenger)
        {
            base.DeregisterPassenger(passenger);

            passenger.ZeroOutVelocityOnOneAxis('Y', PLATFORM_VELOCITY_COMPONENT);
            if (currentPassengers.Count == 0)
            {
                _state.Moving = false;
            }
        }

        public void MovePassengers(Vector3 velocity)
        {
            CalculatePassengerMovement(velocity);
        }

        /// <summary>
        /// A co-routine that performs a shake of the platform to indicate to the player that indicates the platform is about to fall.
        /// On completion, it sets the collapse platform variable so the collapse begins.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<float> CollapsePlatform()
        {
            _sinkingState.CollapseWarningActive = true;

            var transformToActUpon = RendererMasterObject != null ? RendererMasterObject.transform : transform;
            var rigidbodyToActUpon = transformRigidbody;
            if (transformToActUpon != transform)
            {
                rigidbodyToActUpon = transformToActUpon.GetComponent<Rigidbody2D>();
            }

            for (int i = 0; i < 10; ++i)
            {
                float percentRotationComplete = 0f;
                Vector3 startingRotation = transform.rotation.eulerAngles;

                while (percentRotationComplete < 1f)
                {
                    percentRotationComplete += Time.deltaTime / 0.1f;
                    if (i == 0) //i = 0, rotation = 0 to -2.5 degrees
                    {
                        if (rigidbodyToActUpon != null)
                        {
                            rigidbodyToActUpon.MoveRotation(Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z - 2.5f, percentRotationComplete)));
                        }
                        else
                        {
                            transformToActUpon.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z - 2.5f, percentRotationComplete));
                        }
                    }
                    else if (i % 2 == 0) //i == 2, 4, 6, 8, rotation = 2.5 to -2.5 degrees
                    {
                        if (rigidbodyToActUpon != null)
                        {
                            rigidbodyToActUpon.MoveRotation(Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z - 5f, percentRotationComplete)));
                        }
                        else
                        {
                            transformToActUpon.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z - 5f, percentRotationComplete));
                        }
                    }
                    else if (i % 2 != 0 && i != 9) //i == 1, 3, 5, 7, rotation = -2.5 to 2.5 degrees
                    {
                        if (rigidbodyToActUpon != null)
                        {
                            rigidbodyToActUpon.MoveRotation(Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z + 5f, percentRotationComplete)));
                        }
                        else
                        {
                            transformToActUpon.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z + 5f, percentRotationComplete));
                        }
                    }
                    else //i == 9, rotation = -2.5 to 0 degrees
                    {
                        if (rigidbodyToActUpon != null)
                        {
                            rigidbodyToActUpon.MoveRotation(Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z + 2.5f, percentRotationComplete)));
                        }
                        else
                        {
                            transformToActUpon.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(startingRotation.z, startingRotation.z + 2.5f, percentRotationComplete));
                        }
                    }
                    yield return 0f;
                }
            }

            _sinkingState.CollapsePlatform = true;

            currentPassengers.Clear();
        }

        public void DespawnPlatform()
        {
            transform.localScale = new Vector3(0, 0, 0); //Set the scale to 0 to effectively render the platform invisible and impossible to interact with without disabling it's update ability.'

            //Restore the platform to it's starting position in preparation for respawn. But only if this is not part of a composite platform, in that case the controller will handle positioning.
            if (transformRigidbody != null)
            {
                transformRigidbody.position = _state.StartingPosition;
            }
            else
            {
                transform.position = _state.StartingPosition;
            }

            //Disable the collider so that we can restore it when the platform has fully respawned (i.e. so the player can't jump and land on a platform currently re-scaling during the respawn).
            _collisionCollider.enabled = false;

            _sinkingState.CollapseWarningActive = false;
            _sinkingState.CollapsePlatform = false;

            _sinkingState.NeedsRespawn = true;
        }

        /// <summary>
        /// A co-routine that respawns the platform by re-scaling it from a scale of 0 to it's original scale.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<float> RespawnPlatform()
        {
            _sinkingState.NeedsRespawn = false;
            _sinkingState.InRespawnProcess = true;
            _sinkingState.HowLongUntilRespawn = _sinkingProperties.TimeUntilRespawn;

            float percentScalingComplete = 0f;
            while (percentScalingComplete < 1f)
            {
                percentScalingComplete += Time.deltaTime / 0.5f;
                transform.localScale = new Vector3(Mathf.Lerp(transform.localScale.x, _state.StartingScale.x, percentScalingComplete), Mathf.Lerp(transform.localScale.y, _state.StartingScale.y, percentScalingComplete), Mathf.Lerp(transform.localScale.z, _state.StartingScale.z, percentScalingComplete));
                yield return 0f;
            }

            //Make sure the player nor any enemy is inside the collider when we re-enable
            var overlapTestResults = new RaycastHit2D[3];
            bool overlapTest = true;

            _collisionCollider.enabled = true;
            Vector2 colliderSize = _collisionCollider.bounds.size; //The collider needs to be enabled to get a size reading that isn't 0.
            _collisionCollider.enabled = false;

            while (overlapTest)
            {
                int resultCount = Physics2D.BoxCastNonAlloc(_collisionCollider.bounds.center, colliderSize, 0, new Vector2(1, 1), overlapTestResults, 0, GlobalData.PLAYER_LAYER_SHIFTED | GlobalData.NPC_LAYER_SHIFTED | GlobalData.ENEMY_LAYER_SHIFTED);
                overlapTest = resultCount != 0;
                yield return 0f;
            }

            //Re-enable the collides now that the respawning is done.
            _collisionCollider.enabled = true;

            _sinkingState.InRespawnProcess = false;
            _state.Moving = false;
            _state.MaxDistanceAchieved = false;
        }

        public override void SetBasePositionOffset(Vector3 offsetAmount)
        {
            _state.StartingPosition += offsetAmount;
            if (transformRigidbody != null)
            {
                transformRigidbody.MovePosition(offsetAmount);
            }
            else
            {
                transform.position += offsetAmount;
            }
        }

        private void CheckObjectBlockingMovement()
        {
            if (objectBlockingMovement)
            {
                platformWaitTimer += Time.deltaTime;
                if (platformWaitTimer >= PLATFORM_WAIT_ON_BLOCK)
                {
                    objectBlockingMovement = false;
                    platformWaitTimer = 0;
                }
            }
        }
    }
}
