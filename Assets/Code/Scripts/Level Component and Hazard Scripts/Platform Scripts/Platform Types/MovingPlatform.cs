﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using Waypoint = MovingPlatformPath.Waypoint;

namespace LevelComponents.Platforms
{
    public abstract class MovingPlatform : Platform
    {
        // Constant values that define how a moving platform speed up when it goes off screen (if it is allowed to do so).
        protected const float BASE_OFFSCREEN_SPEED_MULTIPLIER = 3.0f;
        protected const float MIN_TIME_OFFSCREEN_BEFORE_SPEEDUP = 2.0f;
        protected const float THRESHOLD_TIME_OFFSCREEN_BEFORE_FURTHER_SPEEDUP = 0.5f;
        protected const float FURTHER_SPEEDUP_PERCENTAGE_MULTIPLIER = 0.25f;
        protected const float MAX_SPEED_MULTIPLIER = 10f;
        protected const int THRESHOLD_TIME_FLOORED_MULTIPLIER = (int)(MIN_TIME_OFFSCREEN_BEFORE_SPEEDUP / THRESHOLD_TIME_OFFSCREEN_BEFORE_FURTHER_SPEEDUP) - 1;


        /// <summary>
        /// The current speed of the platform (which factors in the possibility that it may be sped up by being off screen).
        /// </summary>
        public float CurrentSpeed { get; protected set; }


        /// <summary>
        /// How long the platform should wait when it reaches a waypoint.
        /// </summary>
        [SerializeField]
        protected float waitTime;

        /// <summary>
        /// The value the Easing function will use to create a smooth decceleration as the platform reaches the next waypoint.
        /// Clamped to between 1-3 for best results (1 providing linear change/no easing).
        /// </summary>
        [SerializeField]
        [Range(1, 3)]
        protected float easeFactor;

        /// <summary>
        /// The locations the platform will travel between in local space.
        /// </summary>
        [SerializeField]
        protected List<Waypoint> waypoints = new List<Waypoint>();

        /// <summary>
        /// The waypoint in the platform's list that it will start from.
        /// </summary>
        [SerializeField]
        protected int startingWaypoint = 0;

        /// <summary>
        /// Whether the platform should start at the position of the starting waypoint, or start at the position it is in the world and use the starting waypoint merely as a guide for where it is on its path.
        /// </summary>
        [Tooltip("This will warp the position of the platform to that of the starting waypoint. Making it false will have the platform travel to the starting waypoint from its current location.")]
        [SerializeField]
        protected bool startFromStartingWaypoint = true;

        /// <summary>
        /// Additional paths that this platform could follow, as MovingPlatformPathComponents assignable in the editor.
        /// </summary>
        [SerializeField]
        protected List<MovingPlatformPathComponent> editorPathList = new List<MovingPlatformPathComponent>();

        /// <summary>
        /// The way this platform should handle reaching the end waypoint.
        /// </summary>
        [SerializeField]
        protected MovingPlatformPath.EndWaypointMode endWaypointMode = MovingPlatformPath.EndWaypointMode.Reverse;

        /// <summary>
        /// Should the platform speed up after going off screen for the period of time in order so it gets back to the player faster.
        /// </summary>
        [SerializeField]
        protected bool speedUpWhenOffScreen = false;

        /// <summary>
        /// The Collider2D representing the area in which a passenger, as long as they are within it, should remain registered to the platform if they are already registered.
        /// </summary>
        [SerializeField]
        protected Collider2D influencedPassengerArea;

        /// <summary>
        /// The layer mask that determines what the moving platform collides with on the sides that don't carry passengers (left, right, bottom).
        /// </summary>
        [SerializeField]
        protected LayerMask nonPassengerSideCollisionMask;

        /// <summary>
        /// The list of passengers who aren't directly in contact with the platform (and are thus registered), but are still within the area of influence.
        /// </summary>
        protected List<MoveableObject> influencedPassengers = new List<MoveableObject>();

        /// <summary>
        /// The path the platform is currently following.
        /// </summary>
        protected MovingPlatformPath currentPath;

        /// <summary>
        /// The previous path the platform was following.
        /// </summary>
        protected MovingPlatformPath previousPath;

        /// <summary>
        /// The path constructed from the waypoints specifically attributed to the platform.
        /// </summary>
        protected MovingPlatformPath basePath;

        /// <summary>
        /// The complete list of constructed paths this platform can follow.
        /// </summary>
        protected List<MovingPlatformPath> platformPaths = new List<MovingPlatformPath>();

        /// <summary>
        /// The percentage (between 0 - 1) the platform has travelled between its previous waypoint and its next waypoint.
        /// </summary>
        protected float percentBetweenWaypoints;

        /// <summary>
        /// The percentage (between 0 - 1) the platform has travelled from the last frame.
        /// </summary>
        protected float lastPercentBetweenWaypoints;

        /// <summary>
        /// The direction the platform was moving in last frame.
        /// </summary>
        protected Vector2 previousFrameTravelDirection;

        /// <summary>
        /// The Unity time of when the platform is next allowed to move when it has reached a waypoint.
        /// </summary>
        protected float nextMoveTime;

        /// <summary>
        /// Whether the platform was instructed to switch to a new path which the platform should be searching for.
        /// </summary>
        protected bool transferToNewPath;

        /// <summary>
        /// The path the platform should be looking for to switch to.
        /// </summary>
        protected MovingPlatformPath pathToTransferTo;

        /// <summary>
        /// Whether the platform needs to move towards the first set point on its new path after a transfer.
        /// </summary>
        protected bool moveToNewPath;

        /// <summary>
        /// How long the platform has been offscreen.
        /// </summary>
        protected float timeOffScreen = 0;


        /// <summary>
        /// Whether the platform should check for obstacles while traveling in a horizontal direction.
        /// </summary>
        [SerializeField]
        private bool _checkForObstacles;


        protected override void Start()
        {
            base.Start();

            if (waypoints.Count != 0)
            {
                var platformSpecificWaypoints = new List<Waypoint>();
                for (int i = 0; i < waypoints.Count; ++i)
                {
                    if (waypoints[i].EditorComponentPathToTransferTo != null)
                    {
                        platformSpecificWaypoints.Add(new Waypoint(waypoints[i].Position, waypoints[i].EditorComponentPathToTransferTo.Path));
                    }
                    else
                    {
                        platformSpecificWaypoints.Add(new Waypoint(waypoints[i].Position));
                    }
                }
                basePath = new MovingPlatformPath(platformSpecificWaypoints, endWaypointMode, startingWaypoint);
                platformPaths.Add(basePath);
            }

            foreach (MovingPlatformPathComponent pathComponent in editorPathList)
            {
                if (pathComponent.Path.Waypoints.Length > 0)
                {
                    platformPaths.Add(pathComponent.Path);
                }
            }
            
            if (platformPaths.Count > 0)
            {
                currentPath = platformPaths[0];
            }
            else
            {
                Debug.LogWarning("Moving platform named " + gameObject.name + " doesn't have any paths to follow." +
                    "\n If you don't want the platform to move, consider using a regular platform.");
            }

            if (currentPath != null)
            {
                startingWaypoint = Mathf.Clamp(startingWaypoint, 0, waypoints.Count - 1);
                currentPath.SetPathWaypoint(startingWaypoint, false);

                if (startFromStartingWaypoint)
                {
                    if (transformRigidbody != null)
                    {
                        transformRigidbody.position = currentPath.GetPathWaypointPosition(startingWaypoint);
                    }
                    else
                    {
                        transform.position = currentPath.GetPathWaypointPosition(startingWaypoint);
                    }

                    percentBetweenWaypoints = 0;
                }
                else
                {
                    float distanceBetweenWaypoints = Vector2.Distance(currentPath.FromPosition, currentPath.ToPosition);
                    float distanceBetweenFromAndCurrentPosition = Vector2.Distance(currentPath.ToPosition, transform.position);

                    percentBetweenWaypoints = distanceBetweenFromAndCurrentPosition / distanceBetweenWaypoints;
                    percentBetweenWaypoints = Mathf.Clamp01(percentBetweenWaypoints); //Values outside of 0-1 may cause undefined behaviour.
                }

                SetVelocity(Speed * GetDirectionToNextWaypoint(), OBJECT_VELOCITY_COMPONENT);
            }

            CurrentSpeed = Speed;
            easeFactor = Mathf.Clamp(easeFactor, 1, 3);
        }

        protected override void FixedUpdate()
        {
            var finalVelocity = CalculateFrameVelocity();
            var actualFinalVelocity = finalVelocity;

            if (CurrentVelocityDirection.y != 1)
            {
                controller.Move(ref actualFinalVelocity, 'B', 'N', false, Space.World, nonPassengerSideCollisionMask);
            }
            else
            {
                controller.Move(ref actualFinalVelocity, 'B');
            }

            distanceTravelledThisFrame = Vector2.Distance(Vector2.zero, actualFinalVelocity);

            if (finalVelocity != actualFinalVelocity)
            {
                //Roll back the applied percent moved to accurately reflect how far the platform actually moved.
                var distanceDifference = Vector2.Distance(finalVelocity, actualFinalVelocity);
                var distanceBetweenWaypoints = Vector3.Distance(currentPath.FromPosition, currentPath.ToPosition);

                var percentOfDistanceDifference = distanceDifference / distanceBetweenWaypoints;
                percentBetweenWaypoints -= percentOfDistanceDifference;
            }
        }

        public Vector2 GetDirectionToNextWaypoint()
        {
            return (currentPath.ToPosition - currentPath.FromPosition).normalized;
        }

        /// <summary>
        /// Sets the platform to begin looking for the waypoint that will allow transfer to the specified path.
        /// </summary>
        /// <param name="newPath">The path to transfer to.</param>
        public void SetPathTransfer(MovingPlatformPath newPath)
        {
            if (newPath != null && newPath != currentPath)
            {
                transferToNewPath = true;
                pathToTransferTo = newPath;
            }
        }

        public void GoBackToBasePath()
        {
            if (basePath != null)
            {
                //TODO: This isn't finished.
                transferToNewPath = true;
            }
        }

        /// <summary>
        /// Handles the transfer to the desired new path once the appropriate transfer waypoint has been reached.
        /// </summary>
        protected virtual void HandlePathTransfer()
        {
            var newPathPosition = Vector3.zero;

            previousPath = currentPath;
            currentPath = pathToTransferTo;

            if (currentPath.From.WaypointIndexOnOtherPath != -1)
            {
                var canSetPathWaypoint = currentPath.SetPathWaypoint(previousPath.From.WaypointIndexOnOtherPath, false);
                if (!canSetPathWaypoint)
                {
                    currentPath.SetPathWaypointClosestToPosition(transform.position);
                }
            }
            else
            {
                pathToTransferTo.SetPathWaypointClosestToPosition(transform.position);
            }

            percentBetweenWaypoints = 0;
            moveToNewPath = true;

            transferToNewPath = false;
            pathToTransferTo = null;
        }

        public override bool RegisterPassenger(MoveableObject passenger)
        {
            var registrationSuccessful = base.RegisterPassenger(passenger);
            if (registrationSuccessful)
            {
                //If the passenger was successfully registered, check if they are already in the influenced passengers collection, and remove them if so.
                //This can happen if the passenger jumped while on the platform, removing themselves from direct registration, but stayed within the influenced area before landing on the platform again.
                influencedPassengers.Remove(passenger);
            }
            
            return registrationSuccessful;
        }

        public override void DeregisterPassenger(MoveableObject passenger)
        {
            base.DeregisterPassenger(passenger);

            if (!influencedPassengers.Contains(passenger) && influencedPassengerArea.bounds.Contains(new Vector3(passenger.transform.position.x, passenger.transform.position.y, influencedPassengerArea.transform.position.z)))
            {
                influencedPassengers.Add(passenger);
            }
        }

        protected override void ApplyPassengerVelocityDifference(Vector2 newVelocity)
        {
            base.ApplyPassengerVelocityDifference(newVelocity);

            var influencedPassengersCopy = new MoveableObject[influencedPassengers.Count];
            influencedPassengers.CopyTo(influencedPassengersCopy);

            foreach (MoveableObject passenger in influencedPassengersCopy)
            {
                if (!influencedPassengerArea.bounds.Contains(new Vector3(passenger.transform.position.x, passenger.transform.position.y, influencedPassengerArea.transform.position.z)))
                {
                    influencedPassengers.Remove(passenger);
                    passenger.ZeroOutVelocity(PLATFORM_VELOCITY_COMPONENT);
                }
                else if (previousFrameTravelDirection != GetDirectionToNextWaypoint())
                {
                    passenger.SetVelocity(newVelocity, PLATFORM_VELOCITY_COMPONENT);
                }
            }
        }

        protected void CheckForSetStandingOnUpwardMovingPlatform(Vector2 newVelocity)
        {
            if (newVelocity.normalized.y > 0 && Velocity.normalized.y <= 0)
            {
                //The platform has just started travelling upward.
                foreach (MoveableObject passenger in currentPassengers)
                {
                    passenger.Controller.StandingOnUpwardMovingPlatform = true;
                }
            }
            else if (newVelocity.normalized.y <= 0 && Velocity.normalized.y > 0)
            {
                //The platform has just started travelling in any other direction.
                foreach (MoveableObject passenger in currentPassengers)
                {
                    passenger.Controller.StandingOnUpwardMovingPlatform = false;
                }
            }
        }
    }
}
