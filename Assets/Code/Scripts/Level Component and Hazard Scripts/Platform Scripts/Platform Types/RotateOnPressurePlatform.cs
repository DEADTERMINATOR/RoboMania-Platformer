﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

namespace LevelComponents.Platforms
{
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(CollisionRaycaster2D))]
    public class RotateOnPressurePlatform : Platform
    {
        private const float RETURN_TO_BASE_ROTATION_TIME_MULTIPLIER = 3f;
        private const float FORCE_UNITS_APPLIED_PER_UNITY_OF_DISTANCE = 100f;

        public struct RotationProcedure
        {
            public float PercentComplete;

            public bool IsActive { get { return _isActive; } }
            public float StartingRotation { get { return _startingRotation; } }
            public float EndingRotation { get { return _endingRotation; } }
            public float DesiredRotationTime { get { return _desiredRotationTime; } }


            private bool _isActive;
            private float _startingRotation;
            private float _endingRotation;
            private float _desiredRotationTime;


            public void ActivateProcedure(float startingRotation, float endingRotation, float desiredRotationTime)
            {
                _isActive = true;
                PercentComplete = 0;

                _startingRotation = startingRotation;
                _endingRotation = endingRotation;
                _desiredRotationTime = desiredRotationTime;
            }

            public void DeactivateProcedure()
            {
                _isActive = false;
                PercentComplete = 0;

                _startingRotation = 0;
                _endingRotation = 0;
                _desiredRotationTime = 0;
            }
        }

        /// <summary>
        /// How far the platform is allowed to rotate in either direction (so the entire rotation range of the platform is -maxRotation to maxRotation).
        /// </summary>
        [Range(0, 85)]
        [SerializeField]
        private float _maxRotationInEitherDirection;

        [Tooltip("The player applies 100 units of force per unit of distance away from the center.")]
        [SerializeField]
        private float _forceToAchieveMaxRotation;

        [SerializeField]
        private float _forceUnitsAccelerationPerSecond;

        /// <summary>
        /// How long should it take for the platform to go from a zero degree rotation to the maximum allowed rotation on either side.
        /// The time it takes for the partial rotations along the way will be a portion of this value based on how far the platform is rotating at one time.
        /// </summary>
        [SerializeField]
        private float _maxRotationTime = 1.25f;

        /// <summary>
        /// How long after the player leaves the platform should it begin returning to a zero degree rotation.
        /// </summary>
        [SerializeField]
        private float _timeUntilPlatformReset = 1.5f;

        /// <summary>
        /// Whether the pivot point of the platform sprite is not at the center of the object
        /// </summary>
        [SerializeField]
        private bool _pivotPointIsNotAtCenterOfObject = false;

        /// <summary>
        /// If the pivot point is not at the center of the platform sprite, there is a greater distance the player can traverse as the platform rotates on one side of the platform (the "dominant" side) than the other.
        /// This is whether the platform should only be allowed to rotate to that side.
        /// </summary>
        [SerializeField]
        private bool _onlyRotateToDominantSide = false;

        /// <summary>
        /// Is the logic of the platform (i.e. the game object this script is attached to) a child game object of the game object that contains the renderer component for the platform.
        /// </summary>
        [SerializeField]
        private bool _platformLogicIsChildOfRenderer = false;

        private BoxCollider2D _collider;
        private Transform _localCenter;
        private Vector2 _platformPivotPoint;

        private Dictionary<Transform, Tuple<Vector3, float>> _topPassengerPositionsDictionary = new Dictionary<Transform, Tuple<Vector3, float>>();
        private Dictionary<Transform, Vector3> _bottomHitPositionsDictionary = new Dictionary<Transform, Vector3>();

        private float _percentOfRotationMaximumAllowedOnRightSide;
        private float _percentOfRotationMaximumAllowedOnLeftSide;
        private float _halfWidth;

        private bool _platformAtBasePosition = true;
        private float _remainingTimeUntilRotationReset;

        private Vector3 _baseRotation;
        private Bounds _baseBounds;

        private RotationProcedure _currentRotationProcedure;

        private CollisionRaycaster2D _collisionRaycaster;


        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();
            _collisionRaycaster = (CollisionRaycaster2D)collisionCaster;

            _collider = GetComponent<BoxCollider2D>();
            _remainingTimeUntilRotationReset = _timeUntilPlatformReset;

            if (_collider.transform.localEulerAngles.z != 0 && !_pivotPointIsNotAtCenterOfObject)
            {
                Debug.LogError("The collider on pressure platform " + gameObject.name + " is rotated. This will fuck shit up");
            }

            _collisionRaycaster.RaycastAlongGameObjectNormal = true;

            if (_pivotPointIsNotAtCenterOfObject)
            {
                _localCenter = transform.Find("Center");
                if (_localCenter == null)
                {
                    Debug.LogError("Pressure platform " + gameObject.name + " is missing a game object defining the local center.");
                }

                if (_platformLogicIsChildOfRenderer)
                {
                    _platformPivotPoint = transform.parent.position;
                    _baseRotation = transform.parent.eulerAngles;
                }
                else
                {
                    _platformPivotPoint = transform.position;
                    _baseRotation = transform.eulerAngles;
                }

                _baseBounds = _collider.bounds;

                if (_onlyRotateToDominantSide)
                {
                    if (_percentOfRotationMaximumAllowedOnRightSide > _percentOfRotationMaximumAllowedOnLeftSide)
                    {
                        _percentOfRotationMaximumAllowedOnRightSide = 1;
                        _percentOfRotationMaximumAllowedOnLeftSide = 0;
                    }
                    else
                    {
                        _percentOfRotationMaximumAllowedOnLeftSide = 1;
                        _percentOfRotationMaximumAllowedOnRightSide = 0;
                    }
                }
                else
                {
                    _percentOfRotationMaximumAllowedOnRightSide = 1.0f - (Vector2.Distance(_platformPivotPoint, _collider.bounds.min) / _collider.bounds.size.x);
                    if (StaticTools.ThresholdApproximately(_percentOfRotationMaximumAllowedOnRightSide, 1.0f, 0.15f))
                    {
                        _percentOfRotationMaximumAllowedOnRightSide = 1.0f;
                    }
                    _percentOfRotationMaximumAllowedOnLeftSide = 1.0f - _percentOfRotationMaximumAllowedOnRightSide;
                }

                if (_localCenter.transform.eulerAngles.z != 0)
                {
                    Debug.LogError("The collider on pressure platform " + gameObject.name + " does not properly offset the rotation of the parent object. This is no bueno.");
                }
            }
            else
            {
                _platformPivotPoint = transform.position;

                _percentOfRotationMaximumAllowedOnRightSide = 1.0f;
                _percentOfRotationMaximumAllowedOnLeftSide = 1.0f;
            }

            _halfWidth = _collider.size.x / 2f * transform.localScale.x;

            _currentRotationProcedure = new RotationProcedure();
        }

        //Runs in FixedUpdate() in ActivatableLevelObject.cs
        protected void BareBonesUpdate()
        {
            _remainingTimeUntilRotationReset -= Time.deltaTime;
            if (!_currentRotationProcedure.IsActive && !_platformAtBasePosition && _remainingTimeUntilRotationReset <= 0)
            {
                SetupRotationProcedure(GetCurrentPlatformRotation(), 0);
            }
            else if (_currentRotationProcedure.IsActive)
            {
                IncrementRotationProcedure();
            }
        }

        //Runs in FixedUpdate() in ActivatableLevelObject.cs
        protected void FullUpdate()
        {
            _topPassengerPositionsDictionary.Clear();
            _bottomHitPositionsDictionary.Clear();

            CalculatePassengerMovement(Vector3.zero);

            float nextPlatformRotationTarget = CalculatePlatformRotation();
            if (nextPlatformRotationTarget != -999)
            {
                nextPlatformRotationTarget = Mathf.Clamp(nextPlatformRotationTarget, -_maxRotationInEitherDirection, _maxRotationInEitherDirection);
            }

            float currentPlatformRotation = GetCurrentPlatformRotation();

            bool allowNewRotationProcedure = true;
            if (nextPlatformRotationTarget == -999 && _remainingTimeUntilRotationReset > 0)
            {
                allowNewRotationProcedure = false;
                _remainingTimeUntilRotationReset -= Time.deltaTime;
            }

            if ((!_platformAtBasePosition || nextPlatformRotationTarget != -999) && allowNewRotationProcedure)
            {
                if (nextPlatformRotationTarget == -999)
                {
                    nextPlatformRotationTarget = 0;
                }

                SetupRotationProcedure(currentPlatformRotation, nextPlatformRotationTarget);
            }
            else if (_topPassengerPositionsDictionary.Count == 0 && _bottomHitPositionsDictionary.Count == 0)
            {
                _currentRotationProcedure.DeactivateProcedure();
            }

            if (_currentRotationProcedure.IsActive)
            {
                IncrementRotationProcedure();
            }
        }

        protected override void CalculatePassengerMovement(Vector3 velocity)
        {
            _collisionRaycaster.UpdateCastOrigins();
            _collisionRaycaster.SetCastLength(0.25f);
            RaycastHit2D hit;

            for (int i = 0; i < _collisionRaycaster.VerticalRayCount; ++i)
            {
                _collisionRaycaster.SetCastOrigin('Y', 1);
                _collisionRaycaster.OffsetRayOrigin('Y', _collisionRaycaster.VerticalRaySpacing * i);
                hit = _collisionRaycaster.FireAndDrawCastAndReturnFirstResult('Y', 1, Color.yellow, 0.15f);

                if (hit && !_topPassengerPositionsDictionary.ContainsKey(hit.transform))
                {
                    float yDistanceFromPassengerToPlatform = CollisionRaycaster2D.PointDistanceFromGround(hit.transform.position);
                    _topPassengerPositionsDictionary.Add(hit.transform, Tuple.Create<Vector3, float>(hit.transform.position, yDistanceFromPassengerToPlatform));
                }
            }

            for (int i = 0; i < _collisionRaycaster.VerticalRayCount; ++i)
            {
                _collisionRaycaster.SetCastOrigin('Y', -1);
                _collisionRaycaster.OffsetRayOrigin('Y', _collisionRaycaster.VerticalRaySpacing * i);
                hit = _collisionRaycaster.FireAndDrawCastAndReturnFirstResult('Y', -1, Color.yellow, 0.15f);

                if (hit && !_bottomHitPositionsDictionary.ContainsKey(hit.transform))
                {
                    _bottomHitPositionsDictionary.Add(hit.transform, hit.point);
                }
            }
        }

        /// <summary>
        /// Calculates what the current rotation of the platform should be based on the distance the player is relative to the platform's pivot point, passed through a quad ease out function.
        /// </summary>
        /// <returns>The current desired rotation of the platform.</returns>
        private float CalculatePlatformRotation()
        {
            float finalWeightDistance = 0f;

            foreach (Transform passenger in _topPassengerPositionsDictionary.Keys)
            {
                float distanceFromCenter = _topPassengerPositionsDictionary[passenger].Item1.x - _platformPivotPoint.x;
                finalWeightDistance += distanceFromCenter;

                _remainingTimeUntilRotationReset = _timeUntilPlatformReset;
            }

            foreach (Transform hit in _bottomHitPositionsDictionary.Keys)
            {
                float distanceFromCenter = _bottomHitPositionsDictionary[hit].x - _platformPivotPoint.x;
                finalWeightDistance += -distanceFromCenter; //We want the platform to tilt the opposite direction if it's hit from below.
            }

            if (finalWeightDistance > 0)
            {
                finalWeightDistance *= _percentOfRotationMaximumAllowedOnRightSide;
            }
            else
            {
                finalWeightDistance *= _percentOfRotationMaximumAllowedOnLeftSide;
            }

            if (finalWeightDistance == 0f)
            {
                return -999f;
            }

            float totalDistanceToConsider = _halfWidth;
            if (_pivotPointIsNotAtCenterOfObject)
            {
                if (finalWeightDistance > 0)
                {
                    totalDistanceToConsider = _platformPivotPoint.x - _baseBounds.max.x;
                }
                else
                {
                    totalDistanceToConsider = _platformPivotPoint.x - _baseBounds.min.x;
                }
            }

            float easedFinalRotation = StaticTools.QuadEaseOut(0, _maxRotationInEitherDirection, Mathf.Abs(finalWeightDistance), Mathf.Abs(totalDistanceToConsider));
            return easedFinalRotation * Mathf.Sign(-finalWeightDistance); //We need to negate the finalWeightDistance result, becasue counterclockwise rotations are positive, while the math would produce a negative rotation.
        }

        /// <summary>
        /// Sets the platform up to begin a procedure to rotate the platform from a starting rotation to a target rotation.
        /// </summary>
        /// <param name="currentPlatformRotation"></param>
        /// <param name="nextPlatformRotationTarget"></param>
        private void SetupRotationProcedure(float currentPlatformRotation, float nextPlatformRotationTarget)
        {
            if (nextPlatformRotationTarget != 0)
            {
                _platformAtBasePosition = false;
            }

            float maxTimeToRotate = _maxRotationTime;
            if (StaticTools.ThresholdApproximately(nextPlatformRotationTarget, 0, 0.05f) && _topPassengerPositionsDictionary.Count == 0 && _bottomHitPositionsDictionary.Count == 0) //If the platform is returning to base rotation.
            {
                maxTimeToRotate *= RETURN_TO_BASE_ROTATION_TIME_MULTIPLIER;
            }

            float percentageDegreesOfRotationOfTotal = Mathf.Abs(currentPlatformRotation - nextPlatformRotationTarget) / _maxRotationInEitherDirection;
            float desiredRotationTime = maxTimeToRotate * percentageDegreesOfRotationOfTotal;

            _currentRotationProcedure.ActivateProcedure(currentPlatformRotation, nextPlatformRotationTarget, desiredRotationTime);
        }

        /// <summary>
        /// Progresses the rotation procedure by a single frames worth of time.
        /// </summary>
        private void IncrementRotationProcedure()
        {
            float currentPlatformRotation = transform.eulerAngles.z;
            if (currentPlatformRotation > 240)
            {
                currentPlatformRotation = -(360 - currentPlatformRotation);
            }

            _currentRotationProcedure.PercentComplete += Time.deltaTime / _currentRotationProcedure.DesiredRotationTime;
            Quaternion newRotation = Quaternion.Lerp(Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, _currentRotationProcedure.StartingRotation), Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, _currentRotationProcedure.EndingRotation), _currentRotationProcedure.PercentComplete);
            if (!_platformLogicIsChildOfRenderer)
            {
                transform.localRotation = Quaternion.Euler(new Vector3(0, 0, newRotation.eulerAngles.z + _baseRotation.z));
            }
            else
            {
                transform.parent.localRotation = Quaternion.Euler(new Vector3(0, 0, newRotation.eulerAngles.z + _baseRotation.z));
            }

            if (_currentRotationProcedure.PercentComplete >= 1f)
            {
                if (_currentRotationProcedure.EndingRotation == 0)
                {
                    _platformAtBasePosition = true;
                }

                _currentRotationProcedure.DeactivateProcedure();
            }

            if (_topPassengerPositionsDictionary.Count != 0 && GlobalData.Player.Velocity.x != 0)
            {
                var playerController = (PlayerController2D)GlobalData.Player.Controller;
                if (currentPlatformRotation < 0)
                {
                    //Raycast from the bottom left
                    playerController.Raycaster.SetCastOrigin(playerController.Raycaster.Origins.BottomLeft);
                    playerController.Raycaster.SetCastLength(2.0f);
                    RaycastHit2D hit = playerController.Raycaster.FireCastAndReturnFirstResult('Y', -1);

                    if (hit)
                    {
                        Vector3 correctionVelocity = new Vector3(0, -hit.distance, 0);
                        playerController.Move(ref correctionVelocity);
                    }
                }
                else
                {
                    //Raycast from the bottom right
                    playerController.Raycaster.SetCastOrigin(playerController.Raycaster.Origins.BottomRight);
                    playerController.Raycaster.SetCastLength(2.0f);
                    RaycastHit2D hit = playerController.Raycaster.FireCastAndReturnFirstResult('Y', -1);

                    if (hit)
                    {
                        Vector3 correctionVelocity = new Vector3(0, -hit.distance, 0);
                        playerController.Move(ref correctionVelocity);
                    }
                }
            }
        }

        private float GetCurrentPlatformRotation()
        {
            float currentPlatformRotation = 0;
            if (_platformLogicIsChildOfRenderer)
            {
                currentPlatformRotation = transform.parent.localEulerAngles.z;
            }
            else
            {
                currentPlatformRotation = transform.localEulerAngles.z;
            }

            if (currentPlatformRotation > 240)
            {
                currentPlatformRotation = -(360 - currentPlatformRotation);
            }

            return currentPlatformRotation;
        }
    }
}
