﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using Characters.Player;

namespace LevelComponents.Platforms
{
    public abstract class Platform : MoveableObject
    {
        private class PlatformGravityState : GravityState { }

        /// <summary>
        /// How long should the platform wait upon being blocked before trying to move again.
        /// </summary>
        protected const float PLATFORM_WAIT_ON_BLOCK = 0.5f;


        public override Controller2D Controller { get { return controller; } }
        public override Controller2D.CollisionInfo Collisions { get { return controller.Collisions; } }
        public override GravityState CurrentActiveGravityState { get { return _gravityState; } }

        /// <summary>
        /// The current normalized direction the platform is travelling in.
        /// </summary>
        public virtual Vector2 CurrentVelocityDirection { get { return currentVelocityDirection; } }

        public float DistanceTravelledThisFrame { get { return distanceTravelledThisFrame; } }


        /// <summary>
        /// The speed of the platform.
        /// </summary>
        public float Speed;


        /// <summary>
        /// Whether the platform is allowed to continue moving (after a period of time) regardless of whether it is being blocked by an obstacle or passenger.
        /// </summary>
        [SerializeField]
        protected bool allowedToMoveEvenIfBlocked = false;

        /// <summary>
        /// The current velocity of the platform as a normalized direction vector.
        /// </summary>
        protected Vector2 currentVelocityDirection = Vector2.zero;

        /// <summary>
        /// The maximum distance this platform can move in one fixed update step.
        /// </summary>
        protected float maxDistancePerFixedStep = 0;

        /// <summary>
        /// The distance this platform actually travelled this frame.
        /// </summary>
        protected float distanceTravelledThisFrame = 0;

        /// <summary>
        /// Whether moving a passenger along with the platform would cause the passenger to go through a collider.
        /// This essentially means the passenger is preventing the platform from moving.
        /// </summary>
        protected bool objectBlockingMovement;

        /// <summary>
        /// The timer that activates when the platform has been forced to wait (likely because a passenger is blocking it).
        /// </summary>
        protected float platformWaitTimer;

        /// <summary>
        /// The list of passengers currently registered with the platform.
        /// </summary>
        protected List<MoveableObject> currentPassengers = new List<MoveableObject>();

        /// <summary>
        /// Reference to the Rigidbody2D component on the platform (if it has one).
        /// </summary>
        protected Rigidbody2D transformRigidbody;

        /// <summary>
        /// Reference to the controller that's responsible for moving the platform.
        /// </summary>
        protected Controller2D controller;

        /// <summary>
        /// A raycaster used to handle collisions between the platform and other objects.
        /// </summary>
        protected CollisionCaster2D collisionCaster;

        /// <summary>
        /// Reference to the collider for the platform.
        /// </summary>
        protected BoxCollider2D platformCollider;


        private PlatformGravityState _gravityState = new PlatformGravityState();


        //Use this for initialization
        protected virtual void Start()
        {
            controller = GetComponent<Controller2D>();
            collisionCaster = GetComponent<CollisionCaster2D>();
            transformRigidbody = GetComponent<Rigidbody2D>();
            platformCollider = GetComponent<BoxCollider2D>();

            maxDistancePerFixedStep = Speed * Time.fixedDeltaTime;;
        }

        protected virtual void FixedUpdate()
        {
            var finalVelocity = CalculateFrameVelocity();
            controller.Move(ref finalVelocity, 'B');

            distanceTravelledThisFrame = Vector2.Distance(Vector2.zero, finalVelocity);
        }

        /// <summary>
        /// Registers a Controller2D as being a passenger on this platform, and thus needs to be moved with the platform.
        /// </summary>
        /// <param name="passenger">The Controller2D representing the object being registered as a passenger.</param>
        /// <returns>True if the passenger was not already registered and thus was registered. False if the passenger was already registered.</returns>
        public virtual bool RegisterPassenger(MoveableObject passenger)
        {
            bool registrationOccurred = false;
            if (!currentPassengers.Contains(passenger))
            {
                currentPassengers.Add(passenger);

                var currentPlatformVelocity = passenger.GetVelocityComponentVelocity(PLATFORM_VELOCITY_COMPONENT);
                passenger.AddToVelocity(Velocity - currentPlatformVelocity, PLATFORM_VELOCITY_COMPONENT);

                if (CurrentVelocityDirection.y > 0)
                {
                    passenger.Controller.StandingOnUpwardMovingPlatform = true;
                }

                passenger.HasBeenRegistered(this);
                registrationOccurred = true;
            }

            return registrationOccurred;
        }

        public virtual void DeregisterPassenger(MoveableObject passenger)
        {
            //Remove simply returns false if the passenger is not found, as opposed to throwing an error, hence an additional check to see if the passenger is even present is unnecessary.
            currentPassengers.Remove(passenger);
            passenger.HasBeenDeregistered();

            passenger.Controller.StandingOnUpwardMovingPlatform = false;
        }

        public bool PlatformHasPassengers()
        {
            return currentPassengers.Count != 0;
        }

        /// <summary>
        /// Calculates the difference between the new velocity for the platform and the old one, and adds that difference to every platform passenger.
        /// </summary>
        /// <param name="newVelocity"></param>
        protected virtual void ApplyPassengerVelocityDifference(Vector2 newVelocity)
        {
            var difference = newVelocity - Velocity;
            foreach (MoveableObject passenger in currentPassengers)
            {
                passenger.AddToVelocity(difference, PLATFORM_VELOCITY_COMPONENT);
            }
        }

        /// <summary>
        /// Zeroes out the platform velocities for all passengers currently on the platform.
        /// </summary>
        protected void ZeroOutAllPassengerVelocity()
        {
            foreach (MoveableObject passenger in currentPassengers)
            {
                passenger.SetVelocity(Vector2.zero, PLATFORM_VELOCITY_COMPONENT);
            }
        }

        /// <summary>
        /// Applies the equation y = x^easeFactor / (x^easeFactor + (1 - x)^easeFactor) to ease the given value. 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        protected float Ease(float x, float EaseFactor)
        {
            return Mathf.Pow(x, EaseFactor) / (Mathf.Pow(x, EaseFactor) + Mathf.Pow(1 - x, EaseFactor)); //Easing equation y = x^a / (x^a + (1 - x)^a)
        }

        public virtual void SetBasePositionOffset(Vector3 offsetAmount) { }

        protected virtual void CalculatePassengerMovement(Vector3 velocity) { }

        protected virtual void OnDrawGizmosSelected()
        {
            if (platformCollider != null)
            {
                Gizmos.DrawSphere(new Vector2(platformCollider.bounds.min.x, platformCollider.bounds.min.y), 0.01f);
                Gizmos.DrawSphere(new Vector2(platformCollider.bounds.max.x, platformCollider.bounds.min.y), 0.01f);
                Gizmos.DrawSphere(new Vector2(platformCollider.bounds.min.x, platformCollider.bounds.max.y), 0.01f);
                Gizmos.DrawSphere(new Vector2(platformCollider.bounds.max.x, platformCollider.bounds.max.y), 0.01f);
            }
        }
    }
}
