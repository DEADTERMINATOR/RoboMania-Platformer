﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Enemies;

public class ScreenTraversingDrillSpawner : MonoBehaviour
{
    /// <summary>
    /// Reference to the Drill enemy to be spawned.
    /// </summary>
    public GameObject Drill;

    /// <summary>
    /// Whether the spawning should occur.
    /// </summary>
    public bool SpawnActivated;

    /// <summary>
    /// The speed the drills will travel at.
    /// </summary>
    public float DrillSpeed;

    /// <summary>
    /// The amount of time that should elapse between each spawn.
    /// </summary>
    public float TimeBetweenSpawns;

    /// <summary>
    /// The distance the drill should travel before disappearing.
    /// </summary>
    public float TravelDistance;

    /// <summary>
    /// The angle the drill should travel at.
    /// </summary>
    public float TravelAngle;


    /// <summary>
    /// Reference to the collider that activates/deactives the spawning (if one exists).
    /// </summary>
    private BoxCollider2D _spawnCollider;

    /// <summary>
    /// How long has elapsed since the last spawn.
    /// </summary>
    private float _timeSinceLastSpawn = 0;

    /// <summary>
    /// A list containing the drills current spawned by this spawner.
    /// </summary>
    private List<Drill> _spawnedDrills = new List<Drill>();


    private void Start()
    {
        _spawnCollider = GetComponentInChildren<BoxCollider2D>();
        _timeSinceLastSpawn = TimeBetweenSpawns;
    }

    private void Update()
    {
        if (SpawnActivated)
        {
            _timeSinceLastSpawn += Time.deltaTime;
            if (_timeSinceLastSpawn >= TimeBetweenSpawns)
            {
                Drill instantiatedDrill = Instantiate(Drill, transform.position, Quaternion.Euler(0, 0, TravelAngle)).GetComponent<Drill>();
                instantiatedDrill.DrillDestroyed += new Drill.DestroyEvent(OnDrillDestroyed);

                instantiatedDrill.Activated = true;
                instantiatedDrill.BaseMoveSpeed = DrillSpeed;
                instantiatedDrill.TimeBetweenAttacks = 0;
                instantiatedDrill.TravelDistance = TravelDistance;
                instantiatedDrill.TravelAngle = TravelAngle;
                instantiatedDrill.DestroyAfterTravellingDesiredDistance = true;

                _spawnedDrills.Add(instantiatedDrill);

                _timeSinceLastSpawn = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            SpawnActivated = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            SpawnActivated = false;
        }
    }

    private void OnDrillDestroyed(Drill destroyedDrill)
    {
        _spawnedDrills.Remove(destroyedDrill);
    }

    private void OnDrawGizmos()
    {
        if (TravelAngle == 90)
            Gizmos.DrawIcon(transform.position, "Drill (90 Degrees)", false);
        else if (TravelAngle == 180)
            Gizmos.DrawIcon(transform.position, "Drill (180 Degrees)", false);
        else if (TravelAngle == 270)
            Gizmos.DrawIcon(transform.position, "Drill (270 Degrees)", false);
        else
            Gizmos.DrawIcon(transform.position, "Drill", false);
    }
}
