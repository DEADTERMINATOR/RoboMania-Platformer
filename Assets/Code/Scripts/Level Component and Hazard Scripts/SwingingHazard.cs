﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct StoredTrasfromData
{
    public Vector3 position;
    public Quaternion rotation;
    public StoredTrasfromData(Vector3 pos,Quaternion rot) 
    {
        position = pos;
        rotation = rot;
    }
}

public class SwingingHazard : MonoBehaviour ,IActivatable 
{
    /// <summary>
    /// Should The Hazard start moving
    /// </summary>
    public bool StartMoving = false;

    public bool Active { get { return _isActive; } }

    private bool _isActive;
    private List<Rigidbody2D> _ridgedBodies = new List<Rigidbody2D>();
    private List<StoredTrasfromData> _startingTrasfroms = new List<StoredTrasfromData>();


    public void Activate(GameObject activator)
    {
        RestToStart();
        SetSimulating(true);
        _isActive = true;
    }

    public void Deactivate(GameObject activator)
    {
        SetSimulating(false);
        _isActive = false;
    }

    // Use this for initialization
    void Awake()
    {
        foreach (Rigidbody2D body in  GetComponentsInChildren<Rigidbody2D>())
        {
            _ridgedBodies.Add(body);

            _startingTrasfroms.Add(new StoredTrasfromData( body.transform.position, body.transform.rotation));
        }
        if (!StartMoving)
        {
            SetSimulating(false);
        }
    }

    private void RestToStart()
    {
        for(int i = 0; i < _ridgedBodies.Count; i++ )
        {
            _ridgedBodies[i].transform.position = _startingTrasfroms[i].position;
            _ridgedBodies[i].transform.rotation = _startingTrasfroms[i].rotation;
        }
    }

    private void SetSimulating(bool isTrue)
    {
        for (int i = 0; i < _ridgedBodies.Count; i++)
        {
            _ridgedBodies[i].simulated = isTrue;
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
