﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatableJetFire : MonoBehaviour, IActivatable
{
    public bool Active { get; private set; } = false;


    [SerializeField]
    private bool _debugActive;
    private Animator _jetFireAnim;
    private BoxCollider2D _jetFireCollider;


    // Start is called before the first frame update
    private void Awake()
    {
        Active = _debugActive;

        _jetFireAnim = GetComponent<Animator>();
        _jetFireAnim.SetBool("Fire", Active);

        _jetFireCollider = GetComponent<BoxCollider2D>();
    }

    public void Activate(GameObject activator)
    {
        Active = true;

        _jetFireAnim.SetBool("Fire", true);
        _jetFireCollider.enabled = true;
    }

    public void Deactivate(GameObject activator)
    {
        Active = false;

        _jetFireAnim.SetBool("Fire", false);
        _jetFireCollider.enabled = false;
    }
}
