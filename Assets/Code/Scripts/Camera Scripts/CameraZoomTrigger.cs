﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A component that can turn an area into a trigger for a new camera zoom level.
/// 
/// NOTE: It's important to ensure that there is ample room between triggers (minimum of character collider width),
/// otherwise the second trigger can activate before leaving the first trigger. It doesn't seem prudent waste time 
/// continually checking for this case when it is easy enough to avoid.
/// </summary>
[RequireComponent(typeof(Collider2D))]
//[RequireComponent(typeof(Rigidbody2D))]
public class CameraZoomTrigger : MonoBehaviour
{
    /// <summary>
    /// A backup value to use if the previous camera zoom isn't stored. This seems to happen if the player spawns inside a camera zoom trigger. The OnTriggerEnter call comes before the camera is setup, so the call for the current orthographic size fails.
    /// This should be temporary until a proper fix can be put in place.
    /// </summary>
    private const float BACKUP_BASE_CAMERA_ZOOM = 8.5f;


    /// <summary>
    /// The desired zoom level for the area.
    /// </summary>
    public float ZoomLevel;

    /// <summary>
    /// How long it should take to set the new zoom level.
    /// </summary>
    public float TimeToSetZoom;

    /// <summary>
    /// Whether the camera zoom level should return to its previous value once the zoom trigger is exited.
    /// </summary>
    public bool ReturnToPreviousZoomOnExit = false;


    private CinemachineBrain _cinemachineBrain;

    private CameraManager _cameraManager;

    /// <summary>
    /// Reference to the character collider that will activate the trigger.
    /// </summary>
    private Collider2D _characterCollider;

    /// <summary>
    /// The zoom level of the camera before entering the zoom trigger.
    /// </summary>
    private float _previousZoomLevel = -1;

    /// <summary>
    /// Temporary. Should only be necessary until the OnTriggerEnter issue is solved.
    /// </summary>
    private bool _zoomLevelSet = false;


	private void Start()
    {
        //_cameraFollow = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraFollow>();
        _cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
        _cameraManager = Camera.main.GetComponent<CameraManager>();
        _characterCollider = GlobalData.Player.GetComponent<Collider2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == GlobalData.PLAYER_TAG)
        {
            if (_previousZoomLevel == -1 && _cameraManager.CurrentVCamera != null)
            {
                //_previousZoomLevel = _cameraFollow.ZoomLevel;
                _previousZoomLevel = _cameraManager.CurrentVCamera.m_Lens.OrthographicSize;
            }

            if (other == _characterCollider)
            {
                _cameraManager.ChangeCameraZoom(ZoomLevel, TimeToSetZoom);
                _zoomLevelSet = true;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collider)
    {


        if (!_zoomLevelSet && collider == _characterCollider)
        {
            _cameraManager.ChangeCameraZoom(ZoomLevel, TimeToSetZoom);
            _zoomLevelSet = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (ReturnToPreviousZoomOnExit && collider == _characterCollider)
        {
            if (_previousZoomLevel == -1)
                _previousZoomLevel = BACKUP_BASE_CAMERA_ZOOM;

            _cameraManager.ChangeCameraZoom(_previousZoomLevel, TimeToSetZoom);
            _zoomLevelSet = false;
        }
    }
}
