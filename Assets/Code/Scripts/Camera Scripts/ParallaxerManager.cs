﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Experimental.Rendering.Universal;

public class ParallaxerManager : MonoBehaviour
{
    public List<Parallaxer> BackgroundParalaxers;
    public Light2D light;

    public 
    // Use this for initialization
    void Awake()
    {
        if(BackgroundParalaxers == null)
        {
            BackgroundParalaxers = gameObject.GetComponentsInChildren<Parallaxer>().ToList();
        }
    }

    public void ResetAll()
    {
       foreach(Parallaxer Paralaxer in BackgroundParalaxers)
       {
            Paralaxer.DoReset();
       }
    }


}
