﻿using UnityEngine;
using System.Collections;
using Cinemachine;
[RequireComponent(typeof(CinemachineTargetGroup))]
public class AddPlayerToTargetGroup : MonoBehaviour
{

    private void Awake()
    {
        GetComponent<CinemachineTargetGroup>().m_Targets[0].target = GlobalData.Player.transform;
    }
}
