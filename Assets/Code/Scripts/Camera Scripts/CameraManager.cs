﻿using Cinemachine;
using MovementEffects;
using System.Collections.Generic;
using UnityEngine;


public class CameraManager : MonoBehaviour
{

    public float ZoomLevel { get {  return _cameraZoomLevel; } }


    /// <summary>
    /// The main script thats runs a set of vitual cameras 
    /// </summary>
    private CinemachineBrain _cinemachineBrain;

    /// <summary>
    /// Whether the camera is in the process of switching follow targets.
    /// </summary>
    private bool _targetSwitching;

    /// <summary>
    /// How long the target switch should take.
    /// </summary>
    private float _targetSwitchSmoothTime;


    /// <summary>
    /// Whether the camera is in the process of switching zoom levels.
    /// </summary>
    private bool _zoomSwitching = false;

    /// <summary>
    /// The current (or target, if mid-change) zoom level of the camera.
    /// </summary>
    private float _cameraZoomLevel = GlobalData.DefultZoom;

    /// <summary>
    /// How long the camera zoom switch should take.
    /// </summary>
    private float _cameraZoomSmoothSpeed;

    /// <summary>
    /// The total height of the camera.
    /// </summary>
    private float _cameraHeight;

    /// <summary>
    /// The total width of the camera.
    /// </summary>
    private float _cameraWidth;

    private Vector2 _cameraExtends;

    /// <summary>
    /// The area in which the camera is allowed to move around.
    /// </summary>
    private Collider2D _activeCameraMovementArea;


    /// <summary>
    /// Trargets last pos of the traget used not to update bound unless the traget has moved
    /// </summary>
    private Vector3 _targetLastPos = Vector3.zero;

    /// <summary>
    /// Last traget the camera was following this is the one it will be set back to 
    /// </summary>
    private GameObject _lastTraget;



    //The box colliders sitting at the edges of the camera that indicate the aiming bounds.
    private BoxCollider2D _topOfScreen;
    private BoxCollider2D _bottomOfScreen;
    private BoxCollider2D _leftOfScreen;
    private BoxCollider2D _rightOfScreen;




    /// <summary>
    /// Return the active camera from the brain 
    /// </summary>
    public CinemachineVirtualCameraBase CurrentCamera
    {
        get
        {
            if(_cinemachineBrain == null)
            {
                _cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
            }
            return (CinemachineVirtualCameraBase)_cinemachineBrain.ActiveVirtualCamera;

        }
    }
    /// <summary>
    /// try to get the active camera as a CinemachineVirtualCamera may return null
    /// </summary>
    public CinemachineVirtualCamera CurrentVCamera
    {
        get
        {
            return (CinemachineVirtualCamera)_cinemachineBrain.ActiveVirtualCamera;

        }
    }

    private void Awake()
    {
       
        GlobalData.CameraManager = this;
        _cinemachineBrain = Camera.main.GetComponent<CinemachineBrain>();
        
    }

    public void Start()
    {
        _topOfScreen = transform.Find("Top of Screen").gameObject.GetComponent<BoxCollider2D>();
        _bottomOfScreen = transform.Find("Bottom of Screen").gameObject.GetComponent<BoxCollider2D>();
        _leftOfScreen = transform.Find("Left of Screen").gameObject.GetComponent<BoxCollider2D>();
        _rightOfScreen = transform.Find("Right of Screen").gameObject.GetComponent<BoxCollider2D>();

        GlobalData.Player.Dead += OnPlayerDead;
    }

    public void ActiveCameraChanged()
    {
        CurrentVCamera.m_Lens.OrthographicSize = _cameraZoomLevel;
    }

    /// <summary>
    /// Set A new Follow Target of all cameras in the set CleraShoot
    /// </summary>
    /// <param name="target">The transform of the object to follow</param>
    public void SetNewTraget(Transform target)
    {
        if (target != null)
        {
            CurrentCamera.Follow = target;
        }
    }

    public void LateUpdate()
    {

        #region Set Off Camera Bounds 

        _topOfScreen.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1, 0)) + new Vector3(0, _cameraHeight * 0.1f);
        _topOfScreen.size = new Vector2(_cameraWidth * 1.5f, 0.1f);

        _bottomOfScreen.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0, 0)) - new Vector3(0, _cameraHeight * 0.1f);
        _bottomOfScreen.size = new Vector2(_cameraWidth * 1.5f, 0.1f);

        _leftOfScreen.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0, 0.5f, 0)) - new Vector3(_cameraWidth * 0.1f, 0);
        _leftOfScreen.size = new Vector2(0.1f, _cameraHeight * 1.5f);

        _rightOfScreen.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(1, 0.5f, 0)) + new Vector3(_cameraWidth * 0.1f, 0);
        _rightOfScreen.size = new Vector2(0.1f, _cameraHeight * 1.5f);
        #endregion
    }

    /// <summary>
    /// Sets up and calls a co-routine to change the camera zoom level.
    /// </summary>
    /// <param name="newCameraZoom">The new camera zoom level.</param>
    /// <param name="smoothSpeed">How long the zoom adjustment should take.</param>
    public void ChangeCameraZoom(float newCameraZoom, float smoothSpeed)
    {
        if (_zoomSwitching) //If we are still in the process of switching to a new zoom level, cancel that before changing zoom levels again.
        {
            Timing.KillCoroutines("CameraZoom");
        }
        _cameraZoomLevel = newCameraZoom;
        _cameraZoomSmoothSpeed = smoothSpeed;
        _zoomSwitching = true;
        Timing.RunCoroutine(PerformCameraZoomChange(), "CameraZoom");
    }

    /// <summary>
    /// Recalculates the height and width of the camera in the event that the zoom level changes.
    /// </summary>
    private void RecalculateCameraHeightAndWidth()
    {
        CinemachineVirtualCamera cam = CurrentCamera as CinemachineVirtualCamera;
        if (cam)
        {
            _cameraHeight = cam.m_Lens.OrthographicSize * 2f;
            _cameraWidth = _cameraHeight * cam.m_Lens.Aspect;

            _cameraExtends = new Vector2(cam.m_Lens.OrthographicSize * Screen.width / Screen.height, cam.m_Lens.OrthographicSize);

        }
    }

    /// <summary>
    /// A co-routine that performs the camera zoom change in the desired time.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> PerformCameraZoomChange()
    {
        CinemachineVirtualCamera cam = CurrentCamera as CinemachineVirtualCamera;
        if (cam)
        {
            float currentZoom = cam.m_Lens.OrthographicSize;
            float percentageTimeElapsed = 0f;
            while (percentageTimeElapsed < 1f)
            {
                percentageTimeElapsed += Time.deltaTime / _cameraZoomSmoothSpeed;

                cam.m_Lens.OrthographicSize = Mathf.Lerp(currentZoom, _cameraZoomLevel, percentageTimeElapsed);
                RecalculateCameraHeightAndWidth();
                yield return 0f;
            }
            _zoomSwitching = false;
        }
    }

    private void OnPlayerDead()
    {
        CurrentCamera.Follow = null;
    }

    private void OnDestroy()
    {
        GlobalData.CameraManager = null; 
    }
}