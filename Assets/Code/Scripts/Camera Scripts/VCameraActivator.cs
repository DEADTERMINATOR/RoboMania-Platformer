﻿using UnityEngine;
using System.Collections;
using Cinemachine;
using Characters;
using Characters.Player;

public class VCameraActivator : MonoBehaviour, IActivatable
{
    public CinemachineVirtualCameraBase Camera;
    public CinemachineDollyCart DollyCart;
    public bool DeactivateOnDollyEnd;
    public float ActiveTime;

    public bool Active { get { return _active; } }

    private bool _active;
    private float _timeElapsed;
    private Character.CharacterEvent _deathDelegate;

    public void Activate(GameObject activator)
    {
        _deathDelegate = new Character.CharacterEvent(PlayerDied);
        GlobalData.Player.Dead += _deathDelegate;
        Camera.VirtualCameraGameObject.SetActive(true);
        if (DollyCart)
        {
            DollyCart.m_Position = 0;
            DollyCart.gameObject.SetActive(true);
        }
        _active = true;
    }

    public void Deactivate(GameObject activator)
    {
        GlobalData.Player.Dead -= _deathDelegate;
        Camera.VirtualCameraGameObject.SetActive(false);
        if (DollyCart)
        {
            DollyCart.m_Position = 0;
            DollyCart.gameObject.SetActive(false);
        }
        _active = false;
    }

    void PlayerDied()
    {
        Deactivate(null);
    }

    void Update()
    {
        if(_active)
        {
            if (DeactivateOnDollyEnd)
            {
                if(DollyCart.m_Path.MaxPos == DollyCart.m_Position)
                {
                    Deactivate(null);
                }
            }
            else
            {
                if (ActiveTime > 0)
                {
                    _timeElapsed += Time.deltaTime;
                    if (_timeElapsed >= ActiveTime)
                    {
                        Deactivate(null);
                        _active = false;
                    }
                }
            }
        }
    }
}
