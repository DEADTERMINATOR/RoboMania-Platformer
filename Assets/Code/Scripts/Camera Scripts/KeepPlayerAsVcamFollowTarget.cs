﻿using UnityEngine;
using System.Collections;
using Cinemachine;

[RequireComponent(typeof(CinemachineVirtualCameraBase))]
public class KeepPlayerAsVcamFollowTarget : MonoBehaviour
{
    private CinemachineVirtualCameraBase _camera;

    private void Awake()
    {
        _camera = GetComponent<CinemachineVirtualCameraBase>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(_camera)
        {
            if (_camera.Follow !=GlobalData.Player.transform)
            {
                _camera.Follow = GlobalData.Player.transform;
            }
        }

    }
}
