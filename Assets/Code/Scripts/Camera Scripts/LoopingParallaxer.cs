﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using LevelSystem;

public class LoopingParallaxer : Parallaxer
{
    public SpriteRenderer Sprite;
    // used as away to add a buffer to an image
    public Rect MovmentBounds;
    private Vector3 DefualtPos;
    protected override void Awake()
    {
        base.Awake();
        DefualtPos = Sprite.transform.position;
        Level.CurentLevel.OnLevelLoaded.AddListener(SetUpOnLevelLoaded);
        overrideMovment = true;
    }
    protected override void SetUpOnLevelLoaded()
    {
        Sprite.transform.localPosition = DefualtPos;
        lastCameraPos = Camera.transform.position;//set the pos 
    }
    public override void DoReset()
    {
        Sprite.transform.localPosition = DefualtPos;
        transform.localPosition = Vector3.zero;
    }
    public override void Update()
    {
        base.Update();
        // transform.localPosition = sprite.transform.localPosition + ScaledDelta;
        if (Delta != Vector3.zero)
        {
            bool Left = Delta.x < 0;
            bool horizontalMovement = (Delta.x != 0);
            bool verticalMovement = (Delta.y != 0);
            bool Up = Delta.y < 0;

            Sprite.transform.localPosition = Sprite.transform.localPosition + ScaledDelta;
            if (horizontalMovement && !IsOnScreenX(Sprite, Left))
            {
                if (Left)
                {
                    PlaceRightOffScreen(Sprite);
                }
                else
                {
                    PlaceLeftOffScreen(Sprite);
                }
            }
            if (verticalMovement && !IsOnScreenY(Sprite, Up))
            {
                if (Up)
                {
                    PlaceDownOffScreen(Sprite);
                }
                else
                {
                    PlaceUpOffScreen(Sprite);
                }
            }
        }

    }

    private bool IsOnScreenX(SpriteRenderer image, bool left = true)
    {

        if (left)
        {
            Vector3 viewportPos = Camera.main.WorldToViewportPoint(image.transform.position + (Vector3.right * (MovmentBounds.width / 2)));
            ; if (viewportPos.x < 1 && viewportPos.x > 0)
            {
                return true;
            }
            return false;
        }
        else
        {
            Vector3 viewportPos = Camera.main.WorldToViewportPoint(image.transform.position + (Vector3.left * (MovmentBounds.width/2)));
            if (viewportPos.x < 1 && viewportPos.x > 0)
            {
                return true;
            }
            return false;
        }
    }
    private bool IsOnScreenY(SpriteRenderer image, bool left = true)
    {

        if (left)
        {
            Vector3 viewportPos = Camera.main.WorldToViewportPoint(image.transform.position + (Vector3.down *( MovmentBounds.height/2)));
            if (viewportPos.x < 1 && viewportPos.x > 0)
            {
                return true;
            }
            return false;
        }
        else
        {
            Vector3 viewportPos = Camera.main.WorldToViewportPoint(image.transform.position + (Vector3.up *( MovmentBounds.height/2)));
            if (viewportPos.x < 1 && viewportPos.x > 0)
            {
                return true;
            }
            return false;
        }
    }

    private bool PlaceRightOffScreen(SpriteRenderer image, float buffer = 0)
    {
        Vector3 edgePos = Camera.main.ViewportToWorldPoint(Vector3.one);
        image.gameObject.transform.position = new Vector3(edgePos.x + (MovmentBounds.width / 2), image.transform.position.y, 1);

        return false;
    }
    private bool PlaceLeftOffScreen(SpriteRenderer image, float buffer = 0)
    {
        Vector3 edgePos = Camera.main.ViewportToWorldPoint(Vector3.zero);
        image.gameObject.transform.position = new Vector3(edgePos.x - (MovmentBounds.width / 2), image.transform.position.y, 1);

        return false;
    }

    private bool PlaceUpOffScreen(SpriteRenderer image, float buffer = 0)
    {
        Vector3 edgePos = Camera.main.ViewportToWorldPoint(Vector3.one);
        image.gameObject.transform.position = new Vector3(image.transform.position.x, edgePos.y + (MovmentBounds.height / 2), 1);

        return false;
    }
    private bool PlaceDownOffScreen(SpriteRenderer image, float buffer = 0)
    {
        Vector3 edgePos = Camera.main.ViewportToWorldPoint(Vector3.zero);
        image.gameObject.transform.position = new Vector3(image.transform.position.x, edgePos.y - (MovmentBounds.height / 2), 1);

        return false;
    }
}

