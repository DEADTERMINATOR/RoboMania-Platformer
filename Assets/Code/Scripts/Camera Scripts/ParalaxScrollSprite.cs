﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Characters.Player;

[System.Serializable]
public struct LayerInfoSprite
{
    public SpriteRenderer img1;
    public SpriteRenderer img2;
    public float factor;
    public bool loaded;
    public float[] offsets;
    public bool oneIsMain;


}

public class ParalaxScrollSprite : MonoBehaviour
{

    public List<LayerInfoSprite> layers = new List<LayerInfoSprite>(); 
    public PlayerMaster Player;
    public Vector3 lastPlayerPos;

    // Use this for initialization
    void Start()
    {
        Player = StaticTools.GetPlayerRef();
        lastPlayerPos = Player.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 delta = lastPlayerPos - Player.transform.localPosition;
        lastPlayerPos = Player.transform.localPosition;

        foreach (LayerInfoSprite layer in layers)
        {
            Vector3 screenPos = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().WorldToScreenPoint(layer.img1.transform.position);
            if(layer.oneIsMain && !layer.img1.isVisible)
            {
               /* layer.img1.transform.localPosition = layer.img2.transform.localPosition +=  new Vector3(layer.img1.rectTransform.rect.width,0,0);
                layer.offsets[0] = 0;*/
            }
            if ( !layer.img1.isVisible)
            {
                /*layer.img2.transform.localPosition = layer.img1.transform.localPosition += new Vector3(layer.img1.rectTransform.rect.width, 0, 0);
                layer.offsets[1] = 0;*/
            }
            layer.img1.transform.localPosition = layer.img1.transform.localPosition + new Vector3(delta.x * layer.factor, delta.y * layer.factor/2);
            layer.img2.transform.localPosition = layer.img2.transform.localPosition + new Vector3(delta.x * layer.factor, delta.y * layer.factor/2);
            if (layer.oneIsMain)
            {
                layer.offsets[0] += delta.x;
            }
            else
            {
                layer.offsets[1] += delta.x;
            }
        }
    }
}
