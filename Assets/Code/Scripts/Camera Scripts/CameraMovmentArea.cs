﻿using UnityEngine;
using System.Collections;
using Cinemachine;
using LevelSystem;
using System.Collections.Generic;

public class CameraMovmentArea : MonoBehaviour
{
    /// <summary>
    /// The Virtual Camera that is the main camera other cameras can be part of the area but should be activated speratly 
    /// </summary>
    public CinemachineVirtualCameraBase MainMovmentCamera;
    /// <summary>
    /// The Virtual Camera that is the main camera other cameras can be part of the area but should be activated speratly 
    /// </summary>
    public CinemachineVirtualCameraBase LookCamera;
    /// <summary>
    /// The Background index this area will use
    /// </summary>
    public int BackgroundIndex = 0;
    /// <summary>
    /// Do not set the background
    /// </summary>
    public bool SetbackGround = true;
    /// <summary>
    /// The last activated area could be null
    /// </summary>
    public static CameraMovmentArea ActiveMovmentArea { get; private set; }

    public AudioSource AmbianceSound;

    public static List<CameraMovmentArea> CameraMovmentAreas { get; private set; } = new List<CameraMovmentArea>();



    public static bool BlockCameraChanges
    {
        get 
        {
            return _BlockCameraChanges;
        }
        set
        {
             if(!value)
            {
                if(_heldChange != -1 && _heldChange != ActiveMovmentArea._index)
                {
                    ActiveMovmentArea.SetAsInactive();
                    CameraMovmentAreas[_heldChange].SetAsActive();
                    _heldChange = -1;
                }
            }
            _BlockCameraChanges = value;
        }
    }

    private int _index;
    private static bool _BlockCameraChanges;
    private static int _heldChange = -1;

    private void Awake()
    {
        MainMovmentCamera.gameObject.SetActive(false);
        CameraMovmentAreas.Add(this);
        _index = CameraMovmentAreas.IndexOf(this);
    }
    private void Start()
    {
        //Level.CurentLevel.OnLevelReloaded.AddListener(OnLevelWasLoaded);
        MainMovmentCamera.Follow = GlobalData.PlayerTransfrom;
    }

    public void SetAsActive()
    {
        ActiveMovmentArea = this;
        MainMovmentCamera.gameObject.SetActive(true);
        MainMovmentCamera.Follow = GlobalData.PlayerTransfrom;
        if(AmbianceSound)
        {
            AmbianceSound.Play();
        }
        if (SetbackGround && BackgroundManager.Activeindex != -1 && BackgroundManager.Activeindex != BackgroundIndex)
        {
            BackgroundManager.TryAndChnageBackGroundWithKey(BackgroundIndex);
        }
    }
    public void SetAsInactive()
    {
        MainMovmentCamera.gameObject.SetActive(false);  
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!_BlockCameraChanges)
        {
            if (AmbianceSound)
            {
                AmbianceSound.Play();
            }

            if (!MainMovmentCamera.gameObject.activeInHierarchy && collision.gameObject.layer == GlobalData.PLAYER_LAYER)
            {
                ActiveMovmentArea = this;
                MainMovmentCamera.gameObject.SetActive(true);
                if (SetbackGround && BackgroundManager.Activeindex != -1 && BackgroundManager.Activeindex != BackgroundIndex)
                {
                    BackgroundManager.TryAndChnageBackGroundWithKey(BackgroundIndex);
                }
                //GlobalData.LevelInformation.ActiveCameraMovementArea = this;
            }
        }
        else
        {
            _heldChange = _index;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (!_BlockCameraChanges)
        {
            if (AmbianceSound && !AmbianceSound.isPlaying)
            {
                AmbianceSound.Play();
            }
            if (!MainMovmentCamera.gameObject.activeInHierarchy && other.tag == GlobalData.Player.tag)
            {
                if (!LookCamera.gameObject.activeInHierarchy && !MainMovmentCamera.gameObject.activeInHierarchy)
                {
                    MainMovmentCamera.gameObject.SetActive(true);
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!_BlockCameraChanges)
        {
            if (AmbianceSound)
            {
                AmbianceSound.Stop();
            }
            if (MainMovmentCamera.gameObject.activeInHierarchy && collision.tag == GlobalData.Player.tag)
            {

                MainMovmentCamera.gameObject.SetActive(false);
            }
        }
    }  
}
