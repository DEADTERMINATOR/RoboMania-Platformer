﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Scales the background sprite so that it sufficiently covers the entire screen, no matter the resolution or aspect ratio.
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class ScaleBackgroundImage : MonoBehaviour
{
    /// <summary>
    /// The camera the background sprite is attached to.
    /// </summary>
    public Camera theCamera;


    /// <summary>
    /// The current width of the screen.
    /// </summary>
    private float _screenWidth;

    /// <summary>
    /// The current height of the screen.
    /// </summary>
    private float _screenHeight;


    //Use this for initialization.
    private void Start()
    {
        if (theCamera != null)
        {
            Resize();
        }
    }

    private void Update()
    {
        if (!Mathf.Approximately(Screen.width, _screenWidth) || !Mathf.Approximately(Screen.height, _screenHeight))
        {
            Resize();
        }
    }

    /// <summary>
    /// Resize the attached sprite according to the camera view.
    /// </summary>
    private void Resize()
    {
        SpriteRenderer background = GetComponent<SpriteRenderer>();

        _screenWidth = Screen.width;
        _screenHeight = Screen.height;

        float desiredXScale = Screen.width / background.sprite.bounds.size.x;
        float desiredYScale = Screen.height / background.sprite.bounds.size.y;
        transform.localScale = new Vector3(desiredXScale, desiredYScale, 1f);
    }
}
