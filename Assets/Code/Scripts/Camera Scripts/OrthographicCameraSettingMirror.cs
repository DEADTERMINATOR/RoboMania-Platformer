﻿using UnityEngine;
public class OrthographicCameraSettingMirror : MonoBehaviour
{

    public Camera MasterCamera;
    public Camera MirrorCamera;

    private void Update()
    {
        MirrorCamera.orthographicSize = MasterCamera.orthographicSize;
    }
}
