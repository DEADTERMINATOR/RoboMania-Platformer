﻿using UnityEngine;
using System.Collections;
using Cinemachine;
using GameMaster;
/// <summary>
/// Lets the player look ahead
/// Issues to be addressed
/// Switching the directing while look take too long 
/// should it be a hard cut or transition
/// maybe use the main camera center as distance cap point
/// </summary>
public class CameraLookAhead : MonoBehaviour
{
    /// <summary>
    /// The to point the Cinemachine will look at needs to be a gameobject 
    /// </summary>
    /// 
    [Tooltip("The to point the Cinemachine will look at needs to be a gameobject ")]
    public Transform LookTransfrom;
    /// <summary>
    /// How Fast the camera will move 
    /// </summary>
    public float Speed = 1.2f;
    /// <summary>
    /// Should the camera be capped to being a max distance from the players 
    /// </summary>
    public bool CapLookAmount = false;
    /// <summary>
    /// The max distance the camera look can go 
    /// </summary>
    public float CapedDistance = 50;

    private bool _HasInput = false;

    // Update is called once per frame
    void Update()
    {
        if (!LoadingLocks.IsLocked())
        {
            bool lastHasInputSate = _HasInput;
            Vector3 movment = Vector3.zero;
            _HasInput = false;
            ///Collect input with inverse keys canceling each other out 
            if (Input.GetButton("LookAhedUp"))
            {
                movment.y++;
                _HasInput = true;
            }
            if (Input.GetButton("LookAhedDown"))
            {
                movment.y--;
                _HasInput = true;
            }
            if (Input.GetButton("LookAhedLeft"))
            {
                movment.x--;
                _HasInput = true;
            }
            if (Input.GetButton("LookAhedRight"))
            {
                movment.x++;
                _HasInput = true;
            }
            // turn in to a unity vector as not to over move then scale by speed
            movment = movment.normalized * (Speed * Time.deltaTime);

            if (_HasInput && !lastHasInputSate)// we just got input 
            {
                if (CameraMovmentArea.ActiveMovmentArea != null)//we have a valid active area
                {
                    CameraMovmentArea.ActiveMovmentArea.LookCamera.gameObject.SetActive(true);
                    CameraMovmentArea.ActiveMovmentArea.LookCamera.Follow = LookTransfrom;
                    CameraMovmentArea.ActiveMovmentArea.MainMovmentCamera.gameObject.SetActive(true);
                    movment *= 3;//start on the player get it moving faster
                }
            }
            if (!_HasInput && lastHasInputSate)// we just lost input 
            {
                if (CameraMovmentArea.ActiveMovmentArea != null)//we have a valid active area
                {
                    CameraMovmentArea.ActiveMovmentArea.LookCamera.gameObject.SetActive(false);
                    CameraMovmentArea.ActiveMovmentArea.MainMovmentCamera.gameObject.SetActive(true);

                }
            }
            if (!_HasInput && CameraMovmentArea.ActiveMovmentArea != null)// no input 
            {
                LookTransfrom.position = CameraMovmentArea.ActiveMovmentArea.MainMovmentCamera.transform.position;
            }

            if (_HasInput)
            {
                if (CapLookAmount && Vector3.Distance(CameraMovmentArea.ActiveMovmentArea.MainMovmentCamera.transform.position, LookTransfrom.position) > CapedDistance)// the falg will stop the dist function runing when false
                {
                    return;// has been caped
                }
                //add the movement to the target 
                LookTransfrom.position += movment;
            }
        }
    }
}
