﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementAreaOverrideZone : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            GlobalData.Player.State.SetInsideCameraMovementAreaOverrideZone(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            GlobalData.Player.State.SetInsideCameraMovementAreaOverrideZone(false);
        }
    }
}
