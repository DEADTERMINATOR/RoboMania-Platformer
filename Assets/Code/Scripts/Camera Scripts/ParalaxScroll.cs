﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct LayerInfo
{
    public Image[] images;
    public float factor;
    [HideInInspector]
    public List<double> offsets;
    [HideInInspector]
    public int FirstImage;
    [HideInInspector]
    public int LastImage;
}


public class ParalaxScroll : MonoBehaviour
{
    /// <summary>
    /// The Layers of the Paralax with 0 as the far back
    /// </summary>
    public List<LayerInfo> BackgroundLayers = new List<LayerInfo>();
    /// <summary>
    /// The Game Camera 
    /// </summary>
    public Camera MainCamera;

    public Vector3 lastPlayerPos;
    public int screenbuffer = 10;
    /// <summary>
    /// 
    /// </summary>
    public Vector3 BackGroundAncor;


    // Use this for initialization
    void Start()
    {
        MainCamera = GameObject.FindObjectOfType<Camera>();
        lastPlayerPos = MainCamera.transform.localPosition;///Player.transform.localPosition;
       
        for (int i = 0; i < BackgroundLayers.Count; i++)
        {
            LayerInfo layer = BackgroundLayers[i];
            layer.FirstImage = 0;
            layer.LastImage = layer.images.Length - 1;
            layer.offsets = new List<double>(); 

            for (int ii = 0; ii < layer.images.Length; ii++)
                layer.offsets.Add( layer.images[0].rectTransform.rect.width * (layer.LastImage - ii));

            BackgroundLayers[i] = layer;
        }
    }

    public void AncorToLevel(Vector3 pos)
    {
        BackGroundAncor = pos;
        transform.position = pos;
    }

    private int GetClampedIndex(int index, LayerInfo layer)
    {
        if (index < 0)
        {
            return layer.images.Length - 1;
        }
        return index % layer.images.Length;
    }
    // Update is called once per frame
    void Update()
    {
        if (GameMaster.GameManager.Instance.LoadState == GameMaster.GameManager.LoadingState.LOADED && GlobalData.Player.State != null && !GlobalData.Player.State.Dead)//Make sure we should move the background
        {
            Vector3 delta = lastPlayerPos - MainCamera.transform.localPosition; //lastPlayerPos - Player.transform.localPosition;
            lastPlayerPos = MainCamera.transform.localPosition;///Player.transform.localPosition;

            for (int i = 0; i < BackgroundLayers.Count; i++)
            {
                LayerInfo layer = BackgroundLayers[i];
                if (delta.x < 0)
                {

                    if (layer.offsets[layer.FirstImage] >= (layer.images[layer.FirstImage].rectTransform.rect.width * layer.images.Length) + screenbuffer)
                    {
                        layer.images[layer.FirstImage].transform.localPosition = new Vector3(layer.images[layer.LastImage].transform.localPosition.x + layer.images[layer.LastImage].rectTransform.rect.width, layer.images[layer.LastImage].transform.localPosition.y, layer.images[layer.LastImage].transform.localPosition.z);
                        layer.offsets[layer.FirstImage] = 0;
                        layer.LastImage = layer.FirstImage;
                        layer.FirstImage = GetClampedIndex(layer.FirstImage + 1, layer);
                    }
                }
                else if (delta.x > 0)
                {
                    if (layer.offsets[layer.LastImage] <= 0 - screenbuffer)
                    {
                        layer.images[layer.LastImage].transform.localPosition = new Vector3(layer.images[layer.FirstImage].transform.localPosition.x - layer.images[layer.FirstImage].rectTransform.rect.width, layer.images[layer.FirstImage].transform.localPosition.y, layer.images[layer.FirstImage].transform.localPosition.z);
                        layer.offsets[layer.LastImage] = layer.images[layer.LastImage].rectTransform.rect.width * layer.images.Length;
                        int lastmoved = layer.LastImage;
                        layer.LastImage = GetClampedIndex(layer.LastImage - 1, layer);
                        layer.FirstImage = lastmoved;
                    }

                }

                for (int ii = 0; ii < layer.images.Length; ii++)
                {
                    layer.images[ii].transform.localPosition = layer.images[ii].transform.localPosition + new Vector3(delta.x * layer.factor, delta.y * (layer.factor / 124));
                    layer.offsets[ii] -= delta.x * layer.factor;
                }
                BackgroundLayers[i] = layer;
            }
        }
        else
        {
            lastPlayerPos = MainCamera.transform.localPosition; 
        }
    }
}
