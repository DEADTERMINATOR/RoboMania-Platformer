﻿using UnityEngine;
using System.Collections;
using LevelSystem;

public class ParallaxObject : MonoBehaviour
{
    public Transform trakingTransfrom;
    public bool ISAtFinal = true;
    public Vector2 Speed;
    private Vector3 _finalPos;
    private Vector3 _startPOs;
    private Vector3 _lastCameraPos;
    /// <summary>
    /// cal the pos this OBject need to start to be at the place point when the player it at the same point 
    /// </summary>
    private void CalcStartPos()
    {
        ///The player   start position - 
        float StartX = (_finalPos.x - Level.CurentLevel.PlayerSpwnPoint.x );
        StartX *= Speed.x;
        float StartY = (_finalPos.y - Level.CurentLevel.PlayerSpwnPoint.y );
        StartY *= Speed.y;
        _startPOs = _finalPos + new Vector3(StartX, StartY, 0);
    }


    // Use this for initialization
    void Start()
    {
        trakingTransfrom = Camera.main.transform; ///GlobalData.Player.transform;
        if (ISAtFinal)
        {
            _finalPos = transform.position;

            CalcStartPos();

            this.transform.position = _startPOs;
        }
        _lastCameraPos = trakingTransfrom.position;

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 Delta = trakingTransfrom.position - _lastCameraPos;
        _lastCameraPos = trakingTransfrom.position;
        Delta.Scale(Speed);
        this.transform.position -= Delta;
    }
}
