﻿using UnityEngine;
using System.Collections;
using LevelSystem;
/// <summary>
/// Remade Prallaxing system a screen space canvas is no longer used now the spites are children of the camera the main  
//new script BackgroundParalaxer that is added to sprites to be paralaxed.BackgroundParalaxerCollection is added to a parent node that contains all the BackgroundParalaxer as children this allows the background manager to reset the offset on a reset.
/// </summary>
public class Parallaxer : MonoBehaviour
{
    

    /// <summary>
    /// How Fast this move in relation to the main camera on X and Y
    /// </summary>
    public Vector2 MovementFactor;
    /// <summary>
    /// The camera to track if left null Camera.main is used
    /// </summary>
    public Camera Camera;
    protected Vector3 Delta { get; private set; }
    protected Vector3 ScaledDelta { get; private set; }
    /// <summary>
    /// The cameras last pos;
    /// </summary>
    protected bool overrideMovment = false;
    protected Vector3 lastCameraPos = Vector3.zero;
    // Use this for initialization

    protected virtual void Awake()
    {
        // Camera need to have moved to the player start point 
        Level.CurentLevel.OnLevelLoaded.AddListener(SetUpOnLevelLoaded);
    }

    void Start()
    {
        if (Camera == null)
        {
            Camera = Camera.main;
        }

    }

    protected virtual void SetUpOnLevelLoaded()
    {
         lastCameraPos = Camera.transform.position;//set the pos 
    }
    public virtual void DoReset()
    {
        lastCameraPos = Camera.transform.position;//set the pos 
        transform.localPosition = Vector3.zero;
    }

    // Update is called once per frame
   public virtual void Update()
    {
        if (lastCameraPos != Vector3.zero)
        {
            Delta = lastCameraPos - Camera.transform.position;
            lastCameraPos = Camera.transform.position;
       
        }else
        {
            Delta = Vector3.zero;
        }
        ScaledDelta = new Vector3(Delta.x * MovementFactor.x, Delta.y * MovementFactor.y, 0);
        if (!overrideMovment)
        {
            transform.localPosition = transform.localPosition + ScaledDelta;
        }
    }
}
