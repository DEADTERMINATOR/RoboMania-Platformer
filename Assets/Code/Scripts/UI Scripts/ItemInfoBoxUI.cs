﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class  ItemInfoBoxUI: MonoBehaviour 
{
    public ItemInfoBox currentInfoBox;
    public ItemInfoBox equipedInfoBox;


    // Use this for initialization
    void Start ()
	{
        Hide();

    }

    

    public void Hide()
    {
        if(currentInfoBox)
        currentInfoBox.gameObject.SetActive(false);
        if(equipedInfoBox)
        equipedInfoBox.gameObject.SetActive(false);
    }
}
