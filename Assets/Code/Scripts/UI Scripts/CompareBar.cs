﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CompareBar : MonoBehaviour
{
    public Image BarCover;
    public Image MainBar;
    public Image PositiveBar;
    public Image NagativeBar;
    public Text StatText;
    public Text DiffreanceText;


    // Use this for initialization
    void Start()
    {

    }

    /// <summary>
    /// Set the Text color from the rarity 
    /// </summary>
    /// <param name="rarity"></param>
    public void SetRarity(Rarity rarity)
    {
        StatText.color = GlobalData.GetDisplayColorFromRarity(rarity);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="color"></param>
    public void SetTextColor(Color color)
    {
        StatText.color = color;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mainvalue">The percentage at a float from 0 - 1  Clamped to that range </param>
    /// <param name="diffValue"> The percentage at a float from 0 - 1 Clamped to that range </param>
    public void Compare(float main, float mainMax , float diff,float diffMax, bool onlyMainBar = false)
    {
        float mainvalue, diffValue;
        //Get the Vlaues As Percentage 
        mainvalue = main / mainMax;
        diffValue = diff / diffMax; 

        //Reset the values 
        NagativeBar.fillAmount = 0;
        MainBar.fillAmount = 0;
        PositiveBar.fillAmount = 0;
        DiffreanceText.text = "";
        // Clamp The Values to make sure the are in range 
        mainvalue = Mathf.Clamp(mainvalue, 0, 1);
        diffValue = Mathf.Clamp(diffValue, 0, 1);

        StatText.text = string.Format("{0:F2}", main);
        
        if (diffValue == 0 || diffValue == mainvalue)// if the compare is zero just act as a normal bar 
        {
            MainBar.fillAmount = mainvalue;
        }
        else if(diff > main)// The diff is bigger 
        {
            if (onlyMainBar)
            {
                PositiveBar.fillAmount = 0;
                MainBar.fillAmount = mainvalue;
            }
            else
            {
                NagativeBar.fillAmount = diffValue;
                MainBar.fillAmount = mainvalue;
                DiffreanceText.text = string.Format("-{0:F2}", Mathf.Abs(main - diff));
                DiffreanceText.color = Color.red;
            }
     

        }
        else
        {
            if (onlyMainBar)
            {
                NagativeBar.fillAmount = 0;
                MainBar.fillAmount = mainvalue;
            }
            else
            {
                    PositiveBar.fillAmount = mainvalue;
                    MainBar.fillAmount = diffValue;
                    DiffreanceText.text = string.Format("+{0:F2}", Mathf.Abs(main - diff));
                    DiffreanceText.color = Color.green;
            }

         
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="mainvalue">The percentage at a float from 0 - 1  Clamped to that range </param>
    /// <param name="diffValue"> The percentage at a float from 0 - 1 Clamped to that range </param>
    public void Compare(float main, float mainMax, float DiffreancePrecent)
    {
        float mainvalue, diffValue;
        mainvalue = main / mainMax;
        diffValue = DiffreancePrecent;

        //Reset the values 
        DiffreanceText.text = "";
        NagativeBar.fillAmount = 0;
        MainBar.fillAmount = 0;
        PositiveBar.fillAmount = 0;
        // Clamp The Values to make sure the are in range 
        mainvalue = Mathf.Clamp(mainvalue, 0, 1);
        diffValue = Mathf.Clamp(diffValue, 0, 1);
        StatText.text = string.Format("{0:F2}", main);

        if (diffValue == 0 || diffValue == mainvalue)// if the compare is zero just act as a normal bar 
        {
            MainBar.fillAmount = mainvalue;
        }
        else if (diffValue > mainvalue)// The dif is bigger 
        {
            PositiveBar.fillAmount = diffValue;
            MainBar.fillAmount = mainvalue;
            DiffreanceText.text = "";
        }
        else
        {
            NagativeBar.fillAmount = mainvalue ;
            MainBar.fillAmount = diffValue;
            DiffreanceText.text = "";
        }
    }

    
}
