﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ProjectileTypeDisplay : MonoBehaviour {
    public Image ReflectionIcon;
    public Image AmsourbIcon;


    
    public void Hide()
    {
        ReflectionIcon.enabled = false;
        AmsourbIcon.enabled = false;
    }
    public void SetColour(Color color)
    {
        ReflectionIcon.color = color;
        AmsourbIcon.color = color;
    }
	// Use this for initialization
	void Start () {
        ReflectionIcon.enabled = true;
        AmsourbIcon.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
