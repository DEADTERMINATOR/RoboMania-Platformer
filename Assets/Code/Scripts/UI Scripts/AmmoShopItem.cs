﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Characters.Player;

public class AmmoShopItem : OldShopItem
{
    /// <summary>
    /// The type of ammom
    /// </summary>
    public PlayerData.Ammo.AmmoTypes ammoType;

    /// <summary>
    /// 
    /// </summary>
    public bool overridePriceData = false;
    /// <summary>
    /// The discount to be aplied to normal pricing ie 0.1 is 10% disscount
    /// </summary>
    public float discount;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        if(!overridePriceData && GlobalData.Player.Data.AmmoInventory != null)
            //get the price and apply discount
            Price = Mathf.RoundToInt(GlobalData.Player.Data.AmmoInventory.GetPriceOfAmmo(ammoType, Amount)*(1-discount));

        ///Set the display text to #rounds type Rounds : price(S) eg = 10 Bullet Rounds : 33(S) 
        displayText.text = string.Format("{1} {0} Rounds : {2}(S)", StaticTools.FirstLetterToUpper(ammoType.ToString()), Amount, Price);
    }
    

    public override void BuyClick()
    {
        if (GlobalData.Player.Data.TotalScrap >= Price)
        {
            GlobalData.Player.Data.RequestScrap(Price);
            GlobalData.Player.Data.AmmoInventory.AddAmmo(ammoType, Amount);
        }
    }

    public override void OnSelect()
    {
        backgroundImage.color = Color.gray;
        _currentShop.UpdateText.text = string.Format("{0}:{1}", StaticTools.FirstLetterToUpper(ammoType.ToString()), GlobalData.Player.Data.AmmoInventory.GetCurrentAmmoCount(ammoType));
    }

    public override void OnDeSelect()
    {
        backgroundImage.color = Color.white;
        _currentShop.UpdateText.text = "";

    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
      //  _currentShop.SetSelected(this);
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        

    }
}
