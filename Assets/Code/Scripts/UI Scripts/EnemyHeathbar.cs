﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Characters.Enemies;

public class EnemyHeathbar : MonoBehaviour
{
    public Image FillImage;
    public Image FillImageMove;
    public Enemy Enemy;
    private float _actualFill =1;
    private float _currentFill =1;
  
    // Update is called once per frame
    void Update()
    {
        _actualFill = Enemy.Health / Enemy.MaxHealth;
        if(_actualFill < _currentFill)
        {
            if(_currentFill - _actualFill >= 0.05f)
            {
                _currentFill -= 0.05f;
            }
            else
            {
                _currentFill = _actualFill;
            }
            
        }
        if(Enemy.Health <= 0)
        {
            _currentFill = 0;
        }
        FillImage.fillAmount = _actualFill;
        FillImageMove.fillAmount = _currentFill;
    }

    
}
