﻿using UnityEngine;
using System.Collections;

public class UIScroller : MonoBehaviour
{
    public GameObject theObject;
    public Vector3 ScrollAmount;
    public bool UseLocal = false;
    public float speed = 1.0f;
    public float time = 1;
    public bool Done =true;
    protected Vector3 _finalpos;
    protected Vector3 _startPos;
    protected float elapsedTime  =0;
    /// <summary>
    /// Will throw an exption if not in a valid run state
    /// </summary>
    public void Run()
    {
        if (!UseLocal)
        {
            _startPos = theObject.transform.position;
        }
        else
        {
            _startPos = theObject.transform.localPosition;
        }
        _finalpos = _startPos + ScrollAmount;
        InternalRun();
    }
    public void Run(Vector3 offset,GameObject obj)
    {
        ScrollAmount = offset;
        theObject = obj;
        if (!UseLocal)
        {
            _startPos = theObject.transform.position;
        }
        else
        {
            _startPos = theObject.transform.localPosition;
        }
        _finalpos = _startPos + ScrollAmount;
        InternalRun();
    }
    public void Run(Vector3 startPos,Vector3 endPos, GameObject obj)
    {
        _startPos = startPos;
        _finalpos = endPos;
        ScrollAmount = startPos-endPos;
        theObject = obj;

        InternalRun();
    }
    private void InternalRun()
    {   if (theObject == null)
            throw new System.Exception() ;
        if (time <= 0 || time >= 10)
            throw new System.Exception();
        if (_finalpos == _startPos)
            return;
        //if exeaction made it this far we are in a vlid state

        elapsedTime = 0;
        Done = false;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(!Done)//then run
        {
            elapsedTime += Time.unscaledDeltaTime;
            if (!UseLocal)
            {
                theObject.transform.position = Vector3.Lerp(_startPos, _finalpos, elapsedTime/time);
            }
            else
            {
                theObject.transform.localPosition = Vector3.Lerp(_startPos, _finalpos, elapsedTime/time);
            }
            if(elapsedTime >= time)
            {
                Done = true;
            }
        }
    }
}
