﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using static OverlayFader;

public class OverlayFader : MonoBehaviour
{
    public Image FadeImage;
    public OverlayFaderEvent OnFinished = new OverlayFaderEvent();
    public UnityEvent OnAboutToShow = new UnityEvent();


    public enum States { CLEAR, FADEINGIN, FULL, FADEINGOUT }
    private States _state = States.CLEAR;


    private float _fadeOutTime;
    private bool _autoFadeInAfter;
    private float _fadeInTime;
    private float _currentSFXTime = 0;
    private float _stayFulltime = 0;
    private Color32 _colourREf;
    private bool _didWarn = false;

    public void FadeIn(float fadeInTime = 1, bool autoFadeInAfter = false, float stayFulltime = 1, float fadeOutime = 0)
    {
        if (_state == States.CLEAR)
        {
            _fadeOutTime = fadeOutime;
            _autoFadeInAfter = autoFadeInAfter;
            _stayFulltime = stayFulltime;
            _fadeInTime = fadeInTime;
            _currentSFXTime = 0;
            _state = States.FADEINGIN;
            _didWarn = false;
        }

    }

    public void FadeOut(float fadeOutime = 0)
    {
        if (_state == States.FULL)
        {
            _fadeOutTime = fadeOutime;
            _state = States.FADEINGOUT;
            _didWarn = false;
        }
    }
    // Use this for initialization
    void Start()
    {
        FadeImage = GetComponentInChildren<Image>();
        _colourREf = FadeImage.color;
    }

    // Update is called once per frame
    void Update()
    {
        switch (_state)
        {
            case States.FADEINGIN:
                _colourREf.a = (byte)Mathf.Lerp(0, 255, _currentSFXTime / _fadeInTime);
                if (_currentSFXTime > _fadeInTime)
                {
                    OnFinished?.Invoke(States.FADEINGIN);
                    _state = States.FULL;
                    _currentSFXTime = 0;
                }
                break;
            case States.FULL:
                if (_autoFadeInAfter)
                {
                    if (_currentSFXTime > _stayFulltime)
                    {
                        OnFinished?.Invoke(States.FULL);
                        _state = States.FADEINGOUT;
                        _currentSFXTime = 0;
                    }
                }
                break;
            case States.FADEINGOUT:
                _colourREf.a = (byte)Mathf.Lerp(255,0, _currentSFXTime / _fadeOutTime);
                if(_colourREf.a <= 240 && !_didWarn )
                {
                    OnAboutToShow?.Invoke();
                    _didWarn = true;
                }
                if (_currentSFXTime > _fadeOutTime)
                {
                    OnFinished?.Invoke(States.FADEINGOUT);
                    _state = States.CLEAR;
                    _currentSFXTime = 0;
                }
                break;
        }
        FadeImage.color = _colourREf;
        if (_state != States.CLEAR)
        {
            _currentSFXTime += Time.deltaTime;
        }
    }
}
public class OverlayFaderEvent : UnityEvent<States>
{

}