﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmoShop : InGameShop
{
    protected override void Update()
    {
        base.Update();
        AmmoShopItem activeitem = (AmmoShopItem)items[index];
        UpdateText.text = string.Format("{0}:{1}", StaticTools.FirstLetterToUpper(activeitem.ammoType.ToString()), GlobalData.Player.Data.AmmoInventory.GetCurrentAmmoCount(activeitem.ammoType));
    }
}
