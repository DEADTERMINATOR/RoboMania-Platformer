﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// This script control the individual item info boxes 
/// </summary>
public class ItemInfoBox : MonoBehaviour
{
    public Text ItemName;
    public ShieldInfoUi ShildInfo;
   // public GunChipInfoUI GunChipInfo;
    public Image BackGround;
   

    public void SetTIntFrocRatity(PickupItem item)
    {
        //BackGround.color = PickupItem.UIRaityColors[(int)item.itemRarity];
    }
    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
