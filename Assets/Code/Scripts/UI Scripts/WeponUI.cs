﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeponUI : MonoBehaviour {

    public Image gunImage;
    public Image background;

    private bool _vis = true;
    void Toogle()
    {

        _vis = !_vis;
        gunImage.enabled = _vis;
        background.enabled = _vis; 
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
