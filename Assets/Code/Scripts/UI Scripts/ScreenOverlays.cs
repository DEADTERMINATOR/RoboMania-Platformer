﻿using UnityEngine;


namespace HUD
{
    public class ScreenOverlays : MonoBehaviour
    {
        public RepairHUD Repair;
        public HeathBarHUD HeathBar;
        public ScrapAmountHUD Scrap;
        public EnergyCellHUD energyCells;
        public GunInfoHUD gunInfo;
        public ShieldHUD shield;
        public EnemyHealthBarHUD enemayHealthBar;

        #region Overlays
        /// <summary>
        /// A refecance to the info box script
        /// </summary>
        public InGameInfoBoxUI infoBox;


        /// <summary>
        /// A refecance to the info box script
        /// </summary>
        public ItemInfoBoxUI itemInfoBoxUI;

        /// <summary>
        /// A refrence to the instance with safty may be null
        /// </summary>
        public static ScreenOverlays Instance { get { return _instance; } }

        private static ScreenOverlays _instance;

        public InGameShop AmmoShop;


        #endregion

        // Use this for initialization
        void Awake()
        {
            _instance = this;
        }



        public void DisplyInfoTextBox(string text, Vector3 pos)
        {
            infoBox.DisplayText.text = text;
            infoBox.showBox(-1, pos);
        }

        public void HideInfoBox()
        {
            infoBox.ShowTime = 0;
        }
    }
}


