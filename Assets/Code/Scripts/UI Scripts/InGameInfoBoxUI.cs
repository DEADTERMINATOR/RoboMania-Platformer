﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameInfoBoxUI : MonoBehaviour
{

    public Text DisplayText;
    public Image Background;
    public float ShowTime = 0;
    protected CanvasGroup canvasGroup;
    protected Vector3 position;
    public Camera WorldCam;

    public Color TextColour;
	// Use this for initialization
	void Start ()
	{
	    canvasGroup = GetComponent<CanvasGroup>();
        //WorldCam =  GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        Hide();

    }

    public virtual void MoveToPos()
    {
        Vector3 screenPos = Camera.main.WorldToScreenPoint(position);
        screenPos.y += 10;
        transform.position = screenPos;
       ;
    }
	// Update is called once per frame
	void Update () {
        if (ShowTime >= 0)
        {
            canvasGroup.alpha = 0;

        }
        else if (ShowTime == -1)
        {
            canvasGroup.alpha = 0.95f;
        }
	    else
        {
            canvasGroup.alpha = 0.95f;
            ShowTime -= Time.deltaTime;
	    }
	}

    void LateUpdate()
    {
        if (canvasGroup.alpha > 0)
        {
            MoveToPos();
        }
    }
    public void Hide()
    {
        canvasGroup.alpha = 0;
        ShowTime = -0;
    }
    public void Toggele()
    {
        if (ShowTime == 0)
        {
            ShowTime = -1;
        }

        if (ShowTime == -1)
        {
            ShowTime = 0;
        }
    }
    public void showBox(float time,Vector3 pos)
    {
        position = pos;
        ShowTime = time;
        MoveToPos();
    }
}
