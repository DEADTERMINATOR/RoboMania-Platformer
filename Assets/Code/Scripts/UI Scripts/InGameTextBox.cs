﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HUD;

public class InGameTextBox : MonoBehaviour {
    /// <summary>
    /// The text to show
    /// </summary>
    public string TipText;
    /// <summary>
    /// the offset of the infox bbox fron the origian of the triger
    /// </summary>
    public Vector3 offset;
    /// <summary>
    /// The colour of the text
    /// </summary>
    public Color32 TextColor;

    public bool OverrideBackgroundSize = false;
    public float BackgroundWidth = 135f;
    public float BackgroundHeight = 79.9f;

    public bool CanActivate = false;

    public GameObject theOBJToActivate;


    private bool InTrigger = false;

    public InGameUITextBox InGameUI;

    private Vector2 _baseBackgroundSize;


    private void Start()
    {
        _baseBackgroundSize = ScreenOverlays.Instance.infoBox.Background.rectTransform.sizeDelta;
    }

    public void ShowTip()
    {
        Vector3 infoPos = transform.position;
        infoPos += offset;

        if (OverrideBackgroundSize)
            ScreenOverlays.Instance.infoBox.Background.rectTransform.sizeDelta = new Vector2(BackgroundWidth, BackgroundHeight);
        else
            ScreenOverlays.Instance.infoBox.Background.rectTransform.sizeDelta = _baseBackgroundSize;

        ScreenOverlays.Instance.infoBox.DisplayText.color = TextColor;
        ScreenOverlays.Instance.infoBox.DisplayText.text = TipText;
        ScreenOverlays.Instance.infoBox.showBox(-1, infoPos);
    }

    private void Update()
    {
        if(InTrigger)
        {
            if (Input.GetButtonDown("Action"))
            {
                (theOBJToActivate.GetComponent<IActivatable>()).Activate(gameObject);
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            InTrigger = true;
            ShowTip();
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            InTrigger = false;
            ScreenOverlays.Instance.infoBox.Hide();

        }
    }

}
