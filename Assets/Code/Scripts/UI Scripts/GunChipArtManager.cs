﻿using UnityEngine;
using System.Collections;

public class GunChipArtManager : MonoBehaviour
{
    public Sprite commom;
    public Sprite uncommom;
    public Sprite rare;
    public Sprite epic;


    public Sprite GetSprite(Rarity rarity)
    {
        switch(rarity)
        {
            case Rarity.UNCOMMON:
                    return uncommom;
            case Rarity.RARE:
                return rare;
            case Rarity.EPIC:
                return epic;
            default:
                return commom;

        }
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
