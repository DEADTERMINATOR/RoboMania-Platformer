﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Characters.Player;

namespace HUD
{
    public class RepairHUD : MonoBehaviour
    {
        /// <summary>
        /// The image for repair that will be filled based on elapsed time of repair 
        /// </summary>
        public Image RepairBackround;
        /// <summary>
        /// The ouuter image of the repair UI
        /// </summary>
        public Image RepairOutline;
        /// <summary>
        /// The overlay image of the repir
        /// </summary>
        public Image RepairOverlay;

        /// <summary>
        /// Wanning about repair such as not being able to repair 
        /// </summary>
        public Text RepairInfoText;
        /// <summary>
        /// Is a rapir in prosses
        /// </summary>
        private bool _isplayerRepairing;
        /// <summary>
        /// UI String 
        /// </summary>
        private const string CANNOT_REPAIR_FROM_LACK_OF_SCRP = "You need more scrap To repair";
        /// <summary>
        /// UI String 
        /// </summary>
        private const string CANNOT_REPAIR_FROM_GAME_BLOCK = "You can not repair here";
        /// <summary>
        /// How Long Is The wanning text show on a repair fail
        /// </summary>
        private const int WANNRING_DISPLAY_TIME = 3;

        // Use this for initialization
        void Start()
        {
            //Make Sure we are in the startin state
            RepairOutline.enabled = false;
            RepairOverlay.enabled = false;
            RepairInfoText.enabled = false;
            RepairBackround.fillAmount = 0;
            ///Lisen For repair events
            GlobalData.Player.OnRepair += new PlayerMaster.StateChange(OnRepair);
            GlobalData.Player.OnRepairFailed += new PlayerMaster.StateChange(OnRepairFailed);


        }
        /// <summary>
        /// Handel An Repair Event
        /// </summary>
        /// <param name="positive">True Start False Stop</param>
        private void OnRepair(bool positive)
        {
            if (positive)
            {
                _isplayerRepairing = true;
                RepairBackround.fillAmount = 0;
                RepairOutline.enabled = true;
                RepairOverlay.enabled = true;
            }
            else
            {
                _isplayerRepairing = false;
                RepairOutline.enabled = false;
                RepairOverlay.enabled = false;

                RepairBackround.fillAmount = 0;
            }
        }

        void OnRepairFailed(bool reson)
        {
            RepairInfoText.enabled = true;
            if (reson)//the player can not affrod it 
            {
                RepairInfoText.text = CANNOT_REPAIR_FROM_LACK_OF_SCRP;
            }
            else// is blocked by the game 
            {
                RepairInfoText.text = CANNOT_REPAIR_FROM_GAME_BLOCK;
            }
            //Hide The Text after 5 seconds
            StartCoroutine(TimeOutWarringText());
        }


        IEnumerator TimeOutWarringText()
        {
            yield return new WaitForSeconds(WANNRING_DISPLAY_TIME);
            RepairInfoText.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (_isplayerRepairing)
            {
                RepairBackround.fillAmount = GlobalData.Player.Repair.timeElapsed / GlobalData.Player.Repair.secondsToRepair;
                RepairOverlay.transform.rotation = Quaternion.Euler(0, 0, 360 * RepairBackround.fillAmount);
            }
        }
    }
}
