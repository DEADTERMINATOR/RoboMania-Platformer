﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShieldPickUpUI : PickUpUI
{

    /// <summary>
    /// The Compare bar For the Power
    /// </summary>
    public CompareBar PowerCompareBar;
    /// <summary>
    /// The Singel stat Compare bar
    /// </summary>
    public CompareBar AltComPareBar;

    public GameObject SingleAltStatLayout;

    public GameObject MultiAltStatLayout;

    public Text PowerText;

    public Text AltStatText;



    private static ShieldPickUpUI _referance;

    public static ShieldPickUpUI Instance { get { return _referance; } }


    private void Awake()
    {
        //Allways set the last added to be the referace 
        _referance = this;
    }

    /// <summary>
    /// Shield
    /// </summary>
    /// <param name="sheild"></param>
    /// <param name=""></param>
    public void Show(Shield shield)
    {
        MultiAltStatLayout.SetActive(false);
        //SingleAltStatLayout.SetActive(true);

        PowerText.text = shield.MaxPower.ToString();


        Show();
    }


    
}
