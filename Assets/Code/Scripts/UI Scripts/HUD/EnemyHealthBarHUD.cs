﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace HUD
{
    public class EnemyHealthBarHUD : MonoBehaviour
    {
        /// <summary>
        /// The health bar image
        /// </summary>
        public Image HealthBar;
        /// <summary>
        /// The UI Connatiner to turn off and on 
        /// </summary>
        public GameObject Container;
        /// <summary>
        /// Is the Bar showing
        /// </summary>
        public bool Display;
        public float Amount { get { return _amount; } set { _amount = Mathf.Clamp(value, 0, 1); } }
        private float _amount;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Container.activeInHierarchy != Display)
            {
                Container.SetActive(Display);
            }

            if (Display)
            {
                HealthBar.fillAmount = Amount;
            }

        }
    }
}
