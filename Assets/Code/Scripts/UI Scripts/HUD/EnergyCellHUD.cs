﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HUD
{
    public class EnergyCellHUD : MonoBehaviour
    {
        /// <summary>
        /// The Sprites used to show the Number of Cells Index buy the Number of Cells -2 ie I 0 = 2 Cells
        /// </summary>
        public Sprite[] EnergyCellBars;
        /// <summary>
        /// The bars Image  
        /// </summary>
        public Image EnergyBar;
        /// <summary>
        /// The out line of The Energy Bars
        /// </summary>
        public Image EnergyOutline;


        void Update()
        {
            if (GlobalData.Player.Data.TotalEnergyCellEnergy == GlobalData.Player.Data.FreeCellEnergy)
            {
                EnergyBar.fillAmount = 1;
            }
            else { 
            EnergyBar.fillAmount = GlobalData.Player.Data.FreeCellEnergy/GlobalData.Player.Data.TotalEnergyCellEnergy ;
            }
        }
    }
}
