﻿using System.Collections;
using UnityEngine;
using Weapons.Player.Chips;

/// <summary>
/// Manages the unChipPickUpUI's 
/// </summary>
public class GunChipPickUpUIManager : MonoBehaviour
    {
    public GunChipPickUpUI MainWindow;
    public GunChipPickUpUI AboutToDroopWindow;
    public static GunChipPickUpUIManager Instance { get; private set; } 
    private void Awake()
    {
        Instance = this;

    }

    public void Show(GunChip chip)
    {
        MainWindow.Show(chip);
        if (GlobalData.Player.PlayerWeapon.ActiveForm.FirstSlottedGunChip != null && (!GlobalData.Player.PlayerWeapon.ActiveForm.CanSlotSecondGunChip || GlobalData.Player.PlayerWeapon.ActiveForm.SecondSlottedGunChip != null))
        {
            if (PlayerPickupHandler.GunChipToReplace == 1)
            {
                AboutToDroopWindow.Show(GlobalData.Player.PlayerWeapon.ActiveForm.FirstSlottedGunChip);
            }
            else
            {
                AboutToDroopWindow.Show(GlobalData.Player.PlayerWeapon.ActiveForm.SecondSlottedGunChip);
            }
        }
    }
     public void Hide()
    {
        MainWindow.Hide();
        AboutToDroopWindow.Hide();
    }
}
