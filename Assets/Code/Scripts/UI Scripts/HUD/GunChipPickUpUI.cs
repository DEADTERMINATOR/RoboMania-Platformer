﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Weapons.Player.Chips;

public class GunChipPickUpUI : PickUpUI
{
    public Text PositaveTraitName;
    public Text NagativeTraitName;
    public GunChip GunChipToDisplay;
    /// <summary>
    /// Fake singalton grabs the last refinance seen during awake    /// </summary>
    public static GunChipPickUpUI Instance { get; private set; }

    private readonly string _displayFormateStringPrecetage = "{0}  {1}  {2:#.00}%";
    private readonly string _displayFormateStringIntager = "{0}  {1}  {2}";


    private void Awake()
    {
        Instance = this;
    }
    /// <summary>
    /// Show the UI usign the given chip 
    /// </summary>
    /// <param name="chip">the chip to show</param>
    public void Show(GunChip chip)
    {
        if (chip != null)
        {
            GunChipToDisplay = chip;
            Rarity displayRarity = (Rarity)(int)chip.ChipRarity-1;
            SetNameAndRarity("Name Not implanted ", displayRarity);
            OtherNameText.text = "";
            Show();
        }
    }

    protected override void SetUPDataForShow()
    {
        switch (GunChipToDisplay.ImprovedProperty)
        {
            case Weapons.Player.WeaponFormProperties.WeaponProperties.AmmoCapacity:
            case Weapons.Player.WeaponFormProperties.WeaponProperties.ChargeTime:
                PositaveTraitName.text = string.Format(_displayFormateStringIntager, GunChipToDisplay.ImprovedPropertyName, "+", GunChipToDisplay.PropertyImprovementAmount);
                break;
            default:
                PositaveTraitName.text = string.Format(_displayFormateStringPrecetage, GunChipToDisplay.ImprovedPropertyName, "+", GunChipToDisplay.PropertyImprovementAmount *100);
                break;
        }
        if (GunChipToDisplay.ChipRarity != GunChip.RarityLevels.Epic)
            switch (GunChipToDisplay.ImprovedProperty)
            {
                case Weapons.Player.WeaponFormProperties.WeaponProperties.AmmoCapacity:
                case Weapons.Player.WeaponFormProperties.WeaponProperties.ChargeTime:
                    NagativeTraitName.text = string.Format(_displayFormateStringIntager, GunChipToDisplay.ImpairedPropertyName, "-", GunChipToDisplay.PropertyImpairmentAmount);
                    break;
                default:
                    NagativeTraitName.text = string.Format(_displayFormateStringPrecetage, GunChipToDisplay.ImpairedPropertyName, "-", GunChipToDisplay.PropertyImpairmentAmount*100);
                    break;
            }
    }
}
