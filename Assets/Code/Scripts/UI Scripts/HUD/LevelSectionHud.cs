﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSectionHud : MonoBehaviour
{
    public Text SectionText;
   
    // Use this for initialization
    void Start()
    {
       
        SectionText.text = LevelSectionScript.CurrentSectionName;
        LevelSectionScript.OnUpdated.AddListener(UpdateText);
    }

    // Update is called once per frame
    void UpdateText()
    {
        SectionText.text = LevelSectionScript.CurrentSectionName;
    }
}
