﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpUI : MonoBehaviour
{

    /// <summary>
    /// The Main Contaner of The UI
    /// </summary>
    public GameObject Container;
    /// <summary>
    /// The Bacground Image Ref 
    /// </summary>
    public Image backgroundImage;
    /// <summary>
    /// The Borader image 
    /// </summary>
    public Image BoradImage;

    public Text NameText;
    public Text OtherNameText;

    protected CanvasGroup canvasGroup;

    public void SetNameAndRarity(string name,Rarity rarity)
    {
        NameText.color = GlobalData.GetDisplayColorFromRarity(rarity);
        BoradImage.color = GlobalData.GetDisplayColorFromRarity(rarity);
        NameText.text =  StaticTools.FirstLetterToUpper(rarity.ToString()) +" "+ name;
    }
    protected virtual void SetUPDataForShow()
    {

    }
    // Use this for initialization
    void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        Container.SetActive(false);
    }
    protected void Show()
    {
        SetUPDataForShow();
        Container.SetActive(true);
         //StartCoroutine(FadeIn());
        
    }
    public void Hide()
    {
        Container?.SetActive(false);
        //StartCoroutine(FadeOut());
    }


    /// <summary>
    /// Fades the screen to black as the player leaves the screen.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> FadeOut()
    {
        float alphaPercentage = 1f;
        while (alphaPercentage > 0)
        {
            alphaPercentage -= Time.deltaTime*2 ;
            canvasGroup.alpha = alphaPercentage;
            yield return 0f;
        }
        Container.SetActive(false);
        yield return 0f;
    }

    /// <summary>
    /// Brings color back to the screen as the player returns.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> FadeIn()
    {
        float alphaPercentage = 0f;
        while (alphaPercentage < 1f)
        {
            alphaPercentage += Time.deltaTime*2;
            canvasGroup.alpha = alphaPercentage;
            yield return 0f;
        }
        
        yield return 0f;
    }
}
