﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace HUD
{
    /// <summary>
    /// Shield HUD
    /// </summary>
    public class ShieldHUD : MonoBehaviour
    {
        /// <summary>
        /// The image of the sheaildbar that will be filled based on the eqiped sheild health or hidden if not equiped
        /// </summary>
        public Image ShieldBars;
        /// <summary>
        /// The outter image of the shield UI
        /// </summary>
        public Image ShieldOurline;
        /// <summary>
        /// The image of the sheaildbar that will be filled based on the eqiped sheild health or hidden if not equiped
        /// </summary>
        public Image ShieldBars2;


        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
        }
    }
}
