﻿using MovementEffects;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HUD
{
    public class HeathBarHUD : MonoBehaviour
    {
        public Sprite[] HeathBarsSprites;
        public Image HeathBarsSprite;
        public Image HeathBarOutLineSprite;
        private float _lastHeathMaxVal = 0;
        private float _lastHeathVal = 0;


        void Update()
        {
            if (_lastHeathMaxVal != GlobalData.Player.Data.MaxHealth)
            {
                _lastHeathMaxVal = GlobalData.Player.Data.MaxHealth;

                if (_lastHeathMaxVal <= 100)
                    HeathBarsSprite.sprite = HeathBarsSprites[0];
                else if (_lastHeathMaxVal >= 110 && _lastHeathMaxVal < 120)
                    HeathBarsSprite.sprite = HeathBarsSprites[1];
                else if (_lastHeathMaxVal >= 120 && _lastHeathMaxVal < 130)
                    HeathBarsSprite.sprite = HeathBarsSprites[2];
                else if (_lastHeathMaxVal >= 130 && _lastHeathMaxVal < 140)
                    HeathBarsSprite.sprite = HeathBarsSprites[3];
                else if (_lastHeathMaxVal >= 140 && _lastHeathMaxVal < 150)
                    HeathBarsSprite.sprite = HeathBarsSprites[4];
                else
                    HeathBarsSprite.sprite = HeathBarsSprites[5];
            }

            if (_lastHeathVal != GlobalData.Player.Data.Health)
            {
                StartCoroutine(ApplyDamageFlash());
                _lastHeathVal = GlobalData.Player.Data.Health;
            }
        }

        protected IEnumerator<float> ApplyDamageFlash()
        {
            float diff = HeathBarsSprite.fillAmount - (GlobalData.Player.Data.Health / GlobalData.Player.Data.MaxHealth);

            HeathBarsSprite.color = Color.white;
            HeathBarsSprite.fillAmount -= diff / 5;
            yield return Timing.WaitForSeconds(0.2f);

            HeathBarsSprite.color = Color.red;
            HeathBarsSprite.fillAmount -= diff / 5;
            yield return Timing.WaitForSeconds(0.2f);

            HeathBarsSprite.color = Color.white;
            HeathBarsSprite.fillAmount -= diff / 5;
            yield return Timing.WaitForSeconds(0.2f);

            HeathBarsSprite.color = Color.green;
            HeathBarsSprite.fillAmount -= diff / 5;
            yield return Timing.WaitForSeconds(0.2f);

            HeathBarsSprite.color = Color.white;
            HeathBarsSprite.fillAmount -= diff / 5;
            yield return 0f;
        }
    }
}
