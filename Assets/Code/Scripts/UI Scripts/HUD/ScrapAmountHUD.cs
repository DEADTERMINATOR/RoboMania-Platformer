﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace HUD
{
    public class ScrapAmountHUD : MonoBehaviour
    {
        /// <summary>
        /// The UI text to diplay scrap
        /// </summary>
        public Text ScrapText;
        /// <summary>
        /// The Text info update
        /// </summary>
        public Text ScrapUpdateText;
        /// <summary>
        /// The Scrap Value on update
        /// </summary>
        private int _lastSeenScrapAmount = 0;
        /// <summary>
        /// The text color of the update text When Scrap is added
        /// </summary>
        public Color PositaveValueColor = Color.green;
        /// <summary>
        /// The text color of the update text When Scrap is removed
        /// </summary>
        public Color NegativeValueColor = Color.red;

        /// <summary>
        /// The Color being used
        /// </summary>
        private Color _curentWanrringTextColor;

        private float _curentAlpha = 0;

        // Use this for initialization
        void Start()
        {
            ScrapText.text = GlobalData.Player?.Data?.TotalScrap.ToString();
            _curentAlpha = 0;
            UpdateInfoAlpha();
        }

        // Update is called once per frame
        void Update()
        {
            int scrapAmount = 0;
            scrapAmount = GlobalData.Player.Data.TotalScrap;
            if (scrapAmount != _lastSeenScrapAmount)
            {
                int theAddedAmount = scrapAmount - _lastSeenScrapAmount;
                ScrapUpdateText.text = theAddedAmount.ToString();
                if (theAddedAmount > 0)
                {
                    _curentWanrringTextColor = PositaveValueColor;
                }
                else
                {
                    _curentWanrringTextColor = NegativeValueColor;
                }
                _lastSeenScrapAmount = scrapAmount;
                StartCoroutine(ShowScrapUpdate());
            }
            ScrapText.text = scrapAmount.ToString();
        }

        private void UpdateInfoAlpha()
        {
            ScrapUpdateText.color = new Color(_curentWanrringTextColor.r, _curentWanrringTextColor.g, _curentWanrringTextColor.b, _curentAlpha);
        }

        IEnumerator ShowScrapUpdate()
        {

            for (_curentAlpha = 1; _curentAlpha > 0; _curentAlpha -= 0.005f)
            {
                UpdateInfoAlpha();
                yield return new WaitForEndOfFrame();
            }
            UpdateInfoAlpha();
        }
    }
}
