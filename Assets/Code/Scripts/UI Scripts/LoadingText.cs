﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingText : MonoBehaviour
{
    public Text theText;
    public int MaxDots;
    public string DotChar = ".";
    public float TimeBwetweenDots = 0.1f;

    private int _numberOfDots=0;
    private float _time =0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        _time += Time.deltaTime;
        if(_time >= TimeBwetweenDots)
        {
            _numberOfDots++;
            if (_numberOfDots == MaxDots)
            {
                _numberOfDots = 0;
                theText.text = "";
            }
            else
            {
                theText.text = theText.text + DotChar;
            }
            _time = 0;
        }
    }
}
