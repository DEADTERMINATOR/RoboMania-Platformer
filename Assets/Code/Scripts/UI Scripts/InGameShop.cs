﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InGameShop : MonoBehaviour
{
    protected List<OldShopItem> items = new List<OldShopItem>();
    public Text ScrapInfo;
    public Text UpdateText;
    public GameObject content;
    public Text ShopName;
    public ShopContents contnets;
    public int index = 0;
    public int MaxOnScreen = 5;
    public int movementSize = 43;
    public int MaxOffset { get { return (items.Count - MaxOnScreen) * movementSize; } }
    public int LastOnScreen = 1;


    private int offset = 0;
    private Vector3 startPos;

    protected UIScroller uIScroller;

    public bool isVisible = false;

    // Use this for initialization
    protected virtual void Start()
    {
        LastOnScreen = MaxOnScreen;
        startPos = content.transform.localPosition;
        uIScroller = gameObject.AddComponent<UIScroller>();
        uIScroller.UseLocal = true;
        uIScroller.time = 0.5f;
    }
    public void SetAllInBackGroundActive(bool enable)
    {
        foreach (Transform child in transform)
        {
            if (child.gameObject != gameObject)
            {
                child.gameObject.SetActive(enable);
            }
        }
    }
    public void LoadStore()
    {
        ShopName.text = contnets.ShopName;
        foreach (Transform child in content.transform)
        {
            //BUGFIX: Destroy 
            child.gameObject.SetActive(false);
            Destroy(child.gameObject);

        }
        items.Clear();
        foreach (OldShopItem it in contnets.items)
        {
            OldShopItem newIt = Instantiate(it);
            newIt.gameObject.transform.SetParent(content.transform, false);
            items.Add(newIt);
        }
        foreach (OldShopItem it in items)
        {
            it.SetOwningShop(this);
        }
        Show();
        index = 0;
        offset = 0;
        
        content.transform.localPosition = Vector3.zero;
        startPos = content.transform.localPosition;
        LastOnScreen = MaxOnScreen;
        items[0].OnSelect();
    }

    public void Show()
    {
        gameObject.SetActive(true);
        isVisible = true;
        GlobalData.Player.Movement.Stop();
        Time.timeScale = 0;
    }

    public void SetSelected(OldShopItem item)
    {
        items[index].OnDeSelect();
        index = items.IndexOf(item);
        items[index].OnSelect();

    }

    public void ScrollDown()
    {
        if (index  < items.Count && index > 0)
        {
            if (index == LastOnScreen - 1)
            {
                if (offset + movementSize <= MaxOffset)
                {
                    LastOnScreen++;
                    offset += movementSize;
                }
            }
        }
        else if(index <= 0)
        {
            LastOnScreen = MaxOnScreen;
            offset = MaxOffset;
        }
        else 
        { 
            index = 0;
            offset = 0;
            LastOnScreen = MaxOnScreen;  
        }

        uIScroller.Run(content.transform.localPosition, startPos + new Vector3(0, offset, 0), content);
    }

    public void ScrollUp()
    {
        if (index >= 0)
        {
            if ((index == LastOnScreen - 4))
            {
                if (offset - movementSize >= 0)
                {
                    LastOnScreen--;
                    offset -= movementSize;                  
                }
                else
                {
                    LastOnScreen = MaxOnScreen;
                    offset = 0;
                }
            }

            if(index == items.Count -1)
            {
                LastOnScreen = MaxOnScreen;
                offset = 0;
            }
        }
        else
        {
            LastOnScreen = items.Count - 1;
            index = items.Count - 1;
            offset = MaxOffset;

        }
        uIScroller.Run(content.transform.localPosition, startPos + new Vector3(0, offset, 0), content);
    }
    // Update is called once per frame
    protected virtual void Update()
    {
        ScrapInfo.text = GlobalData.Player.Data.TotalScrap.ToString();

        if(!isVisible)
            gameObject.SetActive(false);

        if(Input.GetAxis("MouseWeel") < 0)
        {
            items[index].OnDeSelect();
            index++;

            if (index == items.Count)
                index = 0;

            ScrollDown();
            items[index].OnSelect();
        }
        if (Input.GetAxis("MouseWeel") > 0)
        {
            items[index].OnDeSelect();

            if (index == 0)
                index = items.Count;

            index--;
            index = index % items.Count;
            ScrollUp();
            items[index].OnSelect();
        }
        if (Input.GetButtonDown("CloseUi"))
        {
            Time.timeScale = 1;
            GlobalData.Player.Movement.Restart();
            gameObject.SetActive(false);
            isVisible = false;
        }
        if (Input.GetKeyDown("down"))
        {
            items[index].OnDeSelect();
            index++;
            ScrollDown();

            items[index].OnSelect();
        }
        if (Input.GetKeyDown("up"))
        {
            items[index].OnDeSelect();
            index--;
            ScrollUp();
            items[index].OnSelect();

        }
        if (Input.GetKeyDown("return"))
            items[index].BuyClick();
    }
}
