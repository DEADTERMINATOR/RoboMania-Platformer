﻿using IngameDebugConsole;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Characters.Player;
using static GlobalData;

public class DebugHUD : MonoBehaviour ,IDamageGiver
{
    public Button HurtButton;
    public Button AddScrapButton;
    public Button AddHealthBUtton;
    public Button AddEnergyButton;
    public Button HurtProjectileButton;
    public Text PlayerHealthInfo;
    public GameObject Container;
    public GameObject ConsoleContainer;
    public Button ToggleButton;
    public Text MainText;
    public bool ISShowing = true;
    /// <summary>
    /// A Ui debug text
    /// </summary>
    public Text DebugText;
    /// <summary>
    /// Should the UI debug text be hidden 
    /// </summary>
    public bool ShowDebug;


    /// <summary>
    /// A display that calculates and returns the current FPS and millisecond per frame count.
    /// </summary>
    private FPSDisplayOBI _fpsDisplay;

    /// <summary>
    /// Reference to the player stored due to frequent access.
    /// </summary>
    private static PlayerMaster _playerRef;


    void Start ()
    {
        if (Debug.isDebugBuild)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(true);
        }

        if (ToggleButton)
        {
            ToggleButton?.onClick.AddListener(Toggle);
        }

        Toggle();
        _fpsDisplay = gameObject.AddComponent<FPSDisplayOBI>();
        DebugLogConsole.AddCommandStatic("AddJumps", "Adds Jumps to The Player", "AddEnergy", typeof(DebugHUD));
        DebugLogConsole.AddCommandStatic("AddAmmo", "Adds Ammo Of The Players Curent type", "AddAmmo", typeof(DebugHUD));
        DebugLogConsole.AddCommandStatic("AddScrap", "Adds Ammo Of The Players Curent type", "AddScrapButtonClick", typeof(DebugHUD));
        _playerRef = GlobalData.Player;
    }
	
	void Update ()
    {
        if (_playerRef != null)
        {
            PlayerHealthInfo.text = HealthOutput();
            MainText.text = _fpsDisplay.ToString();
        }
    }


    void Toggle()
    {
        ISShowing = !ISShowing;
        Container.SetActive(ISShowing);
       /// ConsoleContainer.SetActive(ISShowing);

        if (ISShowing)
            ToggleButton.GetComponentInChildren<Text>().text = ">";
        else
            ToggleButton.GetComponentInChildren<Text>().text = "<";
    }

    void HurtProjectileButtonClick()
    {
        Projectile pro = new Projectile();
        _playerRef.TakeDamage(pro, 5, DamageType.PROJECTILE,Vector3.zero);  
    }

    void AddScrap(int amount)
    {
        _playerRef.Data.AddScrapAmount(amount);
    }

    static public void AddEnergy()
    {
        _playerRef.Data.AddEnergyCell();
    }

    
    static public void AddAmmo(int amount = 10)
    {
        //_playerRef.Data.AmmoInventory.AddAmmo(GlobalData.Player.Weapon.TheGun.ActiveChip.ammoType, amount);
    }
    

    void AddEnergyButtonClick()
    {
        _playerRef.Data.AddEnergyCell();
    }

    void AddScrapButtonClick()
    {
        _playerRef.Data.AddScrapAmount(5);
    }

    void HurtButtonClick()
    {
        _playerRef.TakeDamage(this, 5, DamageType.MELEE,Vector3.zero);
    }

    void AddHealthBUttonClick()
    {
        _playerRef.Data.IncreaseMaxHealth(10);
        _playerRef.Data.Heal(_playerRef.Data.MaxHealth - _playerRef.Data.Health);
    }

    string HealthOutput()
    {
        return string.Format("Pos{0} | MemAloc{1:0.000}MB"  , _playerRef.gameObject.transform.position.ToString(), (System.GC.GetTotalMemory(false) / 1048576f).ToString()); 
    }
}
