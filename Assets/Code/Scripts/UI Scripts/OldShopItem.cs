﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using Characters.Player;

public class OldShopItem : MonoBehaviour, IPointerEnterHandler , IPointerExitHandler
{
    public Image backgroundImage;
    public Image dispalyImage;
    public Text displayText;
    public Button buyButton; 
    public Sprite displaySprite;
    public int Price;
    public int Amount;
    

    protected PlayerMaster _player;
    protected InGameShop _currentShop;
    public void SetOwningShop(InGameShop shop)
    {
        _currentShop = shop;
    }    

    // Use this for initialization
   protected virtual void Start()
    {
        _player = StaticTools.GetPlayerRef();
        dispalyImage.sprite = displaySprite;
        buyButton.onClick.AddListener(BuyClick);
    }


    public virtual void BuyClick()
    {
    }
    // Update is called once per frame
    void Update()
    {

    }
    public virtual void OnSelect()
    {

    }

    public virtual void OnDeSelect()
    {

    }
    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        OnSelect();
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        
    }
}
