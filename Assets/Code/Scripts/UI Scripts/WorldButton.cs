﻿using UnityEngine;
public class WorldButton : MonoBehaviour, IActivatable
{
    public Sprite[] sprites = new Sprite[3];
    public enum ButtonState { ON, OFF, DEACTIVATED }
    private SpriteRenderer rendered;
    private ButtonState _buttonState = ButtonState.OFF;

    public ButtonState buttonState { get { return _buttonState; }set { _buttonState = value; rendered.sprite = sprites[(int)_buttonState]; } }
    public bool Active { get { return buttonState == ButtonState.ON; } }

    public void Start()
    {
        rendered = GetComponent<SpriteRenderer>();
    }

    public void Activate(GameObject activator)
    {
        buttonState = ButtonState.ON;
    }

    public void Deactivate(GameObject activator)
    {

    }
}


