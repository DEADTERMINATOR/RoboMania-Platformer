﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HUD;

public class TipBox : MonoBehaviour {
    /// <summary>
    /// The text to show
    /// </summary>
    public string TipText;
    /// <summary>
    /// the offset of the infox bbox fron the origian of the triger
    /// </summary>
    public Vector3 offset;
    /// <summary>
    /// The colour of the text
    /// </summary>
    public Color32 TextColor;

    public bool CanActivate = false;

    public GameObject theOBJToActivate;

    private bool InTrigger = false;

    public void ShowTip()
    {
        Vector3 infoPos = transform.position;
        infoPos += offset;
        ScreenOverlays.Instance.infoBox.DisplayText.color = TextColor;
        ScreenOverlays.Instance.infoBox.DisplayText.text = TipText;
        ScreenOverlays.Instance.infoBox.showBox(-1, infoPos);
    }

    private void Update()
    {
        if(InTrigger)
        {
            if (Input.GetButtonDown("Action"))
            {
                (theOBJToActivate.GetComponent<IActivatable>()).Activate(gameObject);
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            InTrigger = true;
            ShowTip();
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            InTrigger = false;
            ScreenOverlays.Instance.infoBox.Hide();

        }
    }

}
