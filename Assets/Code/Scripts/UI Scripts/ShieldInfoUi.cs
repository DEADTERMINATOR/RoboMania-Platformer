﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using static GlobalData;

public class ShieldInfoUi : MonoBehaviour
{
    /// <summary>
    /// Meele bar
    /// </summary>
    public Image MeeleBar;
    public Image ProjectileBar;
    /*public Image ReflectIcon;
    public Image AbsorbIcon;*/
    public Text MeeleCompare;
    public Text ProjectileCompare;
     public ProjectileTypeDisplay ProjectileType  ;
    public ProjectileTypeDisplay ProjectileTypeCompare;

    /*public void ShowInfo(AShield sheild, AShield sheild2 = null)
    {
        ProjectileTypeCompare.Hide();
        MeeleCompare.text = "";
        ProjectileCompare.text = "";
        MeeleBar.fillAmount = sheild.GetDamagePercentage(DamageType.MELEE);
        ProjectileBar.fillAmount = sheild.GetDamagePercentage(DamageType.PROJECTILE);
        ProjectileType.SetIcon(sheild);
        if (sheild2)
        {
            CalcAndShowCompareAmount(MeeleCompare, DamageType.MELEE,sheild,sheild2);
            CalcAndShowCompareAmount(ProjectileCompare, DamageType.PROJECTILE, sheild, sheild2);
            ProjectileTypeCompare.SetIcon(sheild2);

            if(sheild.ReflectionType == sheild2.ReflectionType)
                ProjectileTypeCompare.SetColour(Color.yellow);
            else
                ProjectileTypeCompare.SetColour(Color.green);
        }

    }

    void CalcAndShowCompareAmount(Text elm, DamageType type, AShield sheild, AShield sheild2)
    {
        int diff = (int)((sheild.GetDamagePercentage(type) - sheild2.GetDamagePercentage(type)) * 100);
        if (diff > 0)
        {
            elm.color = Color.green;
            elm.text = diff.ToString();
        }
        else if (diff < 0)
        {
            elm.color = Color.red;
            diff *= -1; //invert nagative;
            elm.text = diff.ToString();
        }
        else
        {
            elm.color = Color.yellow;
            elm.text = "~";
        }
    }*/
}
