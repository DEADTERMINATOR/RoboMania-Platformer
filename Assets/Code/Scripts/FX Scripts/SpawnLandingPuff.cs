﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Player
{
    public class SpawnLandingPuff : MonoBehaviour
    {
        /// <summary>
        /// The vertical velocity required to spawn a landing effect when the player lands.
        /// </summary>
        public const float VELOCITY_SPAWN_THRESHOLD = -0.09f;


        /// <summary>
        /// The prefab representing the landing puff effect that will be spawned.
        /// </summary>
        private GameObject _landingPuffEffect;


        // Use this for initialization
        void Start()
        {
            _landingPuffEffect = Resources.Load("Prefabs/Effects/Landing Puff") as GameObject;
            GlobalData.Player.CollisionBelow += new PlayerMaster.MovementCollisionEvent(OnCollisionBelow);
        }

        /// <summary>
        /// Spawns a copy of the landing puff effect at the position the player has landed.
        /// </summary>
        private void OnCollisionBelow(Vector2 velocity)
        {
            PlayerController2D.PlayerCollisionInfo castedCollisionInfo = (PlayerController2D.PlayerCollisionInfo)GlobalData.Player.Controller.Collisions;
            if (velocity.y <= VELOCITY_SPAWN_THRESHOLD && castedCollisionInfo.VerticalCollidingObject.layer == GlobalData.OBSTACLE_LAYER_SHIFTED)
                Instantiate(_landingPuffEffect, transform.position, transform.rotation);
        }
    }
}
