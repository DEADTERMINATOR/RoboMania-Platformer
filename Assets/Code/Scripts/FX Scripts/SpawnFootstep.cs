﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Player
{
    public class SpawnFootstep : MonoBehaviour
    {
        /// <summary>
        /// How long the delay between each footstep effect should be.
        /// </summary>
        public float DelayBetweenFootsteps = 1.0f;


        /// <summary>
        /// The prefab representing the landing puff effect that will be spawned.
        /// </summary>
        private GameObject _footstepEffect;

        /// <summary>
        /// Reference to the character's controller to know when the character has just landed
        /// so we can fire the effect.
        /// </summary>
        private PlayerMaster _playerRef;

        /// <summary>
        /// How much time has elapsed since the last footstep effect.
        /// </summary>
        private float _timeSinceLastFootstep = 0f;


        // Use this for initialization
        private void Start()
        {
            _footstepEffect = Resources.Load("Prefabs/Effects/Player Actions/Footstep") as GameObject;
            _playerRef = GlobalData.Player;
        }

        private void Update()
        {
            if (TheGame.GetRef.gameState != TheGame.GameState.RESPAWNING)
            {
                _timeSinceLastFootstep += Time.deltaTime;
                if (_timeSinceLastFootstep >= DelayBetweenFootsteps && _playerRef.MovementInput.x != 0 && _playerRef.Controller.Collisions.Left.CollidingLayer != GlobalData.OBSTACLE_LAYER && _playerRef.Controller.Collisions.Right.CollidingLayer != GlobalData.OBSTACLE_LAYER && _playerRef.Controller.Collisions.IsCollidingBelow)
                {
                    SpawnEffect();
                }
            }
        }

        /// <summary>
        /// Spawns a copy of the landing puff effect at the position the player has landed.
        /// </summary>
        private void SpawnEffect()
        {
            _playerRef.Sound.PlayFootstepSound();
            Instantiate(_footstepEffect, transform.position, transform.rotation);
            _timeSinceLastFootstep = 0f;
        }
    }
}
