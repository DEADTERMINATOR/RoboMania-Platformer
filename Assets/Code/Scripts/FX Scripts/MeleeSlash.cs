﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeSlash : MonoBehaviour, IDamageGiver
{
    /// <summary>
    /// The speed the slash effect should travel.
    /// </summary>
    public float Speed;

    /// <summary>
    /// How long the slash effect should be alive before it dies (presuming it doesn't hit something first).
    /// </summary>
    public float AliveTime;

    /// <summary>
    /// A layer mask containing the layers that the slash should be killed by.
    /// </summary>
    public LayerMask CollideSurfaces;


    /// <summary>
    /// The amount of damage the slash should do if it hits something that takes damage. Set by the melee attack as it will deal a portion of it.
    /// </summary>
    [HideInInspector]
    public float Damage;

    /// <summary>
    /// The effect is spawned at the beginning of the melee attack, but how long should the effect wait before triggering (since it should sync with the animation).
    /// </summary>
    [HideInInspector]
    public float WaitTimeBeforeActivating;


    /// <summary>
    /// How long the slash has been alive for.
    /// </summary>
    private float _currentAliveTime;

    /// <summary>
    /// The sprite renderer for the slash effect.
    /// </summary>
    private SpriteRenderer _renderer;


    //Use this for initialization
    private void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _renderer.enabled = false;
        WaitTimeBeforeActivating = 0.25f;
	}
	
	//Update is called once per frame
	private void Update()
    {
        if (!_renderer.enabled && _currentAliveTime >= WaitTimeBeforeActivating)
        {
            _renderer.enabled = true;
            _currentAliveTime = 0;
        }

        _currentAliveTime += Time.deltaTime;
        if (_currentAliveTime >= AliveTime + WaitTimeBeforeActivating)
        {
            //BUGFIX: Destroy 
            gameObject.transform.root.gameObject.SetActive(false);
            Destroy(gameObject.transform.root.gameObject,1);
        }

        if (_renderer.enabled)
        {
            _currentAliveTime += Time.deltaTime;
            transform.Translate(Vector2.right * Speed);
            _renderer.color = new Color(1.0f, 1.0f, 1.0f, 1.0f - (_currentAliveTime / AliveTime));
        }
	}


    private void OnTriggerEnter2D(Collider2D collider)
    {
        HandleCollision(collider);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        HandleCollision(collision.collider);
    }

    /// <summary>
    /// Checks if the collision occured with a layer that should terminate the melee slash.
    /// </summary>
    /// <param name="collider">The collider that triggered the collision response.</param>
    private void HandleCollision(Collider2D collider)
    {
        if (((1 << collider.gameObject.layer) & CollideSurfaces) != 0)
        {
            //TODO: non zero HitPoint 
            if (collider.gameObject.layer == LayerMask.NameToLayer("Enemy") || collider.gameObject.layer == LayerMask.NameToLayer("RoboticEnemy") || collider.gameObject.layer == LayerMask.NameToLayer("Destryable"))
                collider.gameObject.GetComponent<EnemyCollider>().TakeDamage(this, Damage, GlobalData.DamageType.MELEE,Vector3.zero);
            //BUGFIX: Destroy 
            gameObject.transform.root.gameObject.SetActive(false);
            Destroy(gameObject.transform.root.gameObject,1);
        }
    }
}
