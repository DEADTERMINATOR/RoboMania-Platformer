﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Characters.Enemies;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/EnamyDestructionEffectCollection", order = 1)]
public class EnemyDestructionEffectCollection : ScriptableObject
{
    public Enemy TheEnemy;
    public List<Explodable> ExplodableOptions;
    public Sprite RefeanceSprite;
    public Vector3 SpawnScale = Vector3.one;
    public Vector3 SpawnOffset = Vector3.zero;
    public Explodable GetRandomDestruction()
    {
        return ExplodableOptions[Random.Range(0, ExplodableOptions.Count)];
    }

}
