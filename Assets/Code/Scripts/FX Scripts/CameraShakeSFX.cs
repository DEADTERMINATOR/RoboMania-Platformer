﻿using UnityEngine;
using System.Collections;

public class CameraShakeSFX : PlayableObject
{
    private IntangerInWrappingRange _ShakeType = new IntangerInWrappingRange(3);

    protected override void Run()
    {
        Vector3 basePosition = transform.position;
        Vector3 shakeFactor;

        switch (_ShakeType)
        {
            default:
                shakeFactor = new Vector3(Random.Range(0.01f, 0.05f), Random.Range(0.01f, 0.05f), 0);
                break;
            case 1:
                shakeFactor = new Vector3(Random.Range(-0.01f, -0.05f), Random.Range(-0.01f, -0.05f), 0);
                break;
            case 2:
                shakeFactor = new Vector3(Random.Range(0.01f, 0.05f), Random.Range(-0.01f, -0.05f), 0);
                break;
            case 3:
                shakeFactor = new Vector3(Random.Range(-0.01f, 0.05f), Random.Range(-0.01f, 0.05f), 0);
                break;
        }
        _ShakeType++;///Not Magic a IntangerInWarppingRange;
        transform.position = basePosition + shakeFactor;
    }

}
