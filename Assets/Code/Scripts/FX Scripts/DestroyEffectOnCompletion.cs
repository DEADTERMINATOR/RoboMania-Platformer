﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEffectOnCompletion : MonoBehaviour
{
    /// <summary>
    /// An enum holding the options for the possible types the effect can be.
    /// </summary>
    public enum EffectType { SPRITE, PARTICLE_SYSTEM }


    /// <summary>
    /// The type of effect this is.
    /// </summary>
    public EffectType Type;

    /// <summary>
    /// Whether the destruction should go to the root object, or only destroy the object this script is attached to.
    /// </summary>
    public bool DestroyRoot;


    /// <summary>
    /// Reference to the animator for the effect, so we can kill the effect when required.
    /// Only use if the effect type is a sprite.
    /// </summary>
    private Animator _effectAnim;

    /// <summary>
    /// Reference to the particle system, so we can kill the effect when required.
    /// Only used if the effect type is a particle system.
    /// </summary>
    private ParticleSystem _particleSystem;


    private void Start()
    {
        if (Type == EffectType.SPRITE)
        {
            _effectAnim = GetComponent<Animator>();
        }
        else if (Type == EffectType.PARTICLE_SYSTEM)
        {
            _particleSystem = GetComponent<ParticleSystem>();
        }
    }

    private void Update()
    {
        if (Type == EffectType.SPRITE)
        {
            if (_effectAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
            {
                DestroyNow();
            }
        }
        else if (Type == EffectType.PARTICLE_SYSTEM)
        {
            if (!_particleSystem.isPlaying)
            {
                DestroyNow();
            }
        }
    }

    public void DestroyNow()
    {
        if (DestroyRoot)
        {
            Destroy(gameObject.transform.root);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
