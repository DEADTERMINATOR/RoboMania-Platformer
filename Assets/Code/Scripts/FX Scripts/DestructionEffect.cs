﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MovementEffects;
using Characters.Enemies;

public class DestructionEffect : MonoBehaviour
{
    /// <summary>
    /// The base scale for the explosion effect.
    /// </summary>
    private const float BASE_EXPLOSION_SCALE = 5f;


    /// <summary>
    /// The prefab that is supposed to replace the original sprite during the destruction.
    /// </summary>
    //public GameObject ReplacementPrefab;

    /// <summary>
    /// The prefab that represents the explosion effect that will be played during the destruction.
    /// </summary>
    public GameObject ExplosionEffect;


    /// <summary>
    /// Who the destruction effect is being called on.
    /// </summary>
    private GameObject _caller;


    /// <summary>
    /// Plays an explosion effect, destroys the original object, and replaces it with a destroyed variant.
    /// </summary>
    /// <param name="replacementXScale">The X scale for the destroyed replacement (mostly used to match orientation with the original object).</param>
    /// <param name="explosionScale">The scale of the explosion effect.</param>
    public void DestroyAndReplace(float replacementXScale = 1, int enemyScrap = 0, float explosionScale = BASE_EXPLOSION_SCALE)
    {
        PlayDestruction(true, replacementXScale, explosionScale, enemyScrap);
    }

    /// <summary>
    /// Plays an explosion effect, hides the original object, and replaces it with a destroyed variant.
    /// </summary>
    /// <param name="replacementXScale">The X scale for the destroyed replacement (mostly used to match orientation with the original object).</param>
    /// <param name="explosionScale">The scale of the explosion effect.</param>
    public void HideAndReplace(float replacementXScale = 1, float explosionScale = BASE_EXPLOSION_SCALE)
    {
        PlayDestruction(false, replacementXScale, explosionScale, 0);
    }

    /// <summary>
    /// Plays an explosion effect at the specified position.
    /// </summary>
    /// <param name="position">The position to play the explosion effect.</param>
    /// <param name="explosionScale">The scale of the explosion effect.</param>
    public void JustPlayExplosion(Vector3 position, float explosionScale = BASE_EXPLOSION_SCALE)
    {
        Timing.RunCoroutine(PlayExplosionAtPosition(explosionScale, position), "Play Explosion");
    }


    /// <summary>
    /// Plays an explosion effect and replaces the intact version of the object with a destroyed variant, either destroying the original or simply hiding it in the process.
    /// </summary>
    /// <param name="destroyObject">Whether the object should be destroyed, or just hidden.</param>
    /// <param name="replacementXScale">The X scale for the destroyed replacement (mostly used to match orientation with the original object)</param>
    /// <param name="explosionScale">The scale of the explosion effect.</param>
    /// <returns></returns>
    private void PlayDestruction(bool destroyObject, float replacementXScale, float explosionScale, int enemyScrap)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage[0].Path));

        GameObject instantiatedExplosionEffect = Instantiate(ExplosionEffect, new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z), Quaternion.identity);
        instantiatedExplosionEffect.transform.localScale = new Vector3(explosionScale, explosionScale, explosionScale);

        Enemy enemyComponent = gameObject.GetComponent<Enemy>();
        if (enemyComponent?.Exploder != null)
        {
            enemyComponent.Exploder.gameObject.SetActive(true);
            foreach (Transform child in transform)
            {
                child.localScale = new Vector3(-replacementXScale, child.localScale.y, child.localScale.z);
            }
            enemyComponent.Exploder.explode(enemyScrap);
        }

        if (destroyObject)
        {
            Destroy(transform.gameObject); //If there ever comes a case where we don't want to destroy the parent, just create another variable where the object to be destroyed is specified.
        }
        else
        {
            transform.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Plays an explosion effect at the provided position. Does not destroy or hide the original object.
    /// </summary>
    /// <param name="explosionScale">The scale of the explosion effect.</param>
    /// <param name="position">The position the explosion effect should occur at.</param>
    /// <returns></returns>
    private IEnumerator<float> PlayExplosionAtPosition(float explosionScale, Vector3 position)
    {
        GameObject instantiatedExplosionEffect = Instantiate(ExplosionEffect, position, Quaternion.identity);
        instantiatedExplosionEffect.transform.localScale = new Vector3(explosionScale, explosionScale, explosionScale);

        Animator explosionAnim = instantiatedExplosionEffect.GetComponent<Animator>();
        while (explosionAnim.GetCurrentAnimatorStateInfo(0).shortNameHash != Animator.StringToHash("On") ||
              (explosionAnim.GetCurrentAnimatorStateInfo(0).shortNameHash == Animator.StringToHash("On") && explosionAnim.GetCurrentAnimatorStateInfo(0).normalizedTime <= 0.5f))
        {
            yield return 0f;
        }
    }
}
