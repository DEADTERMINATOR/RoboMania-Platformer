﻿using UnityEngine;
using System.Collections;

public abstract class PlayableObject : MonoBehaviour
{
    /// <summary>
    /// IS it playing
    /// </summary>
    public bool IsPlaying { get { return _play;} }
    /// <summary>
    /// What is the Run time 
    /// </summary>
    public float RunTime { get { return _totalRunTime; } }
    /// <summary>
    /// Should Late Update be uesed if not Update is uesed 
    /// </summary>
    public bool UseLateUpdate = true;

    private bool _play;
    private float _totalRunTime;
    private float _runTime;

    public void Play(float timeToPlay)
    {
        _play = true;
        _totalRunTime = timeToPlay;
        _runTime = 0;
    }

    public void Stop()
    {
        _play = false;
    }

    protected virtual void Run()
    {

    }

    private void DoUpdate()
    {
        if (_play)
        {
            Run();
            _runTime += Time.deltaTime;
            if (_runTime >= _totalRunTime)
            {
                _play = false;
            }
        }
    }

    private void Update()
    {
        if(!UseLateUpdate)
        {
            DoUpdate();
        }
    }
    private void LateUpdate()
    {
        if (UseLateUpdate)
        {
            DoUpdate();
        }

    }
}
