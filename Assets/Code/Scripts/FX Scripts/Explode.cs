﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explode : MonoBehaviour
{
    public enum ExplodeDirection { Up, Down, Left, Right, All }

    /// <summary>
    /// How forceful the explosion should be.
    /// </summary>
    public float ExplosionForce;

    /// <summary>
    /// The general direction the piece should travel in when it scatters.
    /// </summary>
    public ExplodeDirection ExplosionDirection;


    /// <summary>
    /// Reference to the rigid body so the explosion force can be applied.
    /// </summary>
    private Rigidbody2D _rigidBody;

    /// <summary>
    /// The direction the explosion force will be applied in.
    /// </summary>
    private Vector2 _scatterDirectionVector;


    public void ApplyExplosionProperties()
    {
        if (_rigidBody == null)
        {
            _rigidBody = GetComponent<Rigidbody2D>();
        }

        if (ExplosionDirection == ExplodeDirection.Up)
        {
            _scatterDirectionVector = StaticTools.Vector2FromAngle(Random.Range(0, 180));
        }
        else if (ExplosionDirection == ExplodeDirection.Down)
        {
            _scatterDirectionVector = StaticTools.Vector2FromAngle(Random.Range(180, 360));
        }
        else if (ExplosionDirection == ExplodeDirection.Left)
        {
            _scatterDirectionVector = StaticTools.Vector2FromAngle(Random.Range(90, 270));
        }
        else if (ExplosionDirection == ExplodeDirection.Right)
        {
            float randomAngle = Random.Range(270, 450);
            if (randomAngle < 360)
            {
                randomAngle -= 360;
            }

            _scatterDirectionVector = StaticTools.Vector2FromAngle(randomAngle);
        }
        else
        {
            _scatterDirectionVector = StaticTools.Vector2FromAngle(Random.Range(0, 360));
        }

        _rigidBody.AddForce(_scatterDirectionVector * ExplosionForce);

        GetComponent<ScrapPickUp>()?.AllowPositionShifting();
    }
}
