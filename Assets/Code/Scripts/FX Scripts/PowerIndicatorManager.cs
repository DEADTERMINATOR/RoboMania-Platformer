﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerIndicatorManager : MonoBehaviour, IActivatableByMultiple
{
    public int ActiveCount { get; private set; }
    public bool Active { get { return _hasPower; } }


    [SerializeField]
    private bool _hasPower;
    [SerializeField]
    private GameObject _powerIndicatorOn;
    [SerializeField]
    private GameObject _powerIndicatorOff;
    [SerializeField]
    private GameObject _powerIndicatorNoPower;


    public void Activate(GameObject activator)
    {
        if (_hasPower)
        {
            if (ActiveCount == 0)
            {
                _powerIndicatorOn.SetActive(true);
                _powerIndicatorOff.SetActive(false);
                if (_powerIndicatorNoPower != null)
                {
                    _powerIndicatorNoPower.SetActive(false);
                }
            }
            ++ActiveCount;
        }
    }

    public void Deactivate(GameObject activator)
    {
        if (_hasPower)
        {
            --ActiveCount;
            if (ActiveCount <= 0)
            {
                ActiveCount = 0;

                _powerIndicatorOn.SetActive(false);
                _powerIndicatorOff.SetActive(true);
            }
        }
    }

    public void SupplyPower()
    {
        _hasPower = true;

        if (_powerIndicatorNoPower != null)
        {
            _powerIndicatorNoPower.SetActive(false);
        }

        if (ActiveCount > 0)
        {
            _powerIndicatorOn.SetActive(true);
            _powerIndicatorOff.SetActive(false);
        }
        else
        {
            _powerIndicatorOn.SetActive(false);
            _powerIndicatorOff.SetActive(true);
        }
    }

    public void KillPower()
    {
        --ActiveCount;
        if (ActiveCount <= 0)
        {
            ActiveCount = 0;
            _hasPower = false;

            _powerIndicatorOn.SetActive(false);
            if (_powerIndicatorNoPower != null)
            {
                _powerIndicatorOff.SetActive(false);
                _powerIndicatorNoPower.SetActive(true);
            }
            else
            {
                _powerIndicatorOff.SetActive(true);
            }
        }
    }
}
