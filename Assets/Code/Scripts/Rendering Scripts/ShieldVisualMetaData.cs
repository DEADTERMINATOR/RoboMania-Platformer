﻿using UnityEngine;
using System.Collections;

public class ShieldVisualMetaData : MonoBehaviour
{
    public Vector3 PlayerAttachOffset = Vector3.zero;
    public MeshRenderer ConeRenderer;
}
