﻿using UnityEngine;
using System.Collections;

public class SpriteMaskingController : MonoBehaviour
{
    private MaterialPropertyBlock _propertyBlock;
    const string StencilValue = "_StencilValue";
    const string StencileFunction = "_StencileFunction";
    public bool DiaableMasking = false;
    // Use this for initialization
    void Start()
    {
        _propertyBlock = new MaterialPropertyBlock();
        _propertyBlock.SetFloat(StencileFunction, 0);
        GetComponent<Renderer>().SetPropertyBlock(_propertyBlock);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
