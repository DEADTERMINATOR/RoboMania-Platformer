﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;

public class RenderToGlobalTextuer : MonoBehaviour
{
    public RenderTexture renderTexture;
    [Tooltip("Do not change unless using a different shader set up")]
    public string GlobalTextuerName = "_ObsticalPass";
    public Camera RenderCamera;
    public bool TrackMainCamera = true;
    private int _IsWritePassID;
    // Unity calls this method automatically when it enables this component
    private void OnEnable()
    {
        _IsWritePassID = Shader.PropertyToID("_IsWritePass");
        // Add WriteLogMessage as a delegate of the RenderPipelineManager.beginCameraRendering event
        RenderPipelineManager.beginCameraRendering += HandelAboutToRender;
        RenderPipelineManager.endCameraRendering += HandelDidRender;
    }

    // Unity calls this method automatically when it disables this component
    private void OnDisable()
    {
        // Remove WriteLogMessage as a delegate of the  RenderPipelineManager.beginCameraRendering event
        RenderPipelineManager.beginCameraRendering -= HandelAboutToRender;
    }

    // Use this for initialization
    void Start()
    {
        if (RenderCamera == null)
        {
            RenderCamera = GetComponent<Camera>();
        }
        //RenderCamera.SetReplacementShader(replacmentShader, "WriteObsticalData");
        //if no render texture assets then make a new one in memory
        if (renderTexture == null)
        {
            renderTexture = new RenderTexture(Screen.width, Screen.height, 1, RenderTextureFormat.Default);
        }
        else // set the defult values 
        {
            renderTexture.format = RenderTextureFormat.Default;
            renderTexture.width = Screen.width;
            renderTexture.height = Screen.height;// Set to the screen then let dynamic scaling kick in to keep the textuer size the same as the screen
        }
        Shader.SetGlobalTexture(GlobalTextuerName, renderTexture);//set the testuer as a shader global
        RenderCamera.targetTexture = renderTexture;// set the textuer as a render tragert of the camera
        RenderCamera.allowDynamicResolution = true; // make sure  dynamic scaling is set
    }

    private void OnPreRender()
    {

    }

    public void SaveTexture()
    {
        byte[] bytes = toTexture2D(renderTexture).EncodeToPNG();
        System.IO.File.WriteAllBytes(Application.dataPath + "/SavedScreen.png", bytes);
    }
    Texture2D toTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(1920, 1080, TextureFormat.ARGB32, false);
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }
    void DoTrackMainCamera()
    {
        RenderCamera.orthographic = Camera.main.orthographic;
        RenderCamera.orthographicSize = Camera.main.orthographicSize;
    }

    // Update is called once per frame
    void Update()
    {
        DoTrackMainCamera();
        //SaveTexture();
    }

    public Camera ObsticalCamera;


    void HandelAboutToRender(ScriptableRenderContext context, Camera camera)
    {
        if (camera == ObsticalCamera)
        {
            
            Shader.SetGlobalFloat(_IsWritePassID, 1);
        }
    }

    void HandelDidRender(ScriptableRenderContext context, Camera camera)
    {
        if (camera == ObsticalCamera)
        {
            Shader.SetGlobalFloat(_IsWritePassID, 0);
        }
    }
}
