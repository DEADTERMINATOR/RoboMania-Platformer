﻿using UnityEngine;
using System.Collections;

public class LavaVisualController : MonoBehaviour
{
    MaterialPropertyBlock MaterialPropertyBlock;
    public Vector2 TopTiling = new Vector2(2, 2);
    // Use this for initialization
    void Start()
    {
        MaterialPropertyBlock = new MaterialPropertyBlock();
        MaterialPropertyBlock.SetVector("Vector2_979FCBEA", TopTiling);
        GetComponent<Renderer>().SetPropertyBlock(MaterialPropertyBlock);


    }

    // Update is called once per frame
    void Update()
    {

    }
}
