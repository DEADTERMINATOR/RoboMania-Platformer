﻿using Obi;
using System;
using UnityEngine;
using static Easing;

public class RopeMovingPlatform : MonoBehaviour, IActivatable
{
    /// <summary>
    /// A way to activate the platform from the editor for debug purposes.
    /// </summary>
    public bool DebugActive = false;

    public float WaitTime = 0;
    /// <summary>
    /// to be used with new IActivatable
    /// </summary>
    public bool Active { get; set; } = false;

    /// <summary>
    /// The also used to run the movement
    /// </summary>
    public EaseType EasingAlgo; 
    /// <summary>
    /// possible state 
    /// </summary>
    public enum State {STOPED,LOWERING,WAIT, RAISING };

    /// <summary>
    /// The data to move the rope
    /// </summary>
    [Serializable]
    public class RopeData
    {
        public ObiRopeCursor cursor;
        public ObiRope rope;
        [HideInInspector]
        public float StartingRopeLength = 0;
        [HideInInspector]
        public float EndRopeLength = 0;
    }
    /// <summary>
    /// The left rope
    /// </summary>
    public RopeData RopeL;
    /// <summary>
    /// The Right rope
    /// </summary>
    public RopeData RopeR;
    /// <summary>
    /// How far should this move
    /// </summary>
    public float Amount;
    /// <summary>
    /// How long  should it take
    /// </summary>
    public float RunTime = 4;
    /// <summary>
    /// the easing function set
    /// </summary>
    private EasingFunction _easingFunction;
    /// <summary>
    /// The runtime
    /// </summary>
    private float _ranTime = 0;
    /// <summary>
    /// the wait time
    /// </summary>
    private float _waitedTime = 0;
    /// <summary>
    /// the mode to run in set on awake 
    /// </summary>
    private bool _nowait = false;
    private State _curentState = State.STOPED;
    private State _LastState = State.STOPED;


    private void Awake()
    {
        Active = DebugActive;
        if (Active)
        {
            _curentState = State.LOWERING;
        }
        _easingFunction = Easing.GetFuctionFromEnum(EasingAlgo);
        RopeL.StartingRopeLength = RopeL.rope.CalculateLength();
        RopeL.EndRopeLength = RopeL.rope.CalculateLength() + Amount;
        RopeR.StartingRopeLength = RopeR.rope.CalculateLength();
        RopeR.EndRopeLength = RopeR.rope.CalculateLength() + Amount;
        _nowait = (WaitTime <= 0);

    }

    private void Update()
    {
        if (_curentState == State.LOWERING || _curentState == State.RAISING)
        {
            Run(_curentState == State.LOWERING);
        }
        else if(_curentState == State.WAIT)
        {
            _waitedTime += Time.deltaTime;
        }
        UpdateSate();
    }

    protected void UpdateSate()
    {
        if (!Active)
        {
            _curentState = State.STOPED;
        }
        if (!_nowait)
        {
            if (_ranTime >= RunTime)
            {
                SetNewState(State.WAIT);
            }
            if (_waitedTime >= WaitTime)
            {
            
                if (_LastState == State.RAISING)
                {
                    SetNewState(State.LOWERING);
                }
                else if (_LastState == State.LOWERING)
                {
                    SetNewState(State.RAISING);
                }
                _ranTime = 0;
                _waitedTime = 0;
            }
        
        }
        else
        {
            if (_ranTime >= RunTime)
            {
                if (_curentState == State.RAISING)
                {
                    SetNewState(State.LOWERING);
                }
                else
                {
                    SetNewState(State.RAISING);
                }
                _ranTime = 0;
            }
        }
    }

    protected void SetNewState(State state)
    {
        _LastState = _curentState;
        _curentState = state;
        
    }

    protected void Run(bool revers)
    {
        _ranTime += Time.deltaTime;
        Debug.Log(RopeR.rope.CalculateLength());
        if (revers)
        {
            RopeL.cursor.ChangeLength(Mathf.Lerp(RopeL.EndRopeLength, RopeL.StartingRopeLength, _easingFunction(_ranTime / RunTime)));
            RopeR.cursor.ChangeLength(Mathf.Lerp(RopeR.EndRopeLength, RopeR.StartingRopeLength, _easingFunction(_ranTime / RunTime)));
        }
        else
        {
            RopeL.cursor.ChangeLength(Mathf.Lerp(RopeL.StartingRopeLength, RopeL.EndRopeLength, _easingFunction(_ranTime / RunTime)));
            RopeR.cursor.ChangeLength(Mathf.Lerp(RopeR.StartingRopeLength, RopeR.EndRopeLength, _easingFunction(_ranTime / RunTime)));
        }
        if (_ranTime >= RunTime)
        {
            _ranTime = 0;
        }

    }


    public virtual void Activate(GameObject activator)
    {
        Active = true;
        _curentState = State.LOWERING;
    }

    public virtual void Deactivate(GameObject activator)
    {
        Active = false;
        _curentState = State.STOPED;
    }
}
