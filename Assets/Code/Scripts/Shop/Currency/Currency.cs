﻿using System;
using UnityEngine;
[System.Serializable]
public class Currency
{
    public int Value;
    protected float Base = 1;
    public Currency()
    {
    }
    public Currency(int amount)
    {
        Value = amount;
    }
    public Currency(float amount)
    {
        if (Base != 1)
        {
            Value = (int)(amount * Base);
        }
        else
        {
          Value = (int)amount;
        }
    }

    public float FloatValue { get { return Value / Base; } }

    public virtual string FormatePrice()
    {
        return String.Format("{0} Scrap", FloatValue);
    }


    public static implicit operator int(Currency c) => c.Value;
    public static implicit operator Currency(byte b) => new Currency(b);

}

