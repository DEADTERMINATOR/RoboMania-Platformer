﻿using UnityEngine;
using System.Collections;
namespace ItemShop
{ 
    public sealed class ShopWorldShop : PlayerShop
    {
        protected override void PopulateShop()
        {
            this.ActiveProducts.AddRange(ProductsCatalog.AllProducts);
        }
    }
}