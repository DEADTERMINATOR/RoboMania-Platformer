﻿using UnityEngine;
using System.Collections;
using Weapons.Player.Chips;
using ItemShop;
using ForestSpritePack;
using RoboMania.UI;

public class BuyGunChip : ShopBuyScript
{
    public GunChip chipToGive;

    public override bool ItemWasPurchased(Product product, Shop shop)
    {
        
        bool chip1 = GlobalData.Player.Weapon.PlayerWeapon.ActiveForm.FirstSlottedGunChip != null;
        bool chip2 = GlobalData.Player.Weapon.PlayerWeapon.ActiveForm.SecondSlottedGunChip != null;
        if (chip1 && chip2)
        {
            PopUpHandle popUpHandle = UIPopUp.Make("Replace Shield ", "Replace The Current Shield", "Chip 1", "Chip 2");
            popUpHandle.OnOK.AddListener(() => { GlobalData.Player.Weapon.PlayerWeapon.ActiveForm.DropGunChip(1); });
            popUpHandle.OnCancel.AddListener(() => { GlobalData.Player.Weapon.PlayerWeapon.ActiveForm.DropGunChip(2); });
            popUpHandle.OnAny.AddListener(() => { GlobalData.Player.Weapon.PlayerWeapon.ActiveForm.SlotGunChip(chipToGive); });
            UIPopUp.Show(popUpHandle);
        }
        else 
        {
           return GlobalData.Player.Weapon.PlayerWeapon.ActiveForm.SlotGunChip(chipToGive);
        }
       
        return true;
    }
    // Use this for initialization

}
