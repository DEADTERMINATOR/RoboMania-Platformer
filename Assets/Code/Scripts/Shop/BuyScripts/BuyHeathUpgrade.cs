﻿using UnityEngine;
using System.Collections;
using ItemShop;
using ForestSpritePack;

public class BuyHeathUpgrade : ShopBuyScript
{
    float HeathIncrease = 10;

    public override bool ItemWasPurchased(Product product, Shop shop)
    {
        GlobalData.Player.Data.IncreaseMaxHealth(HeathIncrease);
        return true;
         
    }
}
