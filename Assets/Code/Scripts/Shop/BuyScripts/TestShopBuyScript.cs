﻿using ItemShop;
using RoboMania.UI;
using System;

public class TestShopBuyScript : ShopBuyScript
{

    public override bool ItemWasPurchased(Product product, Shop shop)
    {
        PopUpHandle popUpHandle = UIPopUp.Make("Test item percussed ", "You percussed "+product.Name+" For"+product.Price.FormatePrice(),"Ok","Test Refund");
        popUpHandle.OnCancel.AddListener(() => { shop.Refun(product.Price); });
        UIPopUp.Show(popUpHandle);
        return true;
    }
}

