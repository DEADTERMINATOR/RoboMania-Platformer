﻿using UnityEngine;
using System.Collections;

public class ScrapGivingDebugScript : MonoBehaviour
{

    public int ScrapToGivePlayer = 1000000;
    // Use this for initialization
    void Start()
    {
        GlobalData.Player.Data.AddScrapAmount(ScrapToGivePlayer);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
