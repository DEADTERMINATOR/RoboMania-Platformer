﻿using UnityEngine;
using System.Collections;
namespace ItemShop
{
    public class PlayerShop : Shop
    {
        private Currency _PlayerCurrency = new Currency();

        protected override bool Buy(Product item)
        {
            if(GlobalData.Player.Data.TryAndSpendScrap(item.Price))
            {
                return true;
            }
            return false;
        }

        protected override bool CanPlayerBuy(Product item)
        {
            if (GlobalData.Player.Data.TotalScrap >= item.Price)
            {
                return true;
            }
            return false;
        }

        internal override void Refun(Currency amount)
        {
            GlobalData.Player.Data.AddScrapAmount(amount);
        }

        internal override Currency GetPlayersTotalCurrency()
        {
            _PlayerCurrency.Value = GlobalData.Player.Data.TotalScrap;
            return _PlayerCurrency;
        }

        public override void CloseShop()
        {
            ActiveProducts?.Clear();
        }
    }
}
