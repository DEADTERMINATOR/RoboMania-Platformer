﻿using UnityEngine;
using System.Collections;
using ItemShop;
using Weapons.Player.Chips;

public class RuntimeProductFactories : MonoBehaviour
{

   public Product ProductFromGunChip(GunChip Chip, Currency price)
    {
        Product newProduct = ScriptableObject.CreateInstance<Product>();

        BuyGunChip buyScript = ScriptableObject.CreateInstance<BuyGunChip>();
        buyScript.chipToGive = Chip;
        newProduct.BuyScript = buyScript;
        newProduct.Name = Chip.name;
        newProduct.Decription = "Still Needs To Be implemented ";
        newProduct.Price = price;
        return newProduct;
    }

    public Product ProductFromSheaild(Shield shiled, Currency price)
    {
        Product newProduct = ScriptableObject.CreateInstance<Product>();

        BuyShiled buyScript = ScriptableObject.CreateInstance<BuyShiled>();
        buyScript.chipToGive = shiled;
        newProduct.BuyScript = buyScript;
        newProduct.Name = shiled.name;
        newProduct.Decription = "Still Needs To Be implemented ";
        newProduct.Price = price;
        return newProduct;
    }

    public Product RuntimeProduct<T>() where T : ShopBuyScript
    {
        Product newProduct = ScriptableObject.CreateInstance<Product>();
        newProduct.BuyScript = ScriptableObject.CreateInstance<T>();
        return newProduct;
    }

    public Product RuntimeProduct(string type)
    {
        Product newProduct = ScriptableObject.CreateInstance<Product>();
        ShopBuyScript buyScript = (ShopBuyScript)ScriptableObject.CreateInstance(type);
        if (buyScript)
        {
            newProduct.BuyScript = buyScript;
            return newProduct;
        }

        return null;
    }
}
