﻿#define ITEMSHOP_USING_TMP
#if ITEMSHOP_USING_TMP
using TMPro;
#endif
using UnityEngine;
using UnityEngine.UI;
namespace ItemShop.UI
{
    public class ShopUIItem : MonoBehaviour
    {
#if ITEMSHOP_USING_TMP
        public TMP_Text NameText;
        public TMP_Text DescriptionText;
        public TMP_Text PriceText;
#else
        public Text NameText;
        public Text DescriptionText;
        public Text PriceText;
#endif
        public Image ProductImage;
        public Button BuyButton;
        protected Product Product;
        internal ShopUI iParnetShopUI;

        private void Awake()
        {
            BuyButton.onClick.AddListener(TryAndBuy);
        }

        public void SetSelected()
        {
            BuyButton.Select();
        }

        private void TryAndBuy()
        {
            //TODO:Add popup conflation
            iParnetShopUI.Shop.TryandBuy(Product);
        }
        internal void Populate(ShopUI parentShop, Product product)
        {
            Product = product;
            NameText.text = Product.Name;
            DescriptionText.text = Product.Decription;
            PriceText.text = Product.Price.FormatePrice();
            ProductImage.sprite = Product.Image;
            iParnetShopUI = parentShop;
        }
    }

}