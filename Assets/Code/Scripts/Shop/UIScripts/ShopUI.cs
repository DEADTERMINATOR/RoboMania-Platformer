﻿using UnityEngine;
using System.Collections;
using ItemShop;
using RoboMania.UI;
using System.Collections.Generic;
using UnityEngine.UI;
using ForestSpritePack;
using System;
using Inputs;
using System.Linq;
using LevelSystem;

namespace ItemShop.UI
{

    public class ShopUI : UIWindow
    {
        // the shop the Ui is showing
        public Shop Shop;
        /// <summary>
        /// the Ui Template for products 
        /// </summary>
        public ShopUIItem ProductTemplate;
        /// <summary>
        /// THe root of the view 
        /// </summary>
        public Transform ProductListRoot;

        public Text PlayerCurrencyText;
        #region Input
        private InputManager _inputManagerInstanceRef;
        public InputManager.RegisteredCallback SelectCallback;
        public InputManager.RegisteredCallback BackCallback;
        public InputManager.RegisteredCallback LeftCallback;
        public InputManager.RegisteredCallback RightCallback;
        #endregion
        private int SelectedProduct = 0;

        private Dictionary<Guid, ShopUIItem> shopUIItems = new Dictionary<Guid, ShopUIItem>();


        public override void Start()
        {
            base.Start();

            Level.CurentLevel.OnPause.AddListener(HandlePause);
            Level.CurentLevel.OnUnpause.AddListener(HandlePause);
            _inputManagerInstanceRef = InputManager.Instance;


            _registeredInputCallbacks.Add(_inputManagerInstanceRef.RegisterSpecificActionPerformed("Select", () =>
            {
                if (SelectedProduct != shopUIItems.Count)
                {

                    shopUIItems.ElementAt(SelectedProduct).Value.BuyButton.onClick?.Invoke();
                }
                else
                {
                    Close();
                }

            }, false));
            _registeredInputCallbacks.Add(_inputManagerInstanceRef.RegisterSpecificActionPerformed("Back", () =>
            {
                Close();
            }, false));
            _registeredInputCallbacks.Add(_inputManagerInstanceRef.RegisterSpecificActionPerformed("Left", () =>
            {
                SelectedProduct--;
                if (SelectedProduct < 0)
                {
                    SelectedProduct = shopUIItems.Count;
                }
                if (SelectedProduct == shopUIItems.Count)
                {
                    CloseButton.Select();
                }
                else
                {
                    shopUIItems.ElementAt(SelectedProduct).Value.SetSelected();
                }
                    
            }, false));
            _registeredInputCallbacks.Add(_inputManagerInstanceRef.RegisterSpecificActionPerformed("Right", () =>
           {
               SelectedProduct++;
               if (SelectedProduct == shopUIItems.Count)
               {
                   CloseButton.Select();
               }
               else
               {
                   if (SelectedProduct > shopUIItems.Count)
                   {
                       SelectedProduct = 0;
                   }

                   shopUIItems.ElementAt(SelectedProduct).Value.SetSelected();
               }

           }, false));
            _registeredInputCallbacks.Add(_inputManagerInstanceRef.RegisterSpecificActionPerformed("CloseWindow", () =>
            {
                Close();

            }, false));
        }

        private void ProductPrechesed(Product item)
        {
            ShopUIItem uiIten = shopUIItems[item.uID];
            shopUIItems.Remove(item.uID);
            Destroy(uiIten.gameObject);
            PlayerCurrencyText.text = GlobalData.Player.Data.TotalScrap.ToString();
        }

        public override void Show()
        {

            var instance = InputManager.Instance;

            instance.EnableMenuActions();
            instance.DisableInGameActions();
            EnableRegisteredInputCallbacks();
            CleanList();
            Shop.OpenShop();
            shopUIItems.Clear();
            Shop.OnProductPrechesed.AddListener(ProductPrechesed);
            foreach (Product product in Shop.ActiveProducts)
            {
                ShopUIItem newItemUI = Instantiate<ShopUIItem>(ProductTemplate);
                newItemUI.Populate(this, product);
                newItemUI.transform.parent = ProductListRoot;
                shopUIItems.Add(product.uID, newItemUI);
            }
            PlayerCurrencyText.text = GlobalData.Player.Data.TotalScrap.ToString();
            base.Show();
        }
        public override void Close()
        {
            var instance = InputManager.Instance;
            instance.DisableMenuActions();
            instance.EnableInGameActions();
            DisableRegisteredInputCallbacks();
            base.Close();
            Shop.CloseShop();
        }

        private void HandlePause()
        {
            var instance = InputManager.Instance;
            instance.DisableMenuActions();
            instance.EnableInGameActions();
            DisableRegisteredInputCallbacks();
        }

        private void HandleUnpause()
        {
            var instance = InputManager.Instance;
            instance.EnableMenuActions();
            instance.DisableInGameActions();
            DisableRegisteredInputCallbacks();
        }
        private void CleanList()
        {
            foreach (ShopUIItem item in shopUIItems.Values)
            {
                Destroy(item.gameObject);
            }
        }
    }
}
