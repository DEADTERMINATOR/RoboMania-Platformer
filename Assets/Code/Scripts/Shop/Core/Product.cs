﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ItemShop
{
   /// [CreateAssetMenu(fileName = "New Product.Product", menuName = "Store/New Standalone Product", order = 1)]
    [RequireComponent(typeof(ShopBuyScript))]
    public class Product : ScriptableObject 
    {
        /// <summary>
        /// Name of the product
        /// </summary>
        public string Name;
        /// <summary>
        /// The description of the product 
        /// </summary>
        public string Decription;
        /// <summary>
        /// The shop display image
        /// </summary>
        public Sprite Image;
        /// <summary>
        /// The price on the shop can be $ or in game currency  
        /// </summary>
        public Currency Price = 0;
        /// <summary>
        /// How much is this Product is on sale if the amount if gather of equal to the price the item will be free 
        /// </summary>
        public int SalePriceReduction;
        /// <summary>
        /// Should This be hidden from the shop
        /// </summary>
        public bool Hidden;
        /// <summary>
        /// unique identifier
        /// </summary>
        public Guid uID = Guid.NewGuid();

        public ShopBuyScript BuyScript;
    }
    public class ProductEvent : UnityEvent<Product> { };
}