﻿using UnityEngine;
using System.Collections;
using Characters.Player;


namespace ItemShop
{
    /// <summary>
    /// Handles the logic of buying an item 
    /// </summary>
    public abstract class ShopBuyScript : ScriptableObject
    {
        /// <summary>
        /// Called after the player has Purchased an item
        /// </summary>
        /// <param name="product">The Product Thats was Purchased as  buy script are  one to many relationship </param>
        /// <param name="shop">The Shop The Products Was in as Product is one to many with shop</param>
        /// <returns>true they player got the good and Flase something whet wrong a refund is issued if false is returned </returns>
        public abstract bool ItemWasPurchased(Product product,Shop shop);

    }
}    