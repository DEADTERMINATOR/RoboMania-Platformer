﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;
using System;
using UnityEngine.InputSystem.UI;
using System.ComponentModel.Design;

namespace ItemShop
{
    public abstract class Shop : MonoBehaviour
    {
        #region Unity Interface
        /// <summary>
        /// All possible products this can be filtered 
        /// </summary>
        [Tooltip("Should not be edited at run time")]
        public ProductsCatalog ProductsCatalog;
        /// Public interface to ActiveProducts 
        /// </summary>
        /// 
        #endregion

        #region Public Getters
        public List<Product> ActiveProducts { get { return _activeProducts; }  }
        public List<Product> AllProductsList { get { return ProductsCatalog.AllProducts.ToList(); } }

        #endregion

        #region Events
        public ProductEvent OnProductAdded = new ProductEvent();
        public ProductEvent OnProductRemoved = new ProductEvent();
        public ProductEvent OnProductPrechesed = new ProductEvent();
        #endregion

        #region private Vars
        private List<Product> _activeProducts = new List<Product>();
        #endregion

        #region Product Manipulation 
        protected virtual void PopulateShop()
        {
            _activeProducts = ProductsCatalog.AllProducts.ToList(); 
        }
        public void AddProduct(Product product)
        {
            _activeProducts.Add(product);
            OnProductAdded?.Invoke(product);
        }
        public void AddProduct(int index)
        {
            if (index > 0 && index < ProductsCatalog.AllProducts.Length)
            {
               AddProduct(ProductsCatalog.AllProducts[index]);
            }
        }
        public void RemoveProduct(Product product)
        {
            _activeProducts.Add(product);
            OnProductRemoved?.Invoke(product);
        }
        public void RemoveProduct(int index)
        {
            if (index > 0 && index < ProductsCatalog.AllProducts.Length)
            {
               RemoveProduct(ProductsCatalog.AllProducts[index]);
            }
        }
        #endregion

        #region Perches  Functions 
        /// <summary>
        /// Implement the buy function 
        /// </summary>
        /// <param name="item"></param>
        public void TryandBuy(Product item)
        {
            if (CanPlayerBuy(item))
            {
                if (Buy(item))
                {
                    if (item.BuyScript.ItemWasPurchased(item,this))
                    {
                        OnProductPrechesed?.Invoke(item);
                    }
                }
            }
        }

        /// <summary>
        /// Implement the buy function 
        /// </summary>
        /// <param name="item"></param>
        protected abstract bool Buy(Product item);

        internal abstract void Refun(Currency amount);

        

        protected bool Buy(int i)
        {
            if(i > 0 && i < _activeProducts.Count)
            {
               return Buy(_activeProducts[i]);
            }
            return false;//no item so you can not buy
        }

        protected virtual bool CanPlayerBuy(Product item)
        {
            return false;
        }
        #endregion

        internal abstract Currency GetPlayersTotalCurrency();

        private void Awake()// populate here
        {
            
        }

        public virtual void OpenShop()
        {
            PopulateShop();
        }
        public virtual void CloseShop()
        {

        }
    }

}