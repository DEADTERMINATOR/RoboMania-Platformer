﻿
using UnityEngine;

namespace ItemShop
{
    [CreateAssetMenu(fileName = "New Product.asset", menuName = "Store/New Product Catalog", order = 1)]
    public class ProductsCatalog : ScriptableObject
    {

        public Product[] AllProducts;

    }
}
