﻿using UnityEngine;
using System.Collections;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Linq;

public class TimelinePlayerRuntimeInjector : MonoBehaviour
{
    public PlayableDirector Director;
    public TimelineAsset Timeline;
    public int Index = 0;
    // Use this for initialization
    private void Awake()
    {
        Object track = (TrackAsset)Timeline.outputs.ElementAt(Index).sourceObject; 
        Director.SetGenericBinding(track, GlobalData.Player.gameObject);

    }
}
