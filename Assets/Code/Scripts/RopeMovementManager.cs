﻿using LevelComponents.Platforms;
using Obi;
using System;
using UnityEngine;


public class RopeMovementManager : MonoBehaviour , IActivatable
{
    /// <summary>
    /// to be used with new IActivatable
    /// </summary>
    public bool Active { get; protected set; } = false;

    [Serializable]
    public class RopeData
    {
        public ObiRopeCursor cursor;
        public ObiRope rope;
        public float StartingRopeLength = 0;
        public float EndRopeLength = 0;
    }
    public RopeData[] Ropes;
    public float speed;
    public float Amount;
    public float RunTime = 4;
    private float _ranTime = 0;
    private bool _runForward = false;
   
    private void Update()
    {
        if(Active)
        {
            Run(_runForward);
        }
    }

    protected void Run(bool revers)
    {

        foreach (RopeData ropeData in Ropes)
        {
            _ranTime += Time.deltaTime;
            if (revers)
            {
                ropeData.cursor.ChangeLength(Mathf.Lerp(ropeData.EndRopeLength, ropeData.StartingRopeLength, _ranTime / RunTime));
            }
            else
            {
                ropeData.cursor.ChangeLength(Mathf.Lerp(ropeData.StartingRopeLength, ropeData.EndRopeLength, _ranTime / RunTime));
            }
            if (_ranTime >= RunTime)
            {
                Active = false;
                _runForward = !_runForward;
                _ranTime = 0;
            }
        }
    }

    private void Start()
    {
        foreach (RopeData ropeData in Ropes)
        {
            ropeData.StartingRopeLength = ropeData.rope.restLength;
            ropeData.EndRopeLength = ropeData.rope.restLength+ Amount;
        }
    }

    public virtual void Activate(GameObject activator)
    {
        Active = true;
    }

    public virtual void Deactivate(GameObject activator)
    {
        Active = false;
    }
}

