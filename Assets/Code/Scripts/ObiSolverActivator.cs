﻿using Obi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class ObiSolverActivator :MonoBehaviour, IActivatable
 {
    /// <summary>
    /// the solver to controll 
    /// </summary>
    public ObiSolver Solver;
    public bool InstialState;

    public bool Active { get { return Solver.gameObject.activeSelf; } }

    public void Awake()
    {
        Solver.gameObject.SetActive(InstialState);
    }

    public void Activate(GameObject activator)
    {
        Solver.gameObject.SetActive(true);
    }

    public void Deactivate(GameObject activator)
    {
        Solver.gameObject.SetActive(false);
    }
}

