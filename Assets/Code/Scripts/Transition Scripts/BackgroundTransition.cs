﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using MovementEffects;
using Characters.Player;

public class BackgroundTransition : MonoBehaviour
{

    /// <summary>
    /// An enum containing the possible directions the player can transition off screen.
    /// </summary>
    public enum TransitionDirection { RIGHT, LEFT, UP, DOWN, SPECIAL }


    /// <summary>
    /// The distance that the transition will move the player when taking them off screen.
    /// </summary>
    public float WalkOffDistance;

    /// <summary>
    /// The distance the transition will move the player when bringing them back on screen.
    /// </summary>
    [Tooltip("Only the X value is used in right or left transitions.")]
    public Vector2 WalkInDistance;

    /// <summary>
    /// How long the transition will take to fade to black.
    /// </summary>
    public float FadeToBlackTime;

    /// <summary>
    /// How long the transition will stay black before beginning to fade back in.
    /// </summary>
    public float StayBlackTime;

    /// <summary>
    /// How long the transition will take to fade back in.
    /// </summary>
    public float FadeBackInTime;

    /// <summary>
    /// The name of the scene this transition goes into.
    /// </summary>
    public string SceneToTransitionTo;

    /// <summary>
    /// The starting point that should be used on the other side of the transition.
    /// </summary>
    public GameObject StartingPoint;

    /// <summary>
    /// The direction the transition takes the player.
    /// </summary>
    public TransitionDirection Direction;

    /// <summary>
    /// The camera movement area that should be used after the transition. This only needs to be set if there are multiple camera movement areas
    /// in the same scene (or the main movement area defies the standard naming convention). Otherwise, the code will find the appropriate camera
    /// movement area.
    /// </summary>
    [Tooltip("This only needs to be set if there are multiple camera movement areas in the same scene\n(or the main movement area defies the standard naming convention)")]
    public Transform AlternateCameraMovementAreaToUse;

    public bool SkipSettingCheckPoint = false;

    public bool SetZoomLevel = false;

    public float zoomLevel = 7.5f;

    /// <summary>
    /// The box collider trigger that begins the transition.
    /// </summary>
    private BoxCollider2D _transitionTrigger;

    /// <summary>
    /// The black image that will be faded in and out during a transition.
    /// </summary>
    private Image _fadeImage;

    /// <summary>
    /// Reference to the player master stored because of frequent use.
    /// </summary>
    private PlayerMaster _playerRef;


    // Use this for initialization
    protected virtual void Start()
    {
        _transitionTrigger = GetComponent<BoxCollider2D>();
        _fadeImage = GameObject.FindGameObjectWithTag("FadeImage").GetComponent<Image>();
        _playerRef = GlobalData.Player;
    }

    /// <summary>
    /// Triggers the co-routine that handles the transition off the current screen and onto the next one.
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            if (TheGame.GetRef.gameState != TheGame.GameState.TRANSITION)
            {
                //Block the player update loop with a bool and start the co-routine.
                TheGame.GetRef.gameState = TheGame.GameState.TRANSITION;
                GlobalData.NotifySceneTransition();
                Timing.RunCoroutine(Transition(), "Area Transition");
            }
        }
    }

    /// <summary>
    /// Handles the transition off the screen and onto the next one.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> Transition()
    {
        TheGame.GetRef.gameState = TheGame.GameState.TRANSITION;

        //Lock the movement of the camera, so it doesn't follow the player in whatever happens off screen.
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        /*CameraFollow cameraFollow = camera.GetComponent<CameraFollow>();
        cameraFollow.Locked = true;*/

        //Hold the player's current position and set the final off screen position.
        Vector3 startingPosition = _playerRef.transform.position;
        Vector3 finalPosition = Vector3.zero;

        //Declarations for future use.
        float percentagePositionChange;

        //Debug.Break();
        #region Move to offscreen position and fade out
        if (Direction == TransitionDirection.RIGHT || Direction == TransitionDirection.LEFT)
        {
            _playerRef.Movement.CurrentCollisionCheck = PlayerMovementController.CollisionCheck.Vertical;
            //If the player is on the ground when the cross the transition, or they are in the air, but not dashing, we walk them off the screen. Otherwise, we just let them maintain their velocity
            if (_playerRef.Controller.Collisions.IsCollidingBelow || (!_playerRef.Controller.Collisions.IsCollidingBelow && _playerRef.Velocity.x <= _playerRef.MoveSpeed))
            {
                if (Direction == TransitionDirection.RIGHT) //The Y value in the final position is ultimately ignored. Instead, gravity determines the final Y position. But a value needs to be provided.
                {
                    finalPosition = new Vector3(startingPosition.x + WalkOffDistance, startingPosition.y, startingPosition.z);
                    _playerRef.Animation.SetSpriteDirection(true); //Make sure the player is facing the direction of the transition.
                }
                else
                {
                    finalPosition = new Vector3(startingPosition.x - WalkOffDistance, startingPosition.y, startingPosition.z);
                    _playerRef.Animation.SetSpriteDirection(false);
                }

                //Zero out the X-velocity so the only X movement is the walk-off.
                _playerRef.SetVelocityOnOneAxis('X', 0f);

                //Begin the co-routine to fade the screen to black as the player leaves.
                Timing.RunCoroutine(FadeOut(), "Fade Out");

                //Move the player off the current screen.
                percentagePositionChange = 0f;
                while (percentagePositionChange < 1f)
                {
                    percentagePositionChange += Time.deltaTime / FadeToBlackTime;
                  
                    _playerRef.transform.position = new Vector3(Mathf.Lerp(startingPosition.x, finalPosition.x, percentagePositionChange), _playerRef.transform.position.y, startingPosition.z);
                    yield return 0f;
                }
            }
            else
            {
                //Begin the co-routine to fade the screen to black as the player leaves.
                Timing.RunCoroutine(FadeOut(), "Fade Out");
            }
        }
        else if (Direction == TransitionDirection.UP || Direction == TransitionDirection.DOWN)
        {
            if (Direction == TransitionDirection.UP)
            {
                _playerRef.GravityScale = 0;
                finalPosition = new Vector3(startingPosition.x, startingPosition.y + WalkOffDistance, startingPosition.z);
            }
            else
            {
                finalPosition = new Vector3(startingPosition.x, startingPosition.y - WalkOffDistance, startingPosition.z);
            }

            //Begin the co-routine to fade the screen to black as the player leaves.
            Timing.RunCoroutine(FadeOut(), "Fade Out");

            if (Direction == TransitionDirection.UP)
            {
                //Move the player off the current screen.
                percentagePositionChange = 0f;
                while (percentagePositionChange < 1f)
                {
                    percentagePositionChange += Time.deltaTime / FadeToBlackTime;
                    _playerRef.transform.position = new Vector3(_playerRef.transform.position.x, Mathf.Lerp(startingPosition.y, finalPosition.y, percentagePositionChange), startingPosition.z);
                    yield return 0f;
                }
            }
        }
        else
        {
            //Begin the co-routine to fade the screen to black as the player leaves.
            Timing.RunCoroutine(FadeOut(), "Fade Out");
        }
        #endregion

        //Wait for the designated amount of time. This is so the process feels smoother.
        yield return Timing.WaitForSeconds(StayBlackTime);



        #region Find onscreen starting point in next scene
        //Set the player's starting position in the new area and the final position when control resumes.
        GameObject newStartingPosition = StartingPoint;
        if (!StartingPoint)
        {
            newStartingPosition = GameObject.Find(GameMaster.GameManager.Instance.LoadedDatatpackage[0].Path  + " Starting Point");
            if (!newStartingPosition)
            {
                //Debug.LogError("No starting point assigned, and no default starting point to fall back on.");
            }
        }

        startingPosition = newStartingPosition.transform.position;

        if (Direction == TransitionDirection.RIGHT) //The Y value is ignored for the same reason as above.
        {
            finalPosition = new Vector3(startingPosition.x + Mathf.Abs(WalkInDistance.x), startingPosition.y, startingPosition.z);

            //Reset the player's Y position since gravity may have moved it somewhere undesirable in between screens, and the transition doesn't call for Y movement.
            _playerRef.transform.position = new Vector3(_playerRef.transform.position.x, startingPosition.y, _playerRef.transform.position.z);
            _playerRef.SetVelocityOnOneAxis('X', 0f);
        }
        else if (Direction == TransitionDirection.LEFT)
        {
            finalPosition = new Vector3(startingPosition.x - Mathf.Abs(WalkInDistance.x), startingPosition.y, startingPosition.z);

            //Reset the player's Y position since gravity may have moved it somewhere undesirable in between screens, and the transition doesn't call for Y movement.
            _playerRef.transform.position = new Vector3(_playerRef.transform.position.x, startingPosition.y, _playerRef.transform.position.z);
            _playerRef.SetVelocityOnOneAxis('X', 0f);
        }
        else if (Direction == TransitionDirection.UP)
        {
            finalPosition = new Vector3(startingPosition.x + WalkInDistance.x, startingPosition.y + WalkInDistance.y, startingPosition.z);

            //Reset the player's position since leftover velocity has probably moved the player away from the desired starting spot.
            _playerRef.transform.position = new Vector3(startingPosition.x, startingPosition.y, _playerRef.transform.position.z);
            _playerRef.SetVelocityOnOneAxis('X', 0f);
            _playerRef.SetVelocityOnOneAxis('Y', 0f);

            //Make sure the player is facing the direction they will be popped into the next scene.
            if (WalkInDistance.x < 0)
            {
                _playerRef.Animation.SetSpriteDirection(false, true);
            }
            else
            {
                _playerRef.Animation.SetSpriteDirection(true, true);
            }
        }
        else
        {
            finalPosition = startingPosition; //In the case of a down transition, gravity can do all the movement for us.

            //Reset the player's position since gravity will have likely dropped the player all the way into the next scene.
            _playerRef.transform.position = new Vector3(startingPosition.x, startingPosition.y, _playerRef.transform.position.z);
            _playerRef.SetVelocityOnOneAxis('X', 0f);
            _playerRef.SetVelocityOnOneAxis('Y', 0f);
        }
        #endregion

        #region Set up camera in next scene
        //Position the camera in the new scene and set the new active camera movement area.
       /* cameraFollow.Locked = false;

        if (AlternateCameraMovementAreaToUse == null)
            //A specific camera movement area was not set, so find the default.
            cameraFollow.SetNewActiveCameraMovementArea(GameObject.Find(GameMaster.GameManager.Instance.LoadedDatatpackage[0].Path  + " Camera Movement Area").transform, Direction == TransitionDirection.RIGHT);
        else
            cameraFollow.SetNewActiveCameraMovementArea(AlternateCameraMovementAreaToUse, Direction == TransitionDirection.RIGHT);
            */
        if (SetZoomLevel)
        {
            //cameraFollow.ChangeCameraZoom(zoomLevel, 0);
            GlobalData.CameraManager.ChangeCameraZoom(zoomLevel, 0);
        }
           
        #endregion

        #region Move into next scene and fade in
        //Being the co-routine to bring the color back as the player comes back on the screen.
        Timing.RunCoroutine(FadeIn(), "Fade In");

        //Move the player onto the current screen.
        if (Direction == TransitionDirection.RIGHT || Direction == TransitionDirection.LEFT)
        {
            percentagePositionChange = 0f;
            while (percentagePositionChange < 1f)
            {
                percentagePositionChange += Time.deltaTime / FadeBackInTime;
                _playerRef.transform.position = new Vector3(Mathf.Lerp(startingPosition.x, finalPosition.x, percentagePositionChange), _playerRef.transform.position.y, startingPosition.z);
                yield return 0f;
            }
        }
        else if (Direction == TransitionDirection.UP)
        {
            /*
            For an upward transition, we want to "pop" the player up in an extra high jump arc so that they land on the platform beside the hole they just came up in.
            So we need to control how the player enters the scene to mimic a jump arc.

            To do this, the player will be moved up and forward based on the provided MoveInDistance, which should be set up so that the X distance is about the required amount to get onto the platform,
            but the Y distance is higher than the platform, so gravity can bring them back to the ground and complete the "jump arc".

            The player's jump velocity will be used as a base for the amount that the player can be moved towards the peak of the jump each frame.
            */

            bool peakHeightAchieved = false;
            float maxStepTime = _playerRef.Movement.JumpVelocity * 1.25f * Time.deltaTime;

            Vector3 peakHeightPosition = new Vector3(_playerRef.transform.position.x + WalkInDistance.x, _playerRef.transform.position.y + WalkInDistance.y, _playerRef.transform.position.z);
            float totalDistanceToPeakHeight = Vector3.Distance(_playerRef.transform.position, peakHeightPosition);

            while (!peakHeightAchieved)
            {
                Vector3 newPosition = Vector3.MoveTowards(_playerRef.transform.position, peakHeightPosition, maxStepTime);
                _playerRef.transform.position = newPosition;

                if (Vector3.Distance(_playerRef.transform.position, peakHeightPosition) <= 0.05f)
                {
                    peakHeightAchieved = true;
                    _playerRef.GravityScale = 1;

                    _playerRef.SetVelocityOnOneAxis('X', _playerRef.MoveSpeed * _playerRef.Controller.Collisions.FaceDir);
                    _playerRef.SetVelocityOnOneAxis('Y', _playerRef.Movement.Gravity.y * Time.deltaTime);
                }

                yield return 0f;
            }
        }
        else if (Direction == TransitionDirection.DOWN)
        {
            _playerRef.SetVelocityOnOneAxis('Y', -1.61f); //We don't want the gravity carried over, but we want to set the velocity to right beyond the falling animation threshold so that animation is immediately active.
            //TODO: Set the player's falling animation as well.
        }
        else
        {
            PerformSpecialTransition();
        }
        #endregion

        if (!SkipSettingCheckPoint)
        {
            /* LUpdae //Set the last checkpoint to the player's current location as transitions act as checkpoints.
             GlobalData.LevelInformation.LastCheckpoint = _playerRef.transform.position;
             GlobalData.LevelInformation.CheckpointZoomLevel = GlobalData.CameraManager.ZoomLevel; //cameraFollow.ZoomLevel;*/
        }

        //End the blocking of the player update loop and set the player back to the idle animation.
        _playerRef.Movement.CurrentCollisionCheck = PlayerMovementController.CollisionCheck.Both;

        //Finish at the end of the current frame.
        yield return 0f;
    }

    /// <summary>
    /// Fades the screen to black as the player leaves the screen.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> FadeOut()
    {
        TheGame.GetRef.gameState = TheGame.GameState.TRANSITION;
        float alphaPercentage = 0f;
        while (alphaPercentage < 1f)
        {
            alphaPercentage += Time.deltaTime / FadeToBlackTime;
            Color temp = _fadeImage.color;
            temp.a = alphaPercentage;
            _fadeImage.color = temp;
            yield return 0f;
        }
        yield return 0f;
    }

    /// <summary>
    /// Brings color back to the screen as the player returns.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> FadeIn()
    {
        float alphaPercentage = 0f;
        while (alphaPercentage < 1f)
        {
            alphaPercentage += Time.deltaTime / FadeBackInTime;
            Color temp = _fadeImage.color;
            temp.a = 1f - alphaPercentage;
            _fadeImage.color = temp;
            yield return 0f;
        }

        _playerRef.Input.BlockInput = false;
        TheGame.GetRef.gameState = TheGame.GameState.RUNNING;
        yield return 0f;
    }

    protected virtual void PerformSpecialTransition()
    {
        _playerRef.Input.BlockInput = true;
    }
}

