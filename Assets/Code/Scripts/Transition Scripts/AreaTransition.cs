﻿using UnityEngine;
using System.Collections;
using UnityEngine.Timeline;
using Characters.Player;
using System.Collections.Generic;
using UnityEngine.Events;
using LevelSystem.SateData;

[RequireComponent(typeof(WalkPlayerToPoint))]
public class AreaTransition : MonoBehaviour
{
    /// <summary>
    /// The Direct this Transition works in
    /// </summary>
    public enum TransitionDirection { VERTICAL, HORIZONTAL, SPECIAL }
    public float FadeOutTime = 1;
    public float FadeInTime = 1;
    public TransitionDirection transitionDirection = TransitionDirection.HORIZONTAL;
    public TransitionPoint LeftOrDownPoint;
    public TransitionPoint RightOrUpPoint;
    public Color LeftOrDownDebugColor = Color.green;
    public Color RightOrUpDebugColor = Color.yellow;
    /// <summary>
    /// Unity Editor only Should this alway show its gizmo's
    /// </summary>
    public bool ShowGizmosAtAllTimes = false;
 

    public string LeftOrDownStateVar;
    public string RightOrUpStateVar;

    /// <summary>
    /// This event is called after  the fade to black happens 
    /// </summary>
    public UnityEvent OnTransition = new UnityEvent();

    private bool _isDoingTransition = false;
    private PlayerMaster _player;
    private WalkPlayerToPoint _walkScript;
    private Vector3 _actualPoint;
    private Vector3 _VelocityOnEnter;
    private float _savedGravity = 0;
    private Collider2D _theCollider;
    private bool _isGoingUp = false;
    private bool _isGoingLeft = false;

    public LevelStateBool _leftOrDownStateVar;
    public LevelStateBool _rightOrUpStateVar;


    private bool _DoNotAllowPlayerToGoBackDown = false;
    private float _playersLastY;

    private void Start()
    {
        _player = GlobalData.Player;
        _walkScript = GetComponent<WalkPlayerToPoint>();
        if (!string.IsNullOrEmpty(LeftOrDownStateVar))
        {
            _leftOrDownStateVar = LevelState.Instance.LoadLevelStateBool(LeftOrDownStateVar, false);
        }
        if (!string.IsNullOrEmpty(RightOrUpStateVar))
        {
            _rightOrUpStateVar = LevelState.Instance.LoadLevelStateBool(RightOrUpStateVar, false);
        }
        _theCollider = GetComponent<Collider2D>();
        if (transitionDirection == TransitionDirection.HORIZONTAL)
        {
            _walkScript.OnCompleet.AddListener(AfterWalk);
        }


    }


    public bool GoingLeft()
    {

        if (_theCollider.bounds.center.x < GlobalData.PlayerTransfrom.position.x)
        {
            return true;
        }

        return false;
    }
    public bool GoingUP()
    {

        if (_theCollider.bounds.center.y > GlobalData.PlayerTransfrom.position.y)
        {
            return true;
        }

        return false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
          if (collision.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            if (!_isDoingTransition)
            {
                CameraMovmentArea.BlockCameraChanges = true;
                if (transitionDirection == TransitionDirection.HORIZONTAL)
                {
                    BackgroundManager.OverlayFader.OnAboutToShow.AddListener(WalkPlayeyTo);
                    _isGoingLeft = GoingLeft();
                    Debug.Log(_isGoingLeft);
                    if (_isGoingLeft)
                    {

                        _actualPoint = LeftOrDownPoint.ExitPoint.position;
                        _actualPoint = new Vector3(_actualPoint.x, _player.transform.position.y, 0);
                        _player.Movement.Stop();
                    }
                    else
                    {
                        _actualPoint = RightOrUpPoint.ExitPoint.position;
                        _actualPoint = new Vector3(_actualPoint.x, _player.transform.position.y, 0);
                        _player.Movement.Stop();
                    }
                }
                if (transitionDirection == TransitionDirection.VERTICAL)
                {
                    _isGoingUp = GoingUP();
                    if (!_isGoingUp)
                    {
                        BackgroundManager.OverlayFader.OnAboutToShow.AddListener(KeepMoving);
                        _savedGravity = _player.GravityScale;
                        _player.GravityScale = 0;
                        _VelocityOnEnter = GlobalData.Player.Velocity;
                        _actualPoint = LeftOrDownPoint.ExitPoint.position;
                        _actualPoint = new Vector3(_player.transform.position.x, _actualPoint.y, 0);
                    }
                    else
                    {
                        _savedGravity = _player.GravityScale;
                        _player.GravityScale = 0;
                        _DoNotAllowPlayerToGoBackDown = true;
                        _actualPoint = RightOrUpPoint.ExitPoint.position;
                        BackgroundManager.OverlayFader.OnAboutToShow.AddListener(PopUp);
                    }
                }
                BackgroundManager.OverlayFader.OnFinished.AddListener(HandleFadeEvents);
                BackgroundManager.OverlayFader.FadeIn(FadeInTime, true, 0.5f, FadeOutTime);
                _isDoingTransition = true;
            }
        }
    }

    private void HandleFadeEvents(OverlayFader.States state)
    {
        if (state == OverlayFader.States.FADEINGIN)//done fading in
        {

            GlobalData.PlayerTransfrom.position = _actualPoint;
            CameraMovmentArea.BlockCameraChanges = false;
            OnTransition?.Invoke();
            if(_isGoingUp)
            {
                _player.GravityScale = 0;
            }
            if (!_isGoingUp && !_isGoingUp)
            {
                if(_leftOrDownStateVar != null)
                    _leftOrDownStateVar.Value = true;
                if (_rightOrUpStateVar != null)
                    _rightOrUpStateVar.Value = false;
            }
            else
            {
                if (_leftOrDownStateVar != null)
                    _leftOrDownStateVar.Value = false;
                if (_rightOrUpStateVar != null)
                    _rightOrUpStateVar.Value = true;
            }
        }

    }

    private void WalkPlayeyTo()
    {
        BackgroundManager.OverlayFader.OnAboutToShow.RemoveListener(WalkPlayeyTo);
        Debug.Log("Start To Walk");
        Vector3 walkToPoint;
        if (_isGoingLeft)
        {
            walkToPoint = GlobalData.PlayerTransfrom.position + LeftOrDownPoint.WalkOffAmount;
        }
        else
        {
            walkToPoint = GlobalData.PlayerTransfrom.position + RightOrUpPoint.WalkOffAmount;
        }
        _walkScript.StartWalk(walkToPoint);
    }
    private void KeepMoving()
    {
        BackgroundManager.OverlayFader.OnAboutToShow.RemoveListener(KeepMoving);
        _player.Movement.Restart();
        _player.Movement.SetVelocity('y', _VelocityOnEnter.y);
        _player.Movement.SetVelocity('x', _VelocityOnEnter.x);
        _player.GravityScale = _savedGravity;
        CleanUp();
    }

    private void PopUp()
    {
        BackgroundManager.OverlayFader.OnAboutToShow.RemoveListener(PopUp);
        StartCoroutine(DoUp());
    }
    private void AfterWalk()
    {
        CleanUp();
        _player.Movement.Restart();
    }

    private void OnDrawGizmosSelected()
    {
        DrawGizmos();
    }
    private void OnDrawGizmos()
    {
        if (ShowGizmosAtAllTimes)
        {
            DrawGizmos();
        }
    }

    private void CleanUp()
    {
        BackgroundManager.OverlayFader.OnFinished.RemoveListener(HandleFadeEvents);
        _isDoingTransition = false; ;
    }

    private void Update()
    {
        if(_DoNotAllowPlayerToGoBackDown)
        {
            if(_playersLastY > _player.transform.position.y)
            {
                _player.GravityScale = 0;
                _DoNotAllowPlayerToGoBackDown = false;
            }
            else
            {
                _playersLastY = _player.transform.position.y;
            }
        }
    }

    private void DrawGizmos()
    {
        if (!_theCollider)
        {
            _theCollider = GetComponent<Collider2D>();
        }
        if (transitionDirection == TransitionDirection.HORIZONTAL)
        {
            Vector3 LeftCenter = new Vector3(_theCollider.bounds.center.x - _theCollider.bounds.extents.x / 2, _theCollider.bounds.center.y, 0);
            Vector3 RightCenter = new Vector3(_theCollider.bounds.center.x + _theCollider.bounds.extents.x / 2, _theCollider.bounds.center.y, 0);
            Vector3 size = new Vector3(_theCollider.bounds.extents.x, _theCollider.bounds.size.y);
            Gizmos.color = LeftOrDownDebugColor;
            Gizmos.DrawCube(RightCenter, size);
            Gizmos.color = RightOrUpDebugColor;
            Gizmos.DrawCube(LeftCenter, size);
        }
        if (transitionDirection == TransitionDirection.VERTICAL)
        {
            Vector3 UpperCenter = new Vector3(_theCollider.bounds.center.x, _theCollider.bounds.center.y - _theCollider.bounds.extents.y / 2, 0);
            Vector3 LowerCenter = new Vector3(_theCollider.bounds.center.x, _theCollider.bounds.center.y + _theCollider.bounds.extents.y / 2, 0);
            Vector3 size = new Vector3(_theCollider.bounds.size.x, _theCollider.bounds.extents.y);
            Gizmos.color = LeftOrDownDebugColor;
            Gizmos.DrawCube(LowerCenter, size);
            Gizmos.color = RightOrUpDebugColor;
            Gizmos.DrawCube(UpperCenter, size);
        }

        if (LeftOrDownPoint.ExitPoint != null)
        {
            VisualDebug.DrawGizmoPointWithCircle(LeftOrDownPoint.ExitPoint.position, Color.red, LeftOrDownDebugColor);
            VisualDebug.DrawGizmoPointWithCircle(LeftOrDownPoint.ExitPoint.position + LeftOrDownPoint.WalkOffAmount, Color.black, LeftOrDownDebugColor);
        }
        if (RightOrUpPoint.ExitPoint != null)
        {
            VisualDebug.DrawGizmoPointWithCircle(RightOrUpPoint.ExitPoint.position, Color.red, RightOrUpDebugColor);
            VisualDebug.DrawGizmoPointWithCircle(RightOrUpPoint.ExitPoint.position + RightOrUpPoint.WalkOffAmount, Color.green, RightOrUpDebugColor);
        }
    }

    IEnumerator<float> DoUp()
    {
        /*
          For an upward transition, we want to "pop" the player up in an extra high jump arc so that they land on the platform beside the hole they just came up in.
          So we need to control how the player enters the scene to mimic a jump arc.

          To do this, the player will be moved up and forward based on the provided MoveInDistance, which should be set up so that the X distance is about the required amount to get onto the platform,
          but the Y distance is higher than the platform, so gravity can bring them back to the ground and complete the "jump arc".

          The player's jump velocity will be used as a base for the amount that the player can be moved towards the peak of the jump each frame.
          */

        _player.Input.BlockInput = true;
        _player.GravityScale = 0;
        GlobalData.PlayerTransfrom.position = _actualPoint;
        bool peakHeightAchieved = false;
        float maxStepTime = _player.Movement.JumpVelocity * 1.25f * Time.deltaTime;
        _player.Movement.Restart();
        Vector3 peakHeightPosition = new Vector3(RightOrUpPoint.ExitPoint.position.x + RightOrUpPoint.WalkOffAmount.x, RightOrUpPoint.ExitPoint.position.y + RightOrUpPoint.WalkOffAmount.y, _player.transform.position.z);
        float totalDistanceToPeakHeight = Vector3.Distance(_player.transform.position, peakHeightPosition);
        VisualDebug.DrawDebugPoint(peakHeightPosition, Color.green);
        while (!peakHeightAchieved)
        {
            Vector3 newPosition = Vector3.MoveTowards(_player.transform.position, peakHeightPosition, maxStepTime);
            _player.transform.position = newPosition;

            if (Vector3.Distance(_player.transform.position, peakHeightPosition) <= 0.05f)
            {
                peakHeightAchieved = true;
                _player.GravityScale = 1;

                _player.SetVelocityOnOneAxis('X', _player.MoveSpeed * _player.Controller.Collisions.FaceDir);
                _player.SetVelocityOnOneAxis('Y', _player.Movement.Gravity.y * Time.deltaTime);
            }


            yield return 0f;
        }
        while (!_player.Controller.Collisions.Below.IsColliding)// wait till we hit the ground
        {
            yield return 0f;
        }
        _player.Movement.Restart();
        CleanUp();
        _isDoingTransition = false;
        _player.Input.BlockInput = false;
    }
}
[System.Serializable]
public struct TransitionPoint
{
    public Transform ExitPoint;
    public Vector3 WalkOffAmount;
}