﻿using UnityEngine;
using System.Collections;
using Characters.Player;
using UnityEngine.Events;

public class WalkPlayerToPoint : MonoBehaviour
{
    public Vector3 EndPoint;
    private PlayerMaster _player;
    public float WalkTime;
    public enum States {IDLE,WALKING}
    public States State = States.IDLE;
    public UnityEvent OnCompleet = new UnityEvent();

    // Use this for initialization
    void Start()
    {
        _player = GlobalData.Player;
    }

    public void StartWalk( Vector3 end)
    {
        EndPoint = end;
        VisualDebug.DrawDebugPoint(EndPoint);
        _player.Animation.StartAnimation(PlayerAnimationController.Animations.Walk, true);
        State = States.WALKING;
        _player.Input.BlockInput = true;
    }
    // Update is called once per frame
    void Update()
    {
        if(State == States.WALKING)
        {
            Vector3 newPos =  Vector3.MoveTowards(_player.transform.position, EndPoint, _player.Movement.BaseMoveSpeed * Time.deltaTime);
            //_player.Movement.SetVelocity('x', _player.Movement.BaseMoveSpeed);
            if(StaticTools.ThresholdApproximately(newPos, EndPoint, 0.01f))//at the point 
            {
                _player.Animation.StartAnimation(PlayerAnimationController.Animations.Idle, false);
                OnCompleet?.Invoke();
                State = States.IDLE;
                _player.Input.BlockInput = false;
            }
            _player.transform.position = newPos;

        }
    }
}
