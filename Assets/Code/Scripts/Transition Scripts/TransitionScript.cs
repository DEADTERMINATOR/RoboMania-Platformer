﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameMaster
{
    public abstract class TransitionScript : MonoBehaviour
    {
        /// <summary>
        /// Performs any desired functionality at the start of the fade out process (everything will still be visible).
        /// </summary>
        public virtual void OnFadeOutStart()
        {

        }

        /// <summary>
        /// Performs any desired functionality at the conclusion of the fade out process (nothing will be visible).
        /// </summary>
        public virtual void OnFadeOutComplete()
        {

        }

        /// <summary>
        /// Performs any desried functionality at the conclusion of the fade in process (everything will be visible on the other side of the transition).
        /// </summary>
        public virtual void OnFadeInComplete()
        {

        }
    }
}
