﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbingTransition : BackgroundTransition
{
    public ClimbableObject ClimbableObject;

    protected override void Start()
    {
        base.Start();
        Direction = TransitionDirection.SPECIAL;
    }

    protected override void PerformSpecialTransition()
    {
        base.PerformSpecialTransition();

        GlobalData.Player.transform.position = StartingPoint.transform.position;
        ClimbableObject.SetOnClimbableObject(false);
    }
}
