﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequireInterfaceAttribute : PropertyAttribute
{
    public System.Type[] InterfaceTypes { get; private set; }

    public RequireInterfaceAttribute(params System.Type[] interfaceType)
    {
        InterfaceTypes = interfaceType;
    }
}
