﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckEnergyCellCount : MonoBehaviour
{
    public int DesiredNumberEnergyCell = 1;

    private bool _energyCellsApplied = false;

    private void OnTriggerEnter2D()
    {
        if (!_energyCellsApplied && GlobalData.Player.Data.TotalEnergyCells < DesiredNumberEnergyCell)
        {
            GlobalData.Player.Data.SetEnergyCells(DesiredNumberEnergyCell);
            _energyCellsApplied = true;
        }
    }
}
