﻿using UnityEngine;
using System.Collections;

public class FrameStepper : MonoBehaviour
{
    public bool IsOn { get; private set; }
    bool IsStepping;

    public void Toggle()
    {
        IsOn = !IsOn;
    }

    public void Step()
    {
        IsStepping = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Toggle();
        }
        else if (Input.GetKeyDown(KeyCode.G))
        {
            Step();
        }

        if (IsStepping)
        {
            Time.timeScale = 1;
            IsStepping = false;
        }
        else
        {
            Time.timeScale = IsOn ? 0 : 1;
        }
    }
}