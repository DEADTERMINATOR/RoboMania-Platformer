using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class ShowOrHideLineRendererInPlay : MonoBehaviour
{
    private LineRenderer _line;

    // Start is called before the first frame update
    void Start()
    {
        _line = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_line == null)
        {
            _line = GetComponent<LineRenderer>();
        }

        if (_line != null)
        {
            if (Application.isPlaying)
            {
                _line.enabled = false;
            }
            else
            {
                _line.enabled = true;

                for (int i = 0; i < _line.positionCount; ++i)
                {
                    var currentPos = _line.GetPosition(i);
                    _line.SetPosition(i, new Vector3(currentPos.x, currentPos.y, 0));
                }
            }
        }
    }
}
