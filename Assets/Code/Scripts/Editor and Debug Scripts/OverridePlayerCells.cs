﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverridePlayerCells : MonoBehaviour
{
    public int OverrideNumOfCells;


    private bool _overrideApplied = false;


    private void Update()
    {
        if (!_overrideApplied)
        {
            for (int i = 1; i < OverrideNumOfCells; ++i)
                GlobalData.Player.Data.AddEnergyCell();
            Destroy(this);
        }
    }
}
