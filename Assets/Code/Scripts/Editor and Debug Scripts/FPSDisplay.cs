﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Modified from a script created by Dave Hampson.
/// </summary>
public class FPSDisplay : MonoBehaviour
{
    private float _deltaTime = 0.0f;

    private void Update()
    {
        _deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;
    }

    public override string ToString()
    {
        float msec = _deltaTime * 1000.0f;
        float fps = 1.0f / _deltaTime;
        return string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
    }
}
