﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum FillTriggerIcons { NoIcon, CameraZone, skull, list, disk, unload, PlayerStart }
public class FillInTrigger : MonoBehaviour
{
    /// <summary>
    /// The color we want the trigger to be filled in with.
    /// </summary>
    public Color color;

    /// <summary>
    /// The trigger being filled in.
    /// </summary>
    public Collider2D Trigger;

    

    public FillTriggerIcons icon = FillTriggerIcons.NoIcon;

    private void OnDrawGizmos()
    {
        if (Trigger != null)
        {
            Gizmos.color = color;
            //Gizmos.DrawCube(transform.position, Trigger.bounds.size);
            Gizmos.DrawCube((transform.position + (Vector3)Trigger.offset), Trigger.bounds.size);
            if (icon != FillTriggerIcons.NoIcon)
            {
                Gizmos.DrawIcon(transform.position, icon + ".png", false);
            }
        }
    }
}
