﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawSphereAtTransformPosition : MonoBehaviour
{
    public float Radius = 0.1f;
    public bool DrawOnSelectedOnly = true;

    private void OnDrawGizmos()
    {
        if (!DrawOnSelectedOnly)
        {
            DrawSphere();
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (DrawOnSelectedOnly)
        {
            DrawSphere();
        }
    }

    private void DrawSphere()
    {
        Gizmos.DrawSphere(transform.position, Radius);
    }
}
