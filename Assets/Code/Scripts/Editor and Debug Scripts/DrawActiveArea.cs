﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Draw an enemy's active area (the area they can navigate within when they are alerted to the player).
/// </summary>
public class DrawActiveArea : MonoBehaviour
{
    /// <summary>
    /// Reference to the trigger collider used for awareness.
    /// </summary>
    public BoxCollider2D _collider;
    public Color Color = Color.red;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(new Vector3(transform.position.x + _collider.offset.x, transform.position.y + _collider.offset.y, transform.position.z), _collider.size);
    }
}
