﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorNote : MonoBehaviour {

    public string note;
    public string WrittenBy;
    public string ResponseNote;
         
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 2f);
        Gizmos.DrawIcon(transform.position, "NoteIcon.png", true);
    }
}
