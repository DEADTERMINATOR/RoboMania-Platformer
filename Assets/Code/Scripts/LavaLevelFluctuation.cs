﻿using UnityEngine;
using System.Collections;

public class LavaLevelFluctuation : MonoBehaviour , IActivatable
{
    public LiquidLevelManager liquidLevel;
    public float FluctuationTime = 3;
    public float WaitTime = 5;
    public bool StartActive = false;
    private bool _moving = false;

    bool _active = false;
    bool IActivatable.Active { get;  }
    float _waitTime = 0;
    void IActivatable.Activate(GameObject activator)
    {
        _active = true;
    }

    void IActivatable.Deactivate(GameObject activator)
    {
        _active = false;
    }

    // Use this for initialization
    void Start()
    {
        _active = StartActive;
        liquidLevel.OnComplete.AddListener(DoneMoving);
    }


    void DoneMoving()
    {
        _moving = false;
        _waitTime = 0;
    }
    // Update is called once per frame
    void Update()
    {
        if(_active)
        {
            if(!_moving)
            {
                _waitTime += Time.deltaTime;
                if(_waitTime >= WaitTime)
                {
                    if(liquidLevel.LevelIndex == 0)
                    {
                        _moving =  liquidLevel.StartChangeLevel(1, FluctuationTime);
                    }
                    else
                    {
                        _moving =  liquidLevel.StartChangeLevel(0, FluctuationTime);
                    }
                }
            }
        }
    }
}
