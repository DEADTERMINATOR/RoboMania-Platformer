﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weapons.Player.Forms;

namespace Weapons.Player.Upgrades
{
    public class PistolTestWeaponUpgrade : WeaponUpgrade
    {
        public override void ApplyUpgradeToWeaponForm(WeaponForm form)
        {
            form.HoldToFireAllowed = true;
        }

        public override void RemoveUpgradeFromWeaponForm(WeaponForm form)
        {
            form.HoldToFireAllowed = false;
        }
    }
}
