﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weapons.Player.Forms;
using Weapons.Player.Upgrades;
using static Weapons.Player.WeaponFormProperties;

namespace Weapons.Player.Upgrades
{
    [CreateAssetMenu(fileName = "New Weapon Upgrade", menuName = "Weapon System/Create Weapon Upgrade")]
    public class WeaponUpgrade : ScriptableObject
    {
        public struct UpgradeSaveData
        {
            public string UpgradeName;
            public string UpgradeTypeName;

            public UpgradeSaveData(string upgradeName, string upgradeTypeName) => (UpgradeName, UpgradeTypeName) = (upgradeName, upgradeTypeName);
        }

        public WeaponUpgradePropertySetter ChangedProperties;
        public string UpgradeName { get { return _upgradeName; } }
        public WeaponForms ApplicableWeaponForm { get { return _applicableWeaponForm; } }
        public string UpgradeTypeName { get { return _weaponUpgradeTypeName; } }


        [SerializeField]
        private string _upgradeName;
        [SerializeField]
        protected WeaponForms _applicableWeaponForm;
        [SerializeField]
        private string _weaponUpgradeTypeName;
        [SerializeField]
        private int _weaponUpgradeTypeNamePopupSelection = 0;


        public virtual void ApplyUpgradeToWeaponForm(WeaponForm form) { }

        public virtual void RemoveUpgradeFromWeaponForm(WeaponForm form) { }

        public virtual void CopyOtherWeaponUpgradeScriptableObjectIntoThisOne(WeaponUpgrade otherUpgrade)
        {
            _upgradeName = otherUpgrade.UpgradeName;
            _applicableWeaponForm = otherUpgrade.ApplicableWeaponForm;
            ChangedProperties = otherUpgrade.ChangedProperties;
            _weaponUpgradeTypeName = otherUpgrade.UpgradeTypeName;
        }
    }
}
