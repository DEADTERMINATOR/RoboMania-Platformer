﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static Weapons.Player.WeaponFormProperties;

namespace Weapons.Player.Upgrades
{
    [CreateAssetMenu(fileName = "New Weapon Upgrade Property Setter", menuName = "Weapon System/Create Weapon Upgrade Property Setter")]
    public class WeaponUpgradePropertySetter : ScriptableObject
    {
        /// <summary>
        /// The weapon form this upgrade will be applicable to.
        /// </summary>
        public WeaponForms ApplicableWeaponForm { get { return _applicableWeaponForm; } }

        /// <summary>
        /// The list of properties that this upgrade will modify in some way.
        /// </summary>
        public List<WeaponProperties> ChangedPropertiesList { get; private set; } = new List<WeaponProperties>();

        /// <summary>
        /// The list of the actual value changes to the above properties, held in the same order as the properties themselves.
        /// </summary>
        public List<float> ValueChangesList { get { return _valueChangesList; } }


        [SerializeField]
        private WeaponForms _applicableWeaponForm;

        /* The list of changed properties held by their string names. Used by the custom editor only */
        [SerializeField]
        private List<string> _changedPropertyNamesList = new List<string>();

        /* The list of changed properties held as their associated enum values. Facilitates the persistance and modification of the properties in the editor. */
        [SerializeField]
        private List<int> _changedPropertiesEditorEnumValues = new List<int>();

        [SerializeField]
        private List<float> _valueChangesList = new List<float>();

        /* Whether the custom editor should add a new line for another property to be changed (one that has not been saved to the list of properties yet). */
        [SerializeField]
        private bool _hasNewPropertyField = true;


        /// <summary>
        /// Initializes the upgrade by converting all the changed properties from the string names they were stored as in the custom editor to the WeaponProperties they
        /// are used as in the rest of the code base.
        /// </summary>
        public void InitPropertySetter()
        {
            ChangedPropertiesList.Clear();
            foreach (string propertyName in _changedPropertyNamesList)
            {
                ChangedPropertiesList.Add(GetWeaponPropertyFromName(propertyName));
            }
        }
    }
}
