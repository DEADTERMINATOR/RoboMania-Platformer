﻿using Characters.Player;
using System.Collections.Generic;
using UnityEngine;
using Weapons.Player.Forms;
using Weapons.Player.Upgrades;
using static Weapons.Player.WeaponFormProperties;
using System;

namespace Weapons.Player
{
    public class PlayerWeapon : MonoBehaviour
    {
        public delegate void WeaponEvent(WeaponForm newForm);
        public event WeaponEvent WeaponSwitch;

        public delegate void UpdateEvent(float updateTime);
        public event UpdateEvent UpdateCalled;


        /* The minimum amount of time that must elapse after the spawning of a muzzle flash effect before another may be spawned. */
        private const float MINIMUM_TIME_BETWEEN_MUZZLE_FLASHES = 0.1f;

        
        private enum ChargeRifleState { None, CanCharge, Charging, Charged, ShouldFire }


        #region Public Properties + Fields
        /// <summary>
        /// The weapon forms the player currently possesses.
        /// </summary>
        [HideInInspector]
        public WeaponForm[] PossessedForms;
        
        /// <summary>
        /// The weapon form the player is currently using.
        /// </summary>
        [HideInInspector]
        public WeaponForm ActiveForm;

        /// <summary>
        /// Whether the player is currently allowed to fire.
        /// </summary>
        public bool CanFire { get { return ActiveForm != null && _currentCooldownTime <= 0; } }

        /// <summary>
        /// The pool that manages projectiles for this gun chip.
        /// </summary>
        [HideInInspector]
        public PrefabPool Pool { get; protected set; }

        /// <summary>
        /// Public setter for the player ref.
        /// </summary>
        public PlayerMaster PlayerRef { set { _playerRef = value; } }
        #endregion

        #region Private Properties + Fields
        /* Whether the weapon should use debug settings. */
        [SerializeField]
        private bool _useDebug;
        [SerializeField]
        private List<WeaponForm> _debugPossessedForms;

        /* The complete list of all the weapon form scriptable objects. Used as a reference to set the base values of the weapon forms the player possesses. */
        [SerializeField]
        private List<WeaponForm> _weaponFormScriptableObjects;

        /* The complete list of all the weapon upgrade scriptable objects for all weapon form types. Used as a reference to set the properties of the upgrades that the player possesses. */
        [SerializeField]
        private List<WeaponUpgrade> _weaponUpgradeScriptableObject;

        private PlayerMaster _playerRef;

        /* The index of the currently equipped weapon in the list of weapon forms the player possesses. */
        private int _possessedFormsListIndex = 0;

        /* The amount of time elapsed since the player last fired their weapon. */
        private float _currentCooldownTime = 0;

        /* The amount of time elapsed since a muzzle flash effect was last spawned. */
        private float _timeSinceLastMuzzleFlash = 0;

        /* The current firing state of the charge rifle. Only relevant if the player currently has the charge rifle equipped. */
        private ChargeRifleState _chargeRifleState = ChargeRifleState.None;
        #endregion

        #region Unity Methods
        private void Start()
        {
            if (!_useDebug)
            {
                LoadWeaponData(GlobalData.CURRENT_SAVE_FILE_NAME);
            }
            else
            {
                CreateDebugWeapons();
            }

            EnableWeapons();

            PlayerInput input = GlobalData.Player.Input;
            input.Pistol += new PlayerInput.OnWeaponSwitch(SwitchToForm);
            input.MachineGun += new PlayerInput.OnWeaponSwitch(SwitchToForm);
            input.Shotgun += new PlayerInput.OnWeaponSwitch(SwitchToForm);
            input.RocketLauncher += new PlayerInput.OnWeaponSwitch(SwitchToForm);
            input.ChargeRifle += new PlayerInput.OnWeaponSwitch(SwitchToForm);

            input.FireInputPressed += new PlayerInput.InputStatus(OnFireInputPressed);
            input.FireInputReleased += new PlayerInput.InputStatus(OnFireInputReleased);
        }

        private void Update()
        {
            _currentCooldownTime -= Time.deltaTime;
            _timeSinceLastMuzzleFlash += Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.B) && ActiveForm.FirstSlottedGunChip != null)
            {
                ActiveForm.DropGunChip(1);
            }

            UpdateCalled?.Invoke(Time.deltaTime);
        }
        #endregion

        /// <summary>
        /// Fires the currently equipped weapon, including determing whether spawning a muzzle flash effect is allowed. Also manages the charge state of the charge rifle
        /// if that is the weapon the player currently has equipped and is trying to fire.
        /// </summary>
        /// <param name="firePosition">The position the projectile should spawn at.</param>
        /// <param name="fireRotation">The rotation the projectile should spawn at.</param>
        public void FireWeapon(Vector3 firePosition, Vector3 fireRotation)
        {
            if (_possessedFormsListIndex == (int)WeaponForms.ChargeRifle)
            {
                var chargeRifle = ActiveForm as ChargeRifleWeaponForm;

                if (_chargeRifleState == ChargeRifleState.ShouldFire)
                {
                    chargeRifle.Fire(firePosition, fireRotation, true);
                }
                else if (_chargeRifleState != ChargeRifleState.None)
                {
                    var chargeRifleIsCharged = chargeRifle.ChargeWeapon(firePosition, fireRotation);
                    if (_chargeRifleState != ChargeRifleState.None)
                    {
                        //We need to check again because it's possible that the charge expired, which triggers a callback which changes the charge rifle state back to none, which this would immediately override.
                        _chargeRifleState = chargeRifleIsCharged == true ? ChargeRifleState.Charged : ChargeRifleState.Charging;
                    }
                }
            }
            else
            {
                bool shouldInstantiateMuzzleFlash = _timeSinceLastMuzzleFlash >= MINIMUM_TIME_BETWEEN_MUZZLE_FLASHES;
                ActiveForm.Fire(firePosition, fireRotation, shouldInstantiateMuzzleFlash);
                if (shouldInstantiateMuzzleFlash)
                {
                    _timeSinceLastMuzzleFlash = 0;
                }
            }
        }

        #region Switch Weapon Form
        /// <summary>
        /// Switches the player's currently active weapon to the weapon form that is the provided number of cycles away (including looping back around to the start of the list if necessary).
        /// </summary>
        /// <param name="cycleCount">The number of weapons that should be cycled through.</param>
        /// <returns>The player's new weapon form.</returns>
        public WeaponForms CycleForms(bool cycleBackwards)
        {
            if (PossessedForms.Length > 1)
            {
                int newFormListIndex = cycleBackwards ? _possessedFormsListIndex - 1 : _possessedFormsListIndex + 1;

                //Check if the cycle count forces us to wrap around.
                if (newFormListIndex < 0)
                {
                    newFormListIndex = PossessedForms.Length - 1;
                }
                else
                {
                    newFormListIndex %= PossessedForms.Length;
                }

                SwitchToForm((WeaponForms)newFormListIndex);
                return (WeaponForms)newFormListIndex;
            }

            return (WeaponForms)_possessedFormsListIndex;
        }

        /// <summary>
        /// Switches the player's currently active weapon form to the specified weapon form (if the player possesses it).
        /// </summary>
        /// <param name="formToSwitchTo">The weapon form to switch to.</param>
        private void SwitchToForm(WeaponForms formToSwitchTo)
        {
            if (PossessedForms[(int)formToSwitchTo] != null && formToSwitchTo != ActiveForm.WeaponFormType)
            {
                if (_possessedFormsListIndex == (int)WeaponForms.ChargeRifle)
                {
                    var chargeRifle = ActiveForm as ChargeRifleWeaponForm;
                    chargeRifle.ResetCalled -= OnChargeRifleChargeReset;
                }

                ActiveForm.Fired -= new WeaponForm.WeaponFire(OnWeaponFire); //Remove the weapon fire callback from the previous active form.

                ActiveForm = PossessedForms[(int)formToSwitchTo];
                WeaponSwitch?.Invoke(ActiveForm);

                _possessedFormsListIndex = (int)formToSwitchTo;

                ActiveForm.Fired += new WeaponForm.WeaponFire(OnWeaponFire); //Add a weapon fire callback for the new active form.

                if (_possessedFormsListIndex == (int)WeaponForms.ChargeRifle)
                {
                    var chargeRifle = ActiveForm as ChargeRifleWeaponForm;
                    chargeRifle.ResetCalled += OnChargeRifleChargeReset;
                }

                Pool.PoolPrefab = ActiveForm.ProjectileAsset;
                Pool.PreparePoolForPrefabSwitch();
            }
        }
        #endregion

        /// <summary>
        /// Gets the associated weapon upgrade scriptable object from the complete list based on the string name of the upgrade.
        /// </summary>
        /// <param name="upgradeName">The name of the upgrade.</param>
        /// <returns></returns>
        public WeaponUpgrade GetWeaponUpgradeFromUpgradeName(string upgradeName)
        {
            return _weaponUpgradeScriptableObject.Find(x => x.UpgradeName == upgradeName);
        }

        public void AddForm(WeaponForms formToSwitchTo)
        {
            if (PossessedForms.Length >= (int)formToSwitchTo)
            {
                ActiveForm = PossessedForms[(int)formToSwitchTo];
            }

            if (ActiveForm != null)
            {
                Pool = gameObject.AddComponent<PrefabPool>();
                Pool.PoolPrefab = ActiveForm.ProjectileAsset;
                ActiveForm.Fired += new WeaponForm.WeaponFire(OnWeaponFire);
            }
        }

        /// <summary>
        /// Creates instances of all the weapons the player should possessed when using debug settings.
        /// </summary>
        private void CreateDebugWeapons()
        {
            PossessedForms = new WeaponForm[_debugPossessedForms.Count];
            for (int i = 0; i < _debugPossessedForms.Count; ++i)
            {
                var form = _debugPossessedForms[i];
                int index = -1;
                WeaponForm formInstance = null;

                if (form is PistolWeaponForm)
                {
                    formInstance = (WeaponForm)ScriptableObject.CreateInstance("PistolWeaponForm");
                    index = (int)WeaponForms.Pistol;
                }
                else if (form is MachineGunWeaponForm)
                {
                    formInstance = (WeaponForm)ScriptableObject.CreateInstance("MachineGunWeaponForm");
                    index = (int)WeaponForms.MachineGun;
                }
                else if (form is ShotgunWeaponForm)
                {
                    formInstance = (WeaponForm)ScriptableObject.CreateInstance("ShotgunWeaponForm");
                    index = (int)WeaponForms.Shotgun;
                }
                else if (form is RocketLauncherWeaponForm)
                {
                    formInstance = (WeaponForm)ScriptableObject.CreateInstance("RocketLauncherWeaponForm");
                    index = (int)WeaponForms.RocketLauncher;
                }
                else if (form is ChargeRifleWeaponForm)
                {
                    formInstance = (WeaponForm)ScriptableObject.CreateInstance("ChargeRifleWeaponForm");
                    index = (int)WeaponForms.ChargeRifle;
                }

                if (formInstance != null)
                {
                    formInstance.InitWeaponForm(this, form, _playerRef, "", true);
                    PossessedForms[index] = formInstance;
                }
            }
        }

        #region Enable + Disable
        /// <summary>
        /// Enables the firing of weapons.
        /// </summary>
        public void EnableWeapons()
        {            
            if (ActiveForm == null && PossessedForms.Length != 0)
            {
                //If you change the starting active form index to something other than 0 for debug purposes, you must change _currentPlayerWeaponForm in PlayerInput.cs to the same value.
                var startingWeaponIndex = 1;

                ActiveForm = PossessedForms[startingWeaponIndex];
                _possessedFormsListIndex = startingWeaponIndex;
            }

            if (ActiveForm != null)
            {
                Pool = gameObject.AddComponent<PrefabPool>();
                Pool.PoolPrefab = ActiveForm.ProjectileAsset;
                ActiveForm.Fired += new WeaponForm.WeaponFire(OnWeaponFire);
            }
        }

        
        /// <summary>
        /// Disables the firing of weapons.
        /// </summary>
        public void DisableWeapon()
        {
            Destroy(Pool);
            ActiveForm = null;
        }
        #endregion

        #region Save + Load
        /// <summary>
        /// Saves the weapon data for the weapons the player currently has possessed (will save weapons and upgrades provided to the player through debug settings).
        /// </summary>
        /// <param name="saveFile">The file to save to.</param>
        public void SaveWeaponData(string saveFile)
        {
            foreach (WeaponForm form in PossessedForms)
            {
                form.SaveWeaponFormToFile(saveFile);
            }
        }

        /// <summary>
        /// Loads the weapon data for all weapons the player possessed based on the provided save file.
        /// </summary>
        /// <param name="saveFile">The file to load from.</param>
        private void LoadWeaponData(string saveFile)
        {
            PossessedForms = new WeaponForm[_weaponFormScriptableObjects.Count];
            int weaponIndex = -1;

            if (ES3.Load<bool>("Has Pistol", saveFile, false))
            {
                var pistol = (PistolWeaponForm)ScriptableObject.CreateInstance("PistolWeaponForm");
                weaponIndex = _weaponFormScriptableObjects.FindIndex(x => x is PistolWeaponForm);
                if (weaponIndex != -1)
                {
                    pistol.InitWeaponForm(this, _weaponFormScriptableObjects[weaponIndex], _playerRef, saveFile);
                    PossessedForms[(int)WeaponForms.Pistol] = pistol;
                }
            }
            if (ES3.Load<bool>("Has Machine Gun", saveFile, false))
            {
                var machineGun = (MachineGunWeaponForm)ScriptableObject.CreateInstance("MachineGunWeaponForm");
                weaponIndex = _weaponFormScriptableObjects.FindIndex(x => x is MachineGunWeaponForm);
                if (weaponIndex != -1)
                {
                    machineGun.InitWeaponForm(this, _weaponFormScriptableObjects[weaponIndex], _playerRef, saveFile);
                    PossessedForms[(int)WeaponForms.MachineGun] = machineGun;
                }
            }
            if (ES3.Load<bool>("Has Shotgun", saveFile, false))
            {
                var shotgun = (ShotgunWeaponForm)ScriptableObject.CreateInstance("ShotgunWeaponForm");
                weaponIndex = _weaponFormScriptableObjects.FindIndex(x => x is ShotgunWeaponForm);
                if (weaponIndex != -1)
                {
                    shotgun.InitWeaponForm(this, _weaponFormScriptableObjects[weaponIndex], _playerRef, saveFile);
                    PossessedForms[(int)WeaponForms.Shotgun] = shotgun;
                }
            }
            if (ES3.Load<bool>("Has Rocket Launcher", saveFile, false))
            {
                var rocketLauncher = (RocketLauncherWeaponForm)ScriptableObject.CreateInstance("RocketLauncherWeaponForm");
                weaponIndex = _weaponFormScriptableObjects.FindIndex(x => x is RocketLauncherWeaponForm);
                if (weaponIndex != -1)
                {
                    rocketLauncher.InitWeaponForm(this, _weaponFormScriptableObjects[weaponIndex], _playerRef, saveFile);
                    PossessedForms[(int)WeaponForms.RocketLauncher] = rocketLauncher;
                }
            }
            if (ES3.Load<bool>("Has Charge Rifle", saveFile, false))
            {
                var chargeRifle = (ChargeRifleWeaponForm)ScriptableObject.CreateInstance("ChargeRifleWeaponForm");
                weaponIndex = _weaponFormScriptableObjects.FindIndex(x => x is ChargeRifleWeaponForm);
                if (weaponIndex != -1)
                {
                    chargeRifle.InitWeaponForm(this, _weaponFormScriptableObjects[weaponIndex], _playerRef, saveFile);
                    PossessedForms[(int)WeaponForms.ChargeRifle] = chargeRifle;
                }
            }
        }
        #endregion

        #region Callback Methods
        /// <summary>
        /// Callback method for when a weapon form is successfully fired.
        /// </summary>
        private void OnWeaponFire()
        {
            _currentCooldownTime = ActiveForm.CooldownTime;
            if (_possessedFormsListIndex == (int)WeaponForms.ChargeRifle)
            {
                _chargeRifleState = ChargeRifleState.None;
            }
        }

        /// <summary>
        /// Callback method for when the fire input is first pressed.
        /// </summary>
        private void OnFireInputPressed()
        {
            if (_possessedFormsListIndex == (int)WeaponForms.ChargeRifle)
            {
                Debug.Log("Pressed");
                _chargeRifleState = ChargeRifleState.CanCharge;
            }
        }

        /// <summary>
        /// Callback method for when the fire input is first released.
        /// </summary>
        private void OnFireInputReleased()
        {
            if (_possessedFormsListIndex == (int)WeaponForms.ChargeRifle)
            {
                var chargeRifle = ActiveForm as ChargeRifleWeaponForm;

                if (_chargeRifleState == ChargeRifleState.Charged)
                {
                    _chargeRifleState = ChargeRifleState.ShouldFire;
                }
                else
                {
                    chargeRifle.ResetCharge();
                    _chargeRifleState = ChargeRifleState.None;
                }
            }   
        }

        /// <summary>
        /// Callback method for when Reset is called on the charge rifle.
        /// </summary>
        private void OnChargeRifleChargeReset()
        {
            _chargeRifleState = ChargeRifleState.None;
        }
        #endregion
    }
}
