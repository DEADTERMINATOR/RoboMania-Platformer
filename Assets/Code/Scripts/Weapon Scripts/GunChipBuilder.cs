﻿using UnityEngine;
using System.Collections;
/// <summary>
/// A set of tool to build GunChip from stored data
/// </summary>
public class GunChipBuilder : MonoBehaviour
{

    /// <summary>
    /// Remake a gun chip base using a stored type name 
    /// </summary>
    /// <param name="typeName"></param>
    /// <returns>The gameobject of suscess and null on fail</returns>
    public static  GameObject BasicChipFactory(string typeName)
    {
        if (typeName == "StraightShotGunChip")
        {
            return Instantiate(Resources.Load("Prefabs/Weapons/Chips/Straight Shot") as GameObject);
        }
        else if (typeName == "SpreadShotGunChip")
        {
            return Instantiate(Resources.Load("Prefabs/Weapons/Chips/Spread Shot") as GameObject);
        }
        else if(typeName == "RandomShotGunChip")
        {
            return Instantiate(Resources.Load("Prefabs/Weapons/Chips/Random Shot") as GameObject);
        }
        return null;
    }


    /// <summary>
    /// Loads and assigns the correct muzzle flash effect (based on the chip's projectile type and rarity) to the gun chip.
    /// </summary>
    /// <param name="gunChip">The gun chip game object.</param>
    /// <param name="projectileType">The string representing the type of projectile chosen.</param>
    /// <param name="rarity">The rarity of the gun chip.</param>
    public static void AssignAssociatedMuzzleFlash(ref GameObject gunChip, ProjectileTypes projectileType, int rarity)
    {
        string loadString = "";
        switch (projectileType)
        {
            case ProjectileTypes.Bullet:
                loadString += "Prefabs/Effects/Muzzle Flashes/Player Muzzle Flashes/Bullet Muzzle Flashes/";
                switch (rarity)
                {
                    case 0:
                        loadString += "Common Bullet Muzzle Flash";
                        break;
                    case 1:
                        loadString += "Uncommon Bullet Muzzle Flash";
                        break;
                    case 2:
                        loadString += "Rare Bullet Muzzle Flash";
                        break;
                    case 3:
                        loadString += "Epic Bullet Muzzle Flash";
                        break;
                }
                break;
            case ProjectileTypes.Energy:
                loadString += "Prefabs/Effects/Muzzle Flashes/Player Muzzle Flashes/Energy Muzzle Flashes/";
                switch (rarity)
                {
                    case 0:
                        loadString += "Common Energy Muzzle Flash";
                        break;
                    case 1:
                        loadString += "Uncommon Energy Muzzle Flash";
                        break;
                    case 2:
                        loadString += "Rare Energy Muzzle Flash";
                        break;
                    case 3:
                        loadString += "Epic Energy Muzzle Flash";
                        break;
                }
                break;
            case ProjectileTypes.Electric:
                loadString += "Prefabs/Effects/Muzzle Flashes/Player Muzzle Flashes/Electric Muzzle Flashes/";
                switch (rarity)
                {
                    case 0:
                        loadString += "Common Electric Muzzle Flash";
                        break;
                    case 1:
                        loadString += "Uncommon Electric Muzzle Flash";
                        break;
                    case 2:
                        loadString += "Rare Electric Muzzle Flash";
                        break;
                    case 3:
                        loadString += "Epic Electric Muzzle Flash";
                        break;
                }
                break;
            case ProjectileTypes.Rocket:
                loadString += "Prefabs/Effects/Muzzle Flashes/Player Muzzle Flashes/Rocket Muzzle Flashes/";
                switch (rarity)
                {
                    case 0:
                        loadString += "Common Rocket Muzzle Flash";
                        break;
                    case 1:
                        loadString += "Uncommon Rocket Muzzle Flash";
                        break;
                    case 2:
                        loadString += "Rare Rocket Muzzle Flash";
                        break;
                    case 3:
                        loadString += "Epic Rocket Muzzle Flash";
                        break;
                }
                break;
        }
       // gunChip.GetComponent<OldGunChip>().MuzzleFlashEffect = Resources.Load(loadString) as GameObject;
    }


    /// <summary>
    ///
    /// </summary>
    /// <returns>A string representing the chosen type of projectile.</returns>
    public static void InstantiateAndAssingeProjectile(ref GameObject projectileObject, int rarity, ProjectileTypes type)
    {
        if (type == ProjectileTypes.Bullet) //Bullet
        {
            string loadString = "Prefabs/Weapons/Projectiles/Bullet Projectiles/";
            switch (rarity)
            {
                case 0:
                    loadString += "Common Bullet";
                    break;
                case 1:
                    loadString += "Uncommon Bullet";
                    break;
                case 2:
                    loadString += "Rare Bullet";
                    break;
                case 3:
                    loadString += "Epic Bullet";
                    break;
            }

            projectileObject = Resources.Load(loadString) as GameObject;
        }
        else if (type == ProjectileTypes.Energy) //Energy
        {
            string loadString = "Prefabs/Weapons/Projectiles/Energy Projectiles/";
            switch (rarity)
            {
                case 0:
                    loadString += "Common Energy";
                    break;
                case 1:
                    loadString += "Uncommon Energy";
                    break;
                case 2:
                    loadString += "Rare Energy";
                    break;
                case 3:
                    loadString += "Epic Energy";
                    break;
            }

            projectileObject = Resources.Load(loadString) as GameObject;
        }
        else if (type == ProjectileTypes.Energy) //Electric
        {
            string loadString = "Prefabs/Weapons/Projectiles/Electric Projectiles/";
            switch (rarity)
            {
                case 0:
                    loadString += "Common Electric";
                    break;
                case 1:
                    loadString += "Uncommon Electric";
                    break;
                case 2:
                    loadString += "Rare Electric";
                    break;
                case 3:
                    loadString += "Epic Electric";
                    break;
            }

            projectileObject = Resources.Load(loadString) as GameObject;
        }
        else //Rocket
        {
            string loadString = "Prefabs/Weapons/Projectiles/Rocket Projectiles/";
            switch (rarity)
            {
                case 0:
                    loadString += "Common Rocket";
                    break;
                case 1:
                    loadString += "Uncommon Rocket";
                    break;
                case 2:
                    loadString += "Rare Rocket";
                    break;
                case 3:
                    loadString += "Epic Rocket";
                    break;
            }

            projectileObject = Resources.Load(loadString) as GameObject;

        }
    }

    }
