﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamCollider : MonoBehaviour
{
    private Beam _beam;


    private void Start()
    {
        _beam = transform.root.GetComponentInChildren<Beam>();
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        _beam.HandleCollision(collider, true);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        _beam.HandleCollision(collision.collider, false);
    }
}
