﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : IChracterCarrable
{
    public int DamageAmount = 1;
    public float CoolDownTime =  0.1f;
    [HideInInspector]
    public Vector3 DamageDirection = Vector3.forward;
    [HideInInspector]
    public Vector3 DamageRotation = Vector3.zero;
    public abstract void Attack(GameObject owner);
    protected float _timeSinceLastAttack = 0.1f;
    public GameObject dropPrefab;

    public delegate void WeaponReloaded();
    public event WeaponReloaded OnWeaponReloaded;

    public virtual bool IsFull { get { return (true); } }

    // Use this for initialization
    void Start ()
    {
        _timeSinceLastAttack = CoolDownTime;
    }
	
	// Update is called once per frame
	void Update ()
	{
	    _timeSinceLastAttack += Time.deltaTime;
	}

    public void WeaponWasReloaded()
    {
        if(OnWeaponReloaded != null)
        {
            OnWeaponReloaded();
        }
    }


}