﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Security.Policy;
using UnityEngine;
using Weapons.Player;
using static Weapons.Player.WeaponFormProperties;

namespace Weapons.Player.Chips
{
    [CreateAssetMenu(fileName = "Gun Chip", menuName = "Weapon System/Create Gun Chip")]
    public class GunChip : ScriptableObject
    {
        public enum RarityLevels { Common = 1, Uncommon = 2, Rare = 3, Epic = 4 }


        public RarityLevels ChipRarity { get { return _chipRarity; } }
        public string ChipRarityName { get { return GetChipRarityName(); } }
        public int ChipRarityNumber { get { return (int)_chipRarity; } }
        public WeaponProperties ImprovedProperty { get { return _improvedProperty; } }
        public string ImprovedPropertyName { get { return GetPropertyName(_improvedProperty); } }
        public float PropertyImprovementAmount { get { return _propertyImprovementAmount / 100f; } }
        public WeaponProperties ImpairedProperty { get { return _impairedProperty; } }
        public string ImpairedPropertyName { get { return GetPropertyName(_impairedProperty); } }
        public float PropertyImpairmentAmount { get { return _propertyImpairmentAmount / 100f; } }


        [SerializeField]
        private RarityLevels _chipRarity = RarityLevels.Common;
        [SerializeField]
        private WeaponProperties _improvedProperty;
        [SerializeField]
        [Range(5f, 200f)]
        private float _propertyImprovementAmount;
        [SerializeField]
        private WeaponProperties _impairedProperty;
        [SerializeField]
        [Range(5f, 200f)]
        private float _propertyImpairmentAmount;


        public string ConstructSaveStringForGunChip()
        {
            return (int)ChipRarity + "/" + (int)_improvedProperty + "," + _propertyImprovementAmount + "/" + (int)_impairedProperty + "," + _propertyImpairmentAmount;
        }

        public static GunChip ConstructGunChipFromSaveString(string saveString)
        {
            GunChip newChip = ScriptableObject.CreateInstance<GunChip>();

            char[] delimiterChars = { '/', ',' };
            string[] stringParts = saveString.Split(delimiterChars);

            newChip._chipRarity = (RarityLevels)Convert.ToInt32(stringParts[0]);
            newChip._improvedProperty = (WeaponProperties)Convert.ToInt32(stringParts[1]);
            newChip._propertyImprovementAmount = Convert.ToInt32(stringParts[2]);
            newChip._impairedProperty = (WeaponProperties)Convert.ToInt32(stringParts[3]);
            newChip._propertyImpairmentAmount = Convert.ToInt32(stringParts[4]);

            return newChip;
        }

        private string GetChipRarityName()
        {
            switch (_chipRarity)
            {
                case RarityLevels.Common:
                    return "Common";
                case RarityLevels.Uncommon:
                    return "Uncommon";
                case RarityLevels.Rare:
                    return "Rare";
                case RarityLevels.Epic:
                    return "Epic";
                default:
                    return "Invalid";
            }
        }
    }
}
