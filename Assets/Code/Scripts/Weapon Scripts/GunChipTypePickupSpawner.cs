﻿using LevelSystem;
using LevelSystem.SateData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GunChipTypePickupSpawner : MonoBehaviour
{
  /*  /// <summary>
    /// Should this spawn a chip in on Start 
    /// </summary>
    public bool AutoSpawn = true;
    /// <summary>
    /// The Chance of An Uncommon % 0-100
    /// </summary>
    public int UncommonSpawnChance = 25;
    /// <summary>
    /// The Chance of An Uncommon % 0-100
    /// </summary>
    public int RareSpawnChance = 15;
    /// <summary>
    /// The Chance of An Uncommon % 0-100
    /// </summary>
    public int EpicSpawnChance = 10;
    /// <summary>
    /// Count = No Force any other Will make this only Gen the Set Rarity
    /// </summary>
    public Rarity ForeceRarity = Rarity.COUNT;

    /// <summary>
    /// The Gun Chip That can spawn 
    /// </summary>
    public List<OldGunChip> ChipsToSpawn;

    public bool Active { get { return _activatorSpawned; } }


    /// <summary>
    /// The prefab for the gun chip pickup.
    /// </summary>
    private GameObject PickUpPrefab;

    /// <summary>
    /// The generated gun chip.
    /// </summary>
    private OldGunChip _generatedChip;
    private LevelStateBool _hasBeedSpanwed;
    private bool _activatorSpawned = false;


    public void Spawn()
    {
        string id = transform.position.x.ToString() + transform.position.y.ToString();
        SceneManager.SetActiveScene(SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage.MasterScene)); //Set the active scene to the master scene so the actual gun chip persists between loads and unloads.
        _hasBeedSpanwed = LevelState.Instance.LoadLevelStateBool(id,false); 
         _generatedChip = Instantiate<OldGunChip>(ChipsToSpawn[Random.Range(0, ChipsToSpawn.Count)]);
        int generatedNum = Random.Range(0, 100);
        Rarity rarityToSpawn = Rarity.COMMON;

        if (generatedNum < UncommonSpawnChance)
        {
            rarityToSpawn = Rarity.UNCOMMON;
        }
        else if (generatedNum >= UncommonSpawnChance && generatedNum < UncommonSpawnChance + RareSpawnChance)
        {
            //rare
            rarityToSpawn = Rarity.RARE;
        }
        else if (generatedNum >= UncommonSpawnChance + RareSpawnChance && generatedNum < UncommonSpawnChance + RareSpawnChance + EpicSpawnChance)
        {
            //epic
            rarityToSpawn = Rarity.EPIC;
        }

        if (ForeceRarity != Rarity.COUNT)
        {
            rarityToSpawn = ForeceRarity;
        }

        SceneManager.SetActiveScene(gameObject.scene); //Set the active scene to the scene the gun chip spawner is a part of, so the pickup unloads when it is supposed to.
        PickUpPrefab = Resources.Load(GlobalData.PREFAB_GUNCHIP_TEMPLATE_PATH) as GameObject;
        GameObject instantiatedPickUp = Instantiate(PickUpPrefab);
        _generatedChip.Generate(rarityToSpawn);
        GunChipPickUp instantiatedPickupComponent = instantiatedPickUp.GetComponent<GunChipPickUp>();
        instantiatedPickupComponent.Gunchip = _generatedChip.GetComponent<OldGunChip>();
        instantiatedPickupComponent.ID = id;
        instantiatedPickUp.transform.position = transform.position;
        instantiatedPickupComponent.Dropped();

        Debug.Log(instantiatedPickupComponent.Gunchip.DisplayName + " " + instantiatedPickupComponent.Gunchip.DamagePerSecond);
    }

    //Use this for initialization
    private void Start()
    {
        if(ChipsToSpawn == null || ChipsToSpawn.Count == 0)
        {
            LoadDefaultChips();
        }
        if (AutoSpawn)
        {
            string id = transform.position.x.ToString() + transform.position.y.ToString();
            _hasBeedSpanwed = LevelState.Instance.LoadLevelStateBool("GunChipPickupSpawner-" + id, false);

            if (_hasBeedSpanwed)
            {
                Spawn();
            }

            //BUGFIX: Destroy 
            gameObject.SetActive(false);
            Destroy(gameObject, 1);
        }
    }


    private void LoadDefaultChips()
    {
        ChipsToSpawn = new List<OldGunChip>();
        ChipsToSpawn.Add(Resources.Load<OldGunChip>("Prefabs/Weapons/Chips/New Chips/Gun Chip"));
        ChipsToSpawn.Add(Resources.Load<OldGunChip>("Prefabs/Weapons/Chips/New Chips/Rapid Fire Chip"));
        ChipsToSpawn.Add(Resources.Load<OldGunChip>("Prefabs/Weapons/Chips/New Chips/RocketChip"));
        ChipsToSpawn.Add(Resources.Load<OldGunChip>("Prefabs/Weapons/Chips/New Chips/Spread Fire Chip"));
       
    }

    public void Activate(GameObject activator)
    {
        Spawn();
        _activatorSpawned = true;
    }

    public void Deactivate(GameObject activator)
    {
        gameObject.SetActive(false);
        Destroy(_generatedChip.gameObject);
        _activatorSpawned = false;
    }*/
}
