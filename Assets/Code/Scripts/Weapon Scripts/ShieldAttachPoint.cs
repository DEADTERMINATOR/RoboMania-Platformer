﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class ShieldAttachPoint : MonoBehaviour
{

    /// <summary>
    /// Is the Attach point on the player
    /// </summary>
    public bool IsPlayer;
    /// <summary>
    /// Is the Attach point stating facing right 
    /// </summary>
    public bool IsRightFacing;


    private float _startXOffest;
    // Use this for initialization
    public void Awake()
    {
       
    }

    void Start()
    {
      /*  _startXOffest = transform.position.x;*/
        GlobalData.Player.Shield.AttchPoint = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsPlayer && TheGame.GetRef.gameState != TheGame.GameState.RESPAWNING)
        {
            if (GlobalData.Player.State.WallSliding)
            {
                if (GlobalData.Player.Controller.Collisions.FaceDir != (transform.localScale.x*-1))
                {
                    transform.localScale = new Vector3(GlobalData.Player.Controller.Collisions.FaceDir, 1, 1);
                    transform.position = GlobalData.Player.Animation.RootBone.GetWorldPosition(GlobalData.Player.transform);
                }
            }
            else
            {
                if (GlobalData.Player.Controller.Collisions.FaceDir != transform.localScale.x)
                {
                    transform.localScale = new Vector3(GlobalData.Player.Controller.Collisions.FaceDir, 1, 1);
                    transform.position = GlobalData.Player.Animation.RootBone.GetWorldPosition(GlobalData.Player.transform);
                }
            }
        }

    }
}
