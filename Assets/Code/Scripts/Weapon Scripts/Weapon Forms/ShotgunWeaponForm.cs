﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weapons.Player.Upgrades;
using Characters.Player;
using Weapons.Player.Chips;

namespace Weapons.Player.Forms
{
    [CreateAssetMenu(fileName = "Shotgun Weapon Form", menuName = "Weapon System/Create Weapon Form/Shotgun")]
    public class ShotgunWeaponForm : WeaponForm
    {
        public float Spread { get; private set; }


        [SerializeField]
        private float _baseSpread;


        public override void InitWeaponForm(PlayerWeapon playerWeapon, WeaponForm scriptableWeaponForm, PlayerMaster playerMaster, string saveFile = "", bool useDebug = false)
        {
            base.InitWeaponForm(playerWeapon, scriptableWeaponForm, playerMaster, saveFile, useDebug);
        }

        public override void Fire(Vector3 firePosition, Vector3 fireRotation, bool shouldInstantiateMuzzleFlash)
        {
            if (GlobalData.Player.Data.AmmoInventory.TryGetAmmo(Characters.Player.PlayerData.Ammo.AmmoTypes.Energy))
            {
                base.Fire(firePosition, fireRotation, shouldInstantiateMuzzleFlash);

                GameObject obj;
                Projectile pro;

                for (int i = 1; i <= NumberOfProjectiles; ++i)
                {
                    Vector3 pelletRotation = Vector3.zero;
                    if (i != 0)
                    {
                        if (i % 2 == 0)
                        {
                            pelletRotation.z = UnityEngine.Random.Range(0, Spread / 2);
                        }
                        else
                        {
                            pelletRotation.z = UnityEngine.Random.Range(0, -(Spread / 2));
                        }
                    }

                    obj = _playerWeaponRef.Pool.Spawn();
                    if (obj)
                    {
                        pro = obj.GetComponent<Projectile>();
                        if (pro)
                        {
                            pro.ProjectilePrefab = _playerWeaponRef.Pool.PoolPrefab;

                            pro.ProjectileTransform.position = firePosition; //Set the position of the projectile to the appropriate firing position.
                            pro.ProjectileSpeed = ProjectileSpeed;

                            pro.Fire(fireRotation + pelletRotation, DamagePerProjectile, _playerGORef);
                        }
                    }
                    else
                    {
                        Debug.LogError("Object pool returned null object in Shotgun Weapon Form for projectile #" + i);
                    }
                }
            }
        }

        #region Gun Chips
        protected override void ApplyGunChipProperties(GunChip gunChip)
        {
            base.ApplyGunChipProperties(gunChip);

            if (gunChip.ImprovedProperty == WeaponFormProperties.WeaponProperties.Spread)
            {
                Spread -= _baseSpread * gunChip.PropertyImprovementAmount;
                if (Spread < 0)
                {
                    Spread = 0;
                }
            }
            else if (gunChip.ImpairedProperty == WeaponFormProperties.WeaponProperties.Spread)
            {
                Spread += _baseSpread * gunChip.PropertyImpairmentAmount;
            }
        }

        protected override void ReverseGunChipProperties(GunChip gunChip)
        {
            base.ReverseGunChipProperties(gunChip);

            if (gunChip.ImprovedProperty == WeaponFormProperties.WeaponProperties.Spread)
            {
                Spread += _baseSpread * gunChip.PropertyImprovementAmount;
            }
            else if (gunChip.ImpairedProperty == WeaponFormProperties.WeaponProperties.Spread)
            {
                Spread -= _baseSpread * gunChip.PropertyImpairmentAmount;
            }
        }
        #endregion

        #region Weapon Upgrades
        protected override void ApplyWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            base.ApplyWeaponUpgradeProperties(upgrade);

            if (upgrade.ChangedProperties != null)
            {
                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    if (upgrade.ChangedProperties.ChangedPropertiesList[i] == WeaponFormProperties.WeaponProperties.Spread)
                    {
                        Spread += _baseSpread * upgrade.ChangedProperties.ValueChangesList[i];
                    }
                }
            }

            upgrade.ApplyUpgradeToWeaponForm(this);
        }

        protected override void ReverseWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            base.ReverseWeaponUpgradeProperties(upgrade);

            if (upgrade.ChangedProperties != null)
            {
                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    if (upgrade.ChangedProperties.ChangedPropertiesList[i] == WeaponFormProperties.WeaponProperties.Spread)
                    {
                        Spread -= _baseSpread * upgrade.ChangedProperties.ValueChangesList[i];
                    }
                }
            }

            upgrade.RemoveUpgradeFromWeaponForm(this);
        }
        #endregion

        protected override void ApplyBaseWeaponFormStatsFromScriptableObject(WeaponForm weaponForm)
        {
            base.ApplyBaseWeaponFormStatsFromScriptableObject(weaponForm);

            ShotgunWeaponForm shotgun = weaponForm as ShotgunWeaponForm;
            _baseSpread = Spread = shotgun._baseSpread;
        }

        public override void SaveWeaponFormToFile(string saveFile)
        {
            base.SaveWeaponFormToFile(saveFile);
            ES3.Save<bool>("Has Shotgun", true, saveFile);
        }

        public override string ToString()
        {
            return "Shotgun";
        }
    }
}
