﻿using UnityEngine;
using Weapons.Player.Chips;
using UnityEngine.SceneManagement;
using Characters.Player;
using System.Collections.Generic;
using Weapons.Player.Upgrades;
using static Weapons.Player.WeaponFormProperties;

namespace Weapons.Player.Forms
{
    public abstract class WeaponForm : ScriptableObject
    {
        public delegate void WeaponFire();
        public event WeaponFire Fired;


        private const float MINIMUM_DAMAGE_PER_PROJECTILE = 1;
        private const float MINIMUM_COOLDOWN_TIME = 0;
        private const float MINIMUM_PROJECTILE_SPEED = 5;


        #region Public Properties + Fields
        public bool HoldToFireAllowed;
        public WeaponForms WeaponFormType { get { return _weaponForm; } }
        public PlayerData.Ammo.AmmoTypes AmmoType { get { return _ammoType; } }
        public float CooldownTime { get; protected set; }
        public float DamagePerProjectile { get; protected set; }
        public int NumberOfProjectiles { get; protected set; }
        public float ProjectileSpeed { get; protected set; }
        public GunChip FirstSlottedGunChip { get { return _firstSlottedGunChip; } }
        public GunChip SecondSlottedGunChip { get { return _secondSlottedGunChip; } }
        public List<WeaponUpgrade> PossessedUpgrades { get { return _possessedUpgrades; } }
        public bool CanSlotSecondGunChip { get; protected set; } = true;
        public int HighestGunChipRarity { get; private set; } = 1;
        public GameObject ProjectileAsset { get { return _projectileAssetToUse; } }

        #endregion

        #region Protected Properties + Fields
        [SerializeField]
        protected PlayerData.Ammo.AmmoTypes _ammoType;
        [SerializeField]
        protected float _baseCooldownTime;
        [SerializeField]
        protected float _baseDamagePerProjectile;
        [SerializeField]
        [Min(1)]
        protected int _baseNumberOfProjectiles;
        [SerializeField]
        protected float _baseProjectileSpeed;

        protected GunChip _firstSlottedGunChip;
        protected GunChip _secondSlottedGunChip;
        protected List<WeaponUpgrade> _possessedUpgrades = new List<WeaponUpgrade>();
        protected GameObject _projectileAssetToUse;

        protected PlayerWeapon _playerWeaponRef;
        protected PlayerMaster _playerRef;
        protected GameObject _playerGORef;

        protected GameObject _muzzleFlashEffectInstance;
        #endregion

        #region Private Properties + Fields
        [SerializeField]
        private WeaponForms _weaponForm;
        [SerializeField]
        private GameObject _baseProjectileAsset;
        [SerializeField]
        private GameObject _uncommonProjectileAsset;
        [SerializeField]
        private GameObject _rareProjectileAsset;
        [SerializeField]
        private GameObject _epicProjectileAsset;
        [SerializeField]
        private GameObject _baseMuzzleFlashAsset;
        [SerializeField]
        private GameObject _uncommonMuzzleFlashAsset;
        [SerializeField]
        private GameObject _rareMuzzleFlashAsset;
        [SerializeField]
        private GameObject _epicMuzzleFlashAsset;

        [SerializeField]
        private GunChip _debugFirstGunChip;
        [SerializeField]
        private GunChip _debugSecondGunChip;
        [SerializeField]
        private bool _debugCanSlotSecondGunChip;

        private GameObject _muzzleFlashAssetToUse;
        #endregion

        /// <summary>
        /// Initializes the weapon form by loading in its base stats and then applying any possessed gunchips or upgrades on top of that (whether loaded from a save file, or specifically applied through debug settings).
        /// </summary>
        /// <param name="playerWeaponRef">Reference to the player weapon component.</param>
        /// <param name="scriptableWeaponForm">The scriptable object matching the actual type of the weapon form being initialized.</param>
        /// <param name="playerRef">Reference to the player master component.</param>
        /// <param name="saveFile">The save file to load from. If an empty string is passed in, no loading is attempted.</param>
        /// <param name="useDebug">Whether to use debug settings when initializing the weapon form.</param>
        public virtual void InitWeaponForm(PlayerWeapon playerWeaponRef, WeaponForm scriptableWeaponForm, PlayerMaster playerRef, string saveFile = "", bool useDebug = false)
        {
            _playerRef = playerRef;
            _playerGORef = playerRef.gameObject;

            if (saveFile != "")
            {
                LoadWeaponFormFromFile(saveFile, scriptableWeaponForm, playerWeaponRef);
            }
            else
            {
                ApplyBaseWeaponFormStatsFromScriptableObject(scriptableWeaponForm);

                if (useDebug)
                {
                    /* We need to apply weapon upgrades first, as weapon upgrades are a permanent alteration to the weapon's base stats (unless the player respecs), which the gun chips modify further. */
                    if (_possessedUpgrades.Count != 0)
                    {
                        foreach (WeaponUpgrade upgrade in _possessedUpgrades)
                        {
                            ApplyWeaponUpgradeProperties(upgrade);
                        }
                    }

                    if (scriptableWeaponForm._debugFirstGunChip != null)
                    {
                        SlotGunChip(scriptableWeaponForm._debugFirstGunChip);
                    }
                    if (scriptableWeaponForm._debugSecondGunChip != null && scriptableWeaponForm._debugCanSlotSecondGunChip)
                    {
                        CanSlotSecondGunChip = true;
                        SlotGunChip(scriptableWeaponForm._debugSecondGunChip);
                    }
                }
            }

            SetProjectileAndMuzzleFlashAssetsBasedOnChipRarity();

            _playerWeaponRef = playerWeaponRef;
        }

        /// <summary>
        /// Handles the firing of the weapon form.
        /// </summary>
        /// <param name="firePosition">The position the projectile and muzzle flash effect (if applicable) should spawn at.</param>
        /// <param name="fireRotation">The rotation the projectile and muzzle flash effect (if applicable) should spawn at.</param>
        /// <param name="shouldInstantiateMuzzleFlash">Whether a muzzle flash effect should be spawned.</param>
        public virtual void Fire(Vector3 firePosition, Vector3 fireRotation, bool shouldInstantiateMuzzleFlash)
        {
            if (shouldInstantiateMuzzleFlash)
            {
                SceneManager.SetActiveScene(SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage.MasterScene));
                _muzzleFlashEffectInstance = Instantiate(_muzzleFlashAssetToUse);
                SceneManager.SetActiveScene(SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage[0].Path));

                _muzzleFlashEffectInstance.transform.position = firePosition;
                //We need to rotate the muzzle effect on the X because all the muzzle effects have already been rotated on the Y to turn them from 3D assets into 2D ones.
                _muzzleFlashEffectInstance.transform.rotation = Quaternion.Euler(new Vector3(fireRotation.z, _muzzleFlashAssetToUse.transform.eulerAngles.y, _muzzleFlashAssetToUse.transform.eulerAngles.z));
            }

            Fired?.Invoke();
        }

        #region Gun Chips
        /// <summary>
        /// Slots a gun chip by applying its property changes and checks if the rarity of the gun chips necessitates a projectile asset change.
        /// </summary>
        /// <param name="newGunChip">The gun chip to slot.</param>
        /// <returns></returns>
        public bool SlotGunChip(GunChip newGunChip)
        {
            if (_firstSlottedGunChip == null)
            {
                _firstSlottedGunChip = newGunChip;
                ApplyGunChipProperties(_firstSlottedGunChip);

                if (newGunChip.ChipRarityNumber > HighestGunChipRarity)
                {
                    HighestGunChipRarity = newGunChip.ChipRarityNumber;
                    SetProjectileAndMuzzleFlashAssetsBasedOnChipRarity();
                }

                return true;
            }
            else if (CanSlotSecondGunChip && _secondSlottedGunChip == null)
            {
                _secondSlottedGunChip = newGunChip;
                ApplyGunChipProperties(_secondSlottedGunChip);

                if (newGunChip.ChipRarityNumber > HighestGunChipRarity)
                {
                    HighestGunChipRarity = newGunChip.ChipRarityNumber;
                    SetProjectileAndMuzzleFlashAssetsBasedOnChipRarity();
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Drops the specified gun chip.
        /// </summary>
        /// <param name="whichChip">The int value representing which gun chip to drop (1 or 2).</param>
        /// <returns>Return the chip that was drooped or null on fail</returns>
        public GunChip DropGunChip(int whichChip)
        {
            switch (whichChip)
            {
                case 1:
                    ReverseGunChipProperties(_firstSlottedGunChip);
                    return _firstSlottedGunChip;
                case 2:
                    ReverseGunChipProperties(_secondSlottedGunChip);
                    return _firstSlottedGunChip;
                default:
                    Debug.LogError("We shouldn't be here. Something is trying to drop a non-existent chip number.\n" + System.Environment.StackTrace);
                    return null;
            }
        }

        /// <summary>
        /// Applies the property changes of the provided gun chip.
        /// </summary>
        /// <param name="gunChip">The gun chip to apply properties from.</param>
        protected virtual void ApplyGunChipProperties(GunChip gunChip)
        {
            switch (gunChip.ImprovedProperty)
            {
                case WeaponProperties.Damage:
                    float damageIncrease = _baseDamagePerProjectile * gunChip.PropertyImprovementAmount;
                    DamagePerProjectile += damageIncrease;
                    break;
                case WeaponProperties.CooldownTime:
                    float cooldownTimeReduction = _baseCooldownTime * gunChip.PropertyImprovementAmount;
                    CooldownTime -= cooldownTimeReduction;
                    if (CooldownTime < MINIMUM_COOLDOWN_TIME)
                    {
                        CooldownTime = MINIMUM_COOLDOWN_TIME;
                    }
                    break;
                case WeaponProperties.ProjectileSpeed:
                    float speedIncrease = _baseProjectileSpeed * gunChip.PropertyImprovementAmount;
                    ProjectileSpeed += speedIncrease;
                    break;
                case WeaponProperties.AmmoCapacity:
                    _playerRef.Data.AmmoInventory.AddToAmmoMultiplier(_ammoType, -gunChip.PropertyImprovementAmount);
                    break;
            }

            switch (gunChip.ImpairedProperty)
            {
                case WeaponProperties.Damage:
                    float damageReduction = _baseDamagePerProjectile * gunChip.PropertyImpairmentAmount;
                    DamagePerProjectile -= damageReduction;
                    if (DamagePerProjectile < MINIMUM_DAMAGE_PER_PROJECTILE)
                    {
                        DamagePerProjectile = MINIMUM_DAMAGE_PER_PROJECTILE;
                    }
                    break;
                case WeaponProperties.CooldownTime:
                    float cooldownTimeIncrease = _baseCooldownTime * gunChip.PropertyImpairmentAmount;
                    CooldownTime += cooldownTimeIncrease;
                    break;
                case WeaponProperties.ProjectileSpeed:
                    float speedReduction = _baseProjectileSpeed * gunChip.PropertyImpairmentAmount;
                    ProjectileSpeed -= speedReduction;
                    if (ProjectileSpeed < MINIMUM_PROJECTILE_SPEED)
                    {
                        ProjectileSpeed = MINIMUM_PROJECTILE_SPEED;
                    }
                    break;
                case WeaponProperties.AmmoCapacity:
                    _playerRef.Data.AmmoInventory.AddToAmmoMultiplier(_ammoType, gunChip.PropertyImpairmentAmount);
                    break;
            }
        }

        /// <summary>
        /// Reverses any property changes made by the provided gun chip.
        /// </summary>
        /// <param name="gunChip">The gun chip whose property changes need reversing.</param>
        protected virtual void ReverseGunChipProperties(GunChip gunChip)
        {
            switch (gunChip.ImprovedProperty)
            {
                case WeaponProperties.Damage:
                    DamagePerProjectile -= _baseDamagePerProjectile * gunChip.PropertyImprovementAmount;
                    break;
                case WeaponProperties.CooldownTime:
                    CooldownTime += _baseCooldownTime * gunChip.PropertyImprovementAmount;
                    break;
                case WeaponProperties.ProjectileSpeed:
                    ProjectileSpeed -= _baseProjectileSpeed * gunChip.PropertyImprovementAmount;
                    break;
                case WeaponProperties.AmmoCapacity:
                    _playerRef.Data.AmmoInventory.AddToAmmoMultiplier(_ammoType, gunChip.PropertyImprovementAmount);
                    break;
            }

            switch (gunChip.ImpairedProperty)
            {
                case WeaponProperties.Damage:
                    DamagePerProjectile += _baseDamagePerProjectile * gunChip.PropertyImpairmentAmount;
                    break;
                case WeaponProperties.CooldownTime:
                    CooldownTime -= _baseCooldownTime * gunChip.PropertyImpairmentAmount;
                    break;
                case WeaponProperties.ProjectileSpeed:
                    ProjectileSpeed += _baseProjectileSpeed * gunChip.PropertyImpairmentAmount;
                    break;
                case WeaponProperties.AmmoCapacity:
                    _playerRef.Data.AmmoInventory.AddToAmmoMultiplier(_ammoType, -gunChip.PropertyImpairmentAmount);
                    break;
            }
        }
        #endregion

        #region Weapon Upgrades
        /// <summary>
        /// Applies the property changes of the provided weapon upgrade.
        /// </summary>
        /// <param name="upgrade">The weapon upgrade whose properties need to be applied.</param>
        protected virtual void ApplyWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            if (upgrade.ChangedProperties != null)
            {
                upgrade.ChangedProperties.InitPropertySetter();

                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    switch (upgrade.ChangedProperties.ChangedPropertiesList[i])
                    {
                        case WeaponProperties.Damage:
                            DamagePerProjectile += _baseDamagePerProjectile * upgrade.ChangedProperties.ValueChangesList[i];
                            if (DamagePerProjectile < MINIMUM_DAMAGE_PER_PROJECTILE)
                            {
                                DamagePerProjectile = MINIMUM_DAMAGE_PER_PROJECTILE;
                            }
                            break;
                        case WeaponProperties.CooldownTime:
                            CooldownTime -= _baseCooldownTime * upgrade.ChangedProperties.ValueChangesList[i];
                            if (CooldownTime < MINIMUM_COOLDOWN_TIME)
                            {
                                CooldownTime = MINIMUM_COOLDOWN_TIME;
                            }
                            break;
                        case WeaponProperties.ProjectileSpeed:
                            ProjectileSpeed += _baseProjectileSpeed * upgrade.ChangedProperties.ValueChangesList[i];
                            if (ProjectileSpeed < MINIMUM_PROJECTILE_SPEED)
                            {
                                ProjectileSpeed = MINIMUM_PROJECTILE_SPEED;
                            }
                            break;
                        case WeaponProperties.AmmoCapacity:
                            _playerRef.Data.AmmoInventory.AddToAmmoMultiplier(_ammoType, upgrade.ChangedProperties.ValueChangesList[i]);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Reverses the property changes of the provided weapon upgrade.
        /// </summary>
        /// <param name="upgrade">The weapon uprgade whose properties need to be reversed.</param>
        protected virtual void ReverseWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            if (upgrade.ChangedProperties != null)
            {
                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    switch (upgrade.ChangedProperties.ChangedPropertiesList[i])
                    {
                        case WeaponProperties.Damage:
                            DamagePerProjectile -= _baseDamagePerProjectile * upgrade.ChangedProperties.ValueChangesList[i];
                            break;
                        case WeaponProperties.CooldownTime:
                            CooldownTime += _baseCooldownTime * upgrade.ChangedProperties.ValueChangesList[i];
                            break;
                        case WeaponProperties.ProjectileSpeed:
                            ProjectileSpeed -= _baseProjectileSpeed * upgrade.ChangedProperties.ValueChangesList[i];
                            break;
                        case WeaponProperties.AmmoCapacity:
                            _playerRef.Data.AmmoInventory.AddToAmmoMultiplier(_ammoType, -upgrade.ChangedProperties.ValueChangesList[i]);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Removes the provided weapon upgrade from the weapon and reverses any changes the upgrade provided.
        /// </summary>
        /// <param name="upgrade">The upgrade to remove.</param>
        protected void RemoveWeaponUpgrade(WeaponUpgrade upgrade)
        {
            /* When removing an upgrade, we need to first remove any gun chips attached, remove the upgrade, then reapply the gun chips.
             * The reason for this is that gun chips are a modification of the weapon form's base stats, which upgrades more permanently modify.
             * So if an upgrade is removed without re-calculating and re-applying gun chip values, a gun chip may end improving/impairing a weapon more than it should. */
            if (FirstSlottedGunChip != null)
            {
                ReverseGunChipProperties(_firstSlottedGunChip);
            }
            if (SecondSlottedGunChip != null)
            {
                ReverseGunChipProperties(_secondSlottedGunChip);
            }

            ReverseWeaponUpgradeProperties(upgrade);

            if (FirstSlottedGunChip != null)
            {
                ApplyGunChipProperties(_firstSlottedGunChip);
            }
            if (SecondSlottedGunChip != null)
            {
                if (CanSlotSecondGunChip)
                {
                    ApplyGunChipProperties(_secondSlottedGunChip);
                }
                else
                {
                    //The player has removed the upgrade that allowed them to slot two gun chips in this weapon form, so drop the 2nd chip.
                    DropGunChip(2);
                }
            }
        }
        #endregion

        /// <summary>
        /// Applies the base states for the weapon form from the provided scriptable object weapon form.
        /// </summary>
        /// <param name="scriptableObjectWeaponForm">The base scriptable object for the actual weapon form type.</param>
        protected virtual void ApplyBaseWeaponFormStatsFromScriptableObject(WeaponForm scriptableObjectWeaponForm)
        {
            _baseCooldownTime = CooldownTime = scriptableObjectWeaponForm._baseCooldownTime;
            _baseDamagePerProjectile = DamagePerProjectile = scriptableObjectWeaponForm._baseDamagePerProjectile;
            _baseNumberOfProjectiles = NumberOfProjectiles = scriptableObjectWeaponForm._baseNumberOfProjectiles;
            _baseProjectileSpeed = ProjectileSpeed = scriptableObjectWeaponForm._baseProjectileSpeed;

            _weaponForm = scriptableObjectWeaponForm.WeaponFormType;
            _ammoType = scriptableObjectWeaponForm.AmmoType;

            HoldToFireAllowed = scriptableObjectWeaponForm.HoldToFireAllowed;

            _baseProjectileAsset = scriptableObjectWeaponForm._baseProjectileAsset;
            _uncommonProjectileAsset = scriptableObjectWeaponForm._uncommonProjectileAsset;
            _rareProjectileAsset = scriptableObjectWeaponForm._rareProjectileAsset;
            _epicProjectileAsset = scriptableObjectWeaponForm._epicProjectileAsset;

            _baseMuzzleFlashAsset = scriptableObjectWeaponForm._baseMuzzleFlashAsset;
            _uncommonMuzzleFlashAsset = scriptableObjectWeaponForm._uncommonMuzzleFlashAsset;
            _rareMuzzleFlashAsset = scriptableObjectWeaponForm._rareMuzzleFlashAsset;
            _epicMuzzleFlashAsset = scriptableObjectWeaponForm._epicMuzzleFlashAsset;
        }

        /// <summary>
        /// Sets the correct projectile and muzzle flash assets based on the highest rarity gun chip slotted to the weapon form.
        /// </summary>
        private void SetProjectileAndMuzzleFlashAssetsBasedOnChipRarity()
        {
            switch (HighestGunChipRarity)
            {
                case (int)GunChip.RarityLevels.Common:
                    _projectileAssetToUse = _baseProjectileAsset;
                    _muzzleFlashAssetToUse = _baseMuzzleFlashAsset;
                    break;
                case (int)GunChip.RarityLevels.Uncommon:
                    _projectileAssetToUse = _uncommonProjectileAsset;
                    _muzzleFlashAssetToUse = _uncommonMuzzleFlashAsset;
                    break;
                case (int)GunChip.RarityLevels.Rare:
                    _projectileAssetToUse = _rareProjectileAsset;
                    _muzzleFlashAssetToUse = _rareMuzzleFlashAsset;
                    break;
                case (int)GunChip.RarityLevels.Epic:
                    _projectileAssetToUse = _epicProjectileAsset;
                    _muzzleFlashAssetToUse = _epicMuzzleFlashAsset;
                    break;
            }
        }

        #region Save + Load
        /// <summary>
        /// Saves the weapon form to the provided save file, including any upgrades and gun chips attached to the weapon form.
        /// </summary>
        /// <param name="saveFile">The save file to save to.</param>
        public virtual void SaveWeaponFormToFile(string saveFile)
        {
            if (_possessedUpgrades.Count != 0)
            {
                ES3.Save<Dictionary<string, WeaponUpgrade.UpgradeSaveData>>("Possessed Upgrades " + this, ConvertPossessedUpgradesToUpgradeDictionary(), saveFile);
            }
            else
            {
                ES3.DeleteKey("Possessed Upgrades " + this, saveFile);
            }

            if (_firstSlottedGunChip != null)
            {
                ES3.Save<string>("First Chip " + this, _firstSlottedGunChip.ConstructSaveStringForGunChip(), saveFile);
            }
            else
            {
                ES3.DeleteKey("First Chip " + this, saveFile);
                ES3.DeleteKey("First Chip Fired " + this, saveFile);
            }

            ES3.Save<bool>("Can Slot Second Chip " + this, CanSlotSecondGunChip, saveFile);

            if (_secondSlottedGunChip != null)
            {
                ES3.Save<string>("Second Chip " + this, _secondSlottedGunChip.ConstructSaveStringForGunChip(), saveFile);
            }
            else
            {
                ES3.DeleteKey("Second Chip " + this, saveFile);
                ES3.DeleteKey("Second Chip Fired " + this, saveFile);
            }
        }

        /// <summary>
        /// Loads the weapon from from the provided save file, including any upgrades and gun chips the weapon should possess.
        /// </summary>
        /// <param name="saveFile">The save file to load from.</param>
        /// <param name="scriptableObjectWeaponForm">The base scriptable object for the actual weapon form type being loaded, so base stats can be retrieved.</param>
        /// <param name="playerWeaponRef">Reference to the player weapon component.</param>
        protected virtual void LoadWeaponFormFromFile(string saveFile, WeaponForm scriptableObjectWeaponForm, PlayerWeapon playerWeaponRef)
        {
            ApplyBaseWeaponFormStatsFromScriptableObject(scriptableObjectWeaponForm);

            if (ES3.KeyExists("Possessed Upgrades " + this, saveFile))
            {
                _possessedUpgrades = ConvertUpgradeDictionaryToPossessedUpgrades(ES3.Load<Dictionary<string, WeaponUpgrade.UpgradeSaveData>>("Possessed Upgrades " + this, saveFile), playerWeaponRef);
                foreach (WeaponUpgrade upgrade in _possessedUpgrades)
                {
                    ApplyWeaponUpgradeProperties(upgrade);
                }
            }

            if (ES3.KeyExists("First Chip " + this, saveFile))
            {
                var firstSlottedGunChip = GunChip.ConstructGunChipFromSaveString(ES3.Load<string>("First Chip " + this, saveFile));
                SlotGunChip(firstSlottedGunChip);
            }

            CanSlotSecondGunChip = ES3.Load<bool>("Can Slot Second Chip " + this, saveFile, false);

            if (CanSlotSecondGunChip && ES3.KeyExists("Second Chip " + this, saveFile))
            {
                var secondSlottedGunChip = GunChip.ConstructGunChipFromSaveString(ES3.Load<string>("Second Chip " + this, saveFile));
                SlotGunChip(secondSlottedGunChip);
            }
        }

        /// <summary>
        /// Converts any upgrades possessed by this weapon form to an upgrade dictionary that can be written to the save file.
        /// </summary>
        /// <returns>A dictionary containing necessary information about the possessed weapon upgrades.</returns>
        private Dictionary<string, WeaponUpgrade.UpgradeSaveData> ConvertPossessedUpgradesToUpgradeDictionary()
        {
            Dictionary<string, WeaponUpgrade.UpgradeSaveData> upgrades = new Dictionary<string, WeaponUpgrade.UpgradeSaveData>();

            foreach (WeaponUpgrade upgrade in _possessedUpgrades)
            {
                upgrades.Add(upgrade.UpgradeTypeName, new WeaponUpgrade.UpgradeSaveData(upgrade.UpgradeName, upgrade.UpgradeTypeName));
            }

            return upgrades;
        }

        /// <summary>
        /// Converts a dictionary of possessed upgrades retrived from a save file to an actual list of upgrades.
        /// </summary>
        /// <param name="upgrades">The dictionary of possessed upgrades.</param>
        /// <param name="playerWeapon">Reference to the player weapon component.</param>
        /// <returns></returns>
        private List<WeaponUpgrade> ConvertUpgradeDictionaryToPossessedUpgrades(Dictionary<string, WeaponUpgrade.UpgradeSaveData> upgrades, PlayerWeapon playerWeapon)
        {
            List<WeaponUpgrade> possessedUpgrades = new List<WeaponUpgrade>();

            foreach (WeaponUpgrade.UpgradeSaveData savedUpgrade in upgrades.Values)
            {
                WeaponUpgrade upgradeInstance;
                if (savedUpgrade.UpgradeTypeName == "None")
                {
                    upgradeInstance = ScriptableObject.CreateInstance("WeaponUpgrade") as WeaponUpgrade;
                }
                else
                {
                    upgradeInstance = ScriptableObject.CreateInstance(savedUpgrade.UpgradeTypeName) as WeaponUpgrade;
                }

                WeaponUpgrade scriptableUpgradeToCopyFrom = playerWeapon.GetWeaponUpgradeFromUpgradeName(savedUpgrade.UpgradeName);
                if (scriptableUpgradeToCopyFrom != null)
                {
                    upgradeInstance.CopyOtherWeaponUpgradeScriptableObjectIntoThisOne(scriptableUpgradeToCopyFrom);

                    //I can't find another way to ensure the upgrade gets added to the list as the proper type other than to just test all upgrade types to find out which it is.
                    //Eventually all the weapon upgrade types will be here.
                    if (upgradeInstance is PistolTestWeaponUpgrade)
                    {
                        possessedUpgrades.Add(upgradeInstance as PistolTestWeaponUpgrade);
                    }
                }
                else
                {
                    Debug.LogError("PlayerWeapon couldn't find a possessed weapon upgrade that had been saved to the save file in its own collection of weapon upgrades. Check if any are missing from the list.");
                }
            }

            return possessedUpgrades;
        }
        #endregion

        public override string ToString()
        {
            return "Weapon Form";
        }
    }
}
