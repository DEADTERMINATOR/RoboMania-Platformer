﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;
using Weapons.Player.Chips;
using Weapons.Player.Upgrades;

namespace Weapons.Player.Forms
{
    [CreateAssetMenu(fileName = "Charge Rifle Weapon Form", menuName = "Weapon System/Create Weapon Form/Charge Rifle")]
    public class ChargeRifleWeaponForm : WeaponForm
    {
        private const float MAX_TIME_TO_HOLD_CHARGE = 5.0f;
        private const float CHARGE_PARTICLE_EFFECT_TIME_MULTIPLIER = 5.0f;


        public delegate void ChargeReset();
        public event ChargeReset ResetCalled;


        public float ChargeTime { get; private set; }


        [SerializeField]
        private float _baseChargeTime;
        [SerializeField]
        private GameObject _baseChargeEffect;
        [SerializeField]
        private GameObject _uncommonChargeEffect;
        [SerializeField]
        private GameObject _rareChargeEffect;
        [SerializeField]
        private GameObject _epicChargeEffect;

        /* The charge effect asset to be used based on the highest rarity gun chip slotted on the charge rifle. */
        private GameObject _chargeEffectToUse;
        private GameObject _instantiatedChargeEffect;

        private float _currentChargeTime = 0;
        private float _currentFullChargeHeldTime = 0;


        public override void InitWeaponForm(PlayerWeapon playerWeapon, WeaponForm scriptableWeaponForm, PlayerMaster playerRef, string saveFile = "", bool useDebug = false)
        {
            base.InitWeaponForm(playerWeapon, scriptableWeaponForm, playerRef, saveFile, useDebug);

            var scriptableChargeRifle = scriptableWeaponForm as ChargeRifleWeaponForm;
            switch (HighestGunChipRarity)
            {
                case (int)GunChip.RarityLevels.Common:
                    _chargeEffectToUse = scriptableChargeRifle._baseChargeEffect;
                    break;
                case (int)GunChip.RarityLevels.Uncommon:
                    _chargeEffectToUse = scriptableChargeRifle._uncommonChargeEffect;
                    break;
                case (int)GunChip.RarityLevels.Rare:
                    _chargeEffectToUse = scriptableChargeRifle._rareChargeEffect;
                    break;
                case (int)GunChip.RarityLevels.Epic:
                    _chargeEffectToUse = scriptableChargeRifle._epicChargeEffect;
                    break;
            }
        }

        public override void Fire(Vector3 firePosition, Vector3 fireRotation, bool shouldInstantiateMuzzleFlash)
        {
            base.Fire(firePosition, fireRotation, shouldInstantiateMuzzleFlash);

            Destroy(_instantiatedChargeEffect);
            _muzzleFlashEffectInstance.transform.rotation = Quaternion.Euler(fireRotation.z, _muzzleFlashEffectInstance.transform.eulerAngles.y, _muzzleFlashEffectInstance.transform.eulerAngles.z);

            _currentChargeTime = 0;
            _currentFullChargeHeldTime = 0;

            GameObject obj;
            Projectile pro;

            obj = _playerWeaponRef.Pool.Spawn();
            if (obj)
            {
                pro = obj.GetComponent<Projectile>();
                if (!pro)
                {
                    pro = obj.GetComponentInChildren<Projectile>(); // The projectile component might be in the child object if the projectile needs a parent object for rotation control.
                }

                if (pro)
                {
                    pro.ProjectilePrefab = _playerWeaponRef.Pool.PoolPrefab;

                    //Set the position of the projectile to the appropriate firing position.
                    pro.ProjectileTransform.position = firePosition;
                    pro.ProjectileSpeed = ProjectileSpeed;

                    pro.Fire(fireRotation, DamagePerProjectile, _playerGORef);
                }
            }
            else
            {
                Debug.LogError("Object pool returned null object in Rocket Launcher Weapon Form");
            }
        }

        #region Charging
        /// <summary>
        /// Handles the charging of the weapon, including the cancellation of the charge if the charge is complete and held for too long.
        /// </summary>
        /// <param name="firePosition">The position the weapon will fire from.</param>
        /// <param name="fireRotation">The rotation the weapon will fire at.</param>
        /// <returns></returns>
        public bool ChargeWeapon(Vector3 firePosition, Vector3 fireRotation)
        {
            if (_instantiatedChargeEffect == null)
            {
                var particleSystem = _chargeEffectToUse.GetComponent<ParticleSystem>();
                var particleSystemMain = particleSystem.main;
                particleSystemMain.duration = ChargeTime * CHARGE_PARTICLE_EFFECT_TIME_MULTIPLIER;

                _instantiatedChargeEffect = Instantiate(_chargeEffectToUse, firePosition, Quaternion.Euler(fireRotation));
            }
            else
            {
                _instantiatedChargeEffect.transform.position = firePosition;
                _instantiatedChargeEffect.transform.rotation = Quaternion.Euler(fireRotation);
            }

            var timeToAdd = Time.deltaTime;
            if (_currentChargeTime + timeToAdd <= ChargeTime)
            {
                _currentChargeTime += Time.deltaTime;
                return false;
            }
            else
            {
                //If the charge time is maxed, we need to add any remaining time to the hold time so the timing on everything feels consistent.
                var leftoverTime = timeToAdd - (ChargeTime - _currentChargeTime);
                _currentChargeTime = ChargeTime;
                _currentFullChargeHeldTime += leftoverTime;
                
                if (_currentFullChargeHeldTime > MAX_TIME_TO_HOLD_CHARGE)
                {
                    ResetCharge();
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Resets the charge of the rifle and stops the charging particle effect.
        /// </summary>
        public void ResetCharge()
        {
            ResetCalled?.Invoke();

            if (_instantiatedChargeEffect != null)
            {
                _instantiatedChargeEffect.GetComponent<ParticleSystem>().Stop(true, ParticleSystemStopBehavior.StopEmitting);
                _instantiatedChargeEffect = null; //The charge effects have been set up to destroy themselves on Stop, so we don't need to manually call Destroy.
            }

            _currentChargeTime = 0;
            _currentFullChargeHeldTime = 0;
        }
        #endregion

        #region Gun Chips
        protected override void ApplyGunChipProperties(GunChip gunChip)
        {
            base.ApplyGunChipProperties(gunChip);

            if (gunChip.ImprovedProperty == WeaponFormProperties.WeaponProperties.ChargeTime)
            {
                ChargeTime -= _baseChargeTime * gunChip.PropertyImprovementAmount;
                if (ChargeTime < 0)
                {
                    ChargeTime = 0;
                }
            }
            else if (gunChip.ImpairedProperty == WeaponFormProperties.WeaponProperties.ChargeTime)
            {
                ChargeTime += _baseChargeTime * gunChip.PropertyImpairmentAmount;
            }
        }

        protected override void ReverseGunChipProperties(GunChip gunChip)
        {
            base.ReverseGunChipProperties(gunChip);

            if (gunChip.ImprovedProperty == WeaponFormProperties.WeaponProperties.ChargeTime)
            {
                ChargeTime += _baseChargeTime * gunChip.PropertyImprovementAmount;
            }
            else if (gunChip.ImpairedProperty == WeaponFormProperties.WeaponProperties.ChargeTime)
            {
                ChargeTime -= _baseChargeTime * gunChip.PropertyImpairmentAmount;
            }
        }
        #endregion

        #region Weapon Upgrades
        protected override void ApplyWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            base.ApplyWeaponUpgradeProperties(upgrade);

            if (upgrade.ChangedProperties != null)
            {
                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    if (upgrade.ChangedProperties.ChangedPropertiesList[i] == WeaponFormProperties.WeaponProperties.ChargeTime)
                    {
                        ChargeTime += _baseChargeTime * upgrade.ChangedProperties.ValueChangesList[i];
                    }
                }
            }

            upgrade.ApplyUpgradeToWeaponForm(this);
        }

        protected override void ReverseWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            base.ReverseWeaponUpgradeProperties(upgrade);

            if (upgrade.ChangedProperties != null)
            {
                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    if (upgrade.ChangedProperties.ChangedPropertiesList[i] == WeaponFormProperties.WeaponProperties.ChargeTime)
                    {
                        ChargeTime -= _baseChargeTime * upgrade.ChangedProperties.ValueChangesList[i];
                    }
                }
            }

            upgrade.RemoveUpgradeFromWeaponForm(this);
        }
        #endregion

        protected override void ApplyBaseWeaponFormStatsFromScriptableObject(WeaponForm weaponForm)
        {
            base.ApplyBaseWeaponFormStatsFromScriptableObject(weaponForm);

            var chargeRifle = weaponForm as ChargeRifleWeaponForm;
            _baseChargeTime = ChargeTime = chargeRifle._baseChargeTime;
        }

        public override void SaveWeaponFormToFile(string saveFile)
        {
            base.SaveWeaponFormToFile(saveFile);
            ES3.Save<bool>("Has Charge Rifle", true, saveFile);
        }

        public override string ToString()
        {
            return "Charge Rifle";
        }
    }
}
