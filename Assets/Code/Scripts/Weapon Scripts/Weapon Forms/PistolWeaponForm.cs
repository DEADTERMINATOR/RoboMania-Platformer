﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weapons.Player.Upgrades;
using Characters.Player;
using Weapons.Player.Chips;

namespace Weapons.Player.Forms
{
    [CreateAssetMenu(fileName = "Pistol Weapon Form", menuName = "Weapon System/Create Weapon Form/Pistol")]
    public class PistolWeaponForm : WeaponForm
    {
        /* The amount of space between each projectile if the pistol is fired while possessing the spread upgrade */
        private const float PROJECTILE_SPACING = 0.4f;
        /* The amount of time the projectile spacing should occur over after the projectiles are fired. */
        private const float SPACING_TIME = 0.15f;


        public float ProjectileSize { get; private set; }


        [SerializeField]
        private float _baseProjectileSize;


        public override void InitWeaponForm(PlayerWeapon playerWeapon, WeaponForm scriptableWeaponForm, PlayerMaster playerMaster, string saveFile = "", bool useDebug = false)
        {
            base.InitWeaponForm(playerWeapon, scriptableWeaponForm, playerMaster, saveFile, useDebug);
        }

        public override void Fire(Vector3 firePosition, Vector3 fireRotation, bool shouldInstantiateMuzzleFlash)
        {
            base.Fire(firePosition, fireRotation, shouldInstantiateMuzzleFlash);

            GameObject obj;
            Projectile pro;

            for (int i = 1; i <= NumberOfProjectiles; ++i)
            {
                float spacingValue = 0;
                if (i == 2)
                {
                    spacingValue = PROJECTILE_SPACING;
                }
                else if (i == 3)
                {
                    spacingValue = -PROJECTILE_SPACING;
                }

                obj = _playerWeaponRef.Pool.Spawn();
                if (obj)
                {
                    pro = obj.GetComponent<Projectile>();
                    if (!pro)
                    {
                        pro = obj.GetComponentInChildren<Projectile>(); // The projectile component might be in the child object if the projectile needs a parent object for rotation control.
                    }

                    if (pro)
                    {
                        if (i != 1) //Since the first shot in a spread of projectiles will have no spacing, we can just skip it.
                        {
                            pro.InitialPositionVector = firePosition; //Store the initial position separate from the transform.

                            pro.FinalSpacedVector = firePosition + (Vector3)(StaticTools.RotateVector2(Vector2.up, fireRotation.z).normalized * spacingValue);
                            pro.LerpProjectile(SPACING_TIME); //Call the co-routine that performs the projectile spacing.
                        }

                        pro.ProjectilePrefab = _playerWeaponRef.Pool.PoolPrefab;

                        //Set the position of the projectile to the appropriate firing position.
                        pro.ProjectileTransform.position = firePosition;
                        pro.ProjectileSpeed = ProjectileSpeed;

                        pro.Fire(fireRotation, DamagePerProjectile, _playerGORef);
                    }
                }
                else
                {
                    Debug.LogError("Object pool returned null object in Pistol Weapon Form for projectile #" + i);
                }
            }
        }

        #region Gun Chips
        protected override void ApplyGunChipProperties(GunChip gunChip)
        {
            base.ApplyGunChipProperties(gunChip);

            if (gunChip.ImprovedProperty == WeaponFormProperties.WeaponProperties.ProjectileSize)
            {
                ProjectileSize += _baseProjectileSize * gunChip.PropertyImprovementAmount;
            }
            else if (gunChip.ImpairedProperty == WeaponFormProperties.WeaponProperties.ProjectileSize)
            {
                ProjectileSize -= _baseProjectileSize * gunChip.PropertyImpairmentAmount;
            }
        }

        protected override void ReverseGunChipProperties(GunChip gunChip)
        {
            base.ReverseGunChipProperties(gunChip);

            if (gunChip.ImprovedProperty == WeaponFormProperties.WeaponProperties.ProjectileSize)
            {
                ProjectileSize -= _baseProjectileSize * gunChip.PropertyImprovementAmount;
            }
            else if (gunChip.ImpairedProperty == WeaponFormProperties.WeaponProperties.ProjectileSize)
            {
                ProjectileSize += _baseProjectileSize * gunChip.PropertyImpairmentAmount;
            }
        }
        #endregion

        #region Weapon Upgrades
        protected override void ApplyWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            base.ApplyWeaponUpgradeProperties(upgrade);

            if (upgrade.ChangedProperties != null)
            {
                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    if (upgrade.ChangedProperties.ChangedPropertiesList[i] == WeaponFormProperties.WeaponProperties.ProjectileSize)
                    {
                        ProjectileSize += _baseProjectileSize * upgrade.ChangedProperties.ValueChangesList[i];
                    }
                }
            }

            upgrade.ApplyUpgradeToWeaponForm(this);
        }

        protected override void ReverseWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            base.ReverseWeaponUpgradeProperties(upgrade);

            if (upgrade.ChangedProperties != null)
            {
                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    if (upgrade.ChangedProperties.ChangedPropertiesList[i] == WeaponFormProperties.WeaponProperties.ProjectileSize)
                    {
                        ProjectileSize -= _baseProjectileSize * upgrade.ChangedProperties.ValueChangesList[i];
                    }
                }
            }

            upgrade.RemoveUpgradeFromWeaponForm(this);
        }
        #endregion

        protected override void ApplyBaseWeaponFormStatsFromScriptableObject(WeaponForm weaponForm)
        {
            base.ApplyBaseWeaponFormStatsFromScriptableObject(weaponForm);

            PistolWeaponForm pistol = weaponForm as PistolWeaponForm;
            _baseProjectileSize = ProjectileSize = pistol._baseProjectileSize;
        }

        public override void SaveWeaponFormToFile(string saveFile)
        {
            base.SaveWeaponFormToFile(saveFile);
            ES3.Save<bool>("Has Pistol", true, saveFile);
        }

        public override string ToString()
        {
            return "Pistol";
        }
    }
}
