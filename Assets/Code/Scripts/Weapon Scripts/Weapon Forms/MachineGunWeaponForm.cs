﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weapons.Player.Upgrades;
using Characters.Player;
using Weapons.Player.Chips;
using static Weapons.Player.WeaponFormProperties;

namespace Weapons.Player.Forms
{
    [CreateAssetMenu(fileName = "Machine Gun Weapon Form", menuName = "Weapon System/Create Weapon Form/Machine Gun")]
    public class MachineGunWeaponForm : WeaponForm
    {
        private delegate void FireInput();

        public float RandomSpreadRange { get; private set; }
        public float TimeToMaxSpread { get; private set; }
        public float CurrentSpread { get; private set; }


        [SerializeField]
        private float _baseRandomSpreadRange;
        [SerializeField]
        private float _baseTimeToMaxSpread;

        private bool _isBeingFired = false;
        private float _howLongHasBeenFired = 0;

        private float _spreadIncreasePerSec = 0;

        private PlayerInput.InputStatus _pressed;
        private PlayerInput.InputStatus _released;
        private bool _fireDelegatesAdded = false;


        private void Awake()
        {
            _pressed = () => { _isBeingFired = true; };
            _released = () => { _isBeingFired = false; };
        }

        public override void InitWeaponForm(PlayerWeapon playerWeapon, WeaponForm scriptableWeaponForm, PlayerMaster playerMaster, string saveFile = "", bool useDebug = false)
        {
            base.InitWeaponForm(playerWeapon, scriptableWeaponForm, playerMaster, saveFile, useDebug);

            _playerRef.PlayerWeapon.WeaponSwitch += OnWeaponSwitch;
            _playerRef.PlayerWeapon.UpdateCalled += HandleSpreadIncreaseOrDecrease;
        }

        public override void Fire(Vector3 firePosition, Vector3 fireRotation, bool shouldInstantiateMuzzleFlash)
        {
            if (!_fireDelegatesAdded)
            {
                //This is for the case where the player spawns with the machine gun already selected.
                AddFireDelegates();
                _isBeingFired = true;
            }

            if (GlobalData.Player.Data.AmmoInventory.TryGetAmmo(PlayerData.Ammo.AmmoTypes.Bullet))
            {
                base.Fire(firePosition, fireRotation, shouldInstantiateMuzzleFlash);

                GameObject obj;
                Projectile pro;

                for (int i = 1; i <= NumberOfProjectiles; ++i)
                {
                    //Generate a random rotation between -(CurrentSpread / 2) and CurrentSpread / 2
                    //e.g. _currentSpread = 20 means a value between -10 and 10.
                    //_currentSpread increases as the fire button is held longer (up to a max of RandomSpreadRange) and decreases to 0 the longer the fire button is released.
                    Vector3 rotationValue = new Vector3(0, 0, UnityEngine.Random.Range(0 - CurrentSpread, 0 + CurrentSpread));

                    obj = _playerWeaponRef.Pool.Spawn();
                    if (obj)
                    {
                        pro = obj.GetComponent<Projectile>();
                        if (pro)
                        {
                            pro.ProjectilePrefab = _playerWeaponRef.Pool.PoolPrefab;

                            pro.ProjectileTransform.position = firePosition; //Set the position of the projectile to the appropriate firing position.
                            pro.ProjectileSpeed = ProjectileSpeed;

                            pro.Fire(fireRotation + rotationValue, DamagePerProjectile, _playerGORef);
                        }
                    }
                    else
                    {
                        Debug.LogError("Object pool returned null object in Machine Gun Weapon Form");
                    }
                }
            }
        }

        private void HandleSpreadIncreaseOrDecrease(float time)
        {
            if (_isBeingFired && _playerRef.Data.AmmoInventory.GetCurrentAmmoCount(PlayerData.Ammo.AmmoTypes.Bullet) > 0)
            {
                if (_howLongHasBeenFired + time > TimeToMaxSpread)
                {
                    _howLongHasBeenFired = TimeToMaxSpread;
                    CurrentSpread = RandomSpreadRange;
                }
                else
                {
                    _howLongHasBeenFired += time;
                    CurrentSpread += _spreadIncreasePerSec * time;
                }
            }
            else
            {
                if (_howLongHasBeenFired - time < 0)
                {
                    _howLongHasBeenFired = 0;
                    CurrentSpread = 0;
                }
                else
                {
                    _howLongHasBeenFired -= time;
                    CurrentSpread -= _spreadIncreasePerSec * time;
                }
            }
        }

        private void OnWeaponSwitch(WeaponForm newForm)
        {
            if (newForm is MachineGunWeaponForm)
            {
                AddFireDelegates();
            }
            else
            {
                RemoveFireDelegates();
            }
        }

        private void AddFireDelegates()
        {
            _playerRef.Input.FireInputPressed += _pressed;
            _playerRef.Input.FireInputReleased += _released;
            _fireDelegatesAdded = true;
        }

        private void RemoveFireDelegates()
        {
            _playerRef.Input.FireInputPressed -= _pressed;
            _playerRef.Input.FireInputReleased -= _released;

            _fireDelegatesAdded = false;
            _isBeingFired = false;
        }

        #region Gun Chips
        protected override void ApplyGunChipProperties(GunChip gunChip)
        {
            base.ApplyGunChipProperties(gunChip);

            switch (gunChip.ImprovedProperty)
            {
                case WeaponProperties.RandomSpreadRange:
                    RandomSpreadRange -= _baseRandomSpreadRange * gunChip.PropertyImprovementAmount;
                    if (RandomSpreadRange < 0)
                    {
                        RandomSpreadRange = 0;
                    }
                    break;
                case WeaponProperties.TimeToMaxSpread:
                    break;
            }

            switch (gunChip.ImpairedProperty)
            {
                case WeaponProperties.RandomSpreadRange:
                    RandomSpreadRange += _baseRandomSpreadRange * gunChip.PropertyImpairmentAmount;
                    break;
                case WeaponProperties.TimeToMaxSpread:
                    break;
            }

            _spreadIncreasePerSec = RandomSpreadRange / TimeToMaxSpread;
        }

        protected override void ReverseGunChipProperties(GunChip gunChip)
        {
            base.ReverseGunChipProperties(gunChip);

            switch (gunChip.ImprovedProperty)
            {
                case WeaponProperties.RandomSpreadRange:
                    RandomSpreadRange += _baseRandomSpreadRange * gunChip.PropertyImprovementAmount;
                    break;
                case WeaponProperties.TimeToMaxSpread:
                    break;
            }

            switch (gunChip.ImpairedProperty)
            {
                case WeaponProperties.RandomSpreadRange:
                    RandomSpreadRange -= _baseRandomSpreadRange * gunChip.PropertyImpairmentAmount;
                    break;
                case WeaponProperties.TimeToMaxSpread:
                    break;
            }

            _spreadIncreasePerSec = RandomSpreadRange / TimeToMaxSpread;
        }
        #endregion

        #region Weapon Upgrades
        protected override void ApplyWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            base.ApplyWeaponUpgradeProperties(upgrade);

            if (upgrade.ChangedProperties != null)
            {
                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    if (upgrade.ChangedProperties.ChangedPropertiesList[i] == WeaponFormProperties.WeaponProperties.RandomSpreadRange)
                    {
                        RandomSpreadRange += _baseRandomSpreadRange * upgrade.ChangedProperties.ValueChangesList[i];
                    }
                }
            }

            upgrade.ApplyUpgradeToWeaponForm(this);
        }

        protected override void ReverseWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            base.ReverseWeaponUpgradeProperties(upgrade);

            if (upgrade.ChangedProperties != null)
            {
                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    if (upgrade.ChangedProperties.ChangedPropertiesList[i] == WeaponFormProperties.WeaponProperties.RandomSpreadRange)
                    {
                        RandomSpreadRange -= _baseRandomSpreadRange * upgrade.ChangedProperties.ValueChangesList[i];
                    }
                }
            }

            upgrade.RemoveUpgradeFromWeaponForm(this);
        }
        #endregion

        #region Save & Load
        protected override void ApplyBaseWeaponFormStatsFromScriptableObject(WeaponForm weaponForm)
        {
            base.ApplyBaseWeaponFormStatsFromScriptableObject(weaponForm);

            MachineGunWeaponForm machineGun = weaponForm as MachineGunWeaponForm;
            _baseRandomSpreadRange = RandomSpreadRange = machineGun._baseRandomSpreadRange;
            _baseTimeToMaxSpread = TimeToMaxSpread = machineGun._baseTimeToMaxSpread;

            _spreadIncreasePerSec = _baseRandomSpreadRange / _baseTimeToMaxSpread;
        }

        public override void SaveWeaponFormToFile(string saveFile)
        {
            base.SaveWeaponFormToFile(saveFile);
            ES3.Save<bool>("Has Machine Gun", true, saveFile);
        }

        public override string ToString()
        {
            return "Machine Gun";
        }
        #endregion
    }
}
