﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Weapons.Player.Forms;

namespace Weapons.Player
{
    public static class WeaponFormProperties
    {
        private const int PROPERTY_NONE = 0;
        private const int PROPERTY_GENERIC_DAMAGE = 1;
        private const int PROPERTY_GENERIC_COOLDOWN_TIME = 2;
        private const int PROPERTY_GENERIC_PROJECTILE_SPEED = 3;
        private const int PROPERTY_GENERIC_AMMO_CAPACITY = 4;
        private const int PROPERTY_GENERIC_SCRAP_MULTIPLIER = 5;
        private const int PROPERTY_PISTOL_PROJECTILE_SIZE = 6;
        private const int PROPERTY_MACHINE_GUN_RANDOM_SPREAD_RANGE = 7;
        private const int PROPERTY_MACHINE_GUN_TIME_TO_MAX_SPREAD = 8;
        private const int PROPERTY_SHOTGUN_SPREAD = 9;
        private const int PROPERTY_ROCKET_LAUNCHER_EXPLOSION_RADIUS = 10;
        private const int PROPERTY_CHARGE_RIFLE_CHARGE_TIME = 11;

        private const string PROPERTY_GENERIC_DAMAGE_NAME = "Damage";
        private const string PROPERTY_GENERIC_COOLDOWN_TIME_NAME = "Cooldown Time";
        private const string PROPERTY_GENERIC_PROJECTILE_SPEED_NAME = "Projectile Speed";
        private const string PROPERTY_GENERIC_AMMO_CAPACITY_NAME = "Ammo Capacity";
        private const string PROPERTY_GENERIC_SCRAP_MULTIPLIER_NAME = "Scrap Multiplier";
        private const string PROPERTY_PISTOL_PROJECTILE_SIZE_NAME = "Projectile Size";
        private const string PROPERTY_MACHINE_GUN_RANDOM_SPREAD_RANGE_NAME = "Random Spread";
        private const string PROPERTY_MACHINE_GUN_TIME_TO_MAX_SPREAD_NAME = "Time to Max Spread";
        private const string PROPERTY_SHOTGUN_SPREAD_NAME = "Spread";
        private const string PROPERTY_ROCKET_LAUNCHER_EXPLOSION_RADIUS_NAME = "Explosion Radius";
        private const string PROPERTY_CHARGE_RIFLE_CHARGE_TIME_NAME = "Charge Time";


        public enum WeaponForms { Pistol, MachineGun, Shotgun, RocketLauncher, ChargeRifle }

        public enum WeaponProperties
        {
            None = PROPERTY_NONE, Damage = PROPERTY_GENERIC_DAMAGE, CooldownTime = PROPERTY_GENERIC_COOLDOWN_TIME, ProjectileSpeed = PROPERTY_GENERIC_PROJECTILE_SPEED, AmmoCapacity = PROPERTY_GENERIC_AMMO_CAPACITY, ScrapMultiplier = PROPERTY_GENERIC_SCRAP_MULTIPLIER,
            ProjectileSize = PROPERTY_PISTOL_PROJECTILE_SIZE, RandomSpreadRange = PROPERTY_MACHINE_GUN_RANDOM_SPREAD_RANGE, TimeToMaxSpread = PROPERTY_MACHINE_GUN_TIME_TO_MAX_SPREAD, Spread = PROPERTY_SHOTGUN_SPREAD, ExplosionRadius = PROPERTY_ROCKET_LAUNCHER_EXPLOSION_RADIUS, ChargeTime = PROPERTY_CHARGE_RIFLE_CHARGE_TIME
        }


        private static Dictionary<int, string> _propertiesToNamesDictionary = new Dictionary<int, string>()
        {
            { (int)WeaponProperties.Damage, PROPERTY_GENERIC_DAMAGE_NAME },
            { (int)WeaponProperties.CooldownTime, PROPERTY_GENERIC_COOLDOWN_TIME_NAME },
            { (int)WeaponProperties.ProjectileSpeed, PROPERTY_GENERIC_PROJECTILE_SPEED_NAME },
            { (int)WeaponProperties.AmmoCapacity, PROPERTY_GENERIC_AMMO_CAPACITY_NAME },
            { (int)WeaponProperties.ScrapMultiplier, PROPERTY_GENERIC_SCRAP_MULTIPLIER_NAME },
            { (int)WeaponProperties.ProjectileSize, PROPERTY_PISTOL_PROJECTILE_SIZE_NAME },
            { (int)WeaponProperties.RandomSpreadRange, PROPERTY_MACHINE_GUN_RANDOM_SPREAD_RANGE_NAME },
            { (int)WeaponProperties.TimeToMaxSpread, PROPERTY_MACHINE_GUN_TIME_TO_MAX_SPREAD_NAME },
            { (int)WeaponProperties.Spread, PROPERTY_SHOTGUN_SPREAD_NAME },
            { (int)WeaponProperties.ExplosionRadius, PROPERTY_ROCKET_LAUNCHER_EXPLOSION_RADIUS_NAME },
            { (int)WeaponProperties.ChargeTime, PROPERTY_CHARGE_RIFLE_CHARGE_TIME_NAME }
        };

        private static Dictionary<string, WeaponProperties> _namesToPropertiesDictionary = new Dictionary<string, WeaponProperties>()
        {
            { PROPERTY_GENERIC_DAMAGE_NAME, WeaponProperties.Damage },
            { PROPERTY_GENERIC_COOLDOWN_TIME_NAME, WeaponProperties.CooldownTime },
            { PROPERTY_GENERIC_PROJECTILE_SPEED_NAME, WeaponProperties.ProjectileSpeed },
            { PROPERTY_GENERIC_AMMO_CAPACITY_NAME, WeaponProperties.AmmoCapacity },
            { PROPERTY_GENERIC_SCRAP_MULTIPLIER_NAME, WeaponProperties.ScrapMultiplier },
            { PROPERTY_PISTOL_PROJECTILE_SIZE_NAME, WeaponProperties.ProjectileSize },
            { PROPERTY_MACHINE_GUN_RANDOM_SPREAD_RANGE_NAME, WeaponProperties.RandomSpreadRange },
            { PROPERTY_MACHINE_GUN_TIME_TO_MAX_SPREAD_NAME, WeaponProperties.TimeToMaxSpread },
            { PROPERTY_SHOTGUN_SPREAD_NAME, WeaponProperties.Spread },
            { PROPERTY_ROCKET_LAUNCHER_EXPLOSION_RADIUS_NAME, WeaponProperties.ExplosionRadius },
            { PROPERTY_CHARGE_RIFLE_CHARGE_TIME_NAME, WeaponProperties.ChargeTime }
        };


        public static List<WeaponProperties> GetValidPropertiesForWeaponForm(WeaponForms form)
        {
            List<WeaponProperties> genericPropertiesList = new List<WeaponProperties>(new WeaponProperties[] { WeaponProperties.Damage, WeaponProperties.CooldownTime, WeaponProperties.ProjectileSpeed, WeaponProperties.AmmoCapacity, WeaponProperties.ScrapMultiplier });
            switch (form)
            {
                case WeaponForms.Pistol:
                    genericPropertiesList.Add(WeaponProperties.ProjectileSize);
                    return genericPropertiesList;
                case WeaponForms.MachineGun:
                    genericPropertiesList.Add(WeaponProperties.RandomSpreadRange);
                    return genericPropertiesList;
                case WeaponForms.Shotgun:
                    genericPropertiesList.Add(WeaponProperties.Spread);
                    return genericPropertiesList;
                case WeaponForms.RocketLauncher:
                    genericPropertiesList.Add(WeaponProperties.ExplosionRadius);
                    return genericPropertiesList;
                case WeaponForms.ChargeRifle:
                    genericPropertiesList.Add(WeaponProperties.ChargeTime);
                    return genericPropertiesList;
            }

            return genericPropertiesList;
        }

        public static string GetPropertyName(WeaponProperties property)
        {
            string name;
            bool valueExists = _propertiesToNamesDictionary.TryGetValue((int)property, out name);

            if (valueExists)
            {
                return name;
            }
            return "Invalid";
        }

        public static string[] GetPropertyNames(List<WeaponProperties> properties)
        {
            string[] namesArray = new string[properties.Count];
            for (int i = 0; i < namesArray.Length; ++i)
            {
                namesArray[i] = GetPropertyName(properties[i]);
            }

            return namesArray;
        }

        public static WeaponProperties GetWeaponPropertyFromName(string propertyName)
        {
            WeaponProperties property;
            bool valueExists = _namesToPropertiesDictionary.TryGetValue(propertyName, out property);

            if (valueExists)
            {
                return property;
            }
            return WeaponProperties.None;
        }
    }
}
