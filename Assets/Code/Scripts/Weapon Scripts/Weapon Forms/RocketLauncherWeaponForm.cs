﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;
using Weapons.Player.Chips;
using Weapons.Player.Upgrades;

namespace Weapons.Player.Forms
{
    [CreateAssetMenu(fileName = "Rocket Launcher Weapon Form", menuName = "Weapon System/Create Weapon Form/Rocket Launcher")]
    public class RocketLauncherWeaponForm : WeaponForm
    {
        public float ExplosionRadius { get; private set; }


        [SerializeField]
        private float _baseExplosionRadius;


        public override void InitWeaponForm(PlayerWeapon playerWeapon, WeaponForm scriptableWeaponForm, PlayerMaster playerMaster, string saveFile = "", bool useDebug = false)
        {
            base.InitWeaponForm(playerWeapon, scriptableWeaponForm, playerMaster, saveFile, useDebug);
        }

        public override void Fire(Vector3 firePosition, Vector3 fireRotation, bool shouldInstantiateMuzzleFlash)
        {
            base.Fire(firePosition, fireRotation, shouldInstantiateMuzzleFlash);

            GameObject obj;
            Projectile pro;

            obj = _playerWeaponRef.Pool.Spawn();
            if (obj)
            {
                pro = obj.GetComponent<Projectile>();
                if (!pro)
                {
                    pro = obj.GetComponentInChildren<Projectile>(); // The projectile component might be in the child object if the projectile needs a parent object for rotation control.
                }

                if (pro)
                {
                    pro.ProjectilePrefab = _playerWeaponRef.Pool.PoolPrefab;

                    //Set the position of the projectile to the appropriate firing position.
                    pro.ProjectileTransform.position = firePosition;
                    pro.ProjectileSpeed = ProjectileSpeed;

                    pro.Fire(fireRotation, DamagePerProjectile, _playerGORef);
                }
            }
            else
            {
                Debug.LogError("Object pool returned null object in Rocket Launcher Weapon Form");
            }
        }

        #region Gun Chips
        protected override void ApplyGunChipProperties(GunChip gunChip)
        {
            base.ApplyGunChipProperties(gunChip);

            if (gunChip.ImprovedProperty == WeaponFormProperties.WeaponProperties.ExplosionRadius)
            {
                ExplosionRadius += _baseExplosionRadius * gunChip.PropertyImprovementAmount;
            }
            else if (gunChip.ImpairedProperty == WeaponFormProperties.WeaponProperties.ExplosionRadius)
            {
                ExplosionRadius -= _baseExplosionRadius * gunChip.PropertyImpairmentAmount;
            }
        }

        protected override void ReverseGunChipProperties(GunChip gunChip)
        {
            base.ReverseGunChipProperties(gunChip);

            if (gunChip.ImprovedProperty == WeaponFormProperties.WeaponProperties.ExplosionRadius)
            {
                ExplosionRadius -= _baseExplosionRadius * gunChip.PropertyImprovementAmount;
            }
            else if (gunChip.ImpairedProperty == WeaponFormProperties.WeaponProperties.ExplosionRadius)
            {
                ExplosionRadius += _baseExplosionRadius * gunChip.PropertyImpairmentAmount;
            }
        }
        #endregion

        #region Weapon Upgrades
        protected override void ApplyWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            base.ApplyWeaponUpgradeProperties(upgrade);

            if (upgrade.ChangedProperties != null)
            {
                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    if (upgrade.ChangedProperties.ChangedPropertiesList[i] == WeaponFormProperties.WeaponProperties.ExplosionRadius)
                    {
                        ExplosionRadius += _baseExplosionRadius * upgrade.ChangedProperties.ValueChangesList[i];
                    }
                }
            }

            upgrade.ApplyUpgradeToWeaponForm(this);
        }

        protected override void ReverseWeaponUpgradeProperties(WeaponUpgrade upgrade)
        {
            base.ReverseWeaponUpgradeProperties(upgrade);

            if (upgrade.ChangedProperties != null)
            {
                for (int i = 0; i < upgrade.ChangedProperties.ChangedPropertiesList.Count; ++i)
                {
                    if (upgrade.ChangedProperties.ChangedPropertiesList[i] == WeaponFormProperties.WeaponProperties.ExplosionRadius)
                    {
                        ExplosionRadius -= _baseExplosionRadius * upgrade.ChangedProperties.ValueChangesList[i];
                    }
                }
            }

            upgrade.RemoveUpgradeFromWeaponForm(this);
        }
        #endregion

        protected override void ApplyBaseWeaponFormStatsFromScriptableObject(WeaponForm weaponForm)
        {
            base.ApplyBaseWeaponFormStatsFromScriptableObject(weaponForm);

            var rocketLauncher = weaponForm as RocketLauncherWeaponForm;
            _baseExplosionRadius = ExplosionRadius = rocketLauncher._baseExplosionRadius;
        }

        public override void SaveWeaponFormToFile(string saveFile)
        {
            base.SaveWeaponFormToFile(saveFile);
            ES3.Save<bool>("Has Rocket Launcher", true, saveFile);
        }

        public override string ToString()
        {
            return "Rocket Launcher";
        }
    }
}
