﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class Beam : Projectile
{
    public delegate void BeamEffect();
    public event BeamEffect BeamStarted;
    public event BeamEffect BeamFinished;

    /// <summary>
    /// How far from the raycast hit point the end effect is positioned
    /// </summary>
    public const float BEAM_END_OFFSET = 0.75f;

    /// <summary>
    /// How fast the texture scrolls along the beam
    /// </summary>
    public const float TEXTURE_SCROLL_SPEED = 8f;

    /// <summary>
    /// Length of the beam texture
    /// </summary>
    public const float TEXTURE_LENGTH_SCALE = 3;


    /// <summary>
    /// The movement speed of the beam.
    /// </summary>
    public float BeamMovementSpeed = 40f;

    /// <summary>
    /// Whether the beam should be turned off (the components are set to inactive), instead of the having the components destroyed, on a kill call.
    /// </summary>
    public bool ShouldTurnOffInsteadOfKill { get { return _shouldTurnOffInsteadOfKill; } set { _shouldTurnOffInsteadOfKill = value; killOffScreen = !value; } }


    private bool _shouldTurnOffInsteadOfKill;

    /// <summary>
    /// The instantiated version of the start of the beam.
    /// </summary>
    private GameObject _beamStart;

    /// <summary>
    /// The instantiated version of the end of the beam.
    /// </summary>
    private GameObject _beamEnd;

    /// <summary>
    /// The instantiated version of the beam itself.
    /// </summary>
    private GameObject _beam;

    /// <summary>
    /// The particle system for the start of the beam.
    /// </summary>
    private ParticleSystem _beamStartPS;

    /// <summary>
    /// The particle system for the end of the beam.
    /// </summary>
    private ParticleSystem _beamEndPS;

    /// <summary>
    /// Reference to the line renderer component of the beam.
    /// </summary>
    private LineRenderer _line;

    private BoxCollider2D _collider;

    private bool _currentlyFiring = false;

    private Vector3 _currentFiringDirection;
    private int _currentLayerMask;
    private Collider2D _lastHitCollider;

    private Vector3 _baseColliderPosition;
    private Vector3 _baseColliderSize;


    protected override void Awake()
    {
        base.Awake();
        performInitialScaling = false;
    }

    protected override void Update()
    {
        base.Update();

        if (_currentlyFiring)
        {
            RaycastHit2D hit = Physics2D.Raycast(_beamStart.transform.position, _currentFiringDirection, 100, _currentLayerMask);
            if ((hit && _lastHitCollider == null) || (hit.collider != null && hit.collider != _lastHitCollider))
            {
                _beamEnd.transform.position = hit.point;
                _line.SetPosition(1, _beamEnd.transform.position);

                _collider.size = new Vector3(hit.distance * 2 + BEAM_END_OFFSET, _collider.size.y);
                _collider.transform.localPosition = new Vector3(_beamStart.transform.localPosition.z + _collider.size.x, _collider.transform.localPosition.y);
            }
        }
    }

    public override void Fire(Vector3 angle, float damage, GameObject owner)
    {
        Fire(transform.position, angle, 1 << 8 | 1 << 9 | 1 << 12 | 1 << 13 | 1 << 16 | 1 << 17, damage, owner);
    }

    public void Fire(Vector3 start, Vector3 angle, int layerMask, float damage, GameObject owner)
    {
        base.Fire(angle, damage, owner);

        if (_beamStart == null)
        {
            _beamStart = transform.Find("Beam Start").gameObject;
            _beamEnd = transform.Find("Beam End").gameObject;
            _beam = transform.Find("Beam").gameObject;

            _line = _beam.GetComponent<LineRenderer>();

            Transform colliderTransform = transform.Find("Collider");
            if (colliderTransform == null)
                colliderTransform = transform.parent.Find("Collider");

            _collider = colliderTransform.GetComponent<BoxCollider2D>();

            _baseColliderPosition = _collider.transform.localPosition;
            _baseColliderSize = _collider.size;

            _beamStartPS = _beamStart.GetComponent<ParticleSystem>();
            _beamEndPS = _beamEnd.GetComponent<ParticleSystem>();

            _beamStartPS.Play();
            _beamEndPS.Play();
        }

        FireBeamInDirection(start, StaticTools.Vector2FromAngle(angle.z), layerMask);
    }

    public override void Kill()
    {
        if (!ShouldTurnOffInsteadOfKill)
        {
            Destroy(_beamStart);
            Destroy(_beamEnd);
            Destroy(_beam);

            _line = null;
            _beamStartPS = null;
            _beamEndPS = null;
        }
        else
        {
            TurnOff();
        }
    }

    public void TurnOff()
    {
        Timing.RunCoroutine(FinishBeamEffect(), "Finish Beam Effect");
    }

    protected void OnTriggerStay2D(Collider2D collider)
    {
        HandleCollision(collider, true);
    }

    protected void OnCollisionStay2D(Collision2D collision)
    {
        HandleCollision(collision.collider, false);
    }

    /// <summary>
    /// Handles the projectile's collision, including whether the collision should even be accepted, applying the appropriate modifiers,
    /// and destroying the projectile, among other things. This method handles both trigger and non-trigger colliders.
    /// </summary>
    /// <param name="collider">The 2D collider that the projectile collided with.</param>
    /// <param name="isTrigger">Whether the collider was a trigger.</param>
    public override void HandleCollision(Collider2D collider, bool isTrigger)
    {
        IDamageReceiver receiver = collider.GetComponent<IDamageReceiver>();
        Projectile projectileComponent = collider.gameObject.GetComponent<Projectile>(); //Try to get a projectile component from the collision result to see if we collided with another projectile.
        if (collider.gameObject != Owner && collider.gameObject.layer != Owner.layer && (projectileComponent == null || projectileComponent.Owner != Owner))
        {
            if (receiver != null)
            {
                receiver.TakeDamage(this, damageAmount * Time.deltaTime, GlobalData.DamageType.CONSTANT, Vector3.zero);
            }
        }
    }

    /// <summary>
    /// Calculates and sets the length of the beam. Also uses the beam length calculation to check and see if the beam needs to deal damage.
    /// </summary>
    /// <param name="start">The starting position of the beam.</param>
    /// <param name="direction">The direction of the beam.</param>
    private void FireBeamInDirection(Vector3 start, Vector3 direction, int layerMask)
    {
        _currentFiringDirection = direction;
        _currentLayerMask = layerMask;

        _line.positionCount = 2;
        _line.SetPosition(0, start);

        _beamStart.transform.position = start;
        _beamEnd.transform.position = start;

        Vector3 end = Vector3.zero;
        RaycastHit2D hit = Physics2D.Raycast(start, direction, 100, layerMask);
        if (hit)
        {
            end = hit.point;
            _lastHitCollider = hit.collider;
        }
        else
        {
            end = transform.position + (direction * 100);
            _lastHitCollider = null;
        }

        _beamStartPS.Play();
        //_beamEndPS.Play();

        _beam.SetActive(true);

        _currentlyFiring = true;
        Timing.RunCoroutine(StartBeamEffect(end, hit), "Start Beam Effect");
    }

    /// <summary>
    /// Starts the beam effecty by having the end of the beam travel to the desired final position (either the object it should hit, or its max distance), instead of having the entire beam suddenly appear.
    /// </summary>
    /// <param name="finalEndPosition">The position for the end of the beam.</param>
    /// <param name="beamCollisionRaycast">The raycast performed to determine where the beam should end.</param>
    /// <returns></returns>
    private IEnumerator<float> StartBeamEffect(Vector3 finalEndPosition, RaycastHit2D beamCollisionRaycast)
    {
        while (Vector3.Distance(_beamEnd.transform.position, finalEndPosition) > 0.05f)
        {
            _beamEnd.transform.position = Vector3.MoveTowards(_beamEnd.transform.position, finalEndPosition, BeamMovementSpeed * Time.deltaTime);
            _line.SetPosition(1, _beamEnd.transform.position);
            
            if (_collider != null)
            {
                float currentBeamLength = Vector3.Distance(_beamStart.transform.position, _beamEnd.transform.position);
                float xColliderSize = currentBeamLength + BEAM_END_OFFSET;

                _collider.size = new Vector2(xColliderSize * 2, _collider.size.y);
                _collider.transform.localPosition = new Vector2(_beamStart.transform.localPosition.z + xColliderSize, _collider.transform.localPosition.y);
            }

            yield return 0f;
        }

        _beamEnd.transform.position = finalEndPosition;
        _line.SetPosition(1, finalEndPosition);

        _beamEndPS.Play();

        _beamStart.transform.LookAt(_beamEnd.transform.position);
        _beamEnd.transform.LookAt(_beamStart.transform.position);

        if (_collider != null)
        {
            float xColliderSize = beamCollisionRaycast.collider != null ? beamCollisionRaycast.distance + BEAM_END_OFFSET : 100 + BEAM_END_OFFSET;
            _collider.size = new Vector3(xColliderSize * 2, _collider.size.y);
            _collider.transform.localPosition = new Vector3(_beamStart.transform.localPosition.z + xColliderSize, _collider.transform.localPosition.y);
        }

        float distance = Vector3.Distance(_beamStart.transform.position, finalEndPosition);
        _line.sharedMaterial.mainTextureScale = new Vector2(distance / TEXTURE_LENGTH_SCALE, 1);
        _line.sharedMaterial.mainTextureOffset -= new Vector2(Time.deltaTime * TEXTURE_SCROLL_SPEED, 0);

        BeamStarted?.Invoke();
        yield return 0f;
    }

    /// <summary>
    /// Finishes the beam effect by having the start of the beam travel to the end, instead of having the entire beam suddenly disappear.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> FinishBeamEffect()
    {
        Vector3 beamStartingPosition = _beamStart.transform.position;
        float currentBeamLength = Vector3.Distance(_beamStart.transform.position, _beamEnd.transform.position);

        while (currentBeamLength > 0.05f)
        {
            _beamStart.transform.position = Vector3.MoveTowards(_beamStart.transform.position, _beamEnd.transform.position, BeamMovementSpeed * Time.deltaTime);
            _line.SetPosition(0, _beamStart.transform.position);
            currentBeamLength = Vector3.Distance(_beamStart.transform.position, _beamEnd.transform.position);

            if (_collider != null)
            {
                float xColliderSize = currentBeamLength + BEAM_END_OFFSET;
                _collider.size = new Vector2(xColliderSize * 2, _collider.size.y);
                _collider.transform.localPosition = new Vector2(_beamStart.transform.localPosition.z + xColliderSize, _collider.transform.localPosition.y);
            }

            yield return 0f;
        }

        _beamStartPS.Stop();
        _beamEndPS.Stop();

        _beam.SetActive(false);

        _collider.transform.localPosition = _baseColliderPosition;
        _collider.size = _baseColliderSize;

        _currentlyFiring = false;
        BeamFinished?.Invoke();
        yield return 0f;
    }
}
