﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingRocket : Rocket
{
    /// <summary>
    /// A delegate for notifications that the homing rocket has hit something.
    /// </summary>
    public delegate void NotifyRocketHit(HomingRocket rocket);

    /// <summary>
    /// Notifies subscribers that the rocket has hit something.
    /// </summary>
    public event NotifyRocketHit Hit;


    /// <summary>
    /// How many degrees should the rocket be able to turn within the span of a second.
    /// </summary>
    public float TurnSpeed;

    /// <summary>
    /// How long should the rocket travel along it's initial velocity before the homing functionality begins.
    /// </summary>
    public float TimeBeforeHomingStarts;

    /// <summary>
    /// The target the rocket is homed in on.
    /// </summary>
    [HideInInspector]
    public GameObject Target;


    /// <summary>
    /// The current angle of the rocket.
    /// </summary>
    private Vector3 _currentAngle;

    /// <summary>
    /// How long has the rocket been flying for.
    /// </summary>
    private float _currentElapsedTime;


    protected override void Awake()
    {
        base.Awake();
        Health = 50;
    }

    //Update is called once per frame
    protected override void Update()
    {
        _currentElapsedTime += Time.deltaTime;
        if (_currentElapsedTime >= TimeBeforeHomingStarts)
        {
            RotateToTarget();
        }
        base.Update();
	}


    public override void Kill()
    {
        Hit?.Invoke(this);
        base.Kill();
    }


    /// <summary>
    /// Handles the homing rocket's collision. Checks whether the rocket has collided with a layer that should trigger it,
    /// (ensuring that layer does not belong to its owner), and deals damage and detonates if that is the case.
    /// </summary>
    /// <param name="collider">The collider that triggered the collision check.</param>
    /// <param name="isTrigger">Whether the collider is a trigger.</param>
    public override void HandleCollision(Collider2D collider, bool isTrigger)
    {
        if (LayerIsAcceptedLayer(collider.gameObject.layer, isTrigger) && collider.gameObject.layer != Owner.gameObject.layer)
        {
            base.HandleCollision(collider, isTrigger);
            Kill();
        }
    }

    /// <summary>
    /// Rotates the rocket towards the current position of its target. The rocket rotates at TurnSpeed number
    /// of degrees per second.
    /// </summary>
    private void RotateToTarget()
    {
        float angleToTarget = Vector2.SignedAngle(transform.right, Target.transform.position - transform.position);
        Debug.DrawRay(transform.position, transform.right * 5.0f, Color.red);
        Debug.DrawRay(transform.position, (Target.transform.position - transform.position) * 5.0f, Color.blue);
        float amountToRotateThisFrame = TurnSpeed * Time.deltaTime;

        Vector3 newAngle = _currentAngle;
        if (Mathf.Sign(angleToTarget) == 1)
        {
            newAngle = new Vector3(0, 0, transform.rotation.eulerAngles.z + amountToRotateThisFrame);
        }
        else
        {
            newAngle = new Vector3(0, 0, transform.rotation.eulerAngles.z - amountToRotateThisFrame);
        }

        if (transformRigidbody != null)
        {
            transformRigidbody.MoveRotation(Quaternion.Euler(newAngle));
        }
        else
        {
            transform.rotation = Quaternion.Euler(newAngle);
        }

        _currentAngle = newAngle;
    }
}
