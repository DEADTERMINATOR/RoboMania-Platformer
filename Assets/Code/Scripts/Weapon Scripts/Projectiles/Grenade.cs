﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;

public class Grenade : ArcedProjectile
{
    /// <summary>
    /// The maximum amount of knockback the player can receive from the explosion (if they absorb 2/3rds or more of the explosion).
    /// </summary>
    public Vector2 MaxKnockback;


    /// <summary>
    /// The actual amount of knockback the player will received from the explosion (adjusted if they absorbed less than 2/3rds of the explosion).
    /// </summary>
    private Vector2 _appliedKnockback;

    /// <summary>
    /// Reference to the collider that represents the explosion radius to activate and deactivate it, as well as to determine the amount of explosion
    /// the player absorbed (if any).
    /// </summary>
    private CircleCollider2D _explosionCollider;


	// Use this for initialization
	protected override void Awake()
    {
        base.Awake();
        _explosionCollider = transform.Find("Explosion Collider").GetComponent<CircleCollider2D>();
        _explosionCollider.enabled = false;
	}


    public override void Kill()
    {
        HandleExplosion();
        _explosionCollider.enabled = false;
        base.Kill();
    }


    /// <summary>
    /// Handles the grenade's collision, including whether the collision should even be accepted,
    /// and destroying the projectile, among other things. This method handles both trigger and non-trigger colliders.
    /// </summary>
    /// <param name="collider">The 2D collider that the projectile collided with.</param>
    /// <param name="isTrigger">Whether the collider was a trigger.</param>
    public override void HandleCollision(Collider2D collider, bool isTrigger)
    {
        if (_explosionCollider == null)
        {
            Awake();
        }

        Projectile projectileComponent = collider.gameObject.GetComponent<Projectile>(); //Try to get a projectile component from the collision result to see if we collided with another projectile.
        if (collider.gameObject != Owner && LayerIsAcceptedLayer(collider.gameObject.layer, isTrigger) && collider.gameObject.layer != Owner.layer && (projectileComponent == null || projectileComponent.Owner != Owner) && !isDead)
        {
            HandleExplosion();
            bypassCollisionCheck = true;
            base.HandleCollision(collider, isTrigger);
        }
    }


    /// <summary>
    /// Handles the exploding of the grenade, damaging any nearby characters if necessary.
    /// It also checks how far away from the explosion center the affected targets are,
    /// and adjusts damage and knockback if necessary.
    /// </summary>
    /// <param name="collider">The collider that triggered the explosion.</param>
    protected void HandleExplosion()
    {
        _explosionCollider.enabled = true;
        Collider2D[] collidersToCheckFor = new Collider2D[0];

        if (Owner.layer == GlobalData.ENEMY_LAYER)
        {
            //The owner of the projectile is an enemy, so the only target they want to hurt is the player.
            collidersToCheckFor = Physics2D.OverlapCircleAll(transform.position, _explosionCollider.radius * _explosionCollider.transform.root.transform.localScale.x, 1 << LayerMask.NameToLayer("Player"));
        }
        else if (Owner.layer == GlobalData.PLAYER_LAYER)
        {
            //The owner is the player, so they will want to hurt any enemies.
            collidersToCheckFor = Physics2D.OverlapCircleAll(transform.position, _explosionCollider.radius * _explosionCollider.transform.root.transform.localScale.x, 1 << LayerMask.NameToLayer("RoboticEnemy") | 1 << LayerMask.NameToLayer("Enemy"));
        }

        foreach (Collider2D overlappedCollider in collidersToCheckFor)
        {
            float distanceFromExplosion = Vector2.Distance(transform.position, overlappedCollider.transform.position); //The distance the character caught in the explosion is from the center.
            float percentOfExplosionAbsorbed = 100 - distanceFromExplosion / _explosionCollider.radius * 100f; //The percentage of the explosion the character absorbed (based on how far away from the center they were).
            Vector3 directionToCharacter = overlappedCollider.transform.position - transform.position; //Where the character is in relation to the explosion

            if (percentOfExplosionAbsorbed < 66 && percentOfExplosionAbsorbed >= 33)
            {
                //If the character only absorbed between 33% and 65% of the explosion, cut damage and knockback in half.
                modifiedDamageAmount = damageAmount / 2;
                _appliedKnockback = new Vector2(MaxKnockback.x * 0.5f, MaxKnockback.y * 0.5f);
            }
            else if (percentOfExplosionAbsorbed < 33)
            {
                //If the character only absorbed 32% or less of the explosion, cut damage and knockback 2/3rds.
                modifiedDamageAmount = damageAmount / 3;
                _appliedKnockback = new Vector2(MaxKnockback.x * 0.33f, MaxKnockback.y * 0.33f);
            }
            else
            {
                //Otherwise, the character absorbed the majority of the blast, so give them the full force (damage does not need to be adjusted as it is already at its base value).
                _appliedKnockback = MaxKnockback;
            }

            if (directionToCharacter.x < 0) //The character is to the left of the explosion.
            {
                _appliedKnockback.x *= -1;
            }
            if (directionToCharacter.y < 0) //The character is below the explosion.
            {
                _appliedKnockback.y *= -1;
            }

            IDamageReceiver objectToDamage = overlappedCollider.gameObject.GetComponent<IDamageReceiver>(); //Only characters will be picked up by the physics overlap check.
            if (objectToDamage == null)
            {
                objectToDamage = overlappedCollider.transform.parent.GetComponent<IDamageReceiver>(); //If the character component wasn't on the object we collided with, it's very likely on the parent.
            }

            if (objectToDamage != null)
            {
                objectToDamage.TakeDamage(this, modifiedDamageAmount, GlobalData.DamageType.PROJECTILE,Vector3.zero);
                if (objectToDamage is PlayerMaster)
                    GlobalData.Player.Movement.SetDamageKnockback(_appliedKnockback);

                ignoreDamage = true;
            }
        }
    }
}
