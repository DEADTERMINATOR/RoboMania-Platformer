﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricShock : Projectile
{
    /// <summary>
    /// How much stun power does the electric shock have. Different enemies will have different thresholds for the amount they
    /// can absorb before they get stunned.
    /// </summary>
    public float StunPower;


    /// <summary>
    /// Reference to the animator component for this projectile.
    /// </summary>
    private Animator _anim;


    //Use this for initialization
    protected override void Awake()
    {
        damageAmount = 0; //Electric shocks do no damage, zero out damage just in case.
        base.Awake();

        if (type == AssetType.SPRITE)
        {
            _anim = GetComponentInChildren<Animator>();
            _anim.SetBool("On", true);
        }
	}

    /// <summary>
    /// Handles the electric shot's collision, applying the stun power if the collided object is a character. 
    /// It then continues on to the base version. This method handles both trigger and non-trigger colliders.
    /// </summary>
    /// <param name="collider">The 2D collider that the projectile collided with.</param>
    /// <param name="isTrigger">Whether the collider was a trigger.</param>
    public override void HandleCollision(Collider2D collider, bool isTrigger)
    {
        IDamageReceiver receiver = collider.GetComponent<IDamageReceiver>();
        Projectile projectileComponent = collider.gameObject.GetComponent<Projectile>(); //Try to get a projectile component from the collision result to see if we collided with another projectile.

        if (collider.gameObject != Owner
            && collider.gameObject.layer != Owner.layer
            && (projectileComponent == null || projectileComponent.Owner != Owner)
            && !isDead)
        {
            IStunnable hitObject = collider.gameObject.GetComponent<IStunnable>(); //Try to get the stunnable component, if it has one.
            if (hitObject == null)
            {
                //If the character component is null, check if it is in the parent.
                hitObject = collider.gameObject.GetComponentInParent<IStunnable>();
            }
            
            if (hitObject != null)
            {
                //It had a Character (or derived) component, so it can be stunned.
                hitObject.SetStunned(StunPower, this, Owner);
            }
            ignoreDamage = true;
        }
        base.HandleCollision(collider, isTrigger);
    }
}
