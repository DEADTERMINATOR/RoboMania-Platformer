﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class WaspStinger : Projectile
{
    private SkeletonAnimation _skeleton;

    protected override void Awake()
    {
        base.Awake();
        _skeleton = GetComponentInChildren<SkeletonAnimation>();
        performInitialScaling = false;
        IgnoreDeath = true;
    }

    protected override void Update()
    {
        base.Update();

        if (_skeleton.AnimationState.GetCurrent(0).Animation.Name == "Hit" && _skeleton.AnimationState.GetCurrent(0).IsComplete)
        {
            Kill();
        }
    }

    public override void Fire(Vector3 angle, float damage, GameObject owner)
    {
        base.Fire(angle, damage, owner);

        if (_skeleton == null)
        {
            _skeleton = GetComponentInChildren<SkeletonAnimation>();
        }

        _skeleton.AnimationState.SetAnimation(0, "Fire", true);
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        if (LayerIsAcceptedLayer(collision.collider.gameObject.layer, false))
        {
            SetImpactEffectPositionAndRotation(collision.collider);
            _skeleton.AnimationState.SetAnimation(0, "Hit", false);
            base.OnCollisionEnter2D(collision);
            vectorSpeed.x = 0;
        }
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        if (LayerIsAcceptedLayer(collider.gameObject.layer, true))
        {
            SetImpactEffectPositionAndRotation(collider);
            _skeleton.AnimationState.SetAnimation(0, "Hit", false);
            base.OnTriggerEnter2D(collider);
            vectorSpeed.x = 0;
        }
    }

    private void SetImpactEffectPositionAndRotation(Collider2D hitCollider = null)
    {
        soundManager?.PlayHitSound();

        if (projectileKilledBy || hitCollider) //There is a specific collider we want to use to calculate the impact effect position from.
        {
            Collider2D colliderToUse = hitCollider != null ? hitCollider : projectileKilledBy;
            if (colliderToUse.gameObject.layer != GlobalData.PROJECTILE_LAYER)
            {
                RaycastHit2D hitToUse = FireRayAtDesiredImpactSurface(colliderToUse);
                if (hitToUse.collider != null)
                {
                    transform.GetChild(0).localRotation = Quaternion.identity;

                    //Offset the surface normal by the position of the projectile and then subtract that from the projectile's position
                    //to create a vector that we can rotate the impact effect towards. Zero out the x and y rotations since this is 2D and we don't
                    //care about those.
                    Quaternion surfaceRotation = Quaternion.LookRotation(transform.position - new Vector3(hitToUse.normal.x + transform.position.x,
                        hitToUse.normal.y + transform.position.y, transform.position.z), Vector3.forward);

                    surfaceRotation.x = 0.0f;
                    surfaceRotation.y = 0.0f;

                    //Assign the projectile position and the calculated rotation to the impact effect.

                    //TODO: Find a way to consistently spawn the impact effect on the ground at the same Y-axis value without breaking other stuff.
                    transform.position = hitToUse.point;
                    transform.rotation = surfaceRotation;
                }
                else
                {
                    Debug.LogWarning("Raycasting against the desired collider for the impact effect did not return a valid raycast");
                }
            }
        }
    }
}
