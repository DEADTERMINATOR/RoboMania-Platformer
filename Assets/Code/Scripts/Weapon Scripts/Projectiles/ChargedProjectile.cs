﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargedProjectile : Projectile
{
    /// <summary>
    /// The total amount of charge the shot received (as a percent of its possible maximum).
    /// </summary>
    [HideInInspector]
    [Range(0, 1)]
    public float PercentCharged;


    /// <summary>
    /// The multiplier applied to the base damage based on the amount of time the projectile was charged.
    /// </summary>
    public virtual float DamageMultiplier { get { return _damageMultiplier; } set { _damageMultiplier = value; } }


    private float _damageMultiplier;


    protected override void Awake()
    {
        base.Awake();
        modifiedDamageAmount = damageAmount * DamageMultiplier;
    }
}
