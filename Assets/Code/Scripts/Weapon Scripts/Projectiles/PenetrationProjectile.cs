﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MainModule = UnityEngine.ParticleSystem.MainModule;
using EmissionModule = UnityEngine.ParticleSystem.EmissionModule;
using MinMaxCurve = UnityEngine.ParticleSystem.MinMaxCurve;
using Burst = UnityEngine.ParticleSystem.Burst;

public class PenetrationProjectile : ChargedProjectile
{
    //Various constant values used in tweaking the particle system based on how charged the shot is.
    private const float TRAIL_START_SIZE_LOWER_BOUND_RANGE = 0.1f;
    private const float SWIRLY_START_SIZE_LOWER_BOUND_RANGE = 5;
    private const float STARS_START_SIZE_LOWER_BOUND_RANGE = 0.2f;

    private const float STARS_START_SIZE_UPPER_BOUND_RANGE = 0.5f;

    private const float TRAIL_RATE_OVER_DISTANCE_MIN = 15;
    private const float TRAIL_RATE_OVER_DISTANCE_MAX = 75;

    private const float SWIRLY_RATE_OVER_DISTANCE_MIN = 6;
    private const float SWIRLY_RATE_OVER_DISTANCE_MAX = 18;

    private const float SWIRLY_SPEED_RANGE = 4;

    private const float STARS_BURSTS_RANGE = 20;

    /* The maximum amount of time a collision with an object on the obstacle layer can be before the projectile is killed.
       This is to prevent reflections that reflect the projectile into an obstacle collider without the opportunity to exit
       then re-enter and register another collision, and thus travel through the collider. */
    private const float MAXIMUM_ACCEPTABLE_OBSTACLE_COLLISION_TIME = 0.075f;


    /// <summary>
    /// Whether the projectile is allowed to reflect off surfaces (whether it's allowed is based on the rarity of the gunchip spawning the projectile).
    /// </summary>
    public bool ReflectAllowed = false;

    /// <summary>
    /// The current rate over distance for the beam part of the projectile.
    /// </summary>
    [HideInInspector]
    public float BeamRateOverDistance;

    /// <summary>
    /// The current rate over distance for the swirly effect part of the projectile.
    /// </summary>
    [HideInInspector]
    public float SwirlyRateOverDistance;


    /// <summary>
    /// The maximum number of objects the projectile can hit before it dies.
    /// </summary>
    public int TotalPenetration { get { return _totalPenetration; } set { _totalPenetration = value; _currentRemainingPenetration = value; } }

    /// <summary>
    /// The multiplier applied to the base damage based on the amount of time the projectile was charged.
    /// </summary>
    public override float DamageMultiplier { get { return base.DamageMultiplier; } set { base.DamageMultiplier = value; _previouslyHitEnemies.Clear(); } }


    /// <summary>
    /// The remaining amount of charge the projectile possesses.
    /// </summary>
    private float _currentRemainingCharge;

    /// <summary>
    /// The total number of objects the projectile can penetrate.
    /// </summary>
    private int _totalPenetration;

    /// <summary>
    /// The remaining amount of penetration the projectile possesses.
    /// </summary>
    private int _currentRemainingPenetration;

    /// <summary>
    /// Holds the enemies this shot has already hit, so they aren't hit multiple times.
    /// </summary>
    private List<GameObject> _previouslyHitEnemies = new List<GameObject>();

    /// <summary>
    /// The particle system's main module for the beam.
    /// </summary>
    private MainModule _beamMain;

    /// <summary>
    /// The particle system's main module for the swirly effect.
    /// </summary>
    private MainModule _swirlyMain;

    /// <summary>
    /// The particle system's main module for the stars effect.
    /// </summary>
    private MainModule _starsMain;

    /// <summary>
    /// The particle system's emission module for the beam.
    /// </summary>
    private EmissionModule _beamEmission;

    /// <summary>
    /// The particle system's emission module for the swirly effect.
    /// </summary>
    private EmissionModule _swirlyEmission;

    /// <summary>
    /// The particle system's emission module for the stars effect.
    /// </summary>
    private EmissionModule _starsEmission;

    /// <summary>
    /// The min max curve for the start size of the beam.
    /// </summary>
    private MinMaxCurve _beamStartSize;

    /// <summary>
    /// The min max curve fore the start size of the swirly effect.
    /// </summary>
    private MinMaxCurve _swirlyStartSize;

    /// <summary>
    /// The min max curve for the start speed of the swirly effect.
    /// </summary>
    private MinMaxCurve _swirlyStartSpeed;

    private MinMaxCurve _starsStartSize;

    /// <summary>
    /// The burst effect for the stars.
    /// </summary>
    private Burst _starsBursts;

    /// <summary>
    /// The trail renderer for the following trail effect.
    /// </summary>
    private TrailRenderer _trailRenderer;

    private bool _particleModulesSetUp = false;

    private float _totalCollisionTime = 0;

    
    protected override void Awake()
    {
        base.Awake();

        if (!_particleModulesSetUp)
        {
            SetUpParticleSystemModules();
        }
    }


    public void SetParticleSystemModuleValues()
    {
        if (!_particleModulesSetUp)
        {
            SetUpParticleSystemModules();
        }

        _beamStartSize.constantMin = _beamStartSize.constantMin + TRAIL_START_SIZE_LOWER_BOUND_RANGE * PercentCharged;

        _swirlyStartSize.constant = _swirlyStartSize.constant + SWIRLY_START_SIZE_LOWER_BOUND_RANGE * PercentCharged;
        _swirlyStartSpeed.constant = _swirlyStartSpeed.constant + SWIRLY_SPEED_RANGE * PercentCharged;

        _starsStartSize.constantMin = _starsStartSize.constantMin + STARS_START_SIZE_LOWER_BOUND_RANGE * PercentCharged;
        _starsStartSize.constantMax = _starsStartSize.constantMax + STARS_START_SIZE_UPPER_BOUND_RANGE * PercentCharged;

        _beamEmission.rateOverDistanceMultiplier = (TRAIL_RATE_OVER_DISTANCE_MAX - TRAIL_RATE_OVER_DISTANCE_MIN) * PercentCharged;
        _swirlyEmission.rateOverDistanceMultiplier = (SWIRLY_RATE_OVER_DISTANCE_MAX - SWIRLY_RATE_OVER_DISTANCE_MIN) * PercentCharged;

        _starsBursts.minCount = (short)(_starsBursts.minCount + Mathf.CeilToInt(STARS_BURSTS_RANGE * PercentCharged));
        _starsBursts.maxCount = (short)(_starsBursts.maxCount + Mathf.CeilToInt(STARS_BURSTS_RANGE * PercentCharged));
    }

    private void SetUpParticleSystemModules()
    {
        ParticleSystem[] particleSystems = GetComponentsInChildren<ParticleSystem>();

        _beamMain = particleSystems[0].main;
        _swirlyMain = particleSystems[1].main;
        _starsMain = particleSystems[2].main;

        _beamEmission = particleSystems[0].emission;
        _swirlyEmission = particleSystems[1].emission;
        _starsEmission = particleSystems[2].emission;

        _beamStartSize = _beamMain.startSize;
        _swirlyStartSize = _swirlyMain.startSize;
        _starsStartSize = _starsMain.startSize;

        _swirlyStartSpeed = _swirlyMain.startSpeed;
        _starsBursts = _starsEmission.GetBurst(0);

        _trailRenderer = GetComponentInChildren<TrailRenderer>();

        _particleModulesSetUp = true;
    }

    protected virtual void OnCollisionStay2D(Collision2D collision)
    {
        //HandleCollisionStay(collision.collider);
    }

    protected virtual void OnTriggerStay2D(Collider2D collider)
    {
        //HandleCollisionStay(collider);
    }

    protected virtual void OnCollisionExit2D(Collision2D collision)
    {
        HandleCollisionExit();
    }

    protected virtual void OnTriggerExit2D(Collider2D collision)
    {
        HandleCollisionExit();
    }

    public override void HandleCollision(Collider2D collider, bool isTrigger)
    {
        modifiedDamageAmount = damageAmount * DamageMultiplier;

        if (collider.gameObject.layer == GlobalData.OBSTACLE_LAYER)
        {
            if (ReflectAllowed && _currentRemainingPenetration > 1.0f)
            {
                RaycastHit2D colliderHit = Physics2D.Raycast(transform.position, transform.right, 5.0f, GlobalData.OBSTACLE_LAYER_SHIFTED);
                if (colliderHit)
                {
                    //Find the reflected velocity using the current velocity (which is equal to the right axis of the transform, since projectiles only travel to the right relative to their rotation)
                    //and the normal of the collided surface.
                    Vector3 reflectedVector = -2 * Vector3.Dot(transform.right, colliderHit.normal) * (Vector3)colliderHit.normal + transform.right;
                    float reflectedAngle = Mathf.Atan2(-reflectedVector.x, reflectedVector.y) * Mathf.Rad2Deg;
                    ProjectileTransform.position = colliderHit.point;
                    ProjectileTransform.rotation = Quaternion.AngleAxis(reflectedAngle + 90, Vector3.forward);

                    --_currentRemainingPenetration;
                    _beamEmission.rateOverDistanceMultiplier = TRAIL_RATE_OVER_DISTANCE_MIN * _currentRemainingPenetration;
                    _swirlyEmission.rateOverDistanceMultiplier = SWIRLY_RATE_OVER_DISTANCE_MIN * _currentRemainingPenetration;

                    /*
                    //Do a check for what is in front of the projectile on its new trajectory. If it can't even travel 1 unit without hitting another obstacle,
                    //just kill the projectile because it runs the risk of entering into a collider.
                    Collider2D projectileCollider = GetComponent<Collider2D>();
                    Ray2D raycastRay = new Ray2D((Vector2)ProjectileTransform.position + projectileCollider.offset, ProjectileTransform.right);
                    RaycastHit2D futureCollisionRaycast = Physics2D.Raycast(raycastRay.origin, raycastRay.direction, projectileCollider.bounds.extents.x + 1.0f, GlobalData.OBSTACLE_LAYER_SHIFTED);
                    Debug.DrawRay(raycastRay.origin, raycastRay.direction * (projectileCollider.bounds.extents.x + 1.0f), Color.red, 3.0f);
                    if (futureCollisionRaycast)
                        Kill(collider);
                    */
                }
            }
            else
            {
                Kill(collider);
            }
        }
        else if (collider.gameObject.layer == GlobalData.ENEMY_LAYER)
        {
            EnemyCollider enemyColliderRef = collider.GetComponent<EnemyCollider>();
            if (enemyColliderRef != null && !_previouslyHitEnemies.Contains(enemyColliderRef.transform.root.gameObject))
            {
                if (_currentRemainingPenetration > 1)
                {
                    enemyColliderRef.TakeDamage(this, modifiedDamageAmount, GlobalData.DamageType.PROJECTILE, transform.position);
                    _previouslyHitEnemies.Add(enemyColliderRef.transform.root.gameObject);
                    PlayImpactEffect(collider);
                }
                else
                {
                    enemyColliderRef.TakeDamage(this, modifiedDamageAmount, GlobalData.DamageType.PROJECTILE, transform.position);
                    Kill(collider);
                }

                --_currentRemainingPenetration;
                _beamEmission.rateOverDistanceMultiplier = TRAIL_RATE_OVER_DISTANCE_MIN * _currentRemainingPenetration;
                _swirlyEmission.rateOverDistanceMultiplier = SWIRLY_RATE_OVER_DISTANCE_MIN * _currentRemainingPenetration;
            }
        }
        else
        {
            base.HandleCollision(collider, isTrigger);
        }
    }

    protected virtual void HandleCollisionStay(Collider2D collider)
    {
        _totalCollisionTime += Time.deltaTime;
        if (ReflectAllowed && collider.gameObject.layer == GlobalData.OBSTACLE_LAYER && _totalCollisionTime > MAXIMUM_ACCEPTABLE_OBSTACLE_COLLISION_TIME)
        {
            Kill(collider);
        }
    }

    protected virtual void HandleCollisionExit()
    {
        _totalCollisionTime = 0f;
    }

    public override void Kill()
    {
        _trailRenderer.Clear();
        base.Kill();
    }

    public override void Kill(Collider2D killedBy)
    {
        _trailRenderer.Clear();
        base.Kill(killedBy);
    }
}
