﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncingGrenade : Grenade
{
    /// <summary>
    /// A delegate for notifications that the bouncing grenade has detonated.
    /// </summary>
    public delegate void NotifyGrenadeDetonated(BouncingGrenade grenade);

    /// <summary>
    /// Notifies subscribers that the bouncing grenade has detonated.
    /// </summary>
    public event NotifyGrenadeDetonated Detonated;


    /// <summary>
    /// The maximum number of bounces the grenade can do before detonating.
    /// </summary>
    public int MaxNumberOfBounces;

    /// <summary>
    /// The layers that the grenade is allowed to bounce off.
    /// </summary>
    public LayerMask BounceableSurfaces;


    /// <summary>
    /// The mask denoting the layers that will trigger a "kill" of the projectile, irregardless of how much health the projectile has left.
    /// </summary>
    [SerializeField]
    protected LayerMask triggerableSurfaces;


    /// <summary>
    /// The number of times the grenade has bounced so far.
    /// </summary>
    private int _currentNumberOfBounces = 0;


    public override void Kill()
    {
        _currentNumberOfBounces = 0;
        Detonated?.Invoke(this);

        base.Kill();
    }


    /// <summary>
    /// Handles the grenade's collision by checking if the surface is a bounceable or triggerable surface. If it's a
    /// bounceable surface, the grenade's bounce counter is incremented and if the new bounce count matches the max
    /// accepted number of bounces, the grenade is detonated. Otherwise, the force of the grenade is reduced.
    /// If the surface if a triggerable surface, the grenade is immediately detonated.
    /// </summary>
    /// <param name="collider">The 2D collider that the projectile collided with.</param>
    /// <param name="isTrigger">Whether the collider was a trigger.</param>
    public override void HandleCollision(Collider2D collider, bool isTrigger)
    {
        bool trigger = false;
        if (((1 << collider.gameObject.layer) & BounceableSurfaces) != 0)
        {
            ++_currentNumberOfBounces;
            if (_currentNumberOfBounces >= MaxNumberOfBounces)
            {
                trigger = true;
            }
        }
        if (trigger || ((1 << collider.gameObject.layer) & triggerableSurfaces) != 0)
        {
            Detonated?.Invoke(this);

            base.HandleCollision(collider, isTrigger);
            Kill();
        }

    }
}
