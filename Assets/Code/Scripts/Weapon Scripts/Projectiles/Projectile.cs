﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;
using static GlobalData;
using Characters.Player;

public class Projectile : PooledItem, IDamageGiver, IDamageReceiver
{
    #region Particle Element Struct
    /// <summary>
    /// Represents an element of a particle system that needs to be modified to provide the damage
    /// flash effect for a particle effect projectile.
    /// </summary>
    public struct ParticleElement
    {
        /// <summary>
        /// Reference to the particle system.
        /// </summary>
        public ParticleSystem ParticleSystem;

        /// <summary>
        /// The starting color of the main element of the particle system.
        /// </summary>
        public Color StartingColor;
        

        public ParticleElement(ParticleSystem particleSystem, Color startingColor)
        {
            ParticleSystem = particleSystem;
            StartingColor = startingColor;
        }
    }
    #endregion

    #region Enums + Constants
    /// <summary>
    /// An enum holding the different types of art assets a projectile could be.
    /// </summary>
    public enum AssetType { SPRITE, PARTICLE_EFFECT, MODEL }

    protected const float MIN_PROJECTILE_SCALE_LERP_TIME = 0.05f;
    protected const float MAX_PROJECTILE_SCALE_LERP_TIME = 0.45f;

    protected const float PROJECTILE_SCALE_LERP_TIME_RANGE = 0.4f;
    protected const float MIN_PROJECTILE_SPEED = 2.5f;

    /// <summary>
    /// The color for projectile to flash when they take damage.
    /// </summary>
    public readonly Vector4 DAMAGE_FLASH_COLOR = new Vector4(1, 0, 0, 1);
    #endregion

    #region Static Variables
    /// <summary>
    /// Any non-trigger colliders that belong to layers in this accepted list will be responded to.
    /// </summary>
    private static HashSet<int> ACCEPTED_NON_TRIGGER_LAYERS;

    /// <summary>
    /// Any trigger colliders that belong to layers in this accepted list will be responded to.
    /// </summary>
    private static HashSet<int> ACCEPTED_TRIGGER_LAYERS;

    /// <summary>
    /// Whether the layer sets have been populated.
    /// </summary>
    private static bool LAYER_SETS_POPULATED = false;
    #endregion


    /// <summary>
    /// A delegate for notifications that the projectile has been destroyed.
    /// </summary>
    public delegate void NotifyProjectileDestroyed(Projectile projectile);

    /// <summary>
    /// Notifies subscribers that the projectile has been destroyed.
    /// </summary>
    public event NotifyProjectileDestroyed Destroyed;


    #region Public Variables + Getters
    /// <summary>
    /// The speed of the projectile as a unit value.
    /// </summary>
    public float ProjectileSpeed;

    /// <summary>
    /// The amount of health this projectile has. Projectiles can be destroyed by other projectiles provided the
    /// appropriate amount of damage is done. For default, the health is set to 1 so most projectiles should be
    /// able to be destroyed with one shot.
    /// </summary>
    [HideInInspector]
    public float Health = 1;

    /// <summary>
    /// A non sticky setting to ignore the next kill on impact (Your code MUST!!! call kill)
    /// </summary>
    [HideInInspector]
    public bool IgnoreDeath;

    /// <summary>
    /// The initial position the projectile was fired from.
    /// </summary>
    [HideInInspector]
    public Vector3 InitialPositionVector = Vector3.zero;

    /// <summary>
    /// The initial position with any appropriate spacing applied. This is used in conjunction with the initial position vector to
    /// calculate how far and in what direction a projectile should travel if it need to space itself out from it's neighbours (a
    /// functionality that is currently only present in straight shot type projectiles).
    /// </summary>
    [HideInInspector]
    public Vector3 FinalSpacedVector = Vector3.zero;

    /// <summary>
    /// Whether the projectile should play its impact effect on collision with an object.
    /// </summary>
    [HideInInspector]
    public bool PlayImpactFX = true;

    /// <summary>
    /// The prefab for this projectile.
    /// </summary>
    [HideInInspector]
    public GameObject ProjectilePrefab;

    public AssetType Type { get { return type; } }

    /// <summary>
    /// The change in position for the current frame.
    /// </summary>
    public Vector3 DeltaVector { get; private set; }

    /// <summary>
    /// The transform for this projectile's sprite.
    /// </summary>
    public Transform ProjectileTransform { get { return transformToOperateOn; } }

    /// <summary>
    /// The game object that currently owns the projectile.
    /// </summary>
    public GameObject Owner { get; protected set; }
    #endregion

    #region Serialized Fields
    /// <summary>
    /// The type of art asset the projectile is.
    /// </summary>
    [SerializeField]
    protected AssetType type;

    /// <summary>
    /// Number of pixels beyond the visible screen a projectile will travel and do damage.
    /// </summary>
    [SerializeField]
    protected int screenBuffer = 500;

    /// <summary>
    /// How long the Projectile will Live if not set to KillOffScreen
    /// </summary>
    [SerializeField]
    protected float killTime = 1f;

    /// <summary>
    /// Should this projectile die off screen
    /// </summary>
    [SerializeField]
    protected bool killOffScreen = true;

    /// <summary>
    /// The game objects that contains the color element to modify to indicate damage has been taken by the projectile (particle effect projectiles only).
    /// </summary>
    [SerializeField]
    protected List<GameObject> particleElementsToModifyForDamageIndication;

    /// <summary>
    /// The sound manager that will handle the various sounds of this projectile.
    /// </summary>
    [SerializeField]
    protected ProjectileSound soundManager;

    /// <summary>
    /// The transform that movement and rotations should be performed on for this projectile.
    /// </summary>
    [Tooltip("The transform that movement and rotations should be performed on for this projectile.")]
    [SerializeField]
    protected Transform transformToOperateOn;

    /// <summary>
    /// The game object that represents the effect to be spawned when the projectile collides with something.
    /// </summary>
    [SerializeField]
    private GameObject _projectileImpactEffect;

    /// <summary>
    /// The game object that represents the visuals for the projectile.
    /// </summary>
    [SerializeField]
    private Transform _projectileVisualTransform;
    #endregion

    #region Protected Variables
    /// <summary>
    /// Whether the projectile asset used must be rotated around the X-axis to achieve the desired result, as opposed to the usual Z-axis.
    /// </summary>
    [SerializeField]
    protected bool projectileMustRotateAroundX;

    /// <summary>
    /// How long the projectile has been alive 
    /// </summary>
    protected float aliveTime = 0;

    /// <summary>
    /// The amount of damage this projectile will do.
    /// </summary>
    protected float damageAmount = 3;

    /// <summary>
    /// The speed of the projectile as a vector.
    /// </summary>
    protected Vector3 vectorSpeed;

    /// <summary>
    /// Is the projectile dead 
    /// </summary>
    protected bool isDead = true;

    /// <summary>
    /// The modified damage that needs to be applied if a derived type or modifier needs to apply a damage value other than the base.
    /// </summary>
    protected float modifiedDamageAmount;

    /// <summary>
    /// Is the projectile moving
    /// </summary>
    protected bool inMotion = false;

    /// <summary>
    /// This is set so the base collision method ignores the application of damage, because a derived projectile is going to handle it.
    /// Used when the damage application needs to be customized beyond the modification of the value (e.g. splash damage).
    /// </summary>
    protected bool ignoreDamage = false;

    /// <summary>
    /// Whether the various checks at the start of collision handling should be bypassed (likely because they have already been done elsewhere).
    /// </summary>
    protected bool bypassCollisionCheck;

    /// <summary>
    /// Whether the projectile should scaled up upon firing to its target scale starting from a scale of 0. Better conveys the effect of the projectile leaving the barrel of the gun.
    /// </summary>
    protected bool performInitialScaling = true;

    /// <summary>
    /// The scale the projectile should end up at after the scaling procedure.
    /// </summary>
    protected Vector3 desiredTargetScale;

    /// <summary>
    /// How lon the scalinmg procedure should take.
    /// </summary>
    protected float lerpScalingTime = 0.1f;

    /// <summary>
    /// Whether the scaling procedure has begun.
    /// </summary>
    protected bool scalingBegun = false;

    /// <summary>
    /// Reference to the particle system components of the projectile (only if the projectile is a particle effect).
    /// </summary>
    protected List<ParticleElement> particleElements;

    /// <summary>
    /// The object the projectile collided with that triggered the killing of the projectile.
    /// </summary>
    protected Collider2D projectileKilledBy;

    /// <summary>
    /// The Rigidbody2D attached to the transform being moved (if there is one).
    /// </summary>
    protected Rigidbody2D transformRigidbody;
    #endregion

    #region Private Variables
    /// <summary>
    /// The sprite renderer for this projectile.
    /// </summary>
    private SpriteRenderer _spriteRenderer;

    /// <summary>
    /// Reference to the co-routine handle for the spacing function.
    /// </summary>
    private CoroutineHandle _lerpHandle;

    /// <summary>
    /// Reference to the co-routine handle for the scaling function.
    /// </summary>
    private CoroutineHandle _lerpScaleHandle;

    /// <summary>
    /// Reference to the co-routine handle for the damage flash function.
    /// </summary>
    private CoroutineHandle _damageFlashHandle;
    #endregion

    #region Unity Methods
    // Use this for initialization
    protected virtual void Awake()
    {
        if (type == AssetType.SPRITE)
        {
            _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }
        else if (type == AssetType.PARTICLE_EFFECT)
        {
            particleElements = new List<ParticleElement>();
            foreach (GameObject particleElement in particleElementsToModifyForDamageIndication)
            {
                ParticleSystem particleSystem = particleElement.GetComponent<ParticleSystem>();
                ParticleSystem.MainModule main = particleSystem.main;

                particleElements.Add(new ParticleElement(particleSystem, main.startColor.color));
            }
        }

        modifiedDamageAmount = damageAmount;

        if (!LAYER_SETS_POPULATED)
        {
            PopulateAcceptedLayers();
        }

        transformRigidbody = transformToOperateOn.GetComponent<Rigidbody2D>();
        if (transformRigidbody == null)
        {
            Debug.LogError("The projectile prefab " + ProjectilePrefab.gameObject.name + " does not have a rigidbody2D on it.");
        }
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        aliveTime += Time.deltaTime;

        //A flag to kill projectile off screen the default 
        if (killOffScreen)
        {
            Vector3 screenpos = Camera.main.WorldToScreenPoint(ProjectileTransform.position);
            if (screenpos.x <= -screenBuffer || screenpos.y <= -screenBuffer || screenpos.x >= Screen.width + screenBuffer || screenpos.y >= Screen.height + screenBuffer)
            {
                Kill();
            }
        }
        else if (!isDead)
        {
            if(aliveTime >= killTime)
            {
                Kill();
            }
        }

        if (isDead)
        {
            aliveTime = 0;
        }

        if (inMotion && isDead)
        {
            inMotion = false;
        }

        //Test for invisible projectile from z values
        //Safe to remove if left in 
        if (!isDead && (transform.position.z > 1000 || transform.position.z < -1000) && transform.position != GlobalData.VEC3_OFFSCREEN)
        {
            Debug.LogWarning("Projectile with bad Z values found - it may be not be visible in the game window.");
        }
    }

    protected virtual void FixedUpdate()
    {
        if (inMotion)
        {
            MoveProjectile();
        }
    }

    /// <summary>
    /// Handles what should occur when a projectile collides with a non-trigger collider.
    /// </summary>
    /// <param name="collision">The collision result.</param>
    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (LayerIsAcceptedLayer(collision.collider.gameObject.layer, false))
        {
            HandleCollision(collision.collider, false);
        }
    }

    /// <summary>
    /// Handles what should happen when a projectile collides with a trigger collider.
    /// </summary>
    /// <param name="collider">The collider the projectile hit.</param>
    protected virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if (LayerIsAcceptedLayer(collider.gameObject.layer, true))
        {
            HandleCollision(collider, true);
        }
    }
    #endregion

    #region Projectile State Methods
    /// <summary>
    /// Fires the projectile.
    /// </summary>
    /// <param name="angle">The angle to fire the projectile.</param>
    public virtual void Fire(Vector3 angle, float damage, GameObject owner)
    {
        if (!LAYER_SETS_POPULATED)
        {
            PopulateAcceptedLayers();
        }

        if (ProjectilePrefab == null)
        {
            ProjectilePrefab = gameObject;
        }

        PlayImpactFX = true;
        isDead = false;
        inMotion = true;

        soundManager?.PlayShootSound();

        if (_spriteRenderer == null)
        {
            //If the projectile visuals are a sprite, we want a reference to the sprite renderer to we can disable and enable it as pooled projectiles are spawned and despawned.
            _spriteRenderer = ProjectileTransform.GetComponent<SpriteRenderer>();
        }
        else
        {
            _spriteRenderer.enabled = true;
        }

        if (particleElements == null)
        {
            particleElements = new List<ParticleElement>();
            foreach (GameObject particleElement in particleElementsToModifyForDamageIndication)
            {
                ParticleSystem particleSystem = particleElement.GetComponent<ParticleSystem>();
                ParticleSystem.MainModule main = particleSystem.main;

                particleElements.Add(new ParticleElement(particleSystem, main.startColor.color));
            }
        }

        if (performInitialScaling && _projectileVisualTransform != null)
        {
            desiredTargetScale = _projectileVisualTransform.localScale;
            _projectileVisualTransform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        }

        transformRigidbody.rotation = angle.z;
        vectorSpeed = ProjectileSpeed * StaticTools.Vector2FromAngle(angle.z);
        damageAmount = damage;

        if (owner.gameObject.layer == PLAYER_LAYER)
        {
            //This is to fix the weird physics interations that can occur during the brief period the projectile collider is inside the player collider when the projectile first spawns.
            Physics2D.IgnoreCollision(Player.MovementCollider, GetComponent<Collider2D>());
        }
        Owner = owner;
    }

    /// <summary>
    /// Handle The movement of the projectile
    /// </summary>
    protected virtual void MoveProjectile()
    {
        if (performInitialScaling && !scalingBegun && _projectileVisualTransform != null)
        {
            BeginProjectileScaling();
        }

        DeltaVector = new Vector3(vectorSpeed.x * Time.deltaTime, vectorSpeed.y * Time.deltaTime);

        var newPosition = transformToOperateOn.position + DeltaVector;
        transformRigidbody.MovePosition(newPosition);
    }

    /// <summary>
    /// Clean up the projectile state.
    /// </summary>
    public virtual void Kill()
    {
        Timing.KillCoroutines(_lerpHandle);
        Timing.KillCoroutines(_lerpScaleHandle);
        Timing.KillCoroutines(_damageFlashHandle);

        scalingBegun = false;
        if (performInitialScaling && _projectileVisualTransform != null)
        {
            _projectileVisualTransform.localScale = desiredTargetScale;
        }

        if (type == AssetType.PARTICLE_EFFECT)
        {
            foreach (ParticleElement element in particleElements)
            {
                ParticleSystem.MainModule main = element.ParticleSystem.main;
                main.startColor = element.StartingColor;
            }
        }

        if (PlayImpactFX)
        {
            PlayImpactEffect();
        }

        Destroyed?.Invoke(this);

        if (_spriteRenderer)
        {
            _spriteRenderer.enabled = false;
        }

        isDead = true;
        Owner = null;

        bypassCollisionCheck = false;
        ignoreDamage = false;
        IgnoreDeath = false;
        PlayImpactFX = false;

        transformRigidbody.MoveRotation(Quaternion.Euler(ProjectilePrefab.transform.eulerAngles.x, ProjectilePrefab.transform.eulerAngles.y, ProjectilePrefab.transform.eulerAngles.z));

        projectileKilledBy = null;
        NotifyOfObjectDeath();
        ProjectileTransform.gameObject.SetActive(false);
    }

    public virtual void Kill(Collider2D killedBy)
    {
        projectileKilledBy = killedBy;
        Kill();
    }
    #endregion

    #region On Impact Methods
    /// <summary>
    /// Handles the projectile's collision, including whether the collision should even be accepted, applying the appropriate modifiers,
    /// and destroying the projectile, among other things. This method handles both trigger and non-trigger colliders.
    /// </summary>
    /// <param name="collider">The 2D collider that the projectile collided with.</param>
    /// <param name="isTrigger">Whether the collider was a trigger.</param>
    public virtual void HandleCollision(Collider2D collider, bool isTrigger)
    {
        //Is the damage reciving collider tagged as a EnemyCollider
        EnemyCollider enemyColliderRef = collider.GetComponent<EnemyCollider>();
        if (enemyColliderRef)
        {
            //if we get a ref is it the same NPC owner as this projectile?
            if (enemyColliderRef != null && enemyColliderRef.Enemy != null && Owner != null && enemyColliderRef.Enemy.gameObject == Owner.gameObject)
            {
                //It is, so don't respond to this collision.
                return;
            }
        }

       

        Projectile projectileComponent = collider.gameObject.GetComponent<Projectile>(); //Try to get a projectile component from the collision result to see if we collided with another projectile.

        if (bypassCollisionCheck ||
           (Owner != null && collider.gameObject != Owner
            && collider.gameObject.layer != Owner.layer
            && (projectileComponent == null || projectileComponent.Owner != Owner)
            && !isDead))
        {
            IDamageReceiver receiver = collider.GetComponent<IDamageReceiver>();
            if (receiver != null && !ignoreDamage)
            {
                receiver.TakeDamage(this, modifiedDamageAmount, DamageType.PROJECTILE, transform.position);
            }

            modifiedDamageAmount = damageAmount;
            ignoreDamage = false;

            //The first && was an || and I'm not sure why, but if shit breaks, this might be it.
            if (!IgnoreDeath && gameObject.tag != "ReflectedProjectile" && collider.gameObject.tag != "KeepProjectilesAlive")
            {
                Kill(collider);
            }
        }
    }

    public void Reflect(GameObject owner, bool rand = false, int MaxDeg = -230, int minDeg = -100)
    {
        Owner = owner;

        if (rand)
        {
            var randDeg = UnityEngine.Random.Range(minDeg, MaxDeg);
            if (transformRigidbody != null)
            {
                transformRigidbody.MoveRotation(Quaternion.Euler(0, 0, randDeg));
            }
            else
            {
                transformToOperateOn.Rotate(new Vector3(0, 0, UnityEngine.Random.Range(minDeg, MaxDeg)));
            }
        }
        else
        {
            if (transformRigidbody != null)
            {
                transformRigidbody.MoveRotation(Quaternion.Euler(0, 0, -180));
            }
            else
            {
                transformToOperateOn.Rotate(new Vector3(0, 0, -180));
            }
        }
    }

    /// <summary>
    /// Plays the effect set to occur when the projectile collides with an object, positioned based on the collided that killed this projectile, or a provided collider.
    /// <param name="hitCollider">The collider to be used in calculating the position of the impact effect.</param>
    /// </summary>
    protected virtual void PlayImpactEffect(Collider2D hitCollider = null)
    {
        //AUDIO: Play the impactSound  
        soundManager?.PlayHitSound();

        if (_projectileImpactEffect != null)
        {
            //Instantiate the impact effect.
            GameObject instancedImpactEffect = Instantiate(_projectileImpactEffect);

            if (projectileKilledBy || hitCollider) //There is a specific collider we want to use to calculate the impact effect position from.
            {
                Collider2D colliderToUse = hitCollider != null ? hitCollider : projectileKilledBy;

                if (colliderToUse.gameObject.layer != PROJECTILE_LAYER)
                {
                    RaycastHit2D hitToUse = FireRayAtDesiredImpactSurface(colliderToUse);
                    if (hitToUse.collider != null)
                    {
                        //Offset the surface normal by the position of the projectile and then subtract that from the projectile's position
                        //to create a vector that we can rotate the impact effect towards. Zero out the x and y rotations since this is 2D and we don't
                        //care about those.
                        Quaternion surfaceRotation = Quaternion.LookRotation(transform.position - new Vector3(hitToUse.normal.x + transform.position.x,
                            hitToUse.normal.y + transform.position.y, transform.position.z), Vector3.forward);

                        surfaceRotation.x = 0.0f;
                        surfaceRotation.y = 0.0f;

                        //Assign the projectile position and the calculated rotation to the impact effect.

                        //TODO: Find a way to consistently spawn the impact effect on the ground at the same Y-axis value without breaking other stuff.
                        instancedImpactEffect.transform.position = hitToUse.point;
                        instancedImpactEffect.transform.rotation = surfaceRotation;
                    }
                    else
                    {
                        Debug.LogWarning("Raycasting against the desired collider for the impact effect did not return a valid raycast");
                        PlayImpactPositionDefault(ref instancedImpactEffect);
                    }
                }
                else
                {
                    PlayImpactPositionDefault(ref instancedImpactEffect);
                }
            }
            else
            {
                PlayImpactPositionDefault(ref instancedImpactEffect);
            }
        }
    }

    /// <summary>
    /// Plays the effect set to occur when the projectile collides with an object, based on a provided position.
    /// </summary>
    /// <param name="position">The position to play the effect at.</param>
    /// <param name="rotation">The rotation to play the effect at.</param>
    protected void PlayImpactEffect(Vector3 position, Quaternion rotation)
    {
        //AUDIO: Play the impactSound  
        soundManager?.PlayHitSound();

        //Instantiate the impact effect.
        GameObject instancedImpactEffect = Instantiate(_projectileImpactEffect);

        instancedImpactEffect.transform.position = position;
        instancedImpactEffect.transform.rotation = rotation;
    }

    /// <summary>
    /// Plays the effect set to occur when the projectile collides with an object with the default position and rotation
    /// (current projectile position, rotated 180 degrees from the projectile's current rotation).
    /// </summary>
    /// <param name="instancedImpactEffect">The instanced impact effect game object.</param>
    private void PlayImpactPositionDefault(ref GameObject instancedImpactEffect)
    {
        instancedImpactEffect.transform.position = transform.position;
        instancedImpactEffect.transform.rotation = Quaternion.Euler(_projectileImpactEffect.transform.rotation.eulerAngles.x, _projectileImpactEffect.transform.rotation.eulerAngles.y, ProjectileTransform.rotation.eulerAngles.z);
        instancedImpactEffect.transform.Rotate(new Vector3(0, 0, 180));
    }
    #endregion


    /// <summary>
    /// Handles the dealing of damage to projectiles.
    /// </summary>
    /// <param name="giver">The object giving the damage.</param>
    /// <param name="amount">The amount of damage.</param>
    /// <param name="type">The type of damage.</param>
    public virtual void TakeDamage(IDamageGiver giver, float amount, DamageType type, Vector3 hitPoint)
    {
        Health -= amount;

        if (Health <= 0)
        {
            Kill();
        }
        else
        {
            if (this.type == AssetType.PARTICLE_EFFECT)
            {
                _damageFlashHandle = Timing.RunCoroutine(ApplyDamageFlashParticleEffect(DAMAGE_FLASH_COLOR));
            }
        }

        if (amount == 0)
        {
            //If the damage amount was 0, it might be an electric weapon. Electric weapons should still do damage to projectiles,
            //since stunning projectiles doesn't really make sense.
            if (giver is ElectricShock)
            {
                ElectricShock electric = giver as ElectricShock;
                Health -= electric.StunPower;
            }
        }
    }


    #region Scaling + Spacing Methods
    /// <summary>
    /// Public function to begin the co-routine for spacing projectiles.
    /// </summary>
    /// <param name="spacingTime"></param>
    public void LerpProjectile(float spacingTime)
    {
        _lerpHandle = Timing.RunCoroutine(LerpSpacedProjectile(spacingTime), "Lerp Projectile");
    }

    /// <summary>
    /// Lerps the position of a projectile from an initial vector to a final one.
    /// Used to create spacing between the multiple projectiles of a straight shot type projectile.
    /// </summary>
    /// <param name="spacingTime">How long should it take to space the projectiles (i.e. how long should the lerp process take).</param>
    /// <returns></returns>
    private IEnumerator<float> LerpSpacedProjectile(float spacingTime)
    {
        Vector3 currentVector = transformToOperateOn.position; //The vector that holds the current result of the lerp step.
        Vector3 previousVector = transformToOperateOn.position; //The vector that holds the previous lerp step result.
        Vector3 spacingDelta = Vector3.zero; //The change between the current and previous vectors.

        float spacingAngle = Vector3.Angle(InitialPositionVector, FinalSpacedVector); //TODO: This doesn't work. Figure out how to rotate the projectile during the lerp process and rotate it back at the end.

        float percentageScaleChange = 0f;
        while (percentageScaleChange < 1f)
        {
            percentageScaleChange += Time.deltaTime / spacingTime;
            previousVector = currentVector;
            currentVector = Vector3.Lerp(InitialPositionVector, FinalSpacedVector, percentageScaleChange);
            spacingDelta = currentVector - previousVector;

            //Set the projectile's position to it's current position plus the change in this lerp step.
            if (transformRigidbody != null)
            {
                var newPosition = transformToOperateOn.position + spacingDelta;
                transformRigidbody.MovePosition(newPosition);
            }
            else
            {
                //Note we can't use transform.Translate here because that function takes into account the projectile's rotation
                //which we don't actually want in this case.
                ProjectileTransform.position = new Vector3(transform.position.x + spacingDelta.x, transform.position.y + spacingDelta.y, transform.position.z);
            }
            yield return 0f;
        }
    }

    protected void BeginProjectileScaling()
    {
        scalingBegun = true;
        lerpScalingTime = 0.1f;//MAX_PROJECTILE_SCALE_LERP_TIME - ProjectileSpeed / MIN_PROJECTILE_SPEED / 10f * PROJECTILE_SCALE_LERP_TIME_RANGE;
        _lerpScaleHandle = Timing.RunCoroutine(LerpProjectileScaling(), "Lerp Projectile");
    }

    protected IEnumerator<float> LerpProjectileScaling()
    {
        float percentComplete = 0f;
        while (percentComplete < 1)
        {
            if (_projectileVisualTransform == null)
            {
                //If this co-routine is still running when the player dies and the game begins to reload (thus destroying all existing projectile), just end it.
                break;
            }

            percentComplete += Time.deltaTime / lerpScalingTime;

            float lerpXScale = Mathf.Lerp(0, desiredTargetScale.x, percentComplete);
            float lerpYScale = Mathf.Lerp(0, desiredTargetScale.y, percentComplete);
            float lerpZScale = Mathf.Lerp(0, desiredTargetScale.z, percentComplete);
            _projectileVisualTransform.localScale = new Vector3(lerpXScale, lerpYScale, lerpZScale);

            yield return 0f;
        }
    }
    #endregion

    #region Support Methods
    protected RaycastHit2D FireRayAtDesiredImpactSurface(Collider2D colliderToUse)
    {
        //Raycast to find the surface the projectile collided with so we can get that surface's normal.
        Ray2D ray = new Ray2D(transform.position + -transform.right, transform.right);
        Debug.DrawRay(ray.origin, ray.direction * 1.0f, Color.green, 5.0f);
        RaycastHit2D[] collidedSurfacesHit = Physics2D.RaycastAll(ray.origin, ray.direction, 5.0f, PLAYER_LAYER_SHIFTED | OBSTACLE_LAYER_SHIFTED | NPC_LAYER_SHIFTED | ENEMY_LAYER_SHIFTED | SHIELD_LAYER_SHIFTED);

        //Find which of the surfaces the raycast hit matches the one we are looking for.
        foreach (RaycastHit2D hit in collidedSurfacesHit)
        {
            if (hit.collider == colliderToUse)
            {
                Debug.DrawRay(hit.point, hit.normal, Color.blue, 5.0f);
                return hit;
            }
        }

        return new RaycastHit2D();
    }

    /// <summary>
    /// Applies a damage flash effect when the particle effect projectile takes damage.
    /// </summary>
    /// <param name="colorToFlash">The color to flash the particle effect to.</param>
    /// <returns></returns>
    protected IEnumerator<float> ApplyDamageFlashParticleEffect(Color colorToFlash)
    {
        foreach (ParticleElement element in particleElements)
        {
            ParticleSystem.MainModule main = element.ParticleSystem.main;
            main.startColor = colorToFlash;
        }

        yield return Timing.WaitForSeconds(0.05f);

        foreach (ParticleElement element in particleElements)
        {
            ParticleSystem.MainModule main = element.ParticleSystem.main;
            main.startColor = element.StartingColor;
        }

        yield return 0f;
    }

    /// <summary>
    /// Checks whether the layer the object the projectile collided with belongs to is an accepted layer,
    /// and returns the result.
    /// </summary>
    /// <param name="layer">The laye the collided object belongs to.</param>
    /// <param name="isTrigger">Whether the object collided with has a trigger collider.</param>
    /// <returns>Whether the layer is an accepted layer.</returns>
    protected bool LayerIsAcceptedLayer(int layer, bool isTrigger)
    {
        if (isTrigger)
        {
            return ACCEPTED_TRIGGER_LAYERS.Contains(layer);
        }

        return ACCEPTED_NON_TRIGGER_LAYERS.Contains(layer);
    }

    /// <summary>
    /// Populates the hashsets containing the accepted layers for trigger and non-trigger colliders.
    /// Run only once.
    /// </summary>
    private void PopulateAcceptedLayers()
    {
        ACCEPTED_NON_TRIGGER_LAYERS = new HashSet<int>();
        ACCEPTED_TRIGGER_LAYERS = new HashSet<int>();

        ACCEPTED_NON_TRIGGER_LAYERS.Add(IGNORE_RAYCAST_LAYER);
        ACCEPTED_NON_TRIGGER_LAYERS.Add(PLAYER_LAYER);
        ACCEPTED_NON_TRIGGER_LAYERS.Add(OBSTACLE_LAYER);
        ACCEPTED_NON_TRIGGER_LAYERS.Add(ENEMY_LAYER);
        ACCEPTED_NON_TRIGGER_LAYERS.Add(PROJECTILE_LAYER);
        ACCEPTED_NON_TRIGGER_LAYERS.Add(SHIELD_LAYER);

        ACCEPTED_TRIGGER_LAYERS.Add(NPC_LAYER);
        ACCEPTED_TRIGGER_LAYERS.Add(ENEMY_LAYER);

        LAYER_SETS_POPULATED = true;
    }
    #endregion
}
