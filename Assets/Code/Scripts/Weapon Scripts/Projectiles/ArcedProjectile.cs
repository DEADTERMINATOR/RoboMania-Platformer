﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcedProjectile : Projectile
{
    /// <summary>
    /// The amount of time the projectile must be alive for before it considers collisions with itself to be valid.
    /// </summary>
    private const float PROJECTILE_LIVE_TIME_BEFORE_SELF_COLLIDE_ALLOWED = 0.25f;


    /// <summary>
    /// The force that should be applied to the arced projectile when it fires.
    /// </summary>
    public float Force;


    protected override void FixedUpdate()
    {
        float shotAngle = Vector3.Angle(Vector3.right, transformRigidbody.velocity);
        shotAngle = transformRigidbody.velocity.y < 0 ? -shotAngle : shotAngle;
        transformRigidbody.MoveRotation(Quaternion.Euler(0, 0, shotAngle));
    }


    public override void Fire(Vector3 angle, float damage, GameObject owner)
    {
        if (transformRigidbody == null)
        {
            transformRigidbody = GetComponent<Rigidbody2D>();
        }

        base.Fire(angle, damage, owner);

        Vector2 angleVector = StaticTools.Vector2FromAngle(angle.z);
        Debug.DrawRay(transform.position, angleVector * 5.0f, Color.magenta, 10.0f);
        transformRigidbody.AddForce(Force * angleVector);
    }

    public virtual void Fire(Vector3 angle, Vector3 forceVector, float damage, GameObject owner)
    {
        if (transformRigidbody == null)
        {
            transformRigidbody = GetComponent<Rigidbody2D>();
        }

        base.Fire(angle, damage, owner);

        transformRigidbody.velocity = forceVector;
    }


    /// <summary>
    /// Handles the projectile's collision, including whether the collision should even be accepted, applying the appropriate modifiers,
    /// and destroying the projectile, among other things. This method handles both trigger and non-trigger colliders.
    /// </summary>
    /// <param name="collider">The 2D collider that the projectile collided with.</param>
    /// <param name="isTrigger">Whether the collider was a trigger.</param>
    public override void HandleCollision(Collider2D collider, bool isTrigger)
    {
        //If the projectile isn't dead and the projectile hit it's owner, just make it disappear.
        if (collider.gameObject == Owner && !isDead && aliveTime >= PROJECTILE_LIVE_TIME_BEFORE_SELF_COLLIDE_ALLOWED)
        {
            Kill();
        }
        else
        {
            base.HandleCollision(collider, isTrigger);
        }
    }
}
