﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Enemies;

namespace Characters.Sound
{
    public class TrackerSounds : CharacterSounds
    {
        public AudioClip BeamCharge;
        public AudioClip BeamFire;

        public TrackerSounds(Tracker tracker)
        {
            SoundSourcePool = tracker.GetComponent<SoundSourceGlobalPool>();

            BeamCharge = Resources.Load("Sounds/Enemy Sounds/Tracker/Beam Charge") as AudioClip;
            BeamFire = Resources.Load("Sounds/Enemy Sounds/Tracker/Beam Fire") as AudioClip;
        }

        public void PlayBeamCharge()
        {
            PlaySoundOnceWithPositionalAudio(BeamCharge, 10f, 20f, AudioRolloffMode.Linear, 0.2f);
        }

        public void PlayBeamFire()
        {
            PlaySoundLoopWithPositionalAudio(BeamFire, 10f, 20f, AudioRolloffMode.Linear, 0.2f);
        }

        public void StopBeamCharge()
        {
            if (AudioSource.clip == BeamCharge)
                StopLastSound();
            else
                StopSound(BeamCharge);
        }

        public void StopBeamFire()
        {
            StopSoundLoop(BeamFire);
        }
    }
}
