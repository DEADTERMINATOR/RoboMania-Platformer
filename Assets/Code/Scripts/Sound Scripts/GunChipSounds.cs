﻿using UnityEngine;
using System.Collections;

public class GunChipSounds : SoundManager
{
    public AudioClip ShootSound;

    public void PlayShootSound()
    {
        PlaySoundRandomPitch(ShootSound, 0.95f, 1.05f);
    }

  
}
