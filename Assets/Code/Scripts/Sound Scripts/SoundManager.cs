﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// An abstract base class for sound managers
/// </summary>
public class SoundManager
{
    public AudioSource AudioSource;
    public bool Debug = false;


    public virtual void PlaySoundOnce(AudioClip clip, float volume = 1, string outputMixerSubgroupName = "", float spatialBlend = 0)
    {
        if (clip)
        {
            AudioSource.volume = volume;
            AudioSource.pitch = 1;
            AudioSource.clip = clip;
            AudioSource.loop = false;
            AudioSource.spatialBlend = 0;
            AudioSource.Play();
            //if (debug)
                //Debug.Log("Playing sound once", clip);
        }
    }

    public virtual void PlaySoundRandomPitch(AudioClip clip, float minPitch = 0.5f, float MaxPitch = 1.5f, float volume = 1, string outputMixerSubgroupName = "", float spatialBlend = 0)
    {
        if (clip)
        {
            AudioSource.volume = volume;
            AudioSource.clip = clip;
            AudioSource.loop = false;
            AudioSource.pitch = Random.Range(minPitch, MaxPitch);
            AudioSource.spatialBlend = 0;
            AudioSource.Play();
            //if (debug)
                ////Debug.Log(string.Format("Playing with random pitch {0}", audioSource.pitch), clip);

        }
    }

    public virtual void PlaySoundLoop(AudioClip clip, float volume = 1, string outputMixerSubgroupName = "", float spatialBlend = 0)
    {
        if (clip)
        {
            //save the ref
            AudioSource.volume = volume;
            AudioSource.pitch = 1;
            AudioSource.clip = clip;
            AudioSource.loop = true;
            AudioSource.spatialBlend = 0;
            AudioSource.Play();
            //if (debug)
                //Debug.Log("Playing sound loop", clip);
        }
    }

    public virtual void PlaySoundOnceWithPositionalAudio(AudioClip clip, float minDistance, float maxDistance, AudioRolloffMode audioRolloffMode = AudioRolloffMode.Linear, float volume = 1, string outputMixerSubgroupName = "", float spatialBlend = 1)
    {
        if (clip)
        {
            AudioSource.spatialBlend = spatialBlend;
            AudioSource.minDistance = minDistance;
            AudioSource.maxDistance = maxDistance;
            AudioSource.rolloffMode = audioRolloffMode;

            AudioSource.volume = volume;
            AudioSource.pitch = 1;
            AudioSource.clip = clip;
            AudioSource.loop = false;

            AudioSource.Play();
            //if (debug)
                //Debug.Log("Playing positional sound once", clip);
        }
    }

    public virtual void PlaySoundRandomPitchWithPositionalAudio(AudioClip clip, float minDistance, float maxDistance, AudioRolloffMode audioRolloffMode = AudioRolloffMode.Linear, float volume = 1, float minPitch = 0.5f, float maxPitch = 1.5f, string outputMixerSubgroupName = "", float spatialBlend = 1)
    {
        if (clip)
        {
            AudioSource.spatialBlend = spatialBlend;
            AudioSource.minDistance = minDistance;
            AudioSource.maxDistance = maxDistance;
            AudioSource.rolloffMode = audioRolloffMode;

            AudioSource.volume = volume;
            AudioSource.pitch = Random.Range(minPitch, maxPitch);
            AudioSource.clip = clip;
            AudioSource.loop = false;

            AudioSource.Play();
            //if (debug)
                //Debug.Log(string.Format("Playing positional audio with random pitch {0}", audioSource.pitch), clip);
        }
    }

    public virtual void PlaySoundLoopWithPositionalAudio(AudioClip clip, float minDistance, float maxDistance, AudioRolloffMode audioRolloffMode = AudioRolloffMode.Linear, float volume = 1, string outputMixerSubgroupName = "", float spatialBlend = 1)
    {
        if (clip)
        {
            AudioSource.spatialBlend = spatialBlend;
            AudioSource.minDistance = minDistance;
            AudioSource.maxDistance = maxDistance;
            AudioSource.rolloffMode = audioRolloffMode;

            AudioSource.volume = volume;
            AudioSource.pitch = 1;
            AudioSource.clip = clip;
            AudioSource.loop = true;

            AudioSource.Play();
            //if (debug)
                //Debug.Log("Playing positional sound loop", clip);
        }
    }

    public virtual void StopLastSound()
    {
        //use set ref to avoid lost handles
        AudioSource.Stop();
        //if (debug)
            //Debug.Log("Stopping last sound", this);
    }
}
