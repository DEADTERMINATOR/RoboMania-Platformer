﻿using UnityEngine;
using System.Collections;

public class ProjectileSound : SoundManagerMultiChannel
{
    public AudioClip ShootSound;
    public AudioClip HitSound;

    public void PlayShootSound()
    {
        PlaySoundRandomPitch(ShootSound, 0.95f, 1.05f);
    }

    public void PlayHitSound()
    {
        PlaySoundRandomPitch(HitSound, 0.9f, 1.1f);
    }

}
