﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;

public class SoundSourceGlobalPool : MonoBehaviour
{
    /// <summary>
    /// The mixer that contains the group that this pool manages.
    /// </summary>
    public AudioMixer Mixer;

    /// <summary>
    /// The mixer group all sound in this pool will be played on.
    /// </summary>
    public AudioMixerGroup Group;

    //the number of AudioSources to start with
    public int StartingAmount;
    [HideInInspector]
    public int NextIndex = 0;


    protected List<AudioSource> _audioSources;
    protected Dictionary<string, AudioMixerGroup> _subgroups;


    // Use this for initialization
    void Start()
    {
        _audioSources = new List<AudioSource>();
        for(int i = 0; i < StartingAmount; i++)
        {
            _audioSources.Add(AddNewAudioSource());
        }

        if (Mixer == null)
        {
            Mixer = Resources.Load("MainAudioMixer") as AudioMixer;
        }

        AudioMixerGroup[] subgroups = Mixer.FindMatchingGroups(Group.name);
        _subgroups = new Dictionary<string, AudioMixerGroup>();
        foreach (AudioMixerGroup group in subgroups)
        {
            if (group.name == Group.name)
                continue; //Don't add the result that matches the main group for this pool.
            _subgroups.Add(group.name, group);
        }
    }

    protected AudioSource AddNewAudioSource(string subgroupName = "")
    {
        AudioSource newAudioSource = gameObject.AddComponent<AudioSource>();
        SetAudioSourceOutputMixerGroup(ref newAudioSource, subgroupName);
        return newAudioSource;
    }

    public AudioSource GetAudioSource(string subgroupName = "")
    {
        AudioSource audioSourceToUse;

        ///Is the Next audioSource in use
        if (_audioSources[NextIndex].isPlaying)
        {//yes
            ///Increment the index
            NextIndex++;
            //is this the last in the array (could do a serach fro next free one but will one look if the start is free)
            //if the start is not free we will just add to the end of the array
            if (NextIndex == _audioSources.Count)
            {//yes
                //can we warp back to the start
                if (_audioSources[0].isPlaying)
                {//no
                    //set index back to start
                    NextIndex = 0;

                    //add and return new audioSource
                    AudioSource newAudioSource = AddNewAudioSource(subgroupName);
                    _audioSources.Add(newAudioSource);
                    return newAudioSource;
                }
                else
                {//yes
                    NextIndex = 1;
                    audioSourceToUse = _audioSources[0];
                    SetAudioSourceOutputMixerGroup(ref audioSourceToUse, subgroupName);
                    return _audioSources[0];
                }
            }
            else
            {
                //try the next index as we are not at the end
                return GetAudioSource(subgroupName);
            }
        }

        NextIndex++;
        if (NextIndex == _audioSources.Count)
        {
            NextIndex = 0;
        }

        audioSourceToUse = _audioSources[NextIndex];
        SetAudioSourceOutputMixerGroup(ref audioSourceToUse, subgroupName);
        return _audioSources[NextIndex];  
    }

    public AudioSource GetAudioSourcePlayingGivenClip(AudioClip clip)
    {
        foreach (AudioSource source in _audioSources)
        {
            if (source.isPlaying && source.clip == clip)
                return source;
        }
        return null;
    }

    private void SetAudioSourceOutputMixerGroup(ref AudioSource audioSource, string subgroupName)
    {
        if (subgroupName == "")
            audioSource.outputAudioMixerGroup = Group;
        else
        {
            bool validSubgroup = _subgroups.ContainsKey(subgroupName);
            if (validSubgroup)
                audioSource.outputAudioMixerGroup = _subgroups[subgroupName];
            else
                audioSource.outputAudioMixerGroup = Group;
        }
    }
}
