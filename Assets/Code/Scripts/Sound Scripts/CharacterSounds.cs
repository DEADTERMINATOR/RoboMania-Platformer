﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

namespace Characters.Sound
{
    /// <summary>
    /// A base sound manager for characters such as the player and npcs
    /// </summary>
    public class CharacterSounds : SoundManagerMultiChannel
    {
        public AudioClip WalkSound;
        public AudioClip HitSound;
        public AudioClip DeathSound;

        public virtual void PlayHitSound()
        {
            PlaySoundRandomPitch(HitSound, 0.7f, 1.3f);
        }

        public virtual void PlayDeathSound()
        {
            PlaySoundOnce(DeathSound);
        }
    }
}
