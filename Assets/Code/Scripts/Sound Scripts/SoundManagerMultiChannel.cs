﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManagerMultiChannel : SoundManager
{
    public SoundSourceGlobalPool SoundSourcePool;
    public Dictionary<string, AudioSource> LoopedAudioSources = new Dictionary<string, AudioSource>();
    

    public SoundManagerMultiChannel()
    {
        if (SoundSourcePool == null)
            SoundSourcePool = GameObject.FindObjectOfType<SoundSourceGlobalPool>();
    }


    /// <summary>
    /// Used the main audio souce if it not is use other wise use the alt
    /// </summary>
    protected virtual void SetAudioSource(string outputMixerSubgroupName = "")
    {
        AudioSource = SoundSourcePool.GetAudioSource(outputMixerSubgroupName); 
    } 

    public override void PlaySoundOnce(AudioClip clip, float volume = 1, string outputMixerSubgroupName = "", float spatialBlend = 0)
    {
        SetAudioSource(outputMixerSubgroupName);
        base.PlaySoundOnce(clip, volume);
    }

    public override void PlaySoundRandomPitch(AudioClip clip, float minPitch = 0.5f, float maxPitch = 1.5f, float volume = 1, string outputMixerSubgroupName = "", float spatialBlend = 0)
    {
        SetAudioSource(outputMixerSubgroupName);
        base.PlaySoundRandomPitch(clip, minPitch, maxPitch, volume);
    }

    public override void PlaySoundLoop(AudioClip clip, float volume = 1, string outputMixerSubgroupName = "", float spatialBlend = 0)
    {
        SetAudioSource(outputMixerSubgroupName);
        if (LoopedAudioSources.ContainsKey(clip.name))
        {
            return;//Is playing still
        }

        LoopedAudioSources.Add(clip.name, AudioSource);
        base.PlaySoundLoop(clip, volume, outputMixerSubgroupName, spatialBlend);
    }

    public override void PlaySoundOnceWithPositionalAudio(AudioClip clip, float minDistance, float maxDistance, AudioRolloffMode audioRolloffMode = AudioRolloffMode.Linear, float volume = 1, string outputMixerSubgroupName = "", float spatialBlend = 1)
    {
        SetAudioSource(outputMixerSubgroupName);
        base.PlaySoundOnceWithPositionalAudio(clip, minDistance, maxDistance, audioRolloffMode, volume, outputMixerSubgroupName, spatialBlend);
    }

    public override void PlaySoundRandomPitchWithPositionalAudio(AudioClip clip, float minDistance, float maxDistance, AudioRolloffMode audioRolloffMode = AudioRolloffMode.Linear, float volume = 1, float minPitch = 0.5f, float maxPitch = 1.5f, string outputMixerSubgroupName = "", float spatialBlend = 1)
    {
        SetAudioSource(outputMixerSubgroupName);
        base.PlaySoundRandomPitchWithPositionalAudio(clip, minDistance, maxDistance, audioRolloffMode, volume, minPitch, maxPitch, outputMixerSubgroupName, spatialBlend);
    }

    public override void PlaySoundLoopWithPositionalAudio(AudioClip clip, float minDistance, float maxDistance, AudioRolloffMode audioRolloffMode = AudioRolloffMode.Linear, float volume = 1, string outputMixerSubgroupName = "", float spatialBlend = 1)
    {
        SetAudioSource(outputMixerSubgroupName);
        if (LoopedAudioSources.ContainsKey(clip.name))
        {
            return;
        }

        LoopedAudioSources.Add(clip.name, AudioSource);
        base.PlaySoundLoopWithPositionalAudio(clip, minDistance, maxDistance, audioRolloffMode, volume, outputMixerSubgroupName, spatialBlend);
    }

    /// <summary>
    /// Stops the audio source playing the given clip if the source can be found and is still playing.
    /// This method does not search the sound loop list and remove the sound from there. Use StopSoundLoop
    /// if the sound is looped.
    /// </summary>
    /// <param name="clip">The audio clip to stop playing.</param>
    public void StopSound(AudioClip clip)
    {
        AudioSource source = SoundSourcePool.GetAudioSourcePlayingGivenClip(clip);
        if (source != null)
        {
            source.Stop();
        }
    }

    public void StopSoundLoop(AudioClip clip)
    {
        if (!LoopedAudioSources.ContainsKey(clip.name))
            return;//Is not playing

        AudioSource audioSource;
        audioSource = LoopedAudioSources[clip.name];
        
        ///use set ref to avoid lost handels
        audioSource.loop = false;
        audioSource.Stop();
        LoopedAudioSources.Remove(clip.name);

        if (Debug)
            UnityEngine.Debug.Log("Stoping Sound loop " + clip.name);
    }

    /// <summary>
    /// Stop all looped sound managed by the manager 
    /// </summary>
    /// <param name="clip"></param>
    public virtual void StopAllLoopedSounds()
    {
        foreach (AudioSource audioSource in LoopedAudioSources.Values)
        {
            audioSource.loop = false;
            audioSource.Stop();
        }
    }

}
