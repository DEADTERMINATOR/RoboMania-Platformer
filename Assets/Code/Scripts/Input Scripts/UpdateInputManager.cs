﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static Inputs.InputManager;

namespace Inputs
{
    public class UpdateInputManager : MonoBehaviour
    {
        private InputManager _inputManagerInstanceRef;


        private void Start()
        {
            _inputManagerInstanceRef = InputManager.Instance;
        }

        private void Update()
        {
            _inputManagerInstanceRef.Update();
        }
    }
}
