﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HoldToPerformMultipleActions : IInputInteraction
{
    public bool SpeedUpRepeatInteractionsAfterTheFirst = false;
    public float HoldTime = 0.5f;
    public float RepeatHoldTime = 0.1f;

    public void Process(ref InputInteractionContext context)
    {
        bool performActionAgain = false;

        if (context.timerHasExpired)
        {
            performActionAgain = true;
        }

        switch (context.phase)
        {
            case InputActionPhase.Waiting:
                if (context.ReadValue<float>() == 1)
                {
                    context.PerformedAndStayPerformed();
                    context.SetTimeout(HoldTime);
                }

                break;
            case InputActionPhase.Performed:
                if (performActionAgain)
                {
                    context.Performed();
                    context.SetTimeout(SpeedUpRepeatInteractionsAfterTheFirst == true ? RepeatHoldTime : HoldTime);
                }
                else if (context.ReadValue<float>() == 0)
                {
                    context.Canceled();
                }

                break;
            case InputActionPhase.Canceled:
                context.Waiting();
                break;
        }
    }

    public void Reset() { }
}
