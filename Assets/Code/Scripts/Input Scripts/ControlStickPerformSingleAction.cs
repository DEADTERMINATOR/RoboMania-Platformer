﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ControlStickPerformSingleAction : IInputInteraction
{
    public bool RegisterFurtherInteractionsWithoutResettingControlStick = false;
    public bool SpeedUpRepeatInteractionsAfterTheFirst = false;
    public float DurationToRegisterFurtherInteractions = 0.5f;
    public float SpeedUpDuration = 0.1f;

    public void Process(ref InputInteractionContext context)
    {
        bool performActionAgain = false;

        if (context.timerHasExpired)
        {
            performActionAgain = true;
        }

        switch (context.phase)
        {
            case InputActionPhase.Waiting:
                if (context.ReadValue<float>() > GlobalData.CONTROLLER_STICK_DEADZONE)
                {
                    context.Started();
                }
                break;
            case InputActionPhase.Started:
                context.PerformedAndStayPerformed();
                if (RegisterFurtherInteractionsWithoutResettingControlStick)
                {
                    context.SetTimeout(DurationToRegisterFurtherInteractions);
                }
                break;
            case InputActionPhase.Performed:
                if (context.ReadValue<float>() < GlobalData.CONTROLLER_STICK_DEADZONE)
                {
                    context.Waiting();
                }
                else if (performActionAgain)
                {
                    context.Performed();
                    context.SetTimeout(SpeedUpRepeatInteractionsAfterTheFirst ==  true ? SpeedUpDuration : DurationToRegisterFurtherInteractions);
                }
                break;
        }
    }

    public void Reset() { }
}
