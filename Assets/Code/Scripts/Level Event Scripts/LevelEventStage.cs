﻿using UnityEngine;
using System.Collections;
namespace LevelEventStageSystem
{
    /// <summary>
    /// MonoBehaviour IS MONSTLY HERE SO IT CAN BE CHAGED IN EDITOR
    /// </summary>
    public abstract class LevelEventStage : MonoBehaviour
    {
        
        /// <summary>
        /// How long the sate will last in seconds 
        /// </summary>
        public float StateTime = 30;
        /// <summary>
        /// Set the easign in editor
        /// </summary>
        public Easing.EaseType EasingAlgo = Easing.EaseType.Linear;

        /// <summary>
        /// The easing function ref
        /// </summary>
        private Easing.EasingFunction _easeFunction;
        /// <summary>
        /// The internal run time 
        /// </summary>
        protected float _intrenalTime { get;set; }

        public void Awake()
        {
            _easeFunction = Easing.GetFuctionFromEnum(EasingAlgo);
        }
        /// <summary>
        /// Unity update exposure only run when this stage is active
        /// </summary>
        /// <param name="DealtTime"></param>
        public void UpdateStage(float DealtTime)
        {
            _intrenalTime += DealtTime;
            RunStage(EassedTimeScale());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="EasedTime">Time as 0 - 1 as</param>
        public virtual void RunStage(float EasedTime)
        {
           
        }

        private float EassedTimeScale()
        {
            return _easeFunction(_intrenalTime / StateTime);
        }

        public virtual bool IsDone() 
        { 
            return _intrenalTime >= StateTime; 
        } 
    }

}