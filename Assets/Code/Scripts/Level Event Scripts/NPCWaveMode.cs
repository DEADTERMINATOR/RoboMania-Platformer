﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Characters;

public struct WaveData
{
    public int  NumberOFNpcs;
    public bool UseSpecial;
    public int  KilledNPCS;
    public int  TotalSpawned;
}



public class NPCWaveMode : MonoBehaviour , IActivatable 
{
    public bool Active { get { return _running; } }

    #region References
    public List<NPCSpawner> Spawners = new List<NPCSpawner>();
    public List<NPCSpawner> SpecialSpawners = new List<NPCSpawner>();
    public List<Transform> PickSpawnPoints = new List<Transform>();
    public GameObject GunChipSpawnner;
    public GameObject AmmoPickUp;
    public Text info;
    #endregion

    #region Settings
    public int MaxSpawnWave;
    public int SpawnSpecialEvery = 5; 
    public int MaxNumberPreWave;
    public int MaxSpawnAtaTime;
    public int MinWavesToWaitForDroop;
    public float TimeBetweenWaves;
    #endregion

    private int _wave = 0;
    private int _nextDroopWave;
    private bool _running = false;
    private bool _runningWave = false;
    private float _waitStarted = -1;
    private WaveData _currentWaveData;
    private int  _alive = 0;


    public void Activate(GameObject activator)
    {
        _running = true;
        _waitStarted = Time.realtimeSinceStartup;
    }

    public void Deactivate(GameObject activator)
    {
        throw new System.NotImplementedException();
    }


    private WaveData NextWave()
    {
        _wave++;
        float max = (float)_wave / (float)MaxSpawnWave;
        WaveData newWave = new WaveData();
        newWave.NumberOFNpcs = Mathf.Max((int)Mathf.Floor(MaxNumberPreWave * max),1);
        newWave.UseSpecial = (_wave % SpawnSpecialEvery == 0);
        newWave.KilledNPCS = 0;
        return newWave;
    }

   private  void RunWave()
    {
        if (_currentWaveData.TotalSpawned != _currentWaveData.NumberOFNpcs && _alive != MaxSpawnAtaTime)
        {
            Spawn();
        }
        if(ISWaveDone())
        {
            _waitStarted = Time.realtimeSinceStartup;
            _runningWave = false;
        }

    }
    private void Spawn()
    {
        if (_wave % SpawnSpecialEvery == 0)
        {
            SpawnSpecial();
        }
        else
        {
            SpawnNormal();
        }
    }
    private void SpawnSpecial()
    {
        var shuffledSpawnersSpecial = SpecialSpawners.OrderBy(a => Guid.NewGuid()).ToList();
        foreach (NPCSpawner spawner in shuffledSpawnersSpecial)
        {
            if (!spawner.NPCActive)
            {
                NPC spawn = spawner.TryAndSpawnNPC()?.GetComponent<NPC>();

                if (spawn)
                {
                    spawn.Dead += NPCDied;
                    _alive++;
                    _currentWaveData.TotalSpawned++;
                }
            }
            if (_currentWaveData.TotalSpawned == _currentWaveData.NumberOFNpcs || _alive == MaxSpawnAtaTime)
            {
                break;
            }
        }
    }

    private void SpawnNormal()
    {
        var shuffledSpawners = Spawners.OrderBy(a => Guid.NewGuid()).ToList();

        foreach (NPCSpawner spawner in shuffledSpawners)
        {
            if (!spawner.NPCActive)
            {
                NPC spawn = spawner.TryAndSpawnNPC()?.GetComponent<NPC>();

                if (spawn)
                {
                    spawn.Dead += NPCDied;
                    _alive++;
                    _currentWaveData.TotalSpawned++;
                }
            }
            if (_currentWaveData.TotalSpawned == _currentWaveData.NumberOFNpcs || _alive == MaxSpawnAtaTime)
            {
                break;
            }
        }
    }
    private void DoDroops()
    {
        var shuffledSpawners = PickSpawnPoints.OrderBy(a => Guid.NewGuid()).ToList();
        if(UnityEngine.Random.Range(0,4)==0)
        {
            Instantiate(AmmoPickUp, shuffledSpawners[0].position, shuffledSpawners[0].rotation);
        }
        else
        {
            Instantiate(GunChipSpawnner, shuffledSpawners[0].position, shuffledSpawners[0].rotation);
        }
        

    }
    private bool ISWaveDone()
    {
        return (_currentWaveData.KilledNPCS >= _currentWaveData.NumberOFNpcs);
    }
    private void NPCDied()
    {
        _currentWaveData.KilledNPCS++;
        _alive--;
    }

    // Use this for initialization
    void Start()
    {
        _nextDroopWave = MinWavesToWaitForDroop; 
    }

    // Update is called once per frame
    void Update()
    {
        if(_running)
        {
            
            if (_runningWave)
            {
                info.gameObject.SetActive(true);
                info.text = "WAVE:" + _wave.ToString() + "\n " + _currentWaveData.KilledNPCS.ToString() + "/" + _currentWaveData.NumberOFNpcs.ToString(); 
                RunWave();
            }
            else
            {
                info.gameObject.SetActive(true);
                if (Time.realtimeSinceStartup - _waitStarted >= TimeBetweenWaves)
                {
                    _currentWaveData = NextWave();
                    if(_nextDroopWave == _wave)
                    {
                        DoDroops();
                        _nextDroopWave = _wave + MinWavesToWaitForDroop;
                        Debug.Log(_nextDroopWave);
                    }
                    _runningWave = true;
                }
                else
                {
                    info.text = ""+Mathf.Round((TimeBetweenWaves-(Time.realtimeSinceStartup - _waitStarted))).ToString();
                }
            }

        }
    }

   
}
