﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MovementEffects;

/// <summary>
/// Handles the unloading of the previous place the player came from.
/// </summary>
public class UnloadArea : MonoBehaviour
{
    /// <summary>
    /// The string name of the scene that will be unloaded.
    /// </summary>
    public string SceneName;


    /// <summary>
    /// The collider trigger that begins the unloading of the associated area.
    /// </summary>
    private BoxCollider2D _unloadTrigger;


    // Use this for initialization
    private void Start()
    {
        _unloadTrigger = GetComponent<BoxCollider2D>();
    }

    /// <summary>
    /// Triggers the co-routine that loads the next area when the player crosses the trigger.
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == GlobalData.PLAYER_LAYER && SceneManager.GetSceneByPath(SceneName).isLoaded)
        {
            Timing.RunCoroutine(UnloadAreaAsync(), "Unload Scene " + SceneName);
        }
    }

    /// <summary>
    /// Loads the designated area of the level.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> UnloadAreaAsync()
    {
        AsyncOperation op = SceneManager.UnloadSceneAsync(SceneName);
        Resources.UnloadUnusedAssets();
        yield return 0f;
    }
}
