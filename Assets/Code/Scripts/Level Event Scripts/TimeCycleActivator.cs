﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

namespace RoboMania
{
    public class TimeCycleActivator : Activator
    {
        public new bool Active { get; private set; }

        [SerializeField]
        private bool _separateOnOffCycleDurations;

        [SerializeField]
        private float _timeCycleDuration;
        [SerializeField]
        private float _cycleOnDuration;
        [SerializeField]
        private float _cycleOffDuration;


        private bool _cycleOn;
        private float _timeRemainingUntilCycleSwitch;


        protected override void Start()
        {
            if (activeOnStart)
            {
                ActivateCycle();
                Active = true;
            }
        }

        // Update is called once per frame
        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            if (Active)
            {
                _timeRemainingUntilCycleSwitch -= Time.deltaTime;
                if (_timeRemainingUntilCycleSwitch <= 0)
                {
                    if (_cycleOn)
                    {
                        DeactivateCycle();
                    }
                    else
                    {
                        ActivateCycle();
                    }
                }
            }
        }

        protected override void OnTriggerEnter2D(Collider2D collider)
        {
            if ((_defaultToPlayerCanActivate && collider.gameObject.layer == GlobalData.PLAYER_LAYER)
                || _tagsAllowedToActivate.Contains(collider.tag))
            {
                ActivateCycle();
                Active = true;
            }
        }

        protected override void OnTriggerExit2D(Collider2D collider)
        {
            if (DeactivateOnExit
                && ((_defaultToPlayerCanActivate && collider.gameObject.layer == GlobalData.PLAYER_LAYER) || _tagsAllowedToActivate.Contains(collider.tag)))
            {
                DeactivateCycle();
                Active = false;
            }
        }

        public override void Deactivate(GameObject deactivator)
        {
            if (ActivateOverTime)
            {
                Timing.RunCoroutine(DoDeactivateOverTime(false));
            }
            else
            {
                DeactivateAllNow(false);
            }
        }

        private void ActivateCycle()
        {
            _cycleOn = true;
            _timeRemainingUntilCycleSwitch = _separateOnOffCycleDurations == true ? _cycleOnDuration : _timeCycleDuration;

            Activate(gameObject);
        }

        private void DeactivateCycle()
        {
            _cycleOn = false;
            _timeRemainingUntilCycleSwitch = _separateOnOffCycleDurations == true ? _cycleOffDuration : _timeCycleDuration;

            Deactivate(gameObject);
        }
    }
}
