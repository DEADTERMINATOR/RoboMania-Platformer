﻿using UnityEngine;
using System.Collections;

public class LoadLevelArea : MonoBehaviour
{
    public bool StartLoaded = false;
    public GameObject ToLoad;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            ToLoad.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            ToLoad.SetActive(false);
        }
    }
    // Use this for initialization
    void Start()
    {
        if(StartLoaded)
        {
            ToLoad.SetActive(true); 
        }
        else
        {
            ToLoad.SetActive(false);
        }
    }
}
