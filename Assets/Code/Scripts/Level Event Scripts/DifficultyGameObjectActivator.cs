﻿using UnityEngine;
using System.Collections;
using LevelSystem;

public class DifficultyGameObjectActivator : MonoBehaviour
{
    public enum Modes {EQUALS,LESSTHEN,GRATERTHEN}
    public Modes Mode = Modes.EQUALS;
    public Difficulty Difficulty;
    public GameObject ObjectToActivate;
    public bool DeactivateIfPlayerHas;
    public bool LessThen;
    // Use this for initialization
    void Awake()
    {

        switch(Mode)
        {
            case Modes.EQUALS:
                ObjectToActivate.SetActive(Difficulty == Level.CurentLevel.CurentDifficulty ^ DeactivateIfPlayerHas);
                return;
            case Modes.GRATERTHEN:
                ObjectToActivate.SetActive(Difficulty >= Level.CurentLevel.CurentDifficulty ^ DeactivateIfPlayerHas);
                return;
            case Modes.LESSTHEN:
                ObjectToActivate.SetActive(Difficulty >= Level.CurentLevel.CurentDifficulty ^ DeactivateIfPlayerHas);
                return;
        }
       
    }
}

    

