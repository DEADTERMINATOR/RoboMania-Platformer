﻿using UnityEngine;
using LevelComponents.Platforms;
using System.Collections;

public class PlatfromTrackLiquidLevel : MonoBehaviour
{
    [Tooltip("You can add the ref the manager or just assign the event But not booth ")]
    public LiquidLevelManager LiquidelevelManager;
    [Tooltip("not all platforms will react to this")]
    public Platform Platform;
    [Tooltip("Ignore the Platform ref and just update the transform (Use a platform if one exits to avoid issues)")]
    public bool UseTransform = false;
    [Tooltip("This is attached to a Platform or has a child with one Platform so just get the ref on awake")]
    public bool GetSelfReferance = false;
    public HasTagInTrigger PlayerPusher;
    [Tooltip("Max This can move ( 0 = infinite )")]
    public float MaxDistance = 0;
    public float MinDistance = 0;
    public float StartOffsetDistance = 0;
    public bool SnapTolavalTopONstart;
    private float _totalAmount = 0;
    private Rigidbody2D _rigidbody2D;


    // Use this for initialization
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        if (LiquidelevelManager)
        {
            LiquidelevelManager.OnLevelChahged.AddListener(HandleLevelChange);
            if(SnapTolavalTopONstart)
            {
                if (UseTransform)
                {
                    _rigidbody2D.MovePosition( new Vector3(transform.position.x, LiquidelevelManager.GetYHeightOfFluid, transform.position.z)) ;
                }
                else if (Platform != null)
                {
                    Platform.SetBasePositionOffset(new Vector3(0, transform.position.y -LiquidelevelManager.GetYHeightOfFluid , 0));
                }
            }
        }else
        {
            Debug.LogError(name + " Dose not have a LiquidelevelManager set and will not move ");
            gameObject.name = string.Format("{0}-{1}", "Missing Manager", gameObject.name);
        }
        if(MinDistance > 0)
        {
            //Debug.LogError(name + " has a min distance grate then zero and was set to zero");
            MinDistance = 0;
        }
    }


    public void HandleLevelChange(float amount)
    {
        if (MinDistance != 0 && _totalAmount + amount > MaxDistance)
        {
            amount = MaxDistance - amount;
        }
        if (MinDistance != 0 && _totalAmount + amount < MinDistance)
        {
            amount = MaxDistance - amount;
        }
        _totalAmount += amount;

        float actualAmount = (transform.position.y - LiquidelevelManager.GetYHeightOfFluid);
        if (actualAmount != 0)
        {
            actualAmount += StartOffsetDistance;
        }
        if (UseTransform)
        {
            if (PlayerPusher && PlayerPusher.IsInTrigger)
            {
                GlobalData.PlayerTransfrom.position += (Vector3.up * amount);
            }
            _rigidbody2D.MovePosition(new Vector3(transform.position.x, transform.position.y + amount, transform.position.z));
        }
        else if (Platform != null)
        {
            Platform.SetBasePositionOffset(new Vector3(0, amount, 0));
        }

    }
    private void Awake()
    {
        if (GetSelfReferance)
        {
            Platform = GetComponentInChildren<Platform>();
        }

    }
    private void OnDrawGizmosSelected()
    {
        if (!Application.isPlaying)
        {
            if (StartOffsetDistance != 0)
            {
                //VisualDebug.DrawGizmoPoint(transform.position + Vector3.down * StartOffsetDistance, Color.green);
                Vector3 mid = transform.position + Vector3.up * StartOffsetDistance;
                Gizmos.DrawLine(mid + Vector3.left * 4, mid + Vector3.right * 4);
            }
            if (MaxDistance > 0)
            {

                VisualDebug.DrawGizmoPoint(transform.position + Vector3.up * MaxDistance, Color.red);
                VisualDebug.DrawGizmoPoint(transform.position + Vector3.up * MinDistance, Color.blue);
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (!Application.isPlaying)
        {
            Platform = GetComponent<Platform>();
            if(Platform)
            {
                UseTransform = false;
            }
            Gizmos.DrawIcon(transform.position, "LiquidManagerControlled.png", true);
        }
        
    }
}
