﻿using UnityEngine;
using System.Collections;
/// <summary>
/// This is a object to receive player lock events from a animation or timeline or event
/// </summary>
public class PlayerMovmentLocker : MonoBehaviour
{

    public static PlayerMovmentLocker MovmentLocker;


    private void Awake()
    {
        MovmentLocker = this;///Unity LastRef only
    }

    public void LockPlayer()
    {
        GlobalData.Player.Movement.Stop();
    }
    public void UnLockPlayer()
    {
        GlobalData.Player.Movement.Restart();
    }
}
