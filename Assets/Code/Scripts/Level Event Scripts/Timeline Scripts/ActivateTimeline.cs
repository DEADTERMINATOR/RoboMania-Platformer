﻿using UnityEngine;
using System.Collections;
using UnityEngine.Playables;
/// <summary>
/// Middelware between IActivatable And Playables 
/// </summary>
public class ActivateTimeline : MonoBehaviour, IActivatable
{
    public PlayableDirector Director;
    public bool Active { get { return _running; } }


    private bool _running = false;


    public void Activate(GameObject activator)
    {
        Director.Play();
        _running = true;
    }

    public void Deactivate(GameObject activator)
    {
        Director.Stop();
        _running = false;
    }

  
}
