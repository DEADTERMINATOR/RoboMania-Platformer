﻿using UnityEngine;
using System.Collections;
using LevelSystem;
using LevelSystem.SateData;

public class LevelStateSetterBool : MonoBehaviour , IActivatable 
{
    /// <summary>
    /// The Key for the Level State 
    /// </summary>
    public string LevelStateKey = "key";
    /// <summary>
    /// The Value Th use when activated
    /// </summary>
    public bool ActivateValue = true;
    /// <summary>
    /// Start Activated 
    /// </summary>
    public bool StartActive = false;
    /// <summary>
    /// Start based on the set srating state
    /// </summary>
    public bool SetStateOnStart = false;

    public bool Active { get { return _active; } }


    private LevelState _levelState;
    private bool _active;


    public void Activate(GameObject activator)
    {
        if (!_levelState.DoseStateExists(LevelStateKey))
        {
            Level.CurentLevel.LevelState.AddStateKey(LevelStateKey);
        }
        Level.CurentLevel.LevelState.SetState(LevelStateKey, ActivateValue);
    }

    public void Deactivate(GameObject activator)
    {
        if (!_levelState.DoseStateExists(LevelStateKey))
        {
            Level.CurentLevel.LevelState.AddStateKey(LevelStateKey);
        }
        Level.CurentLevel.LevelState.SetState(LevelStateKey, !ActivateValue);
    }

    // Use this for initialization
    void Start()
    {
        _levelState = LevelState.Instance;
        if (SetStateOnStart)
        {
            if(StartActive)
            {
                Activate(null);

            }
            else
            {
                Deactivate(null);
            }
        }
    }
}
