﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HangingTrunk : MonoBehaviour
{
    private const int WATER_LAYER = 4;

    public List<DestructableObject> DestructableTrunks = new List<DestructableObject>();

    private Rigidbody2D _rigidbody;


    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.bodyType = RigidbodyType2D.Kinematic;

        foreach (DestructableObject obj in DestructableTrunks)
            obj.Destroyed += new DestructableObject.DestructionTriggered(OnDestructableTrunkDestroyed);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == WATER_LAYER) //Using the water layer is a bit of a hack, but it's the most sensible existing layer for mud that the player doesn't collide with and such that the collider can be a non-trigger collider with a rigidbody so the trunk collides with the mud.
            _rigidbody.bodyType = RigidbodyType2D.Kinematic;
    }

    private void OnDestructableTrunkDestroyed(DestructableObject trunk)
    {
        DestructableTrunks.Remove(trunk);
        if (DestructableTrunks.Count == 0)
            _rigidbody.bodyType = RigidbodyType2D.Dynamic;
    }
}
