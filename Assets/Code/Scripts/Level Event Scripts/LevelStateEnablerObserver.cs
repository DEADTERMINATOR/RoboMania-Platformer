﻿using LevelSystem;
using LevelSystem.SateData;
using UnityEngine;

public class LevelStateEnablerObserver : MonoBehaviour
{
    /// <summary>
    /// The Game object to be observred
    /// </summary>
    public GameObject ObservedGO;
    /// <summary>
    /// The State key
    /// </summary>
    public string LevelStateKey = "Enabler1";
    /// <summary>
    /// Default Case;
    /// </summary>
    public bool DefaultState = false;
    /// <summary>
    ///  Should activate object when false
    /// </summary>
    [Tooltip("GameObject should be Active while the Sate var = false")]
    public bool Inverse = false;
    private LevelStateBool _stateVar;

    private void Start()
    {
        _stateVar = Level.CurentLevel.LevelState.LoadLevelStateBool(LevelStateKey, DefaultState);
        _stateVar.AddOnChangedEventListener(ValueChaged);
        
        if (Inverse)
        {
            ObservedGO.SetActive(!_stateVar.Value);
        }
        else
        {
            ObservedGO.SetActive(_stateVar.Value);
        }
    }
  
    private void ValueChaged(int value)
    {
        if (Inverse)
        {
            ObservedGO.SetActive(!LevelState.IntToBool(value));
        }
        else
        {
            ObservedGO.SetActive(LevelState.IntToBool(value));
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawCube(transform.position, new Vector3(1, 1, 1));
        Gizmos.DrawIcon(transform.position, "observ", false);
        Gizmos.DrawLine(transform.position, ObservedGO.transform.position);
    }
}

