﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

namespace RoboMania
{
    public class Activator : MonoBehaviour, IActivatable 
    {
        /// <summary>
        /// The public facing list of objects this activator should activate.
        /// </summary>
        public List<GameObject> ObjectsToActivate = new List<GameObject>();

        /// <summary>
        /// Activates the objects over an indeterminate number of frames. Useful when the activations
        /// are computationally heavy (such as spawning an enemy).
        /// </summary>
        public bool ActivateOverTime;

        /// <summary>
        /// Whether the objects should be deactivated once the player exits the activator collider.
        /// </summary>
        public bool DeactivateOnExit = false;

        public bool OnlyTriggerActivationOnce = false;
        public float TimeOutOnReactivation = 1;
        public bool DrawDebugLines = true;
        public Color DebugLineColour = Color.red;

        public bool Active { get { return true; } }


        [SerializeField]
        protected bool activeOnStart = true;

        /// <summary>
        /// Objects that possess a tag that is in this list can trigger the activator.
        /// </summary>
        [SerializeField]
        protected List<string> _tagsAllowedToActivate = new List<string>();

        protected bool _defaultToPlayerCanActivate = false;


        /// <summary>
        /// Whether the activation of the associated item in the ObjectsToActivate list (by index) should be inverted.
        /// e.g. The object should be deactivated on activation, and activated on deactivation.
        /// </summary>
        [SerializeField]
        private List<bool> _invertObjectActivation = new List<bool>();

        /// <summary>
        /// The objects that this activator should activate.
        /// </summary>
        private List<IActivatable> _objectsToActivate;

        private bool _hasBeenActive = false;
        private float _timeSinceActivation;


        private void Awake()
        {
            _objectsToActivate = new List<IActivatable>();
            foreach (GameObject item in ObjectsToActivate)
            {
                if (item != null)
                {
                    IActivatable activatableObject;
                    InterfaceUtility.GetInterface(out activatableObject, item);
                    if (activatableObject != null)
                    {
                        _objectsToActivate.Add(activatableObject);
                    }
                }
            }
            _timeSinceActivation = TimeOutOnReactivation;

            if (_tagsAllowedToActivate.Count == 0)
            {
                _defaultToPlayerCanActivate = true;
            }

            //Quickly checking/setting the collider this activator is attached to a trigger in case we've forgotten.
            var activatorCollider = GetComponent<Collider2D>();
            if (activatorCollider != null)
            {
                activatorCollider.isTrigger = true;
            }
        }

        protected virtual void Start()
        {
            if (activeOnStart)
            {
                ActivateAllNow();
            }
        }

        protected virtual void FixedUpdate()
        {
            _timeSinceActivation += Time.deltaTime;
        }

        /// <summary>
        /// Activates any objects this activator is responsible for.
        /// </summary>
        /// <param name="collider">The collider that entered the activator trigger.</param>
        protected virtual void OnTriggerEnter2D(Collider2D collider)
        {
            if ((_defaultToPlayerCanActivate && collider.gameObject.layer == GlobalData.PLAYER_LAYER)
                || _tagsAllowedToActivate.Contains(collider.tag))
            {
                Activate(gameObject);
            }
        }

        /// <summary>
        /// Deactivates any objects this activator is resposible for.
        /// </summary>
        /// <param name="collider">The collider that exited the activator trigger.</param>
        protected virtual void OnTriggerExit2D(Collider2D collider)
        {
            if (DeactivateOnExit
                && ((_defaultToPlayerCanActivate && collider.gameObject.layer == GlobalData.PLAYER_LAYER) || _tagsAllowedToActivate.Contains(collider.tag)))
            {
                Deactivate(gameObject);
            }
        }

        public void Activate(GameObject activator)
        {
            if ((OnlyTriggerActivationOnce && !_hasBeenActive) || _timeSinceActivation >= TimeOutOnReactivation)
            {
                _hasBeenActive = true;
                if (ActivateOverTime)
                {
                    Timing.RunCoroutine(DoActivateOverTime());
                }
                else
                {
                    ActivateAllNow();
                }
            }
        }

        public virtual void Deactivate(GameObject deactivator)
        {
            if (ActivateOverTime)
            {
                Timing.RunCoroutine(DoDeactivateOverTime(true));
            }
            else
            {
                DeactivateAllNow(true);
            }
        }

        protected void ActivateAllNow()
        {
            for (int i = 0; i < _objectsToActivate.Count; ++i)
            {
                if (_invertObjectActivation.Count != 0 && _invertObjectActivation[i])
                {
                    _objectsToActivate[i].Deactivate(gameObject);
                }
                else
                {
                    _objectsToActivate[i].Activate(gameObject);
                }
            }
        }

        protected void DeactivateAllNow(bool disableActivator)
        {
            for (int i = 0; i < _objectsToActivate.Count; ++i)
            {
                if (_invertObjectActivation.Count != 0 && _invertObjectActivation[i])
                {
                    _objectsToActivate[i].Activate(gameObject);
                }
                else
                {
                    _objectsToActivate[i].Deactivate(gameObject);
                }
            }
        }

        protected IEnumerator<float> DoActivateOverTime()
        {
            for (int i = 0; i < _objectsToActivate.Count; ++i)
            {
                if (_invertObjectActivation.Count != 0 && _invertObjectActivation[i])
                {
                    _objectsToActivate[i].Deactivate(gameObject);
                }
                else
                {
                    _objectsToActivate[i].Activate(gameObject);
                }
                yield return 0f;
            }
        }

        protected IEnumerator<float> DoDeactivateOverTime(bool disableActivator)
        {
            for (int i = 0; i < _objectsToActivate.Count; ++i)
            {
                if (_invertObjectActivation.Count != 0 && _invertObjectActivation[i])
                {
                    _objectsToActivate[i].Activate(gameObject);
                }
                else
                {
                    _objectsToActivate[i].Deactivate(gameObject);
                }
                yield return 0f;
            }
        }

        private void OnDrawGizmos()
        {
            if (ObjectsToActivate != null && ObjectsToActivate.Count > 0)
            {
                Gizmos.color = DebugLineColour;
                //float size = 0.5f;
                foreach (GameObject it in ObjectsToActivate)
                {
                    if (it)
                    {
                        Gizmos.DrawLine(transform.position, it.transform.position);
                    }
                }
            }
        }
    }
}
