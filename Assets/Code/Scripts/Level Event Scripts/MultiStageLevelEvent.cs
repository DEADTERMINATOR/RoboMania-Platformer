﻿using System;
using System.Collections.Generic;
using UnityEngine;
using LevelSystem.SateData;

namespace LevelEventStageSystem
{
    public class MultiStageLevelEvent : MonoBehaviour, IActivatable
    {
        public List<LevelEventStage> stages;
        public int StartingStage = 0;

        public bool Active { get { return _running; } } 


        /// <summary>
        /// Only true if Starting stage is 0
        /// </summary>
        public bool RunAllStagesBeForeStartingStage;
        public bool StartActive;
        public string LevelStateKey; 
        private int _intenalStateCount = 0;
        private bool _running;
        private LevelStateInt StageLevelState = null;

        public void Start()
        {
            _running = StartActive;
            if(!String.IsNullOrEmpty(LevelStateKey))
            {
                StageLevelState = LevelState.Instance.LoadLevelStateInt(LevelStateKey, StartingStage);
            }
            if (StartingStage != 0 && RunAllStagesBeForeStartingStage)
            {
                for(int i = 0;  i <  StartingStage;  i ++ )
                {
                    stages[_intenalStateCount].UpdateStage(1);
                }
            }
           _intenalStateCount = StartingStage;
           
        }

        public void Update()
        {
            if (_running)
            {
                if (stages[_intenalStateCount].IsDone())// if crent stage is done move to the next and  
                {
                    _intenalStateCount++;
                    if(StageLevelState != null)
                    {
                        StageLevelState.Value = _intenalStateCount;
                    }
                    if(_intenalStateCount >= stages.Count)
                    {
                        _running = false;
                        return;
                    }
                }
                stages[_intenalStateCount].UpdateStage(Time.deltaTime);
            }
        }

        public void Activate(GameObject activator)
        {
            _running = !_running;
        }

        public void Deactivate(GameObject activator)
        {
            _running = !_running;
        }
    }
}

