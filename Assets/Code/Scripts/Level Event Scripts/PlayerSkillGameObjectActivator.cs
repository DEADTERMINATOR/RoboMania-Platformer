﻿using UnityEngine;
using static GlobalData;

public class PlayerSkillGameObjectActivator : MonoBehaviour
{
    public PlayerDynamicModules.Enum ModuleToObserve;
    public GameObject ObjectToActivate;
    public bool DeactivateIfPlayerHas;
    private void Awake()
    {
        //Needles one line Active = PlayerHas XOR DeactivateIfPlayerHas
        ObjectToActivate.SetActive(Player.Data.GetModuleIfPossessed(PlayerDynamicModules.EnumToString(ModuleToObserve)) ^ DeactivateIfPlayerHas);
    }
}

