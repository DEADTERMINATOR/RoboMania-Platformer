﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MovementEffects;

/// <summary>
/// Handles the loading of the next area the player will travel to.
/// </summary>
public class LoadArea : MonoBehaviour
{
    /// <summary>
    /// The string name of the scene that will be loaded.
    /// </summary>
    public string SceneName;


    private static bool _behaviorManagerSaved;

    /// <summary>
    /// The collider trigger that begins the loading of the associated area.
    /// </summary>
    private BoxCollider2D _loadTrigger;


	// Use this for initialization
	private void Start ()
    {
        _loadTrigger = GetComponent<BoxCollider2D>();
	}

    /// <summary>
    /// Triggers the co-routine that loads the next area when the player crosses the trigger.
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            if (!SceneManager.GetSceneByPath(SceneName).isLoaded)
            {
                Timing.RunCoroutine(LoadAreaAsync(), "Load Scene " + SceneName);
            }

            if (!_behaviorManagerSaved)
            {
                GameObject behaviorManager = GameObject.Find("Behavior Manager");
                if (behaviorManager != null)
                {
                    DontDestroyOnLoad(behaviorManager);
                    _behaviorManagerSaved = true;
                }
            }
        }
    }

    /// <summary>
    /// Loads the designated area of the level.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> LoadAreaAsync()
    {
        AsyncOperation op = SceneManager.LoadSceneAsync(SceneName, LoadSceneMode.Additive);
        //op.allowSceneActivation = false;
        yield return 0f;
    }
}
