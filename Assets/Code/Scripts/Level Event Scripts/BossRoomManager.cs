﻿using UnityEngine;
using System.Collections;
using System;
using Characters.Enemies.Bosses;

[RequireComponent(typeof(BoxCollider2D))]
public class BossRoomManager : MonoBehaviour
{
    BossRoomData data = new BossRoomData();

    public Boss BossRef;
    public GameObject BossActivator;
    public GameObject BossKilledActivator;

    private IActivatable _bossActivator;
    private IActivatable _bossKilledActivator;



    private void Awake()
    {
        data.HasBeenSpaned = false;
        data.WasKilled = false;
        _bossActivator = BossActivator.GetComponent<IActivatable>();
        _bossKilledActivator = BossKilledActivator.GetComponent<IActivatable>();

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == GlobalData.PLAYER_TAG)
        {

            if (!data.HasBeenSpaned)
            {
                if (_bossActivator != null)
                    _bossActivator.Activate(gameObject);
                data.HasBeenSpaned = true;
            }
        }
    }

    public void Update()
    {
        if (data.HasBeenSpaned)
        {
            if (BossRef.Health <= 0)
            {
                data.HasBeenSpaned = false;
                if (_bossKilledActivator != null)
                    _bossKilledActivator.Activate(gameObject);
            }
        }
    }
}
[Serializable()]
public struct BossRoomData
{
    public bool HasBeenSpaned;
    public bool WasKilled;

}
