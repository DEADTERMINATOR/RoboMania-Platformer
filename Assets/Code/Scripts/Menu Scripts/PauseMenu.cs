﻿using Inputs;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using RoboMania.UI;
using LevelSystem;

namespace RoboMania.UI
{
    public class PauseMenu : Menu
    {
        private enum PauseMenuControls { ReturnToGame = 1, ReturnToMainMenu = 2, MusicVolume = 3 }

        private const float SLIDER_STEP_VALUE = 0.025f;

        /// <summary>
        /// Is the game in a paused state.
        /// </summary>
        public bool Paused = false;

        /// <summary>
        /// Reference to the button that unpauses the game.
        /// </summary>
        public Button ReturnToGameButton;

        /// <summary>
        /// Reference to the button that returns the player to the main menu scene.
        /// </summary>
        public Button ReturnToMainMenuButton;

        /// <summary>
        /// Slider for controlling the volume of the game's music.
        /// </summary>
        public Slider MusicVolumeSlider;

        /// <summary>
        /// The image in the background of the pause menu
        /// </summary>
        public Image Background;

        /// <summary>
        /// The name of the Main Menu scene
        /// </summary>
        public string MainMenuSceneName;

        public AudioMixer mixerMusic;


        private InputManager _inputManagerInstanceRef;
        private PopUpHandle _areYouSurePopUp;

        public InputManager.RegisteredCallback SelectCallback;
        public InputManager.RegisteredCallback BackCallback;
        public InputManager.RegisteredCallback LeftCallback;
        public InputManager.RegisteredCallback RightCallback;
        public InputManager.RegisteredCallback PausedCallback;
        public InputManager.RegisteredCallback UnpasuedCallback;

        private void Start()
        {
            ReturnToGameButton.onClick.AddListener(() => OnButtonClicked((int)PauseMenuControls.ReturnToGame));
            ReturnToMainMenuButton.onClick.AddListener(() => OnButtonClicked((int)PauseMenuControls.ReturnToMainMenu));
            MusicVolumeSlider.onValueChanged.AddListener((value) => ChangeMusicVolume(value));

            //Setup on mouse enter and exit listeners
            //Generic callback whenever a button stops being hovered over.
            UnityEngine.Events.UnityAction<BaseEventData> onHoverStop = (data) => OnMenuControlStopHover();

            //Return to Game Button
            var trigger = ReturnToGameButton.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) => OnMenuControlStartHover((int)PauseMenuControls.ReturnToGame));
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener(onHoverStop);
            trigger.triggers.Add(entry);

            //Return to Main Menu Button
            trigger = ReturnToMainMenuButton.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) => OnMenuControlStartHover((int)PauseMenuControls.ReturnToMainMenu));
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener(onHoverStop);
            trigger.triggers.Add(entry);

            //Music Volume Slider
            trigger = MusicVolumeSlider.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) => OnMenuControlStartHover((int)PauseMenuControls.MusicVolume));
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener(onHoverStop);
            trigger.triggers.Add(entry);

            _inputManagerInstanceRef = InputManager.Instance;

            _registeredInputCallbacks.Add(_inputManagerInstanceRef.RegisterSpecificActionPerformed("Up", () =>
            {
                if (_currentlySelectedControlIterator > 1)
                {
                    --_currentlySelectedControlIterator;
                    ChangeSelectedControl(_currentlySelectedControlIterator);
                }
            }));
            _registeredInputCallbacks.Add(_inputManagerInstanceRef.RegisterSpecificActionPerformed("Down", () =>
            {
                if (_currentlySelectedControlIterator <= 2)
                {
                    ++_currentlySelectedControlIterator;
                    ChangeSelectedControl(_currentlySelectedControlIterator);
                }
            }));
            _registeredInputCallbacks.Add(_inputManagerInstanceRef.RegisterSpecificActionPerformed("Right", () =>
            {
                if (_currentlySelectedControlIterator == (int)PauseMenuControls.MusicVolume)
                {
                    ChangeMusicVolume(MusicVolumeSlider.value + SLIDER_STEP_VALUE);
                }
            }));
            _registeredInputCallbacks.Add(_inputManagerInstanceRef.RegisterSpecificActionPerformed("Left", () =>
            {
                if (_currentlySelectedControlIterator == (int)PauseMenuControls.MusicVolume)
                {
                    ChangeMusicVolume(MusicVolumeSlider.value - SLIDER_STEP_VALUE);
                }
            }));

            _registeredInputCallbacks.Add(_inputManagerInstanceRef.RegisterSpecificActionPerformed("Select", () =>
            {
                OnButtonClicked(_currentlySelectedControlIterator);
            }));
            _registeredInputCallbacks.Add(_inputManagerInstanceRef.RegisterSpecificActionPerformed("Back", () =>
            {
                ReturnToGame();
            }));

            PausedCallback = _inputManagerInstanceRef.RegisterSpecificActionPerformed("Pause", () =>
            {
                SwitchPausedState();
            });
            UnpasuedCallback = _inputManagerInstanceRef.RegisterSpecificActionPerformed("Unpause", () =>
            {
                SwitchPausedState();
            });

            SetAllInBackGroundActive(false);
        }

        protected override void ChangeSelectedControl(int selectedControl)
        {
            EventSystem.current.SetSelectedGameObject(null);

            switch (selectedControl)
            {
                case (int)PauseMenuControls.ReturnToGame:
                    ReturnToGameButton.Select();
                    break;
                case (int)PauseMenuControls.ReturnToMainMenu:
                    ReturnToMainMenuButton.Select();
                    break;
                case (int)PauseMenuControls.MusicVolume:
                    MusicVolumeSlider.Select();
                    break;
            }
        }

        protected override void OnButtonClicked(int selectedButton)
        {
            switch (selectedButton)
            {
                case (int)PauseMenuControls.ReturnToGame:
                    ReturnToGame();
                    break;
                case (int)PauseMenuControls.ReturnToMainMenu:
                    ReturnToMainMenu();
                    break;
            }
        }

        private void ReturnToMainMenu()
        {
            SetAllInBackGroundActive(false);
            DisableRegisteredInputCallbacks();
            InputManager.Instance.SetEnabledRegisteredInputCallback(PausedCallback,false);
            InputManager.Instance.SetEnabledRegisteredInputCallback(UnpasuedCallback, false);
            if (_areYouSurePopUp == null)
            {
                _areYouSurePopUp = new PopUpHandle("Exit?", "Are you sure you want to exit the game");
                _areYouSurePopUp.OnOK.AddListener(() =>
                {
                    GlobalData.Player.Save();
                    CleanupRegisteredInputCallbacks();
                    SwitchPausedState();
                    LevelSystem.Level.CurentLevel.ExitLevel(false);
                });
                _areYouSurePopUp.OnCancel.AddListener(() =>
                {
                    SetAllInBackGroundActive(true);
                    EnableRegisteredInputCallbacks();

                // If a controller is connected, immediately highlight the first button on the menu.
                if (_inputManagerInstanceRef.ControllerConnected)
                {
                    _currentlySelectedControlIterator = (int)PauseMenuControls.ReturnToGame;
                    ChangeSelectedControl(_currentlySelectedControlIterator);
                }
                });
            }

            UIPopUp.Show(_areYouSurePopUp);
        }

        private void ReturnToGame()
        {
            SwitchPausedState();
        }

        private void ChangeMusicVolume(float value)
        {
            MusicVolumeSlider.value = value;
            mixerMusic.SetFloat("MusicVolume", StaticTools.LinearToDecibel(value));
        }

        private void SetAllInBackGroundActive(bool enable)
        {
            Background.enabled = enable;
            foreach (Transform child in Background.transform)
            {
                child.gameObject.SetActive(enable);
            }
        }

        private void SwitchPausedState()
        {
            if (!Paused)
            {
                if (Level.CurentLevel.ReqestLevlePause())
                {
                    DisableRegisteredInputCallbacks();

                    Paused = true;

                    _inputManagerInstanceRef.EnableMenuActions();

                    SetAllInBackGroundActive(Paused);

                    // If a controller is connected, immediately highlight the first button on the menu.
                    if (_inputManagerInstanceRef.ControllerConnected)
                    {
                        _currentlySelectedControlIterator = (int)PauseMenuControls.ReturnToGame;
                        ChangeSelectedControl(_currentlySelectedControlIterator);
                    }
                }
            }
            else
            {
                if (Level.CurentLevel.ReqestLevleUnpause())
                {
                    EnableRegisteredInputCallbacks();
                    Paused = false;


                    _inputManagerInstanceRef.DisableMenuActions();

                    SetAllInBackGroundActive(Paused);
                }
            }
        }
    }
}

