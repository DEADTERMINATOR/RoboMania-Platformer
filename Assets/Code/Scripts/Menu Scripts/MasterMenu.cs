﻿using GameMaster;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using Inputs;

namespace RoboMania.UI
{
    public class MasterMenu : Menu
    {
        private enum MainButtons { NewGame = 1, Settings = 2, Exit = 3 }
        private enum StartGameButtons { MainGame = 1, PlatformingGauntlet = 2, NPCWave = 3, LoadSave = 4, UseDebug = 5 }


        /// <summary>
        /// Buttons on the Main Menu Section
        /// </summary>
        #region Main Buttons
        public Button NewGameButton;
        public Button SettingsButton;
        public Button ExitButton;
        #endregion

        #region Overlays
        public GameObject LoadingOverlay;
        public GameObject StartGameDebugMenu;
        #endregion

        #region Start Game Overlay Elements
        public Button StartFromBeginButton;
        public Button StartGauntletButton;
        public Button StartWaveButton;
        public Button LoadSaveButton;
        public Image SaveIndicator;

        public Image DebugIndicator;
        public Button DebugButton;
        #endregion


        private bool _startGameExpanded = false;
#pragma warning disable 414 //value is assigned but never used
        private bool _settingsExpanded = false; //This will be used once there is a settings menu implemented.
#pragma warning restore 414 //value is assigned but never used


        protected void Start()
        {
            //Setup on click listeners
            NewGameButton.onClick.AddListener(() => OnButtonClicked((int)MainButtons.NewGame));
            //TODO: Settings button here
            ExitButton.onClick.AddListener(() => OnButtonClicked((int)MainButtons.Exit));

            StartFromBeginButton.onClick.AddListener(() => OnButtonClicked((int)StartGameButtons.MainGame));
            StartGauntletButton.onClick.AddListener(() => OnButtonClicked((int)StartGameButtons.PlatformingGauntlet));
            StartWaveButton.onClick.AddListener(() => OnButtonClicked((int)StartGameButtons.NPCWave));
            LoadSaveButton.onClick.AddListener(() => OnButtonClicked((int)StartGameButtons.LoadSave));
            DebugButton.onClick.AddListener(() => OnButtonClicked((int)StartGameButtons.UseDebug));

            //Setup on mouse enter and exit listeners
            //Generic callback whenever a button stops being hovered over.
            UnityEngine.Events.UnityAction<BaseEventData> onHoverStop = (data) => OnMenuControlStopHover();

            //New Game Button
            var trigger = NewGameButton.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) => OnMenuControlStartHover((int)MainButtons.NewGame));
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener(onHoverStop);
            trigger.triggers.Add(entry);

            //Settings Button
            trigger = SettingsButton.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) => OnMenuControlStartHover((int)MainButtons.Settings));
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener(onHoverStop);
            trigger.triggers.Add(entry);

            //Exit Button
            trigger = ExitButton.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) => OnMenuControlStartHover((int)MainButtons.Exit));
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener(onHoverStop);
            trigger.triggers.Add(entry);

            //Start From Beginning Button
            trigger = StartFromBeginButton.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) => OnMenuControlStartHover((int)StartGameButtons.MainGame));
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener(onHoverStop);
            trigger.triggers.Add(entry);

            //Start Platforming Gauntlet Button
            trigger = StartGauntletButton.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) => OnMenuControlStartHover((int)StartGameButtons.PlatformingGauntlet));
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener(onHoverStop);
            trigger.triggers.Add(entry);

            //Start NPC Wave Button
            trigger = StartWaveButton.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) => OnMenuControlStartHover((int)StartGameButtons.NPCWave));
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener(onHoverStop);
            trigger.triggers.Add(entry);

            //Load Save Button
            trigger = LoadSaveButton.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) => OnMenuControlStartHover((int)StartGameButtons.LoadSave));
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener(onHoverStop);
            trigger.triggers.Add(entry);

            //Use Debug Button
            trigger = DebugButton.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((data) => OnMenuControlStartHover((int)StartGameButtons.UseDebug));
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener(onHoverStop);
            trigger.triggers.Add(entry);

            //Turn off overlays
            StartGameDebugMenu.SetActive(false);
            LoadingOverlay.SetActive(false);

            //Setup controller callbacks.
            InputManager instance = InputManager.Instance;
            instance.EnableMenuActions();

            _registeredInputCallbacks.Add(instance.RegisterSpecificActionPerformed("Up", () =>
            {
                if (_currentlySelectedControlIterator > 1)
                {
                    --_currentlySelectedControlIterator;
                    ChangeSelectedControl(_currentlySelectedControlIterator);
                }
            }));
            _registeredInputCallbacks.Add(instance.RegisterSpecificActionPerformed("Down", () =>
            {
                if (_startGameExpanded && _currentlySelectedControlIterator <= 4)
                {
                    ++_currentlySelectedControlIterator;
                    ChangeSelectedControl(_currentlySelectedControlIterator);
                }
                else if (_currentlySelectedControlIterator <= 2)
                {
                    ++_currentlySelectedControlIterator;
                    ChangeSelectedControl(_currentlySelectedControlIterator);
                }
            }));

            _registeredInputCallbacks.Add(instance.RegisterSpecificActionPerformed("Select", () =>
            {
                OnButtonClicked(_currentlySelectedControlIterator);
            }));
            _registeredInputCallbacks.Add(instance.RegisterSpecificActionPerformed("Back", () =>
            {
                if (_startGameExpanded)
                {
                    OnGoBackFromNewGame();
                    ResetButtonIterator();
                }
            }));

            // If a controller is connected from a start, immediately highlight the first button on the menu.
            if (instance.ControllerConnected)
            {
                _currentlySelectedControlIterator = 1;
                NewGameButton.Select();
            }
        }

        protected override void OnButtonClicked(int buttonIteratorCount)
        {
            if (_startGameExpanded)
            {
                GameManager gameManager = GameManager.Instance;
                switch (buttonIteratorCount)
                {
                    case (int)StartGameButtons.MainGame:
                        {
                            //set the overide scene to null;

                            InputManager.Instance.DisableMenuActions();
                            CleanupRegisteredInputCallbacks();
                            PopUpHandle newGamePopUP = UIPopUp.Make("New Game?", "This will erase any level save data", "Yes", "No");
                            newGamePopUP.OnOK.AddListener(() => { GlobalData.IsANewGame = true; });
                            newGamePopUP.OnAny.AddListener(() => { gameManager.LoadLevelByIndex(1); });
                            UIPopUp.Show(newGamePopUP);

                            break;
                        }
                    case (int)StartGameButtons.PlatformingGauntlet:
                        {
                            InputManager.Instance.DisableMenuActions();
                            CleanupRegisteredInputCallbacks();

                            gameManager.LoadLevelByIndex(2);

                            break;
                        }
                    case (int)StartGameButtons.NPCWave:
                        {
                            InputManager.Instance.DisableMenuActions();
                            CleanupRegisteredInputCallbacks();

                            gameManager.LoadLevelByIndex(3);

                            break;
                        }
                    case (int)StartGameButtons.LoadSave:
                        {
                            GlobalData.IsANewGame = !GlobalData.IsANewGame;
                            if (!GlobalData.IsANewGame)
                            {
                                SaveIndicator.color = Color.green;
                            }
                            else
                            {
                                SaveIndicator.color = Color.red;
                            }

                            break;
                        }
                    case (int)StartGameButtons.UseDebug:
                        {
                            GlobalData.UseDebugSettings = !GlobalData.UseDebugSettings;
                            if (GlobalData.UseDebugSettings)
                            {
                                DebugIndicator.color = Color.green;
                            }
                            else
                            {
                                DebugIndicator.color = Color.red;
                            }

                            break;
                        }
                }
            }
            else
            {
                switch (buttonIteratorCount)
                {
                    case (int)MainButtons.NewGame:
                        {
                            StartGameDebugMenu.SetActive(true);
                            _startGameExpanded = true;

                            ResetButtonIterator();

                            break;
                        }
                    case (int)MainButtons.Settings:
                        break;
                    case (int)MainButtons.Exit:
                        Application.Quit();
                        break;
                }
            }
        }

        protected override void ChangeSelectedControl(int selectedButton)
        {
            if (_startGameExpanded)
            {
                switch (selectedButton)
                {
                    case (int)StartGameButtons.MainGame:
                        StartFromBeginButton.Select();
                        break;
                    case (int)StartGameButtons.PlatformingGauntlet:
                        StartGauntletButton.Select();
                        break;
                    case (int)StartGameButtons.NPCWave:
                        StartWaveButton.Select();
                        break;
                    case (int)StartGameButtons.LoadSave:
                        LoadSaveButton.Select();
                        break;
                    case (int)StartGameButtons.UseDebug:
                        DebugButton.Select();
                        break;
                }
            }
            else
            {
                switch (selectedButton)
                {
                    case (int)MainButtons.NewGame:
                        NewGameButton.Select();
                        break;
                    case (int)MainButtons.Settings:
                        SettingsButton.Select();
                        break;
                    case (int)MainButtons.Exit:
                        ExitButton.Select();
                        break;
                }
            }
        }

        public void OnGoBackFromNewGame()
        {
            StartGameDebugMenu.SetActive(false);
            _startGameExpanded = false;

            ResetButtonIterator();
        }
    }
}

