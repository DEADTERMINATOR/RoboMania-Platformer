﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelSelectionMenu : MonoBehaviour
{
    public List<Button> Buttons;

    // Use this for initialization
    protected void Start()
    {
        foreach (Button button in Buttons)
        {
            LevelSelectionButton levelSelectionButton = button.GetComponent<LevelSelectionButton>();
            if (levelSelectionButton != null)
            {
                button.onClick.AddListener(delegate { OnButtonCliked(levelSelectionButton.levelToLoad); });
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnButtonCliked(LevelLoadingInfo loadingInfo )
    {
      /*  newStartingScene = null;
        nextLevel = loadingInfo;
        LoadLevel();*/
    }
}
