﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Inputs;
using static Inputs.InputManager;

namespace RoboMania.UI
{
    public abstract class Menu : MonoBehaviour
    {
        protected int _currentlySelectedControlIterator = 0;
        protected List<RegisteredCallback> _registeredInputCallbacks = new List<RegisteredCallback>();


        protected void OnMenuControlStartHover(int buttonIteratorCount)
        {
            _currentlySelectedControlIterator = buttonIteratorCount;
            ChangeSelectedControl(buttonIteratorCount);
        }

        protected void OnMenuControlStopHover()
        {
            _currentlySelectedControlIterator = 0;
            EventSystem.current.SetSelectedGameObject(null);
        }

        protected void ResetButtonIterator()
        {
            if (InputManager.Instance.ControllerConnected)
            {
                _currentlySelectedControlIterator = 1;
            }
            else
            {
                _currentlySelectedControlIterator = 0;
            }

            ChangeSelectedControl(_currentlySelectedControlIterator);
        }

        /// <summary>
        /// Disables (but does not cleanup) any registered input callbacks so they are not processed. Use this if the callbacks
        /// only need to be ignored for a short period of time.
        /// </summary>
        protected void DisableRegisteredInputCallbacks()
        {
            foreach (RegisteredCallback callback in _registeredInputCallbacks)
            {
                InputManager.Instance.SetEnabledRegisteredInputCallback(callback, false);
            }
        }

        /// <summary>
        /// Re-enables any registered input callbacks so they are processed once again. Does nothing if they were already enabled.
        /// </summary>
        protected void EnableRegisteredInputCallbacks()
        {
            foreach (RegisteredCallback callback in _registeredInputCallbacks)
            {
                InputManager.Instance.SetEnabledRegisteredInputCallback(callback, true);
            }
        }

        /// <summary>
        /// Cleans up any registered input callbacks so they are not processed after the menu has been destroyed/deactivated.
        /// </summary>
        protected void CleanupRegisteredInputCallbacks()
        {
            foreach (RegisteredCallback callback in _registeredInputCallbacks)
            {
                InputManager.Instance.RemoveRegisteredInputCallback(callback);
            }
        }


        protected abstract void OnButtonClicked(int selectedButton);
        protected abstract void ChangeSelectedControl(int selectedControl);
    }
}
