﻿using System;
using UnityEngine;

namespace LegacyAnimationHelpers
{

    /*public struct ManagedAnimatinClip
    {
        public AnimationClip Clip;
        public WrapMode WrapMode;
        public ManagedAnimatinClip(AnimationClip clip, WrapMode wrapMode = WrapMode.Default)
        {
            Clip = clip;
            WrapMode = wrapMode;
        }
    }

    public class AnimationClipsManager 

    {

        public AnimationClipsManager(Animation animationComponent, ManagedAnimatinClip[] clips )
        {
            _animationComponent = animationComponent;
            foreach(ManagedAnimatinClip managedClip in clips )
            {
                AddManageedClip(managedClip);
            }
            
        }
        /// <summary>
        /// The animation component 
        /// </summary>
        private  Animation _animationComponent;

        public AnimationClip _lastPlayed;
        /// <summary>
        /// will play the clip if it has been added 
        /// </summary>
        /// <param name="clip"></param>
        /// <param name="playMode"></param>
        /// <returns>True if the clip was found false if not</returns>
        public bool Play(AnimationClip clip, PlayMode playMode = PlayMode.StopAll)      
        {
            if (_animationComponent[clip.name] != null)// make sure not to use a non added clip
            {
                _animationComponent.Play(clip.name, playMode);
                _lastPlayed = clip;
                return true;
            }
            return false;
        }
        /// <summary>
        /// will play the clip if it has been added 
        /// </summary>
        /// <param name="ManagedClip"></param>
        /// <param name="playMode"></param>
        /// <returns>True if the clip was found false if not</returns>
        public bool Play(ManagedAnimatinClip ManagedClip, PlayMode playMode = PlayMode.StopAll)
        {
            return Play(ManagedClip, playMode);
        }
        public void CrossFade(AnimationClip clip, float time = 0.2f)
        {
            if (_animationComponent[clip.name] != null)// make sure not to use a non added clip
            {
                _animationComponent.CrossFade(clip.name, time);
            }
        }
        public void CrossFade(ManagedAnimatinClip ManagedClip, float time = 0.2f)
        {
            CrossFade(ManagedClip.Clip, time);
        }

        public bool IsDonePlaying(AnimationClip clip)
        {
            return (_lastPlayed == clip && !_animationComponent.IsPlaying(clip.name));

        }

        public void SetDefaultState(AnimationClip clip)
        {
            if (_animationComponent[clip.name] != null)// make sure not to use a non added clip
            {
                _animationComponent.clip = clip;
                Play(clip);
            }
        }

        public void SetDefaultState(ManagedAnimatinClip ManagedClip)
        {
            SetDefaultState(ManagedClip.Clip);
        }

        public void AddManageedClip(AnimationClip clip, WrapMode wrapMode = WrapMode.Loop)
        {
            _animationComponent.AddClip(clip, clip.name);
            _animationComponent[clip.name].wrapMode = wrapMode;
        }

        public void AddManageedClip(ManagedAnimatinClip ManagedClip)
        {
            AddManageedClip(ManagedClip.Clip, ManagedClip.WrapMode);
        }
    }*/
}
