﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpGameObjectOnActivate : MonoBehaviour, IActivatable
{
    public bool Active { get; private set; }
    
    [RequireInterface(typeof(IWarpable))]
    [SerializeField]
    private Object _objectToWarp;
    [SerializeField]
    private Vector2 _localPositionToWarpTo;
    [SerializeField]
    private Vector3 _objectRotationAfterWarp;
    [SerializeField]
    private bool _oneTimeWarpOnly = false;
    [SerializeField]
    private float _timeOutOnWarp = 15;

    private Vector2 _globalWarpPosition;
    private bool _hasWarpedAtLeastOnce = false;
    private float _timeSinceLastWarp = 0;


    private void Awake()
    {
        _globalWarpPosition = _localPositionToWarpTo + (Vector2)transform.position;
        _timeSinceLastWarp = _timeOutOnWarp;
    }

    private void Update()
    {
        _timeSinceLastWarp += Time.deltaTime;
    }

    public void Activate(GameObject activator)
    {
        if ((_oneTimeWarpOnly && !_hasWarpedAtLeastOnce) || (!_oneTimeWarpOnly && _timeSinceLastWarp >= _timeOutOnWarp))
        {
            var castedObject = _objectToWarp as IWarpable;
            castedObject.WarpToPosition(_globalWarpPosition, _objectRotationAfterWarp);

            _hasWarpedAtLeastOnce = true;
            _timeSinceLastWarp = 0;

            Active = true;
        }
    }

    public void Deactivate(GameObject activator)
    {
        Active = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        float size = 0.3f;

        Vector3 globalWaypointPos = Application.isPlaying ? _globalWarpPosition : _localPositionToWarpTo + (Vector2)transform.position;
        Gizmos.DrawLine(globalWaypointPos - Vector3.up * size, globalWaypointPos + Vector3.up * size);
        Gizmos.DrawLine(globalWaypointPos - Vector3.left * size, globalWaypointPos + Vector3.left * size);
    }
}
