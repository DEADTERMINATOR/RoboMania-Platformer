﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformingGauntletStartData : LevelStartData
{
    public bool ResetSecondEnergyCell = false;

    public bool ResetThirdEnergyCell = false;


    public override void ResetLevelParameters()
    {
        base.ResetLevelParameters();
        if (SetParametersForReset)
        {
            if (ResetSecondEnergyCell)
                ES3.Save<bool>("PlatformingGauntletCellTwo", false, "PlatformingGauntlet.txt");
            if (ResetThirdEnergyCell)
                ES3.Save<bool>("PlatformingGauntletCellThree", false, "PlatformingGauntlet.txt");
        }
    }
}
