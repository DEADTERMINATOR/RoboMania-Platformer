﻿using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem.XR.Haptics;
using UnityEngine.Events;

public class LevelSectionScript : MonoBehaviour
{
    public string SectionName;
    public bool IsStartingZone;
    public GameObject MainActivaotr;

    public static string CurrentSectionName { get; private set; }
    public static UnityEvent OnUpdated = new UnityEvent();
    private IActivatable _activatable;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject ==GlobalData.Player.gameObject)
        {
            CurrentSectionName = SectionName;
            OnUpdated?.Invoke();
            _activatable?.Activate(null);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == GlobalData.Player.gameObject)
        {
            _activatable?.Deactivate(null);
        }
    }

    // Use this for initialization
    void Start()
    {
        if (MainActivaotr != null)
        {
            InterfaceUtility.GetInterface(out _activatable, MainActivaotr);
        }
        if (IsStartingZone)
        {
            CurrentSectionName = SectionName;
            OnUpdated?.Invoke();
            _activatable?.Activate(null);
        }
    }

}
