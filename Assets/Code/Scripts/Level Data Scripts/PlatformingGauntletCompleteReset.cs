﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformingGauntletCompleteReset : MonoBehaviour
{
    private PlatformingGauntletStartData _startData;

    //private List<WorldCheckpoint> _checkpoints = new List<WorldCheckpoint>();


    private void Start()
    {
        _startData = GameObject.Find("PlatformingGauntlet Starting Point").GetComponent<PlatformingGauntletStartData>();

        GameObject[] checkpoints = GameObject.FindGameObjectsWithTag("Checkpoint");
        //foreach (GameObject checkpoint in checkpoints)
           // _checkpoints.Add(checkpoint.GetComponent<WorldCheckpoint>());
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        _startData.StartingNumberOfEnergyCells = 1;
        _startData.ResetSecondEnergyCell = true;
        _startData.ResetThirdEnergyCell = true;

       /* foreach (WorldCheckpoint checkpoint in _checkpoints)
        {
            if (checkpoint.DisableCheckpointOnActivation && ES3.KeyExists("Disable " + checkpoint.name, "PlatformingGauntlet.txt"))
                ES3.Save<bool>("Restore " + checkpoint.name, true, "PlatformingGauntlet.txt");
        }*/

        _startData.ResetLevelParameters();
    }
}
