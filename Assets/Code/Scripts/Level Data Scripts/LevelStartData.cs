﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelStartData : MonoBehaviour
{
    public string FileToSaveAndLoadFrom = "Save.txt";

    //public WorldCheckpoint StartAtSpecifiedCheckpoint;

    public bool SetParametersForReset = false;

    public int StartingNumberOfEnergyCells = 1;


    public virtual void ResetLevelParameters()
    {
       /* ES3.Save<Vector3>("Last Checkpoint", StartAtSpecifiedCheckpoint == null ? transform.position : StartAtSpecifiedCheckpoint.transform.position, FileToSaveAndLoadFrom);
        if (SetParametersForReset)
        {
            ES3.Save<float>("Total Energy", StartingNumberOfEnergyCells * 100f, FileToSaveAndLoadFrom);
        }*/
    }
    /*
    private void Awake()
    {
        if (!StartAtLastCheckpoint)
        {
            if (StartAtSpecifiedCheckpoint == null)
            {
                ES3.Save<float>("Total Energy", 100f, "PlatformingGauntlet.txt");
                ES3.Save<bool>("PlatformingGauntletCellTwo", false, "PlatformingGauntlet.txt");
                ES3.Save<Vector3>("Last Checkpoint", transform.position, "PlatformingGauntlet.txt");

                StartAtLastCheckpoint = true;
            }
            else
            {
                ES3.Save<float>("Total Energy", StartAtSpecifiedCheckpoint.NumberOfEnergyCellsToGivePlayer * 100, "PlatformingGauntlet.txt");
                ES3.Save<Vector3>("Last Checkpoint", StartAtSpecifiedCheckpoint.transform, "PlatformingGauntlet.txt");
            }
        }
    }
    */
}
