﻿using UnityEngine;

class RemoveOnActivation : MonoBehaviour, IActivatable
{
    public bool Active { get { return !gameObject.activeSelf; } }

    public void Activate(GameObject activator)
    {
        gameObject.SetActive(false);
    }

    public void Deactivate(GameObject activator)
    {

    }
}

