﻿using UnityEngine;
using System.Collections;

public class GameObjectAlias : MonoBehaviour
{
    public GameObject RealGameObject;


    Vector3 GizmoSize = new Vector3(5, 5, 5);

    private void Awake()
    {
        GameObject Copy = Instantiate(RealGameObject);
        Copy.transform.parent = transform.parent;
        Copy.transform.position = RealGameObject.transform.position;
    }

    /* private void OnEnable()
     {
         Debug.Log(RealGameObject.name + "Was Enabled");
         RealGameObject.SetActive(true);
     }

     private void OnDisable()
     {
         Debug.Log(RealGameObject.name + "Was Disabled");
         RealGameObject.SetActive(false);
     }*/

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(RealGameObject.transform.position, GizmoSize);
    }

   /* public void Update()
     {
         if(RealGameObject.activeInHierarchy != gameObject.activeInHierarchy)
         {
             gameObject.SetActive(RealGameObject.activeInHierarchy);
         }
     }*/
}
