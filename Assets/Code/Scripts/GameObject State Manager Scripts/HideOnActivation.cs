﻿using UnityEngine;
using System.Collections;

public class HideOnActivation : MonoBehaviour , IActivatable 
{
    public bool Active { get { return !gameObject.activeSelf; } }

    public void Activate(GameObject activator)
    {
        gameObject.SetActive(false);
    }

    public void Deactivate(GameObject activator)
    {
        gameObject.SetActive(false);
    }

   
}
