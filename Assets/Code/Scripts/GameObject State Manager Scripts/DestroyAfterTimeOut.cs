﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTimeOut : MonoBehaviour
{
    public float timeOut;
    /// <summary>
    /// The point to spawn the obj Relative To The spawner
    /// </summary>

    private float _time;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        _time += Time.deltaTime;
        if (_time >= timeOut)
        {
            //BUGFIX: Destroy 
            gameObject.SetActive(false);
            Destroy(gameObject, 1);
        }
    }
}
