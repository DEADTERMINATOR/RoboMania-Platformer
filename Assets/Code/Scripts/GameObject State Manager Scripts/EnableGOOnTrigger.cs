﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnableGOOnTrigger : MonoBehaviour
{
    public List<GameObject> Objects;

    public bool StartingState;
   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == GlobalData.PLAYER_TAG)
        {
            Activate();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == GlobalData.PLAYER_TAG)
        {
            Deactivate();
        }
    }
    

    private void Activate()
    {
        for (int i = 0; i < Objects.Count; i++)
        {
            Objects[i].SetActive(true);
        }
    }

    private void Deactivate()
    {
        for (int i = 0; i < Objects.Count; i++)
        {
            Objects[i].SetActive(false);
        }
    }

    private void Awake()
    {
        if (!StartingState)
        {
            for (int i = 0; i < Objects.Count; i++)
            {
                Objects[i].SetActive(StartingState);
            }
        }
    }

    public void Start()
    {   
        if (StartingState && GetComponent<PolygonCollider2D>().IsTouching(GlobalData.Player.MovementCollider))// if we need the GO to be on at the start lets make sure any alis have been activated
        {
            for (int i = 0; i < Objects.Count; i++)
            {
                Objects[i].SetActive(StartingState);
            }
        }
    }


    
}
