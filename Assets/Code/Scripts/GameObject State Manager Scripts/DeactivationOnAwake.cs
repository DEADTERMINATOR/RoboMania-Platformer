﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivationOnAwake : MonoBehaviour
{
    public List<GameObject> Items = new List<GameObject>();

    private void Awake()
    {
        foreach (GameObject go in Items)
            go.SetActive(false);
    }
}
