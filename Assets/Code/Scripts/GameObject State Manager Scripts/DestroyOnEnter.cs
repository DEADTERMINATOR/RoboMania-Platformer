﻿using UnityEngine;
using System.Collections;

public class DestroyOnEnter : MonoBehaviour
{
    public LayerMask DestroyOnCollisionWithLayers;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (StaticTools.IsLayerInLayerMask(DestroyOnCollisionWithLayers, collision.gameObject.layer))
            Destroy(gameObject);
    }
}
