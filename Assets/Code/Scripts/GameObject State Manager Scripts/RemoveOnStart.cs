﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveOnStart : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        //BUGFIX: Destroy 
        gameObject.SetActive(false);
        Destroy(gameObject, 1);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
