﻿using UnityEngine;
using System.Collections;

public class EnableGOOnActivate : MonoBehaviour, IActivatable
{
    public GameObject GameObject;

    public bool Active { get { return GameObject.activeSelf; } }

    public void Activate(GameObject activator)
    {
        GameObject.SetActive(true);
    }

    public void Deactivate(GameObject activator)
    {
        GameObject.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
