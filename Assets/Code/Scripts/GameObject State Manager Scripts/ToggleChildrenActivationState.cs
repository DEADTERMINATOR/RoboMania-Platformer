﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleChildrenActivationState : MonoBehaviour
{
    public void ActivateChildren()
    {
        foreach (Transform child in transform)
            child.gameObject.SetActive(true);
    }

    public void DeactivateChildren()
    {
        foreach (Transform child in transform)
            child.gameObject.SetActive(false);
    } 
}
