﻿using UnityEngine;
using System.Collections.Generic;
/// <summary>
/// A simpel script to have an elment hiden in the editor suck as a ui that is then activated on start
/// </summary>
public class ActivationOnAwake : MonoBehaviour
{
    public List<GameObject> Items = new List<GameObject>();
    public List<GameObject> MaintainActive = new List<GameObject>();

    private void Awake()
    {
        if (Items == null && Items.Count <= 0)
            return;
        foreach (GameObject go in Items)
            go.SetActive(true);
    }

    private void LateUpdate()
    {
        //TODO: We shouldn't need to do this. Hopefully we can remove these objects from animations.
        if (MaintainActive == null && MaintainActive.Count <= 0)
            return;
        foreach (GameObject go in MaintainActive)
            go.SetActive(true);
    }
}
