﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOnActivation : MonoBehaviour ,IActivatable {

    public GameObject theOBJ;
    public bool HideOnShow;

    public bool Active { get { return theOBJ.activeSelf; } }

    public void Activate(GameObject activator)
    {
        theOBJ.SetActive(!HideOnShow);
    }
    public void Deactivate(GameObject activator)
    {
        theOBJ.SetActive(HideOnShow);
    }
    public void Toggel()
    {
        theOBJ.SetActive(!theOBJ.activeInHierarchy);
    }
    // Use this for initialization
    void Start () {
        theOBJ.SetActive(HideOnShow);
    }
	
	
}
