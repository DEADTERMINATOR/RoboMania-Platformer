﻿using UnityEngine;

public class ShowOnActivationList : MonoBehaviour, IActivatable
{
    public bool Active { get; private set; }

    public GameObject[] ObjectList;
    public bool HideOnShow;

    public void Activate(GameObject activator)
    {
        for(int i =0; i < ObjectList.Length; i++)
        {
            ObjectList[i].SetActive(!HideOnShow);
        }
        Active = true;
        
    }
    public void Deactivate(GameObject activator)
    {
        for (int i = 0; i < ObjectList.Length; i++)
        {
            ObjectList[i].SetActive(HideOnShow);
        }
        Active = false;
    }
    public void Toggel()
    {
        for (int i = 0; i < ObjectList.Length; i++)
        {
            ObjectList[i].SetActive(!ObjectList[i].activeInHierarchy);
        }
        
    }
    /// <summary>
    /// ON start all object set will be set as active
    /// </summary>
    void Start()
    {
        if (ObjectList != null && ObjectList.Length > 0)
            for (int i = 0; i < ObjectList.Length; i++)
            {
                ObjectList[i]?.SetActive(HideOnShow);
            }
    }

}

