﻿using UnityEngine;
using System.Collections;

public class GameObjectKiller : MonoBehaviour, IActivatable
{
    public bool Active { get { return _active; } }

    [SerializeField]
    private bool _active = false;

    // Use this for initialization
    void Start()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_active)
        {
            IWorldKillable obj = collision.GetComponent<IWorldKillable>();
            if (obj != null)
            {
                obj.PreKillLogic();
                //BUGFIX: 
                collision.gameObject.SetActive(false);
                Destroy(collision.gameObject,1);
            }
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void Deactivate(GameObject activator)
    {

    }
    public void Activate(GameObject activator)
    {
        _active = !_active;
    }
}
