﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class ScrapPickUp : PickUp
{

    /// <summary>
    /// The minimum amount of scrap this pick up can provide.
    /// </summary>
    //public int MinAmount;

    /// <summary>
    /// The maximum amount of scrap this pick up can provide.
    /// </summary>
    //public int MaxAmount;


    /// <summary>
    /// The amount of scrap this pick up will provide; a randomly selected value between the min and the max amount.
    /// </summary>
    public int ScrapAmount = 0;

    public bool ShiftPositionOnGround = true;

    /// <summary>
    /// How many seconds till till a low veclity check is alowed
    /// </summary>
    public float Timeout = 3f;

    /// <summary>
    /// The min y veclity used to stop dmage should be nagative
    /// </summary>
    public float yVelocityMin = -0.05f;
    /// <summary>
    /// How long the can be alive in the world
    /// </summary>
    public float aliveTime = 15f;
    /// <summary>
    /// The base amount for damage the aculty fdamage applied is the base+ ABS(velocity.y)
    /// </summary>


    private Rigidbody2D _rigidbody2D;

    /// <summary>
    /// The interal Run time
    /// </summary>
    private float time = 0;

    private bool _positionShiftingAllowed;
    private bool _offsetChosen;
    private float _chosenOffset;

    private CoroutineHandle _shrinkAwayHandle;
#pragma warning disable CS0649 //Field is never assigned to, and will always have its default value
    private CoroutineHandle _shiftHandle;
#pragma warning disable CS0649 //Field is never assigned to, and will always have its default value


    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        Type = PickUpType.SCRAP;
        //ScrapAmount = Random.Range(MinAmount, MaxAmount + 1);
        _rigidbody2D = GetComponent<Rigidbody2D>();

        _positionShiftingAllowed = false;
        _offsetChosen = false;
    }

    protected override void Update()
    {
        time += Time.deltaTime;

        if (_rigidbody2D && time >= Timeout && _rigidbody2D.velocity.y == 0)
        {
            Destroy(_rigidbody2D);
            _rigidbody2D = null;
            GetComponent<Collider2D>().isTrigger = true;
        }

        if (time >= aliveTime)
        {
            _shrinkAwayHandle = Timing.RunCoroutine(ShrinkAway());
        }

        if (_positionShiftingAllowed)
        {
            RaycastHit2D topCollider = Physics2D.Raycast(transform.position, Vector2.down, 0.5f, GlobalData.OBSTACLE_LAYER_SHIFTED);
            if (topCollider && topCollider.collider.gameObject.layer == GlobalData.OBSTACLE_LAYER)
            {
                RaycastHit2D bottomCollider = Physics2D.Raycast(topCollider.point - (0.05f * topCollider.normal), -topCollider.normal, 5.0f, GlobalData.OBSTACLE_LAYER_SHIFTED);
                //Debug.DrawRay(topCollider.point - (0.05f * topCollider.normal), -topCollider.normal, Color.gray, 0.25f);

                if (bottomCollider)
                {
                    if (!_offsetChosen)
                    {
                        float topToBottomDistance = Vector2.Distance(topCollider.point, bottomCollider.point);
                        _chosenOffset = Random.Range(topToBottomDistance * 0.1f, topToBottomDistance * 0.9f);
                        _offsetChosen = true;
                    }

                    //Debug.Log("Top to Bottom Distance: " + topToBottomDistance);
                    //Debug.Log("Chosen Distance: " + randomDistance);

                    Transform spriteTransform = transform.Find("Sprite");

                    float groundAngle = Vector2.Angle(topCollider.normal, Vector2.up);
                    float correctionAngle = 360 - (transform.rotation.eulerAngles.z + groundAngle); //The correction angle adjusts the rotation of the sprite to compensate for the rotation
                                                                                                    //of the actual piece (so that the rotation of the sprite matches the normal of the surface).

                    spriteTransform.localRotation = Quaternion.Euler(0, 0, correctionAngle); //We rotate the sprite to the correction angle when doing movement operations on it,
                                                                                             //so the rotation of the piece does not have an effect on the process.
                    Vector2 correctionVector = StaticTools.Vector2FromAngle(correctionAngle);
                    correctionVector = StaticTools.RotateVector2(correctionVector, spriteTransform.rotation.eulerAngles.z);
                    Debug.DrawRay(transform.position, -correctionVector * 1.25f, Color.red, 0.25f);
                    Vector3 randomPosition = transform.position + (_chosenOffset * -(Vector3)correctionVector);
                    spriteTransform.localRotation = Quaternion.identity;
                    
                    spriteTransform.position = randomPosition;
                    //spriteTransform.rotation = Quaternion.Euler(0, 0, correctionAngle);

                    //_shiftHandle = Timing.RunCoroutine(LerpToShiftedPosition(randomPosition));
                    //_positionShiftingAllowed = false;
                }
            }
        }
    }


    public void ResetTime()
    {
        time = 0;
    }

    public void AllowPositionShifting()
    {
        _positionShiftingAllowed = true;
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            GlobalData.Player.Data.AddScrapAmount(ScrapAmount);

            Timing.KillCoroutines(_shrinkAwayHandle);
            Timing.KillCoroutines(_shiftHandle);

            ///BUGFIX: Destroy
            gameObject.SetActive(false);
            Destroy(gameObject, 1);
        }
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            GlobalData.Player.Data.AddScrapAmount(ScrapAmount);

            Timing.KillCoroutines(_shrinkAwayHandle);
            Timing.KillCoroutines(_shiftHandle);

            gameObject.SetActive(false);
            Destroy(gameObject, 1);
        }
    }

    /// <summary>
    /// A co-routine that respawns the platform by re-scaling it from a scale of 0 to it's original scale.
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> ShrinkAway()
    {
        float percentScalingComplete = 0f;
        Vector3 StartingScale = transform.localScale;
        while (percentScalingComplete < 1f)
        {
            percentScalingComplete += Time.deltaTime / 0.5f;
            transform.localScale = new Vector3(Mathf.Lerp(StartingScale.x, 0, percentScalingComplete), Mathf.Lerp(StartingScale.y, 0, percentScalingComplete), Mathf.Lerp(1, 1, percentScalingComplete));
            yield return 0f;
        }
        //BUGFIX: Destroy 
        gameObject.SetActive(false);
        Destroy(gameObject, 1);
        yield return 0f;
    }

    private IEnumerator<float> LerpToShiftedPosition(Vector3 newPosition)
    {
        float percentLerpComplete = 0f;
        Vector3 startingPosition = transform.position;
        Transform spriteTransform = transform.root.Find("Sprite");

        Vector3 previousLerpedPosition = transform.position;
        Vector3 currentLerpedPosition = transform.position;

        while (percentLerpComplete < 1f)
        {
            percentLerpComplete += Time.deltaTime / 0.1f;

            currentLerpedPosition = Vector3.Lerp(startingPosition, newPosition, percentLerpComplete);
            spriteTransform.Translate(currentLerpedPosition - previousLerpedPosition, Space.World);
            previousLerpedPosition = currentLerpedPosition;

            yield return 0f;
        }
    }
}
