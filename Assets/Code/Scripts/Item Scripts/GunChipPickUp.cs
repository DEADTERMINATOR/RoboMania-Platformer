﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;
using Weapons.Player.Chips;
using UnityEngine.AddressableAssets;
using System.Threading.Tasks;

public class GunChipPickUp : PickUp
{
    public static GunChipPickUp NewChipTemplate = null;



    /// <summary>
    /// Stores the instantiated gun chip so a new one isn't created everytime the chip is dropped and picked up.
    /// </summary>
    public GunChip Gunchip;

    public GunChipArtManager artManager;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        Type = PickUpType.GUNCHIP;
        //GetComponent<SpriteRenderer>().sprite = artManager.GetSprite(Gunchip.itemRarity);
        if (NewChipTemplate == null)
        {
            NewChipTemplate = Resources.Load<GunChipPickUp>(GlobalData.PREFAB_GUNCHIP_TEMPLATE_PATH);

        }


    }

    /// <summary>
    /// Player try to pick up item
    /// </summary>
    /// <param name="player"></param>
    public override void PickUpItem(PlayerMaster player)
    {

        if (Gunchip != null) //When we instantiate the drop template, Gunchip will not have been set yet. So we run this code later.
        {
            GunChip droopedChip = null;
            if (GlobalData.Player.PlayerWeapon.ActiveForm.FirstSlottedGunChip != null && (!GlobalData.Player.PlayerWeapon.ActiveForm.CanSlotSecondGunChip || GlobalData.Player.PlayerWeapon.ActiveForm.SecondSlottedGunChip != null))
            {
                 droopedChip = GlobalData.Player.Weapon.PlayerWeapon.ActiveForm.DropGunChip(PlayerPickupHandler.GunChipToReplace);
            }
            GlobalData.Player.Weapon.PlayerWeapon.ActiveForm.SlotGunChip(Gunchip);
            if (droopedChip != null )// if a chip was drooped we need to sapwn a new pick in this locatiomn
            {
                GunChipPickUp newChip = Instantiate<GunChipPickUp>(NewChipTemplate, transform.position + Vector3.up, Quaternion.identity, null);
                newChip.Gunchip = droopedChip;
                newChip.Dropped();
            }
        }
      
        gameObject.SetActive(false);
        Destroy(gameObject, 1);

    }



    public override void Dropped()
    {
        base.Dropped();
        /* Gunchip.CurrentlyHeldByPlayer = false;
         if (StoredAmmo == -1)
         {
             int ammo = -1;
             ammo = Gunchip.AmmoAmount;
             if (ammo >= 0)
             {
                 StoredAmmo = ammo;
             }
         }*/
    }



}
