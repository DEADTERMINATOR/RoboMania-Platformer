﻿using UnityEngine;
using System.Collections;

public class HealthIncrease : PickUp
{
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            GlobalData.Player.Data.IncreaseMaxHealth(10);
            //BUGFIX: Destroy 
            gameObject.SetActive(false);
            Destroy(gameObject, 1);
        }
    }
}
