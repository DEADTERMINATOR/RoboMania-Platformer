﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour, IActivatable
{
    public GameObject PrefabToSpawn;
    public bool KillOnReactivate;

    public bool Active { get { return _itemSpawned; } }


    private bool _itemSpawned = false;
    private GameObject lastSpawn;


    public void Activate(GameObject activator)
    {
        if (KillOnReactivate && lastSpawn != null)
        {
            //BUGFIX: Destroy 
            lastSpawn.SetActive(false);
            Destroy(lastSpawn,1);
        }
        lastSpawn = Instantiate(PrefabToSpawn, transform.position, Quaternion.identity);
        _itemSpawned = true;
    }

    public void Deactivate(GameObject activator)
    {

    }

    void Start()
    {

    }


    void Update()
    {

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.5f);
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, new Vector3(2, 2, 2));
        Gizmos.color = Color.black;
        float size = 0.3f;
        Vector3 globalWaypointPos = transform.position;
        Gizmos.DrawLine(globalWaypointPos - Vector3.up * size, globalWaypointPos + Vector3.up * size);
        Gizmos.DrawLine(globalWaypointPos - Vector3.left * size, globalWaypointPos + Vector3.left * size);

    }


}
