﻿using GameMaster;
using LevelSystem;
using LevelSystem.SateData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;

public class ShieldPickUpSpawner : MonoBehaviour
{
   // static ShieldAssetManager ShieldAssetManager = null;

    /// <summary>
    /// 
    /// </summary>
    public int UncommonSpawnChance = 25;
    /// <summary>
    /// 
    /// </summary>
    public int RareSpawnChance = 15;
    /// <summary>
    /// 
    /// </summary>
    public int EpicSpawnChance = 10;
    /// <summary>
    /// If set to anything other then count this will only Spawn that rarity 
    /// </summary>
    public Rarity ForceRaraIty = Rarity.COUNT;

    private int EpicMAXPower = 1000;
    private int EpicMINPower = 500;

    private int RareMAXPower = 1000;
    private int RareMINPower = 500;

    private int UncommonMAXPower = 1000;
    private int UncommonMINPower = 500;

    private int CommonMAXPower = 1000;
    private int CommonMINPower = 500;
    /// <summary>
    /// Chance the the shield will be burst 0-1 0.5 = 50%  1=%100  
    /// </summary>
    public float BurstChance = 0.5f;

    private static bool _loading = false;
    private string id = "";
    private LevelStateBool HasBeedSpanwed;

    private void Start()
    {

      /*  if (_loading == false && ShieldAssetManager == null)///This will only run the first ShieldPickUpSpawner to load the asset
        {
            AsyncOperationHandle<ShieldAssetManager> textureHandle = Addressables.LoadAssetAsync<ShieldAssetManager>("Mangers/Shield Asset Manager");
            textureHandle.Completed += ShieldAssetManager_Completed;
            _loading = true;
        }*/
        id = transform.position.x.ToString() + transform.position.y.ToString();
        HasBeedSpanwed = LevelState.Instance.LoadLevelStateBool(id, false);

        if (!HasBeedSpanwed && ShieldAssetManager.Instance != null)//The asset have been load allready
        {
            DropASheild(id);
        }
    }

    

    public GameObject DropASheild(string id)
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        Shield theShield = null;
       /// SceneManager.SetActiveScene(SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage[0].Path)); //Set the active scene to the master scene so the shield persists between loads and unloads.
        int generatedNum = Random.Range(0, 100);
        int powerAmount = 0;
        Rarity rarityToSpawn = Rarity.COMMON;
        powerAmount = Random.Range(CommonMINPower, CommonMAXPower);
        if (generatedNum < UncommonSpawnChance)
        {
            rarityToSpawn = Rarity.UNCOMMON;

            powerAmount = Random.Range(UncommonMINPower, UncommonMAXPower);

        }
        else if (generatedNum >= UncommonSpawnChance && generatedNum < UncommonSpawnChance + RareSpawnChance)
        {
            //rare
            rarityToSpawn = Rarity.RARE;
            powerAmount = Random.Range(RareMAXPower, RareMINPower);

        }
        else if (generatedNum >= UncommonSpawnChance + RareSpawnChance && generatedNum < UncommonSpawnChance + RareSpawnChance + EpicSpawnChance)
        {
            //epic

            rarityToSpawn = Rarity.EPIC;
            powerAmount = Random.Range(EpicMAXPower, EpicMINPower);
        }
        if (ForceRaraIty != Rarity.COUNT)
        {

            rarityToSpawn = ForceRaraIty;
        }

        GameManager.Instance.SetMasterAsActive();
        theShield = Instantiate<Shield>(ShieldAssetManager.GetTemplateFromRarity(rarityToSpawn), GlobalData.VEC3_OFFSCREEN, Quaternion.identity, null);
        theShield.Init(powerAmount, rarityToSpawn);

        SceneManager.SetActiveScene(SceneManager.GetSceneByPath
            (GameMaster.GameManager.Instance.LoadedDatatpackage[0].Path));

        ShieldPickUp pickUp  = Instantiate<ShieldPickUp>(ShieldAssetManager.Instance.ShieldPickUpTemplate,transform.position,Quaternion.identity,null);
        pickUp.Shield = theShield;
        pickUp.Dropped();


        Destroy(gameObject);
        return null;
    }


    /*private void ShieldAssetManager_Completed(AsyncOperationHandle<ShieldAssetManager> handle)
    {
        if (handle.Status == AsyncOperationStatus.Succeeded && ShieldAssetManager == null)//make sure you do not have a ref allreday
        {
            ShieldAssetManager = handle.Result;
            //ShieldAssetManager.Init();
        }
        if (!HasBeedSpanwed && ShieldAssetManager != null)// hande the case a seconde thread has entered but the seconde 
        {
            DropASheild(id);
        }
    }*/

    



}
