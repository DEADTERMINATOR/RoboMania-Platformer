﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Characters.Player;

public class ShieldPickUp : PickUp
{
    /// <summary>
    /// The shield attached to this pick up.
    /// </summary>
    public Shield Shield;


    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        Type = PickUpType.SHIELD;
    }


    public override void PickUpItem(PlayerMaster player)
    {
        base.PickUpItem(player);
        Shield = _playerPickupHandler.PickUpShield(this);//attract to the player
        if (Shield != null)
        {
            transform.position = player.transform.position;// 
            Dropped();
            return;
        }      
        Destroy(gameObject);
    }

    protected override void Update()
    {
        base.Update();

    }

    private void OnDestroy()
    {
       /* if (Shield != null && Shield != GlobalData.Player.Shield.Current )
        {
            //BUGFIX: Destroy 
            Destroy(Shield.gameObject);
        }*/
    }
}
