﻿using UnityEngine;
using System.Collections;
using Characters.Player;

public class AmmoPickUp : PickUp
{
    public int EnergyToAdd = 10;
    public int BulletsToAdd = 15;
    public int RocketToAdd = 2;
    public int PenetrationToAdd = 5;

    private PlayerData.Ammo _inventory;


    protected override void Start()
    {
        base.Start();
        Type = PickUpType.AMMO; //TODO: Why was this an energycell type?
        _inventory = GlobalData.Player.Data.AmmoInventory;
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            //BUGFIX: Destroy 
            gameObject.SetActive(false);
            Destroy(gameObject, 1);

            _inventory.AddAmmo(PlayerData.Ammo.AmmoTypes.Energy, EnergyToAdd);
            _inventory.AddAmmo(PlayerData.Ammo.AmmoTypes.Bullet, BulletsToAdd);
            _inventory.AddAmmo(PlayerData.Ammo.AmmoTypes.Rocket, RocketToAdd);
            _inventory.AddAmmo(PlayerData.Ammo.AmmoTypes.Penetration, PenetrationToAdd);
        }

    }
}
