﻿using Characters.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Weapons.Player.Forms;
using static Weapons.Player.WeaponFormProperties;

public class WeaponFormPickup : PickUp
 {

    public WeaponForms weaponForm;
    protected override void Start()
    {
        Dropped();
    }
    public override void PickUpItem(PlayerMaster player)
    {
        player.Weapon.PlayerWeapon.AddForm(weaponForm);
    }
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            PickUpItem(GlobalData.Player);
            Destroy(gameObject);
        }
    }
}
