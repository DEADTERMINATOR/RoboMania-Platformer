﻿using Characters.Player.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace PickUps
{
    [RequireComponent(typeof(Collider2D))]
    public class PlayerModulePickUp : MonoBehaviour 
    {
        public virtual String ModuleName { get; protected set; } = "OOPS";
        public virtual String HelpText { get; protected set; } = "OOPS";

        protected virtual void Start()
        {
            if (GlobalData.Player.Data.GetModuleIfPossessed(ModuleName) != null)
            {
                Destroy(gameObject);
            }
        }

        protected void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
            {
                GlobalData.Player.Data.AddNewModule(ModuleName);
                Destroy(gameObject);

            }
        }
    }
}
