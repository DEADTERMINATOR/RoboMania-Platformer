﻿using Characters.Player.Modules;

namespace PickUps
{
    public class DashModulePickUp : PlayerModulePickUp
    {
        protected override void Start()
        {
            ModuleName = GlobalData.PlayerDynamicModules.DASH;
            base.Start();
        }
    }
}
