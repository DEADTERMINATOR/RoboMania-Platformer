﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 
/// </summary>
/// 
public enum PickupItemType {GunChip,AShield}
public abstract class PickupItem : MonoBehaviour
{
    public string DisplayName = "DisplayName Not Set";
    public PickupItemType type = PickupItemType.GunChip;
    public Rarity itemRarity;


}