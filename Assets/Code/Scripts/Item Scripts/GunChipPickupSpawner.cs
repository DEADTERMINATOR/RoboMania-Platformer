﻿using LevelSystem;
using LevelSystem.SateData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GunChipPickupSpawner : MonoBehaviour
{
    /// <summary>
    /// The minimum rarity the gun chip drop can be.
    /// </summary>
    public int MinRarity;

    /// <summary>
    /// The maximum rarity the gun chip drop can be.
    /// </summary>
    public int MaxRarity;


    /// <summary>
    /// The prefab for the gun chip pickup.
    /// </summary>
    private GameObject PickUpPrefab;

    /// <summary>
    /// The generated gun chip.
    /// </summary>
    private GameObject _generatedChip;

    private LevelStateBool HasBeedSpanwed; 

    //Use this for initialization
    private void Start()
    {
        string id = transform.position.x.ToString() + transform.position.y.ToString();
        HasBeedSpanwed = LevelState.Instance.LoadLevelStateBool(id,false);

        if (HasBeedSpanwed)
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage.MasterScene)); //Set the active scene to the master scene so the actual gun chip persists between loads and unloads.
            //_generatedChip = GetComponent<WeaponChipGenerator>().GenerateChip(MinRarity, MaxRarity);

            SceneManager.SetActiveScene(gameObject.scene); //Set the active scene to the scene the gun chip spawner is a part of, so the pickup unloads when it is supposed to.
            PickUpPrefab = Resources.Load(GlobalData.PREFAB_GUNCHIP_TEMPLATE_PATH) as GameObject;
            GameObject instantiatedPickUp = Instantiate(PickUpPrefab);
            //GUNCHIP NEED TO BE REPLACD
            SceneManager.SetActiveScene(SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage[0].Path)); //Set the active scene back to the current scene.
        }

        //BUGFIX: Destroy 
        gameObject.SetActive(false);
        Destroy(gameObject, 1);
    }
}
