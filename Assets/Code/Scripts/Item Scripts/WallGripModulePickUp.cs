﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickUps
{
    class WallGripModulePickUp : PlayerModulePickUp
    {
        protected override void Start()
        {
            ModuleName = GlobalData.PlayerDynamicModules.WALL_GRIP;
            base.Start();
        }
    }
}
