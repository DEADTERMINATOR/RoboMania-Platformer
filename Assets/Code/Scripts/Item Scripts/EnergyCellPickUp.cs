﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyCellPickUp : PickUp
{
    /// <summary>
    /// A unique name for a particular cell pickup that will allow its pickup status to be saved, so it doesn't spawn if it has been picked up already.
    /// </summary>
    public string CellPickupName;

    /// <summary>
    /// Whether the pickup of the cell should be saved to the save file.
    /// </summary>
    public bool ShouldSaveCellPickup = true;

    /// <summary>
    /// Force the Cell to spawn no matter What
    /// </summary>
    public bool OverrideSpawn = false;
    /// <summary>
    /// What cell number is tiis
    /// </summary>
    /// 
    [Tooltip("How Many Cell should this cell get the player to. if the player has that many cell it will not spawn")]
    public int NumberOfCellsToUpgradeTo= 2;

    /// <summary>
    /// Whether the cell has already been picked up, per the data in the save file.
    /// </summary>
    // Use this for initialization
    protected override void Start()
    {
        base.Start();



        if (GlobalData.Player.Data.TotalEnergyCells < NumberOfCellsToUpgradeTo)
        {
            gameObject.tag = "EnergyCell";
            Type = PickUpType.ENERGYCELL;
        }
        else if(!OverrideSpawn)
        {
            Destroy(gameObject);
        }
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            GlobalData.Player.Data.SetEnergyCells(NumberOfCellsToUpgradeTo);


            Destroy(gameObject);

            /*
            _pickedUp = true;
            if (!ShouldRespawn)
            {
                DontDestroyOnLoad(gameObject);
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
            }
            */
        }
    }
}
