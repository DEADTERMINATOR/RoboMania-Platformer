﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Characters.Player;

public class PickUp : MonoBehaviour
{
    /// <summary>
    /// The different possible types of pick ups.
    /// </summary>
    [HideInInspector]
    public enum PickUpType { POWERUP, SCRAP, WEAPON, GUNCHIP, SHIELD, ENERGYCELL,AMMO,WEAPONFORM }

    /// <summary>
    /// The id for the pickup.
    /// </summary>
    [HideInInspector]
    public string ID;

    /// <summary>
    /// The type of pick up the item is.
    /// </summary>
    public PickUpType Type;
    /// <summary>
    /// The Diplay Name 
    /// </summary>
   

    /// <summary>
    /// Whether the item should respawn on area/level reload. This will be a temporary measure, as saving hasn't been implemented yet.
    /// </summary>
    public bool ShouldRespawn = true;


    /// <summary>
    /// Whether the item has been picked up.
    /// </summary>
    protected bool _pickedUp;

    /// <summary>
    /// Reference to the player's pick up handler to handle the pick ups.
    /// </summary>
    protected PlayerPickupHandler _playerPickupHandler;


    /// <summary>
    /// Whether the pick up is in the process of falling to the ground.
    /// </summary>
    private bool _dropping = false;

    /// <summary>
    /// The starting position when the pick up is dropped.
    /// </summary>
    private Vector3 _startpos;

    /// <summary>
    /// The final position on the ground when the pick up finishes falling.
    /// </summary>
    private Vector3 _newPos;

    /// <summary>
    /// How long the pick up should take to drop to the ground.
    /// </summary>
    private float _dropTime;


    /// <summary>
    /// Picks the item up and puts it in the player's possession.
    /// </summary>
    /// <param name="player"></param>
    public virtual void PickUpItem(PlayerMaster player)
    {

        _pickedUp = true;
        if (!ShouldRespawn)
        {
            DontDestroyOnLoad(gameObject);
            gameObject.SetActive(false);
        }

        if (_playerPickupHandler)
            _playerPickupHandler.ExitPickUp();
    }

    /// <summary>
    /// Drops the item from the player's possession.
    /// </summary>
    public virtual void Dropped()
    {
        RaycastHit2D hit;

        hit = Physics2D.Raycast(transform.position, Vector3.down, 100, 1 << 9);
        if (hit)
        {
            _newPos = transform.position - Vector3.up * (hit.distance - GetComponentInChildren<Renderer>().bounds.size.y / 2);
            if (hit.distance > 1.1)
            {
                _dropping = true;
                _dropTime = hit.distance / 9.8f;
                _startpos = transform.position;
            }
            else
            {
                transform.position = _newPos;
            }
        }

        //gameObject.SetActive(true);
    }

    protected virtual void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            if (_playerPickupHandler)
                _playerPickupHandler.ExitPickUp();
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            if(_playerPickupHandler)
                _playerPickupHandler.EnterPickUp(this);
        }
    }

    private void OnDestroy()
    {
        if (_playerPickupHandler)
            _playerPickupHandler.ExitPickUp();
    }

    protected virtual void OnTriggerStay2D(Collider2D collider)
    {
       if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            if (_playerPickupHandler)
                _playerPickupHandler.EnterPickUp(this);
        }
    }

    // Use this for initialization
    protected virtual void Start()
    {
        if (ItemShouldRespawn())
        {
            gameObject.tag = "PickUp";
            _playerPickupHandler = GlobalData.Player.GetComponent<PlayerPickupHandler>();
        }
        else
        {
           //BUGFIX: Destroy 
            gameObject.SetActive(false);
            Destroy(gameObject,1);
        }
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (_dropping)
        {
            _dropTime += Time.deltaTime;
            transform.position = Vector3.Lerp(_startpos, _newPos, _dropTime);

            if (Vector3.SqrMagnitude(transform.position - _newPos) < 0.05f)
            {
                _dropping = false;
            }
        }
    }


    /// <summary>
    /// Checks whether the item should respawn on area/level reload.
    /// </summary>
    /// <returns>False if the item has been picked up and shouldn't respawn. True in every other case.</returns>
    private bool ItemShouldRespawn()
    {
        //Picked up and shouldn't respawn - FALSE
        //Not picked up and shouldn't respawn - TRUE
        //Picked up and should respawn - TRUE
        //Not picked up and should respawn - TRUE
        return !(_pickedUp && !ShouldRespawn);
    }
}
