﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HierarchicalStateMachine : MonoBehaviour
{
    /// <summary>
    /// The currently active state machine.
    /// </summary>
    public StateMachine ActiveStateMachine { get; private set; }

    /// <summary>
    /// The state machine that this hierarchical state machine begins with.
    /// </summary>
    public StateMachine InitialStateMachine { get; private set; }


    /// <summary>
    /// A dictionary that holds all the state machines that this machine controls.
    /// </summary>
    private Dictionary<string, StateMachine> _stateMachines;

    /// <summary>
    /// The list of state machines managed by this hierarchical state machine.
    /// </summary>
    private List<StateMachineTransition> _transitions;

    
	//Use this for initialization
	private void Start()
    {
        if (_stateMachines == null)
        {
            _stateMachines = new Dictionary<string, StateMachine>();
        }

        if (_transitions == null)
        {
            _transitions = new List<StateMachineTransition>();
        }
	}
	
	//Update is called once per frame
	private void Update()
    {
        StateMachineTransition triggeredTransition = null;
        List<StateMachineAction> actionsToPerform = new List<StateMachineAction>();

        if (ActiveStateMachine != null)
        {
            foreach (StateMachineTransition transition in _transitions)
            {
                if (ActiveStateMachine == transition.MachineToTransitionFrom && !transition.Disabled && transition.IsTriggered())
                {
                    triggeredTransition = transition;
                    break;
                }
            }

            if (triggeredTransition != null)
            {
                actionsToPerform.AddRange(ActiveStateMachine.CurrentState.ExitActions);
                actionsToPerform.AddRange(triggeredTransition.ActionsToPerform);

                foreach (StateMachineAction action in actionsToPerform)
                {
                    action.PerformAction();
                }
                ActiveStateMachine = triggeredTransition.MachineToTransitionTo;

                //Debug.Log("Active Machine: " + ActiveStateMachine.ToString());
            }
            else
            {
                ActiveStateMachine.RunUpdate();
            }
        }
	}


    /// <summary>
    /// Adds a new state machine to the dictionary of machines. If it's the first machine added,
    /// that machine is set as the initial state machine and as the first active state machine.
    /// </summary>
    /// <param name="machineKey">The key to store with the machine.</param>
    /// <param name="machine">The machine to add.</param>
    public void AddMachine(string machineKey, StateMachine machine)
    {
        if (_stateMachines == null)
        {
            _stateMachines = new Dictionary<string, StateMachine>();
        }

        if (_stateMachines.Values.Count == 0)
        {
            InitialStateMachine = machine;
            ActiveStateMachine = InitialStateMachine;
        }
        _stateMachines.Add(machineKey, machine);
    }

    /// <summary>
    /// Removes a state machine (and any transitions that involve that state machine) from the hierarchical state machine.
    /// </summary>
    /// <param name="machineKey">The key to retrieve the machine from the dictionary.</param>
    public void RemoveMachine(string machineKey)
    {
        StateMachine machineToRemove = _stateMachines[machineKey];
        if (machineToRemove == ActiveStateMachine)
        {
            //Debug.LogWarning("Cannot remove the currently active state machine");
        }
        else
        {
            _stateMachines.Remove(machineKey);
            foreach (StateMachineTransition transition in _transitions)
            {
                if (transition.MachineToTransitionFrom == machineToRemove || transition.MachineToTransitionTo == machineToRemove)
                {
                    _transitions.Remove(transition);
                }
            }
        }
    }

    /// <summary>
    /// Attempts to find a machine using a given key, and if successful, sets that machine as the currently active machine.
    /// </summary>
    /// <param name="machineKey"></param>
    public void SetActiveMachine(string machineKey)
    {
        StateMachine foundMachine = _stateMachines[machineKey];
        if (foundMachine == null)
        {
            //Debug.LogWarning("Could not find a machine with that key");
        }
        else
        {
            ActiveStateMachine = foundMachine;
        }
    }

    public void AddTransition(StateMachineTransition transition)
    {
        if (_transitions == null)
        {
            _transitions = new List<StateMachineTransition>();
        }

        _transitions.Add(transition);
    }
}
