﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Hold A collection of Zones for the Robot Sniper Ai
/// </summary>
public class SniperActiveZones : MonoBehaviour
{

    public List<SniperActiveZone> zones;


    /// <summary>
    /// Will Find The zone The plyer is in 
    /// if the player is not in any zone then the zone with a preach point closest to the player is chosen
    /// </summary>
    /// <returns></returns>
    public SniperActiveZone GetClosestToPlayer()
    {
        float MinDist = float.MaxValue;
        SniperActiveZone clsestToPlayer = null;

        foreach (SniperActiveZone zone in zones)
        {
            float dist = Vector3.Distance(zone.transform.position, GlobalData.Player.transform.position);
            if (MinDist >  dist)
            {
                MinDist = dist;

                clsestToPlayer = zone;
            }

            if (zone.IsPlayerInZone)
            {
                return zone;
            }
        }
        return clsestToPlayer;
    }
}
