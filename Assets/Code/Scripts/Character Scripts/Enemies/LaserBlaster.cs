﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;
using static GlobalData;

namespace Characters.Enemies
{
    public class LaserBlaster : MonoBehaviour, IDamageGiver, IDamageReceiver
    {
        /// <summary>
        /// A delegate for notifications that the blaster has been destroyed.
        /// </summary>
        /// <param name="blaster"></param>
        public delegate void NotifyBlasterDestroyed(LaserBlaster blaster);

        /// <summary>
        /// Notifies subscribers that a blaster has been destroyed.
        /// </summary>
        public event NotifyBlasterDestroyed Destroyed;


        /// <summary>
        /// How much health the laser blaster has.
        /// </summary>
        public float Health;

        /// <summary>
        /// How long should the blaster wait in between shots.
        /// </summary>
        public float TimeBetweenShots;

        /// <summary>
        /// The period of time after which the blasters will self destruct.
        /// </summary>
        public float SelfDestructTimer;

        /// <summary>
        /// The prefab that will be used for the laser blast charge.
        /// </summary>
        public GameObject ChargeEffectPrefab;

        /// <summary>
        /// The prefab that will be used for the laser blast projectile.
        /// </summary>
        public GameObject BlastProjectilePrefab;

        /// <summary>
        /// The prefab that will be used for the explosion on death effect.
        /// </summary>
        public GameObject ExplosionEffectPrefab;


        /// <summary>
        /// The blaster's final position that it will fire from.
        /// </summary>
        private Vector3 _finalPosition;

        /// <summary>
        /// Whether the blaster has begun moving to its final position.
        /// </summary>
        private bool _moveToFinalPosition;

        /// <summary>
        /// Whether the blasher has arrived at its final position.
        /// </summary>
        private bool _atFinalPosition;

        /// <summary>
        /// The amount of time lapsed since the last shot.
        /// </summary>
        private float _fireTimer;

        /// <summary>
        /// The amount of time the blaster has been alive.
        /// </summary>
        private float _lifeTimer;

        /// <summary>
        /// The currently instantiated charge effect.
        /// </summary>
        private GameObject _instantiatedChargeEffect;

        /// <summary>
        /// The prefab pool for the blast projectiles.
        /// </summary>
        private PrefabPool _blastProjectilePool;


        //Use this for initialization
        private void Awake()
        {
            _finalPosition = transform.position;
            _moveToFinalPosition = false;
            _fireTimer = 0;

            _blastProjectilePool = gameObject.AddComponent<PrefabPool>();
            _blastProjectilePool.PoolPrefab = BlastProjectilePrefab;
        }

        //Update is called once per frame
        private void Update()
        {
            if (_moveToFinalPosition)
            {
                Timing.RunCoroutine(LerpToFinalPosition());
            }

            if (_atFinalPosition)
            {
                if (_fireTimer >= TimeBetweenShots)
                {
                    if (_instantiatedChargeEffect == null)
                    {
                        Projectile laserBlast = _blastProjectilePool.Spawn().GetComponent<Projectile>();
                        laserBlast.transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
                        laserBlast.ProjectileSpeed = 20;
                        laserBlast.Fire(new Vector3(0, 0, -90), 30, transform.root.gameObject);

                        _fireTimer = 0f;
                    }
                }
                else
                {
                    _fireTimer += Time.deltaTime;
                    if (_fireTimer >= TimeBetweenShots)
                    {
                        _instantiatedChargeEffect = Instantiate(ChargeEffectPrefab, new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z), Quaternion.identity);
                        _instantiatedChargeEffect.GetComponent<Animator>().SetTrigger("Charge");
                    }
                }
            }

            _lifeTimer += Time.deltaTime;
            if (_lifeTimer >= SelfDestructTimer)
            {
                TakeDamage(this, 1000, DamageType.ENVIROMENT, transform.position);
            }
        }


        public void TakeDamage(IDamageGiver giver, float amount, DamageType type, Vector3 hitPoint)
        {
            Health -= amount;
            if (Health <= 0)
            {
                Destroy();
            }
        }


        /// <summary>
        /// Sets the final position for the blaster.
        /// </summary>
        /// <param name="finalPosition">The blaster's final position.</param>
        public void SetFinalPosition(Vector3 finalPosition)
        {
            _finalPosition = finalPosition;
            _moveToFinalPosition = true;
        }

        /// <summary>
        /// Destroys the laser blaster, either when it runs out of health, or because a new set are being deployed.
        /// </summary>
        public void Destroy()
        {
            if (Destroyed != null)
            {
                Destroyed(this);
            }

            if (_instantiatedChargeEffect != null)
            {
                ///BUGFIX: Destroy
                _instantiatedChargeEffect.SetActive(false);
                Destroy(_instantiatedChargeEffect, 1);
            }
            Instantiate(ExplosionEffectPrefab, transform.position, Quaternion.identity);
            transform.root.gameObject.SetActive(false);
            Destroy(transform.root.gameObject, 1);
        }


        /// <summary>
        /// A co-routine that moves the blaster into its final position over a period of 2 seconds.
        /// </summary>
        /// <returns></returns>
        private IEnumerator<float> LerpToFinalPosition()
        {
            float distanceToFinalPosition = Vector3.Distance(transform.parent.transform.position, _finalPosition);

            while (distanceToFinalPosition > 0.05f)
            {
                transform.parent.transform.position = Vector3.MoveTowards(transform.parent.transform.position, _finalPosition, distanceToFinalPosition * (Time.deltaTime * 0.5f));
                distanceToFinalPosition = Vector3.Distance(transform.parent.transform.position, _finalPosition);

                yield return 0f;
            }

            _atFinalPosition = true;
            yield return 0f;
        }
    }
}
