﻿using UnityEngine;
using System.Collections;

public class SniperActiveZone : MonoBehaviour
{
    /// <summary>
    /// IS the Player in the Zone 
    /// </summary>
    [HideInInspector]
    public bool IsPlayerInZone = false;
    /// <summary>
    /// Defines The Area the Sniper will look
    /// </summary>
    public Collider2D ZoneViewArea;
    /// <summary>
    /// The game object use to get the place of  second place in a zone the Sniper may use
    /// </summary>
    public GameObject AltPerchPointGameObject;
    /// <summary>
    /// The main perch point in a zone is the Postion of the GameObject
    /// </summary>
    public Vector3 MainPerchPoint { get { return transform.position; }  }
    /// <summary>
    /// The secondary Perch point 
    /// </summary>
    public Vector3 AltPerchPoint { get { return AltPerchPointGameObject.transform.position; } }
    public void Start()
    {
        ZoneViewArea = GetComponent<Collider2D>();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag ==  StaticTools.PLAYER_TAG)
        {
            IsPlayerInZone = true;
           
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == StaticTools.PLAYER_TAG)
            IsPlayerInZone = false;
    }

    /// <summary>
    /// Get the closest Perch Point to the player
    /// </summary>
    /// <param name="thePont"></param>
    /// <returns></returns>
    public Vector3 ClosestPerchPointToPoint(Vector3 thePont)
    {
        if(Vector3.Distance(thePont,this.transform.position)< Vector3.Distance(thePont,AltPerchPointGameObject.transform.position))
        {
            return transform.position;
        }
        else
        {
            return AltPerchPointGameObject.transform.position;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position + Vector3.down, transform.position + Vector3.up);
        Gizmos.DrawLine(transform.position + Vector3.left, transform.position + Vector3.right);
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(AltPerchPointGameObject.transform.position + Vector3.down, AltPerchPointGameObject.transform.position + Vector3.up);
        Gizmos.DrawLine(AltPerchPointGameObject.transform.position + Vector3.left, AltPerchPointGameObject.transform.position + Vector3.right);
        Gizmos.color = new Color32(200, 0, 0, 55);
        Gizmos.DrawCube(ZoneViewArea.transform.position + (Vector3)ZoneViewArea.offset, ZoneViewArea.bounds.size);
    }

}
