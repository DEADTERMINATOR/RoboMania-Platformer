﻿using UnityEngine;
using System.Collections;

namespace Characters.Enemies.Bosses.Handy
{
    public class DamagingWaveLandingZone : MonoBehaviour, IDamageGiver
    {
        public float MaxDmage;
        public float MaxDistance;
        public DamagingWave LeftWave;
        public DamagingWave RightWave;
        public float Speed = 2.2f;
        public bool IsRunning { get { return LeftWave.IsMoving && RightWave.IsMoving; } }
        public GameObject MainSFXGameObj;
        private bool _didMainHurt = true;


        public void StartWave()
        {
            LeftWave.gameObject.SetActive(true);
            LeftWave.StartWave(transform.position);
            RightWave.gameObject.SetActive(true);
            RightWave.StartWave(transform.position);
            _didMainHurt = false;
            GlobalData.Timekeeper.StartTimer(EndHurt, .15f);
            MainSFXGameObj.SetActive(true);
        }
        // Use this for initialization
        void Start()
        {
            MainSFXGameObj.SetActive(false);
        }
        private void EndHurt()
        {
            _didMainHurt = true;
            MainSFXGameObj.SetActive(false);
        }
        private void OnTriggerEnter(Collider other)
        {
            if (!_didMainHurt && other.gameObject.tag == GlobalData.PLAYER_TAG)
            {
                GlobalData.Player.TakeDamage(this, 25, GlobalData.DamageType.ENVIROMENT, Vector3.zero);
                EndHurt();
            }
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (!_didMainHurt && collision.gameObject.tag == GlobalData.PLAYER_TAG)
            {
                GlobalData.Player.TakeDamage(this, 25, GlobalData.DamageType.ENVIROMENT, Vector3.zero);
                EndHurt();
            }
        }
    }
}
