﻿using System.Collections.Generic;
using UnityEngine;

namespace Characters.Enemies.Bosses.Handy
{
    public class DamagingWave : MonoBehaviour, IDamageGiver
    {
        public ParticleSystem IndicationSFX;
        public ParticleSystem ExtraSFX;
        public DamagingWaveLandingZone Zone;
        public Vector3 Direction;
        public bool IsMoving { get { return _moving; } }
        private Vector3 _startingPoint;
        private Vector3 _endPoint;
        private List<GameObject> _spawnedSFXs = new List<GameObject>();
        private bool _moving = false;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (_moving)
            {
                if (collision.tag == GlobalData.PLAYER_TAG)
                {
                    GlobalData.Player.TakeDamage(this, Zone.MaxDistance, GlobalData.DamageType.PROJECTILE, transform.position);
                }
                else if (collision.gameObject.layer == GlobalData.OBSTACLE_LAYER)
                {
                    Stop();
                }
            }
        }

        private void Update()
        {
            if (StaticTools.ThresholdApproximately(transform.position, _endPoint, 0.3f))
            {
                Stop();
            }
            if (_moving)
            {
                transform.position = Vector3.MoveTowards(transform.position, _endPoint, Zone.Speed * Time.deltaTime);
                Instantiate(ExtraSFX, transform.position, Quaternion.identity);

            }
        }

        public void StartWave(Vector3 startPoint)
        {
            transform.position = startPoint;
            _startingPoint = startPoint;
            _endPoint = startPoint + Direction * Zone.MaxDistance;
            IndicationSFX.Play();
            _moving = true;
        }

        private void Start()
        {
            IndicationSFX.Stop();
        }
        public void Stop()
        {
            CleanUp();
            IndicationSFX.Stop();
            gameObject.SetActive(false);
            _moving = false;
        }

        private void CleanUp()
        {
            foreach (GameObject sfx in _spawnedSFXs)
            {
                Destroy(sfx);
                _spawnedSFXs.Remove(sfx);

            }
        }
    }
}