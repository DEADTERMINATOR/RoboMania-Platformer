﻿using UnityEngine;
using System.Collections;
using static GlobalData;

namespace Characters.Enemies.Bosses.Handy
{
    public class HandyLandingArea : Knockback, IDamageGiver
    {
        /// <summary>
        /// Is the player in the Landing zone
        /// </summary>
        public bool PlayerInZone { get { return _playerInRange; } }

        public float Damage = 5;

        private bool _playerInRange = false;
        /// <summary>
        /// The SFX To play 
        /// </summary>
        public ParticleSystem SFX;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == GlobalData.PLAYER_TAG)
            {
                _playerInRange = true;
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == GlobalData.PLAYER_TAG)
            {
                _playerInRange = false;
            }
        }

        private void Start()
        {
            collider = GetComponent<Collider2D>();
            SFX.Stop();
        }
        public void DoHurt()
        {
            Instantiate(SFX, transform.position, Quaternion.identity);

            if (_playerInRange)
            {
                GlobalData.Player.TakeDamage(this, Damage, DamageType.MELEE, Vector3.zero);

                Vector2 knockback = CalculateKnockback(GlobalData.Player.transform.position + Vector3.up);


                if (StaticTools.ThresholdApproximately(knockback.x, 0, 0.01f) && StaticTools.ThresholdApproximately(knockback.y, 0, 0.01f))
                    return;

                GlobalData.Player.Movement.SetDamageKnockback(knockback);
            }
            Camera.main.gameObject.GetComponent<CameraShakeSFX>()?.Play(1);
        }
    }
}
