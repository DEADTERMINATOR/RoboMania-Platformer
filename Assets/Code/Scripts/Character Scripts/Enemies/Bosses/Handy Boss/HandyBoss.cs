﻿using Anima2D;
using Characters.Enemies.Bosses;
using LegacyAnimationHelpers;
using UnityEngine;
using UnityEngine.UI;
using Characters.Enemies;
using Characters;
using System.Collections.Generic;

namespace Characters.Enemies.Bosses.Handy
{
    public class HandyBoss : Boss
    {
        #region ArenaVars
        /// <summary>
        /// Dev var to set the bounds of the boss arena
        /// </summary>
        public BoxCollider2D AreaTemp;
        /// <summary>
        /// Whats is the Y offset is need when making points in order not to have the AI in the gound 
        /// </summary>
        private const float YOFFSET = 2.55f;
        #endregion

        #region Hands 
        // the shoot able hands
        public HandyHand LeftHandyHand;
        public HandyHand RightHandyHand;
        #endregion
        // The damage and SFX to play when lading
        public HandyLandingArea LandingArea;

        /// <summary>
        /// 
        /// </summary>
        public enum Hand { LEFT, RIGHT }
        /// <summary>
        /// Array index for the Left hand
        /// </summary>
        public const int LEFTHAND = 0;
        /// <summary>
        /// Array Index for the right  hands
        /// </summary>
        public const int RIGHTHAND = 1;
        /// <summary>
        /// The Ik Control for the Left Hand
        /// </summary>
        public GameObject LeftHandIK;
        /// <summary>
        /// The Ik Control for the Right  Hand
        /// </summary>
        public GameObject RightHandIK;
        /// <summary>
        /// Can the Player see the AI or Can it see the transfom 
        /// </summary>
        /// 
        public AudioSource HitGroundSoundLight;

        public AudioSource HitGroundSoundHevey;
        public AudioSource ShootHandsSound;
        public AudioSource TrusterSound;
        public AudioSource FallingRockRumble;
        public AudioSource TakeOffSound;
        public bool CanBeSeeenByPlayer
        {
            get
            {
                Vector3 point = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().WorldToViewportPoint(transform.position);
                {
                    return (point.x >= 0 && point.x <= 1) && (point.y >= 0 && point.y <= 1);
                }
            }
        }

        /// <summary>
        /// The Speed The NPC Dive at the player  
        /// </summary>
        public float FallSpeedMultiplyer = 3;
        /// <summary>
        /// The Speed The NPC uses in the air 
        /// </summary>
        public float FlySpeedMultiplyer = 1.5f;
        /// <summary>
        /// How factor should the Base speed increesde by when jumping 
        /// </summary>
        public float JumpSpeedMuptiplyer = 1.75f;
        /// <summary>
        /// The manger for falling rocks so that rocks can be droops as well
        /// </summary>
        public FallingRocksArea RockManager;

        public DamagingWaveLandingZone landingWave;

        public ParticleSystem JetSFXParticleSystem;

        public AnimationClip IdleAnimation;
        public AnimationClip TakeOFFlyingAnimation;
        public AnimationClip InAirFlyingAnimation;
        public AnimationClip FlyingLandAnimation;
        public AnimationClip StartJumpAnimation;
        public AnimationClip LandJumpAnimation;
        public AnimationClip InAirJumpAnimation;
        public AnimationClip ShootHandsAnimation;

        private AnimationClipsManager animationClipsManager;
        /// <summary>
        /// Are we in the air and should the 90 or -90 rotation should be handled by UPdateSpriteDirection
        /// </summary>
        private bool _flying = false;
        /// <summary>
        /// Are the Hands Connected
        /// </summary>
        private bool[] _handsConnected = { true, true, true };
        /// <summary>
        /// Was is the distance to the player on the X
        /// </summary>
        private float _XDistanceToPlayer;

        /// <summary>
        /// Can we fly if a transition happens  
        /// </summary>
        private bool _fly = false;

        // how many passes will the flight do over the waypoint system
        private int _numberOfPasses;
        /// <summary>
        /// The Points of the flight path
        /// </summary>
        private WayPoints _flyPoints = new WayPoints();

        /// <summary>
        /// The points the Swoop will use
        /// </summary>
        private Vector3[] _swoopPath = new Vector3[6];
        /// <summary>
        /// Are We gonna Droop From the Left;
        /// </summary>
        private bool _droopFromLeft = true;
        /// <summary>
        /// 
        /// </summary>
        private bool _dive = false;

        //How close on the X should the Ai react to the player;
        private float _reactDistance = 15;
        /// <summary>
        /// Should we wait on the hold and wait state
        /// </summary>
        private bool _IsWatingOnHold = false;

        /// <summary>
        /// Did we shoot hands yet;
        /// </summary>
        private bool _didShootHandsInAir = false;
        private bool _wasHit;

        private int _jumps = 0;
        private int _numberOfTimeHandsHaveBeenShootInARow = 0;
        private Vector3 _leftHandTarget;
        private Vector3 _rightHandTarget;
        private Vector3 _playerPOS { get { return GlobalData.Player.transform.position; } }

        private Vector2 _lastinput;

        public string DebugString = "";
        /// <summary>
        /// How long to wait on wait states ie between moves
        /// </summary>
        public float WaitTime = 1;// wait time

        /// <summary>
        /// How Far above the ground should a Flight be
        /// </summary>
        private const float YFLYHEIGHT = 10.5f;
        /// <summary>
        /// How close can we get to the Side of the Defined area 
        /// </summary>
        private const float AREABUFFERAMOUNT = 3.5f;
        /// <summary>
        /// How many normal jumps should be allowed in a row(Stop the AI from jumping all the time) 
        /// </summary>
        private const int MAXNUMBEROFJUMPSINAROW = 3;
        /// <summary>
        /// How much time to wait on wait and hold
        /// </summary>
        private const float WAITTIMEONHOLD = 1.5f;
        /// <summary>
        /// How long to wait on the big wait and hold after  MAXNUMBEROFJUMPSINAROW jumps
        /// </summary>
        private const float BIGWAITTIMEONHOLD = 3f;
        /// <summary>
        /// Vars Too look at and add comments
        /// </summary>
        private const float DIVE_START_Y = 10f;
        /// <summary>
        /// How long to wait to droop
        /// </summary>
        private readonly float DROOP_INDICATION_TIME = 0.1f;
        /// <summary>
        /// Are we wating on a aimation call back event
        /// </summary>
        private bool _waitOnAnimation = false;
        /// <summary>
        /// Is it ok to use the IK
        /// </summary>
        private bool _canIkAim;
        /// <summary>
        /// ???
        /// </summary>
        private float _slowFactor;
        /// <summary>
        /// ??
        /// </summary>
        private float _RoateTime;
        /// <summary>
        /// 
        /// </summary>
        private float _waitStartTimer;
        private int _branchOneSelecetion;
        private Vector3[] _randomeJumpPoints;
        private int _numberOfJumps;

        private bool _reactionJump = false;
        /// <summary>
        /// Is the jumping random and not targeted
        /// </summary>
        private bool _jumpisRandom = false;
        private int _randomJumpPointIndex = 0;
        /// <summary>
        /// The curent state of the swoop 
        /// </summary>
        private int _swoopStage;
        /// <summary>
        /// ???old??
        /// </summary>
        private bool _droop;
        /// <summary>
        /// Should we do a swoop at the end of the flying loops if not a droop is done 
        /// </summary>
        private bool _swoop = false;

        private int _branchTowSelecetion;
        /// <summary>
        /// Should a new random jump point be selected this is set when the set number of jumps is hit or the NPC is hit 
        /// </summary>
        private bool _getNewJumpPoint = false;
        private Vector3 _savedJumpPoint;
        /// <summary>
        /// Shoud a swoop hapen from the left this is set based on the number of passed set during fly set up 
        /// </summary>
        private bool _swoopFromLeft;
        /// <summary>
        /// The run time of the current take off 
        /// </summary>
        private float _takeOffTime;
        /// <summary>
        /// How long dose a take off take used by the easing functions
        /// </summary>
        private float _TotalTakeOffTime = 2f;

        /// <summary>
        /// The run time of the current take off 
        /// </summary>
        private float _takeOffTimeRot;
        /// <summary>
        /// How long dose a take off take used by the easing functions
        /// </summary>
        private float _TotalTakeOffTimeRot = 1f;
        protected override void Awake()
        {
            base.Awake();
        }
        //set up the animation using legacy system (Yes Dave this was you it was done to have state transitions happens when clip is done player and dose not relay on the events)
        private void SetUpAnimations()
        {
            ManagedAnimatinClip[] clips = { new ManagedAnimatinClip(IdleAnimation), new ManagedAnimatinClip(TakeOFFlyingAnimation), new ManagedAnimatinClip(InAirFlyingAnimation, WrapMode.Loop), new ManagedAnimatinClip(FlyingLandAnimation, WrapMode.ClampForever), new ManagedAnimatinClip(StartJumpAnimation), new ManagedAnimatinClip(InAirJumpAnimation), new ManagedAnimatinClip(LandJumpAnimation), new ManagedAnimatinClip(ShootHandsAnimation, WrapMode.Loop) };
            animationClipsManager = new AnimationClipsManager(CharacterSpriteCollection.GetComponent<Animation>(), clips);
            animationClipsManager.SetDefaultState(IdleAnimation);
        }
        protected override void Start()
        {
            SetUpAnimations();
            base.Start();
            ActiveArea = new Rect(AreaTemp.bounds.min.x, AreaTemp.bounds.min.y, AreaTemp.bounds.size.x, AreaTemp.bounds.size.y);




            #region StateMacineSetUp
            State Move = new State("Move");
            State PreJump = new State("preJump");
            State PreJumpRandom = new State("preJump Random");
            State RejumpJumpRandom = new State("preJump Random");
            State Jump = new State("Jump");
            State JumpRandom = new State("Jump random");
            State Fall = new State("Fall");
            State FallRandom = new State("Fall random");
            State ShootBoothHands = new State("Stage One Hand shoot");
            State PreTakeOff = new State("PreTakeOff");
            State TakeOff = new State("Take Off");
            State Fly = new State("Fly");
            State PreDroop = new State("PreDroop");
            State Droop = new State("Droop");
            State StartShoot = new State("StartShoot");
            State DiveAtPlayer = new State("Dive At Player");
            State WaitStateHands = new State("wait hands");
            State WaitStateJump = new State("wait jump ");
            State WaitStateJumpRandom = new State("wait jump rand");
            State ShootHandsAtWalls = new State("Shoot At Walls");
            State DroopWave = new State("DroopWave");
            State AimHands = new State("AimHands");
            State JumpLanding = new State("JumpLanding");
            State JumpLandingRandom = new State("JumpLanding random");
            State Start = new State("start ");
            State StartAltDive = new State("start alt dive");
            State AltDive = new State("AltDive");
            State StartToDroop = new State("StartToDroop");
            State BranchOne = new State("Do Branch 1");
            State BranchTwo = new State("Do Branch 2");
            State ReturnToFlightPath = new State("ReturnToFlightPatbh");
            // mabey uneeded 


            #endregion


            _behaviourStateMachine = new StateMachine(Start, activated);
            #region Common

            StateMachineAction SetJumpToPoint = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                if (_getNewJumpPoint)
                {
                    _numberOfJumps++;
                    Vector3 point = MakePointInAreaBounds(GlobalData.Player.transform.position + new Vector3(GlobalData.Player.Velocity.x, 0));
                    if (_jumpisRandom)
                    {
                        point = _randomeJumpPoints[_randomJumpPointIndex];
                        _randomJumpPointIndex++;
                    }


                    point = MakePointInAreaBounds(point);
                    point.y = GetMaxYOnScreen() - 4.5f;

                    TargetVector = point;
                    _savedJumpPoint = TargetVector;
                }
                else
                {
                    TargetVector = _savedJumpPoint;
                }
                MoveSpeed = BaseMoveSpeed * JumpSpeedMuptiplyer;
                if (_jumpisRandom)
                {
                    MoveSpeed = BaseMoveSpeed * JumpSpeedMuptiplyer+5;
                }
                
                GravityScale = 0;
                ReadyForNewWaypoint = false;
                _jumps++;
                return true;
            }));

            StateMachineAction StartDroop = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                MoveSpeed = BaseMoveSpeed * FallSpeedMultiplyer;
                TargetVector = new Vector3(TargetVector.x, ActiveArea.min.y + YOFFSET, 0);
                ReadyForNewWaypoint = false;
                return true;
            }));

            //Used
            StateMachineAction FacePlayer = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                float diff = transform.position.x - TargetVector.x;
                // face the player 
                if (Controller.Collisions.FaceDir == 1 && diff > 0)
                {
                    SetInput(Vector2.left);
                }
                else if (Controller.Collisions.FaceDir == -1 && diff < 0)
                {
                    SetInput(Vector2.right);
                }
                return true;
            }));


            //Used
            StateMachineAction FaceJumpDir = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                float diff = transform.position.x - TargetVector.x;
                // face the player 
                if (Controller.Collisions.FaceDir == 1 && diff > 0)
                {
                    SetInput(Vector2.left);
                }
                else if (Controller.Collisions.FaceDir == -1 && diff < 0)
                {
                    SetInput(Vector2.right);
                }
                return true;
            }));

            StateMachineAction EndDroop = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                _waitOnAnimation = true;
                LandingArea.DoHurt();
                MoveSpeed = BaseMoveSpeed;
                transform.eulerAngles = new Vector3(0, 0, 0);
                return true;
            }));


            StateMachineAction moveToTarget = new FunctionPointerAction(() =>
            {
                if (!ReadyForNewWaypoint)//Do not run if we are wating on a new waypoint
                {
                    Vector3 movment = Vector3.MoveTowards(transform.position, TargetVector, MoveSpeed * Time.deltaTime);
                    // Set the input to the normalized moment amount to get Sprite Directions correct

                    SetInput((movment - transform.position));
                    if (movment != transform.position)
                    {
                        transform.position = movment;// Set the updateed pos 
                                                     //SetVelocity(movment - transform.position);
                    }
                    else if(transform.position == TargetVector)
                    {
                        ReadyForNewWaypoint = true;// we are at the target so we need a new one
                    }
                }

                Debug.DrawLine(transform.position, TargetVector);
                return true;

            });

            Condition HitGroundBelow = new FunctionPointerCondition(new System.Func<bool>(() =>
            {

                if (Controller.Collisions.IsCollidingBelow)
                {

                    return true;
                }
                return false;

            }));

            Condition HitGroundAbove = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return Controller.Collisions.IsCollidingAbove;

            }));

            Condition AtWayPoint = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return ReadyForNewWaypoint;

            }));
            Condition AtWayPointOROnTheGround = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return controller.Collisions.Above.IsColliding ||  ReadyForNewWaypoint ;

            }));
            Condition DoneTakeOff = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return _TotalTakeOffTime >= _takeOffTime && ReadyForNewWaypoint;

            }));

            #endregion
            #region FlyStateFunctions

            StateMachineAction SetUPFly = new FunctionPointerAction(new System.Func<bool>(() =>
            {

                
                //SetUPPoint
                //float y = GetMaxYOnScreen();
                _didShootHandsInAir = false;
                
                float MidPoint = ActiveArea.xMin + (ActiveArea.xMax - ActiveArea.xMin) / 2;
                _flyPoints.Clear();
                _flyPoints.Add(new Vector3(ActiveArea.xMin + 2.5f, ActiveArea.yMin + YFLYHEIGHT, 0));

                _flyPoints.Add(new Vector3(MidPoint, ActiveArea.yMin + YFLYHEIGHT - 0.25f, 0));
                _flyPoints.Add(new Vector3(ActiveArea.xMax - 2.5f, ActiveArea.yMin + YFLYHEIGHT, 0));

                //set the First Point to middel of the areana
                _flyPoints.SetTargetFromIndex(1);
                TargetVector = _flyPoints.GetTarget();
             
                //Set the Number of passes
                _numberOfPasses = Random.Range(2, 4);
                ReadyForNewWaypoint = false;
                GravityScale = 0;
                _swoopFromLeft = true;
                //If two passed then invert the Droop Side
                if (_numberOfPasses >= 3)
                {
                    _droopFromLeft = !_droopFromLeft;
                }
                if (_numberOfPasses % 2 == 0)
                {
                    _swoopFromLeft = false;
                }

                _jumps = 0;
                _takeOffTime = 0;
                _takeOffTimeRot = 0;
                _waitOnAnimation = true;
                TrusterSound.Play();
                return true;

            }));
            StateMachineAction RunTakeOff = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                _takeOffTime += Time.deltaTime;
                MoveSpeed = StaticTools.EaseInOutQuart(2.5f, 30, _takeOffTime,_TotalTakeOffTime );
                return true;

            }));

            StateMachineAction RunTakeOffRot = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                _takeOffTimeRot += Time.deltaTime;
                // Set the roation to be the dir 
                Quaternion final = Quaternion.Euler(0, 0, -90);
                if (transform.position.x > ActiveArea.center.x)
                {
                    final = Quaternion.Euler(0, 0, 90);
                }
                transform.localRotation = Quaternion.Slerp(Quaternion.identity, final, StaticTools.EaseOutQuart(0, 1, _takeOffTimeRot , _TotalTakeOffTimeRot));
                return true;

            }));

            StateMachineAction SetFlyNextWaypoint = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                if (ReadyForNewWaypoint)
                {
                    _flyPoints++;
                    if (TargetVector == _flyPoints.First)
                    {
                        FlipDirInAir(false);
                    }
                    if (TargetVector == _flyPoints.Last)
                    {
                        FlipDirInAir(true);
                    }
                    TargetVector = _flyPoints.GetTarget();
                    ReadyForNewWaypoint = false;
                }
                return true;
            }));


            StateMachineAction SetRotationForInAir = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                // did the X input dir change
                if ((_lastinput.x < 0 && MovementInput.x > 0) || (_lastinput.x > 0 && MovementInput.x < 0))
                {
                    if (Controller.Collisions.FaceDir == -1 && MovementInput.x < 0)
                    {
                        transform.eulerAngles = new Vector3(0, 0, 90);
                    }
                    else if (Controller.Collisions.FaceDir == 1 && MovementInput.x > 0)
                    {
                        transform.eulerAngles = new Vector3(0, 0, -90);
                    }
                }
                return true;
            }));


            Condition CanDiveFromFly = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                if (!_swoop && _flyPoints.GetNumberOfForwardLoops >= _numberOfPasses)
                {
                    return true;
                }

                return false;
            }));

            StateMachineAction SetFlying = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                _flying = true;
                JetSFXParticleSystem.Play();
                //transform.eulerAngles = new Vector3(0, 0, 90);
                return true;
            }));

            StateMachineAction SetNotFlying = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                _flying = false;
                JetSFXParticleSystem.Stop();
                TrusterSound.Stop();
                //transform.eulerAngles = new Vector3(0, 0, 0);
                return true;
            }));
            #endregion
            #region Dive
            StateMachineAction StartDiveAtPlayer = new FunctionPointerAction(new System.Func<bool>(() =>
            {

                TargetVector = new Vector3(GlobalData.Player.transform.position.x, GlobalData.Player.transform.position.y + YOFFSET, 0);
                //VisualDebug.DrawDebugPoint(TargetVector, Color.yellow);
                MoveSpeed = BaseMoveSpeed * FallSpeedMultiplyer;
                transform.eulerAngles = new Vector3(0, 0, 0);
                ReadyForNewWaypoint = false;


                return true;
            }));

            StateMachineAction ShootHandsAtWall = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                _didShootHandsInAir = true;
                LeftHandyHand.gameObject.SetActive(true);
                RightHandyHand.gameObject.SetActive(true);
                LeftHandyHand.Shoot(GlobalData.Player.transform.position, true);
                RightHandyHand.Shoot(GlobalData.Player.transform.position, false);
                return true;
            }));


            #endregion
            //Usesed
            StateMachineAction ShootHandsEntry = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                _numberOfTimeHandsHaveBeenShootInARow++;
                _canIkAim = true;
                if (Random.Range(0, 50) < 25)
                {
                    AimLeftHand();
                    AimRightHand();
                    ShootLeftHand();
                    GlobalData.Timekeeper.StartTimer(ShootRightHand, 0.5f);
                }
                else
                {
                    if (Random.Range(0, 50) < 25)
                    {
                        if (Random.Range(0, 50) < 25)
                        {
                            AimLeftHand();
                            ShootLeftHand();
                        }
                        else
                        {
                            AimRightHand();
                            ShootRightHand();
                        }
                    }
                    else
                    {
                        AimLeftHand();
                        AimRightHand();
                        ShootRightHand();
                        GlobalData.Timekeeper.StartTimer(ShootLeftHand, 0.6f);
                    }
                }

                return true;
            }));


            StateMachineAction shootHand = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                switch (Random.Range(0, 3))
                {
                    case 1:
                        _handsConnected[RIGHTHAND] = false;
                        break;
                    case 2:
                        _handsConnected[LEFTHAND] = false;
                        break;
                    default:
                        _handsConnected[LEFTHAND] = false;
                        _handsConnected[RIGHTHAND] = false;
                        break;
                }
                _jumps = 0;
                return true;

            }));
            //used 
            StateMachineAction EndShoot = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                _canIkAim = false;
                return true;
            }));


            Condition ReactToPlayerProximityJump = new FunctionPointerCondition(new System.Func<bool>(() =>
            {

                if (_XDistanceToPlayer > 3 && _XDistanceToPlayer <= _reactDistance && _jumps < MAXNUMBEROFJUMPSINAROW && !_IsWatingOnHold)
                {
                    _dive = !_dive;
                    return true;
                }

                return false;
            }));

            Condition ReactToPlayerProximityBehind = new FunctionPointerCondition(new System.Func<bool>(() =>
            {

                if (_XDistanceToPlayer < 0)
                {
                    return true;
                }

                return false;
            }));

            Condition ShouldFly = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                if (CanBeSeeenByPlayer && _fly && !_IsWatingOnHold)
                {
                    _dive = !_dive;
                    return true;
                }

                return false;
            }));

            Condition ShouldShoot = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                if (CanBeSeeenByPlayer && !_fly && !_IsWatingOnHold)
                {
                    return true;
                }

                return false;
            }));
            //Used 
            Condition WaitingForHands = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return (_handsConnected[LEFTHAND] && _handsConnected[RIGHTHAND]);

            }));

            Condition ShouldShootHandsInAir = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                //if we are on loop 2 and at point 1 but this is not a 2 loop run
                return (!_didShootHandsInAir && _flyPoints.GetNumberOfForwardLoops != _numberOfPasses && _flyPoints.GetNumberOfForwardLoops == 2 && _flyPoints.CurrentInex == 1);

            }));


            Condition WasHit = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                if (_wasHit)
                {
                    _wasHit = false;
                    return true;
                }
                return false;

            }));

            Condition ShouldDoSwoop = new FunctionPointerCondition(new System.Func<bool>(() =>
            {

                if (_swoop && _flyPoints.GetNumberOfForwardLoops >= _numberOfPasses)
                {
                    return true;
                }

                return false;

            }));

            Condition WasHitAndCanShoot = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                if (_wasHit && _jumps >= MAXNUMBEROFJUMPSINAROW && _numberOfTimeHandsHaveBeenShootInARow < 3 && !_IsWatingOnHold)
                {
                    _wasHit = false;
                    return true;
                }
                return false;

            }));


            Condition WaveIsDone = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return !landingWave.IsRunning;

            }));

            StateMachineAction LandingWave = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                landingWave.StartWave();
                return true;
            }));

            Condition PlayerCannotSeeMe = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return !CanBeSeeenByPlayer;
            }));


            Condition WaitingOnAnimation = new FunctionPointerCondition(new System.Func<bool>(() =>
            {

                return !_waitOnAnimation;
            }));
            Condition WaitingOnAnimationTakeoff = new FunctionPointerCondition(new System.Func<bool>(() =>
            {

                return animationClipsManager.IsDonePlaying(TakeOFFlyingAnimation);
            }));
            Condition WaitingOnAnimationLading = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return animationClipsManager.IsDonePlaying(LandJumpAnimation);
            }));

            Condition WaitOnJumpStart = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return animationClipsManager.IsDonePlaying(StartJumpAnimation);
            }));

            Condition WasRecationJump = new FunctionPointerCondition(new System.Func<bool>(() =>
            {

                return _reactionJump;
            }));


            Condition HealthAboveHalf = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return HealthPerentage >= 0.5;
            }));
            Condition HealthBelowHalf = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return HealthPerentage < 0.5;
            }));

            StateMachineAction EnterStartShoot = new FunctionPointerAction(() =>
            {
                _waitOnAnimation = true;
                return false;
            });

            Condition ShouldJumpInPlace = new FunctionPointerCondition(new System.Func<bool>(() =>
            {

                return !(_jumps % 4 == 0);
            }));
            StateMachineAction ExitDiveAtPlayer = new FunctionPointerAction(() =>
            {
                int index = 0;
                try
                {
                    index = _flyPoints.GetIndexOfClosestPointInDirection(transform.position, Controller.Collisions.FaceDir == -1);
                }
                catch (CloseIndexNotFoundException)
                {
                    index = _flyPoints.GetIndexOfPointClosestTo(transform.position);
                    //Debug.Break();
                }
                ReadyForNewWaypoint = false;
                _flyPoints.SetTargetFromIndex(index);
                TargetVector = _flyPoints.GetTarget();
                _wasHit = false;
                _swoop = false;
                return true;
            });

            FunctionPointerCondition doneWithSwoop = new FunctionPointerCondition(() =>
            {
                return (_swoopStage == _swoopPath.Length);//!vitionArea.IsInTrigger;
            });

            //Used
            FunctionPointerCondition CanPlayerSeeMe = new FunctionPointerCondition(() =>
            {
                return CanBeSeeenByPlayer;
            });

            StateMachineAction EntersPreJump = new FunctionPointerAction(() =>
            {
                _waitOnAnimation = true;
                _getNewJumpPoint = true;
                return true;
            });

            StateMachineAction EntersPreJumpRandom = new FunctionPointerAction(() =>
            {
                _waitOnAnimation = true;
                return true;
            });

            StateMachineAction SetIdleAimation = new FunctionPointerAction(() =>
            {
                animationClipsManager.Play(IdleAnimation);
                return true;
            });

            StateMachineAction SetStartJumpAimation = new FunctionPointerAction(() =>
            {
                animationClipsManager.Play(StartJumpAnimation);
                return true;
            });

            StateMachineAction SetInAIrJumpAimation = new FunctionPointerAction(() =>
            {
                return animationClipsManager.Play(InAirJumpAnimation);
            });

            StateMachineAction SetLandJumpAimation = new FunctionPointerAction(() =>
            {
                return animationClipsManager.Play(LandJumpAnimation);
            });

            StateMachineAction SetTakeOffAimation = new FunctionPointerAction(() =>
            {
                return animationClipsManager.Play(TakeOFFlyingAnimation);
            });
            StateMachineAction SetInAirFlyingAimation = new FunctionPointerAction(() =>
            {
                return animationClipsManager.Play(InAirFlyingAnimation);
            });
            StateMachineAction SetLandingFlyAimation = new FunctionPointerAction(() =>
            {
                return animationClipsManager.Play(FlyingLandAnimation);
            });
            StateMachineAction ReturnToFlyingSpeed = new FunctionPointerAction(() =>
            {
                MoveSpeed = BaseMoveSpeed * FlySpeedMultiplyer;
                return true;
            });

            StateMachineAction EnterStartAltDive = new FunctionPointerAction(() =>
            {
                TargetVector = new Vector3(ActiveArea.min.x + (ActiveArea.max.x - ActiveArea.min.x) / 2, ActiveArea.min.y + DIVE_START_Y, 0);
                return true;
            });


            StateMachineAction EnterAltDive = new FunctionPointerAction(() =>
            {
                MoveSpeed = BaseMoveSpeed * 2;
                float edgeOffset = ActiveArea.size.x / 4;
                if (_swoopFromLeft)
                {
                    _swoopPath[0] = new Vector3(ActiveArea.min.x + 2.9f, ActiveArea.min.y + YFLYHEIGHT + 3, 0);// start on the left
                    _swoopPath[1] = new Vector3(ActiveArea.min.x + 3.5f, ActiveArea.min.y + YFLYHEIGHT - 2, 0);// move down and over a bit 
                    _swoopPath[2] = new Vector3(ActiveArea.min.x + edgeOffset, _playerPOS.y + 3.5f);// start of the run 
                    _swoopPath[3] = new Vector3(ActiveArea.max.x - edgeOffset, _playerPOS.y + 3.5f);//end of run 
                    _swoopPath[4] = new Vector3(ActiveArea.max.x - 2.9f, ActiveArea.min.y + YFLYHEIGHT + 3, 0);// up to the right
                    _swoopPath[5] = new Vector3(ActiveArea.min.x + ActiveArea.size.x / 2, ActiveArea.min.y + YFLYHEIGHT, 0);// to the center
                }
                else
                {
                    _swoopPath[0] = new Vector3(ActiveArea.max.x - 2.9f, ActiveArea.min.y + YFLYHEIGHT + 3, 0);//start on the right
                    _swoopPath[1] = new Vector3(ActiveArea.max.x - 3.5f, ActiveArea.min.y + YFLYHEIGHT - 2, 0);// move down and over a bit 
                    _swoopPath[2] = new Vector3(ActiveArea.max.x - edgeOffset, _playerPOS.y + 3.5f);// start of the run 
                    _swoopPath[3] = new Vector3(ActiveArea.min.x + edgeOffset, _playerPOS.y + 3.5f);//end of run 
                    _swoopPath[4] = new Vector3(ActiveArea.min.x + 2.9f, ActiveArea.min.y + YFLYHEIGHT + 3, 0);// up to the left
                    _swoopPath[5] = new Vector3(ActiveArea.min.x + ActiveArea.size.x / 2, ActiveArea.min.y + YFLYHEIGHT, 0);// to the center
                }

                VisualDebug.DrawDebugPointList(_swoopPath);
                _swoopStage = 0;
                return true;
            });

            //used
            StateMachineAction EnterHandsWait = new FunctionPointerAction(() =>
            {
                _waitStartTimer = Time.realtimeSinceStartup;
                return true;
            });
            StateMachineAction ExitHandsWait = new FunctionPointerAction(() =>
            {
                _numberOfTimeHandsHaveBeenShootInARow = 0;
                return true;
            });
            //used
            FunctionPointerCondition DoneWating = new FunctionPointerCondition(() =>
            {
                return (Time.realtimeSinceStartup - _waitStartTimer >= WaitTime);
            });
            //used
            FunctionPointerCondition CanShootHandsAgin = new FunctionPointerCondition(() =>
            {
                return (_numberOfTimeHandsHaveBeenShootInARow < 2 && HealthPerentage >= 0.5) || (_numberOfTimeHandsHaveBeenShootInARow < 3 && HealthPerentage < 0.5);
            });
            FunctionPointerCondition CanJumpAtplayerAgin = new FunctionPointerCondition(() =>
            {
                return (_numberOfJumps < 3 && HealthPerentage >= 0.5) || (_numberOfJumps < 5 && HealthPerentage < 0.5);
            });

            StateMachineAction RunSwoopAtPlayer = new FunctionPointerAction(() =>
            {
                if (ReadyForNewWaypoint)// reached the last point in the last stage get next point
                {
                    TargetVector = _swoopPath[_swoopStage];
                    _swoopStage++;
                    if (_swoopStage == _swoopPath.Length)
                    {
                        ZeroOutVelocity();
                    }
                    if (_swoopStage == 1)
                    {
                        FlipDirInAir(_swoopFromLeft);
                    }

                    ReadyForNewWaypoint = false;
                }

                return true;

            });

            StateMachineAction enterRoateToDroop = new FunctionPointerAction(() =>
            {
                // Debug.Break();
                _RoateTime = 0;
                _flying = false;
                return true;
            });

            StateMachineAction RoateBeforDroop = new FunctionPointerAction(() =>
            {
                _RoateTime += Time.deltaTime;
                if (Controller.Collisions.FaceDir == -1)
                {
                    transform.eulerAngles = new Vector3(0, 0, Mathf.Lerp(-90, 0, _RoateTime / DROOP_INDICATION_TIME));
                }
                else
                {
                    transform.eulerAngles = new Vector3(0, 0, Mathf.Lerp(90, 0, _RoateTime / DROOP_INDICATION_TIME));
                }
                return true;
            });

            FunctionPointerCondition doneWithSwoop2 = new FunctionPointerCondition(() =>
            {
                return (_swoopStage == _swoopPath.Length && ReadyForNewWaypoint);///all point in the swoop ran and we are at the final point as we are redy for a new waypoint
            });

            FunctionPointerCondition doneWithRoateingToDroop = new FunctionPointerCondition(() =>
            {
                return (_RoateTime >= DROOP_INDICATION_TIME);
            });


            FunctionPointerCondition IsPinningPlayer = new FunctionPointerCondition(() =>
            {
                //return ((Player)&& && );
                if (StaticTools.ThresholdApproximately(GlobalData.Player.transform.position.x, ActiveArea.max.x, 2) && transform.position.x < GlobalData.Player.transform.position.x && StaticTools.ThresholdApproximately(GlobalData.Player.transform.position.x, transform.position.x, 3))
                {
                    return true;
                }
                if (StaticTools.ThresholdApproximately(GlobalData.Player.transform.position.x, ActiveArea.min.x, 2) && transform.position.x > GlobalData.Player.transform.position.x && StaticTools.ThresholdApproximately(GlobalData.Player.transform.position.x, transform.position.x, 3))
                {
                    return true;
                }
                return false;
            });


            FunctionPointerCondition IsOverTopOfPlayer = new FunctionPointerCondition(() =>
            {
                if (StaticTools.ThresholdApproximately(_XDistanceToPlayer, 0, 0.5f))
                {
                    ReadyForNewWaypoint = true;
                    return true;
                }
                return false;
            });

            StateMachineAction EnterDoBranchOne = new FunctionPointerAction(() =>
            {
                _branchOneSelecetion = Random.Range(0, 2);
                Debug.Log("Option " + _branchOneSelecetion + " Picked");
                return true;
            });

            FunctionPointerCondition BranchOneOptionOne = new FunctionPointerCondition(() =>
            {
                return (_branchOneSelecetion == 0);
            });

            FunctionPointerCondition BranchOneOptionTwo = new FunctionPointerCondition(() =>
            {
                return (_branchOneSelecetion == 1);
            });

            StateMachineAction EnterDoBranchTwo = new FunctionPointerAction(() =>
            {
                _branchTowSelecetion = Random.Range(0, 3);
                return true;
            });

            FunctionPointerCondition BranchTwoOptionOne = new FunctionPointerCondition(() =>
            {
                return (_branchTowSelecetion == 0);
            });

            FunctionPointerCondition BranchTwoOptionTwo = new FunctionPointerCondition(() =>
            {
                if (_branchTowSelecetion == 1)
                {
                    _swoop = true;
                }
                return (_branchTowSelecetion == 1);
            });
            FunctionPointerCondition BranchTwoOptionThree = new FunctionPointerCondition(() =>
            {
                return (_branchTowSelecetion == 2);
            });
            //Set up the randome jump move
            StateMachineAction EnterStartRandomJump = new FunctionPointerAction(() =>
            {
                _jumpisRandom = true;
                ///get some rando points
                _randomeJumpPoints = new Vector3[7];
                _randomJumpPointIndex = 0;
                for (int i = 0; i < 7; i++)
                {
                    _randomeJumpPoints[i] = new Vector3(Random.Range(ActiveArea.min.x, ActiveArea.max.x), ActiveArea.min.y, 0);
                }
                return true;
            });

            StateMachineAction StartRocksFalling = new FunctionPointerAction(() =>
            {

                RockManager.Activate(this.gameObject);
                FallingRockRumble.Play();

                return true;
            });

            StateMachineAction StopRocksFalling = new FunctionPointerAction(() =>
            {
                RockManager.Deactivate(this.gameObject);
                FallingRockRumble.Stop();
                return true;
            });

            StateMachineAction EnterRejump = new FunctionPointerAction(() =>
            {
                _getNewJumpPoint = false;
                return true;
            });
            StateMachineAction EnterPreJump = new FunctionPointerAction(() =>
            {
                _getNewJumpPoint = true;
                return true;
            });

            StateMachineAction ExitJumpWait = new FunctionPointerAction(() =>
            {
                _numberOfJumps = 0;
                return true;
            });

            StateMachineAction CleanUpRandomJump = new FunctionPointerAction(() =>
            {
                _numberOfJumps = 0;
                _jumpisRandom = false;
                return true;
            });

            StateMachineAction ForceSwoopForTest = new FunctionPointerAction(() =>
            {
                _swoop = true;
                return true;
            });


            StateMachineAction PlayGroundHit = new FunctionPointerAction(() =>
            {
                HitGroundSoundLight.Play();
                return true;
            });


            StateMachineAction PlayGroundHitBig = new FunctionPointerAction(() =>
            {
                HitGroundSoundHevey.Play();
                return true;
            });

            StateMachineAction PlayHandShoot = new FunctionPointerAction(() =>
            {
                ShootHandsSound.Play();
                return true;
            });
            //Stat State AI wats till the player can see it to start 
            Start.AddStateAction(FacePlayer);
            //Staring stuff
            Start.AddTransition(new Transition(ShootBoothHands, CanPlayerSeeMe));
            Start.AddTransition(new Transition(ShootBoothHands, WasHit));
            //Test random jump
            // Start.AddTransition(new Transition(PreJumpRandom, CanPlayerSeeMe));
            // Start.AddTransition(new Transition(PreJumpRandom, WasHit));
            // Test Swoop and Droop
            //Start.AddTransition(new Transition(PreTakeOff, CanPlayerSeeMe));
            //Start.AddTransition(new Transition(PreTakeOff, WasHit));
            //Swoop setting
            //Start.AddExitAction(ForceSwoopForTest);

            #region Shooting hands move 
            ShootBoothHands.AddEntryAction(FacePlayer);//Face the player 
            ShootBoothHands.AddEntryAction(ShootHandsEntry);// Start the hand Shooting
            ShootBoothHands.AddEntryAction(PlayHandShoot);
            ShootBoothHands.AddTransition(new Transition(WaitStateHands, WaitingForHands));// wait for the hands to return
            ShootBoothHands.AddExitAction(EndShoot);//Clean up tehe state 

            WaitStateHands.AddEntryAction(EnterHandsWait);// start wating 
            WaitStateHands.AddStateAction(FacePlayer);
            WaitStateHands.AddTransition(new Transition(ShootBoothHands, new AndCondition(DoneWating, CanShootHandsAgin)));
            WaitStateHands.AddTransition(new Transition(PreJump, new AndCondition(DoneWating, new NotCondition(CanShootHandsAgin), HealthAboveHalf)));
            WaitStateHands.AddTransition(new Transition(PreJumpRandom, new AndCondition(DoneWating, new NotCondition(CanShootHandsAgin), HealthBelowHalf)));
            #endregion

            #region Flying Moves 
            //Flying move 
            PreTakeOff.AddEntryAction(SetUPFly);
            PreTakeOff.AddEntryAction(SetTakeOffAimation);
            PreTakeOff.AddStateAction(RunTakeOff);
            PreTakeOff.AddStateAction(moveToTarget);
            PreTakeOff.AddTransition(new Transition(TakeOff, WaitingOnAnimationTakeoff));

            TakeOff.AddEntryAction(SetInAirFlyingAimation);
            TakeOff.AddStateAction(RunTakeOff);
            TakeOff.AddStateAction(RunTakeOffRot);
            TakeOff.AddStateAction(moveToTarget);
            TakeOff.AddTransition(new Transition(Fly, DoneTakeOff));

            Fly.AddEntryAction(SetFlying);
            Fly.AddStateAction(SetRotationForInAir);
            Fly.AddStateAction(moveToTarget);
            Fly.AddStateAction(SetFlyNextWaypoint);
            Fly.AddTransition(new Transition(PreDroop, CanDiveFromFly));
            //Fly.AddTransition(new Transition(PreDroop, WasHitAndDroop));
            Fly.AddTransition(new Transition(StartAltDive, ShouldDoSwoop));

            PreDroop.AddStateAction(moveToTarget);// Keep mving 
            PreDroop.AddStateAction(SetRotationForInAir);// do rotaing
            PreDroop.AddStateAction(SetFlyNextWaypoint);/// get next point 
            PreDroop.AddStateAction(FacePlayer);
            PreDroop.AddTransition(new Transition(StartToDroop, IsOverTopOfPlayer));// start the droop once the player in under the AI


            Droop.AddEntryAction(SetNotFlying);
            Droop.AddStateAction(moveToTarget);
            Droop.AddExitAction(EndDroop);
            Droop.AddTransition(new Transition(DroopWave, AtWayPointOROnTheGround));


            DroopWave.AddEntryAction(SetLandingFlyAimation);
            DroopWave.AddEntryAction(LandingWave);
            DroopWave.AddStateAction(FacePlayer);
            DroopWave.AddTransition(new Transition(PreJump, WaveIsDone));
            // end flaying move 


            ReturnToFlightPath.AddStateAction(moveToTarget);
            //ReturnToFlightPath.AddStateAction(SetRotationForInAir);
            ReturnToFlightPath.AddTransition(new Transition(Fly, AtWayPoint));
            ReturnToFlightPath.AddExitAction(ReturnToFlyingSpeed);
            #endregion
            #region jump
            //Jump on player move 
            PreJump.AddEntryAction(SetStartJumpAimation);
            PreJump.AddEntryAction(EntersPreJump);
            PreJump.AddStateAction(FacePlayer);
            PreJump.AddTransition(new Transition(Jump, WaitOnJumpStart));

            Jump.AddEntryAction(SetJumpToPoint);
            Jump.AddEntryAction(SetInAIrJumpAimation);
            Jump.AddStateAction(moveToTarget);
            Jump.AddTransition(new Transition(Fall, AtWayPoint));


            Fall.AddEntryAction(StartDroop);
            Fall.AddStateAction(moveToTarget);
            Fall.AddEntryAction(FacePlayer);
            Fall.AddExitAction(FacePlayer);
            Fall.AddExitAction(PlayGroundHit);
            Fall.AddTransition(new Transition(JumpLanding, AtWayPoint));


            JumpLanding.AddEntryAction(EndDroop);
            JumpLanding.AddEntryAction(SetLandJumpAimation);
            JumpLanding.AddTransition(new Transition(ShootBoothHands, new AndCondition(WaitingOnAnimationLading, WasRecationJump)));
            JumpLanding.AddTransition(new Transition(WaitStateJump, new AndCondition(WaitingOnAnimationLading, new NotCondition(WasRecationJump))));

            WaitStateJump.AddEntryAction(EnterHandsWait);
            WaitStateJump.AddStateAction(FacePlayer);
            WaitStateJump.AddTransition(new Transition(PreJump, new AndCondition(DoneWating, CanJumpAtplayerAgin)));
            WaitStateJump.AddTransition(new Transition(BranchOne, new AndCondition(DoneWating, new NotCondition(CanJumpAtplayerAgin)), new StateMachineAction[] { ExitJumpWait }));

            #endregion
            #region random Jump

            PreJumpRandom.AddEntryAction(EnterStartRandomJump);
            PreJumpRandom.AddEntryAction(SetStartJumpAimation);
            PreJumpRandom.AddEntryAction(EntersPreJump);
            PreJumpRandom.AddStateAction(FaceJumpDir);
            PreJumpRandom.AddTransition(new Transition(JumpRandom, WaitOnJumpStart));

            JumpRandom.AddEntryAction(SetJumpToPoint);
            JumpRandom.AddEntryAction(SetInAIrJumpAimation);
            JumpRandom.AddStateAction(moveToTarget);
            JumpRandom.AddTransition(new Transition(FallRandom, AtWayPoint));

            FallRandom.AddEntryAction(StartDroop);
            FallRandom.AddStateAction(moveToTarget);
            FallRandom.AddTransition(new Transition(JumpLandingRandom, AtWayPoint));

            JumpLandingRandom.AddEntryAction(EndDroop);
            JumpLandingRandom.AddEntryAction(SetLandJumpAimation);
            JumpLandingRandom.AddEntryAction(StartRocksFalling);
            JumpLandingRandom.AddExitAction(PlayGroundHitBig);
            JumpLandingRandom.AddTransition(new Transition(WaitStateJumpRandom, WasHit, new StateMachineAction[] { StopRocksFalling }));
            JumpLandingRandom.AddTransition(new Transition(WaitStateJumpRandom, new AndCondition(WaitingOnAnimationLading, new NotCondition(ShouldJumpInPlace)), new StateMachineAction[] { StopRocksFalling }));
            JumpLandingRandom.AddTransition(new Transition(RejumpJumpRandom, new AndCondition(WaitingOnAnimationLading, ShouldJumpInPlace)));

            RejumpJumpRandom.AddEntryAction(EnterRejump);
            RejumpJumpRandom.AddEntryAction(SetStartJumpAimation);
            RejumpJumpRandom.AddEntryAction(EntersPreJumpRandom);
            RejumpJumpRandom.AddTransition(new Transition(JumpRandom, WaitOnJumpStart));


            WaitStateJumpRandom.AddEntryAction(EnterHandsWait);
            WaitStateJumpRandom.AddStateAction(FacePlayer);
            WaitStateJumpRandom.AddTransition(new Transition(PreJumpRandom, new AndCondition(DoneWating, CanJumpAtplayerAgin)));
            WaitStateJumpRandom.AddTransition(new Transition(ShootBoothHands, new AndCondition(DoneWating, new NotCondition(CanJumpAtplayerAgin), HealthAboveHalf), new StateMachineAction[] { CleanUpRandomJump }));
            WaitStateJumpRandom.AddTransition(new Transition(BranchTwo, new AndCondition(DoneWating, new NotCondition(CanJumpAtplayerAgin), HealthBelowHalf), new StateMachineAction[] { CleanUpRandomJump }));
            #endregion
            #region Braches
            BranchOne.AddEntryAction(EnterDoBranchOne);
            BranchOne.AddTransition(new Transition(PreTakeOff, BranchOneOptionOne));
            BranchOne.AddTransition(new Transition(PreJumpRandom, BranchOneOptionTwo));


            BranchTwo.AddEntryAction(EnterDoBranchTwo);
            BranchTwo.AddTransition(new Transition(PreTakeOff, BranchTwoOptionOne));
            BranchTwo.AddTransition(new Transition(PreTakeOff, BranchTwoOptionTwo));
            BranchTwo.AddTransition(new Transition(PreJumpRandom, BranchTwoOptionThree));
            #endregion
            #region AltDive (Swoop)
            ///alt Dive 
            StartAltDive.AddEntryAction(EnterStartAltDive);// Set the targetVectoert to a min point in the active area
            StartAltDive.AddStateAction(SetRotationForInAir);// Handles rotating while moving
            StartAltDive.AddStateAction(moveToTarget);//Move to the tragetVector
            StartAltDive.AddTransition(new Transition(AltDive, AtWayPoint));// move to next sate on ReadyForNextWaypoint

            AltDive.AddEntryAction(EnterAltDive);// make all the way points
            AltDive.AddStateAction(RunSwoopAtPlayer);// move to next point if ReadyForNextWayPoint is true
            AltDive.AddStateAction(moveToTarget); // move to Waypoint
            AltDive.AddStateAction(SetRotationForInAir);// Handles facing dir while flying
            AltDive.AddExitAction(ExitDiveAtPlayer);// clean up after sate Set the moment speed
            AltDive.AddTransition(new Transition(ReturnToFlightPath, doneWithSwoop2));

            StartToDroop.AddEntryAction(enterRoateToDroop);
            StartToDroop.AddEntryAction(StartDiveAtPlayer);
            StartToDroop.AddStateAction(RoateBeforDroop);
            StartToDroop.AddTransition(new Transition(Droop, doneWithRoateingToDroop));
            //end alt dive
            #endregion
            Activated = true;
        }

        protected override void Update()
        {
            base.Update();
            SetSpriteDirection();
            CalcXDistanceToPlayer();
            RotateTowardsTarget();
            /*if (_flyPoints.Count == 0)
            {
                Debug.LogWarning("No fly points");
            }*/
            _lastinput = MovementInput;
        }

        public override bool SetSpriteDirection()
        {
            return base.SetSpriteDirection();
        }
        #region hand external interface

        public void RequestLeftHandShoot()
        {
            LeftHandyHand.gameObject.SetActive(true);
            LeftHandyHand.Shoot(GlobalData.Player.transform.position, Controller.Collisions.FaceDir == 1);
        }

        public void RequestRightHandShoot()
        {
            RightHandyHand.gameObject.SetActive(true);
            RightHandyHand.Shoot(GlobalData.Player.transform.position, Controller.Collisions.FaceDir == 1);
            _handsConnected[RIGHTHAND] = false;
        }

        public void NotifyOfHandReturn(int hand)
        {
            _handsConnected[hand] = true;
        }
        #endregion
        /// <summary>
        /// Get the Y for flaying by geting the wold point from the viewport at 0.95Y(95% of Max Y on screen)
        /// </summary>
        /// <returns></returns>
        private float GetMaxYOnScreen()
        {
            return ((Vector3)Camera.main.ViewportToWorldPoint(new Vector3(0, 0.95F, 0))).y;
        }
        private void CalcXDistanceToPlayer()
        {
            if (Controller.Collisions.FaceDir == 1)
            {
                _XDistanceToPlayer = GlobalData.Player.transform.position.x - transform.position.x;
            }
            else
            {
                _XDistanceToPlayer = transform.position.x - GlobalData.Player.transform.position.x;
            }
        }
        private void OnDrawGizmosSelected()
        {

            // GizmoHelper.drawString(StateMachine.DebugInfo(), transform.position);

        }
        private void RotateTowardsTarget()
        {
            ///Only while flying and not with 0.1 of the traget 
            if (_flying && !StaticTools.ThresholdApproximately(transform.position, TargetVector, 0.1f))
            {
                Vector2 motionVectoer = TargetVector - transform.position;
                Quaternion targetRotation = Quaternion.Euler(new Vector3(0, 0, Vector2.SignedAngle(Vector2.up, motionVectoer)));
                //Debug.Log(Vector2.SignedAngle(Vector2.up, motionVectoer));
                //transform.eulerAngles = new Vector3(0, 0, Vector2.SignedAngle(Vector2.up, motionVectoer));
                // changed this from a lerp to a RotateTowards because you were supplying a "speed" not an interpolation value
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 500 * Time.deltaTime);
            }
        }
        public override void TakeDamage(IDamageGiver giver, float amount, GlobalData.DamageType type, Vector3 hitPoint)
        {
            _wasHit = true;
            base.TakeDamage(giver, amount, type, hitPoint);

        }

        private Vector3 MakePointInAreaBounds(Vector3 point)
        {
            if (point.x < ActiveArea.xMin + AREABUFFERAMOUNT)
            {
                point.x = ActiveArea.xMin + AREABUFFERAMOUNT;
            }
            if (point.x > ActiveArea.xMax - AREABUFFERAMOUNT)
            {
                point.x = ActiveArea.xMax - AREABUFFERAMOUNT;
            }

            return point;
        }
        public void AimRightHand()
        {
            _rightHandTarget = GlobalData.Player.transform.position + Vector3.up;
        }
        public void AimLeftHand()
        {
            _leftHandTarget = GlobalData.Player.transform.position + Vector3.up * 2;
        }

        public void ShootLeftHand()
        {
            ShootHandsSound.Play();
            _handsConnected[LEFTHAND] = false;
            LeftHandyHand.gameObject.SetActive(true);
            LeftHandyHand.Shoot(_leftHandTarget, Controller.Collisions.FaceDir == 1);

        }
        public void ShootRightHand()
        {
            //VisualDebug.DrawDebugPoint(RightHandTarget, Color.blue);
            ShootHandsSound.Play();
            _handsConnected[RIGHTHAND] = false;
            RightHandyHand.gameObject.SetActive(true);
            RightHandyHand.Shoot(_rightHandTarget, Controller.Collisions.FaceDir == 1);
        }


        public void NotifyOfAnimationEnd()
        {
            _waitOnAnimation = false;
        }

        private void FlipDirInAir(bool left)
        {

            if (left)
            {
                transform.eulerAngles = new Vector3(0, 0, 90);//(CharacterSpriteCollection.transform.eulerAngles.x, CharacterSpriteCollection.transform.eulerAngles.y, CharacterSpriteCollection.transform.eulerAngles.z);
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, -90);//(CharacterSpriteCollection.transform.eulerAngles.x, CharacterSpriteCollection.transform.eulerAngles.y, -CharacterSpriteCollection.transform.eulerAngles.z);
            }

        }
        private void LateUpdate()
        {
            InGameDebugText.DebugText[0].ShowText(_behaviourStateMachine.DebugInfo(), transform.position);
            if (_canIkAim)
            {
                LeftHandIK.transform.position = _leftHandTarget;
                RightHandIK.transform.position = _rightHandTarget;

            }
        }
        /*public override void DoDeathExplotion()
         {
             StartCoroutine(Explosions());
         }*/
        public override void DoDeathExplotion()
        {
            if (IsDead && !_destructionPlayed)
            {

                _destructionPlayed = true;
                _destruction?.DestroyAndReplace(1, _scrapValue);
                if (_destruction == null)
                {
                    GameObject.Destroy(gameObject, 1);
                }
            }
        }
        /// <summary>
        /// Creates a series of explosions on the death of the gunner.
        /// </summary>
        /// <returns></returns>
        private IEnumerator<float> Explosions()
        {
            BoxCollider2D gunnerCollider = GetComponent<BoxCollider2D>();

            int totalNumExplosions = 30;
            int currentNumExplosions = 0;
            float timeBetweenExplosions = 0.5f;
            float timeSinceLastExplosion = 0;
            bool timeReduced = false;

            Vector2 minBoundsPosition = new Vector2(gunnerCollider.bounds.min.x + gunnerCollider.bounds.size.x * 0.2f, gunnerCollider.bounds.min.y + gunnerCollider.bounds.size.y * 0.3f);
            Vector2 maxBoundsPosition = new Vector2(gunnerCollider.bounds.max.x - gunnerCollider.bounds.size.x * 0.2f, gunnerCollider.bounds.max.y - gunnerCollider.bounds.size.y * 0.2f);

            while (currentNumExplosions < totalNumExplosions)
            {
                if ((currentNumExplosions == 5 || currentNumExplosions == 10) && !timeReduced)
                {
                    //We want the explosion rate to speed up, so we reduce the time between explosions at certain intervals.
                    timeBetweenExplosions /= 2;
                    timeReduced = true;
                }

                if (timeSinceLastExplosion >= timeBetweenExplosions)
                {
                    if (currentNumExplosions < totalNumExplosions - 1)
                    {
                        //All explosions before the last one should just be for effect (i.e. they don't replace the gunner) and occur at a random spot within the gunner's bounds
                        Vector2 randomExplosionPosition = new Vector2(Random.Range(minBoundsPosition.x, maxBoundsPosition.x), Random.Range(minBoundsPosition.y, maxBoundsPosition.y));
                        _destruction.JustPlayExplosion(randomExplosionPosition,1);

                        ++currentNumExplosions;
                        timeReduced = false;
                    }
                    else
                    {
                        //The final explosion is a much larger one that actually destroys and replaces the gunner.
                        _destruction.DestroyAndReplace(transform.parent.localScale.x * CharacterSpriteCollection.transform.localScale.x, _scrapValue, 3f);
                        ++currentNumExplosions;
                    }

                    timeSinceLastExplosion = 0;
                }
                else
                {
                    timeSinceLastExplosion += Time.deltaTime;
                }

                yield return 0f;
            }

            yield return 0f;
        }
    }

}