﻿using UnityEngine;
using System.Collections;
using Anima2D;
using UnityEngine.SceneManagement;

namespace Characters.Enemies.Bosses.Handy
{
    public class HandyHand : MonoBehaviour, IDamageGiver
    {
        /// <summary>
        /// Amount of Damage applied
        /// </summary>
        public float Damage;
        /// <summary>
        /// The Speed And Dir the hand will travel when facing left. Right facing uses the invers on the speed 
        /// </summary>
        public float Speed;
        /// <summary>
        /// The NPC
        /// </summary>
        public HandyBoss owner;
        /// <summary>
        /// What Hand is this
        /// </summary>
        public HandyBoss.Hand hand;
        /// <summary>
        /// The Hand As An index
        /// </summary>
        [HideInInspector]
        public int handint = 0;
        /// <summary>
        /// The Fore Arm Bone of the NPC
        /// </summary>
        public Bone2D ArmBone;
        /// <summary>
        /// The Real Hand to be replaced
        /// </summary>
        public GameObject RealHand;
        /// <summary>
        /// Is the hand returing to the hand 
        /// </summary>
        public bool IsReturning { get { return _isReturning && _hasBeenFired; } }
        public bool HasReturned { get { return _isHome; } }


        private bool _isHome = false;
        private bool _isReturning;

        private bool _hasBeenFired;

        private Vector3 _velocity;
        private Vector3 _finalVelocity;
        private Vector3 _target;
        private Transform _parent;
        private Transform _SceanRoot;
        private const float MAX_DISTANCE = 5;
        private float _timeShoot;
        private float _storedScale;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="collision"></param>
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == GlobalData.PLAYER_TAG)
            {
                GlobalData.Player.TakeDamage(this, Damage, GlobalData.DamageType.PROJECTILE, Vector3.zero);
                _isReturning = true;
            }
            if (collision.gameObject.layer == GlobalData.OBSTACLE_LAYER)
            {
                _isReturning = true;
            }

            //_isReturning = true;
        }

        private void Awake()
        {
            _isHome = false;
            _isReturning = false;
            _hasBeenFired = false;
            MatchHandBone(ArmBone);
            RealHand.SetActive(false);
            _parent = transform.parent;

            _storedScale = transform.localScale.x;
            if (hand == HandyBoss.Hand.LEFT)
            {
                handint = HandyBoss.LEFTHAND;
            }
            else
            {
                handint = HandyBoss.RIGHTHAND;
            }

        }

        private void OnDisable()
        {
            _isHome = false;
        }

        public void Shoot(bool facingRight = false)
        {
            MatchHandBone(ArmBone);
            Debug.Break();
            _timeShoot = 0;
            _isHome = false;
            _hasBeenFired = true;
            _finalVelocity = Vector3.zero;
            _timeShoot = 0;
            transform.parent = null;
            if (facingRight)// if the NPC is Facing right then shoot with the inversed speed 
            {
                _velocity = Vector3.right * Speed;
                transform.localScale = new Vector3(_storedScale, _storedScale, _storedScale);
                transform.eulerAngles = new Vector3(0, 0, -Vector3.Angle(transform.position, _target));

            }
            else
            {
                _velocity = Vector3.left * Speed;
                transform.localScale = new Vector3(-_storedScale, _storedScale, _storedScale);
                transform.eulerAngles = new Vector3(0, 0, Vector3.Angle(transform.position, _target));

            }
        }

        public void Shoot(Vector3 target, bool facingRight = false)
        {
            MatchHandBone(ArmBone);
            _isHome = false;// set the hands as not home
            _hasBeenFired = true;//set the hand as fired
            _target = target;/// set the traget 
            _finalVelocity = Vector3.zero;
            _timeShoot = 0;
            transform.parent = null;
            Vector3 baseDir = _target - transform.position;
            baseDir.Normalize();

            if (facingRight)// if the NPC is Facing right then shoot with the inversed speed 
            {
                _velocity = baseDir * Speed;
                transform.localScale = new Vector3(_storedScale, _storedScale, _storedScale);
                transform.eulerAngles = new Vector3(0, 0, -Vector3.Angle(transform.position, _target));
            }
            else
            {
                _velocity = baseDir * Speed;
                transform.localScale = new Vector3(-_storedScale, _storedScale, _storedScale);
                transform.eulerAngles = new Vector3(0, 0, Vector3.Angle(transform.position, _target));
            }
        }
        // Update is called once per frame
        void Update()
        {
            //if the owner is dead the destroy
            if (owner.Health <= 0)
            {
                Destroy(gameObject);
            }
            if (_hasBeenFired)
            {
                if (!_isReturning)
                {
                    _timeShoot += Time.deltaTime;
                    if (_timeShoot <= 1)// give the speed a ramp up 
                    {
                        _finalVelocity = StaticTools.EaseInSine(Vector3.zero, _velocity, _timeShoot, 1);
                    }
                    else if (_finalVelocity != _velocity)
                    {
                        _finalVelocity = _velocity;
                    }

                    transform.Translate((_finalVelocity * Time.deltaTime));
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, ArmBone.endPosition, Speed * Time.deltaTime);
                    if (ArmBone.endPosition == transform.position)// if the new pos is the            
                    {
                        Returned();
                    }

                }
            }
        }


        /**
         * Close Approximation sprite must have it Pivot match that of the end of the bone
         * */
        private void MatchHandBone(Bone2D bone)
        {
            //set to bone end point of arm bone
            transform.position = bone.endPosition;
            //set to rotation of hand bone (The first child of the arm bone)
            transform.rotation = bone.child.transform.rotation;
        }

        private bool HasGoneTooFar()
        {
            return (Mathf.Abs(transform.position.x - ArmBone.endPosition.x) > MAX_DISTANCE);
        }

        private void Returned()
        {
            owner.NotifyOfHandReturn(handint);
            gameObject.SetActive(false);
            RealHand.SetActive(true);
            _hasBeenFired = false;
            _isReturning = false;
            _isHome = true;
            transform.parent = _parent;
        }
    }
}