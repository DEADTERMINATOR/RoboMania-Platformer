﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Enemies.AnimationControllers
{
    public class MiteAnimationController : MonoBehaviour, INPCAnimationController
    {
        /// <summary>
        /// Reference to the animator to set the parameters for the animations.
        /// </summary>
        private Animator _anim;


        private void Start()
        {
            _anim = GetComponentInChildren<Animator>();
        }


        public void SetStunnedAnimation(bool status)
        {
            _anim.SetBool("Stunned", status);
        }

        public void SetWalkAnimation()
        {
            SetStunnedAnimation(false);
        }

        #region UNUSED ANIMATIONS
        public void SetIdleAnimation()
        {
            //Do nothing, The mite doesn't have an idle animation.
        }

        public void SetAlertAnimation()
        {
            //Do nothing, The mite doesn't have an alert animation.
        }

        public void SetShootAnimation()
        {
            //Do nothing, The mite doesn't have a shoot animation.
        }

        public void SetStopShootingAnimation()
        {
            //Do nothing, The mite doesn't have a shoot animation.
        }

        public void SetDeathAnimation()
        {
            //Do nothing. The mite doesn't have a death animation (it just blows up).
        }
        #endregion
    }
}
