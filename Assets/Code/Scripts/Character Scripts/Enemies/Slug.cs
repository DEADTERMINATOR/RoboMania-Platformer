﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using CompletionState = StateMachineAction.ActionCompletionState;

namespace Characters.Enemies
{
    public class Slug : Enemy
    {
        #region Constants
        /// <summary>
        /// The factor that we multiply the slug's move speed by when they are charging the player.
        /// </summary>
        private const float CHARGE_SPEED_MODIFIER = 3f;

        /// <summary>
        /// How long the easing period should last when the slug is slowing down from charge into assessing.
        /// </summary>
        private const float STOP_AFTER_CHARGE_EASING_TIME = 0.45f;

        /// <summary>
        /// The factor that we divide the slug's sight distance for their spatial awareness (behind them) raycast. Only slugs that are medium or tough difficulty have spatial awareness.
        /// </summary>
        private const float SPATIAL_AWARENESS_SIGHT_DISTANCE_DIVISOR = 3f;

        /// <summary>
        /// How long the slug's assess state should last.
        /// </summary>
        private const float TOTAL_ASSESS_TIME = 0.75f;
        #endregion

        public enum SlugDifficulties { Easy, Medium, Tough }

        #region Public Variables
        /// <summary>
        /// Reference to the knockback logic on the shield collider.
        /// </summary>
        public KnockbackPlayer ShieldColliderKnockback;

        /// <summary>
        /// How far in front of it can the slug see. If the slug possesses spatial awareness, the distance they can detect behind them
        /// will be a third of this value.
        /// </summary>
        public float SightDistance;

        /// <summary>
        /// The difficulty level of the slug (determines its behaviours).
        /// </summary>
        public SlugDifficulties Difficulty;
        #endregion

        #region Private Variables
        /// <summary>
        /// Whether the slug can currently see the player.
        /// </summary>
        private bool _canSeePlayer = false;

        /// <summary>
        /// Whether the slug can see a projectile coming at them.
        /// </summary>
        private bool _canSeeProjectile = false;

        /// <summary>
        /// Whether the slug is currently blocking.
        /// </summary>
        private bool _isBlocking;

        /// <summary>
        /// Whether the slug is currently alerted.
        /// </summary>
        private bool _isAlerted;

        /// <summary>
        /// Whether the slug has finished its current charge.
        /// </summary>
        private bool _isFinishedCharge;

        /// <summary>
        /// Whether the slug is taking a peek for the player after finishing a charge.
        /// </summary>
        private bool _isPeeking;
        
        /// <summary>
        /// Whether the player is close enough to the ground that if the slug were to charge at them, the player would likely be hit.
        /// </summary>
        private bool _playerLowEnoughToCharge;

        /// <summary>
        /// The elapsed time the slug has been in the assess action.
        /// </summary>
        private float _assessTime;

        /// <summary>
        /// Whether the slug has finished their assessment state.
        /// </summary>
        private bool _finishedAssessment;

        private MoveToTarget _chargeState;
        #endregion

        #region Start + Update
        // Use this for initialization
        protected override void Start()
        {
            base.Start();

            _stillUsingOldStateMachine = false;
            ShieldColliderKnockback.Triggered += new KnockbackPlayer.NotifyPlayerEnteredTrigger(SlugHitPlayer);

            VisualDebug.DrawBox(ActiveArea.center, ActiveArea.size / 2, Quaternion.identity, Color.blue, 5.0f);

            //Setup the state machine.
            PatrolState patrol = new PatrolState(this, BaseMoveSpeed, true, 0, new GenericRaycastingState.PlayerRaycastParameters(EasySlugRaycastForPlayer));
            _chargeState = new MoveToTarget(this, 'X', true, GlobalData.Player.gameObject, platformBoundingBox);
            State assess = new State("Assess");

            //Create and add the state machine.
            _behaviourStateMachine = new StateMachine(patrol, activated);

            //Create actions.
            //Commented out because I may return to this and refine it.
            /*
            StateMachineAction easeSlugMoveSpeedToZero = new FunctionPointerAction(new Func<bool>(() =>
            {
                if (MoveSpeed > 0)
                {
                    MoveSpeed = StaticTools.EaseInSine(_storedStartingMoveSpeed, 0, _elapsedSpeedEasingTime, STOP_AFTER_CHARGE_EASING_TIME);

                    if (MoveSpeed < 0)
                    {
                        MoveSpeed = 0;
                    }

                    _elapsedSpeedEasingTime += Time.deltaTime;
                    return false;
                }
                else
                {
                    _elapsedSpeedEasingTime = 0;
                }

                _storedStartingMoveSpeed = MoveSpeed;
                _animator.SetInteger("Horizontal Velocity", (int)_storedStartingMoveSpeed);
                return true;
            }));
            */
            Func<object, CompletionState> setSlugMoveSpeed = new Func<object, CompletionState>((moveSpeed) =>
            {
                MoveSpeed = (float)moveSpeed;
                SetVelocityOnOneAxis('X', MoveSpeed * Collisions.FaceDir);
                animator.SetInteger("Horizontal Velocity", (int)MoveSpeed);

                return CompletionState.GuaranteedSuccess;
            });
            Func<object, CompletionState> assessWait = new Func<object, CompletionState>((waitTime) =>
            {
                _assessTime += Time.deltaTime;
                if (_assessTime < (float)waitTime)
                {
                    return CompletionState.InProgress;
                }

                _assessTime = 0;
                return CompletionState.Successful;
            });
            StateMachineAction clearAssessingStatus = new FunctionPointerAction(new Func<CompletionState>(() =>
            {
                _finishedAssessment = false;
                return CompletionState.GuaranteedSuccess;
            }));
            Func<object, CompletionState> setPeekStatus = new Func<object, CompletionState>((status) =>
            {
                SetPeekStatus((Boolean)status);
                return CompletionState.GuaranteedSuccess;
            });

            //Create conditions.
            Condition canSeePlayerAndIsAlerted = new FunctionPointerCondition(new Func<bool>(() => { return _canSeePlayer && _isAlerted; }));
            Condition assessmentFinished = new FunctionPointerCondition(new Func<bool>(() => { return _finishedAssessment; }));

            //Create transitions.
            Transition patrolToCharge = new Transition(_chargeState, new FunctionPointerCondition(patrol.CanSeePlayer));
            Transition chargeToAssess = new Transition(assess, new FunctionPointerCondition(_chargeState.ArrivedAtTarget));
            Transition chargeToPatrol = new Transition(patrol, new FunctionPointerCondition(_chargeState.CantGetToTarget));
            Transition assessToCharge = new Transition(_chargeState, canSeePlayerAndIsAlerted);
            Transition assessToPatrol = new Transition(patrol, assessmentFinished);

            //Add actions to states.
            patrol.AddEntryAction(new FunctionPointerAction(setSlugMoveSpeed, BaseMoveSpeed));

            _chargeState.AddEntryAction(new FunctionPointerAction(setSlugMoveSpeed, BaseMoveSpeed * CHARGE_SPEED_MODIFIER));

            assess.AddEntryAction(new FunctionPointerAction(setSlugMoveSpeed, 0f));

            assess.AddStateAction(new FunctionPointerAction(setPeekStatus, true));
            assess.AddStateAction(new FunctionPointerAction(assessWait, TOTAL_ASSESS_TIME / 2f));
            assess.AddStateAction(Raycast);
            assess.AddStateAction(new FunctionPointerAction(assessWait, TOTAL_ASSESS_TIME / 2f));
            assess.AddStateAction(new FunctionPointerAction(() => { _finishedAssessment = true; return CompletionState.GuaranteedSuccess; }));

            assess.AddExitAction(new FunctionPointerAction(setPeekStatus, false));
            assess.AddExitAction(clearAssessingStatus);

            //Add transitions to states.
            patrol.AddTransition(patrolToCharge);

            _chargeState.AddTransition(chargeToAssess);
            _chargeState.AddTransition(chargeToPatrol);

            assess.AddTransition(assessToCharge);
            assess.AddTransition(assessToPatrol);

            //Add remaining states to state machine
            BehaviourStateMachine.AddState(patrol);
            BehaviourStateMachine.AddState(_chargeState);
            BehaviourStateMachine.AddState(assess);
        }

        // Update is called once per frame
        protected override void Update()
        {
            if (!frozen)
            {
                base.Update();
                SetSpriteDirection();
            }
        }
        #endregion

        #region Overridden Methods
        public override void TakeDamage(IDamageGiver giver, float amount, GlobalData.DamageType type, Vector3 hitPoint)
        {
            base.TakeDamage(giver, amount, type, hitPoint);
            if (Difficulty == SlugDifficulties.Tough)
            {
                Vector2 directionToPlayer = GlobalData.Player.transform.position - transform.position;
                directionToPlayer.Normalize();

                if (Mathf.Sign(directionToPlayer.x) != Controller.Collisions.FaceDir)
                    SetSpriteDirection(Controller.Collisions.FaceDir != 1 ? true : false);
            }
        }
        #endregion

        #region Raycasting
        private StateMachineAction.ActionCompletionState Raycast()
        {
            RaycastHit2D hit = new RaycastHit2D();

            if (Difficulty == SlugDifficulties.Easy)
            {
                hit = EasySlugRaycastForPlayer();
            }
            else if (Difficulty == SlugDifficulties.Medium)
            {
                hit = MediumSlugRaycastForPlayer();
            }
            else
            {
                hit = ToughSlugRaycastForPlayer();
            }

            return hit.collider != null ? StateMachineAction.ActionCompletionState.Successful : StateMachineAction.ActionCompletionState.Failed;
        }

        private RaycastHit2D EasySlugRaycastForPlayer()
        {
            RaycastHit2D hit = new RaycastHit2D();

            _isFinishedCharge = false;

            Vector2 playerDirection = RaycastWithoutSpatialAwareness(ref hit);

            if (hit.collider != null)
            {
                //We only want the slug to set the player's current position as their target if they aren't already charging at them.
                //The behiaviour should be that once the slug sees the player, they put their shield down and charge toward them,
                //and they don't look a where they are going. This also gives the player more of an opportunity to jump over them.
                _canSeePlayer = true;

                //If the slug detects the player, we want the slug to go back to its blocking animation. If this method was called during the slug's peek time,
                //this we also need to set the trigger to move the animation state back to the block look.
                SetBlockStatus(true);
                if (_isPeeking)
                {
                    SetBlockFromPeekTrigger();
                }

                //If the raycast hit the player, make sure they are within the enemy's active area before going after them, to prevent the enemy running itself off the edge of a platform,
                //or some other stupid thing.
                VisualDebug.DrawBox(ActiveArea.center, ActiveArea.size / 2, Quaternion.identity, Color.blue, 5.0f);
                VisualDebug.DrawDebugPoint(new Vector2(hit.collider.gameObject.transform.position.x, hit.collider.gameObject.transform.position.y), Color.blue);
                if (_playerLowEnoughToCharge && StaticTools.RectContains(ActiveArea, new Vector2(hit.collider.gameObject.transform.position.x, hit.collider.gameObject.transform.position.y)))
                {
                    _isAlerted = true;
                }
            }
            else
            {
                _canSeePlayer = false;
                _isAlerted = false;

                SetBlockStatus(false);
                if (_isPeeking)
                {
                    SetMoveFromPeekTrigger();
                }
            }

            return hit;
        }

        private RaycastHit2D MediumSlugRaycastForPlayer()
        {
            RaycastHit2D horizontalHit = new RaycastHit2D();
            RaycastHit2D verticalHit = new RaycastHit2D();

            _isFinishedCharge = false;
            Vector2 playerDirection = RaycastWithSpatialAwareness(ref horizontalHit, ref verticalHit);

            if (horizontalHit.collider != null && horizontalHit.collider.gameObject.gameObject.layer == GlobalData.PLAYER_LAYER)
            {
                _canSeePlayer = true;
                if (_playerLowEnoughToCharge && ActiveArea.Contains(new Vector2(horizontalHit.collider.gameObject.transform.position.x + (playerDirection.x * 0.25f), horizontalHit.collider.gameObject.transform.position.y)))
                {
                    _isAlerted = true;
                }
            }
            else if (verticalHit.collider != null)
            {
                //If the slug can't see the player in front of them, but "senses" the player above/jumping over them, then the slug will turn around and see if they are visible.
                //If so, the slug will charge.
                _canSeePlayer = true;
                SetSpriteDirection(Controller.Collisions.FaceDir != 1 ? true : false);
            }
            else
            {
                _canSeePlayer = false;
                _isAlerted = false;
            }

            return verticalHit.collider != null ? verticalHit : horizontalHit;
        }

        private RaycastHit2D ToughSlugRaycastForPlayer()
        {
            RaycastHit2D horizontalHit = new RaycastHit2D();
            RaycastHit2D verticalHit = new RaycastHit2D();

            _isFinishedCharge = false;
            Vector2 playerDirection = RaycastWithSpatialAwareness(ref horizontalHit, ref verticalHit);

            if (horizontalHit.collider != null)
            {
                if (horizontalHit.collider.gameObject.gameObject.layer == GlobalData.PLAYER_LAYER)
                {
                    _canSeePlayer = true;
                    if (ActiveArea.Contains(new Vector2(horizontalHit.collider.gameObject.transform.position.x + (playerDirection.x * 0.25f), horizontalHit.collider.gameObject.transform.position.y)))
                    {
                        _isAlerted = true;
                    }
                }
                else
                {
                    _canSeeProjectile = true;
                }
            }
            else if (verticalHit.collider != null)
            {
                //If the slug can't see the player in front of them, but "senses" the player above/jumping over them, then the slug will turn around and see if they are visible.
                //If so, the slug will charge.
                _canSeePlayer = true;
                SetSpriteDirection(Controller.Collisions.FaceDir != 1 ? true : false, true, false);
            }
            else
            {
                _canSeePlayer = false;
                _canSeeProjectile = false;
                _isAlerted = false;
            }

            return verticalHit.collider != null ? verticalHit : horizontalHit;
        }

        /// <summary>
        /// Performs the appropriate forward raycast check for the player if this NPC doesn't have spatial awareness.
        /// </summary>
        /// <param name="horizontalHit">A RaycastHit2D to store the result of the forward and backward raycasts.</param>
        private Vector2 RaycastWithoutSpatialAwareness(ref RaycastHit2D horizontalHit)
        {
            float baseAngle = CharacterSpriteCollection.transform.rotation.eulerAngles.z;
            if (Controller.Collisions.FaceDir == 1) //slug facing right
            {
                horizontalHit = Physics2D.Raycast(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle), SightDistance, GlobalData.PLAYER_LAYER_SHIFTED);
                Debug.DrawRay(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle) * SightDistance, Color.green, 0.1f);
            }
            else //NPC facing left
            {
                horizontalHit = Physics2D.Raycast(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle + 180), SightDistance, GlobalData.PLAYER_LAYER_SHIFTED);
                Debug.DrawRay(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle + 180) * SightDistance, Color.green, 0.1f);
            }

            //If the player isn't directly in front of the slug, check if they are just slightly above but still in front.
            if (horizontalHit.collider == null)
            {
                RaycastArcInFront(ref horizontalHit, GlobalData.PLAYER_LAYER_SHIFTED);
            }

            if (horizontalHit.collider != null)
            {
                _playerLowEnoughToCharge = true;

                //Create an offset in the direction of the player to be added to the player's collider position to create a target vector slightly behind the player.
                //This solves the occasional issue where the enemy perfectly hits the stopping distance before the player so they just stop in front of the player.
                Vector2 playerDirection = new Vector2(horizontalHit.collider.transform.position.x, horizontalHit.collider.transform.position.y) - new Vector2(transform.position.x, transform.position.y);
                playerDirection.Normalize();
                return playerDirection;
            }
            return Vector2.zero;
        }

        /// <summary>
        /// Performs the appropriate raycast checks for the player if this NPC has spatial awareness. Checks for both the player and for incoming projectiles (since the tough slug,
        /// who blocks incoming projectiles, uses this version of the method).
        /// </summary>
        /// <param name="horizontalHit">A RaycastHit2D to store the result of the forward and backward raycasts.</param>
        /// <param name="verticalHit">A RaycastHit2D to store the result of the diagonal raycasts.</param>
        private Vector2 RaycastWithSpatialAwareness(ref RaycastHit2D horizontalHit, ref RaycastHit2D verticalHit)
        {
            //Everytime we perform a raycast, we need to provide a way to break out, as subsequent raycasts overwrite the results of the previous.
            //Perform the forward and backward raycasts.
            float baseAngle = CharacterSpriteCollection.transform.rotation.eulerAngles.z;
            if (Controller.Collisions.FaceDir == 1) //slug facing right
            {
                horizontalHit = Physics2D.Raycast(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle), SightDistance, GlobalData.PLAYER_LAYER_SHIFTED | GlobalData.PROJECTILE_LAYER_SHIFTED);
                Debug.DrawRay(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle) * SightDistance, Color.green, 0.1f);

                if (horizontalHit.collider == null)
                {
                    horizontalHit = Physics2D.Raycast(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle + 180), SightDistance / SPATIAL_AWARENESS_SIGHT_DISTANCE_DIVISOR, GlobalData.PLAYER_LAYER_SHIFTED | GlobalData.PROJECTILE_LAYER_SHIFTED);
                    Debug.DrawRay(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle + 180) * SightDistance / SPATIAL_AWARENESS_SIGHT_DISTANCE_DIVISOR, Color.red, 0.1f);
                }
            }
            else //NPC facing left
            {
                horizontalHit = Physics2D.Raycast(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle + 180), SightDistance, GlobalData.PLAYER_LAYER_SHIFTED | GlobalData.PROJECTILE_LAYER_SHIFTED);
                Debug.DrawRay(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle + 180) * SightDistance, Color.green, 0.1f);

                if (horizontalHit.collider == null)
                {
                    horizontalHit = Physics2D.Raycast(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle), SightDistance / SPATIAL_AWARENESS_SIGHT_DISTANCE_DIVISOR, GlobalData.PLAYER_LAYER_SHIFTED | GlobalData.PROJECTILE_LAYER_SHIFTED);
                    Debug.DrawRay(CharacterSpriteCollection.transform.position, StaticTools.Vector2FromAngle(baseAngle) * SightDistance / SPATIAL_AWARENESS_SIGHT_DISTANCE_DIVISOR, Color.red, 0.1f);
                }
            }

            //If the player isn't directly in front or behind the slug, check if they are just slightly above but still in front.
            if (horizontalHit.collider == null)
            {
                RaycastArcInFront(ref horizontalHit, GlobalData.PLAYER_LAYER_SHIFTED | GlobalData.PROJECTILE_LAYER_SHIFTED);

                //If the player isn't in front of the slug at all, check if they are above the slug.
                if (horizontalHit.collider == null)
                {
                    Vector2 raycastDirection;

                    //Send rays out in an up to 180 degree arc above the enemy in case the player decides to jump over and aren't in front of the enemy anymore.
                    for (int i = 15; i < 180; i += 15)
                    {
                        raycastDirection = StaticTools.Vector2FromAngle(baseAngle + i);
                        verticalHit = Physics2D.Raycast(CharacterSpriteCollection.transform.position, raycastDirection, SightDistance / SPATIAL_AWARENESS_SIGHT_DISTANCE_DIVISOR, 1 << 8);
                        Debug.DrawRay(CharacterSpriteCollection.transform.position, raycastDirection * SightDistance / 3, Color.magenta, 0.1f);
                        if (verticalHit.collider != null)
                        {
                            //We got a hit, so don't check any other angles.
                            break;
                        }
                    }
                }
            }

            if (horizontalHit.collider != null)
            {
                if (horizontalHit.collider.gameObject.tag == " Player")
                {
                    _playerLowEnoughToCharge = true;
                }

                //Create an offset in the direction of the player to be added to the player's collider position to create a target vector slightly behind the player.
                //This solves the occasional issue where the enemy perfectly hits the stopping distance before the player so they just stop in front of the player.
                Vector2 threatDirection = new Vector2(horizontalHit.collider.transform.position.x, horizontalHit.collider.transform.position.y) - new Vector2(transform.position.x, transform.position.y);
                threatDirection.Normalize();
                return threatDirection;
            }
            return Vector2.zero;
        }

        private void RaycastArcInFront(ref RaycastHit2D horizontalHit, int raycastHitLayer)
        {
            float baseAngle = CharacterSpriteCollection.transform.rotation.eulerAngles.z;
            Vector2 raycastDirection;

            for (int i = 5; i < 50; i += 5)
            {
                //Rotate the vector to the desired angle.
                if (Controller.Collisions.FaceDir == -1)
                {
                    raycastDirection = StaticTools.Vector2FromAngle(baseAngle + (180 - i)); //If the slug is facing to the left, we add 180 degrees so we get the desired rotation on the other side.
                }
                else
                {
                    raycastDirection = StaticTools.Vector2FromAngle(baseAngle + i);
                }

                horizontalHit = Physics2D.Raycast(transform.position, raycastDirection, SightDistance, GlobalData.PLAYER_LAYER_SHIFTED);
                Debug.DrawRay(transform.position, raycastDirection * SightDistance, Color.blue, 0.025f);

                if (horizontalHit.collider != null)
                {
                    //If the slug has seen the player, we don't need to continue with further raycasts
                    if (i <= 10)
                    {
                        _playerLowEnoughToCharge = true;
                    }
                    return;
                }
            }
        }
        #endregion

        #region Animations
        public void SetBlockStatus(bool status)
        {
            animator.SetBool("Block", status);
            _isBlocking = status;
        }

        public void SetPeekStatus(bool status)
        {
            animator.SetBool("Peek", status);
            _isPeeking = status;
        }

        public void SetMoveFromPeekTrigger()
        {
            animator.SetTrigger("Back To Move");
            SetPeekStatus(false);
        }

        public void SetBlockFromPeekTrigger()
        {
            animator.SetTrigger("Back To Block");
            SetPeekStatus(false);
        }
        #endregion

        protected override bool AnalyzePlatform(bool drawDebugInfo = false)
        {
            var result = base.AnalyzePlatform(drawDebugInfo);
            if (result)
            {
                _chargeState.SetMoveableArea(platformBoundingBox);
            }

            return result;
        }

        private void SlugHitPlayer()
        {
            _isFinishedCharge = false;
            _isAlerted = false;
        }
    }
}
