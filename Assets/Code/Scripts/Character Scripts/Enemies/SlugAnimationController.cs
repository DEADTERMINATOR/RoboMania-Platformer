﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Enemies.AnimationControllers
{
    public class SlugAnimationController : MonoBehaviour, INPCAnimationController
    {
        private Animator _anim;


        // Use this for initialization
        private void Start()
        {
            _anim = GetComponentInChildren<Animator>();
        }


        public void SetIdleAnimation()
        {
            _anim.SetBool("Moving", false);
            _anim.SetBool("AlertedMoving", false);
            _anim.SetBool("Blocking", false);
        }

        public void SetWalkAnimation()
        {
            _anim.SetBool("Moving", true);
            _anim.SetBool("AlertedMoving", false);
            _anim.SetBool("Blocking", false);
        }

        public void SetAlertAnimation()
        {
            _anim.SetBool("AlertedMoving", true);
            _anim.SetBool("Moving", false);
        }

        public void SetBlockAnimation()
        {
            _anim.SetBool("Blocking", true);
        }

        public void SetUnblockAnimation()
        {
            _anim.SetBool("Blocking", false);
        }

        public void SetHitTrigger()
        {
            _anim.SetTrigger("Hit");
        }

        public void SetDeathAnimation()
        {
            _anim.SetBool("Dead", true);
        }

        public void SetStunnedAnimation(bool status)
        {
            _anim.SetBool("Stunned", status);
        }

        #region Unused Animations
        public void SetShootAnimation()
        {
            //Does not have a shoot animation.
        }

        public void SetStopShootingAnimation()
        {
            //Does not have a shoot animation.
        }
        #endregion
    }
}
