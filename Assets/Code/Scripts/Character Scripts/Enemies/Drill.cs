﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalData;

namespace Characters.Enemies
{
    public class Drill : Enemy
    {
        private readonly Vector3 OFFSCREEN_POSITION = new Vector3(9999, 9999, 0);

        public delegate void DestroyEvent(Drill destroyedDrill);
        public event DestroyEvent DrillDestroyed;

        /// <summary>
        /// The maximum amount of time that can elapse before the drill drops down again.
        /// This is only used when the drill is a random drill.
        /// </summary>
        public float TimeBetweenAttacks;

        /// <summary>
        /// The amount of time on the drill's drop time that the drill will begin with.
        /// Used to stagger sequential drills.
        /// </summary>
        public float StartingTime;

        /// <summary>
        /// The direction the drill will travel in.
        /// </summary>
        public float TravelAngle;

        /// <summary>
        /// The distance the drill should travel.
        /// </summary>
        public float TravelDistance;

        /// <summary>
        /// Whether the drill should be destroyed after travelling the required distance, instead of just moved offscreen.
        /// </summary>
        [HideInInspector]
        public bool DestroyAfterTravellingDesiredDistance = false;

        /// <summary>
        /// The position the drill should start from (for times when the drill is moved off screen inbetween attacks).
        /// </summary>
        [HideInInspector]
        public Vector3 StartingPosition = new Vector3(9999, 9999, 0);

        /// <summary>
        /// Whether the drill should move on the Y-axis.
        /// </summary>
        public bool MoveOnYAxis;

        /// <summary>
        /// Whether the drill should flip from it's default direction
        /// (go up instead of down, or right instead of left).
        /// </summary>
        public bool FlipDirection;

        /// <summary>
        /// Whether the drill has hit the ground (or the player) during its drill attack.
        /// </summary>
        [HideInInspector]
        public bool HitGround;

        /// <summary>
        /// The starting position of the drill before it begins its attack.
        /// </summary>
        [HideInInspector]
        public float PreDropStartingHeight;


        /// <summary>
        /// The amount of time that has elapsed since the drill last dropped down.
        /// </summary>
        private float _timeSinceLastMovement = 0;

        /// <summary>
        /// The distance the drill has currently travelled.
        /// </summary>
        private float _currentTravelledDistance = 0;


        protected override void Start()
        {
            base.Start();
            CreateScreenTraversingDrill();

            /*
            _timeSinceLastDrop = StartingTime;

            if (MoveOnYAxis)
            {
                PreDropStartingHeight = transform.position.x;
            }
            else
            {
                PreDropStartingHeight = transform.position.y;
            }

            //Setup the state machine.
            NPC drill = this;
            RobotDrill castedDrill = this;
            Animator anim = GetComponentInChildren<Animator>();

            //Create states.
            State patrol = new State("Patrol");
            State drop = new State("Drop");
            State raise = new State("Raise");

            //Create and add the state machine.
            NPCStateMachine stateMachine = new NPCStateMachine(patrol, this);
            StateMachineComponent stateMachineComponent = gameObject.AddComponent<StateMachineComponent>();
            stateMachineComponent.StateMachine = stateMachine;

            //Create actions.
            StateMachineAction setWaypoint = null;
            StateMachineAction moveTowards = null;
            if (GlobalWaypoints.Length != 0)
            {
                setWaypoint = new SetWaypoint(ref drill);
                moveTowards = new MoveTowardsOnOneAxis(ref drill, 0.1f, MoveOnYAxis);
            }
            StateMachineAction drillDown = new DrillDown(ref castedDrill, ref anim);
            StateMachineAction raiseDrill = new RaiseDrill(ref castedDrill);
            StateMachineAction addTime = null;
            StateMachineAction clearTime = null;
            if (!MemberOfNPCGroup)
            {
                unsafe
                {
                    fixed (float* time = &_timeSinceLastDrop)
                    {
                        addTime = new AddTime(time);
                        clearTime = new ClearTime(time);
                    }
                }
            }

            //Create conditions.
            Condition hitGround = new CheckHitGround(ref castedDrill);
            Condition atBaseHeight = new NotCondition(hitGround);
            Condition timeExceeded = null;
            if (!MemberOfNPCGroup)
            {
                unsafe
                {
                    fixed (float* time = &_timeSinceLastDrop)
                    {
                        timeExceeded = new TimeExceeded(time, TimeBetweenDrops);
                    }
                }
            }
            else
            {
                TimedNPCGroup group = GetComponentInParent<TimedNPCGroup>();
                unsafe
                {
                    fixed (float* time = &group.CurrentTime)
                    {
                        timeExceeded = new TimeExceeded(time, group.LoopTime);
                    }
                }
            }

            //Create transitions.
            Transition patrolToDrop = new Transition(drop, timeExceeded);
            Transition dropToRaise = new Transition(raise, hitGround);
            Transition raiseToPatrol = new Transition(patrol, atBaseHeight);

            //Add actions to states.
            if (GlobalWaypoints.Length != 0)
            {
                patrol.AddStateAction(setWaypoint);
                patrol.AddStateAction(moveTowards);
            }
            if (!MemberOfNPCGroup)
            {
                patrol.AddStateAction(addTime);
                patrol.AddExitAction(clearTime);
            }

            drop.AddStateAction(drillDown);

            raise.AddStateAction(raiseDrill);

            //Add transitions to states.
            patrol.AddTransition(patrolToDrop);
            drop.AddTransition(dropToRaise);
            raise.AddTransition(raiseToPatrol);

            //Add remaining states to state machine.
            stateMachine.AddState(drop);
            stateMachine.AddState(raise);
            */
        }

        private void CreateScreenTraversingDrill()
        {
            GravityScale = 0;
            MoveCharacterInLocalSpace = true;
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);

            if (StartingPosition == OFFSCREEN_POSITION)
            {
                StartingPosition = transform.position;
            }

            State wait = new State("Wait");
            State move = new State("Move");

            _behaviourStateMachine = new StateMachine(wait, activated);

            StateMachineAction waitForAttack = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                _timeSinceLastMovement += Time.deltaTime;
                return true;
            }));

            StateMachineAction setPositionAndVelocity = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                transform.position = StartingPosition;
                SetVelocity(BaseMoveSpeed * StaticTools.Vector2FromAngle(TravelAngle), OBJECT_VELOCITY_COMPONENT);
                return true;
            }));

            StateMachineAction addDistance = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                _currentTravelledDistance += Vector2.Distance(transform.position, Velocity * Time.fixedDeltaTime + (Vector2)transform.position);
                return true;
            }));

            StateMachineAction moveDrillOffscreen = new FunctionPointerAction(new System.Func<bool>(() =>
            {
                if (DestroyAfterTravellingDesiredDistance)
                {
                    Destroy(gameObject);
                    DrillDestroyed?.Invoke(this);
                }
                else
                {
                    transform.position = OFFSCREEN_POSITION;
                }

                return true;
            }));


            Condition timeForAttack = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return _timeSinceLastMovement >= TimeBetweenAttacks;
            }));

            Condition desiredDistanceTraversed = new FunctionPointerCondition(new System.Func<bool>(() =>
            {
                return _currentTravelledDistance >= TravelDistance;
            }));


            Transition waitToMove = new Transition(move, timeForAttack);
            Transition moveToWait = new Transition(wait, desiredDistanceTraversed);


            wait.AddStateAction(waitForAttack);
            wait.AddExitAction(setPositionAndVelocity);
            wait.AddTransition(waitToMove);

            move.AddStateAction(addDistance);
            move.AddExitAction(moveDrillOffscreen);
            move.AddTransition(moveToWait);
        }
    }
}
