﻿using UnityEngine;

namespace Characters.Enemies.AnimationControllers
{
    public class TurretAnimationController : MonoBehaviour, INPCAnimationController
    {
        /// <summary>
        /// Reference to the turret this animation controller handles.
        /// </summary>
        private Turret _turret;

        /// <summary>
        /// Reference to the turret's animator component.
        /// </summary>
        private Animator _anim;


        //Use this for initialization
        private void Awake()
        {
            _turret = GetComponent<Turret>();
            _anim = GetComponentInChildren<Animator>();
        }


        public void SetIdleAnimation()
        {
            _anim.SetBool("Walk", false);
            _anim.SetBool("Backpedal", false);
            _anim.SetBool("Shoot", false);
            _anim.SetBool("Stun", false);
        }

        public void SetWalkAnimation()
        {
            _anim.SetBool("Walk", true);
            _anim.SetBool("Backpedal", false);
            _anim.SetBool("Shoot", false);
            _anim.SetBool("Stun", false);
        }

        public void SetAlertAnimation()
        {
            _anim.SetBool("Walk", false);
            _anim.SetBool("Backpedal", true);
            _anim.SetBool("Shoot", false);
            _anim.SetBool("Stun", false);
        }

        public void SetShootAnimation()
        {
            _anim.SetBool("Walk", false);
            _anim.SetBool("Backpedal", false);
            _anim.SetBool("Shoot", true);
            _anim.SetBool("Stun", false);
        }

        public void SetStunnedAnimation(bool stunned)
        {
            _anim.SetBool("Walk", false);
            _anim.SetBool("Backpedal", false);
            _anim.SetBool("Shoot", false);
            _anim.SetBool("Stun", stunned);
        }

        #region Unused Animations
        public void SetStopShootingAnimation()
        {
            //Doesn't need this animation.
        }

        public void SetDeathAnimation()
        {
            //Doesn't need this animation.
        }
        #endregion
    }
}
