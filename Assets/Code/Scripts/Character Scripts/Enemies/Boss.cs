﻿using UnityEngine;
using System.Collections;
using HUD;

namespace Characters.Enemies.Bosses
{
    public class Boss : Enemy
    {
        public float HealthPerentage { get { return Health / MaxHealth; } }
        protected override void Start()
        {

             base.Start();
            ScreenOverlays.Instance.enemayHealthBar.Display = true;
            ScreenOverlays.Instance.enemayHealthBar.Amount = HealthPerentage;
            BossRoomManager bossmanger = transform.root.GetComponentInChildren<BossRoomManager>();
            if (bossmanger)
            {
                bossmanger.BossRef = this;
            }
        }
        protected override void Update()
        {
            if (HealthPerentage > 0)
            {
                ScreenOverlays.Instance.enemayHealthBar.Amount = HealthPerentage;
            }
            else
            {
                ScreenOverlays.Instance.enemayHealthBar.Display = false;
            }
            if (GlobalData.Player.Data.Health <= 0)
            {
                ScreenOverlays.Instance.enemayHealthBar.Display = false;
            }
            base.Update();
        }
    }
}
