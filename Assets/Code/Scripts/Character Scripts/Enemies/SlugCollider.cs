﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalData;
using Characters.Enemies;

public class SlugCollider : EnemyCollider
{
    private enum ColliderParts { FrontOpen, FrontClosed, Back }

    /// <summary>
    /// Which part of the collider is this one?
    /// </summary>
    [SerializeField]
    private ColliderParts _part;


    protected override void Start()
    {
        base.Start();
    }


    public override void TakeDamage(IDamageGiver giver, float amount, DamageType type, Vector3 hitPoint)
    {
        if (_part == ColliderParts.Back || _part == ColliderParts.FrontOpen)
        {
            Enemy.TakeDamage(giver, amount, type, hitPoint);
        }
        else
        {
            Enemy.TakeDamage(giver, 0, type, hitPoint);
        }
    }
}
