﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Characters.Enemies
{
    public class Sniper : Enemy
    {
        /// <summary>
        /// The Line Rendere
        /// </summary>
        public LineRenderer lineRenderer;

        //How far the Sniper will look 
        public float LookDistance = 10;

        /// <summary>
        /// The point that projectiles will be spawned at
        /// </summary>
        public GameObject ProjectileSpawnPosition;

        /// <summary>
        /// The projectile the NPC will fire
        /// </summary>
        public Projectile Projectile;

        /// <summary>
        /// The amount of time between each shoot while shooting
        /// </summary>
        public float TimeBetweenShots = 0.27f;


        /// <summary>
        /// How fast the projectile will move 
        /// </summary>
        public int ProjectialSpeed = 25;

        /// <summary>
        /// Defult State in theSM
        /// </summary>
        private State _defult;

        /// <summary>
        /// Has the player been found
        /// </summary>
        private bool _foundPlayer = false;
        /// <summary>
        /// The current Color of the beem
        /// </summary>
        private Color32 _beemColor = Color.yellow;
        /// <summary>
        /// How long the beem takes to focuses before a shoot is taken
        /// </summary>
        public float TimeTillShoot = 1;

        /// <summary>
        /// The Active areas to be passed in by the spanner
        /// </summary>
        /// 
        public SniperActiveZones RobotSniperActiveZones;

        /// <summary>
        /// The Main IK Control for aiming 
        /// </summary>
        public GameObject AminIkControlls;

        /// <summary>
        /// The Amount of Damage done by the Projectiles
        /// </summary>
        public float ProjectileDamageAmount;

        /// <summary>
        /// The pos the Ik Is reset to when the player is "lost"
        /// </summary>
        public GameObject AimDefaultIk;
        /// <summary>
        /// A refrence to the sprite mesh of the main eyes
        /// </summary>
        public GameObject MainEyes;
        /// <summary>
        /// A refrenace to the eyes to be uses when lostr
        /// </summary>
        public GameObject LoostEyes;

        /// <summary>
        /// The Curent Active Zone in use 
        /// </summary>
        private SniperActiveZone _activeZone;
        /// <summary>
        /// How long has the Sniper had a Clear shoot
        /// </summary>
        private float _TimeAmingATplayer = 0;
        /// <summary>
        /// Prefab Pool uses by the Projectiles 
        /// </summary>
        private PrefabPool _pool;
        /// <summary>
        /// How long Sine The last shot from the Gun 
        /// </summary>
        private float _timeSinceLastShoot = 0;

        /// <summary>
        /// How time is scaled to Calc the time added to the Aim time IE while the player moves Time is Scaled down
        /// </summary>
        private float AimTimeScale = 1;

        /// <summary>
        /// The Timer Var for the Off screen Time out 
        /// </summary>
        private float _offScreenTimeOut = 0;
        /// <summary>
        /// How much Time is left in the Looking for Player mode 
        /// </summary>
        private float _timeLeftInLastSeenTimer = 5;
        /// <summary>
        /// The Place the Player was when looking for player mode is entered 
        /// </summary>
        private Vector3 _playerPosAtTimeOut;

        /// <summary>
        /// the Point the aim is retured to 
        /// </summary>
        private Vector3 _aimResetStartingPoint;

        /// <summary>
        /// The Timer var used to reset the aim
        /// </summary>
        private float _restAimTimer = 0;
        /// <summary>
        /// The dirsction the Amin sheck is going
        /// </summary>
        private bool _reversShankeShootAnimation = false;
        /// <summary>
        /// How long it take to reset aim 
        /// </summary>
        private float _TimeToRestAim = 0.5f;
        /// <summary>
        /// The Time in the gun look aimations
        /// </summary>
        private float _GunlookAnimationTime = 0;
        /// <summary>
        /// IS the gun look animation running in revers 
        /// </summary>
        private bool _isRevesingGunAnima = false;

        protected override void Awake()
        {
            base.Awake();
            if (RobotSniperActiveZones)
            {
                _activeZone = RobotSniperActiveZones.zones[0];
                transform.position = _activeZone.transform.position;
            }
        }

        protected override void Start()
        {
            base.Start();
            _activeZone = RobotSniperActiveZones.zones[0];
            transform.position = _activeZone.transform.position;
            _pool = gameObject.AddComponent<PrefabPool>();
            _pool.PoolDebugName = "Robot Bat NPC";
            _pool.PoolPrefab = Projectile.gameObject;
            /// _DefultAimPos = AminIkControlls.transform.localPosition;

            //set up the state  of the eyes
            LoostEyes.SetActive(false);
            MainEyes.SetActive(true);

            _defult = new State("Default");
            State SeesPlayer = new State("Sees Player");
            State LostPlayer = new State("lost Player");
            State FoundLostPlayer = new State("Found Lost Player");
            State RestState = new State("Reset state ");
            State OffscreenMove = new State("Moving");

            ////This state look to see what zone is the closet zone and perch point 
            StateMachineAction ChoseNext = new FunctionPointerAction(() =>
            {
                SniperActiveZone nextZone = RobotSniperActiveZones.GetClosestToPlayer();
                if (_activeZone != nextZone)
                {
                    _activeZone = nextZone;
                    if (!StaticTools.ThresholdApproximately(transform.position, _activeZone.ClosestPerchPointToPoint(GlobalData.Player.transform.position), 1f))//TODO hoist calc
                {
                        transform.position = _activeZone.ClosestPerchPointToPoint(GlobalData.Player.transform.position);
                    //reset the Gun pos
                    AminIkControlls.transform.position = AimDefaultIk.transform.position;
                    }
                }
                else if (!StaticTools.ThresholdApproximately(transform.position, _activeZone.ClosestPerchPointToPoint(GlobalData.Player.transform.position), 1f))//TODO hoist calc
            {
                    transform.position = _activeZone.ClosestPerchPointToPoint(GlobalData.Player.transform.position);
                    AminIkControlls.transform.position = AimDefaultIk.transform.position;
                }
                return true;
            });
            //Move the AI to the next peach point if it is offscreen
            StateMachineAction MoveToNextPearch = new FunctionPointerAction(() =>
            {
            //Do not Move if the Player can see me
            Vector3 point = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().WorldToViewportPoint(transform.position);
                if ((point.x >= 0 && point.x <= 1) && (point.y >= 0 && point.y <= 1))// only droop if the player can see me
            {
                    return true;
                }
                Vector3 bestPos = _activeZone.ClosestPerchPointToPoint(GlobalData.Player.transform.position);
                if (!StaticTools.ThresholdApproximately(transform.position, bestPos, 1))// IF THE Preach point is not close IE it was not all ready there
            {
                    transform.position = bestPos;
                //reset the Gun pos
                AminIkControlls.transform.position = AimDefaultIk.transform.position;
                }
                return true;
            });
            ///set the Time to Fire scale based on movement 
            StateMachineAction CalcTimeScale = new FunctionPointerAction(() =>
            {
                if (_foundPlayer)
                {
                    Vector3 point = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().WorldToViewportPoint(transform.position);
                    if ((point.x >= 0 && point.x <= 1) && (point.y >= 0 && point.y <= 1))// only droop if the player can see me
                {
                        AimTimeScale = 1;
                        return true;
                    }
                    if (GlobalData.Player.Velocity.x > 0 || GlobalData.Player.Velocity.x < 0)
                    {
                    // The pretage of the Max spped of the player or 0.3 
                    AimTimeScale = Mathf.Max(0.5f, 1 - Mathf.Abs(GlobalData.Player.Velocity.x) / GlobalData.Player.MoveSpeed);

                    }
                    else
                    {
                        AimTimeScale = 1;
                    }
                }
                return true;
            });

            StateMachineAction GetTheShoot = new FunctionPointerAction(() =>
            {
                if (_foundPlayer)
                {
                    _beemColor = new Color32(255, (byte)((1 - _TimeAmingATplayer / TimeTillShoot) * 255), 0, 255);
                    lineRenderer.startColor = _beemColor;
                    lineRenderer.endColor = _beemColor;
                    lineRenderer.widthMultiplier = 0.2f - 0.1f * _TimeAmingATplayer / TimeTillShoot;
                    if (_TimeAmingATplayer >= TimeTillShoot)
                    {
                        Shoot();
                        ReSetShootVars();
                        _TimeAmingATplayer = 0;
                    }
                }
                return true;

            });

            StateMachineAction ResetTheShoot = new FunctionPointerAction(() =>
            {
                ReSetShootVars();
                return true;

            });
            /// Start the timer for looking for a plyer 
            StateMachineAction SetUpLostPlayerTimeOut = new FunctionPointerAction(() =>
            {
                LoostEyes.SetActive(true);
                MainEyes.SetActive(false);
                _timeLeftInLastSeenTimer = 5;
                _GunlookAnimationTime = 0;
                _playerPosAtTimeOut = GlobalData.Player.transform.position;
                return true;
            });
            //set up the timer from offscreen movment 
            StateMachineAction SetUpOffScreenMoveTimeOut = new FunctionPointerAction(() =>
            {
                _offScreenTimeOut = 1;
                return true;
            });
            ///update the off screen timer
            StateMachineAction RunOffSCreenMoveTimeOut = new FunctionPointerAction(() =>
            {
                _offScreenTimeOut -= Time.deltaTime;
                return true;
            });
            // Limits the Offscreen movemnt 
            Condition OffSCreenMoveTimedOut = new FunctionPointerCondition(() =>
            {

                return _offScreenTimeOut <= 0;
            });
            //old
            StateMachineAction RunLostPlayerTimeOut = new FunctionPointerAction(() =>
            {
                _timeLeftInLastSeenTimer -= Time.deltaTime;
                return true;
            });
            ///old
            Condition TimeedOurOrMoved = new FunctionPointerCondition(() =>
            {

                return (Vector3.Distance(_playerPosAtTimeOut, GlobalData.Player.transform.position) > 5 || _timeLeftInLastSeenTimer <= 0);
            });
            ///The player was found (the gun can aim at them)
            Condition FoundPlayer = new FunctionPointerCondition(() =>
            {

                return _foundPlayer;
            });

            ///Uses the NPC pos 
            Condition CanSeePlayer = new FunctionPointerCondition(() =>
            {

                RaycastHit2D hit = Physics2D.Raycast(transform.position, GlobalData.Player.transform.position - ProjectileSpawnPosition.transform.position, LookDistance, GlobalData.PLAYER_LAYER_SHIFTED | GlobalData.OBSTACLE_LAYER_SHIFTED);

                if (hit)
                {
                    if (hit.collider.gameObject.gameObject.layer == GlobalData.PLAYER_LAYER && _activeZone.IsPlayerInZone)
                    {
                        Debug.DrawRay(ProjectileSpawnPosition.transform.position, GlobalData.Player.transform.position - ProjectileSpawnPosition.transform.position, Color.green);
                        return true;
                    }
                }
                return false;
            });
            // is the player in the Same Zone as Ai
            Condition IsInZone = new FunctionPointerCondition(() =>
            {

                return !_activeZone.IsPlayerInZone;
            });
            ///Is the player lost (Gun cant aim at them)
            Condition NotFoundPlayer = new FunctionPointerCondition(() =>
            {

                return !_foundPlayer;
            });
            /// is the curent rest done
            Condition DoneWithAimReset = new FunctionPointerCondition(() =>
            {

                return _restAimTimer >= _TimeToRestAim;
            });
            //set up the aiming rest animation and the eyes
            StateMachineAction StartReturnToRestingAim = new FunctionPointerAction(() =>
            {
                LoostEyes.SetActive(true);
                MainEyes.SetActive(false);
                _aimResetStartingPoint = AminIkControlls.transform.position;
                _restAimTimer = 0;

                return true;
            });
            //Run the resting of the aimer to the defult pos 
            StateMachineAction GoToRestingAim = new FunctionPointerAction(() =>
            {

                if (_restAimTimer <= _TimeToRestAim)// if we are not 100% yet 
            {
                    AminIkControlls.transform.position = Vector3.Lerp(_aimResetStartingPoint, AimDefaultIk.transform.position, _restAimTimer / _TimeToRestAim);

                    _restAimTimer += Time.deltaTime;
                }

                return true;
            });
            //Face the player the Ai should not Face the Player if it done not see them
            StateMachineAction FacePlayerAction = new FunctionPointerAction(() =>
            {
                FacePlayer();

                return true;
            });
            /// set up the Found Player state 
            StateMachineAction StartReturnFromRestingAim = new FunctionPointerAction(() =>
            {
                LoostEyes.SetActive(false);
                MainEyes.SetActive(true);
                FacePlayer();
                _aimResetStartingPoint = GlobalData.Player.transform.position;
                _restAimTimer = 0;

                return true;
            });
            ///Run the Aimer return from resting pos
            StateMachineAction GoFromRestingAim = new FunctionPointerAction(() =>
            {
                _aimResetStartingPoint = GlobalData.Player.transform.position;
                AminIkControlls.transform.position = Vector3.Lerp(AimDefaultIk.transform.position, _aimResetStartingPoint, _restAimTimer / _TimeToRestAim);
                _restAimTimer += Time.deltaTime;

                return true;
            });
            StateMachineAction SetAnimationStateToSeeingPlayer = new FunctionPointerAction(() =>
            {
                animator.SetBool("HasLostPlayer", false);
                return true;
            });
            ///OLD was used to set the sate of the animation but was cauzing isses with IK 
            StateMachineAction SetAnimationStateToNotSeeingPlayer = new FunctionPointerAction(() =>
            {
                if (_restAimTimer >= _TimeToRestAim && !animator.GetBool("HasLostPlayer"))
                {
                    animator.SetBool("HasLostPlayer", true);
                }
                return true;
            });
            ///Moves the Gun IK up and Down to have AI "Look for the player"
            StateMachineAction MoveGunWhileLookForPlayer = new FunctionPointerAction(() =>
            {
                if (_restAimTimer >= _TimeToRestAim)// the rest is done 
            {

                    AminIkControlls.transform.position = Vector3.Lerp(AimDefaultIk.transform.position, AimDefaultIk.transform.position + Vector3.down, _GunlookAnimationTime / 3);

                    if (_isRevesingGunAnima)
                    {
                        _GunlookAnimationTime -= Time.deltaTime;
                        if (_GunlookAnimationTime <= 0)
                        {
                            _isRevesingGunAnima = !_isRevesingGunAnima;
                        }
                    }
                    else
                    {
                        _GunlookAnimationTime += Time.deltaTime;
                        if (_GunlookAnimationTime >= 3)
                        {
                            _isRevesingGunAnima = !_isRevesingGunAnima;
                        }
                    }
                }

                return true;
            });
            ///Aim the Gun Ik and The line render at the player and set the var _foundPlayer
            StateMachineAction AimAtPlayer = new FunctionPointerAction(() =>
            {
                FacePlayer();

                lineRenderer.SetPosition(0, ProjectileSpawnPosition.transform.position);
                RaycastHit2D hit = Physics2D.Raycast(ProjectileSpawnPosition.transform.position, GlobalData.Player.transform.position - ProjectileSpawnPosition.transform.position, LookDistance, GlobalData.PLAYER_LAYER_SHIFTED | GlobalData.OBSTACLE_LAYER_SHIFTED);



                Debug.DrawRay(ProjectileSpawnPosition.transform.position, GlobalData.Player.transform.position - ProjectileSpawnPosition.transform.position, Color.green);
                if (!hit)
                {
                    lineRenderer.enabled = false;
                    _foundPlayer = false;
                }
                else
                {
                    lineRenderer.enabled = true;
                    if (hit.collider.gameObject.gameObject.layer == GlobalData.PLAYER_LAYER && _activeZone.IsPlayerInZone)
                    {
                        lineRenderer.startColor = _beemColor;
                        lineRenderer.endColor = _beemColor;
                        _TimeAmingATplayer += Time.deltaTime * AimTimeScale;
                        if (!_foundPlayer)
                        {
                            _beemColor = Color.yellow;
                        // Debug.Log("Reset Beam color");
                    }
                        _foundPlayer = true;
                        AminIkControlls.transform.position = GlobalData.Player.transform.position;

                        if (_TimeAmingATplayer / TimeTillShoot < 0.8)
                        {
                            if (_reversShankeShootAnimation)
                            {
                                lineRenderer.SetPosition(1, hit.point + new Vector2(0, -0.03f));
                                _reversShankeShootAnimation = false;
                            }
                            else
                            {
                                lineRenderer.SetPosition(1, hit.point + new Vector2(0, 0.03f));
                                _reversShankeShootAnimation = true;
                            }
                        }

                        else
                        {
                            lineRenderer.SetPosition(1, hit.point);

                        }

                    }
                    else
                    {
                        lineRenderer.enabled = false;
                        _foundPlayer = false;
                        lineRenderer.SetPosition(1, hit.point + new Vector2(0, Random.Range(-0.05f, 0.05f)));
                    }


                }

                return true;
            });

            _behaviourStateMachine = new StateMachine(_defult, true);
            ///The main state the ai will make sure it has the best position and is facing the player
            ///Till it can see the player(The main trasfrom has clear sight to the player and the player is in the active zone)
            ///
            _defult.AddEntryAction(ResetTheShoot);
            _defult.AddStateAction(FacePlayerAction);
            _defult.AddStateAction(MoveToNextPearch);
            _defult.AddTransition(new Transition(SeesPlayer, CanSeePlayer));
            _defult.AddTransition(new Transition(OffscreenMove, IsInZone));

            ///The ai know where the player is
            ///SeesPlayer.AddEntryAction(SetAnimationStateToSeeingPlayer);
            SeesPlayer.AddEntryAction(AimAtPlayer);
            SeesPlayer.AddStateAction(AimAtPlayer);
            SeesPlayer.AddStateAction(GetTheShoot);
            SeesPlayer.AddStateAction(CalcTimeScale);
            SeesPlayer.AddTransition(new Transition(LostPlayer, NotFoundPlayer));

            //The AI Has Lost the Player 
            LostPlayer.AddEntryAction(SetUpLostPlayerTimeOut);
            LostPlayer.AddEntryAction(StartReturnToRestingAim);
            LostPlayer.AddStateAction(RunLostPlayerTimeOut);
            LostPlayer.AddStateAction(GoToRestingAim);
            LostPlayer.AddStateAction(MoveGunWhileLookForPlayer);
            LostPlayer.AddStateAction(MoveToNextPearch);
            LostPlayer.AddTransition(new Transition(OffscreenMove, IsInZone));
            LostPlayer.AddTransition(new Transition(FoundLostPlayer, CanSeePlayer));

            //The player has been found after being lost
            FoundLostPlayer.AddEntryAction(StartReturnFromRestingAim);
            FoundLostPlayer.AddStateAction(GoFromRestingAim);
            FoundLostPlayer.AddTransition(new Transition(SeesPlayer, DoneWithAimReset));

            ///The Ai is off screen so it can move
            OffscreenMove.AddEntryAction(ChoseNext);
            OffscreenMove.AddEntryAction(SetUpOffScreenMoveTimeOut);
            OffscreenMove.AddStateAction(RunOffSCreenMoveTimeOut);
            OffscreenMove.AddTransition(new Transition(_defult, OffSCreenMoveTimedOut));


        }
        private void OnDrawGizmos()
        {

        }


        private void ReSetShootVars()
        {
            ///AminIkControlls.transform.position = ProjectileSpawnPosition.transform.position;
            lineRenderer.widthMultiplier = 0.2f;
            _beemColor = Color.yellow;
            _TimeAmingATplayer = 0;

        }

        private void FacePlayer()
        {
            float sign = (transform.position.x > StaticTools.GetPlayerRef().transform.position.x) ? 1.0f : -1.0f;
            if (Controller.Collisions.FaceDir == 1 && sign == -1)
            {
                SetInput(Vector2.left);

            }
            else if (Controller.Collisions.FaceDir == -1 && sign == 1)
            {
                SetInput(Vector2.right);
            }
        }


        public void Shoot()
        {

            ///Vector3 diff = (Vector3)GlobalData.Player.transform.position - ProjectileSpawnPosition.transform.position;
            ///

            /* To hit the player when they are moving, we need to estimate where they will be after the travel time of projectile
            * and fire a projectile there. So, we calculate how long a projectile would take to hit the player given their current
            * position, and use that as a basis to calculate how far they are going to move. */

            /* Normally, we would have to handle both the X and Y axis here, since the dragonfly will be above the player, but we should be able to turn
             * it into a one dimensional problem by treating our starting position as 0, and the target position as the distance to the player.
             * From there, we can use the same calculate time to collision equation, initialPositionXB - initialPositionXA / aXVelocity - bXVelocity,
             * where the distance to the player is positionB, and 0 is positionA. */
            float distanceToPlayer = Vector3.Distance(ProjectileSpawnPosition.transform.position, GlobalData.Player.transform.position);
            float timeToCollision = CalculateTimeToCollision(0, distanceToPlayer, ProjectialSpeed, GlobalData.Player.Velocity.x);

            /* With the time to collision, we extrapolate how far the player can move in this time with the equation d = vi * t + 0.5at^2.
             * Where vi is the player's current velocity, t is the calculated time, and acceleration is 0, which zeroes out the second half
             * of the equation for a final equation of... */
            float playerXDistance = GlobalData.Player.Velocity.x * timeToCollision;

            /* Add this distance to the player's current X position to find their position after the travel time.
             * Note that this assumes the player's Y position does not change during the time. This may or may not be a problem... */
            Vector2 playerEstimatedPosition = new Vector2(GlobalData.Player.transform.position.x + playerXDistance, GlobalData.Player.transform.position.y);


            Vector3 diff = (Vector3)playerEstimatedPosition - ProjectileSpawnPosition.transform.position;
            diff.Normalize();
            float sign = (ProjectileSpawnPosition.transform.position.y < StaticTools.GetPlayerRef().transform.position.y) ? 1.0f : -1.0f;
            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            Vector3 it = Quaternion.Euler(0f, 0f, rot_z).eulerAngles;
            Projectile pro;

            if (_timeSinceLastShoot > TimeBetweenShots)
            {
                _timeSinceLastShoot = 0;
                pro = _pool.Spawn().GetComponent<Projectile>();
                pro.transform.position = ProjectileSpawnPosition.transform.position;
                pro.Fire(it, ProjectileDamageAmount, gameObject);
                pro.ProjectileSpeed = ProjectialSpeed;

            }

        }
        protected override void Update()
        {
            SetSpriteDirection();
            base.Update();
            _timeSinceLastShoot += Time.deltaTime;
        }

        /// <summary>
        /// Calculates the amount of time until two objects will collider, given the position for both objects and their respective velocities.
        /// </summary>
        /// <param name="initialPositionXA">The initial position for the first object.</param>
        /// <param name="initialPositionXB">The initial position for the second object.</param>
        /// <param name="aXVelocity">The velocity for the first object.</param>
        /// <param name="bXVelocity">The velocity for the second object.</param>
        /// <returns>The time until the collision. A positive time indicates that the collision will occur after that much time. A negative time indicates the collision has already occured.
        ///          And a non-zero time indicates that a collision will never happen.</returns>
        private float CalculateTimeToCollision(float initialPositionXA, float initialPositionXB, float aXVelocity, float bXVelocity)
        {
            #region Collision Time Equation Explanation
            /* Calculate the position and time the two enemies will collide.
             * We only need to consider the X-axis in these calculations
             * Equation for finding an object position at a point in time - positionX(t) = initialPositionX + xVelocity * t
             * At the point of collision, the positions of both objects will be the same, so set positionX(t) for both objects
             * to be the same, and solve for t
             * initialPositionXA + aXVelocity * t = initialPositionXB + bXVelocity * t
             * => initialPositionXA + aXVelocity * t - bXVelocity * t = initialPositionXB + (bXVelocity * t) - (bXVelocity * t)
             * => initialPositionXA - initialPositionXA + aXVelocity * t - bXVelocity * t = initialPositionXB - initialPositionXA
             * => aXVelocity * t - bXVelocity * t = initialPositionXB - initialPositionXA
             * => t(aXVelocity - bXVelocity) = initialPositionXB - initialPositionXA
             * => t(aXVelocity - bXVelocity) / aXVelocity - bXVelocity = initialPositionXB - initialPositionXA / aXVelocity - bXVelocity
             * => t = initialPositionXB - initialPositionXA / aXVelocity - bXVelocity
             */
            #endregion

            return (initialPositionXB - initialPositionXA) / (aXVelocity - bXVelocity);
        }
    }
}
