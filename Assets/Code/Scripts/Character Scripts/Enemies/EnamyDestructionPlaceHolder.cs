﻿using UnityEngine;
using System.Collections;
using Characters.Enemies;
using UnityEditor;

public class EnamyDestructionPlaceHolder : MonoBehaviour
{
    public Enemy Enemy;
    private SpriteRenderer previewSprite;

    private void Start()
    {
        Destroy(gameObject);// remove on game start
    }
    /// <summary>
    ///  this is a bit of hack im using this to update the sate of the preview on selection
    /// </summary>
    private void OnDrawGizmosSelected()
    {
#if UNITY_EDITOR
        if (Enemy == null)
        {
            Enemy tempEnemy = transform.parent.GetComponent<Enemy>();
            if(tempEnemy)
            {
                Enemy = tempEnemy;
            }
            else
            {
                Debug.Log("Can load Enemy Reference ");
                return;//fail state noting to recover from
            }
        }
        if(previewSprite == null)
        {
            SpriteRenderer temp = GetComponent<SpriteRenderer>();
            if(temp)
            {
                previewSprite = temp;
            }
            else
            {
                previewSprite = gameObject.AddComponent<SpriteRenderer>();
            }
        }
        previewSprite.sprite = Enemy.EnemyDestructionEffectCollection.RefeanceSprite;
        Enemy.EnemyDestructionEffectCollection.SpawnScale = transform.localScale;
        EditorUtility.SetDirty(Enemy.EnemyDestructionEffectCollection);
#endif
    }
}
