﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Enemies
{
    public class Turtle : Enemy
    {
        private int _asHorizontalVelocity = Animator.StringToHash("Horizontal Velocity");

        protected override void Start()
        {
            base.Start();
            _stillUsingOldStateMachine = false;

            //Setup the state machine.
            PatrolState patrol = new PatrolState(this, BaseMoveSpeed, false);
            _behaviourStateMachine = new StateMachine(patrol, activated);
        }

        protected override void Update()
        {
            if (!frozen)
            {
                base.Update();
            }
            animator.SetInteger(_asHorizontalVelocity, Mathf.CeilToInt(Mathf.Abs(GetVelocityComponentVelocity().x))); //TODO: Find a better way to set and measure velocity.   
        }
    }
}
