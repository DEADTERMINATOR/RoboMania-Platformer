﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;
using static GlobalData;
using System.Linq;
namespace Characters.Enemies
{
    public class Enemy : NPC, IDamageReceiver
    {
        public event StateChange Stunned;

        #region Constants
        /// <summary>
        /// The total amount of time it takes for an enemy to get back to full velocity after taking a melee hit.
        /// </summary>
        protected const float MELEE_VELOCITY_REDUCTION_TIME = 1.0f;

        /// <summary>
        /// How far the ray that look if a player was hit will fire (if it works do not adjust)
        /// </summary>
        protected const float RAY_DISTANCE_TO_LOOK_FOR_PLAYER = 3;
        #endregion

        #region Public Fields + Properties
        /// <summary>
        /// Public getter/setter for whether the enemy is activated.
        /// </summary>
        public bool Activated { get { return activated; } set { activated = value; if (BehaviourStateMachine != null) BehaviourStateMachine.Active = value; if (value == true) OnActivationSetup(); } }

        /// <summary>
        /// The amount of knockback the enemy has affecting its movement.
        /// </summary>
        public Vector2 Knockback { get { return _knockback; } }

        /// <summary>
        /// How likely the enemy is to spawn a gun chip on death.
        /// </summary>
        [Range(0, 100)]
        public int GunChipSpawnChance;

        /// <summary>
        /// How likely the enemy is to spawn a shield on death.
        /// </summary>
        [Range(0, 100)]
        public int ShieldSpawnChance;

        /// <summary>
        /// The game object that represents the destruction sprite.
        /// </summary>
        public GameObject DestructionSprite;

        /// <summary>
        /// The game object that represents the destruction sprite.
        /// </summary>
        public GameObject DestructionSpritePlaceHolder;

        /// <summary>
        /// The minimum amount of scrap this enemy will provide on death.
        /// </summary>
        public int MinScrapValue { get { return _minScrapValue; } }

        /// <summary>
        /// The maximum amount of scarp this enemy can provide on death.
        /// </summary>
        public int MaxScrapValue { get { return _maxScrapValue; } }

        public Explodable Exploder { get; private set; }
        public EnemyDestructionEffectCollection EnemyDestructionEffectCollection;
        #endregion

        #region Protected Fields + Properties
        /// <summary>
        /// How far ahead the enemy will look for other enemies to ask to turn around.
        /// </summary> 
        [SerializeField]
        protected float _enemyLookDistance = 5;

        /// <summary>
        /// The amount of scrap a particular instance of an enemy will provide.
        /// </summary>
        protected int _scrapValue;

        protected bool _isAboutToHitNPC = false;

        /// <summary>
        /// List of who this frame as ask us to turn around 
        /// </summary>
        protected List<Enemy> _requestsFromOthersToTurnAround = new List<Enemy>();

        /// <summary>
        /// The last enemy to successfully request that this enemy turn around to prevent a collision.
        /// </summary>
        protected Enemy _lastToTurnMeAround = null;

        /// <summary>
        /// The destruction effect that will be played when the enemy dies.
        /// </summary>
        protected DestructionEffect _destruction;

        /// <summary>
        /// Whether the destruction effect has already been played and the enemy hasn't been destroyed yet.
        /// </summary>
        protected bool _destructionPlayed = false;

        /// <summary>
        /// A reference to the main collier
        /// </summary>
        protected Collider2D _collider;
        
        protected bool _stillUsingOldStateMachine = true;
        #endregion

        #region Private Properties + Variables
        /// <summary>
        /// The amount by which any knockback received by the enemy will be subtracted to lessen its impact.
        /// </summary>
        [SerializeField]
        private Vector2 _knockbackResistance;

        [SerializeField]
        private int _minScrapValue;

        [SerializeField]
        private int _maxScrapValue;

        private Vector2 _knockback;

        /// <summary>
        /// Whether this enemy can request that other enemies turn around. 
        /// </summary>
        [SerializeField]
        private bool _canRequestTurnAround = true;

        /// <summary>
        /// Whether this enemy can be requested to turn around. 
        /// </summary>
        [SerializeField]
        private bool _canBeRequestedToTurnAround = true;

        /// <summary>
        /// Used to spawn a gun chip on an enemy's death.
        /// </summary>
        private GameObject _gunChipSpawner;

        /// <summary>
        /// Used to spawn a gun chip on an enemy's death.
        /// </summary>
        private GameObject _shieldSpawner;
        #endregion

        #region Unity Methods
        protected override void Start()
        {
            base.Start();
            Health = MaxHealth;
            _collider = GetComponent<Collider2D>();
            _destruction = GetComponentInChildren<DestructionEffect>();
            if (DestructionSprite == null)// keep legecy NPC working 
            {
                DestructionSprite = transform.root.Find("Destruction Sprite")?.gameObject;
            }
           
            if (DestructionSprite != null)
            {
                Exploder = DestructionSprite.GetComponent<Explodable>();
                DestructionSprite.GetComponent<Renderer>().enabled = false;
            }

            if (_gunChipSpawner == null)
            {
                _gunChipSpawner = Resources.Load("Prefabs/Weapons/Chips/Gun Chip Spawner") as GameObject;
            }
            if (_shieldSpawner == null)
            {
                _shieldSpawner = Resources.Load("Prefabs/Shield/Shield Spawner") as GameObject;
            }

            GunChipTypePickupSpawner gunChipSpawnerComponent = _gunChipSpawner.GetComponent<GunChipTypePickupSpawner>();

            _scrapValue = Random.Range(MinScrapValue, MaxScrapValue + 1);

            SetSpriteDirection(Controller.Collisions.FaceDir == 1 ? true : false, true);
        }

        protected virtual void Update()
        {
            if (!frozen && !IsDead)
            {
                if (activated)
                {
                    if (_stillUsingOldStateMachine)
                    {
                        BehaviourStateMachine?.RunUpdate();
                    }
                    else
                    {
                        BehaviourStateMachine?.RunActionCompletionUpdate(true);
                    }
                }

                if (_canRequestTurnAround)
                {
                    EvaluateTurnAround();
                }

                Vector3 damageKnockbackThisFrame = _knockback * Time.fixedDeltaTime;

                //We'll just reuse the player's knockback dissipation values for the enemy as well. Maybe TODO separate them.
                _knockback.x -= damageKnockbackThisFrame.x * PlayerConstants.X_DAMAGE_KNOCKBACK_DISSIPATION_MULTIPLIER;
                _knockback.y -= damageKnockbackThisFrame.y * PlayerConstants.Y_DAMAGE_KNOCKBACK_DISSIPATION_MULTIPLIER;

                AddToVelocity(damageKnockbackThisFrame);
            }
        }

        protected override void FixedUpdate()
        {
            if (activated && !frozen)
            {
                base.FixedUpdate();
            }
        }

        private void LateUpdate()
        {
            _requestsFromOthersToTurnAround.Clear();
        }
        #endregion

        #region Damage + Status
        public override void TakeDamage(IDamageGiver giver, float amount, DamageType type, Vector3 hitPoint)
        {
            base.TakeDamage(giver, amount, type, hitPoint);
            Health -= amount;

            if (Health > 0)
            {
                if (!Timing.IsRunning(damageFlashHandle) && !IsDead)
                {
                    damageFlashHandle = Timing.RunCoroutine(ApplyDamageFlash(), "Enemy Damage Flash");
                }
            }
            else
            {
                IsDead = true;
                NotifyDead();
                OnDeath();
            }          
        }

        public virtual void OnDeath()
        {
            _knockback = Vector2.zero;
            soundManager?.PlayDeathSound();
            DoDeathExplotion();
        }

        public virtual void DoDeathExplotion()
        {
            if (IsDead && !_destructionPlayed)
            {
                int randomChance = Random.Range(1, 101);
                if (randomChance <= GunChipSpawnChance)
                {
                    Instantiate(_gunChipSpawner, transform.position + Vector3.left, Quaternion.identity);
                }
                else
                {
                    //try and spawn a sheild
                    randomChance = Random.Range(1, 101);
                    if (randomChance <= ShieldSpawnChance)
                        Instantiate(_shieldSpawner, transform.position + Vector3.right, Quaternion.identity);
                }

                _destructionPlayed = true;
                if (EnemyDestructionEffectCollection != null)
                {
                    Explodable exploder;
                    Exploder = Instantiate(EnemyDestructionEffectCollection.GetRandomDestruction(), DestructionSprite.transform);
                    Exploder.transform.localScale = EnemyDestructionEffectCollection.SpawnScale;

                }
   
                _destruction?.DestroyAndReplace(transform.localScale.x * CharacterSpriteCollection.transform.localScale.x, _scrapValue);

                if (_destruction == null)
                {
                    GameObject.Destroy(gameObject, 1);
                }
            }
        }
        /// <summary>
        /// Adds the calculated knockback to the character's velocity. This version of the knockback method is used when the knockback
        /// is caused by damage. This will likely be the only type of knockback for enemies.
        /// </summary>
        /// <param name="knockback">The direction and magnitude of the knockback.</param>
        public virtual void SetKnockback(Vector2 knockback)
        {
            if (Mathf.Abs(_knockback.x) - _knockbackResistance.x <= 0)
            {
                knockback.x = 0;
            }
            else
            {
                knockback.x -= _knockbackResistance.x;
            }

            if (Mathf.Abs(_knockback.y) - _knockbackResistance.y <= 0)
            {
                knockback.y = 0;
            }
            else
            {
                knockback.y -= _knockbackResistance.y;
            }

            ZeroOutAllVelocities();
        }
        #endregion

        #region NPC Overrides
        public override void SetVelocity(Vector2 velocity, int velocityComponent = OBJECT_VELOCITY_COMPONENT)
        {
            base.SetVelocity(velocity, velocityComponent);

            var npcVelocity = GetVelocityComponentVelocity();
            if (Velocity == Vector2.zero)
            {
                npcAnim?.SetIdleAnimation();
            }
            else
            {
                npcAnim?.SetWalkAnimation();
            }
        }

        public override void SetVelocityOnOneAxis(char axis, float axisVelocity, int velocityComponent = OBJECT_VELOCITY_COMPONENT)
        {
            base.SetVelocityOnOneAxis(axis, axisVelocity);

            var npcVelocity = GetVelocityComponentVelocity();
            if (npcVelocity == Vector2.zero)
            {
                npcAnim?.SetIdleAnimation();
            }
            else
            {
                npcAnim?.SetWalkAnimation();
            }
        }

        public override void ZeroOutVelocity(int velocityComponent = OBJECT_VELOCITY_COMPONENT)
        {
            base.ZeroOutVelocity(velocityComponent);
            npcAnim.SetIdleAnimation();
        }

        public override void ZeroOutVelocityOnOneAxis(char axis, int velocityComponent = 0)
        {
            base.ZeroOutVelocityOnOneAxis(axis, velocityComponent);

            var npcVelocity = GetVelocityComponentVelocity();
            if (npcVelocity == Vector2.zero)
            {
                npcAnim?.SetIdleAnimation();
            }
            else
            {
                npcAnim?.SetWalkAnimation();
            }
        }

        public override void Activate(GameObject activator)
        {
            base.Activate(activator);
            _behaviourStateMachine.Active = true;
        }

        public override void Deactivate(GameObject deactivator)
        {
            base.Deactivate(deactivator);
            _behaviourStateMachine.Active = false;
        }
        #endregion

        #region Turn Around Requests
        /// <summary>
        /// Evaluates whether the there is another enemy in the path of this enemy, and handles it if so.
        /// </summary>
        protected virtual void EvaluateTurnAround()
        {
            float Xdir = Mathf.Sign(Velocity.x);
            _isAboutToHitNPC = false;
            Vector3 origin = GetLookVectorOrigin();
            RaycastHit2D hit = Physics2D.Raycast(origin, Vector3.right * Xdir, _enemyLookDistance, GlobalData.ENEMY_LAYER_SHIFTED);
            if (hit)
            {
                Debug.DrawLine(origin, hit.point, Color.red);
                if (_isAboutToHitNPC != true)
                {
                    _isAboutToHitNPC = true;
                    OtherEnemyDetected(hit.collider.gameObject);
                }
            }
            else
            {
                Debug.DrawLine(origin, origin + (Vector3.right * (Xdir * _enemyLookDistance)), Color.green);
            }
        }

        /// <summary>
        /// Event handle called by the controller when a inanimate collision is detected 
        /// </summary>
        /// <param name="otherGameObject"></param>
        protected virtual void OtherEnemyDetected(GameObject otherGameObject)
        {
            Enemy otherEnemy = otherGameObject.GetComponentInParent<Enemy>();
            if (otherEnemy != null)// this is an eneny 
            {
                if ((otherEnemy.Velocity.x > 0 && Velocity.x < 0) || (otherEnemy.Velocity.x < 0 && Velocity.x > 0))// different dir 
                {
                    if (!_requestsFromOthersToTurnAround.Contains(otherEnemy))
                    {
                        if (_lastToTurnMeAround == otherEnemy)
                        {
                            _lastToTurnMeAround = null;
                        }
                        _requestsFromOthersToTurnAround.Add(otherEnemy);
                        otherEnemy.RequestTurnAround(this);
                    }
                }
                else
                {
                    float velocityDiff = Velocity.x - otherEnemy.Velocity.x;
                    if (velocityDiff > 0)// this enemy is faster 
                    {
                        SetSpriteDirection(Controller.Collisions.FaceDir != 1, true);
                    }
                }
            }
        }

        /// <summary>
        /// They enemy was requested to turn around if it can it will if not it will request the requester turn around if there is one 
        /// </summary>
        /// <param name="requester">the requesting Enemy or null</param>
        /// <returns></returns>
        public virtual void RequestTurnAround(Enemy requester)
        {
            if (_canBeRequestedToTurnAround && CanTurnAround(requester))
            {
                _requestsFromOthersToTurnAround.Add(requester);// make sure we do not re ask them to tuner aground this frame 
                SetSpriteDirection(Controller.Collisions.FaceDir != 1, true);
                _lastToTurnMeAround = requester;
            }
            else if (requester != null)
            {
                requester.RequestTurnAround(null);
                _lastToTurnMeAround = null;
            }
        }

        /// <summary>
        /// Can this NPC Turn around if requested 
        /// Default:IS this not the same npc to Last turn me around and am i colliding in the dir of travel
        /// Keep default with ((Expresstion) && base.CanTunAround())
        /// </summary>
        /// <returns></returns>
        public virtual bool CanTurnAround(Enemy requester)
        {
            return (_lastToTurnMeAround != requester || _lastToTurnMeAround == null) && !(controller.Collisions.Left.IsColliding && Controller.Collisions.FaceDir == -1) && !(controller.Collisions.Right.IsColliding && Controller.Collisions.FaceDir == 1);
        }

        protected bool IsPlayerInFront()
        {
            if (controller.Collisions.FaceDir == 1)//right
            {
                return (GlobalData.Player.transform.position.x - transform.position.x > 0); 
            }
            else
            {
                return (GlobalData.Player.transform.position.x - transform.position.x < 0);
            }
        }

        /// <summary>
        /// Get a point at the edge of the main collier that is on the side of movement in the vertical center 
        /// </summary>
        /// <returns></returns>
        protected virtual Vector3 GetLookVectorOrigin()
        {
            if (controller.Collisions.FaceDir == 1)//right
            {
                return new Vector3(_collider.bounds.max.x, _collider.bounds.center.y, 0);
            }
            else
            {
                return new Vector3(_collider.bounds.min.x, _collider.bounds.center.y, 0);
            }
        }
        #endregion
    }
}
