﻿using Anima2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using Characters.Enemies.AnimationControllers;
using static StateMachineAction;

namespace Characters.Enemies
{
    public class Cockroach : Enemy
    {
        /// <summary>
        /// The point that projectiles will be spawned at
        /// </summary>
        public GameObject ProjectileSpawnPosition;
        /// <summary>
        /// Amount of dmage projectile will do
        /// </summary>
        public float DamageAmount = 2;
        /// <summary>
        /// The prefab pool for protecctile
        /// </summary>
        [HideInInspector]
        public PrefabPool Pool;

        /// <summary>
        /// The pojectial the NPC will fire
        /// </summary>
        public Projectile Projectile;

        /// <summary>
        /// The amount of time between each shoot wilhe shooting
        /// </summary>
        public float TimeBetweenShots = 0.27f;

        /// <summary>
        /// The variance in amining lower spreed = more acuret
        /// </summary>
        public Vector2 SpreadAmount = new Vector2(-12, 20);
        /// <summary>
        /// How fast the projectile will move 
        /// </summary>
        public int ProjectialSpeed = 25;

        /// <summary>
        /// Should The NPC use Debug Setings as it was not spawned
        /// </summary>
        public bool UseNoSpawnerSettings = false;
        /// <summary>
        /// The light Thats turns on just befor shooting
        /// 
        /// </summary>
        public Light2D ShootLight;

        public bool CanBeOnRoof = false;
        // public GameObject ShootIndacator;

        //private SpriteMeshInstance _SFXRendreer;

        /// <summary>
        /// How long ago was the last shoot
        /// </summary>
        private float _timeSinceLastShoot = 0;

        /// <summary>
        /// Time till droop 
        /// </summary>
        private float _timeValue;

        /// <summary>
        /// The waypoint offset of a droop ie the sub waypoint system staert on elm 15 
        /// </summary>
        private int _droopIndexOffset;

        /// <summary>
        /// The rotation used while on the roof 
        /// </summary>
        private static Vector3 UpSideDownRot = new Vector3(0, 180, 180);
        /// <summary>
        /// How far off from the ray cat hist point to droop to 
        /// </summary>
        private static Vector2 DroopOffset = new Vector3(0, -0.45f, 0);
        /// <summary>
        /// How long dose the droop take
        /// </summary>
        private static float DROOPTIME = 0.3f;
        /// <summary>
        /// How long should the NPC Warn Of incoming shoot. Shoot state will sate active from 1.5 * the shoot time
        /// </summary>
        private static float ShootWarningTime = 1;


        /// <summary>
        ///  A point that is uses fro detecting thing in-front of the AI
        /// </summary>
        public Transform FrontDetectorPosition;
        /// <summary>
        /// A point that is uses fro detecting thing behind of the AI
        /// </summary>
        public Transform BackDetectorPosition;
        /// <summary>
        /// How many units the AI can see
        /// </summary>
        public float ViewDistance = 55;
        /// <summary>
        /// Get the current state and set the current state and rotation
        /// </summary>
        public bool UpSideDown
        {
            get
            {
                return _isUpsideDown;
            }
            set
            {
                _isUpsideDown = value;
            }
        }
        [SerializeField]
        private bool _isUpsideDown;
        /// <summary>
        /// The Traget x pos for movemnt 
        /// </summary>
        private float _tragetX = -1;
        /// <summary>
        /// Are we moving left or right
        /// </summary>
        private bool _movingLeft = true;

        private bool _moving = true;
        private Vector2 _droopPoint;
        /// <summary>
        /// Are we on screen
        /// </summary>
        private bool _isOnScreen = false;
        /// <summary>
        /// Point the droop will start at
        /// </summary>
        private Vector3 _droopStart;
        /// <summary>
        /// How much tile elapsed since the stat of the droop
        /// </summary>
        private float _droopElapsed;
        /// <summary>
        /// Has the AI shoot since entering the shoot state last
        /// </summary>
        private bool _didShoot = false;

        private float _biteStart;
        /// <summary>
        /// How close to the Ai will start a bite
        /// </summary>
        private static int BITEDISTANCE = 6;
        /// <summary>
        /// How long should a bit take at max distance this is scaled to the actual distance
        /// </summary>
        private static float BITETIME = 0.75f;
        /// <summary>
        /// The scaled bite time 
        /// </summary>
        private float _realBiteTime;
        /// <summary>
        /// How much time has elapsed in the state
        /// </summary>
        private float _elapsedBiteTime;

        /// <summary>
        /// Fore the AI to look at the player regardless of vision 
        /// </summary>
        private bool _overrideLookAtPlayer;
        /// <summary>
        /// Elapsed time on shoot state
        /// </summary>
        private float _elapsedTimeOnShoot = 0;

        private float _watedTime = 0;
        private int Updates;

        protected override void Awake()
        {
            base.Awake();
        }


        protected override void Start()
        {
            _stillUsingOldStateMachine = false;

            base.Start();
            //turn off gravity 
            GravityScale = 1;

            Pool = gameObject.AddComponent<PrefabPool>();
            Pool.PoolPrefab = Projectile.gameObject;

            // State OnRoof = new State("On Roof");
            //  State OnGround = new State("On Floor");
            State Droop = new State("Do Droop");
            State UnDroop = new State("UnDroop");

            State TryAndShootGround = new State("Shoot Gound");
            State TryAndShootRoof = new State("Shoot Roof");
            State WaitAftetShoot = new State("wait");

            State TryAndBiteOnGround = new State("Bite");

            State Start = new State("Start");
            PatrolState OnGround = new PatrolState(this, BaseMoveSpeed, false);
            PatrolState OnRoof = new PatrolState(this, BaseMoveSpeed, false);


            StateMachineAction WaitStart = new FunctionPointerAction(() =>
            {
                _watedTime = 0;
                return ActionCompletionState.Successful;
            });


            StateMachineAction Wait = new FunctionPointerAction(() =>
            {
                _watedTime += Time.deltaTime;
                return ActionCompletionState.Successful;
            });

            StateMachineAction DoShoot = new FunctionPointerAction(() =>
            {

                if (_timeSinceLastShoot > ShootWarningTime)// Can not be see by the player
                {
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, GlobalData.Player.transform.position - transform.position, ViewDistance, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.PLAYER_LAYER_SHIFTED);
                    if (!hit)
                    {

                        return ActionCompletionState.Successful;
                    }
                    else if (hit.collider.gameObject.layer == GlobalData.PLAYER_LAYER)
                    {
                        //Debug.DrawLine(transform.position, hit.point, Color.green, 5);
                        ShootProjectile();
                        _didShoot = true;

                    }
                    //Debug.DrawLine(transform.position, hit.point, Color.gray, 5);
                }
                else if (!_didShoot) // IF not able to shoot 
                {

                    Indicator();
                }
                _elapsedTimeOnShoot += Time.deltaTime;
                SetVelocityOnOneAxis('Y', 0);
                return ActionCompletionState.Successful;
            });

            StateMachineAction DoneShoot = new FunctionPointerAction(() =>
            {
                _timeSinceLastShoot = 0;
                _overrideLookAtPlayer = false;
                ShootLight.intensity = 0.01f;
                GravityScale = 1;
                return ActionCompletionState.Successful;
            });

            StateMachineAction EnterShoot = new FunctionPointerAction(() =>
            {
                _didShoot = false;
                _elapsedTimeOnShoot = 0;
                _overrideLookAtPlayer = true;              
                _timeSinceLastShoot = 0;
                GravityScale = 0;
                return ActionCompletionState.Successful;
            });

            StateMachineAction KillVelocity = new FunctionPointerAction(() =>
            {

                SetInput(Vector2.zero);
                ZeroOutVelocity();
                return ActionCompletionState.Successful;
            });


            StateMachineAction FaceThePlayer = new FunctionPointerAction(FacePlayerActionCompletion);


            StateMachineAction EnterBite = new FunctionPointerAction(() =>
            {
                _biteStart = transform.position.x;
                _tragetX = GlobalData.PlayerTransfrom.position.x;
                _realBiteTime = BITETIME * Mathf.Abs(_tragetX - transform.position.x) / BITEDISTANCE;
                _elapsedBiteTime = 0;
                _overrideLookAtPlayer = true;
                GravityScale = 0;
                return ActionCompletionState.Successful;

            });

            StateMachineAction EndBite = new FunctionPointerAction(() =>
            {
                _overrideLookAtPlayer = false;
                GravityScale = 1;
                return ActionCompletionState.Successful;

            });



            StateMachineAction DoBite = new FunctionPointerAction(() =>
            {
                _elapsedBiteTime += Time.deltaTime;
                if (_elapsedBiteTime >= _realBiteTime)
                {
                    _elapsedBiteTime = _realBiteTime;
                }
                float eased = StaticTools.EaseInSine(0, 1, _elapsedBiteTime, _realBiteTime);
                SetVelocity(transform.position - (new Vector3(_biteStart - ((_biteStart - _tragetX) * eased), transform.position.y, 0)));
                return ActionCompletionState.Successful;
            });
            StateMachineAction StartDoDroop = new FunctionPointerAction(() =>
            {
                //Is a valid point and should be the same platform as player

                _droopStart = transform.position;
                _droopElapsed = 0;
                UpSideDown = false;
                return ActionCompletionState.Successful;
            });

            StateMachineAction DoDroop = new FunctionPointerAction(() =>
            {
                _droopElapsed += Time.deltaTime;
                if (_droopElapsed >= DROOPTIME)
                {
                    _droopElapsed = DROOPTIME;
                }
                float eased = StaticTools.EaseInSine(0.01f, 1, _droopElapsed, DROOPTIME);
                SetVelocity(transform.position - new Vector3(_droopPoint.x, _droopStart.y + ((_droopPoint.y - _droopStart.y) * eased), 0));
                return ActionCompletionState.Successful;
            });

            StateMachineAction ReturnToRoof = new FunctionPointerAction(() =>
            {
                if (_droopPoint != Vector2.zero)
                {
                    UpSideDown = true;
                    Controller.MoveToPositionWithOutCollisions(_droopPoint);
                }
                return ActionCompletionState.Successful;
            });


            StateMachineAction SetGravity = new FunctionPointerAction(() =>
            {
                GravityScale = 1;
                return ActionCompletionState.Successful;
            });
            StateMachineAction UnSetGravity = new FunctionPointerAction(() =>
            {
                GravityScale = 0;
                return ActionCompletionState.Successful;
            });

            Condition DoneBite = new FunctionPointerCondition(() =>
            {
                return (!CanMoveToPoint(new Vector3(_tragetX, transform.position.y, 0)) || !StaticTools.ThresholdApproximately(transform.position.y, GlobalData.Player.transform.position.y, 1) || _elapsedBiteTime == _realBiteTime);
            });


            Condition CanBite = new FunctionPointerCondition(() =>
            {

                return (Mathf.Abs(transform.position.x - GlobalData.PlayerTransfrom.position.x) <= BITEDISTANCE && GlobalData.Player.Controller.Collisions.IsCollidingBelow && StaticTools.ThresholdApproximately(transform.position.y, GlobalData.PlayerTransfrom.position.y, 0.4f) && IsBitePathClear());
            });

            Condition DoneDroop = new FunctionPointerCondition(() =>
            {
                return (_droopElapsed >= DROOPTIME);
            });

            Condition DoneShooting = new FunctionPointerCondition(() =>
            {
                return (_elapsedTimeOnShoot >= ShootWarningTime * 1.5f);
            });

            Condition IsAtDsiredX = new FunctionPointerCondition(() =>
            {
                if (_tragetX == -1)
                {
                    return false;
                }
                if (StaticTools.ThresholdApproximately(transform.position.x, _tragetX, 0.3f))
                {
                    return true;
                }
                return false;
            });

            Condition CanDoDroop = new FunctionPointerCondition(() =>
            {

                if (!_isOnScreen)
                {
                    return false;
                }
                float dist = Mathf.Abs(transform.position.x - GlobalData.Player.transform.position.x);
                if (dist > 2) // is the player is to close or to far 
                {
                    //Debug.LogWarning("Player to far away");
                    return false;
                }

                if (!_isOnScreen)// Can not be see by the player
                {
                    //Debug.LogWarning("Off Screen");
                    return false;
                }


                if (transform.position.y < GlobalData.Player.transform.position.y)// player is above the AI 
                {

                    return false;
                }


                return true;
            });


            Condition IsClearToDroop = new FunctionPointerCondition(() =>
            {
                Vector2 dir = (UpSideDown) ? Vector2.down : Vector2.up;
                Vector2 hit1;
                RaycastHit2D hit = Physics2D.Raycast(FrontDetectorPosition.position, dir, 10, GlobalData.OBSTACLE_LAYER_SHIFTED);
                if (hit)
                {
                    hit1 = hit.point;
                    hit = Physics2D.Raycast(BackDetectorPosition.position, dir, 10, GlobalData.OBSTACLE_LAYER_SHIFTED);
                    if (hit && StaticTools.ThresholdApproximately(hit.point.y, hit1.y, 0.1f))
                    {

                        _droopPoint = hit.point + (DroopOffset * dir.y);
                        if (UpSideDown)
                        {
                            return (GlobalData.Player.Controller.Collisions.IsCollidingBelow && StaticTools.ThresholdApproximately(_droopPoint.y, GlobalData.Player.transform.position.y, 1));// if comming from the roof make sure its the same platofrm the player is on 
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                _droopPoint = Vector2.zero;
                return false;
            });


            Condition IsOffScreen = new FunctionPointerCondition(() =>
            {
                if (_isOnScreen)// Can not be see by the player
                {
                    return false;
                }

                return true;
            });

            Condition isOnRoof = new FunctionPointerCondition(() =>
            {
                return UpSideDown;
            });

            Condition CAnBeOnRoof = new FunctionPointerCondition(() =>
            {
                return CanBeOnRoof;
            });

            Condition CanShoot = new FunctionPointerCondition(() =>
            {
                if(!Controller.Collisions.Below.IsColliding)
                {
                    return false;
                }
                if (_timeSinceLastShoot > TimeBetweenShots + ShootWarningTime)// Can not be see by the player
                {
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, GlobalData.Player.transform.position - transform.position, ViewDistance, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.PLAYER_LAYER_SHIFTED);
                    if (!hit)
                    {

                        return false;
                    }
                    else if (hit.collider.gameObject.layer == GlobalData.PLAYER_LAYER)
                    {
                        //Debug.DrawLine(transform.position, hit.point, Color.green, 5);
                        return true;

                    }
                    // Debug.DrawLine(transform.position,  hit.point, Color.gray, 5);
                }

                return false;
            });
            Condition shouldMove = new FunctionPointerCondition(() =>
            {
                if (_timeSinceLastShoot > TimeBetweenShots - ShootWarningTime)// Can not be see by the player
                {
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, GlobalData.Player.transform.position - transform.position, ViewDistance, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.PLAYER_LAYER_SHIFTED);
                    if (!hit)
                    {

                        return false;
                    }
                    else if (hit.collider.gameObject.layer == GlobalData.PLAYER_LAYER)
                    {
                        //Debug.DrawLine(transform.position, hit.point, Color.green, 5);
                        return true;

                    }
                    // Debug.DrawLine(transform.position,  hit.point, Color.gray, 5);
                }

                return false;
            });
            Condition DoneWaitTimeShoot = new FunctionPointerCondition(() =>
            {
                return _watedTime >= TimeBetweenShots;
            });
            Condition CanNotMove = new FunctionPointerCondition(() =>
            {
                float dist = Vector2.Distance(GlobalData.Player.transform.position, transform.position);
                return (IsPlayerInFront() && (dist < 6|| dist < 12 && !StaticTools.ThresholdApproximately(GlobalData.Player.transform.position.y, transform.position.y, 1)));
            });

            AndCondition CanNotMoveAfterShoot = new AndCondition(DoneShooting, CanNotMove);
            AndCondition CanDroop = new AndCondition(CanDoDroop, IsClearToDroop);
            AndCondition CanUnDroop = new AndCondition(IsOffScreen, IsClearToDroop, CAnBeOnRoof);

            OnGround.AddEntryAction(SetGravity);
            OnGround.AddExitAction(KillVelocity);
            OnGround.AddTransition(new Transition(UnDroop, CanUnDroop));
            OnGround.AddTransition(new Transition(TryAndBiteOnGround, CanBite));
           // OnGround.AddTransition(new Transition(TryAndShootGround, CanShoot)); //TODO:fix shoot bug


            OnRoof.AddEntryAction(UnSetGravity);
            OnRoof.AddTransition(new Transition(Droop, CanDroop));
          //  OnRoof.AddTransition(new Transition(TryAndShootRoof, CanShoot));

            Droop.AddEntryAction(StartDoDroop);
            Droop.AddStateAction(DoDroop);
            Droop.AddTransition(new Transition(OnGround, DoneDroop));

            TryAndShootGround.AddEntryAction(EnterShoot);
           // TryAndShootGround.AddStateAction(FaceThePlayer);
            TryAndShootGround.AddStateAction(DoShoot);
            TryAndShootGround.AddExitAction(DoneShoot);
            TryAndShootGround.AddTransition(new Transition(WaitAftetShoot, CanNotMoveAfterShoot));
            TryAndShootGround.AddTransition(new Transition(OnGround, DoneShooting));

            TryAndShootRoof.AddEntryAction(EnterShoot);
            TryAndShootRoof.AddStateAction(DoShoot);
            TryAndShootRoof.AddExitAction(DoneShoot);
            TryAndShootRoof.AddTransition(new Transition(Droop, CanDroop));
            TryAndShootRoof.AddTransition(new Transition(OnRoof, DoneShooting));

            TryAndBiteOnGround.AddEntryAction(EnterBite);
            TryAndBiteOnGround.AddStateAction(DoBite);
            TryAndBiteOnGround.AddExitAction(EndBite);
            TryAndBiteOnGround.AddTransition(new Transition(OnGround, DoneBite));

            UnDroop.AddEntryAction(ReturnToRoof);
            UnDroop.AddTransition(new Transition(OnRoof, new FunctionPointerCondition(() => { return true; })));

            WaitAftetShoot.AddEntryAction(WaitStart);
            WaitAftetShoot.AddStateAction(Wait);
            WaitAftetShoot.AddTransition(new Transition(TryAndShootGround, DoneWaitTimeShoot));


            Start.AddTransition(new Transition(OnRoof, isOnRoof));
            Start.AddTransition(new Transition(OnGround, new NotCondition(isOnRoof)));

            _behaviourStateMachine = new StateMachine(Start, activated);

            //Activated = true;
        }


        private bool CanMoveToPoint(Vector3 point)
        {
            Vector2 dir = (UpSideDown) ? Vector2.down : Vector2.up;
            Vector2 hit1;
            RaycastHit2D hit = Physics2D.Raycast(FrontDetectorPosition.localPosition + point, dir, 10, GlobalData.OBSTACLE_LAYER_SHIFTED);
            if (hit)
            {
                hit1 = hit.point;
                hit = Physics2D.Raycast(BackDetectorPosition.localPosition + point, dir, 10, GlobalData.OBSTACLE_LAYER_SHIFTED);
                if (hit && StaticTools.ThresholdApproximately(hit.point.y, hit1.y, 0.1f))
                {

                    _droopPoint = hit.point + (DroopOffset * dir.y);
                    if (UpSideDown)
                    {
                        return (StaticTools.ThresholdApproximately(_droopPoint.y, GlobalData.Player.transform.position.y, 1));// if comming from the roof make sure its the same platofrm the player is on 
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            _droopPoint = Vector2.zero;
            return false;
        }

        private void OnDrawGizmos()
        {

            //  GizmoHelper.drawString(StateMachine.DebugInfo(), transform.position);


        }

        public bool ShootProjectile()
        {
            Projectile pro;
            Vector3 diff = (GlobalData.Player.transform.position + Vector3.up) - ProjectileSpawnPosition.transform.position;
            diff.Normalize();
            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            Vector3 it = Quaternion.Euler(0f, 0f, rot_z).eulerAngles;
            float sign = (ProjectileSpawnPosition.transform.position.y < GlobalData.Player.transform.position.y) ? 1.0f : -1.0f;

            
            it.z += Random.Range(SpreadAmount.x, SpreadAmount.y);
            pro = Pool.Spawn().GetComponent<Projectile>();
            pro.transform.position = ProjectileSpawnPosition.transform.position;
            pro.ProjectilePrefab = Pool.PoolPrefab;
            pro.Fire(it, DamageAmount, gameObject);
            ShootLight.intensity = 0.01f;
            return true;
        }

        public bool FacePlayer()
        {
            float distance = transform.position.x - GlobalData.Player.transform.position.x;
            // face the player 
            if (Controller.Collisions.FaceDir == 1 && distance > 0)
            {
                SetSpriteDirection(false);
            }
            else if (Controller.Collisions.FaceDir == -1 && distance < 0)
            {
                SetSpriteDirection(true);
            }

            return true;
        }
        public ActionCompletionState FacePlayerActionCompletion()
        {
            float distance = transform.position.x - GlobalData.Player.transform.position.x;
            // face the player 
            if (Controller.Collisions.FaceDir == 1 && distance > 0)
            {
                SetSpriteDirection(false);
            }
            else if (Controller.Collisions.FaceDir == -1 && distance < 0)
            {
                SetSpriteDirection(true);
            }

            return ActionCompletionState.Successful;
        }
        public void Indicator()
        {
            float prectange = StaticTools.ExpoEaseIn(0.01f, 1, _elapsedTimeOnShoot, ShootWarningTime);
            ShootLight.intensity = 1 * prectange;
            _timeSinceLastShoot += Time.deltaTime;
        }


        private bool CanSeePlayer()
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, GlobalData.Player.transform.position + Vector3.up - transform.position, ViewDistance, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.PLAYER_LAYER_SHIFTED);
            if (!hit)
            {
                return false;

            }
            else if (hit.collider.gameObject.layer == GlobalData.PLAYER_LAYER)
            {
                return true;

            }

            return false;
        }

        private bool ShouldLookAtPlayer()
        {

            return (_overrideLookAtPlayer || CanSeePlayer());
        }

        private bool IsBitePathClear()
        {

            RaycastHit2D hit = Physics2D.Raycast(transform.position, GlobalData.Player.transform.position - transform.position, ViewDistance, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.PLAYER_LAYER_SHIFTED);
            if (!hit)
            {
                return false;
            }
            else if (hit.collider.gameObject.layer != GlobalData.PLAYER_LAYER)
            {
                return false;

            }
            Vector2 dir = (!UpSideDown) ? Vector2.down : Vector2.up;
            int diff = Mathf.RoundToInt(GlobalData.Player.transform.position.x - transform.position.x);
            int increment = 1;
            if (diff < 0)
            {
                increment = -1;
            }
            int c = 0;
            while (c != diff)
            {
                c += increment;
                Vector3 point = new Vector3(transform.position.x + c, transform.position.y, 0);


                hit = Physics2D.Raycast(point, dir, 2, GlobalData.OBSTACLE_LAYER_SHIFTED);

                if (!hit)
                {
                    return false;
                }
            }

            return true;
        }


        private void HandelRotation()
        {
            if (_isUpsideDown)
            {
                if (transform.eulerAngles != UpSideDownRot)
                {

                    transform.eulerAngles = UpSideDownRot;
                }
            }
            else
            {
                if (transform.eulerAngles != Vector3.zero)
                {
                    transform.eulerAngles = Vector3.zero;
                }
            }
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
          //  Updates++;
           // Debug.Log(transform.position);
           // Debug.Log(_behaviourStateMachine.DebugInfo());
            //Debug.Log(_velocity);
          /*  if (Activated && !Controller.Collisions.Below.IsColliding)
            {
                Debug.LogWarning("not touching ground");
            }*/
        }

        protected override void Update()
        {


            Updates = 0;
            HandelRotation();
          /*  if (ShouldLookAtPlayer())
            {
                FacePlayer();
            }
            else
            {
                SetSpriteDirection();
            }*/
            _isOnScreen = StaticTools.IsPointOnScreenWithBuffer(transform.position);
            base.Update();
            _timeSinceLastShoot += Time.deltaTime;
        }
    }
}
