﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformSide = MoveAroundPlatform.PlatformSide;

namespace Characters.Enemies
{
    public class Spiny : Enemy
    {
        [HideInInspector]
        public BoxCollider2D AllowedMovementArea;
        [HideInInspector]
        public PlatformSide StartingPlatformSide;

        private bool _isMoving = false;
        private MoveAroundPlatform _move;


        protected override void Awake()
        {
            frozen = true;
            base.Awake();
        }

        protected override void Start()
        {
            base.Start();
            _stillUsingOldStateMachine = false;
        }

        protected override void Update()
        {
            if (!frozen)
            {
                base.Update();

                var miteVelocity = GetVelocityComponentVelocity();
                if (!_isMoving && miteVelocity != Vector2.zero)
                {
                    _isMoving = true;
                    animator.SetBool("Is Moving", true);
                }
                else if (_isMoving && miteVelocity == Vector2.zero)
                {
                    _isMoving = false;
                    animator.SetBool("Is Moving", false);
                }
            }
            else if (_isMoving)
            {
                _isMoving = false;
                animator.SetBool("Is Moving", false);
            }
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            if (controller.Collisions.Right.IsColliding || controller.Collisions.Left.IsColliding)
            {
                ZeroOutVelocityOnOneAxis('X');
            }
        }

        public override void InstantiateStateMachine()
        {
            frozen = false;

            float startingRotation = 0;
            switch (StartingPlatformSide)
            {
                case PlatformSide.Left:
                    startingRotation = 90;
                    break;
                case PlatformSide.Right:
                    startingRotation = 270;
                    break;
                case PlatformSide.Down:
                    startingRotation = 180;
                    break;
            }

            Rect allowedMovementAreaAsRect = Rect.zero;
            if (AllowedMovementArea != null)
            {
                allowedMovementAreaAsRect = new Rect(AllowedMovementArea.bounds.min, AllowedMovementArea.bounds.size);
            }

            _move = new MoveAroundPlatform(this, StartingPlatformSide, startingRotation, allowedMovementAreaAsRect, true);
            _behaviourStateMachine = new StateMachine(_move, true);
        }

        protected override void EvaluateTurnAround()
        {
            var miteTotalVelocity = Velocity;
            float xDir = Mathf.Sign(miteTotalVelocity.x);
            float yDir = Mathf.Sign(miteTotalVelocity.y);

            _isAboutToHitNPC = false;
            Vector3 origin = GetLookVectorOrigin();
            RaycastHit2D hit = new RaycastHit2D();

            if (_move.CurrentSideDirection == PlatformSide.Up || _move.CurrentSideDirection == PlatformSide.Down)
            {
                hit = Physics2D.Raycast(origin, Vector3.right * xDir, _enemyLookDistance, GlobalData.ENEMY_LAYER_SHIFTED);
            }
            else
            {
                hit = Physics2D.Raycast(origin, Vector3.up * yDir, _enemyLookDistance, GlobalData.ENEMY_LAYER_SHIFTED);
            }

            if (hit)
            {
                Debug.DrawLine(origin, hit.point, Color.red);
                if (_isAboutToHitNPC != true)
                {
                    _isAboutToHitNPC = true;
                    OtherEnemyDetected(hit.collider.gameObject);
                }
            }
            else
            {
                if (_move.CurrentSideDirection == PlatformSide.Up || _move.CurrentSideDirection == PlatformSide.Down)
                {
                    Debug.DrawLine(origin, origin + (Vector3.right * (xDir * _enemyLookDistance)), Color.green);
                }
                else
                {
                    Debug.DrawLine(origin, origin + (Vector3.up * (yDir * _enemyLookDistance)), Color.green);
                }
            }
        }

        protected override void OtherEnemyDetected(GameObject otherGameObject)
        {
            Enemy otherEnemy = otherGameObject.GetComponentInParent<Enemy>();
            base.OtherEnemyDetected(otherGameObject);

            if (otherEnemy != null && otherEnemy != this) //This is an enemy and it isn't us.
            {
                if ((_move.CurrentSideDirection == PlatformSide.Up || _move.CurrentSideDirection == PlatformSide.Down) && ((otherEnemy.Velocity.x > 0 && Velocity.x < 0) || (otherEnemy.Velocity.x < 0 && Velocity.x > 0))
                    || (_move.CurrentSideDirection == PlatformSide.Left || _move.CurrentSideDirection == PlatformSide.Right) && ((otherEnemy.Velocity.y > 0 && Velocity.y < 0) || (otherEnemy.Velocity.y < 0 && Velocity.y > 0)))
                {
                    if (!_requestsFromOthersToTurnAround.Contains(otherEnemy))
                    {
                        if (_lastToTurnMeAround == otherEnemy)
                        {
                            _lastToTurnMeAround = null;
                        }
                        _requestsFromOthersToTurnAround.Add(otherEnemy);
                        otherEnemy.RequestTurnAround(this);
                    }
                }
                else
                {
                    float velocityDiff = Velocity.x - otherEnemy.Velocity.x;
                    if (velocityDiff > 0) //This enemy is faster 
                    {
                        SetSpriteDirection(Controller.Collisions.FaceDir != 1, true);
                    }
                }
            }
        }

        public override bool CanTurnAround(Enemy requester)
        {
            return base.CanTurnAround(requester) && !_move.IsRotating;
        }

        protected override Vector3 GetLookVectorOrigin()
        {
            switch (_move.CurrentSideDirection)
            {
                case PlatformSide.Up:
                    if (controller.Collisions.FaceDir == 1) //Facing right when on the up side is moving right.
                    {
                        return new Vector3(_collider.bounds.max.x, _collider.bounds.center.y, 0);
                    }
                    else
                    {
                        return new Vector3(_collider.bounds.min.x, _collider.bounds.center.y, 0);
                    }
                case PlatformSide.Down:
                    if (controller.Collisions.FaceDir == 1) //Facing right when on the down side is moving left.
                    {
                        return new Vector3(_collider.bounds.min.x, _collider.bounds.center.y, 0);
                    }
                    else
                    {
                        return new Vector3(_collider.bounds.max.x, _collider.bounds.center.y, 0);
                    }
                case PlatformSide.Left:
                    if (controller.Collisions.FaceDir == 1) //Facing right when on the left side is moving up.
                    {
                        return new Vector3(_collider.bounds.center.x, _collider.bounds.max.y, 0);
                    }
                    else
                    {
                        return new Vector3(_collider.bounds.center.x, _collider.bounds.min.y, 0);
                    }
                case PlatformSide.Right:
                    if (controller.Collisions.FaceDir == 1) //Facing right when on the right side is moving down.
                    {
                        return new Vector3(_collider.bounds.center.x, _collider.bounds.min.y, 0);
                    }
                    else
                    {
                        return new Vector3(_collider.bounds.center.x, _collider.bounds.max.y, 0);
                    }
                default:
                    return Vector2.zero; //We should never get here, all enum options are accounted for above.
            }
        }
    }
}
