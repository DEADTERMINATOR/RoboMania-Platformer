﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Enemies.AnimationControllers
{
    public class TrackerAnimationController : MonoBehaviour, INPCAnimationController
    {
        /// <summary>
        /// Reference to the animator to set the parameters for the animations.
        /// </summary>
        private Animator _anim;


        // Use this for initialization
        private void Start()
        {
            _anim = GetComponentInChildren<Animator>();
        }


        public void SetStunnedAnimation(bool status)
        {
            _anim.SetBool("Stunned", status);
        }

        public void SetIdleAnimation()
        {
            _anim.SetBool("Fire", false);
            _anim.SetBool("Stunned", false);
        }

        #region Unused Animations
        public void SetWalkAnimation()
        {
            //Doesn't have a walk animation.
        }

        public void SetAlertAnimation()
        {
            //Doesn't have an alert animation.
        }

        public void SetShootAnimation()
        {
            //Doesn't have a shoot animation.
        }

        public void SetStopShootingAnimation()
        {
            //Doesn't have a stop shooting animation.
        }

        public void SetDeathAnimation()
        {
            //Can't die, so doesn't have a death animation.
        }
        #endregion
    }
}
