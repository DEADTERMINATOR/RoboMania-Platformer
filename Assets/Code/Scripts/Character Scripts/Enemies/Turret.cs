﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Enemies.AnimationControllers;

namespace Characters.Enemies
{
    public class Turret : Enemy
    {
        /// <summary>
        /// The width of the extents of the turret's body. Used as an offset when backpedaling to ensure the maximum point chosen
        /// places the turret's back against the edge of their active area, instead of their center.
        /// </summary>
        public const float BODY_EXTENTS = 2f;


        /// <summary>
        /// The amount of time the shot should arc through the air.
        /// </summary>
        public float ArcTime;

        /// <summary>
        /// The amount of time that should elapse between turret shots.
        /// </summary>
        public float TimeBetweenShots;

        /// <summary>
        /// The maximum distance (in X-axis units) the turret will allow itself to be from its target and still fire.
        /// </summary>
        public float MaximumDistance;

        /// <summary>
        /// The minimum distance (in X-axis units) the turret will try to maintain from its target.
        /// </summary>
        public float MinimumDistance;

        /// <summary>
        /// The projectile that the turret fires.
        /// </summary>
        public Grenade Projectile;

        /// <summary>
        /// Refere
        /// </summary>
        [HideInInspector]
        public PrefabPool ProjectilePool;

        /// <summary>
        /// Whether the player is currently within the turret's fire zone.
        /// </summary>
        [HideInInspector]
        public bool TargetInFireZone;

        /// <summary>
        /// The amount of time that has passed since the turret last fired.
        /// </summary>
        [HideInInspector]
        public float TimeSinceLastShot;

        /// <summary>
        /// Whether the turret is capable of backpedaling (i.e. it's not right against the edge of its active area).
        /// </summary>
        [HideInInspector]
        public bool CanBackpedal;


        /// <summary>
        /// The amount of time the turret has been gloating (i.e. doing nothing) after killing the player.
        /// </summary>
        private float _gloatTime;

        /// <summary>
        /// Reference to the animation controller for the turret.
        /// </summary>
        private TurretAnimationController _animController;

        /// <summary>
        /// Reference to the collider that represents the turret's fire zone.
        /// </summary>
        private BoxCollider2D _fireZone;


        //Use this for initialization
        protected override void Start()
        {
            /*
            base.Start();

            ProjectilePool = gameObject.AddComponent<PrefabPool>();
            ProjectilePool.PoolPrefab = Projectile.gameObject;

            _animController = GetComponent<TurretAnimationController>();
            _fireZone = transform.Find("Fire Zone").GetComponent<BoxCollider2D>();

            CanBackpedal = true;

            _gloatTime = 0;

            NPC turret = this;
            Turret castedTurret = this;
            BoxCollider2D activeArea = transform.parent.Find("Active Area").GetComponent<BoxCollider2D>();
            Transform projectileFirePosition = transform.Find("Sprite").Find("Projectile Fire Position");
            Player.PlayerMaster player = GlobalData.Player;
            GameObject playerGO = GlobalData.Player.gameObject;
            INPCAnimationController castedAnimController = _animController as INPCAnimationController;
            float moveSpeed = MoveSpeed;

            //Create states
            State patrol = new State("Patrol");
            State fire = new State("Fire");
            State backpedal = new State("Backpedal");
            State bask = new State("Bask");

            //Create state machine
            NPCStateMachine stateMachine = new NPCStateMachine(patrol, this);
            StateMachineComponent stateMachineComponent = gameObject.AddComponent<StateMachineComponent>();
            stateMachineComponent.StateMachine = stateMachine;

            //Create actions
            StateMachineAction setWaypoint = new SetWaypoint(ref turret);
            StateMachineAction moveTowards = new MoveTowardsOnOneAxis(ref turret, 0.05f, false);
            StateMachineAction checkForPlayer = new TurretCheckForTarget(ref castedTurret, ref playerGO, ref _fireZone);
            StateMachineAction setReadyForNewWaypoint = new SetReadyForNewWaypoint(ref turret, true);
            StateMachineAction notReadyForNewWaypoint = new SetReadyForNewWaypoint(ref turret, false);
            StateMachineAction calculateArcAndFire = new CalculateArcAndFire(ref castedTurret, ref _animController, projectileFirePosition, ArcTime);
            StateMachineAction moveAwayFromPlayer = new Backpedal(ref castedTurret, ref activeArea, MinimumDistance * 2f);
            StateMachineAction moveBackwards = new MoveTowardsOnOneAxis(ref turret, 0.05f, false, false);
            StateMachineAction setIdleAnimation = new SetIdleAnimation(ref castedAnimController);
            StateMachineAction setMoveAnimation = new SetWalkAnimation(ref castedAnimController);
            StateMachineAction setBackpedalAnimation = new SetAlertAnimation(ref castedAnimController);
            StateMachineAction killVelocity = new KillVelocity(ref turret);
            StateMachineAction setBackpedalMoveSpeed = new SetMoveSpeed(ref turret, MoveSpeed * 1.5f);
            StateMachineAction resetMoveSpeed = new SetMoveSpeed(ref turret, moveSpeed);
            StateMachineAction clearCanBackpedal = new SetCanBackpedal(ref castedTurret, true);
            StateMachineAction clearTime;
            StateMachineAction addTime;
            StateMachineAction setToFireTime;
            StateMachineAction addGloatTime;
            StateMachineAction clearGloatTime;
            unsafe
            {
                fixed (float* time = &TimeSinceLastShot)
                {
                    clearTime = new ClearTime(time);
                    addTime = new AddTime(time);
                    setToFireTime = new SetTime(time, TimeBetweenShots);
                }
                fixed (float* time = &_gloatTime)
                {
                    addGloatTime = new AddTime(time);
                    clearGloatTime = new ClearTime(time);
                }
            }

            //Create conditions
            Condition playerInActiveArea = new PlayerInActiveArea(ref player, ref _fireZone, true);
            Condition playerOutOfActiveArea = new PlayerInActiveArea(ref player, ref _fireZone, false);
            Condition turretCanBackpedal = new CanBackpedal(ref castedTurret);
            Condition playerLessThanMinimumDistance = new TargetTooClose(ref turret, ref playerGO, MinimumDistance);
            Condition fireToBackpedalCondition = new AndCondition(turretCanBackpedal, playerLessThanMinimumDistance);
            Condition turretReadyForNewWaypoint = new IsReadyForNewWaypoint(ref turret);
            Condition playerDead = new PlayerIsDead(ref player);
            Condition gloatTimeExceeded;
            unsafe
            {
                fixed (float* time = &_gloatTime)
                {
                    gloatTimeExceeded = new TimeExceeded(time, 4.0f);
                }
            }

            //Create transitions
            Transition patrolToFire = new Transition(fire, playerInActiveArea);
            Transition patrolToBask = new Transition(bask, playerDead);
            Transition fireToPatrol = new Transition(patrol, playerOutOfActiveArea);
            Transition fireToBackpedal = new Transition(backpedal, fireToBackpedalCondition);
            Transition fireToBask = new Transition(bask, playerDead);
            Transition backpedalToPatrol = new Transition(patrol, playerOutOfActiveArea);
            Transition backpedalToFire = new Transition(fire, turretReadyForNewWaypoint);
            Transition backpedalToBask = new Transition(bask, playerDead);
            Transition baskToPatrol = new Transition(patrol, gloatTimeExceeded);

            //Add actions and transitions to states
            patrol.AddEntryAction(setMoveAnimation);
            patrol.AddEntryAction(clearTime);
            patrol.AddEntryAction(clearCanBackpedal);
            patrol.AddStateAction(setWaypoint);
            patrol.AddStateAction(moveTowards);
            patrol.AddStateAction(checkForPlayer);
            patrol.AddTransition(patrolToFire);
            patrol.AddTransition(patrolToBask);

            fire.AddEntryAction(killVelocity);
            fire.AddEntryAction(setToFireTime);
            fire.AddStateAction(calculateArcAndFire);
            fire.AddStateAction(addTime);
            fire.AddTransition(fireToPatrol);
            fire.AddTransition(fireToBackpedal);
            fire.AddTransition(fireToBask);

            backpedal.AddEntryAction(setBackpedalMoveSpeed);
            backpedal.AddEntryAction(setBackpedalAnimation);
            backpedal.AddEntryAction(setReadyForNewWaypoint);
            backpedal.AddEntryAction(moveAwayFromPlayer);
            backpedal.AddStateAction(moveBackwards);
            backpedal.AddExitAction(killVelocity);
            backpedal.AddExitAction(resetMoveSpeed);
            backpedal.AddTransition(backpedalToPatrol);
            backpedal.AddTransition(backpedalToFire);
            backpedal.AddTransition(backpedalToBask);

            bask.AddEntryAction(killVelocity);
            bask.AddEntryAction(clearGloatTime);
            bask.AddEntryAction(setIdleAnimation);
            bask.AddStateAction(addGloatTime);
            bask.AddTransition(baskToPatrol);

            //Add remaining states to state machine.
            stateMachine.AddState(fire);
            stateMachine.AddState(backpedal);
            stateMachine.AddState(bask);

            //Since the turret will be in a patrol state to start, start the turret in it's walking animation state.
            _animController.SetWalkAnimation();
            */
        }

        //Update is called once per frame
        protected override void Update()
        {
            //_animController.SetMoveSpeed(MoveSpeed);
            base.Update();
            SetSpriteDirection();
        }


        public override bool SetSpriteDirection()
        {
            bool result = base.SetSpriteDirection();

            if (result)
                _fireZone.transform.localScale = new Vector3(-_fireZone.transform.localScale.x, _fireZone.transform.localScale.y, _fireZone.transform.localScale.z);

            return result;
        }

        public override bool SetSpriteDirection(bool switchToRight, bool safetyCheck = true, bool isAiming = false)
        {
            bool result = base.SetSpriteDirection(switchToRight, safetyCheck);

            if (result)
                _fireZone.transform.localScale = new Vector3(-_fireZone.transform.localScale.x, _fireZone.transform.localScale.y, _fireZone.transform.localScale.z);

            return result;
        }

        /// <summary>
        /// Calculates the force vector required to have an arc projectile land at the target with the desired amount of air time.
        /// </summary>
        /// <param name="targetMoving">Whether the target is moving.</param>
        /// <returns>The force vector required to perform the arced shot.</returns>
        private Vector3 CalculateArcVelocity(bool targetMoving)
        {
            Transform projectileFirePosition = transform.Find("Sprite").Find("Projectile Fire Position");

            //Calculate target vectors
            Vector3 toTarget;
            Vector3 toTargetX;

            toTarget = GlobalData.Player.transform.position - projectileFirePosition.position;

            if (targetMoving)
            {
                //Predict where the target will be at the end of the desired arc time, and fire there.
                toTargetX = new Vector3(GlobalData.Player.transform.position.x + GlobalData.Player.Velocity.x * ArcTime, 0, 0);
                toTargetX -= projectileFirePosition.position;
            }
            else
            {
                //Fire where the target is now.
                toTargetX = toTarget;
            }
            toTargetX.y = 0;
            toTargetX.z = 0;

            //Calculate x and y
            float y = toTarget.y;
            float x = toTargetX.magnitude;

            /* Calculate starting speeds for x and y. Physics formula d = v0 * t + 1/2 * a * t^2
             * Where a is "-gravity" but only on the y plane, and a is 0 in x plane.
             * So x = v0x * t => v0x = x / t.
             * And y = v0y * t - (1/2 * gravity * t^2)
             * => y + (1/2 * gravity * t^2) = v0y * t - (1/2 * gravity * t^2) + (1/2 * gravity * t^2)
             * => y / t + (1/2 * gravity * t^2) / t = (v0y * t) / t
             * => v0y = y / t + (1/2 * gravity * t)
             */
            float t = ArcTime;
            float v0y = y / t + 0.5f * Physics.gravity.magnitude * t;
            float v0x = x / t;

            //Create result vector for calculated starting speeds
            Vector3 result = toTargetX.normalized;        // get direction of x but with magnitude 1
            result *= v0x;                                // set magnitude of x to v0x (starting speed on the x-axis)
            result.y = v0y;                               // set y to v0y (starting speed on the y-axis)

            return result;
        }

        /*
         * vf = vi + at
         * vf - vi = vi - vi + at
         * vf - vi = at
         * (vf - vi) / a = t
         * 
         * (0 - -20) / 9.8 = t
         * 20 / 9.8 = 2.041s
         * 
         * d = v0 * t + 1/2 * a * t^2
         * d = v0 * t + 0
         * d = v0 * t
         * d = 3 * 2.041
         * = 6.123
        */

        /*
        /// <summary>
        /// Fires the turret's projectile along the calculated arc.
        /// </summary>
        public override bool PerformAction()
        {
            if (_turret.TimeSinceLastShot >= _turret.TimeBetweenShots)
            {
                Vector3 arcVelocity = Vector3.zero;

                if (Mathf.Abs(GlobalData.Player.Movement.Velocity.x) < GlobalData.Player.MoveSpeed * 0.05f)
                {
                    arcVelocity = CalculateArcVelocity(false);
                }
                else if (Mathf.Abs(GlobalData.Player.Movement.Velocity.x) >= GlobalData.Player.MoveSpeed * 0.05f)
                {
                    arcVelocity = CalculateArcVelocity(true);
                }

                //float shotAngle = Vector3.Angle(_turret.Controller.Collisions.FaceDir == 1 ? Vector3.right : Vector3.left, arcVelocity);
                float shotAngle = Vector3.Angle(Vector3.right, arcVelocity);

                ArcedProjectile pro = _turret.ProjectilePool.Spawn().GetComponent<ArcedProjectile>();
                pro.transform.position = _firePoint.position;
                pro.Fire(new Vector3(0, 0, shotAngle), arcVelocity, 25, _turret.transform.parent.gameObject);

                _turret.TimeSinceLastShot = 0;
                _animController.SetShootAnimation();
            }
            else
            {
                _animController.SetIdleAnimation();
            }

            return true;
        }
        */
    }
}
