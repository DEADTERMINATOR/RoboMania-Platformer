﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Enemies
{
    public class Mite : Enemy
    {
        #region JumpData Struct
        public struct JumpData
        {
            public float JumpVelocity;
            public Vector2 AnticipatedLandingPosition;
            public bool IsJumpValid;

            public JumpData(float jumpVelocity, Vector2 anticipatedLandingPosition, bool isJumpValid)
            {
                JumpVelocity = jumpVelocity;
                AnticipatedLandingPosition = anticipatedLandingPosition;
                IsJumpValid = isJumpValid;
            }
        }
        #endregion

        #region Constants
        private const float RAYCAST_DISTANCE = 10f;

        private const float JUMP_VELOCITY = 27.5f;
        private const float JUMP_BEFORE_COLLISION_SECONDS = 0.175f;

        private const float HIGH_SPEED_JUMP_X_VELOCITY_MULTIPLIER = 1f;
        private const float LOW_SPEED_JUMP_X_VELOCITY_MULTIPLIER = 0.25f;

        /* The buffer distances that should be left before a collision when a jump is triggered (from the front, since jumps triggered from behind are based on remaining time to collison).
         * For example, if we are jumping over another mite travelling at 3 units per second, we'd want to trigger the jump 1.5 units before the collision. */
        private const float JUMP_DISTANCE_BUFFER_PER_VELOCITY_UNIT_ENEMIES = 0.1f;
        private const float JUMP_DISTANCE_BUFFER_PER_VELOCITY_UNIT_OBSTACLES = 0.25f;
        #endregion

        public bool PreparedToAvoid { get; private set; } = false;
        public bool IsJumping { get; private set; } = false;
        public bool AirCollisionAvoidanceJump { get; private set; } = false;

        private bool _jumpWhenTargetReached = false;

        private bool _jumpAfterTimeElapsed = false;
        private float _timeUntilJump = 0;

        private BoxCollider2D _miteCollider;
        private Animator _jumpJetAnimator;

        private Enemy _forwardEnemyComponent;

        // Use this for initialization
        protected override void Start()
        {
            base.Start();

            if (ActiveArea == Rect.zero)
            {
                Collider2D activeArea = GameObject.Find("Active Area").GetComponent<Collider2D>();
                if (activeArea != null)
                    ActiveArea = new Rect(activeArea.bounds.min.x, activeArea.bounds.min.y, activeArea.bounds.size.x, activeArea.bounds.size.y);
                //else
                //Debug.LogError("The mite does not have an active area.");
            }

            _miteCollider = GetComponent<BoxCollider2D>();
            _jumpJetAnimator = transform.Find("Sprite").Find("Jump Jet").GetComponent<Animator>();

            //Setup the state machine.
            State patrol = new State("Patrol");

            /*
            NPCStateMachine stateMachine = new NPCStateMachine(patrol, this);
            StateMachineComponent stateMachineComponent = gameObject.AddComponent<StateMachineComponent>();
            stateMachineComponent.StateMachine = stateMachine;
            */
            _behaviourStateMachine = new StateMachine(patrol, activated);

            patrol.AddStateAction(new FunctionPointerAction(new System.Func<bool>(DetermineVelocityAndInputXAxis)));

            patrol.AddStateAction(new FunctionPointerAction(new System.Func<object, object, bool>((axis, returnSuccessOnlyOnCompletion) =>
            {
                return MoveAndJumpWhenTargetReached(axis, returnSuccessOnlyOnCompletion);
            }), 'X', false));

            patrol.AddStateAction(new FunctionPointerAction(new System.Func<bool>(JumpAfterTimeElasped)));
            patrol.AddStateAction(new FunctionPointerAction(new System.Func<bool>(ScanAndJump)));
        }

        // Update is called once per frame
        protected override void Update()
        {
            if (!frozen && !IsDead)
            {
                Debug.DrawLine((Vector2)TargetVector - new Vector2(1, 0), (Vector2)TargetVector + new Vector2(1, 0), Color.black, 0.1f);
                Debug.DrawLine((Vector2)TargetVector - new Vector2(0, 1), (Vector2)TargetVector + new Vector2(0, 1), Color.black, 0.1f);

                if (transform.position.x <= ActiveArea.min.x)
                    SetSpriteDirection(true, true);
                else if (transform.position.x >= ActiveArea.max.x)
                    SetSpriteDirection(false, true);

                if (Controller.Collisions.IsCollidingBelow)
                {
                    animator.SetBool("On Ground", true);
                    if (IsJumping)
                    {
                        IsJumping = false;
                        AirCollisionAvoidanceJump = false;
                        MoveSpeed = BaseMoveSpeed;
                    }
                }
                else
                {
                    animator.SetBool("On Ground", false);
                    if (!IsJumping)
                        IsJumping = true;
                }

                base.Update();
            }
        }

        public override void PostSpawnSetup()
        {
            base.PostSpawnSetup();
        }

        #region Jump Functions
        public bool AttemptJump()
        {
            JumpData jumpPlan = PlanJump(Velocity.x);

            if (jumpPlan.IsJumpValid)
            {
                PerformJump(jumpPlan);
                return true;
            }
            else
            {
                //If the mite couldn't perform a valid jump in it's current travel direction, simulate X-velocity in the opposite direction and see if it can jump in that direction.
                jumpPlan = PlanJump(-Velocity.x);
                if (jumpPlan.IsJumpValid)
                {
                    SetSpriteDirection(Controller.Collisions.FaceDir != 1, true);
                    DetermineVelocityAndInputXAxis();

                    PerformJump(jumpPlan);
                    return true;
                }
            }

            return false;
        }

        private bool PerformJump(JumpData jumpPlan)
        {
            SetVelocityOnOneAxis('Y', jumpPlan.JumpVelocity);

            if (!IsJumping)
            {
                IsJumping = true;
            }

            if (animator.GetCurrentAnimatorStateInfo(0).shortNameHash != Animator.StringToHash("Jump"))
            {
                animator.SetTrigger("Jump");
            }

            PreparedToAvoid = false;
            return true;
        }
        /// <summary>
        /// Plans a potential jump by analyzing where height and distance the mite will achieve with the base jump velocity, and adjusting that velocity if necessary,
        /// or deeming the jump invalid if a valid adjustment is not possible.
        /// </summary>
        /// <param name="xVelocity">The X-velocity that the jump should be planned with. This may be different than the mite's actual current velocity, which is why it's a parameter.</param>
        /// <returns>A jump plan containing the acceptable jump velocity, the anticipated landing position, and whether the jump was deemed valid.</returns>
        private JumpData PlanJump(float xVelocity)
        {
            float actualJumpVelocity = JUMP_VELOCITY; //The jump velocity that will applied should the mite perform the jump.
            bool jumpValid = true;

            Vector2 baseDisplacement; //The displacement created by the jump with no modifications.
            Vector2 actualDisplacement;

            float baseTimeToPeak;
            float actualTimeToPeak;
            float currentGravity = currentActiveGravityState.GravityVelocity.y;

            #region Time Equation Explanation
            /* Calculate time from start of jump to landing. Physics formula vf = vi + at
             * Where vf = 0 (peaked at the top of the jump), vi = JUMP_VELOCITY, a = gravity (-9.8)
             * vf - vi = vi - vi + at
             * => (vf - vi) / a = at / a
             * => t = (vf - vi) / a
             * Then, any time we deal with the X-axis, we multiply the time by two, since we need to account for the time back down.
             */
            #endregion

            actualTimeToPeak = (0 - JUMP_VELOCITY) / currentGravity;
            baseTimeToPeak = actualTimeToPeak;

            #region Displacement Equation Explanation
            /* Calculate displacement. Physics formula d = vi * t + 1/2 * a * t^2
             * Where vi = JUMP_VELOCITY on the y-axis, movement speed on the x-axis
             * And a = -gravity on the y-axis, 0 on the x-axis
             * And t is as calculated above
             * 
             * dx = speed * t + 1/2 * 0 * t^2
             * => dx = speed * t
             * 
             * dy = JUMP_VELOCITY * t - 1/2 * gravity * t^2
             */
            #endregion

            actualDisplacement = Vector2.zero;
            actualDisplacement.x = xVelocity * actualTimeToPeak * 2;
            actualDisplacement.y = JUMP_VELOCITY * actualTimeToPeak + 0.5f * currentGravity * Mathf.Pow(actualTimeToPeak, 2);
            baseDisplacement = actualDisplacement;

            /* Check if the mite is capable of achieving this displacement without encountering issues (e.g. hitting the ceiling).
             * To do this, we raycast up the expected y-axis displacement to check for any possible collisions.
             * We also check if the predicted landing position falls outside of any desired area (active area, passed the next waypoint etc).
             */
            RaycastHit2D aboveRaycast = Physics2D.Raycast(new Vector2(_miteCollider.bounds.center.x, _miteCollider.bounds.max.y), Vector3.up, actualDisplacement.y,
                GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.NPC_LAYER_SHIFTED | GlobalData.ENEMY_LAYER_SHIFTED);

            // Check the percentage of anticipated displacement the mite can perform in the Y-axis. (1.0 == 100%)
            float aboveDisplacementClearence = 1.0f;

            if (aboveRaycast)
                aboveDisplacementClearence = aboveRaycast.distance / actualDisplacement.y;

            // Check if the mite needs to modify its jump velocity based on a potential collision above.
            if (!StaticTools.ThresholdApproximately(aboveDisplacementClearence, 1.0f, 0.1f))
            {
                // If the mite can't jump the full height, adjust its jump height to the amount it can jump.
                actualDisplacement.y *= aboveDisplacementClearence;
                actualTimeToPeak *= aboveDisplacementClearence;

                #region Jump Velocity Equation Explanation
                /* Physics formula d = vi * t - 1/2 * a * t^2.
                 * Solve for new viy.
                 * => dy + 1/2 * a * t^2 = viy * t - (1/2 * a * t^2) + (1/2 * a * t^2)
                 * => dy / t + (1/2 * a * t^2) / t = (viy * t) / t
                 * => viy = dy / t + 1/2at
                 */
                #endregion

                actualJumpVelocity = actualDisplacement.y / actualTimeToPeak - 0.5f * currentGravity * actualTimeToPeak;

                // Recalculate x-axis displacement given the modified jump parameters
                actualDisplacement.x = xVelocity * actualTimeToPeak * 2;
            }

            // We can't necessarily know where the mite will land on the Y (since it could be jumping up a platform, but it shouldn't matter for this case).
            Vector2 anticipatedLandingPosition = new Vector2(transform.position.x + actualDisplacement.x, transform.position.y);

            // Check if the anticipated landing position is within the mite's active area. If not, then the jump isn't valid.
            if (!ActiveArea.Contains(anticipatedLandingPosition))
                jumpValid = false;

            if (Controller.Collisions.FaceDir == -1)
                actualDisplacement.x *= -1;

            return new JumpData(actualJumpVelocity, anticipatedLandingPosition, jumpValid);
        }
        #endregion

        #region Scan Functions
        /// <summary>
        /// Scans for any enemies in front of or behind the mite, and handles the planning of the jump that occurs to avoid them.
        /// </summary>
        private void ScanForObstacles()
        {
            float timeToCollisionInFront;
            float timeToCollisionBehind;

            Collider2D forwardCollision = ScanInFront(out timeToCollisionInFront);
            Collider2D backwardsCollision = ScanBehind(out timeToCollisionBehind);

            // If we have determined that a collision either in front or behind the mite is imminent, determine the position the collision will occur at.
            // If there is a potential collision both in front and behind, determine which is more pressing.
            if (timeToCollisionInFront != float.MaxValue && timeToCollisionInFront > 0 && (timeToCollisionBehind < 0 || timeToCollisionInFront < timeToCollisionBehind))
                ProcessScanInFront(forwardCollision, timeToCollisionInFront);
            else if (timeToCollisionBehind != float.MaxValue && timeToCollisionBehind > 0 && (timeToCollisionInFront < 0 || timeToCollisionBehind <= timeToCollisionInFront))
                ProcessScanBehind(backwardsCollision, timeToCollisionBehind);
        }

        /// <summary>
        /// Scans for any enemies or obstacles in front of the mite that may need to be jumped over.
        /// </summary>
        /// <param name="miteCollisionBox">The collision box that encompasses the Mite.</param>
        /// <param name="timeToCollisionInFront">Holds the amount of time remaining to a potential collision.</param>
        /// <returns>The Collider2D that the Mite is in danger of hitting, or null if nothing is detected.</returns>
        private Collider2D ScanInFront(out float timeToCollisionInFront)
        {
            timeToCollisionInFront = float.MaxValue;

            // Raycast in front of the mite to see if there are any obstacles (enemies, higher platforms, etc) that we need to jump over.
            Ray2D ray = new Ray2D(new Vector2(Controller.Collisions.FaceDir == 1 ? _miteCollider.bounds.max.x : _miteCollider.bounds.min.x, _miteCollider.bounds.center.y),
                Controller.Collisions.FaceDir == 1 ? CharacterSpriteCollection.transform.right : -CharacterSpriteCollection.transform.right);
            RaycastHit2D raycast = Physics2D.Raycast(ray.origin, ray.direction, RAYCAST_DISTANCE, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.ENEMY_LAYER_SHIFTED);

            Debug.DrawRay(ray.origin, ray.direction * RAYCAST_DISTANCE, Color.yellow, 0.1f);

            if (raycast)
            {
                if (1 << raycast.collider.gameObject.layer == GlobalData.ENEMY_LAYER_SHIFTED)
                {
                    Enemy hitEnemyComponent = raycast.collider.transform.root.GetComponent<Enemy>();

                    if (hitEnemyComponent != null && hitEnemyComponent != this)
                    {
                        BoxCollider2D hitEnemyCollisionBox = hitEnemyComponent.GetComponent<BoxCollider2D>();

                        // Calculate how long until the the mite and the other enemy collide.
                        timeToCollisionInFront = CalculateTimeToCollision(Controller.Collisions.FaceDir == 1 ? _miteCollider.bounds.max.x : _miteCollider.bounds.min.x,
                            hitEnemyComponent.Controller.Collisions.FaceDir == 1 ? hitEnemyCollisionBox.bounds.max.x : hitEnemyCollisionBox.bounds.min.x,
                            Velocity.x, hitEnemyComponent.Velocity.x);

                        return raycast.collider;
                    }
                }
                else
                {
                    // Check the angle of the obstacle that was detected; it might be a hill that the might can walk up.
                    float obstacleAngle = Vector2.Angle(Vector2.up, raycast.normal);

                    if (obstacleAngle >= 60 && ActiveArea.Contains(raycast.point))
                    {
                        if (raycast.distance <= 0.05f)
                        {
                            //If the raycast hit terrain and distance is incredible small, there is a chance the mite is stuck against a wall, so just turn around.
                            SetSpriteDirection(Controller.Collisions.FaceDir == 1 ? false : true, true);
                            return null;
                        }

                        // Calculate how long until the mite hits the obstacle.
                        timeToCollisionInFront = CalculateTimeToCollision(Controller.Collisions.FaceDir == 1 ? _miteCollider.bounds.max.x : _miteCollider.bounds.min.x,
                            raycast.point.x, Velocity.x, 0);

                        return raycast.collider;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Processes the detected collision in front of the Mite, including determining where and when it needs to jump to avoid the collision.
        /// </summary>
        /// <param name="miteCollisionBox">The collision box that encompasses the Mite.</param>
        /// <param name="forwardCollision">The Collider2D that the Mite is in danger of colliding with.</param>
        /// <param name="timeToCollisionInFront">The amount of time until the collision is supposed to occur.</param>
        private void ProcessScanInFront(Collider2D forwardCollision, float timeToCollisionInFront)
        {
            // Use the equation that was re-arranged for the time calculation to find the position - positionX(t) = initialPositionX + xVelocity * t
            float miteXCollisionPosition = (Controller.Collisions.FaceDir == 1 ? _miteCollider.bounds.max.x : _miteCollider.bounds.min.x) + Velocity.x * timeToCollisionInFront;

            if (1 << forwardCollision.gameObject.layer == GlobalData.ENEMY_LAYER_SHIFTED)
            {
                Enemy hitEnemyComponent = forwardCollision.transform.root.GetComponent<Enemy>();

                //If the hit enemy component is a robot mite, then we need to communicate with it so that only one of the mites jumps.
                if (hitEnemyComponent is Mite)
                {
                    Mite castedEnemy = hitEnemyComponent as Mite;

                    if (castedEnemy.PreparedToAvoid || (castedEnemy._forwardEnemyComponent == this && castedEnemy.IsJumping))
                        return; //The other mite has already detected us and is prepared to avoid us.
                    else if (!IsJumping)
                        PreparedToAvoid = true; //We'll volunteer to avoid the other mite.
                    else if (!castedEnemy.IsJumping)
                        castedEnemy.PreparedToAvoid = true;
                }

                // Set the mite's potential new target position to the point that it should jump at, the calculated collision point minus the extents of the mite's collision box
                // (to account for the fact that the calculation was done from the front of the mite) plus a buffer based on the velocity of the enemy (so the mite jumps before the collision).
                float xCollisionPositionOffset = _miteCollider.bounds.extents.x + Mathf.Abs(Velocity.x - hitEnemyComponent.Velocity.x) * JUMP_DISTANCE_BUFFER_PER_VELOCITY_UNIT_ENEMIES;
                Vector2 potentialTargetVector = new Vector2(Controller.Collisions.FaceDir == 1 ? miteXCollisionPosition - xCollisionPositionOffset : miteXCollisionPosition + xCollisionPositionOffset, transform.position.y);

                if (ActiveArea.Contains(potentialTargetVector))
                {
                    TargetVector = potentialTargetVector;
                    _forwardEnemyComponent = hitEnemyComponent;

                    _jumpWhenTargetReached = true;
                    _jumpAfterTimeElapsed = false;
                }
            }
            else
            {
                // Set the mite's new target position to the point that is should jump at, except since the obstacle is stationary, we only need to account for the mite's velocity.
                float xCollisionPositionOffset = _miteCollider.bounds.extents.x + Mathf.Abs(Velocity.x) * JUMP_DISTANCE_BUFFER_PER_VELOCITY_UNIT_OBSTACLES;
                Vector2 potentialTargetVector = new Vector2(Controller.Collisions.FaceDir == 1 ? miteXCollisionPosition - xCollisionPositionOffset : miteXCollisionPosition + xCollisionPositionOffset, transform.position.y);

                if (ActiveArea.Contains(potentialTargetVector))
                {
                    TargetVector = potentialTargetVector;
                    _forwardEnemyComponent = null;

                    _jumpWhenTargetReached = true;
                    _jumpAfterTimeElapsed = false;
                }
            }
        }

        /// <summary>
        /// Scans for any enemies approaching the Mite from behind that are moving fast enough that the mite needs to jump to let them pass.
        /// </summary>
        /// <param name="miteCollisionBox">The collision box that encompasses the Mite.</param>
        /// <param name="timeToCollisionBehind">Holds the amount of time until the potential collision.</param>
        /// <returns>The Collider2D that is in danger of running into the Mite, or null if nothing is detected.</returns>
        private Collider2D ScanBehind(out float timeToCollisionBehind)
        {
            timeToCollisionBehind = float.MaxValue;

            // Raycast behind as well, in case something the mite needs to jump over is approaching from behind at a faster speed than the mite.
            Ray2D ray = new Ray2D(new Vector2(Controller.Collisions.FaceDir == 1 ? _miteCollider.bounds.min.x : _miteCollider.bounds.max.x, _miteCollider.bounds.center.y), Controller.Collisions.FaceDir == 1 ? -CharacterSpriteCollection.transform.right : CharacterSpriteCollection.transform.right);
            RaycastHit2D raycast = Physics2D.Raycast(ray.origin, ray.direction, RAYCAST_DISTANCE, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.ENEMY_LAYER_SHIFTED);

            Debug.DrawRay(ray.origin, ray.direction * RAYCAST_DISTANCE, Color.magenta, 0.1f);

            if (raycast)
            {
                Enemy hitEnemyComponent = raycast.collider.transform.root.GetComponent<Enemy>();

                // We don't need to check for other mites behind us because all mites move the same speed, so one behind us will never catch up.
                if (hitEnemyComponent != null && hitEnemyComponent != this)
                {
                    BoxCollider2D hitEnemyCollisionBox = hitEnemyComponent.GetComponent<BoxCollider2D>();

                    // Calculate how long until the the mite and the other enemy collide.
                    // Since we have a collision from behind, we need to calculate the time to collision from the perspective of the enemy behind, so reverse the variables from the in front calculation.
                    timeToCollisionBehind = CalculateTimeToCollision(hitEnemyComponent.Controller.Collisions.FaceDir == 1 ? hitEnemyCollisionBox.bounds.max.x : hitEnemyCollisionBox.bounds.min.x,
                        Controller.Collisions.FaceDir == 1 ? _miteCollider.bounds.min.x : _miteCollider.bounds.max.x,
                        hitEnemyComponent.Velocity.x, Velocity.x);

                    return raycast.collider;
                }
            }

            return null;
        }

        /// <summary>
        /// Processes the detected collision behind the Mite, including when it needs to jump in order to avoid the collision.
        /// </summary>
        /// <param name="miteCollisionBox">The collision box encompassing the Mite.</param>
        /// <param name="backwardsCollision">The Collider2D that is in danger of hitting the Mite.</param>
        /// <param name="timeToCollisionBehind">The amount of time until the collision is supposed to occur.</param>
        private void ProcessScanBehind(Collider2D backwardsCollision, float timeToCollisionBehind)
        {
            float miteXCollisionPosition = Controller.Collisions.FaceDir == 1 ? _miteCollider.bounds.min.x : _miteCollider.bounds.max.x + Velocity.x * timeToCollisionBehind;

            Enemy hitEnemyComponent = backwardsCollision.transform.root.GetComponent<Enemy>();
            BoxCollider2D hitEnemyCollisionBox = hitEnemyComponent.GetComponent<BoxCollider2D>();

            float hitEnemyXCollisionPosition = hitEnemyComponent.Controller.Collisions.FaceDir == 1 ? hitEnemyCollisionBox.bounds.max.x : hitEnemyCollisionBox.bounds.min.x;

            _timeUntilJump = timeToCollisionBehind - JUMP_BEFORE_COLLISION_SECONDS;
            _jumpAfterTimeElapsed = true;
            _jumpWhenTargetReached = false;
        }

        /// <summary>
        /// Scans for enemies below the mite when they have jumped that the mite is in danger of landing on top of.
        /// </summary>
        /// <returns>True if any enemies are detected, false otherwise.</returns>
        private Collider2D ScanBelow()
        {
            // First we do a scan in the front and back of the mite to see how far we are from the ground (for the animator to judge when to start the landing animation).
            Ray2D ray = new Ray2D(new Vector2(_miteCollider.bounds.min.x, _miteCollider.bounds.min.y), -Vector3.up);
            RaycastHit2D minHit = Physics2D.Raycast(ray.origin, ray.direction, 100, GlobalData.OBSTACLE_LAYER_SHIFTED);

            ray.origin = new Vector2(_miteCollider.bounds.max.x, _miteCollider.bounds.min.y);
            RaycastHit2D maxHit = Physics2D.Raycast(ray.origin, ray.direction, 100, GlobalData.OBSTACLE_LAYER_SHIFTED);

            float distanceToGround = float.MaxValue;
            if (minHit)
            {
                distanceToGround = minHit.distance;
            }
            if (maxHit && maxHit.distance < distanceToGround)
            {
                distanceToGround = maxHit.distance;
            }
            animator.SetFloat("Distance From Ground", distanceToGround);

            // Then we do scans along the whole bottom of the mite to see if there is something we need to stay in the air to avoid.
            var groundController = (GroundController2D)Controller;
            for (int i = 0; i < groundController.Raycaster.HorizontalRayCount; ++i)
            {
                ray.origin = new Vector2(_miteCollider.bounds.min.x + Velocity.x * Time.fixedDeltaTime + groundController.Raycaster.HorizontalRaySpacing * i, _miteCollider.bounds.min.y);
                RaycastHit2D raycast = Physics2D.Raycast(ray.origin, ray.direction, 0.5f, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.ENEMY_LAYER_SHIFTED);

                Debug.DrawRay(ray.origin, ray.direction * 0.5f, Color.cyan, 0.1f);

                if (raycast && 1 << raycast.collider.gameObject.layer != GlobalData.OBSTACLE_LAYER_SHIFTED)
                    return raycast.collider;
            }

            return null;
        }
        #endregion

        #region Behaviours
        private bool DetermineVelocityAndInputXAxis()
        {
            var newXVelocity = Controller.Collisions.FaceDir * MoveSpeed;
            SetVelocityOnOneAxis('X', newXVelocity);

            if (newXVelocity < 0)
            {
                movementInput.x = -1;
            }
            else if (newXVelocity > 0)
            {
                movementInput.x = 1;
            }
            else
            {
                movementInput.x = 0;
            }

            return true;
        }

        private bool MoveAndJumpWhenTargetReached(object axis, object returnSuccessOnlyOnCompletion)
        {
            if (_jumpWhenTargetReached)
            {
                if ((Controller.Collisions.FaceDir == 1 && transform.position.x > TargetVector.x) ||
                    (Controller.Collisions.FaceDir == -1 && transform.position.x < TargetVector.x))
                {
                    bool attemptJump = AttemptJump();
                    if (!attemptJump && _forwardEnemyComponent != null && _forwardEnemyComponent is Mite)
                    {
                        Mite castedEnemy = _forwardEnemyComponent as Mite;
                        castedEnemy.AttemptJump();
                    }

                    _jumpWhenTargetReached = false;
                    _jumpAfterTimeElapsed = false;
                }
                else
                {
                    MoveTowardsOnOneAxis(axis, returnSuccessOnlyOnCompletion);

                    bool moveTowardsSuccess = Mathf.Abs(transform.position.x - TargetVector.x) <= ARRIVAL_DISTANCE;

                    if (moveTowardsSuccess)
                    {
                        SetVelocityOnOneAxis('X', Controller.Collisions.FaceDir * BaseMoveSpeed * HIGH_SPEED_JUMP_X_VELOCITY_MULTIPLIER);
                        MoveSpeed = BaseMoveSpeed * HIGH_SPEED_JUMP_X_VELOCITY_MULTIPLIER;
                        AttemptJump();

                        _jumpWhenTargetReached = false;
                        _jumpAfterTimeElapsed = false;
                    }
                }
            }

            return true;
        }

        private bool JumpAfterTimeElasped()
        {
            if (_jumpAfterTimeElapsed)
            {
                _timeUntilJump -= Time.deltaTime;
                if (_timeUntilJump <= 0)
                {
                    SetVelocityOnOneAxis('X', Controller.Collisions.FaceDir * BaseMoveSpeed * LOW_SPEED_JUMP_X_VELOCITY_MULTIPLIER);
                    MoveSpeed = BaseMoveSpeed * LOW_SPEED_JUMP_X_VELOCITY_MULTIPLIER;
                    AttemptJump();

                    _timeUntilJump = 1;
                    _jumpAfterTimeElapsed = false;
                    _jumpWhenTargetReached = false;
                }
            }

            return true;
        }

        private bool ScanAndJump()
        {
            //First, check if the mite is inside an enemy. If so, attempt an emergency jump to get out.
            Ray2D ray = new Ray2D(new Vector2(_miteCollider.bounds.min.x, _miteCollider.bounds.center.y), transform.right);
            RaycastHit2D[] raycasts = Physics2D.RaycastAll(ray.origin, ray.direction, _miteCollider.bounds.extents.x * 2, GlobalData.ENEMY_LAYER_SHIFTED);
            if (raycasts.Length != 0)
            {
                foreach (RaycastHit2D raycast in raycasts)
                {
                    Enemy enemy = raycast.collider.transform.root.GetComponent<Enemy>();
                    if (enemy == this)
                    {
                        continue;
                    }
                    else if (enemy is Mite)
                    {
                        Mite castedMite = enemy as Mite;
                        if (!IsJumping && !castedMite.IsJumping)
                        {
                            //If neither mite is jumping, this one'll attempt to jump to end the collision.
                            AttemptJump();
                        }
                        else if (castedMite.IsJumping)
                        {
                            if (!IsJumping)
                            {
                                //If this mite isn't jumping, but the other one is, they're probably trying to resolve the collision, so we ignore it.
                                continue;
                            }
                            else if (IsJumping && !castedMite.AirCollisionAvoidanceJump)
                            {
                                //If both mite's are jumping, one of the mite's will attempt another jump to resolve the air collision.
                                bool success = AttemptJump();
                                if (success)
                                    AirCollisionAvoidanceJump = true;
                            }
                        }
                    }
                    else
                    {
                        if (!IsJumping)
                        {
                            AttemptJump();
                            return true;
                        }
                    }
                }
            }

            if (!IsJumping)
            {
                ScanForObstacles();
            }
            else
            {
                if (Velocity.y <= 0)
                {
                    Collider2D enemyBelow = ScanBelow();
                    if (enemyBelow != null)
                    {
                        Enemy enemyComponent = enemyBelow.transform.root.GetComponent<Enemy>();

                        if (Mathf.Sign(enemyComponent.Velocity.x) == Mathf.Sign(Velocity.x))
                        {
                            SetVelocity(new Vector2(Controller.Collisions.FaceDir * BaseMoveSpeed * LOW_SPEED_JUMP_X_VELOCITY_MULTIPLIER, 0), OBJECT_VELOCITY_COMPONENT);
                            MoveSpeed = BaseMoveSpeed * LOW_SPEED_JUMP_X_VELOCITY_MULTIPLIER;
                        }
                        else
                        {
                            SetVelocity(new Vector2(Controller.Collisions.FaceDir * BaseMoveSpeed * HIGH_SPEED_JUMP_X_VELOCITY_MULTIPLIER, 0), OBJECT_VELOCITY_COMPONENT);
                            MoveSpeed = BaseMoveSpeed * HIGH_SPEED_JUMP_X_VELOCITY_MULTIPLIER;
                        }
                        AttemptJump();

                        _jumpJetAnimator.SetTrigger("One Time Fire");
                    }
                }
            }

            return true;
        }
        #endregion

        /// <summary>
        /// Calculates the amount of time until two objects will collider, given the position for both objects and their respective velocities.
        /// </summary>
        /// <param name="initialPositionXA">The initial position for the first object.</param>
        /// <param name="initialPositionXB">The initial position for the second object.</param>
        /// <param name="aXVelocity">The velocity for the first object.</param>
        /// <param name="bXVelocity">The velocity for the second object.</param>
        /// <returns>The time until the collision. A positive time indicates that the collision will occur after that much time. A negative time indicates the collision has already occured.
        ///          And a non-zero time indicates that a collision will never happen.</returns>
        private float CalculateTimeToCollision(float initialPositionXA, float initialPositionXB, float aXVelocity, float bXVelocity)
        {
            #region Collision Time Equation Explanation
            /* Calculate the position and time the two enemies will collide.
             * We only need to consider the X-axis in these calculations
             * Equation for finding an object position at a point in time - positionX(t) = initialPositionX + xVelocity * t
             * At the point of collision, the positions of both objects will be the same, so set positionX(t) for both objects
             * to be the same, and solve for t
             * initialPositionXA + aXVelocity * t = initialPositionXB + bXVelocity * t
             * => initialPositionXA + aXVelocity * t - bXVelocity * t = initialPositionXB + (bXVelocity * t) - (bXVelocity * t)
             * => initialPositionXA - initialPositionXA + aXVelocity * t - bXVelocity * t = initialPositionXB - initialPositionXA
             * => aXVelocity * t - bXVelocity * t = initialPositionXB - initialPositionXA
             * => t(aXVelocity - bXVelocity) = initialPositionXB - initialPositionXA
             * => t(aXVelocity - bXVelocity) / aXVelocity - bXVelocity = initialPositionXB - initialPositionXA / aXVelocity - bXVelocity
             * => t = initialPositionXB - initialPositionXA / aXVelocity - bXVelocity
             */
            #endregion

            return (initialPositionXB - initialPositionXA) / (aXVelocity - bXVelocity);
        }
    }
}
