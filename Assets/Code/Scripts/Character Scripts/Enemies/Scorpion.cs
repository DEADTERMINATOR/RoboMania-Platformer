﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using MovementEffects;

/* Behaviour:
 * Not Alerted:
 *     - Move and Scan
 * Sees Player:
 *     - Maintains Distance
 *     - Charge Player with Head Spike if too close
 * If Maintaining Distance:
 *     - Charge and Fire at Player
 * 
 * States:
 *     - Move and Scan
 *     - Maintain Distance and Shoot
 *     - Charge Player
 * 
 * Transitions:
 *     - Move and Scan => Maintain Distance and Shoot - Scorpion detectes player
 *     - Maintain Distance and Shoot => Charge Player - Player gets too close and Scorpion can't move further back
 *     - Maintain Distance and Shoot => Move and Scan - Player gets too far away from Scorpion
 *     - Charge Player => Maintain Distance and Shoot
 * 
 * State Behaviours:
 *     - Move and Scan:
 *         - Move forward and turn around if necessary
 *         - Raycast for player
 *     - Maintain Distance and Shoot:
 *         - If player is closer than X distance, tries to move back.
 *         - Might not be able to move back if at a wall, the edge of a platform, or an enemy is behind them.
 *         - Charges shots, aims at the player, and shoots during this phase.
 *     - Charge Player:
 *         - If the scorpion can't move back, or is forced to move back twice, scorpion may enter this state.
 *         - Lowers head and runs at the player.
 *         - Stops if it hits a wall, or the edge of a platform, or another enemy.
*/
namespace Characters.Enemies
{
    public class Scorpion : Enemy
    {
        private SkeletonAnimation _skeletonAnimation;
        private TrackEntry _mainAnimation;

        private SkinnedMeshRenderer _ikTailRenderer;
        private GameObject _tailIK;
        private GameObject _tailIKBasePosition;

        /*
        private float _timer = 0;
        private bool _tailActive = true;
        private bool _activeLerp = false;
        */

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();

            Activated = true;

            State moveAndScan = new State("Move and Scan");
            State maintainDistanceAndShoot = new State("Maintain Distance and Shoot");
            State chargePlayer = new State("Charge Player");

            _skeletonAnimation = GetComponentInChildren<SkeletonAnimation>();
            _ikTailRenderer = CharacterSpriteCollection.GetComponentInChildren<SkinnedMeshRenderer>();
            _tailIK = transform.Find("Character/Tail IK").gameObject;
            _tailIKBasePosition = transform.Find("Character/Tail IK Base Position").gameObject;

            _ikTailRenderer.enabled = false;

            _mainAnimation = _skeletonAnimation.AnimationState.SetAnimation(0, "Walk", true);
            _skeletonAnimation.AnimationState.SetAnimation(1, "Walk (Tail)", true);

            _skeletonAnimation.skeleton.FindSlot("Gun Charge").Attachment = null;
        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();
            /*
            _timer += Time.deltaTime;
            if (_timer >= 5.0f && !_activeLerp)
            {
                _timer = 0;
                _tailActive = !_tailActive;

                if (_tailActive)
                {
                    Timing.RunCoroutine(LerpIKTailBackToBasePosition());
                }
                else
                {
                    _skeletonAnimation.AnimationState.SetEmptyAnimation(1, 0);
                    _skeletonAnimation.skeleton.FindSlot("Tail").Attachment = null;

                    _ikTailRenderer.enabled = true;
                }
            }
            if (!_tailActive)
            {
                _tailIK.transform.position += new Vector3(2.0f, -1.25f, 0) * Time.deltaTime;
            }
            */
            Move();
        }

        /*
        private IEnumerator<float> LerpIKTailBackToBasePosition()
        {
            _activeLerp = true;

            Vector3 currentTailIKPosition = _tailIK.transform.position;
            float percentComplete = 0;

            while (percentComplete < 1f)
            {
                percentComplete += Time.deltaTime / 1.0f;
                _tailIK.transform.position = Vector3.Lerp(currentTailIKPosition, _tailIKBasePosition.transform.position, percentComplete);
                yield return 0f;
            }

            _tailIK.transform.position = _tailIKBasePosition.transform.position;
            _ikTailRenderer.enabled = false;

            _skeletonAnimation.skeleton.FindSlot("Tail").Attachment = _skeletonAnimation.skeleton.GetAttachment("Tail", "Tail (Complete)");
            TrackEntry tailAnimation = _skeletonAnimation.AnimationState.SetAnimation(1, "Walk (Tail)", true);
            tailAnimation.TrackTime = _mainAnimation.TrackTime;

            _activeLerp = false;
            yield return 0f;
        }
        */

        private void Move()
        {
            if ((controller.Collisions.IsCollidingRight && controller.Collisions.Right.CollidingLayer == GlobalData.OBSTACLE_LAYER)
                || (controller.Collisions.IsCollidingLeft && controller.Collisions.Left.CollidingLayer == GlobalData.OBSTACLE_LAYER))
            {
                SetSpriteDirection(controller.Collisions.IsCollidingLeft);
            }

            SetVelocityOnOneAxis('X', MoveSpeed * controller.Collisions.FaceDir);
        }
    }
}
