﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using MovementEffects;
using CompletionState = StateMachineAction.ActionCompletionState;
using ClipperLib;

namespace Characters.Enemies
{
    public class Wasp : Enemy
    {
        //private const float FLAP_VELOCITY = 10f;
        //private const float FLAP_THRESHOLD = -10f;
        //private const float MAX_Y_VELOCITY = 7.5f;
        private const float TIME_BETWEEN_FLAPS_WHEN_TARGET_IS_PRIMARILY_ON_Y = 0.6f;
        private const float TIME_BETWEEN_FLAPS_WHEN_TARGET_IS_PRIMARILY_ON_X = 0.4f;
        private const float TIME_FOR_STINGER_REGROW = 1.5f;

        private const float MAX_DISTANCE_TO_PLAYER_FIRING_ALLOWED = 15.0f;

        private const float DESIRED_Y_DISTANCE_FROM_PLAYER_WHEN_FIRING = 5.0f;
        private const float ALLOWED_PLUS_MINUS_Y_DISTANCE_FROM_PLAYER_WHEN_FIRING = 1.0f;

        private const float DESIRED_X_DISTANCE_FROM_PLAYER_WHEN_FIRING = 5.0f;
        private const float ALLOWED_PLUS_MINUS_X_DISTANCE_FROM_PLAYER_WHEN_FIRING = 2.0f;

        private const float MIN_DAMAGE_THRESHOLD_PERCENTAGE_TO_MOVE = 0.1f;
        private const float MAX_DAMAGE_THRESHOLD_PERCENTAGE_TO_MOVE = 0.2f;

        private const float FOLLOW_MOVE_SPEED_MULTIPLIER = 2.67f;
        /*
         * Wasp moves between top and bottom of its active area.
         * Fires stingers on an interval.
         * If the player moves within its aim zone, it'll aim at where the player is going and shoot there.
        */
        public bool TravelTowardsFirstWaypointFirst;
        public bool FaceStartingDirectionWheneverPossible;
        public float TimeBetweenShots;
        public float StingerFireSpeed;
        public float StingerDamage;
        public AreaTiller PathTilemap;
        public GameObject StingerProjectile;
        public WayPointsComponent waypoints;


        private SkeletonAnimation _skeleton;
        private Bone _bottomBodyBone;
        //private TrackEntry _mainAnimation;
        private TrackEntry _sideAnimation;
        private float _timeSinceLastShot;
        private bool _isFiring;
        private bool _loopFlapAnimation;
        private float _timeUntilNextFlap = TIME_BETWEEN_FLAPS_WHEN_TARGET_IS_PRIMARILY_ON_Y;
        private GameObject _stingerGO;
        private GameObject _firedStinger;
        private bool _needsToRegrowStinger = false;
        private float _timeUntilStingerRegrow = TIME_FOR_STINGER_REGROW;
        private GameObject _sightRaycastOriginPosition;
        private bool _canSeePlayer;
        private float _baseBottomBodyBoneRotation;
        private float _lastBottomBodyBoneRotation;
        private CoroutineHandle _lerpAimingHandle;
        private List<PathTille> _activePath = new List<PathTille>();
        private bool _hasNewPath = false;
        private bool _hasTarget = true;
        private List<Vector2> _averagePlayerVelocitiesOverStingerChargeTime = new List<Vector2>();
        private float _randomDamageThresholdToForceMove;
        private float _lastDamageMoveHealth;

        private float _testTimer = 0;


        protected override void Start()
        {
            base.Start();
            _stillUsingOldStateMachine = false;

            _skeleton = GetComponentInChildren<SkeletonAnimation>();
            _skeleton.AnimationState.SetAnimation(0, "Blink", true);

            _bottomBodyBone = _skeleton.Skeleton.FindBone("Bottom Body");
            _baseBottomBodyBoneRotation = _bottomBodyBone.WorldRotationX;
            if (_baseBottomBodyBoneRotation < 0)
                _baseBottomBodyBoneRotation += 360;

            _stingerGO = transform.Find("Character/Stinger Projectile").gameObject;
            _stingerGO?.SetActive(false);

            _sightRaycastOriginPosition = transform.Find("Sight Origin Point").gameObject;

            /*
            if (Waypoints == null)
            {
                Debug.LogError("Wasp enemy named " + gameObject.name + " does not have a waypoint system");
            }
            else
            {
                WayPointsComponent waypoints = Waypoints as WayPointsComponent;
                TargetVector = TravelTowardsFirstWaypointFirst == true ? waypoints.WayPoints.First : waypoints.WayPoints.Last;
            }
            */

            var airFollow = new FollowPlayerAirState.AirFollowInformation(DESIRED_Y_DISTANCE_FROM_PLAYER_WHEN_FIRING, MAX_DISTANCE_TO_PLAYER_FIRING_ALLOWED, MAX_DISTANCE_TO_PLAYER_FIRING_ALLOWED * 2);
            State test = new State("Test");
            PatrolState patrol = new PatrolState(this, GenericAIState.MovementAxis.Both, waypoints, BaseMoveSpeed, true, MAX_DISTANCE_TO_PLAYER_FIRING_ALLOWED, new PatrolState.PlayerRaycastParameters(true, true));
            FollowPlayerAirState followPlayer = new FollowPlayerAirState(this, ref airFollow, PathTilemap, BaseMoveSpeed * FOLLOW_MOVE_SPEED_MULTIPLIER, true);
            State shoot = new State("Shoot");
            //State trackPlayer = new State("Track Player");
            _behaviourStateMachine = new StateMachine(patrol, true);

            //StateMachineAction move = new FunctionPointerAction(new Func<CompletionState>(MoveWasp));

            StateMachineAction advanceWaypoint = new FunctionPointerAction(new Func<CompletionState>(() =>
            {
                if (Vector2.Distance(TargetVector, transform.position) <= 0.1f || ReadyForNewWaypoint)
                {
                    WayPointsComponent simpleWaypoints = waypoints as WayPointsComponent;
                    TargetVector = simpleWaypoints.WayPoints.Increment();
                    _hasTarget = true;
                    ReadyForNewWaypoint = false;
                }

                return CompletionState.GuaranteedSuccess;
            }));

            StateMachineAction waspReachedTargetDistanceFromPlayer = new FunctionPointerAction(new Func<CompletionState>(() =>
            {
                if (_activePath != null && _activePath.Count >= 1)
                {
                    if (Vector2.Distance(TargetVector, transform.position) <= 0.2f)
                    {
                        _activePath.RemoveAt(0);

                        if (_activePath.Count >= 1)
                        {
                            TargetVector = _activePath[0].WorldPoint;
                            _hasTarget = true;
                        }
                        else
                        {
                            _hasTarget = false;
                        }
                    }
                    else if (_hasNewPath)
                    {
                        TargetVector = _activePath[0].WorldPoint;
                        _hasTarget = true;
                    }

                //VisualDebug.DrawDebugPoint(TargetVector);
            }

                return CompletionState.GuaranteedSuccess;
            }));

            StateMachineAction lookForPlayer = new FunctionPointerAction(new Func<CompletionState>(() =>
            {
                return LookForPlayer();
            }));

            StateMachineAction aimAtPlayer = new FunctionPointerAction(new Func<CompletionState>(() =>
            {
                if (_lerpAimingHandle == null || (_lerpAimingHandle != null && !_lerpAimingHandle.IsRunning))
                {
                    float bottomBodyBoneWorldRotation = _bottomBodyBone.WorldRotationX;
                    if (bottomBodyBoneWorldRotation < 0)
                        bottomBodyBoneWorldRotation += 360;

                    if (_canSeePlayer)
                    {
                    //VisualDebug.DrawDebugPoint(new Vector3(GlobalData.Player.transform.position.x, GlobalData.Player.transform.position.y + Player.Player.PLAYER_VERTICAL_EXTENTS));
                    Vector2 playerDirection = (new Vector3(GlobalData.Player.transform.position.x, GlobalData.Player.transform.position.y + Player.PlayerMaster.PLAYER_VERTICAL_EXTENTS) - _sightRaycastOriginPosition.transform.position).normalized;
                        float angleToPlayer = Vector2.SignedAngle(new Vector2(1, 0), playerDirection);
                        if (angleToPlayer < 0)
                            angleToPlayer += 360;

                    //Debug.Log("Player Angle: " + angleToPlayer);

                    //if (Mathf.Abs(bottomBodyBoneWorldRotation - angleToPlayer) >= 10)
                    //{
                    //_lerpAimingHandle = Timing.RunCoroutine(LerpBottomBodyToAim(bottomBodyBoneWorldRotation, angleToPlayer), "Lerp Aiming");
                    //}
                    //else
                    //{
                    Vector2 boneToPlayerDirection = GlobalData.Player.transform.position - _bottomBodyBone.GetWorldPosition(transform);
                        Vector3 localDirection = transform.InverseTransformDirection(boneToPlayerDirection);
                        float newRotation = Mathf.Atan2(localDirection.y, localDirection.x) * Mathf.Rad2Deg;
                        if (Controller.Collisions.FaceDir == 1)
                        {
                            float angleToDown = Vector2.Angle(Vector2.down, boneToPlayerDirection);
                            Debug.Log(angleToDown);
                            _bottomBodyBone.Rotation = newRotation - angleToDown * 2;
                        }
                        else
                            _bottomBodyBone.Rotation = newRotation;
                    //Debug.Log("World Rot: " + newRotation);
                    _lastBottomBodyBoneRotation = _bottomBodyBone.Rotation;
                    //}
                }
                    else if (!StaticTools.ThresholdApproximately(bottomBodyBoneWorldRotation, _baseBottomBodyBoneRotation, 0.5f))
                    {
                        _lerpAimingHandle = Timing.RunCoroutine(LerpBottomBodyToAim(bottomBodyBoneWorldRotation, _baseBottomBodyBoneRotation), "Lerp Aiming");
                    }
                }
                return CompletionState.GuaranteedSuccess;
            }));

            StateMachineAction maintainDistanceFromPlayer = new FunctionPointerAction(new Func<CompletionState>(MaintainDistanceFromPlayer));

            StateMachineAction addTimeAndPrepProjectile = new FunctionPointerAction(new Func<CompletionState>(() =>
            {
                if (!_isFiring && !_needsToRegrowStinger)
                {
                    _timeSinceLastShot += Time.deltaTime;
                    if (_timeSinceLastShot >= TimeBetweenShots)
                    {
                        _isFiring = true;
                        for (int i = 0; i < 10; ++i)
                        {
                            _sideAnimation = _skeleton.AnimationState.AddAnimation(2, "Charge Stinger", false, 0);
                        }

                    //_stingerGO.transform.localRotation = Quaternion.Euler(0, 0, _bottomBodyBone.Rotation);
                    //_stingerGO.transform.Find("Stinger Mesh").transform.localRotation = Quaternion.Euler(0, 0, -_bottomBodyBone.Rotation);
                    //_firedStinger = Instantiate(_stingerGO, new Vector2(-9999, -9999), _stingerGO.transform.rotation);
                    //_firedStinger.transform.localScale = transform.localScale;
                    //_firedStinger.SetActive(true);
                    //WaspStinger stingerPro = _firedStinger.GetComponent<WaspStinger>();
                    //stingerPro.SetOwner(gameObject);
                    //_firedStinger.transform.Find("Stinger Mesh").GetComponent<MeshRenderer>().enabled = false;

                    _timeSinceLastShot = 0;
                    }
                }
                return CompletionState.GuaranteedSuccess;
            }));

            StateMachineAction checkIfReadyForFireAndFire = new FunctionPointerAction(new Func<CompletionState>(() =>
            {
                if (_isFiring)
                {
                    _averagePlayerVelocitiesOverStingerChargeTime.Add(GlobalData.Player.Velocity);
                    if (_sideAnimation.TrackTime >= _sideAnimation.AnimationEnd)
                    {
                        _sideAnimation = _skeleton.AnimationState.SetAnimation(2, "Fire Stinger", true);

                        Transform stingerFirePosition = transform.Find("Character/Stinger Fire Position");
                        _firedStinger = Instantiate(_stingerGO, stingerFirePosition.position, Quaternion.Euler(0, 0, _bottomBodyBone.Rotation));
                        _firedStinger.SetActive(true);

                        Transform stingerMesh = _firedStinger.transform.Find("Stinger Mesh");
                        WaspStinger stingerPro = _firedStinger.GetComponent<WaspStinger>();

                        stingerMesh.transform.localRotation = Quaternion.Euler(0, 0, -_bottomBodyBone.Rotation);
                        _firedStinger.transform.localScale = transform.localScale;

                        stingerPro.ProjectileSpeed = StingerFireSpeed;
                        stingerPro.Fire(stingerPro.transform.right, StingerDamage, gameObject);

                        Vector2 totalPlayerVelocity = Vector2.zero;
                        foreach (Vector2 velocity in _averagePlayerVelocitiesOverStingerChargeTime)
                        {
                            totalPlayerVelocity += velocity;
                        }
                        Vector2 averagePlayerVelocity = totalPlayerVelocity / _averagePlayerVelocitiesOverStingerChargeTime.Count;

                        Vector2 predictedCollideWithPlayerPosition = StaticTools.InterceptTarget(_firedStinger.transform.position, StingerFireSpeed, GlobalData.Player.transform.position, averagePlayerVelocity);
                        Vector2 stingerAdjustVector = (predictedCollideWithPlayerPosition - (Vector2)stingerPro.transform.position).normalized;
                        float stingerAdjustAngle = Vector2.SignedAngle(new Vector2(1, 0), stingerAdjustVector);
                        _bottomBodyBone.Rotation = _bottomBodyBone.WorldToLocalRotation(stingerAdjustAngle);

                        Ray2D ray = new Ray2D(stingerPro.transform.position, stingerAdjustVector);
                        Debug.DrawRay(ray.origin, ray.direction * 10f, Color.cyan, 2.5f);

                        stingerPro.transform.rotation = Quaternion.Euler(0, 0, _bottomBodyBone.Rotation);
                        stingerMesh.transform.localRotation = Quaternion.Euler(0, 0, -_bottomBodyBone.Rotation);
                        VisualDebug.DrawDebugPoint(predictedCollideWithPlayerPosition, Color.red, 2.5f);

                        _isFiring = false;
                        _needsToRegrowStinger = true;
                        _averagePlayerVelocitiesOverStingerChargeTime.Clear();
                    }
                }

                return CompletionState.GuaranteedSuccess;
            }));

            StateMachineAction regrowStinger = new FunctionPointerAction(new Func<CompletionState>(() =>
            {
                if (_needsToRegrowStinger)
                {
                    _timeUntilStingerRegrow -= Time.deltaTime;
                    if (_timeUntilStingerRegrow <= 0)
                    {
                        _sideAnimation = _skeleton.AnimationState.SetAnimation(2, "Regrow Stinger", false);
                        _needsToRegrowStinger = false;
                        _timeUntilStingerRegrow = TIME_FOR_STINGER_REGROW;
                    }
                }
                return CompletionState.GuaranteedSuccess;
            }));

            StateMachineAction lookAtPlayer = new FunctionPointerAction(new Func<CompletionState>(() =>
            {
                var directionToPlayer = GlobalData.Player.transform.position - transform.position;
                var facingDirectionToPlayer = Mathf.Sign(directionToPlayer.x);
                if (facingDirectionToPlayer != Collisions.FaceDir)
                {
                    SetSpriteDirection(Collisions.FaceDir == -1 ? true : false);
                }

                return CompletionState.GuaranteedSuccess;
            }), "Look At Player");

            Condition waspCanSeePlayer = new FunctionPointerCondition(new Func<bool>(() => { return patrol.CanSeePlayer(); }));
            Condition waspStopFollowingPlayer = new FunctionPointerCondition(new Func<bool>(() => { return followPlayer.StopFollowingPlayer(); }));
            Condition waspInAttackPosition = new FunctionPointerCondition(new Func<bool>(() => { return followPlayer.InPositionToAttack(); }));

            /*
            patrol.AddEntryAction(new FunctionPointerAction(new Func<CompletionState>(() => { MoveSpeed = BaseMoveSpeed; ReadyForNewWaypoint = true; return CompletionState.GuaranteedSuccess; })));
            patrol.AddStateAction(move);
            patrol.AddStateAction(advanceWaypoint);
            patrol.AddStateAction(lookForPlayer);
            */
            patrol.AddEntryAction(new FunctionPointerAction(() => { patrol.NPCAllowedToChangeFacingDirection = true; return CompletionState.GuaranteedSuccess; }));
            patrol.AddStateAction(ManageWingFlap, "Manage Wing Flap");
            patrol.AddTransition(new Transition(followPlayer, waspCanSeePlayer));

            followPlayer.AddEntryAction(new FunctionPointerAction(() => { followPlayer.NPCAllowedToChangeFacingDirection = false; return CompletionState.GuaranteedSuccess; }));
            followPlayer.AddStateAction(ManageWingFlap, "Manage Wing Flap");
            followPlayer.AddStateAction(lookAtPlayer);
            followPlayer.AddTransition(new Transition(patrol, waspStopFollowingPlayer));
            //followPlayer.AddTransition(new Transition(shoot, waspInAttackPosition));

            //shoot.AddStateAction(aimAtPlayer);
            //shoot.AddStateAction(addTimeAndPrepProjectile);
            //shoot.AddStateAction(checkIfReadyForFireAndFire);
            //shoot.AddStateAction(regrowStinger);
            /*
            trackPlayer.AddEntryAction(new FunctionPointerAction(new Func<CompletionState>(() => { MoveSpeed = BaseMoveSpeed * 1.67f; MaintainDistanceFromPlayer(); return CompletionState.GuaranteedSuccess; })));
            trackPlayer.AddStateAction(move);
            trackPlayer.AddStateAction(waspReachedTargetDistanceFromPlayer);
            trackPlayer.AddStateAction(lookForPlayer);
            trackPlayer.AddStateAction(maintainDistanceFromPlayer);
            trackPlayer.AddStateAction(aimAtPlayer);
            trackPlayer.AddStateAction(addTimeAndPrepProjectile);
            trackPlayer.AddStateAction(checkIfReadyForFireAndFire);
            trackPlayer.AddStateAction(regrowStinger);
            trackPlayer.AddTransition(new Transition(patrol, new NotCondition(waspCanSeePlayer)));
            */

            BehaviourStateMachine.AddState(patrol);
            BehaviourStateMachine.AddState(followPlayer);

            //test.AddStateAction(CalculateAirPathToPlayer);
            //test.AddStateAction(new FunctionPointerAction(() => { _testTimer += Time.deltaTime; return CompletionState.GuaranteedSuccess; }));
        }

        protected override void Update()
        {
            if (!frozen)
            {
                base.Update();

                if (Mathf.Abs(Velocity.x) < 0.25f && FaceStartingDirectionWheneverPossible)
                {
                    SetSpriteDirection(StartFaceDir == StartingFacingDirection.Right ? true : false);
                }
            }
        }

        public override void TakeDamage(IDamageGiver giver, float amount, GlobalData.DamageType type, Vector3 hitPoint)
        {
            Vector3 directionToPlayer = GlobalData.Player.transform.position - transform.position;
            SetSpriteDirection(directionToPlayer.x > 0 ? true : false);

            var success = LookForPlayer();
            _canSeePlayer = success == CompletionState.Successful ? true : false;
            if (_canSeePlayer)
            {
                float currentDamagePercentageSinceLastMove = (_lastDamageMoveHealth - Health) / MaxHealth;
                if (currentDamagePercentageSinceLastMove >= _randomDamageThresholdToForceMove)
                {
                    _randomDamageThresholdToForceMove = UnityEngine.Random.Range(MIN_DAMAGE_THRESHOLD_PERCENTAGE_TO_MOVE, MAX_DAMAGE_THRESHOLD_PERCENTAGE_TO_MOVE);
                    _lastDamageMoveHealth = Health;
                }
            }

            base.TakeDamage(giver, amount, type, hitPoint);
        }

        #region Behaviours
        /*
        private CompletionState MoveWasp()
        {
            if (_hasTarget)
            {
                Vector2 targetDirection = (TargetVector - transform.position).normalized;
                //Debug.Log(TargetVector.x);

                _velocity = targetDirection * MoveSpeed;

                if ((targetDirection.x == 0 && targetDirection.y < 0) || (targetDirection.x != 0 && targetDirection.y <= -0.6f)) //The travel direction of the wasp is primarily downward.
                {
                    if (_loopFlapAnimation)
                        EndLoopFlap();

                    _timeUntilNextFlap -= Time.deltaTime;
                    if (_timeUntilNextFlap <= 0)
                        FlapOnce(TIME_BETWEEN_FLAPS_WHEN_TARGET_IS_PRIMARILY_ON_Y);
                }
                else if (targetDirection.y == 0 || (targetDirection.y != 0 && targetDirection.x <= -0.6f)) //The travel direction of the wasp is primarily to the right or left.
                {
                    if (_loopFlapAnimation)
                        EndLoopFlap();

                    _timeUntilNextFlap -= Time.deltaTime;
                    if (_timeUntilNextFlap <= 0)
                        FlapOnce(TIME_BETWEEN_FLAPS_WHEN_TARGET_IS_PRIMARILY_ON_X);
                }
                else //The travel direction of the wasp is primarily upward.
                {
                    if (!_loopFlapAnimation)
                        LoopFlap();
                }

                if (_velocity.x < -0.25f)
                {
                    _movementInput.x = -1;
                    if (!_canSeePlayer)
                        SetSpriteDirection(false);
                }
                else if (_velocity.x > 0.25f)
                {
                    _movementInput.x = 1;
                    if (!_canSeePlayer)
                        SetSpriteDirection(true);
                }
                else
                {
                    _movementInput.x = 0;
                    if (FaceStartingDirectionWheneverPossible)
                        SetSpriteDirection(StartingDirection == StartingFacingDirection.Right ? true : false);
                }

                if (_velocity.x < 0)
                    _movementInput.y = -1;
                else if (_velocity.y > 0)
                    _movementInput.y = 1;
                else
                    _movementInput.y = 0;
            }
            else
            {
                _velocity = Vector3.zero;

                if (_loopFlapAnimation)
                    EndLoopFlap();

                _timeUntilNextFlap -= Time.deltaTime;
                if (_timeUntilNextFlap <= 0)
                    FlapOnce(TIME_BETWEEN_FLAPS_WHEN_TARGET_IS_PRIMARILY_ON_Y);
            }

            return CompletionState.Successful;
        }
        */

        /// <summary>
        /// Handles the wing flapping animation of the wasp based on the direction of the wasp's velocity.
        /// </summary>
        /// <returns></returns>
        private CompletionState ManageWingFlap()
        {
            if (_hasTarget)
            {
                Vector2 targetDirection = (TargetVector - transform.position).normalized;

                if ((targetDirection.x == 0 && targetDirection.y < 0) || (targetDirection.x != 0 && targetDirection.y <= -0.6f)) //The travel direction of the wasp is primarily downward.
                {
                    if (_loopFlapAnimation)
                    {
                        EndLoopFlap();
                    }

                    _timeUntilNextFlap -= Time.deltaTime;
                    if (_timeUntilNextFlap <= 0)
                    {
                        FlapOnce(TIME_BETWEEN_FLAPS_WHEN_TARGET_IS_PRIMARILY_ON_Y);
                    }
                }
                else if (targetDirection.y == 0 || (targetDirection.y != 0 && targetDirection.x <= -0.6f)) //The travel direction of the wasp is primarily to the right or left.
                {
                    if (_loopFlapAnimation)
                    {
                        EndLoopFlap();
                    }

                    _timeUntilNextFlap -= Time.deltaTime;
                    if (_timeUntilNextFlap <= 0)
                    {
                        FlapOnce(TIME_BETWEEN_FLAPS_WHEN_TARGET_IS_PRIMARILY_ON_X);
                    }
                }
                else //The travel direction of the wasp is primarily upward.
                {
                    if (!_loopFlapAnimation)
                    {
                        LoopFlap();
                    }
                }
            }
            else
            {
                if (_loopFlapAnimation)
                {
                    EndLoopFlap();
                }

                _timeUntilNextFlap -= Time.deltaTime;
                if (_timeUntilNextFlap <= 0)
                {
                    FlapOnce(TIME_BETWEEN_FLAPS_WHEN_TARGET_IS_PRIMARILY_ON_Y);
                }
            }

            return CompletionState.Successful;
        }

        private CompletionState LookForPlayer()
        {
            //If the wasp has already seen the player, increase the distance it can continue spotting the player at to increase the likelihood the wasp will follow.
            float raycastDistance = _canSeePlayer == true ? MAX_DISTANCE_TO_PLAYER_FIRING_ALLOWED * 2f : MAX_DISTANCE_TO_PLAYER_FIRING_ALLOWED;

            Vector2 directionToPlayer = (GlobalData.Player.transform.position - _sightRaycastOriginPosition.transform.position).normalized;
            Ray2D rayToPlayer = new Ray2D(_sightRaycastOriginPosition.transform.position, directionToPlayer);
            Debug.DrawRay(rayToPlayer.origin, rayToPlayer.direction * raycastDistance, Color.magenta, 0.25f);
            RaycastHit2D raycastToPlayer = Physics2D.Raycast(rayToPlayer.origin, rayToPlayer.direction, raycastDistance, GlobalData.PLAYER_LAYER_SHIFTED | GlobalData.OBSTACLE_LAYER_SHIFTED);

            if (raycastToPlayer.collider != null && raycastToPlayer.collider.gameObject.layer == GlobalData.PLAYER_LAYER && Mathf.Sign(directionToPlayer.x) == Controller.Collisions.FaceDir)
            {
                return CompletionState.Successful;
            }

            return CompletionState.Failed;
        }

        private CompletionState MaintainDistanceFromPlayer()
        {
            if (_canSeePlayer && (_activePath == null || (_activePath != null && _activePath.Count == 0)) && PathTilemap.Bounds.Contains(PathTilleMap.Vector2ToVector2Int(GlobalData.Player.transform.position)))
            {
                bool needsAdjustmentOnX = false;
                bool needsAdjustmentOnY = false;

                float xDistanceFromPlayer = Mathf.Abs(transform.position.x - GlobalData.Player.transform.position.x);
                float yDistanceFromPlayer = Mathf.Abs(transform.position.y - GlobalData.Player.transform.position.y);

                if (xDistanceFromPlayer < DESIRED_X_DISTANCE_FROM_PLAYER_WHEN_FIRING - ALLOWED_PLUS_MINUS_X_DISTANCE_FROM_PLAYER_WHEN_FIRING || xDistanceFromPlayer > DESIRED_X_DISTANCE_FROM_PLAYER_WHEN_FIRING + ALLOWED_PLUS_MINUS_X_DISTANCE_FROM_PLAYER_WHEN_FIRING)
                    needsAdjustmentOnX = true;
                if (yDistanceFromPlayer < DESIRED_Y_DISTANCE_FROM_PLAYER_WHEN_FIRING - ALLOWED_PLUS_MINUS_Y_DISTANCE_FROM_PLAYER_WHEN_FIRING || yDistanceFromPlayer > DESIRED_Y_DISTANCE_FROM_PLAYER_WHEN_FIRING + ALLOWED_PLUS_MINUS_Y_DISTANCE_FROM_PLAYER_WHEN_FIRING)
                    needsAdjustmentOnY = true;

                Vector2Int newTargetVector = new Vector2Int(-9999, -9999);
                if (needsAdjustmentOnX || needsAdjustmentOnY)
                {
                    newTargetVector = PathTilleMap.Vector2ToVector2Int(ChooseRandomPointWithinDesiredAttackArea());
                    /*
                    if (needsAdjustmentOnX)
                    {
                        int possibleNewXPosition = transform.position.x < GlobalData.Player.transform.position.x ? (int)(Mathf.Floor(GlobalData.Player.transform.position.x) - DESIRED_X_DISTANCE_FROM_PLAYER_WHEN_FIRING) : (int)(Mathf.Ceil(GlobalData.Player.transform.position.x) + DESIRED_X_DISTANCE_FROM_PLAYER_WHEN_FIRING);
                        if (possibleNewXPosition < PathTilemap.Bounds.xMin || possibleNewXPosition > PathTilemap.Bounds.xMax) //Check if this desired new X position is within the Wasp's tilemap. If it's not, set the desired number of units in the opposite direction.
                            possibleNewXPosition = transform.position.x < GlobalData.Player.transform.position.x ? (int)(Mathf.Ceil(GlobalData.Player.transform.position.x) + DESIRED_X_DISTANCE_FROM_PLAYER_WHEN_FIRING) : (int)(Mathf.Floor(GlobalData.Player.transform.position.x) - DESIRED_X_DISTANCE_FROM_PLAYER_WHEN_FIRING);

                        newTargetVector.x = possibleNewXPosition;
                    }
                    if (needsAdjustmentOnY)
                    {
                        int possibleNewYPosition = (int)(Mathf.Ceil(GlobalData.Player.transform.position.y) + DESIRED_Y_DISTANCE_FROM_PLAYER_WHEN_FIRING);
                        if (possibleNewYPosition > PathTilemap.Bounds.yMax)
                            possibleNewYPosition = PathTilemap.Bounds.yMax;

                        newTargetVector.y = possibleNewYPosition;
                    }
                    */
                }

                if (newTargetVector != new Vector2(-9999, -9999))
                {
                    /*
                    if (newTargetVector.x == -9999)
                        newTargetVector.x = Mathf.RoundToInt(transform.position.x);
                    else if (newTargetVector.y == -9999)
                        newTargetVector.y = Mathf.RoundToInt(transform.position.y);
                    */

                    //VisualDebug.DrawDebugPoint(new Vector3(newTargetVector.x, newTargetVector.y, 0), Color.green);
                    //Reset the damage threshold move values so that the accumulating values don't force another move shortly after.
                    _randomDamageThresholdToForceMove = UnityEngine.Random.Range(MIN_DAMAGE_THRESHOLD_PERCENTAGE_TO_MOVE, MAX_DAMAGE_THRESHOLD_PERCENTAGE_TO_MOVE);
                    _lastDamageMoveHealth = Health;

                    _activePath = PathTilemap.Map.SortestOrCloestsPath(new Vector2Int((int)transform.position.x, (int)transform.position.y), newTargetVector, true);
                    //foreach (PathTille tile in _activePath)
                    //VisualDebug.DrawDebugPoint(tile.WorldPoint, Color.blue);

                    _hasNewPath = true;
                }
            }

            return CompletionState.Successful;
        }

        private Vector2 ChooseRandomPointWithinDesiredAttackArea()
        {
            Bounds desiredAttackArea = new Bounds(new Vector3(GlobalData.Player.transform.position.x, GlobalData.Player.transform.position.y + DESIRED_Y_DISTANCE_FROM_PLAYER_WHEN_FIRING), new Vector3((DESIRED_X_DISTANCE_FROM_PLAYER_WHEN_FIRING + ALLOWED_PLUS_MINUS_X_DISTANCE_FROM_PLAYER_WHEN_FIRING) * 2, ALLOWED_PLUS_MINUS_Y_DISTANCE_FROM_PLAYER_WHEN_FIRING * 2));
            float randomXValue = UnityEngine.Random.Range(desiredAttackArea.min.x, desiredAttackArea.max.x);
            float randomYValue = UnityEngine.Random.Range(desiredAttackArea.min.y, desiredAttackArea.max.y);

            VisualDebug.DrawBox(desiredAttackArea.center, desiredAttackArea.extents, Quaternion.identity, Color.green, 0.75f);
            VisualDebug.DrawDebugPoint(new Vector3(randomXValue, randomYValue), Color.blue, 0.75f);

            return new Vector2(randomXValue, randomYValue);
        }
        #endregion

        #region Flap Methods
        private void FlapOnce(float timeUntilNextFlap)
        {
            _skeleton.AnimationState.SetAnimation(1, "Flap", false);
            _timeUntilNextFlap = timeUntilNextFlap;
        }

        private void LoopFlap()
        {
            _skeleton.AnimationState.SetAnimation(1, "Flap", true);
            _loopFlapAnimation = true;
        }

        private void EndLoopFlap()
        {
            _skeleton.AnimationState.SetEmptyAnimation(1, 0);
            _loopFlapAnimation = false;
        }

        /*
        private void AddFlapVelocity()
        {
            _velocity.y += FLAP_VELOCITY;
            if (_velocity.y > MAX_Y_VELOCITY)
                _velocity.y = MAX_Y_VELOCITY;
        }

        private void LoopFlapCallback(TrackEntry track)
        {
            Debug.Log("Flap Loop Continuation");
            _skeleton.AnimationState.SetAnimation(1, "Flap", false);
        }
        */
        #endregion

        private IEnumerator<float> LerpBottomBodyToAim(float startingRotation, float endingRotation)
        {
            float percentComplete = 0;

            if (startingRotation < 0)
                startingRotation += 360;
            if (endingRotation < 0)
                endingRotation += 360;

            while (percentComplete < 1f)
            {
                percentComplete += Time.deltaTime / 0.35f;
                _bottomBodyBone.Rotation = _bottomBodyBone.WorldToLocalRotation(Mathf.Lerp(startingRotation, endingRotation, percentComplete));
                _lastBottomBodyBoneRotation = _bottomBodyBone.Rotation;
                yield return 0f;
            }
        }
    }
}
