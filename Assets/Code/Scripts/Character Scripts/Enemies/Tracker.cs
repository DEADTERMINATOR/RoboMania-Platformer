﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Characters.Sound;

namespace Characters.Enemies
{
    public class Tracker : Enemy
    {
        /// <summary>
        /// The maximum amount of time that can elapse between shots.
        /// </summary>
        public float TimeBetweenShots;

        /// <summary>
        /// The amount of time on the Tracker's shoot timer that the tracker will begin with.
        /// Used to stagger sequential Trackers.
        /// </summary>
        public float StartingTime;

        /// <summary>
        /// Whether the player should be tracked (followed) on the Y-Axis.
        /// If this is false, then the player will be tracked on the X-Axis
        /// </summary>
        public bool TrackOnYAxis;

        /// <summary>
        /// Whether the tracker has just fired its shot.
        /// </summary>
        [HideInInspector]
        public bool Fired;

        public TrackerSounds TrackerSounds;

        public SkeletonAnimation Skeleton;


        /// <summary>
        /// The amount of time that has elapsed since the last shot was fired.
        /// </summary>
        private float _timeSinceLastShot;

        /// <summary>
        /// The amount of time the tracker should gloat (i.e. do nothing) after killing the player.
        /// </summary>
        private float _gloatTime = 4.0f;


        protected override void Start()
        {
            base.Start();

            Controller.Collisions.FaceDir = 1;

            _timeSinceLastShot = StartingTime;
            Fired = false;

            CreateNonMovingTrackerStateMachine();

            Activated = false;
            GravityScale = 0;

            Skeleton = GetComponent<SkeletonAnimation>();

            /*
            if (GlobalWaypoints.Length == 0)
            {
                CreateNonMovingTrackerStateMachine();
            }
            else if (!MemberOfNPCGroup)
            {
                CreateMovingTrackerStateMachine();
            }
            */
        }


        /// <summary>
        /// Creates the state machine for the tracker if this tracker moves between waypoints and tracks the player.
        /// </summary>
        private void CreateMovingTrackerStateMachine()
        {
            /*
            //Setup the state machine.
            //Create a copy of the tracker to pass around as a reference.
            NPC tracker = this;
            Tracker castedTracker = this;
            Player.PlayerMaster player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player.PlayerMaster>();
            BoxCollider2D activeArea = transform.parent.Find("Active Area").GetComponent<BoxCollider2D>();

            TrackerSounds = new TrackerSounds(this);

            //Create states.
            State patrol = new State("Patrol");
            State track = new State("Track");
            State fire = new State("Fire");
            State baskInGlory = new State("Bask in Glory"); //The tracker has killed the player, so it celebrates it's victory by doing nothing.

            //Create the state machine.
            StateMachine = new NPCStateMachine(patrol, this);

            //Create actions.
            StateMachineAction setWaypoint = new SetWaypoint(ref tracker);
            StateMachineAction moveTowards = new MoveTowardsOnOneAxis(ref tracker, 0.05f, TrackOnYAxis);
            StateMachineAction trackPlayer = new TrackPlayerOnOneAxis(ref tracker, ref player, TrackOnYAxis);
            StateMachineAction killVelocity = new KillVelocity(ref tracker);
            StateMachineAction fireLaserShot = new FireLaserShot(ref castedTracker);
            StateMachineAction clearFiringState = new SetFiringState(ref castedTracker, false);
            StateMachineAction addTime;
            StateMachineAction clearTime;
            unsafe
            {
                fixed (float* time = &_timeSinceLastShot)
                {
                    addTime = new AddTime(time);
                    clearTime = new ClearTime(time);
                }
            }

            //Create conditions.
            Condition playerInActiveArea = new PlayerInActiveArea(ref player, ref activeArea, true);
            Condition playerNotInActiveArea = new PlayerInActiveArea(ref player, ref activeArea, false);
            Condition playerIsDead = new PlayerIsDead(ref player);
            Condition checkIfFired = new CheckIfFired(ref castedTracker);
            Condition checkIfFiredAndInActiveArea = new AndCondition(playerInActiveArea, checkIfFired);
            Condition timeExceeded;
            Condition gloatTimeExceeded;
            unsafe
            {
                fixed (float* time = &_timeSinceLastShot)
                {
                    timeExceeded = new TimeExceeded(time, TimeBetweenShots);
                    gloatTimeExceeded = new TimeExceeded(time, _gloatTime);
                }
            }

            //Create transitions.
            Transition patrolToTrack = new Transition(track, playerInActiveArea);
            Transition trackToPatrol = new Transition(patrol, playerNotInActiveArea);
            Transition trackToFire = new Transition(fire, timeExceeded);
            Transition fireToTrack = new Transition(track, checkIfFiredAndInActiveArea);
            Transition fireToPatrol = new Transition(patrol, playerNotInActiveArea);
            Transition fireToBask = new Transition(baskInGlory, playerIsDead);
            Transition baskToPatrol = new Transition(patrol, gloatTimeExceeded);

            //Add actions to transitions.
            fireToBask.AddActionToPerform(killVelocity);

            //Add actions and transitions to states.
            patrol.AddStateAction(setWaypoint);
            patrol.AddStateAction(moveTowards);

            patrol.AddTransition(patrolToTrack);

            track.AddStateAction(trackPlayer);
            track.AddStateAction(moveTowards);
            track.AddStateAction(addTime);

            track.AddExitAction(clearTime);

            track.AddTransition(trackToPatrol);
            track.AddTransition(trackToFire);

            fire.AddStateAction(trackPlayer);
            fire.AddStateAction(moveTowards);
            fire.AddStateAction(fireLaserShot);

            fire.AddExitAction(clearFiringState);

            fire.AddTransition(fireToTrack);
            fire.AddTransition(fireToPatrol);
            fire.AddTransition(fireToBask);

            baskInGlory.AddEntryAction(clearTime);
            baskInGlory.AddStateAction(addTime);
            baskInGlory.AddExitAction(clearTime);

            baskInGlory.AddTransition(baskToPatrol);

            //Add remaining states to state machine.
            StateMachine.AddState(track);
            StateMachine.AddState(fire);
            StateMachine.AddState(baskInGlory);
            */
        }

        /// <summary>
        /// Creates the state machine for the tracker if this tracker does not move and only fires in its spot.
        /// </summary>
        private void CreateNonMovingTrackerStateMachine()
        {
            /*
            //Setup the state machine.
            //Create a copy of the tracker to pass around as a reference.
            NPC tracker = this;
            Tracker castedTracker = this;
            Player.PlayerMaster player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player.PlayerMaster>();

            TrackerSounds = new TrackerSounds(this);

            //Create states.
            State buildTime = new State("Build Time");
            State fire = new State("Fire");
            State baskInGlory = new State("Bask in Glory"); //The tracker has killed the player, so it celebrates it's victory by doing nothing.

            //Create the state machine.
            StateMachine = new NPCStateMachine(buildTime, this);

            //Create actions.
            StateMachineAction fireLaserShot = new FireLaserShot(ref castedTracker);
            StateMachineAction clearFiringState = new SetFiringState(ref castedTracker, false);
            StateMachineAction addTime = null;
            StateMachineAction clearTime = null;
            if (!MemberOfNPCGroup)
            {
                unsafe
                {
                    fixed (float* time = &_timeSinceLastShot)
                    {
                        addTime = new AddTime(time);
                        clearTime = new ClearTime(time);
                    }
                }
            }

            //Create conditions.
            Condition playerIsDead = new PlayerIsDead(ref player);
            Condition checkIfFired = new CheckIfFired(ref castedTracker);
            Condition timeExceeded;
            if (!MemberOfNPCGroup)
            {
                unsafe
                {
                    fixed (float* time = &_timeSinceLastShot)
                    {
                        timeExceeded = new TimeExceeded(time, TimeBetweenShots);
                    }
                }
            }
            else
            {
                TimedNPCGroup group = GetComponentInParent<TimedNPCGroup>();
                unsafe
                {
                    fixed (float* time = &group.CurrentTime)
                    {
                        timeExceeded = new TimeExceeded(time, TimeBetweenShots);
                    }
                }
            }

            //Create transitions.
            Transition buildToFire = new Transition(fire, timeExceeded);
            Transition fireToBuild = new Transition(buildTime, checkIfFired);
            Transition fireToBask = new Transition(baskInGlory, playerIsDead);

            //Add actions and transitions to states.
            if (!MemberOfNPCGroup)
            {
                buildTime.AddStateAction(addTime);
            }

            buildTime.AddTransition(buildToFire);

            if (!MemberOfNPCGroup)
            {
                fire.AddEntryAction(clearTime);
            }
            fire.AddStateAction(fireLaserShot);
            fire.AddExitAction(clearFiringState);

            fire.AddTransition(fireToBuild);
            fire.AddTransition(fireToBask);

            //Add remaining states to state machine.
            StateMachine.AddState(fire);
            StateMachine.AddState(baskInGlory);
            */
        }

        public override void Deactivate(GameObject deactivator)
        {
            base.Deactivate(deactivator);

            BehaviourStateMachine.Active = true;
            BehaviourStateMachine.RunUpdate(); //Run one last update; some of the actions have cleanup to do if the tracker is no longer activated.
            BehaviourStateMachine.Active = false;
        }
    }
}
