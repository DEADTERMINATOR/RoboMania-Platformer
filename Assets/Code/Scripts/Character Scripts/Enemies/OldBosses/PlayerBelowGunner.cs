﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Enemies.Bosses;

public class PlayerBelowGunner : MonoBehaviour
{
    /// <summary>
    /// The gunner the collider is attached to.
    /// </summary>
    private GunnerBoss _gunner;

	//Use this for initialization
	private void Start()
    {
        _gunner = transform.parent.GetComponentInChildren<GunnerBoss>();
	}
	
	//Update is called once per frame
	private void Update()
    {
        transform.position = _gunner.transform.position;
	}


    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            _gunner.PlayerBelowGunner = true;
            
            if (_gunner.CurrentPhase == GunnerBoss.Phase.WALL)
            {
                _gunner.Attacks.DisableAttack(_gunner.BouncingGrenadeAttack);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.PLAYER_LAYER)
        {
            _gunner.PlayerBelowGunner = false;
            _gunner.Attacks.EnableAttack(_gunner.BouncingGrenadeAttack);
        }
    }
}
