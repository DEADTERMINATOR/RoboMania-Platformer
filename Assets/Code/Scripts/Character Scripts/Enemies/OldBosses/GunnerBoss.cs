﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;
using Characters.Enemies.AnimationControllers;
using static GlobalData;

#pragma warning disable CS0649

namespace Characters.Enemies.Bosses
{
    public class GunnerBoss : Enemy
    {
        #region STRUCTS
        public struct Attack
        {
            /// <summary>
            /// The char that represents the name of the attack.
            /// </summary>
            public char AttackChar;

            /// <summary>
            /// The maximum number of times in a row the gunner can use this attack.
            /// </summary>
            public int MaxConsecutiveAllowed;

            /// <summary>
            /// The transition that leads to this attack if it's used on the wall.
            /// </summary>
            public WeightedTransition AssociatedTransitionWall;

            /// <summary>
            /// The transition that leads to this attack if it's used on the ceiling.
            /// </summary>
            public WeightedTransition AssociatedTransitionCeiling;

            /// <summary>
            /// The transition that leads to this attack if it's used on the ground.
            /// </summary>
            public WeightedTransition AssociatedTransitionGround;


            /// <summary>
            /// Creates an instance of this struct which represents an attack the gunner can perform.
            /// </summary>
            /// <param name="AttackChar">The char representing the name of the attack.</param>
            /// <param name="MaxConsecutiveAllowed">The maximum number of consecutive times this attack can be used.</param>
            /// <param name="AssociatedTransition">The transition that leads to this attack.</param>
            public Attack(char AttackChar, int MaxConsecutiveAllowed, ref WeightedTransition AssociatedTransitionWall, ref WeightedTransition AssociatedTransitionCeiling, ref WeightedTransition AssociatedTransitionGround)
            {
                this.AttackChar = AttackChar;
                this.MaxConsecutiveAllowed = MaxConsecutiveAllowed;
                this.AssociatedTransitionWall = AssociatedTransitionWall;
                this.AssociatedTransitionCeiling = AssociatedTransitionCeiling;
                this.AssociatedTransitionGround = AssociatedTransitionGround;
            }
        }

        public struct AttackTracker
        {
            /// <summary>
            /// The list of attacks the gunner has performed so far.
            /// </summary>
            public List<Attack> AttacksPerformed;

            /// <summary>
            /// The number of times in a row the same attack has been performed.
            /// </summary>
            public int NumTimesSameAttackPerformed;


            /// <summary>
            /// Adds an attack to the list of attacks the gunner has performed.
            /// Also checks if the gunner has performed the same attack too many times in a row.
            /// </summary>
            /// <param name="attack">The attack the gunner just performed.</param>
            public void AddAttack(Attack attack, ref GunnerBoss gunner)
            {
                if (AttacksPerformed == null)
                {
                    AttacksPerformed = new List<Attack>();
                    AttacksPerformed.Add(attack);
                    NumTimesSameAttackPerformed = 1;
                    return;
                }

                Attack previousAttack = AttacksPerformed[AttacksPerformed.Count - 1];
                if (previousAttack.AttackChar == attack.AttackChar)
                {
                    ++NumTimesSameAttackPerformed;
                    if (NumTimesSameAttackPerformed >= attack.MaxConsecutiveAllowed)
                    {
                        DisableAttack(attack);
                    }
                }
                else
                {
                    NumTimesSameAttackPerformed = 1;
                    EnableAttack(previousAttack);
                }
                AttacksPerformed.Add(attack);
            }

            /// <summary>
            /// Enables an attack by setting the disabled flag on all valid transitions for the attack to false.
            /// </summary>
            /// <param name="attack">The attack to enable.</param>
            public void EnableAttack(Attack attack)
            {
                attack.AssociatedTransitionWall.Disabled = false;
                attack.AssociatedTransitionCeiling.Disabled = false;
                attack.AssociatedTransitionGround.Disabled = false;
            }

            /// <summary>
            /// Disables an attack by setting the disabled flag on all valid transitions for the attack to true.
            /// </summary>
            /// <param name="attack">The attack to disable.</param>
            public void DisableAttack(Attack attack)
            {
                attack.AssociatedTransitionWall.Disabled = true;
                attack.AssociatedTransitionCeiling.Disabled = true;
                attack.AssociatedTransitionGround.Disabled = true;
            }
        }
        #endregion

        /// <summary>
        /// An enum containing the different phases of the boss.
        /// </summary>
        public enum Phase : int { WALL, CEILING, GROUND }


        /// <summary>
        /// The color the gunner flashes when it receives reduced damage (from being hit on the gun instead of the face).
        /// </summary>
        private Vector4 REDUCED_DAMAGE_FLASH_COLOUR = new Vector4(1, 125f / 255f, 0, 1);


        /// <summary>
        /// The amount of time the gunner must wait between attacks.
        /// </summary>
        public float TimeBetweenAttacks;

        /// <summary>
        /// How long has elapsed since the gunner finished its last attack.
        /// </summary>
        private float _timeSinceLastAttack;

        #region ATTACK PREFABS
        /// <summary>
        /// The game object that the gunner will use as the homing rocket.
        /// </summary>
        public GameObject HomingRocket;

        /// <summary>
        /// The game object that the gunner will use as the laser blaster.
        /// </summary>
        public GameObject LaserBlaster;

        /// <summary>
        /// The game object that the gunner will use as the bouncing grenade.
        /// </summary>
        public GameObject BouncingGrenade;

        /// <summary>
        /// The game object that the gunner will use as the directed laser charge.
        /// </summary>
        public GameObject DirectedLaserCharge;

        /// <summary>
        /// The game object that the gunner will use as the direction laser shot.
        /// </summary>
        public GameObject DirectedLaserShot;

        /// <summary>
        /// The game object that the gunner will use as the side beam charge.
        /// </summary>
        public GameObject SideBeamCharge;

        /// <summary>
        /// The game object that the gunner will use as the side beam.
        /// </summary>
        public GameObject SideBeam;
        #endregion

        #region ATTACK VARIABLES
        /// <summary>
        /// The amount of time the laser attack should charge before firing (will be reduced as the battle goes on).
        /// </summary>
        public float LaserChargeTime = 1.5f;

        /// <summary>
        /// The amount of time the beam attack should charge before firing.
        /// </summary>
        public float BeamChargeTime = 1.5f;

        /// <summary>
        /// The amount of damage a single homing rocket does.
        /// </summary>
        public float HomingRocketDamage = 15f;

        /// <summary>
        /// The amount of damage a single bouncing grenade does.
        /// </summary>
        public float BouncingGrenadeDamage = 10f;

        /// <summary>
        /// The amount of damage a directed laser shot does.
        /// </summary>
        public float DirectedLaserDamage = 30f;

        /// <summary>
        /// The amount of damage (per second) the side beam does.
        /// </summary>
        public float SideBeamDamagePerSecond = 20f;
        #endregion

        #region PROJECTILE VARIABLES
        /// <summary>
        /// A list containing the currently active homing rockets.
        /// </summary>
        [HideInInspector]
        public List<HomingRocket> ActiveRockets;

        /// <summary>
        /// A list containing the currently active bouncing grenades.
        /// </summary>
        [HideInInspector]
        public List<BouncingGrenade> ActiveGrenades;

        /// <summary>
        /// A list containing the currently active laser blasters.
        /// </summary>
        [HideInInspector]
        public List<LaserBlaster> ActiveBlasters;

        /// <summary>
        /// Holds the currently active charge for the directed laser attack (if there is one).
        /// </summary>
        [HideInInspector]
        public GameObject ActiveLaserCharge;

        /// <summary>
        /// Holds the currently active directed laser shot (if there is one).
        /// </summary>
        [HideInInspector]
        public Projectile ActiveLaser;

        /// <summary>
        /// Holds the currently active charge for the side beam attack (if there is one).
        /// </summary>
        [HideInInspector]
        public GameObject ActiveSideBeamCharge;

        /// <summary>
        /// Holds the currently active side beam attack (if there is one).
        /// </summary>
        [HideInInspector]
        public Beam ActiveSideBeam;

        /// <summary>
        /// The pool of projectiles the gunner will use for homing rockets.
        /// </summary>
        [HideInInspector]
        public PrefabPool RocketProjectilePool;

        /// <summary>
        /// The pool of projectiles the gunner will use for bouncing grenades.
        /// </summary>
        [HideInInspector]
        public PrefabPool GrenadeProjectilePool;

        /// <summary>
        /// The pool of projectiles the gunner will use for directed lasers.
        /// </summary>
        [HideInInspector]
        public PrefabPool LaserProjectilePool;
        #endregion

        #region STATE VARIABLES
        /// <summary>
        /// The current phase of the boss
        /// </summary>
        [HideInInspector]
        public Phase CurrentPhase;

        /// <summary>
        /// Whether the gunner has finished performing it's current attack.
        /// </summary>
        [HideInInspector]
        public bool AttackFinished;

        /// <summary>
        /// Whether the gunner needs to transfer surfaces (as part of a phase change).
        /// </summary>
        [HideInInspector]
        public bool TransferSurfaces;

        /// <summary>
        /// Whether the gunner is currently cleaning up projectiles.
        /// </summary>
        [HideInInspector]
        public bool PauseForProjectileCleanup;

        /// <summary>
        /// The direction the player is relative to the gunner. This is used for ceiling attacks so the gunner knows which side of the
        /// arena to go to best attack the player. -1 for the left and 1 for the right.
        /// </summary>
        [HideInInspector]
        public int PlayerDirection;

        /// <summary>
        /// Whether the player is directly below the gunner.
        /// </summary>
        [HideInInspector]
        public bool PlayerBelowGunner;

        /// <summary>
        /// The base position for the gunner's current phase.
        /// </summary>
        [HideInInspector]
        public Vector3 CurrentBasePosition;

        /// <summary>
        /// Keeps track of the attacks the gunner has performed so far.
        /// </summary>
        [HideInInspector]
        public AttackTracker Attacks;
        #endregion

        #region CANNON FIRE POSITIONS
        /// <summary>
        /// The cannon fire position that is currently active (based on the direction the gunner is facing).
        /// Homing rockets, laser blasters, and grenades come from this cannon.
        /// </summary>
        [HideInInspector]
        public Vector3 ActiveCannonFirePosition;

        /// <summary>
        /// The side cannon fire position that is currently active (based on the direction the gunner is facing).
        /// </summary>
        [HideInInspector]
        public Vector3 ActiveSideCannonFirePosition;

        /// <summary>
        /// The fire position for the cannon when the gunner is facing to the left.
        /// </summary>
        private Transform _cannonFirePositionLeft;

        /// <summary>
        /// The fire position for the cannon when the gunner is facing to the right.
        /// </summary>
        private Transform _cannonFirePositionRight;

        /// <summary>
        /// The fire position for the side cannon when the gunner is facing to the left.
        /// </summary>
        private Transform _sideCannonFirePositionLeft;

        /// <summary>
        /// The fire position for the side cannon when the gunner is facing to the right.
        /// </summary>
        private Transform _sideCannonFirePositionRight;
        #endregion

        #region ATTACKS
        //The various attacks the gunner can perform.
        public Attack HomingRocketAttack;
        public Attack BouncingGrenadeAttack;
        public Attack DirectedLaserAttack;
        public Attack LaserBlastersAttack;
        public Attack SideBeamAttack;
        #endregion

        /// <summary>
        /// Reference to the gunner's animation controller.
        /// </summary>
        [HideInInspector]
        public GunnerAnimationController AnimController;


        //Use this for initialization
        protected override void Start()
        {
            /*
            base.Start();

            RocketProjectilePool = gameObject.AddComponent<PrefabPool>();
            RocketProjectilePool._prefab = HomingRocket.gameObject;

            GrenadeProjectilePool = gameObject.AddComponent<PrefabPool>();
            GrenadeProjectilePool._prefab = BouncingGrenade.gameObject;

            LaserProjectilePool = gameObject.AddComponent<PrefabPool>();
            LaserProjectilePool._prefab = DirectedLaserShot.gameObject;

            _timeSinceLastAttack = 0;
            TransferSurfaces = false;
            AttackFinished = false;
            PlayerBelowGunner = false;
            CurrentPhase = Phase.WALL;
            //Health = 99;

            _cannonFirePositionLeft = transform.Find("Cannon Fire Position Left");
            _cannonFirePositionRight = transform.Find("Cannon Fire Position Right");
            _sideCannonFirePositionLeft = transform.Find("Side Cannon Fire Position Left");
            _sideCannonFirePositionRight = transform.Find("Side Cannon Fire Position Right");

            ActiveCannonFirePosition = _cannonFirePositionLeft.position;
            ActiveSideCannonFirePosition = _sideCannonFirePositionLeft.position;

            CurrentBasePosition = transform.position;

            AnimController = GetComponent<RobotGunnerAnimationController>();

            NPC gunner = this;
            Enemy enemyCastedGunner = this;
            RobotGunner castedGunner = this;

            //Base states
            State wallPhaseBase = new State("Wall Phase");
            State ceilingPhaseBase = new State("Ceiling Phase");
            State groundPhaseBase = new State("Ground Phase");

            //Right wall state attacks
            State wallHomingRocket = new State("Wall Homing Rocket");
            State wallBouncingGrenades = new State("Wall Bouncing Grenades");
            State wallDirectedLaser = new State("Wall Directed Laser");
            State wallClimbToCeiling = new State("Wall Climb To Ceiling");

            //Ceiling state attacks
            State ceilingHomingRocket = new State("Ceiling Homing Rocket");
            State ceilingBouncingGrenades = new State("Ceiling Bouncing Grenades");
            State ceilingDirectedLaser = new State("Ceiling Directed Laser");
            State ceilingLaserBlasters = new State("Ceiling Laser Blasters");
            State ceilingDropToGround = new State("Drop to Ground");

            //Ground state attacks
            State groundHomingRocket = new State("Ground Homing Rocket");
            State groundBouncingGrenades = new State("Ground Bouncing Grenades");
            State groundDirectedLaser = new State("Ground Directed Laser");
            State groundLaserBlasters = new State("Ground Laser Blasters");
            State groundSideBeam = new State("Ground Side Beam");

            //State machines
            NPCWeightedStateMachine wallPhase = new NPCWeightedStateMachine(wallPhaseBase, ref gunner);
            NPCWeightedStateMachine ceilingPhase = new NPCWeightedStateMachine(ceilingPhaseBase, ref gunner);
            NPCWeightedStateMachine groundPhase = new NPCWeightedStateMachine(groundPhaseBase, ref gunner);
            HierarchicalStateMachine stateMachineComponent = gameObject.AddComponent<HierarchicalStateMachine>();

            //General actions
            StateMachineAction facePlayer = new FacePlayer(ref gunner);
            StateMachineAction moveGunnerWall = new MoveTowardsOnOneAxis(ref gunner, 0.05f, true, true, true);
            StateMachineAction moveGunnerCeiling = new MoveTowardsOnOneAxis(ref gunner, 0.05f, false, false, true);
            StateMachineAction moveGunnerGround = new MoveTowardsOnOneAxis(ref gunner, 0.05f, false, true, true);
            StateMachineAction setTransferNeeded = new SetTransferNeeded(ref castedGunner, true);
            StateMachineAction setBasePositionTargetVector = new ReturnToBasePosition(ref castedGunner);
            StateMachineAction setPhaseToCeiling = new SetPhase(ref castedGunner, Phase.CEILING);
            StateMachineAction setPhaseToGround = new SetPhase(ref castedGunner, Phase.GROUND);
            StateMachineAction setAttackNotFinished = new SetAttackFinished(ref castedGunner, false);
            StateMachineAction destroyExistingBlasters = new DestroyExistingBlasters(ref castedGunner);
            StateMachineAction addTime;
            StateMachineAction clearTime;
            unsafe
            {
                fixed (float* time = &_timeSinceLastAttack)
                {
                    addTime = new AddTime(time);
                    clearTime = new ClearTime(time);
                }
            }

            //Wall phase actions
            StateMachineAction wallFireHomingRockets = new FireHomingRocket(ref castedGunner, 1);
            StateMachineAction wallFireBouncingGrenades = new FireBouncingGrenades(ref castedGunner, 8, 400, 1600, 170, 230);
            StateMachineAction wallFireDirectedLaser = new FireDirectedLaser(ref castedGunner, DirectedLaserCharge, DirectedLaserShot);
            StateMachineAction wallSetAttackPositionTargetVector = new SetTargetVectorAsOffsetGunner(ref castedGunner, new Vector3(0, 3.0f, 0), false);
            StateMachineAction setWallClimbPosition = new SetTargetVector(ref gunner, new Vector3(transform.position.x, GlobalWaypoints[0].y, transform.position.z));
            StateMachineAction setCeilingStartPosition = new SetTargetVector(ref gunner, new Vector3(GlobalWaypoints[0].x, GlobalWaypoints[0].y, transform.position.z));
            StateMachineAction climbToCeiling = new ClimbToCeiling(ref castedGunner, new ClimbToCeiling.ColliderToAvoid(GameObject.Find("Ceiling Collider").GetComponent<BoxCollider2D>(), true), new ClimbToCeiling.ColliderToAvoid(GameObject.Find("Wall Collider").GetComponent<BoxCollider2D>(), false));

            //Ceiling phase actions
            StateMachineAction ceilingFireHomingRockets = new FireHomingRocket(ref castedGunner, 1);
            StateMachineAction ceilingFireBouncingGrenades = new FireBouncingGrenades(ref castedGunner, 10, 600, 1700, 140, 200);
            StateMachineAction ceilingFireDirectedLaser = new FireDirectedLaser(ref castedGunner, DirectedLaserCharge, DirectedLaserShot);
            StateMachineAction ceilingFireLaserBlasters = new FireLaserBlasters(ref castedGunner, LaserBlaster, 6, 7, false);
            StateMachineAction cleanupProjectiles = new CleanupProjectiles(ref castedGunner);
            StateMachineAction dropToGround = new DropToGround(ref castedGunner, 0.05f);
            StateMachineAction ceilingCheckPlayerDirection = new CheckPlayerDirection(ref castedGunner);
            StateMachineAction ceilingSetAttackPositionTargetVector = new SetTargetVectorAsOffsetGunner(ref castedGunner, new Vector3(16, 0, 0), true);

            //Ground phase actions
            StateMachineAction groundFireHomingRockets = new FireHomingRocket(ref castedGunner, 2);
            StateMachineAction groundFireBouncingGrenades = new FireBouncingGrenades(ref castedGunner, 8, 500, 1000, 160, 200);
            StateMachineAction groundFireDirectedLaser = new FireDirectedLaser(ref castedGunner, DirectedLaserCharge, DirectedLaserShot);
            StateMachineAction groundFireLaserBlasters = new FireLaserBlasters(ref castedGunner, LaserBlaster, 4, 5, true);
            StateMachineAction groundFireSideBeam = new FireSideBeam(ref castedGunner, SideBeamCharge, SideBeam, 1.5f);
            StateMachineAction setGroundDropPositionX = new SetTargetVector(ref gunner, new Vector3(GlobalWaypoints[1].x, transform.position.y, transform.position.z));
            StateMachineAction setGroundDropPositionY = new SetTargetVector(ref gunner, new Vector3(transform.position.x, GlobalWaypoints[1].y, transform.position.z));

            //Transitions conditions
            Condition yesMan = new YesMan();
            Condition transferNeeded = new TransferNeeded(ref castedGunner);
            Condition transferDone = new NotCondition(transferNeeded);
            Condition phaseIsCeiling = new CheckPhase(ref castedGunner, Phase.CEILING);
            Condition phaseIsGround = new CheckPhase(ref castedGunner, Phase.GROUND);
            Condition attackDone = new AttackFinished(ref castedGunner);
            Condition grenadeCountBelowThreshold = new CheckActiveProjectileCount(ref castedGunner, 'G', 2);
            Condition blasterCountBelowThreshold = new CheckActiveProjectileCount(ref castedGunner, 'L', 1);
            Condition healthBelowCeilingThreshold = new HealthBelowThreshold(ref enemyCastedGunner, MaxHealth * 0.8f);
            Condition healthBelowGroundThreshold = new HealthBelowThreshold(ref enemyCastedGunner, MaxHealth * 0.4f);
            Condition readyForAttack;
            unsafe
            {
                fixed (float* time = &_timeSinceLastAttack)
                {
                    readyForAttack = new TimeExceeded(time, TimeBetweenAttacks);
                }
            }
            Condition grenadeAttackAllowed = new AndCondition(readyForAttack, grenadeCountBelowThreshold);
            Condition blasterAttackAllowed = new AndCondition(readyForAttack, blasterCountBelowThreshold);

            //Right wall phase transitions
            WeightedTransition wallBaseToHomingRocket = new WeightedTransition(wallHomingRocket, readyForAttack);
            WeightedTransition wallBaseToBouncingGrenades = new WeightedTransition(wallBouncingGrenades, grenadeAttackAllowed);
            WeightedTransition wallBaseToDirectedLaser = new WeightedTransition(wallDirectedLaser, readyForAttack);
            WeightedTransition wallAttackToBase = new WeightedTransition(wallPhaseBase, attackDone, 10);
            WeightedTransition wallStartClimb = new WeightedTransition(wallClimbToCeiling, healthBelowCeilingThreshold, 10);
            WeightedTransition climbFinished = new WeightedTransition(ceilingPhaseBase, transferDone, 10);

            //Ceiling phase transitions
            WeightedTransition ceilingBaseToHomingRocket = new WeightedTransition(ceilingHomingRocket, readyForAttack);
            WeightedTransition ceilingBaseToBouncingGrenades = new WeightedTransition(ceilingBouncingGrenades, grenadeAttackAllowed);
            WeightedTransition ceilingBaseToDirectedLaser = new WeightedTransition(ceilingDirectedLaser, readyForAttack);
            WeightedTransition ceilingBaseToLaserBlasters = new WeightedTransition(ceilingLaserBlasters, blasterAttackAllowed);
            WeightedTransition ceilingAttackToBase = new WeightedTransition(ceilingPhaseBase, attackDone, 10);
            WeightedTransition ceilingTransferToGround = new WeightedTransition(ceilingDropToGround, healthBelowGroundThreshold, 10);

            //Ground phase transitions
            WeightedTransition groundBaseToHomingRocket = new WeightedTransition(groundHomingRocket, readyForAttack);
            WeightedTransition groundBaseToBouncingGrenade = new WeightedTransition(groundBouncingGrenades, grenadeAttackAllowed);
            WeightedTransition groundBaseToDirectedLaser = new WeightedTransition(groundDirectedLaser, readyForAttack);
            WeightedTransition groundBaseToLaserBlasters = new WeightedTransition(groundLaserBlasters, blasterAttackAllowed);
            WeightedTransition groundBaseToSideBeam = new WeightedTransition(groundSideBeam, readyForAttack);
            WeightedTransition groundAttackToBase = new WeightedTransition(groundPhaseBase, attackDone, 10);

            //State machine transitions
            StateMachineTransition wallToCeiling = new StateMachineTransition(wallPhase, ceilingPhase, phaseIsCeiling);
            StateMachineTransition ceilingToGround = new StateMachineTransition(ceilingPhase, groundPhase, phaseIsGround);

            //Add actions and transitions to right wall phase states
            wallPhaseBase.AddStateAction(addTime);
            wallPhaseBase.AddExitAction(clearTime);
            wallPhaseBase.AddTransition(wallBaseToHomingRocket);
            wallPhaseBase.AddTransition(wallBaseToBouncingGrenades);
            wallPhaseBase.AddTransition(wallBaseToDirectedLaser);
            wallPhaseBase.AddTransition(wallStartClimb);

            wallPhaseBase.AddEntryAction(setAttackNotFinished);
            wallHomingRocket.AddStateAction(wallFireHomingRockets);
            wallHomingRocket.AddTransition(wallAttackToBase);

            wallPhaseBase.AddEntryAction(setAttackNotFinished);
            wallBouncingGrenades.AddEntryAction(wallSetAttackPositionTargetVector);
            wallBouncingGrenades.AddEntryAction(moveGunnerWall);
            wallBouncingGrenades.AddStateAction(wallFireBouncingGrenades);
            wallBouncingGrenades.AddExitAction(setBasePositionTargetVector);
            wallBouncingGrenades.AddExitAction(moveGunnerWall);
            wallBouncingGrenades.AddTransition(wallAttackToBase);

            wallPhaseBase.AddEntryAction(setAttackNotFinished);
            wallDirectedLaser.AddStateAction(wallFireDirectedLaser);
            wallDirectedLaser.AddTransition(wallAttackToBase);

            //wallClimbToCeiling.AddEntryAction(setTransferNeeded);
            //wallClimbToCeiling.AddEntryAction(facePlayer);
            //wallClimbToCeiling.AddStateAction(transferToCeiling);
            wallClimbToCeiling.AddEntryAction(setWallClimbPosition);
            wallClimbToCeiling.AddEntryAction(moveGunnerWall);
            wallClimbToCeiling.AddStateAction(climbToCeiling);
            wallClimbToCeiling.AddStateAction(setCeilingStartPosition);
            wallClimbToCeiling.AddStateAction(moveGunnerCeiling);
            wallClimbToCeiling.AddStateAction(setPhaseToCeiling);


            //Add actions and transitions to ceiling phase states
            ceilingPhaseBase.AddStateAction(addTime);
            ceilingPhaseBase.AddExitAction(clearTime);
            ceilingPhaseBase.AddTransition(ceilingBaseToHomingRocket);
            ceilingPhaseBase.AddTransition(ceilingBaseToBouncingGrenades);
            ceilingPhaseBase.AddTransition(ceilingBaseToDirectedLaser);
            ceilingPhaseBase.AddTransition(ceilingBaseToLaserBlasters);
            ceilingPhaseBase.AddTransition(ceilingTransferToGround);

            ceilingHomingRocket.AddEntryAction(facePlayer);
            ceilingHomingRocket.AddEntryAction(setAttackNotFinished);
            ceilingHomingRocket.AddStateAction(ceilingFireHomingRockets);
            ceilingHomingRocket.AddTransition(ceilingAttackToBase);

            ceilingBouncingGrenades.AddEntryAction(setAttackNotFinished);
            ceilingBouncingGrenades.AddEntryAction(ceilingCheckPlayerDirection);
            ceilingBouncingGrenades.AddEntryAction(ceilingSetAttackPositionTargetVector);
            ceilingBouncingGrenades.AddEntryAction(moveGunnerCeiling);
            ceilingBouncingGrenades.AddStateAction(ceilingFireBouncingGrenades);
            ceilingBouncingGrenades.AddExitAction(setBasePositionTargetVector);
            ceilingBouncingGrenades.AddExitAction(moveGunnerCeiling);
            ceilingBouncingGrenades.AddTransition(ceilingAttackToBase);

            ceilingDirectedLaser.AddEntryAction(setAttackNotFinished);
            ceilingDirectedLaser.AddEntryAction(ceilingCheckPlayerDirection);
            ceilingDirectedLaser.AddEntryAction(ceilingSetAttackPositionTargetVector);
            ceilingDirectedLaser.AddEntryAction(moveGunnerCeiling);
            ceilingDirectedLaser.AddStateAction(ceilingFireDirectedLaser);
            ceilingDirectedLaser.AddExitAction(setBasePositionTargetVector);
            ceilingDirectedLaser.AddExitAction(moveGunnerCeiling);
            ceilingDirectedLaser.AddTransition(ceilingAttackToBase);

            ceilingLaserBlasters.AddEntryAction(setAttackNotFinished);
            ceilingLaserBlasters.AddEntryAction(ceilingCheckPlayerDirection);
            ceilingLaserBlasters.AddEntryAction(ceilingSetAttackPositionTargetVector);
            ceilingLaserBlasters.AddEntryAction(destroyExistingBlasters);
            ceilingLaserBlasters.AddEntryAction(moveGunnerCeiling);
            ceilingLaserBlasters.AddStateAction(ceilingFireLaserBlasters);
            ceilingLaserBlasters.AddExitAction(setBasePositionTargetVector);
            ceilingLaserBlasters.AddExitAction(moveGunnerCeiling);
            ceilingLaserBlasters.AddTransition(ceilingAttackToBase);

            ceilingDropToGround.AddEntryAction(cleanupProjectiles);
            ceilingDropToGround.AddEntryAction(setBasePositionTargetVector);
            ceilingDropToGround.AddEntryAction(moveGunnerCeiling);
            ceilingDropToGround.AddEntryAction(setGroundDropPositionY);
            ceilingDropToGround.AddStateAction(dropToGround);
            ceilingDropToGround.AddStateAction(moveGunnerGround);
            ceilingDropToGround.AddStateAction(setPhaseToGround);
            ceilingDropToGround.AddExitAction(facePlayer);

            //Add actions and transitions to ground phases states
            groundPhaseBase.AddStateAction(addTime);
            groundPhaseBase.AddExitAction(clearTime);
            groundPhaseBase.AddTransition(groundBaseToHomingRocket);
            groundPhaseBase.AddTransition(groundBaseToBouncingGrenade);
            groundPhaseBase.AddTransition(groundBaseToDirectedLaser);
            groundPhaseBase.AddTransition(groundBaseToLaserBlasters);
            groundPhaseBase.AddTransition(groundBaseToSideBeam);

            groundHomingRocket.AddEntryAction(setAttackNotFinished);
            groundHomingRocket.AddStateAction(groundFireHomingRockets);
            groundHomingRocket.AddTransition(groundAttackToBase);

            groundBouncingGrenades.AddEntryAction(setAttackNotFinished);
            groundBouncingGrenades.AddStateAction(groundFireBouncingGrenades);
            groundBouncingGrenades.AddTransition(groundAttackToBase);

            groundDirectedLaser.AddEntryAction(setAttackNotFinished);
            groundDirectedLaser.AddStateAction(groundFireDirectedLaser);
            groundDirectedLaser.AddTransition(groundAttackToBase);

            groundLaserBlasters.AddEntryAction(setAttackNotFinished);
            groundLaserBlasters.AddEntryAction(destroyExistingBlasters);
            groundLaserBlasters.AddStateAction(groundFireLaserBlasters);
            groundLaserBlasters.AddTransition(groundAttackToBase);

            groundSideBeam.AddEntryAction(setAttackNotFinished);
            groundSideBeam.AddStateAction(groundFireSideBeam);
            groundSideBeam.AddTransition(groundAttackToBase);

            //Set up right wall phase state machine
            wallPhase.AddState(wallPhaseBase);
            wallPhase.AddState(wallHomingRocket);
            wallPhase.AddState(wallBouncingGrenades);
            wallPhase.AddState(wallDirectedLaser);
            wallPhase.AddState(wallClimbToCeiling);

            //Set up ceiling phase state machine
            ceilingPhase.AddState(ceilingPhaseBase);
            ceilingPhase.AddState(ceilingHomingRocket);
            ceilingPhase.AddState(ceilingBouncingGrenades);
            ceilingPhase.AddState(ceilingDirectedLaser);
            ceilingPhase.AddState(ceilingLaserBlasters);
            ceilingPhase.AddState(ceilingDropToGround);

            //Set up ground phase state machine
            groundPhase.AddState(groundPhaseBase);
            groundPhase.AddState(groundHomingRocket);
            groundPhase.AddState(groundBouncingGrenades);
            groundPhase.AddState(groundDirectedLaser);
            groundPhase.AddState(groundLaserBlasters);
            groundPhase.AddState(groundSideBeam);

            //Set up final state machine
            stateMachineComponent.AddMachine("Wall Phase", wallPhase);
            stateMachineComponent.AddMachine("Ceiling Phase", ceilingPhase);
            stateMachineComponent.AddMachine("Ground Phase", groundPhase);
            stateMachineComponent.AddTransition(wallToCeiling);
            stateMachineComponent.AddTransition(ceilingToGround);

            //Set up the attack tracker and attacks.
            Attacks = new AttackTracker();
            HomingRocketAttack = new Attack('R', 2, ref wallBaseToHomingRocket, ref ceilingBaseToHomingRocket, ref groundBaseToHomingRocket);
            BouncingGrenadeAttack = new Attack('G', 99, ref wallBaseToBouncingGrenades, ref ceilingBaseToBouncingGrenades, ref groundBaseToBouncingGrenade);
            DirectedLaserAttack = new Attack('L', 3, ref wallBaseToDirectedLaser, ref ceilingBaseToDirectedLaser, ref groundBaseToDirectedLaser);
            LaserBlastersAttack = new Attack('B', 99, ref ceilingBaseToLaserBlasters, ref ceilingBaseToLaserBlasters, ref groundBaseToLaserBlasters);
            SideBeamAttack = new Attack('S', 2, ref groundBaseToSideBeam, ref groundBaseToSideBeam, ref groundBaseToSideBeam);
            */
        }


        protected override void Update()
        {
            //Debug.Log(ActiveBlasters.Count);
            base.Update();
            UpdateCannonFirePositions();
        }

        public override void TakeDamage(IDamageGiver giver, float amount, DamageType type, Vector3 hitPoint)
        {
            Health -= amount;
            if (Health <= 0)
                OnDeath();

            if (amount > 0)
            {
                if (!Timing.IsRunning(damageFlashHandle) && !IsDead)
                {
                    if (type == DamageType.REDUCED)
                        damageFlashHandle = Timing.RunCoroutine(ApplyDamageFlash(), "Enemy Damage Flash");
                    else
                        damageFlashHandle = Timing.RunCoroutine(ApplyDamageFlash(), "Enemy Damage Flash");
                }
            }

            if (IsDead && !_destructionPlayed)
            {
                _destructionPlayed = true;
                if (_destruction != null)
                {
                    AnimController.SetDeathAnimation();
                    Timing.RunCoroutine(CleanUp(), "Clean Up");
                }
            }
        }


        public void UpdateCannonFirePositions()
        {
            if (Controller.Collisions.FaceDir == 1)
            {
                ActiveCannonFirePosition = _cannonFirePositionRight.position;
                ActiveSideCannonFirePosition = _sideCannonFirePositionRight.position;
            }
            else
            {
                ActiveCannonFirePosition = _cannonFirePositionLeft.position;
                ActiveSideCannonFirePosition = _sideCannonFirePositionLeft.position;
            }
        }

        public void InvertGravity()
        {
            currentActiveGravityState.ChangeGravityDirection(new Vector2(0, 1));
        }

        /// <summary>
        /// Performs cleanup of all the projectiles for the gunner on its death.
        /// </summary>
        public void CleanUpProjectiles()
        {
            HomingRocket[] activeRocketsCopy = new HomingRocket[ActiveRockets.Count];
            ActiveRockets.CopyTo(activeRocketsCopy);
            foreach (HomingRocket rocket in activeRocketsCopy)
            {
                rocket.Kill();
            }
            ActiveRockets.Clear();
            RocketProjectilePool.CleanThePool();

            BouncingGrenade[] activeGrenadesCopy = new BouncingGrenade[ActiveGrenades.Count];
            ActiveGrenades.CopyTo(activeGrenadesCopy);
            foreach (BouncingGrenade grenade in activeGrenadesCopy)
            {
                grenade.Kill();
            }
            ActiveGrenades.Clear();
            GrenadeProjectilePool.CleanThePool();

            LaserBlaster[] activeBlastersCopy = new LaserBlaster[ActiveBlasters.Count];
            ActiveBlasters.CopyTo(activeBlastersCopy);
            foreach (LaserBlaster blaster in activeBlastersCopy)
            {
                blaster.Destroy();
            }
            ActiveBlasters.Clear();

            if (ActiveLaserCharge != null)
            {
                ActiveLaserCharge.SetActive(false);
                Destroy(ActiveLaserCharge, 1);
                ActiveLaserCharge = null;
            }

            if (ActiveSideBeamCharge != null)
            {
                ActiveLaserCharge.SetActive(false);
                Destroy(ActiveSideBeamCharge, 1);
                ActiveSideBeamCharge = null;
            }

            if (ActiveSideBeam != null)
            {
                //BUGFIX: Destroy 
                ActiveSideBeam.gameObject.SetActive(false);
                Destroy(ActiveSideBeam.gameObject, 1);
                ActiveSideBeam = null;
            }
        }

        /// <summary>
        /// Stops operation of the gunner's state machine, cleans up its projectiles, and begins the explosion process.
        /// </summary>
        /// <returns></returns>
        private IEnumerator<float> CleanUp()
        {
            bool deactivated = false;

            if (!deactivated)
            {
                activated = false;
                yield return 0f;
                deactivated = true;
            }

            if (deactivated)
            {
                CleanUpProjectiles();
                yield return 0f;
                Timing.RunCoroutine(Explosions(), "Explosions");
            }

            yield return 0f;
        }

        /// <summary>
        /// Creates a series of explosions on the death of the gunner.
        /// </summary>
        /// <returns></returns>
        private IEnumerator<float> Explosions()
        {
            BoxCollider2D gunnerCollider = GetComponent<BoxCollider2D>();

            int totalNumExplosions = 30;
            int currentNumExplosions = 0;
            float timeBetweenExplosions = 0.5f;
            float timeSinceLastExplosion = 0;
            bool timeReduced = false;

            Vector2 minBoundsPosition = new Vector2(gunnerCollider.bounds.min.x + gunnerCollider.bounds.size.x * 0.2f, gunnerCollider.bounds.min.y + gunnerCollider.bounds.size.y * 0.3f);
            Vector2 maxBoundsPosition = new Vector2(gunnerCollider.bounds.max.x - gunnerCollider.bounds.size.x * 0.2f, gunnerCollider.bounds.max.y - gunnerCollider.bounds.size.y * 0.2f);

            while (currentNumExplosions < totalNumExplosions)
            {
                if ((currentNumExplosions == 5 || currentNumExplosions == 10) && !timeReduced)
                {
                    //We want the explosion rate to speed up, so we reduce the time between explosions at certain intervals.
                    timeBetweenExplosions /= 2;
                    timeReduced = true;
                }

                if (timeSinceLastExplosion >= timeBetweenExplosions)
                {
                    if (currentNumExplosions < totalNumExplosions - 1)
                    {
                        //All explosions before the last one should just be for effect (i.e. they don't replace the gunner) and occur at a random spot within the gunner's bounds
                        Vector2 randomExplosionPosition = new Vector2(Random.Range(minBoundsPosition.x, maxBoundsPosition.x), Random.Range(minBoundsPosition.y, maxBoundsPosition.y));
                        _destruction.JustPlayExplosion(randomExplosionPosition);

                        ++currentNumExplosions;
                        timeReduced = false;
                    }
                    else
                    {
                        //The final explosion is a much larger one that actually destroys and replaces the gunner.
                        _destruction.DestroyAndReplace(transform.parent.localScale.x * CharacterSpriteCollection.transform.localScale.x, _scrapValue, 3f);
                        ++currentNumExplosions;
                    }

                    timeSinceLastExplosion = 0;
                }
                else
                {
                    timeSinceLastExplosion += Time.deltaTime;
                }

                yield return 0f;
            }

            yield return 0f;
        }
    }
}

#pragma warning disable CS0649
