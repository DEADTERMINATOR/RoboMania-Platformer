﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Enemies.AnimationControllers
{
    public class GunnerAnimationController : MonoBehaviour, INPCAnimationController
    {
        private Animator _anim;


        //Use this for initialization
        private void Start()
        {
            _anim = GetComponentInChildren<Animator>();
        }


        public void SetIdleAnimation()
        {
            _anim.SetBool("Walk", false);
            _anim.SetBool("Shoot", false);
            _anim.SetBool("Stunned", false);
            _anim.SetBool("Dead", false);
        }

        public void SetWalkAnimation()
        {
            _anim.SetBool("Walk", true);
            _anim.SetBool("Shoot", false);
            _anim.SetBool("Stunned", false);
            _anim.SetBool("Dead", false);
        }

        public void SetStunnedAnimation(bool status)
        {
            _anim.SetBool("Walk", false);
            _anim.SetBool("Shoot", false);
            _anim.SetBool("Stunned", true);
            _anim.SetBool("Dead", false);
        }

        public void SetDeathAnimation()
        {
            _anim.SetBool("Walk", false);
            _anim.SetBool("Shoot", false);
            _anim.SetBool("Stunned", false);
            _anim.SetBool("Dead", true);
        }

        public void SetShootAnimation()
        {
            _anim.SetTrigger("Shoot");
        }

        #region Unused Animations
        public void SetAlertAnimation()
        {
            //Not used
        }

        public void SetStopShootingAnimation()
        {
            //Not used
        }
        #endregion
    }
}
