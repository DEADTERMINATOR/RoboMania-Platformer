﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Experimental.Rendering.Universal;

namespace Characters.Enemies
{
    public class Bat : Enemy
    {
        /// <summary>
        /// How much damage is done per Bomb
        /// </summary>
        public float BombDamageAmount;
        /// <summary>
        /// The Area Tiller the NPC will use for path finding
        /// </summary>
        public PathTilleMap PathTileMap;
        /// <summary>
        /// How far being the player 
        /// </summary>
        public Vector2Int FollowPalayerOffset;
        /// <summary>
        /// The projectile Drooped by the AI
        /// </summary>
        public Rocket BooombPrefab;
        /// <summary>
        /// Max distance the NPC Can detect the player
        /// </summary>
        public float MaxLookDistance = 15;
        /// <summary>
        /// The prefab pool use the pool projectiles
        /// </summary>

        ////
        public GameObject FastMovingSFX;

        public float MinTimeBetweenRuns = 5;

        public Light2D EyeLight; 

        [HideInInspector]
        public PrefabPool Pool;
        /// <summary>
        ///  Path from the PathTille system the is in use
        /// </summary>
        private WayPoints _path;
        /// <summary>
        /// Last place the player was seen
        /// </summary>
        private Vector3 _lastPlayerPos;
        /// <summary>
        /// The direction of travale 
        /// </summary>
        private Vector3 _travellDir;
        /// <summary>
        /// The Rotation of the NPC is updated By the _travellDir limited by the MAX_ANGULAR_CHANGE
        /// </summary>
        private float _angle;

        /// <summary>
        /// How Many Times The player Has been lost
        /// </summary>
        private int _lostCount = 0;
        /// <summary>
        /// How many times dose the NPC need to lose the Player to give up
        /// </summary>
        private int MAX_LOST_COUNT = 3;


        private float _patrollSpeedMod = 0.9f;
        private float _bombRunSpeedMod = 1.2f;

        private int _lookCount;
        private bool _doneWithPath;
        private float lastTimedDroop;
        private int _stillState;
        private bool _bombWasSkiped = true;
        private float _timeSinceLastBoombRun = 0;
        private float _internalTime = 0;
        private float _StartRunWaitTime = 0;

        public float MinTimeBetweenNonRunBoombDroops = 5;

        protected override void Awake()
        {
            base.Awake();
            SetFastMovingSFX(false);
            lastTimedDroop = Time.realtimeSinceStartup;
        }

        ///private WayPoints debugPoints;
        protected override void Start()
        {
            //debugPoints = new WayPoints();
            base.Start();
            SetFastMovingSFX(false);
            MoveSpeed = BaseMoveSpeed;
            //Set Up The pool
            Pool = gameObject.AddComponent<PrefabPool>();
            Pool.PoolDebugName = "Robot Bat NPC";
            Pool.PoolPrefab = BooombPrefab.gameObject;

            WayPointsComponent simpleWaypoints = Waypoints as WayPointsComponent;
            ///Set the bat to the first point
            transform.position = simpleWaypoints.WayPoints[0];

            //the Defult sate for the SM
            State defaultPatrolState = new State("Patrol");
            State followThePlayer = new State("Follow Player");
            State playerDidNotMoveMuch = new State("Player not moving");
            State flyPast = new State("Fly past");
            State returnAndDroop = new State("Return and droop");
            State returnToPatroll = new State("ReturnToPatroll");
            State DoBoomRunTell = new State("ReturnToPatroll");
            StateMachineAction SetWayPoint = new FunctionPointerAction(SetWaypoint);
            StateMachineAction defualtSettings = new FunctionPointerAction(() =>
            {
                SetFastMovingSFX(false);
                MoveSpeed = BaseMoveSpeed * _patrollSpeedMod;
                return true;

            });

            StateMachineAction ResetMoveSpeedToPatrol = new FunctionPointerAction(() =>
            {
                EyeLight.intensity = 0;
                MoveSpeed = BaseMoveSpeed * _patrollSpeedMod;
                return true;

            });

            StateMachineAction moveToTarget = new FunctionPointerAction(() =>
            {

                Vector3 newPosition = TargetVector - transform.position;

            //
            Vector3 finalVelocity = newPosition.normalized * MoveSpeed;
                Vector3 predict = transform.position + (newPosition.normalized *( base.MoveSpeed * Time.fixedDeltaTime));
                RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1, transform.position - predict, Vector3.Distance(predict, transform.position), GlobalData.OBSTACLE_LAYER_SHIFTED);

                if (!hit)
                {
                // Debug.DrawRay(transform.position, transform.position - predict, Color.green, 10);
            }
                else
                {
                // Debug.DrawRay(transform.position, transform.position - predict, Color.red, 10);
                predict = hit.centroid;
                }

                if ((finalVelocity.x > 0 && predict.x > TargetVector.x) || (finalVelocity.x < 0 && predict.x < TargetVector.x))
                {
                    finalVelocity.x = 0;
                    transform.position = new Vector3(TargetVector.x, transform.position.y);
                }
                if ((finalVelocity.y > 0 && predict.y > TargetVector.y) || (finalVelocity.y < 0 && predict.y < TargetVector.y))
                {
                    finalVelocity.y = 0;
                    transform.position = new Vector3(transform.position.x, TargetVector.y);
                }
                if (!StaticTools.ThresholdApproximately(transform.position, TargetVector, 0.3f))
                {
                //Set the NPC input based on the final velocity, as well as the axis the NPC moves on.

                SetInput(finalVelocity.normalized);
                    SetVelocity(finalVelocity, OBJECT_VELOCITY_COMPONENT);
                    ReadyForNewWaypoint = false;

                }
                else
                {
                    ReadyForNewWaypoint = true;

                }

                return true;

            });
            StateMachineAction NextPathPoint = new FunctionPointerAction(() =>
            {
                if (ReadyForNewWaypoint && _path != null && _path.Count > 0)
                {
                    TargetVector = _path.Increment();
                //debugPoints.Add(TargetVector);
                _travellDir = TargetVector - transform.position;
                    ReadyForNewWaypoint = false;
                }
                return true;

            });

            StateMachineAction GetFollowPath = new FunctionPointerAction(() =>
            {
                MoveSpeed = BaseMoveSpeed;
                
                if ((ReadyForNewWaypoint && !StaticTools.ThresholdApproximately(_lastPlayerPos, GlobalData.Player.transform.position, 2.1f) || _doneWithPath))
                {
                    _path = null;
                    WayPoints tempPath = TryAandGetFollowPath();
                    if (tempPath != null)
                    {
                        if (tempPath.Count > 0)
                        {
                            _doneWithPath = false;
                            _path = tempPath;
                            _lastPlayerPos = GlobalData.Player.transform.position;
                        }
                        else
                        {
                        //ReadyForNewWaypoint = false;
                        //TargetVector = simpleWaypoints.WayPoints.GetTarget();
                    }
                    }
                }
                return true;

            });


            StateMachineAction GetReturnPath = new FunctionPointerAction(() =>
            {
                _path = null;
                simpleWaypoints = Waypoints as WayPointsComponent;

                simpleWaypoints.WayPoints.SetTargetFromIndex(simpleWaypoints.WayPoints.GetIndexOfPointClosestTo(transform.position));
                WayPoints tempPath = PathTileMap.SortestPathWaypoints(PathTilleMap.Vector2ToVector2Int(transform.position), PathTilleMap.Vector2ToVector2Int(simpleWaypoints.WayPoints.GetTarget()));
                if (tempPath != null)
                {
                    if (tempPath.Count > 0)
                    {
                        _path = tempPath;
                    }
                    else
                    {
                        ReadyForNewWaypoint = false;
                        TargetVector = simpleWaypoints.WayPoints.GetTarget();
                    }
                }

                return true;

            });
            StateMachineAction DroopBombAtPoint = new FunctionPointerAction(() =>
            {
                if (ReadyForNewWaypoint && _bombWasSkiped)
                {
                    _bombWasSkiped = false;
                    DroopBomb();
                    return true;
                }
                _bombWasSkiped = true;
                return true;

            });

            StateMachineAction ResetAftherFlyPast = new FunctionPointerAction(() =>
            {
                SetFastMovingSFX(false);
                return true;

            });
            StateMachineAction PlanBombRun = new FunctionPointerAction(() =>
            {
                _timeSinceLastBoombRun = MinTimeBetweenRuns;
                SetFastMovingSFX(true);
                _path = null;
                MoveSpeed = BaseMoveSpeed * _bombRunSpeedMod;
                Vector3 offset;// = (transform.position.x - GlobalData.Player.transform.position.x <= 0) ? (Vector3.right * 5) + (Vector3.up * 3) : (Vector3.left * 5) + (Vector3.up * 3);
                               //Vector3 OffSEt = Vector3.zero;
            if (transform.position.x - GlobalData.Player.transform.position.x <= 0)
                {
                    if (GlobalData.Player.Controller.Collisions.FaceDir == 1)
                    {
                        offset = (Vector3.right * 5) + (Vector3.up * 3);
                    }
                    else
                    {
                        offset = (Vector3.left * 5) + (Vector3.up * 3);
                    }
                }
                else
                {
                    if (GlobalData.Player.Controller.Collisions.FaceDir == 1)
                    {
                        offset = (Vector3.right * 5) + (Vector3.up * 3);
                    }
                    else
                    {
                        offset = (Vector3.left * 5) + (Vector3.up * 3);
                    }
                }
                if (!PathTileMap[PathTilleMap.Vector2ToVector2Int(GlobalData.Player.transform.position + offset)].Passable)
                {
                    offset -= Vector3.up;
                }

                WayPoints tempPath = PathTileMap.SortestPathWaypoints(PathTilleMap.Vector2ToVector2Int(transform.position), PathTilleMap.Vector2ToVector2Int(GlobalData.Player.transform.position + offset));
                if (tempPath != null && tempPath.Count > 0)
                {
                    _path = tempPath;
                    ReadyForNewWaypoint = true;
                }

                return true;

            });


            Condition SeenPlayer = new FunctionPointerCondition(() =>
            {
                if (_lookCount % 5 == 0)
                {
                    _lookCount = 0;

                }
                else
                {
                    _lookCount++;
                    return false;
                }

                float dist = Vector2.Distance(transform.position, GlobalData.Player.transform.position);
                if (dist < MaxLookDistance)// is the player close 
            {
                    if (dist <= MaxLookDistance / 2)
                    {
                        return true;
                    }
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.position - GlobalData.Player.transform.position, MaxLookDistance, GlobalData.OBSTACLE_LAYER_SHIFTED);
                    if (hit)
                    {
                        Debug.DrawRay(transform.position, hit.point, Color.red);
                        return false;
                    }
                    else
                    {
                        Debug.DrawRay(transform.position, (GlobalData.Player.transform.position - transform.position).normalized * MaxLookDistance, Color.green);
                        ReadyForNewWaypoint = true;
                        _lostCount = 0;
                        WayPoints temppath = TryAandGetFollowPath();
                        if (temppath != null && temppath.Count > 0)// Make sure we can follow
                    {
                            _path = temppath;
                            _doneWithPath = true;
                        //set the last player pos to keep the path if the player dose not move
                        _lastPlayerPos = GlobalData.Player.transform.position;
                            return true;
                        }
                        return false;
                    }
                }
                else
                {
                    return false;
                }


            });

            Condition CanNotSeePlayer = new FunctionPointerCondition(() =>
            {
                float dist = Vector3.Distance(GlobalData.Player.transform.position, transform.position);
                if (dist < MaxLookDistance)// could be seen
            {
                    if (dist < MaxLookDistance / 2)// to close to hide
                {
                        _lostCount = 0;
                        return false;
                    }

                    RaycastHit2D hit = Physics2D.Raycast(transform.position, GlobalData.Player.transform.position - transform.position, MaxLookDistance, GlobalData.OBSTACLE_LAYER_SHIFTED & GlobalData.PLAYER_LAYER_SHIFTED);

                    if (hit && hit.collider.gameObject.layer == GlobalData.OBSTACLE_LAYER)// is hiding behind somthing
                {
                        Debug.DrawRay(transform.position, transform.position - (Vector3)hit.point, Color.red, 200);
                        _lostCount++;
                        if (_lostCount == MAX_LOST_COUNT)
                        {
                            Debug.Log("Lost the view of the player");
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else // clear path
                {
                        Debug.DrawRay(transform.position, (GlobalData.Player.transform.position - transform.position).normalized * MaxLookDistance, Color.blue);
                        ReadyForNewWaypoint = true;
                        _lostCount = 0;
                        return false;
                    }

                }
                else
                {
                    return true;
                }
            });

            Condition LostPath = new FunctionPointerCondition(() =>
            {
                if (_path == null || _path.Count <= 0)
                {
                    return true;
                }
                return false;
            });

            Condition AtPathEnd = new FunctionPointerCondition(() =>
            {
                if (_path == null || _path.Count <= 0 || _path.IsOnLastPoint())//&& StaticTools.ThresholdApproximately(transform.position, _path.Last, 0.1f))
            {
                    _doneWithPath = true;
                    return true;
                }
                return false;
            });
            Condition CanDoFlyBy = new FunctionPointerCondition(() =>
            {
                if (_timeSinceLastBoombRun > 0)
                {
                    return true;
                }
                float xdist = transform.position.x - GlobalData.Player.transform.position.x;
                
                if (Mathf.Abs(xdist) > 5)
                {
                    Debug.Log("Player is to far away");
                    return false;
                }
                if (xdist <= 0)
                {
                    if (GlobalData.Player.Controller.Collisions.FaceDir == 1)
                    {
                        return true;
                    }

                }
                else
                {
                    if (GlobalData.Player.Controller.Collisions.FaceDir == -1)
                    {
                        return true;
                    }
                }
                return false;
            });


            StateMachineAction SetNextTagetForStillPlayer = new FunctionPointerAction(() =>
            {

                if (ReadyForNewWaypoint)
                {
                    switch (_stillState)
                    {
                        case 0:
                            TargetVector = GlobalData.Player.transform.position + (Vector3.up * 4) + Vector3.left;
                            break;
                        case 1:
                            TargetVector = GlobalData.Player.transform.position + (Vector3.up * 4);
                            break;
                        case 2:
                            TargetVector = GlobalData.Player.transform.position + (Vector3.up * 4) + Vector3.right;
                            break;
                    }
                    _stillState++;
                    if (_stillState > 2)
                    {
                        _stillState = 0;
                    }
                    int i = 3;
                    while (!PathTileMap[PathTilleMap.Vector2ToVector2Int(TargetVector)].Passable && i > 0)
                    {
                        TargetVector -= Vector3.up;
                        i--;
                    }
                }
                return true;

            });

            StateMachineAction DroopBombAtPointWithTimeOut = new FunctionPointerAction(() =>
            {
                if ((StaticTools.ThresholdApproximately(transform.position.x, GlobalData.Player.transform.position.x, 0.1f)) && Time.realtimeSinceStartup - lastTimedDroop > MinTimeBetweenNonRunBoombDroops )
                {
                    DroopBomb();
                    lastTimedDroop = Time.realtimeSinceStartup;
                }
                return true;

            });

            StateMachineAction PlayerStillSetSpeed = new FunctionPointerAction(() =>
            {
                MoveSpeed = BaseMoveSpeed * 0.45f;
                return true;

            });
            StateMachineAction ResetPlayerStillSetSpeed = new FunctionPointerAction(() =>
            {
                MoveSpeed = BaseMoveSpeed;
                return true;

            });
            StateMachineAction waitForASec = new FunctionPointerAction(() =>
            {
                EyeLight.color =  Color.yellow;
                
                _StartRunWaitTime = _internalTime;
                ZeroOutVelocity();
                return true;

            });
            StateMachineAction RunTellAnimation = new FunctionPointerAction(() =>
            {

                //EyeLight.intensity = Mathf.Lerp(0, 0.5f, (_internalTime - _StartRunWaitTime) / 0.1f);
                return true;

            });
            Condition DoneWayingForTell = new FunctionPointerCondition(() =>
            {
                if (_internalTime - _StartRunWaitTime >= 0.1f)
                {
                    return true;
                }
                return false;
            });

            Condition PlayerHasMoved = new FunctionPointerCondition(() =>
            {
                if (!StaticTools.ThresholdApproximately(_lastPlayerPos, GlobalData.Player.transform.position, 3.5f))
                {
                    return true;
                }
                return false;
            });

            StateMachineAction SetSeePlayerTell = new FunctionPointerAction(() =>
            {
                EyeLight.color = Color.green;
                EyeLight.intensity = 0.5f;
                return true;

            });
            StateMachineAction SetAttackTell = new FunctionPointerAction(() =>
            {
                EyeLight.color = Color.red;
                return true;

            });
            StateMachineAction UnSetTells = new FunctionPointerAction(() =>
            {
                EyeLight.color = Color.green;
                EyeLight.intensity = 0;
                return true;

            });
            StateMachineAction UnSetAttackTell = new FunctionPointerAction(() =>
            {
                EyeLight.color = Color.yellow;
                return true;

            });
            defaultPatrolState.AddEntryAction(defualtSettings);
            defaultPatrolState.AddEntryAction(UnSetTells);
            defaultPatrolState.AddStateAction(SetWayPoint);
            defaultPatrolState.AddStateAction(moveToTarget);
            defaultPatrolState.AddTransition(new Transition(followThePlayer, SeenPlayer));


            followThePlayer.AddEntryAction(SetSeePlayerTell);
            followThePlayer.AddStateAction(GetFollowPath);
            followThePlayer.AddStateAction(NextPathPoint);
            followThePlayer.AddStateAction(moveToTarget);
            followThePlayer.AddTransition(new Transition(returnToPatroll, CanNotSeePlayer));
            followThePlayer.AddTransition(new Transition(returnToPatroll, LostPath));
            followThePlayer.AddTransition(new Transition(DoBoomRunTell, new AndCondition(AtPathEnd, CanDoFlyBy)));

            DoBoomRunTell.AddEntryAction(waitForASec);
            DoBoomRunTell.AddStateAction(RunTellAnimation);
            DoBoomRunTell.AddTransition(new Transition(flyPast, DoneWayingForTell));


            flyPast.AddEntryAction(PlanBombRun);
            flyPast.AddStateAction(NextPathPoint);
            flyPast.AddStateAction(moveToTarget);
           // flyPast.AddStateAction(moveToTarget);
            flyPast.AddTransition(new Transition(returnToPatroll, CanNotSeePlayer));
            flyPast.AddTransition(new Transition(followThePlayer, LostPath));
            flyPast.AddTransition(new Transition(returnAndDroop, AtPathEnd));
            flyPast.AddExitAction(ResetAftherFlyPast);


            playerDidNotMoveMuch.AddEntryAction(PlayerStillSetSpeed);
            playerDidNotMoveMuch.AddStateAction(SetNextTagetForStillPlayer);
            playerDidNotMoveMuch.AddStateAction(moveToTarget);
            playerDidNotMoveMuch.AddStateAction(DroopBombAtPointWithTimeOut);
            playerDidNotMoveMuch.AddTransition(new Transition(followThePlayer, PlayerHasMoved));



            returnAndDroop.AddEntryAction(GetFollowPath);
            returnAndDroop.AddEntryAction(SetAttackTell);
            returnAndDroop.AddStateAction(NextPathPoint);
            returnAndDroop.AddStateAction(moveToTarget);
            returnAndDroop.AddStateAction(DroopBombAtPoint);
            returnAndDroop.AddTransition(new Transition(playerDidNotMoveMuch, new AndCondition(new NotCondition(PlayerHasMoved), AtPathEnd)));
            returnAndDroop.AddTransition(new Transition(returnToPatroll, CanNotSeePlayer));
            returnAndDroop.AddTransition(new Transition(returnToPatroll, LostPath));
            returnAndDroop.AddTransition(new Transition(followThePlayer, AtPathEnd));
            returnAndDroop.AddExitAction(UnSetAttackTell);

            returnToPatroll.AddEntryAction(ResetMoveSpeedToPatrol);
            returnToPatroll.AddEntryAction(GetReturnPath);
            returnToPatroll.AddStateAction(NextPathPoint);
            returnToPatroll.AddStateAction(moveToTarget);
            returnToPatroll.AddTransition(new Transition(followThePlayer, SeenPlayer));
            returnToPatroll.AddTransition(new Transition(defaultPatrolState, AtPathEnd));

            _behaviourStateMachine = new StateMachine(defaultPatrolState, activated);

            // Activated = true;


        }
        protected override void Update()
        {
            if (Activated)
            {
                this.SetSpriteDirection();
                base.Update();
                UpdateAngle();
                _timeSinceLastBoombRun -= Time.deltaTime;
                _internalTime += Time.deltaTime;
            }
        }


        private void SetFastMovingSFX(bool value)
        {
            foreach (ParticleSystem ps in FastMovingSFX.GetComponentsInChildren<ParticleSystem>())
            {
                if (value)
                {
                    ps.Play();
                }
                else
                {
                    ps.Stop();
                }

            }
        }
        private void UpdateAngle()
        {
          
        }
        protected void DroopBomb()
        {
            Vector3 point = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().WorldToViewportPoint(transform.position);
            if ((point.x >= 0 && point.x <= 1) && (point.y >= 0 && point.y <= 1))// only droop if the player can see me
            {
                Projectile pro;
                pro = Pool.Spawn().GetComponent<Projectile>();
                pro.transform.position = transform.position;
                pro.Fire(new Vector3(0, 0, -90), BombDamageAmount, gameObject);
            }
        }

        private WayPoints TryAandGetFollowPath()
        {
            Vector3 OffSEt = Vector3.zero;
            if (transform.position.x - GlobalData.Player.transform.position.x <= 0)
            {
                if (GlobalData.Player.Controller.Collisions.FaceDir == -1)
                {
                    OffSEt = Vector3.right + Vector3.up * 3;
                }
                else
                {
                    OffSEt = Vector3.left + Vector3.up * 3;
                }
            }
            else
            {
                if (GlobalData.Player.Controller.Collisions.FaceDir == -1)
                {
                    OffSEt = Vector3.right + Vector3.up * 3;
                }
                else
                {
                    OffSEt = Vector3.left + Vector3.up * 3;
                }
            }
            if (PathTileMap[PathTilleMap.Vector2ToVector2Int(GlobalData.Player.transform.position + OffSEt)] != null && !PathTileMap[PathTilleMap.Vector2ToVector2Int(GlobalData.Player.transform.position + OffSEt)].Passable)
            {
                OffSEt -= Vector3.up;
            }
            if (PathTileMap[PathTilleMap.Vector2ToVector2Int(GlobalData.Player.transform.position + OffSEt)] != null && !PathTileMap[PathTilleMap.Vector2ToVector2Int(GlobalData.Player.transform.position + OffSEt)].Passable)
            {
                OffSEt -= Vector3.up;
            }
            return PathTileMap.SortestOrClosestPathWaypoints(PathTilleMap.Vector2ToVector2Int(transform.position), PathTilleMap.Vector2ToVector2Int(GlobalData.Player.transform.position + OffSEt));
        }

        public override bool SetWaypoint()
        {

            WayPointsComponent simpleWaypoints = Waypoints as WayPointsComponent;
            if (ReadyForNewWaypoint)
            {
                if (simpleWaypoints.WayPoints.IsLooping || simpleWaypoints.WayPoints.CurrentInex < simpleWaypoints.WayPoints.Count - 1)
                {
                    TargetVector = simpleWaypoints.WayPoints.Increment();
                    ReadyForNewWaypoint = false;
                }
                _travellDir = TargetVector - transform.position;
            }
            return true;
        }

        private void OnDrawGizmos()
        {
            /* if (GlobalData.UseDebugSettings)
             {
                 Gizmos.DrawLine(transform.position, TargetVector);
                 if (_usingPath)
                 {
                     //GizmoHelper.drawString("using Path", transform.position);
                     Gizmos.DrawLine(transform.position, TargetVector);

                     Gizmos.color = Color.yellow;
                     if (_path != null && _path.Count > 1)
                     {
                         Vector2 last = _path[0];
                         for (int i = 1; i < _path.Count; i++)
                         {
                             Gizmos.DrawLine(last, _path[i]);
                             last = _path[i];
                         }

                     }
                 }
             }*/
        }
    }
}
