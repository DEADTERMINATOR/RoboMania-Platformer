﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Enemies.AnimationControllers
{
    public class DragonflyAnimationController : MonoBehaviour, INPCAnimationController
    {
        /// <summary>
        /// Reference to the relevant animator component.
        /// </summary>
        public Animator Animator;

        /// <summary>
        /// Whether this controller is for the tail of the dragonfly.
        /// </summary>
        public bool IsTail;


        public void SetAlertAnimation()
        {
            Animator.SetBool("Shoot", true);
            StartCoroutine(StopShoot());
        }

        IEnumerator StopShoot()
        {
            yield return new WaitForSeconds(0.8F);
            Animator.SetBool("Shoot", false);
        }

        public void SetDeathAnimation()
        {
            Animator.SetBool("Fly", false);
        }

        public void SetIdleAnimation()
        {
            //animator.SetBool("Fly", true);
            Animator.SetBool("Shoot Preparation", false);
        }

        public void SetShootAnimation()
        {
            Animator.SetTrigger("Shoot");
        }

        public void SetShootPrepareAnimation()
        {
            Animator.SetBool("Shoot Preparation", true);
        }

        public void SetStopShootingAnimation()
        {
            //Animator.SetBool("Shoot", false);
        }

        public void SetStunnedAnimation(bool status)
        {
            Animator.SetBool("Stunned", status);
        }

        public void SetWalkAnimation()
        {
            //animator.SetBool("Fly", true);
            Animator.SetBool("Shoot Preparation", false);
        }
    }
}
