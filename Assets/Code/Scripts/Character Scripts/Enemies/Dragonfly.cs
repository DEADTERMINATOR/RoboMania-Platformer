﻿using System.Collections;
using UnityEngine;
using Characters.Enemies.AnimationControllers;
using Characters.Player;
using static GlobalData;

namespace Characters.Enemies
{
    public class Dragonfly : Enemy
    {
#pragma warning disable 414 //value is assigned but never used
        /// <summary>
        /// The pojectial the NPC will fire
        /// </summary>
        public Projectile ProjectilePrefab;

        /// <summary>
        /// The prefab pool
        /// </summary>
        [HideInInspector]
        public PrefabPool pool;
        /// <summary>
        /// The amount of dmage the pojetiles will use
        /// </summary>
        public float DamageAmount = 2;
        /// <summary>
        /// Cool down between shots 
        /// </summary>
        public float TimeBetweenShots = 0.27f;

        /// <summary>
        ///  the Speed of the partical 
        /// </summary>
        public int ProjectialSpeed = 25;

        /// <summary>
        /// The main Coloider that cover the dragofly 
        /// </summary>
        public PolygonCollider2D MainCollider;
        /// <summary>
        /// The shooting colider
        /// </summary>
        public PolygonCollider2D ShootCollider;
        /// <summary>
        /// The curent collider 
        /// </summary>
        [HideInInspector]
        public PolygonCollider2D ActiveCollider;

        /// <summary>
        /// The speed the NPC Will Move
        /// </summary>
        public float BaseSpreed = 50;


        /// <summary>
        /// The target the dragonfly should try to aim at. This can be removed if the dragonfly isn't going to target anything other than the player.
        /// </summary>
        [HideInInspector]
        public GameObject target;

        /// <summary>
        /// The position the projectiles come from on the Dragonfly.
        /// </summary>
        public GameObject firePosition;

        /// <summary>
        /// Reference to the tail's IK object.
        /// </summary>
        public GameObject tailIK;

        /// <summary>
        /// The transform containing the base position of the tail's IK (used so the tail IK knows where to return to when retracting).
        /// </summary>
        public Transform tailIKStartingPosition;

        /// <summary>
        /// Reference to the body animator controller.
        /// </summary>
        public DragonflyAnimationController DragonflyBodyAnimator;

        /// <summary>
        /// Reference to the tail animator controller.
        /// </summary>
        public DragonflyAnimationController DragonflyTailAnimator;


        /// <summary>
        /// Should The NPC use Debug Setings as it was not spawned
        /// </summary>
        public bool UseNoSpawnerSettings = false;

        public PathTilleMap PathTileMap;

        private WayPoints FoundPath;

        /// <summary>
        /// Whether the target has moved since the last frame.
        /// </summary>
        private bool _targetMoved = true;

        /// <summary>
        /// The Vector3 containing the current position of the target.
        /// </summary>
        private Vector3 _currentTargetPosition;

        /// <summary>
        /// Whether the dragonfly has begun the tail retraction process at the end of a firing bout.
        /// </summary>
        private bool _tailRetractStarted = false;

        /// <summary>
        /// The game time the tail retraction started at. Used to determine the speed the retraction should occur at.
        /// </summary>
        private float _startingTime;

        /// <summary>
        /// The position the tail was at when the retraction began. Used by Slerp as a reference point to calculate the tail's
        /// position during the retract on the current frame.
        /// </summary>
        private Vector3 _startingTailRetractPosition;


        /// <summary>
        /// Whether the aiming raycast has hit the target for the first time after folding its tail into the aiming position.
        /// Used to prevent the dragonfly from shooting until the tail is in position, though if the player moves a lot, this
        /// may need to be combined with a second condition to ensure that the firing actually begins.
        /// </summary>
        private bool _targetHitByTailAnimaRay = false;
        /// <summary>
        /// Should the NPC try to aim its tail as the traget 
        /// </summary>
        private bool _aimTailIk = false;


        private float _timeStartedShooting = 0;
        /// <summary>
        /// Was the player hit
        /// </summary>
        private bool _wasHit = false;

        /// <summary>
        /// movment area bounds derived on start from the homearea 
        /// </summary>
        private Vector3 _minPos;
        private Vector3 _maxPos;
        /// <summary>
        /// A flag to detrmin if we are moving to the Max if not we are moving to the min
        /// </summary>
        private bool _movingTowardsMax = true;

        #region Stats
        protected State _shootState;
        protected State _patrol;
        protected State _lookForPlayer;
        #endregion

        private int _lookCount;

        public float MaxLookDistance = 15;
        private int _lostCount;
        private readonly int MAX_LOST_COUNT = 5;

        /// <summary>
        /// The + - that will be randomly applied to the shoot
        /// </summary>
        private Vector2 _spreadAmount = new Vector2(-12, 20);

        private float _timeSinceLastAttack = 0;
        private float _timeSinceLastShoot = 0;

        private bool _isAttacking = false;

        private int _preShootFacingDir;

        private Vector3 _lastTrarget;

        private bool _stuckAmingState = false;

        private Vector3 _stckAtPos = Vector3.zero;
        private Vector3 _npcPosAtStuckAiming = Vector3.zero;
        private Vector3 _lastPlayerPos;

        /* Store player ref due to frequent access. */
        private PlayerMaster _playerRef;

        public BoxCollider2D DEBUGActiveArea;

        private Rigidbody2D _Rigidbody2D;
#pragma warning restore 414 //value is assigned but never used

        protected override void Start()
        {
            //set up the NPC 
            base.Start();
            _Rigidbody2D = GetComponent<Rigidbody2D>();
            ///Set up the pool
            pool = gameObject.AddComponent<PrefabPool>();
            pool.PoolDebugName = "Robot Dragofly NPC";
            pool.PoolPrefab = ProjectilePrefab.gameObject;
            //set the coilder
            ShootCollider.gameObject.SetActive(false);
            ActiveCollider = MainCollider;
            //ref for casting 
            NPC self = this;
            Dragonfly NPCCastref = this;
            DragonflyAnimationController animController = GetComponent<DragonflyAnimationController>();

            ///set the player as the traget for the tail IK 
            target = GlobalData.Player.gameObject;
            _playerRef = GlobalData.Player;

            //Get the min and max pos of the defined home area 
            if (UseNoSpawnerSettings)
            {
                _minPos = new Vector3(DEBUGActiveArea.bounds.min.x, transform.position.y, 0);
                _maxPos = new Vector3(DEBUGActiveArea.bounds.max.x, transform.position.y, 0);
            }
            else
            {
                _minPos = new Vector3(ActiveArea.xMin, ActiveArea.center.y, 0);
                _maxPos = new Vector3(ActiveArea.xMax, ActiveArea.center.y, 0);
            }
            //Set the Npc first movment traget as the max
            TargetVector = _maxPos;
            //Define the sates 
            _patrol = new State("ROBT Dragofly Patrol");
            _shootState = new State(this.GetType() + "Shoot state");
            _lookForPlayer = new State(this.GetType() + "chase state");
            State _returnToPatrol = new State(this.GetType() + "Return to patrol");




            //Look to see if the player is with in the max view area and if more then 1/2 the view is not behind an obstical 
            Condition canSeePlayer = new FunctionPointerCondition(() =>
            {

                float dist = Vector2.Distance(transform.position, GlobalData.Player.transform.position);
                if (dist < MaxLookDistance)// is the player close 
            {
                    if (dist <= MaxLookDistance / 2)
                    {
                        return true;
                    }
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, GlobalData.Player.transform.position - transform.position, MaxLookDistance, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.PLAYER_LAYER_SHIFTED);
                    if (hit)
                    {
                        if (hit.collider.gameObject.layer == GlobalData.OBSTACLE_LAYER)
                        {
                            Debug.DrawRay(transform.position, hit.point, Color.red);
                            return false;
                        }
                        if (hit.collider.gameObject.layer == GlobalData.PLAYER_LAYER)
                        {
                            Debug.DrawRay(transform.position, hit.point, Color.blue);
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        Debug.DrawRay(transform.position, (GlobalData.Player.transform.position - transform.position).normalized * MaxLookDistance, Color.green);
                        ReadyForNewWaypoint = true;
                        return true;

                    }
                }
                else
                {
                    return false;
                }
            });
            Condition CanNotSeePlayer = new FunctionPointerCondition(() =>
            {
                float dist = Vector3.Distance(GlobalData.Player.transform.position, transform.position);
                if (dist < MaxLookDistance)// could be seen
            {
                    if (dist < MaxLookDistance / 2)// to close to hide
                {
                        _lostCount = 0;
                        return false;
                    }

                    RaycastHit2D hit = Physics2D.Raycast(transform.position, GlobalData.Player.transform.position - transform.position, MaxLookDistance, GlobalData.OBSTACLE_LAYER_SHIFTED | GlobalData.PLAYER_LAYER_SHIFTED);

                    if (hit && hit.collider.gameObject.layer == GlobalData.OBSTACLE_LAYER)// is hiding behind somthing
                {
                        Debug.DrawRay(transform.position, transform.position - (Vector3)hit.point, Color.red, 200);
                        _lostCount++;
                        if (_lostCount == MAX_LOST_COUNT)
                        {
                            Debug.Log("Lost the view of the player");
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else // clear path
                {
                        Debug.DrawRay(transform.position, (GlobalData.Player.transform.position - transform.position).normalized * MaxLookDistance, Color.blue);
                        ReadyForNewWaypoint = true;
                        _lostCount = 0;
                        return false;
                    }

                }
                else
                {
                    return true;
                }
            });



            StateMachineAction ReaddyForNewWayPointInbox = new FunctionPointerAction(() =>
            {
                if (ReadyForNewWaypoint)
                {
                    if (_movingTowardsMax)
                    {
                        TargetVector = _minPos;
                    }
                    else
                    {
                        TargetVector = _maxPos;
                    }
                    _movingTowardsMax = !_movingTowardsMax;
                    ReadyForNewWaypoint = false;
                }
                return true;
            });
            StateMachineAction SetUpShootAction = new FunctionPointerAction(() => { StartShoot(); return true; });
            StateMachineAction ShootAction = new FunctionPointerAction(() => { Shoot(); return true; });
            StateMachineAction EndShootAction = new FunctionPointerAction(() => { StopShoot(); return true; });


            StateMachineAction MoveOnX = new FunctionPointerAction(() =>
            {
                MoveTowardsOnOneAxis('B', false);
                if (!StaticTools.ThresholdApproximately(transform.position, TargetVector, 0.1f))
                {
                    ReadyForNewWaypoint = false;
                }
                else
                {
                    ReadyForNewWaypoint = true;

                }


                return true;
            });



            /// Gets a path to the player or atleast close to the player 
            StateMachineAction GetPathToPlayer = new FunctionPointerAction(() =>
            {
                WayPoints newPath = TryAandGetFollowPath();
                if (newPath != null && newPath.Count > 0)
                {
                    FoundPath = newPath;
                }
                return true;
            });

            ///Move the NPC to the Set target
            StateMachineAction moveToTarget = new FunctionPointerAction(() =>
            {

                Vector3 newPosition = TargetVector - transform.position;

            //
            Vector3 finalVelocity = newPosition.normalized * MoveSpeed;
                Vector3 predict = transform.position + (newPosition.normalized * base.MoveSpeed * Time.deltaTime);
                RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1, transform.position - predict, Vector3.Distance(predict, transform.position), GlobalData.OBSTACLE_LAYER_SHIFTED);

                if (!hit)
                {
                // Debug.DrawRay(transform.position, transform.position - predict, Color.green, 10);
            }
                else
                {
                // Debug.DrawRay(transform.position, transform.position - predict, Color.red, 10);
                predict = hit.centroid;
                }

                if ((finalVelocity.x > 0 && predict.x > TargetVector.x) || (finalVelocity.x < 0 && predict.x < TargetVector.x))
                {
                    finalVelocity.x = 0;
                    _Rigidbody2D.MovePosition(new Vector3(TargetVector.x, transform.position.y));
                }
                if ((finalVelocity.y > 0 && predict.y > TargetVector.y) || (finalVelocity.y < 0 && predict.y < TargetVector.y))
                {
                    finalVelocity.y = 0;
                    _Rigidbody2D.MovePosition(new Vector3(transform.position.x, TargetVector.y));
                }
                if (!StaticTools.ThresholdApproximately(transform.position, TargetVector, 0.3f))
                {
                //Set the NPC input based on the final velocity, as well as the axis the NPC moves on.

                SetInput(finalVelocity);
                    SetVelocity(finalVelocity, OBJECT_VELOCITY_COMPONENT);
                    ReadyForNewWaypoint = false;

                }
                else
                {
                    ReadyForNewWaypoint = true;

                }

                return true;

            });


            //gets the next way point
            StateMachineAction NextPathPoint = new FunctionPointerAction(() =>
            {
                if (ReadyForNewWaypoint && FoundPath != null && FoundPath.Count > 0)
                {
                    TargetVector = FoundPath.Increment();
                //debugPoints.Add(TargetVector);
                ReadyForNewWaypoint = false;
                }
                return true;

            });
            //Finds A path back to the Patrol area
            StateMachineAction GetReturnPath = new FunctionPointerAction(() =>
            {



                WayPoints tempPath = PathTileMap.SortestPathWaypoints(PathTilleMap.Vector2ToVector2Int(transform.position), PathTilleMap.Vector2ToVector2Int(_minPos));
                if (tempPath != null)
                {
                    if (tempPath.Count > 0)
                    {
                        FoundPath = tempPath;
                        ReadyForNewWaypoint = true;
                        return true;
                    }
                    else
                    {

                    }
                }
                Debug.Log("Couold not Find path Back to patroll");
                return true;

            });

            /// Determine if the current path is at the end 
            Condition AtPathEnd = new FunctionPointerCondition(() =>
            {
                if (FoundPath == null || FoundPath.Count <= 0 || FoundPath.IsOnLastPoint())//&& StaticTools.ThresholdApproximately(transform.position, _path.Last, 0.1f))
            {
                    return true;
                }
                return false;
            });

            //define Trasitions 
            Transition FromPatrolToShoot = new Transition(_shootState, canSeePlayer);

            //set up the states
            #region Patrol state Setup
            _patrol.AddStateAction(MoveOnX);
            _patrol.AddStateAction(ReaddyForNewWayPointInbox);
            _patrol.AddTransition(FromPatrolToShoot);
            #endregion



            _shootState.AddEntryAction(SetUpShootAction);
            _shootState.AddStateAction(GetPathToPlayer);
            _shootState.AddStateAction(NextPathPoint);
            _shootState.AddStateAction(moveToTarget);
            _shootState.AddStateAction(ShootAction);


            _shootState.AddExitAction(EndShootAction);
            _shootState.AddTransition(new Transition(_returnToPatrol, CanNotSeePlayer));


            _returnToPatrol.AddEntryAction(GetReturnPath);
            _returnToPatrol.AddStateAction(NextPathPoint);
            _returnToPatrol.AddStateAction(moveToTarget);
            _returnToPatrol.AddTransition(new Transition(_patrol, AtPathEnd));
            _returnToPatrol.AddTransition(new Transition(_shootState, canSeePlayer));

            //set up the macine 
            _behaviourStateMachine = new StateMachine(_patrol, activated);


            //Activated = true;

        }
        static Vector3 right = new Vector3(4, 5);
        static Vector3 left = new Vector3(-4, 5);

        //try and get a path to the player
        private WayPoints TryAandGetFollowPath()
        {
            Vector3 OffSEt = Vector3.zero;//Tray and be above The player

            if (transform.position.x - _playerRef.transform.position.x <= 0)
            {
                if (_playerRef.Collisions.FaceDir == 1)
                {
                    OffSEt = right;
                }
                else
                {
                    OffSEt = left;
                }
            }
            else
            {
                if (_playerRef.Collisions.FaceDir == 1)
                {
                    OffSEt = right;
                }
                else
                {
                    OffSEt = left;
                }
            }
            /// Start and 2 unity above and go as low as 1 below
            for (int i = 0; i < 4; i++)
            {
                PathTille tile = PathTileMap[PathTilleMap.Vector2ToVector2Int(_playerRef.transform.position + OffSEt)];
                if (tile != null && !tile.Passable)
                {
                    OffSEt -= Vector3.up;
                }
                else
                {
                    break;
                }

            }
            try
            {
                return PathTileMap.SortestOrClosestPathWaypoints(PathTilleMap.Vector2ToVector2Int(transform.position), PathTilleMap.Vector2ToVector2Int(_playerRef.transform.position + OffSEt), true);
            }
            catch(System.Exception e)
            {
                Debug.LogError(e.Message);
                return null;
            }
        }

        public override bool SetWaypoint()
        {

            WayPointsComponent simpleWaypoints = Waypoints as WayPointsComponent;
            if (ReadyForNewWaypoint)
            {
                if (simpleWaypoints.WayPoints.IsLooping || simpleWaypoints.WayPoints.CurrentInex < simpleWaypoints.WayPoints.Count - 1)
                {
                    TargetVector = simpleWaypoints.WayPoints.Increment();
                    ReadyForNewWaypoint = false;
                }

            }
            return true;
        }

        public override void PostSpawnSetup()
        {



        }

        private IEnumerator SetFlyCollider()
        {
            yield return new WaitForSeconds(0.5F);
            ShootCollider.gameObject.SetActive(false);
            MainCollider.gameObject.SetActive(true);
            ActiveCollider = MainCollider;
        }
        protected void SetShootColider()
        {
            MainCollider.gameObject.SetActive(false);
            ShootCollider.gameObject.SetActive(true);
            ActiveCollider = ShootCollider;

        }


        public void StartShoot()
        {
            DragonflyBodyAnimator.SetShootPrepareAnimation();
            _timeStartedShooting = Time.time;
            _targetHitByTailAnimaRay = false;

            _aimTailIk = true;
            _isAttacking = true;
            ZeroOutVelocity();
            _preShootFacingDir = Controller.Collisions.FaceDir;
            // _npcAnim.SetShootAnimation();\

            Vector3 diference = StaticTools.GetPlayerRef().transform.position - firePosition.transform.position;
            float sign = (firePosition.transform.position.y < StaticTools.GetPlayerRef().transform.position.y) ? 1.0f : -1.0f;
            Vector3 it = new Vector3(0, 0, (Vector3.Angle(Vector3.right, diference) * sign));

            //is the dragonfly facing the player it is shooting
            if (Controller.Collisions.FaceDir == 1 && it.z < -90 || it.z > 90)
            {
                SetInput(Vector2.left);
            }
            else if (Controller.Collisions.FaceDir == -1 && it.z > -90)
            {
                SetInput(Vector2.right);
            }


            _targetHitByTailAnimaRay = false;
            SetShootColider();
            _wasHit = false;

        }

        public void StopShoot()
        {
            _aimTailIk = false;
            SetFlyCollider();
            StartCoroutine(ResetToMove());
            _wasHit = false;
            DragonflyBodyAnimator.SetStopShootingAnimation();
        }

        IEnumerator ResetToMove()
        {
            yield return new WaitForSeconds(0.27F);
            _isAttacking = false;
        }



        public void Shoot()
        {
            if (Activated)
            {
                DragonflyTailAnimator.SetShootAnimation();

                //Only shot if the player is in the Active Detection area and the IK Found the player 
                if (StaticTools.IsPointOnScreenWithBuffer(transform.position))
                {
                    _aimTailIk = true;
                    /*Vector3 diference = target.transform.position - firePosition.transform.position;

                    Vector3 it = new Vector3(0, 0, (Vector3.Angle(Vector3.right, diference) * sign));*/
                    //Vector3 palyerPerdiction = StaticTools.GetPlayerRef().transform.position + new Vector3(StaticTools.GetPlayerRef().Movement.Velocity.x*Time.deltaTime, 0,0);

                    /* To hit the player when they are moving, we need to estimate where they will be after the travel time of projectile
                     * and fire a projectile there. So, we calculate how long a projectile would take to hit the player given their current
                    * position, and use that as a basis to calculate how far they are going to move. */

                    /* Normally, we would have to handle both the X and Y axis here, since the dragonfly will be above the player, but we should be able to turn
                     * it into a one dimensional problem by treating our starting position as 0, and the target position as the distance to the player.
                     * From there, we can use the same calculate time to collision equation, initialPositionXB - initialPositionXA / aXVelocity - bXVelocity,
                     * where the distance to the player is positionB, and 0 is positionA. */
                    float distanceToPlayer = Vector3.Distance(firePosition.transform.position, GlobalData.Player.transform.position);
                    float timeToCollision = CalculateTimeToCollision(0, distanceToPlayer, ProjectialSpeed, GlobalData.Player.Velocity.x);

                    /* With the time to collision, we extrapolate how far the player can move in this time with the equation d = vi * t + 0.5at^2.
                     * Where vi is the player's current velocity, t is the calculated time, and acceleration is 0, which zeroes out the second half
                     * of the equation for a final equation of... */
                    float playerXDistance = GlobalData.Player.Velocity.x * timeToCollision;

                    /* Add this distance to the player's current X position to find their position after the travel time.
                     * Note that this assumes the player's Y position does not change during the time. This may or may not be a problem... */
                    Vector2 playerEstimatedPosition = new Vector2(GlobalData.Player.transform.position.x + playerXDistance, GlobalData.Player.transform.position.y);


                    Vector3 diff = (Vector3)playerEstimatedPosition - firePosition.transform.position;
                    diff.Normalize();
                    float facingSign = (firePosition.transform.position.y < StaticTools.GetPlayerRef().transform.position.y) ? 1.0f : -1.0f;
                    float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
                    Vector3 it = Quaternion.Euler(0f, 0f, rot_z).eulerAngles;
                    float TragetDistanaceOnX = transform.position.x - StaticTools.GetPlayerRef().transform.position.x;
                    //TODO shoot at the were the player will be not were it is at


                    Projectile pro;
                    if (Mathf.Abs(TragetDistanaceOnX) > 0.01)//if the player is tool Close do not change dir
                    {

                        if (TragetDistanaceOnX > 0 && Controller.Collisions.FaceDir == -1)// was facing left
                        {
                            SetInput(Vector2.left);//face right
                        }
                        else if (TragetDistanaceOnX < 0 && Controller.Collisions.FaceDir == 1)// was last facing right
                        {
                            SetInput(Vector2.right);//face left
                        }
                    }

                    //is the dragonfly facing the player it is shooting


                    if (_targetHitByTailAnimaRay && _timeSinceLastShoot > TimeBetweenShots)
                    {
                        _timeSinceLastShoot = 0;
                        it.z += Random.Range(_spreadAmount.x, _spreadAmount.y);
                        pro = pool.Spawn().GetComponent<Projectile>();
                        pro.ProjectilePrefab = pool.PoolPrefab;
                        pro.transform.position = firePosition.transform.position;

                        pro.Fire(it, DamageAmount, gameObject);

                    }

                }
            }
        }



        protected override void Update()
        {
            if (!frozen)
            {
                AnimatorStateInfo bodyAnimInfo = DragonflyBodyAnimator.Animator.GetCurrentAnimatorStateInfo(0);
                ///If the current stae is the shoot state handle shooting IK else in anny other sate tray to reset
                if (_aimTailIk)
                {
                    /*
                     * This code handles moving the Dragonfly's tail IK so that the tail lines up with the target it is shooting at (which I assume will always be the player).
                     * The general idea is that we move the IK object to the target's location, which tends to line the gun up most of the way. We want to move the IK object over
                     * a period of time so that the tail fold happens over time, instead of being instantaneous (makes the animation look better).
                     * 
                     * However, depending on the distance and position of the target, simply moving the tail IK to the target's position can result in the angle of the tail not
                     * visually lining up with the target it's supposed to be shooting at. In these instances, I've found that moving the tail IK further to the right (along the
                     * current rotation of the tail) gets the tail to eventually line up. But we need to know if this kind of adjustment is necessary, and if this adjustment is
                     * necessary, when we can stop adjusting. So a raycast is fired from the firing position of the tail that looks for a collider on the target (for best results,
                     * a very small collider should be added to the center of the target that this raycast should search for). We only do this adjustment if the target tail IK is
                     * suitably close to target's position, and the target hasn't moved, and the adjustment is cancelled if the target moves and we go back to just trying to get close
                     * to the target.
                     * 
                     * When this logic is tied into the shooting, some experimentation will likely have to be done for the best results. But I would imagine for best results, the shooting
                     * should not be tied to this animation logic (as a constantly moving target may result in the Dragonfly never actually firing). The only logic tie I would think would be
                     * that firing not start until the tail lines up for the first time.
                     */

                    if (!StaticTools.ThresholdApproximately(target.transform.position.x, _currentTargetPosition.x, 0.001f) || !StaticTools.ThresholdApproximately(target.transform.position.y, _currentTargetPosition.y, 0.001f))
                    {
                        _targetMoved = true;
                        _currentTargetPosition = target.transform.position;
                    }
                    else
                    {
                        _targetMoved = false;
                    }
                    Ray2D firePositionRay;
                    // Fier the ray in the facing direction (transform.right *-1 == transform.left)
                    if (Collisions.FaceDir == 1)
                    {
                        firePositionRay = new Ray2D(firePosition.transform.position, firePosition.transform.right * -1);
                    }
                    else
                    {
                        firePositionRay = new Ray2D(firePosition.transform.position, firePosition.transform.right);

                    }
                    RaycastHit2D hit = Physics2D.Raycast(firePositionRay.origin, firePositionRay.direction, 100.0f, GlobalData.PLAYER_LAYER_SHIFTED | GlobalData.ENEMY_LAYER_SHIFTED);

                    if (!hit || hit.collider.gameObject.layer == GlobalData.ENEMY_LAYER)///No Hit Or Self hit
                    {
                        if (!StaticTools.ThresholdApproximately(tailIK.transform.position, target.transform.position, 0.1f) || _targetMoved)
                        {
                            Vector3 moveVector;
                            moveVector = Vector3.MoveTowards(tailIK.transform.position, target.transform.position, 30f * Time.deltaTime);
                            /* Vector3 finalVelocity = moveVector - tailIK.transform.position;
                             tailIK.transform.position += finalVelocity;*/
                            tailIK.transform.position = moveVector;
                            _targetHitByTailAnimaRay = false;
                            Debug.DrawRay(firePositionRay.origin, firePositionRay.direction, Color.cyan, 15);
                        }
                        else
                        {

                            _stuckAmingState = true;
                            _stckAtPos = target.transform.position;
                            _npcPosAtStuckAiming = transform.position;

                        }
                    }
                    else
                    {
                        Debug.DrawRay(firePositionRay.origin, firePositionRay.direction, Color.green, 15);
                        _targetHitByTailAnimaRay = true;

                    }
                }
                else
                {

                    if ((bodyAnimInfo.shortNameHash == Animator.StringToHash("Shoot Completion") && bodyAnimInfo.normalizedTime >= 0.5f) || bodyAnimInfo.shortNameHash == Animator.StringToHash("Move"))
                    {
                        if (!_tailRetractStarted)
                        {
                            _startingTime = Time.time;
                            _startingTailRetractPosition = tailIK.transform.position;
                            _tailRetractStarted = true;
                        }
                        float percentComplete = (Time.time - _startingTime) / 1.5f;
                        Vector3 moveVector = Vector3.Slerp(_startingTailRetractPosition, tailIKStartingPosition.position, percentComplete);
                        Vector3 finalVelocity = moveVector - tailIK.transform.position;
                        tailIK.transform.position += finalVelocity;
                    }
                    else
                    {
                        DragonflyBodyAnimator.SetWalkAnimation();
                    }
                }

                base.Update();

                if (!_isAttacking)
                {
                    _timeSinceLastAttack += Time.deltaTime;
                }
                else
                {
                    _timeSinceLastAttack = 0;
                }
                _timeSinceLastShoot += Time.deltaTime;
                SetSpriteDirection();
                _lastPlayerPos = _playerRef.transform.position;
            }
        }

        private void OnDrawGizmos()
        {
           // GizmoHelper.drawString(StateMachine.DebugInfo(), transform.position);
            if (GlobalData.UseDebugSettings)
            {
                Gizmos.color = Color.yellow;
                if (FoundPath != null && FoundPath.Count > 1)
                {
                    Vector2 last = FoundPath[0];
                    for (int i = 1; i < FoundPath.Count; i++)
                    {
                        Gizmos.DrawLine(last, FoundPath[i]);
                        last = FoundPath[i];
                    }

                }
            }

        }

        public override void TakeDamage(IDamageGiver giver, float amount, DamageType type, Vector3 hitPoint)
        {
            base.TakeDamage(giver, amount, type, hitPoint);
            _wasHit = true;
        }

        /// <summary>
        /// Estimates where the player will have moved to during the travel time of the dragonfly's projectile, so the projectile
        /// can be fired at that position instead.
        /// </summary>
        private void FireWherePlayerWillBe()
        {
            /* To hit the player when they are moving, we need to estimate where they will be after the travel time of projectile
             * and fire a projectile there. So, we calculate how long a projectile would take to hit the player given their current
             * position, and use that as a basis to calculate how far they are going to move. */

            /* Normally, we would have to handle both the X and Y axis here, since the dragonfly will be above the player, but we should be able to turn
             * it into a one dimensional problem by treating our starting position as 0, and the target position as the distance to the player.
             * From there, we can use the same calculate time to collision equation, initialPositionXB - initialPositionXA / aXVelocity - bXVelocity,
             * where the distance to the player is positionB, and 0 is positionA. */
            float distanceToPlayer = Vector3.Distance(firePosition.transform.position, GlobalData.Player.transform.position);
            float timeToCollision = CalculateTimeToCollision(0, distanceToPlayer, ProjectialSpeed, GlobalData.Player.Velocity.x);

            /* With the time to collision, we extrapolate how far the player can move in this time with the equation d = vi * t + 0.5at^2.
             * Where vi is the player's current velocity, t is the calculated time, and acceleration is 0, which zeroes out the second half
             * of the equation for a final equation of... */
            float playerXDistance = GlobalData.Player.Velocity.x * timeToCollision;

            /* Add this distance to the player's current X position to find their position after the travel time.
             * Note that this assumes the player's Y position does not change during the time. This may or may not be a problem... */
            Vector2 playerEstimatedPosition = new Vector2(GlobalData.Player.transform.position.x + playerXDistance, GlobalData.Player.transform.position.y);
        }

        /// <summary>
        /// Calculates the amount of time until two objects will collider, given the position for both objects and their respective velocities.
        /// </summary>
        /// <param name="initialPositionXA">The initial position for the first object.</param>
        /// <param name="initialPositionXB">The initial position for the second object.</param>
        /// <param name="aXVelocity">The velocity for the first object.</param>
        /// <param name="bXVelocity">The velocity for the second object.</param>
        /// <returns>The time until the collision. A positive time indicates that the collision will occur after that much time. A negative time indicates the collision has already occured.
        ///          And a non-zero time indicates that a collision will never happen.</returns>
        private float CalculateTimeToCollision(float initialPositionXA, float initialPositionXB, float aXVelocity, float bXVelocity)
        {
            #region Collision Time Equation Explanation
            /* Calculate the position and time the two enemies will collide.
             * We only need to consider the X-axis in these calculations
             * Equation for finding an object position at a point in time - positionX(t) = initialPositionX + xVelocity * t
             * At the point of collision, the positions of both objects will be the same, so set positionX(t) for both objects
             * to be the same, and solve for t
             * initialPositionXA + aXVelocity * t = initialPositionXB + bXVelocity * t
             * => initialPositionXA + aXVelocity * t - bXVelocity * t = initialPositionXB + (bXVelocity * t) - (bXVelocity * t)
             * => initialPositionXA - initialPositionXA + aXVelocity * t - bXVelocity * t = initialPositionXB - initialPositionXA
             * => aXVelocity * t - bXVelocity * t = initialPositionXB - initialPositionXA
             * => t(aXVelocity - bXVelocity) = initialPositionXB - initialPositionXA
             * => t(aXVelocity - bXVelocity) / aXVelocity - bXVelocity = initialPositionXB - initialPositionXA / aXVelocity - bXVelocity
             * => t = initialPositionXB - initialPositionXA / aXVelocity - bXVelocity
             */
            #endregion

            return (initialPositionXB - initialPositionXA) / (aXVelocity - bXVelocity);
        }
    }
}
