﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleLimbRendering : MonoBehaviour
{
    /// <summary>
    /// The initial rendering state the limbs should be in.
    /// </summary>
    public bool InitialState;


    /// <summary>
    /// An array containined the mesh renderers for the pieces of the limb.
    /// </summary>
    private SkinnedMeshRenderer[] _limbPieces;


    //Use this for initialization
    private void Start()
    {
        _limbPieces = GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach (SkinnedMeshRenderer piece in _limbPieces)
        {
            if (piece.gameObject.tag == "BodyPart")
            {
                piece.enabled = InitialState;
            }
        }
    }


    /// <summary>
    /// Enables the limb by enabling the mesh renderers for each piece of the limb.
    /// </summary>
    public void EnableLimb()
    {
        //gameObject.SetActive(true);
        foreach (SkinnedMeshRenderer piece in _limbPieces)
        {
            if (piece.gameObject.tag == "BodyPart")
            {
                piece.enabled = true;
            }
        }
    }

    /// <summary>
    /// Disables the limb by disabling the mesh renderers for each piece of the limb.
    /// </summary>
    public void DisableLimb()
    {
        //gameObject.SetActive(false);
        foreach (SkinnedMeshRenderer piece in _limbPieces)
        {
            if (piece.gameObject.tag == "BodyPart")
            {
                piece.enabled = false;
            }
        }
    }

    /// <summary>
    /// Sets the orientation (left or right) of the limb to the opposite of what it currently is.
    /// </summary>
    public void FlipLimbOrientation()
    {
        transform.rotation = Quaternion.Euler(new Vector3(-transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z));
    }

    /// <summary>
    /// Sets the orientation (left or right) of the limb to the specified direction.
    /// </summary>
    /// <param name="setToRight">Whether the orientation should be set to the right. If false, it is set to the left.</param>
    public void SetLimbOrientation(bool setToRight)
    {
        if (setToRight)
        {
            transform.rotation = Quaternion.Euler(new Vector3(Mathf.Abs(transform.rotation.eulerAngles.x), transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z));
        }
        else
        {
            transform.rotation = Quaternion.Euler(new Vector3(-Mathf.Abs(transform.rotation.eulerAngles.x), transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z));
        }
    }
}
