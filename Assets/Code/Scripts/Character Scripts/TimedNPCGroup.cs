﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedNPCGroup : NPCGroup
{
    /// <summary>
    /// A delegate for notifications that a group has finished its time loop.
    /// </summary>
    public delegate void Finished();

    /// <summary>
    /// Notifies subscribers that this group has finished its time loop.
    /// </summary>
    public event Finished GroupFinished;


    /// <summary>
    /// The total amount of time that this group will consider a loop.
    /// </summary>
    public float LoopTime;

    /// <summary>
    /// The value the initial count starts from.
    /// </summary>
    public float StartingTime;

    /// <summary>
    /// The current amount of time that has passed (within the loop).
    /// </summary>
    [HideInInspector]
    public float CurrentTime;

    /// <summary>
    /// Whether this group is currently the controlling group.
    /// </summary>
    [HideInInspector]
    public bool ControllingGroup;


    /// <summary>
    /// Adds the frame time to the current time for this NPC group.
    /// </summary>
    public void AddTime()
    {
        if (CurrentTime >= LoopTime)
        {
            CurrentTime = 0;
            if (GroupFinished != null)
            {
                ControllingGroup = false;
                GroupFinished();
            }
        }
        CurrentTime += Time.deltaTime;
    }
}
