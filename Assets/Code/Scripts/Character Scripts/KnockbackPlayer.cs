﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;
using Characters;
using Characters.Enemies;
using static GlobalData;

public class KnockbackPlayer : Knockback, IDamageGiver
{
    /// <summary>
    /// A delegate for notifications that the player has entered the trigger this script is attached to.
    /// </summary>
    public delegate void NotifyPlayerEnteredTrigger();

    /// <summary>
    /// Notifies subscribers that the player has entered the trigger.
    /// </summary>
    public event NotifyPlayerEnteredTrigger Triggered;


    /// <summary>
    /// Whether the collider should be allowed to pass through the player when the player is invulnerable.
    /// </summary>
    [SerializeField]
    private bool _applyKnockbackWhenPlayerInvulnerable = false;

    /// <summary>
    /// Whether the object applying the knockback is an enemy.
    /// </summary>
    [SerializeField]
    private bool _isNPC;

    /// <summary>
    /// The NPC that owns this collider (if this is an NPC collider).
    /// </summary>
    [SerializeField]
    private NPC _owningNPC;

    /// <summary>
    /// Whether any damage applied by this object should be applied to any NPCs that happen to come into contact with it. Or should the damage apply to the player only.
    /// </summary>
    [SerializeField]
    private bool _shouldDamageNPCs;

    /// <summary>
    /// Whether NPCs should receive more damage from this object than the player should. If so, this multiplier determines how much more.
    /// </summary>
    [SerializeField]
    private float _npcDamageMultiplier = 1;

    /// <summary>
    /// Locally stored reference to the player.
    /// </summary>
    private PlayerMaster player;


    protected virtual void Start()
    {
        knockbackBaseValue = new Vector2(Mathf.Abs(knockbackBaseValue.x), Mathf.Abs(knockbackBaseValue.y)); //Ensure the knockback is a positive value, signage will be handled during knockback calculations.

        collider = GetComponent<Collider2D>();
        player = GlobalData.Player;

        //Do a quick check to see if this is an enemy that didn't get the OwningEnemy variable set.
        if (_isNPC && _owningNPC == null)
        {
            _owningNPC = gameObject.GetComponentInParent<Enemy>();
            Debug.Assert(_owningNPC != null);
        }
    }

    protected virtual void Update()
    {
        velocity = transform.position - _previousPosition;
        _previousPosition = transform.position;
    }


    protected virtual void OnTriggerEnter2D(Collider2D collider)
    {
        ShouldKnockbackOrDamageBeApplied(collider);
    }

    protected virtual void OnTriggerStay2D(Collider2D collider)
    {
       ShouldKnockbackOrDamageBeApplied(collider);
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        ShouldKnockbackOrDamageBeApplied(collision.collider);
    }

    protected virtual void OnCollisionStay2D(Collision2D collision)
    {
        ShouldKnockbackOrDamageBeApplied(collision.collider);
    }

    /// <summary>
    /// Fires the event that indicates to subscribers that the player has collided with this collider.
    /// Held in a seperate method so it can be called from child classes.
    /// </summary>
    protected void FireTriggerEvent()
    {
        Triggered?.Invoke();
    }


    /// <summary>
    /// Handles the dealing of knockback to the player when they hit the collider (or if by some weird way get inside the collider).
    /// </summary>
    /// <param name="collider">The collider that triggered the collision response.</param>
    protected virtual void HandleKnockbackAndDamageForPlayer(Collider2D collider)
    {
        Vector2 knockback = CalculateKnockback(player.transform.position + Vector3.up);

        bool applyKnockback = true;
        if (StaticTools.ThresholdApproximately(knockback.x, 0, 0.01f) && StaticTools.ThresholdApproximately(knockback.y, 0, 0.01f))
        {
            applyKnockback = false;
        }

        if (damage > 0)
        {
            //Get a better hit point.
            player.TakeDamage(this, damage, _isNPC ? DamageType.MELEE : DamageType.ENVIROMENT, transform.position);

            if (applyKnockback)
            {
                player.Movement.SetDamageKnockback(knockback);
                FireTriggerEvent();
            }
        }
        else if (applyKnockback)
        {
            player.Movement.SetNonDamageKnockback(knockback);
            FireTriggerEvent();
        }
    }

    protected virtual void HandleDamageForNPC(Collider2D collider)
    {
        float damageToApply = damage * _npcDamageMultiplier;
        var npc = collider.GetComponent<NPC>();

        if (npc != null)
        {
            npc.TakeDamage(this, damageToApply, DamageType.ENVIROMENT, transform.position);
        }
    }

    private void ShouldKnockbackOrDamageBeApplied(Collider2D collider)
    {
        if (enabled)
        {
            if (collider.gameObject.layer == PLAYER_LAYER || collider.gameObject.layer == SHIELD_LAYER)
            {
                //player != null - In case an exploded piece of the player collides with the enemy, we check if the player component is null. (TODO: 11-23-2020 - Is this needed anymore?)
                if (player != null && !player.State.Dead && (!player.State.Invulnerable || (_applyKnockbackWhenPlayerInvulnerable && player.State.Invulnerable))
                    && (!_isNPC || (_isNPC && _owningNPC)))
                {
                    HandleKnockbackAndDamageForPlayer(collider);
                }
            }
            else if (_shouldDamageNPCs && damage > 0 && collider.gameObject.layer == ENEMY_LAYER)
            {
                HandleDamageForNPC(collider);
            }
        }
    }
}
