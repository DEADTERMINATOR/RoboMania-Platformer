﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(BoxCollider2D))]
public class PlayerRigidbodyTypeSwitcher : MonoBehaviour
{
    public RigidbodyType2D TypeToSet;

    public bool SwithOnBackExit;
    public int MasToSet = 1;
    public RigidbodyType2D EnterType;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        EnterType = GlobalData.Player.Rigidbody2D.bodyType;
        GlobalData.Player.Rigidbody2D.bodyType = TypeToSet;
        GlobalData.Player.Rigidbody2D.mass = MasToSet;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        GlobalData.Player.Rigidbody2D.bodyType = EnterType;
    }
}
