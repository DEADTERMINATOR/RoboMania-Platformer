﻿using UnityEngine;

namespace Characters
{
    public abstract class Character : MoveableObject , IDamageReceiver
    {
        public delegate void CharacterEvent();
        public event CharacterEvent Dead;

        public delegate void StateChange(bool positive);

        public delegate void DamageEvent(Vector3 position);
        public event DamageEvent DamageReceived;


        /// <summary>
        /// The current value of X and Y-axis input. For non-player controlled characters, these values are simulated based on their current velocity.
        /// </summary>
        public abstract Vector2 MovementInput { get; }

        /// <summary>
        /// Reference to the collider that encompasses a character's body, determining where they can move.
        /// </summary>
        public abstract BoxCollider2D MovementCollider { get; }


        /// <summary>
        /// The current move speed for the character.
        /// </summary>
        [HideInInspector]
        public float MoveSpeed;


        public void NotifyDead()
        {
            Dead?.Invoke();
        }

        public void NotifyDamageReceived(Vector3 position)
        {
            DamageReceived?.Invoke(position);
        }

        public abstract void TakeDamage(IDamageGiver giver, float amount, GlobalData.DamageType type, Vector3 hitPoint);
    }
}
