﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelComponents.Platforms;

[RequireComponent(typeof(CollisionRaycaster2D))]
public class GroundController2D : Controller2D
{
    public class GroundCollisionInfo : CollisionInfo
    {
        /// <summary>
        /// The platform the object this controller handled is registered with (if they are registered with one).
        /// </summary>
        public Platform RegisteredPlatform;

        /// <summary>
        /// Whether the object is currently climbing a slope.
        /// </summary>
        public bool ClimbingSlope;

        /// <summary>
        /// Whether the object is currently descending a slope.
        /// </summary>
        public bool DescendingSlope;

        /// <summary>
        /// The angle of the slope the object is currently on (measured from a horizontal collision check).
        /// </summary>
        public float SlopeAngle;

        /// <summary>
        /// The angle of the slope the object was on in the previous frame (measured from a horizontal collision check).
        /// </summary>
        public float PreviousSlopeAngle;

        public override void Reset()
        {
            base.Reset();

            DescendingSlope = false;
            ClimbingSlope = false;

            PreviousSlopeAngle = SlopeAngle;
            SlopeAngle = 0;
        }
    }


    protected const float PLATFORM_SCAN_DISTANCE = 0.05f;
    protected const float PLATFORM_COMPONENT_MARKER_SCAN_DISTANCE = 0.15f;
    private const float SLIDE_SPEED_MULTIPLIER = 0.005f;


    /// <summary>
    /// Reference to the raycaster attached to the object this controller handles.
    /// </summary>
    public CollisionRaycaster2D Raycaster { get; private set; }

    /// <summary>
    /// Getter for the collision info property cast to its actual class.
    /// </summary>
    public GroundCollisionInfo GroundCollisions { get { return (GroundCollisionInfo)Collisions; } }

    /// <summary>
    /// Should the object this controller handles turn to avoid falling off any platform they are one.
    /// </summary>
    [HideInInspector]
    public bool ShouldTurnToNotFallOffPlatfrom = true;


    /// <summary>
    /// The maximum angle that the object can ascend or descend (without falling).
    /// </summary>
    [SerializeField]
    protected float maxAngle = 60;


    protected override void Awake()
    {
        base.Awake();

        Collisions = new GroundCollisionInfo();
        Raycaster = GetComponent<CollisionRaycaster2D>();
    }

    /// <summary>
    /// Checks for collisions on the horizontal and vertical axes and determines the appropriate velocity considering.
    /// </summary>
    /// <param name="velocity">The velocity of the object prior to collision detection.</param>
    /// <param name="standingOnPlatform">Whether the character is standing on a platform that moves (i.e. has a controller)</param>
    /// <param name="performCollisionCheckOnProvidedAxis">Whether the movement logic should perform a collision check to ensure the movement doesn't enter a collider.</param>
    /// <param name="ejectFromColliderOnX">Whether the horizontal collision check logic should attempt to eject the character should it detect that the character is potentially inside a collider.</param>
    /// <param name="ejectFromColliderOnY">Whether the vertical collision check logic should attempt to eject the character should it detect that the character is potentially inside a collider.</param>
    protected override void CalculateMove(ref Vector3 velocity, char performCollisionCheckOnProvidedAxis = 'B', char ejectFromColliderOnProvidedAxis = 'N', LayerMask? collisionMask = null)
    {
        Raycaster.UpdateCastOrigins();

        Collisions.Reset();
        Collisions.StartingVelocity = velocity;

        if (velocity.y < 0)
        {
            float signedSlopeAngle = CalculateSignedSlopeAngleBelowObject(velocity, false);
            float slideSignedSlopeAngle = CalculateSignedSlopeAngleBelowObject(velocity, true);

            if (signedSlopeAngle != 0 && Mathf.Abs(signedSlopeAngle) <= maxAngle)
            {
                DescendSlope(ref velocity, signedSlopeAngle);
            }
            else if (Mathf.Abs(slideSignedSlopeAngle) > maxAngle)
            {
                velocity.x += (Mathf.Abs(slideSignedSlopeAngle) - maxAngle) * SLIDE_SPEED_MULTIPLIER * Mathf.Sign(slideSignedSlopeAngle);
                DescendSlope(ref velocity, slideSignedSlopeAngle);
            }
        }

        if (performCollisionCheckOnProvidedAxis == 'B' || performCollisionCheckOnProvidedAxis == 'X')
        {
            HorizontalCollisions(ref velocity, ejectFromColliderOnProvidedAxis == 'B' || ejectFromColliderOnProvidedAxis == 'X', false, collisionMask);
        }

        if (performCollisionCheckOnProvidedAxis == 'B' || performCollisionCheckOnProvidedAxis == 'Y')
        {
            VerticalCollisions(ref velocity, ejectFromColliderOnProvidedAxis == 'B' || ejectFromColliderOnProvidedAxis == 'Y', true, true, collisionMask);
        }

        if (StandingOnUpwardMovingPlatform && !Collisions.IsCollidingBelow)
        {
            Collisions.Below.SetColliding(GlobalData.OBSTACLE_LAYER, objectToControl.RegisteredPlatform.gameObject);
        }
    }

    #region Horizontal Collision Check
    /// <summary>
    /// Checks for any collision on the X-axis. This version contains handling for sloped surfaces.
    /// </summary>
    /// <param name="velocity">The current velocity of the object.</param>
    protected override void HorizontalCollisions(ref Vector3 velocity, bool ejectFromCollider, bool drawRaycasts, LayerMask? collisionMask = null)
    {
        float shortestHitDistance = 999f;

        // ShouldTurnToNotFallOffPlatfrom = false;
        //Holds the current extension of the raycast distance that is done to ensure that an accurate count of the number of raycasts actually colliding with a surface is counted.
        //But, since we don't want to apply this extension to any velocity modification, we need to subtract it off any modification.
        //We start it at 0 since we don't want to apply the extension until we have at least 1 confirmed hit.
        float raycastDistanceExtension = 0;

        bool climbSlopeCalculated = false;
        float currentSlopeAngle = 0;

        float xDirection = Mathf.Sign(velocity.x);
        Raycaster.CalculateCastLength(velocity.x);

        var groundCollisions = Collisions as GroundCollisionInfo;

        for (int i = 0; i < Raycaster.HorizontalRayCount; ++i)
        {
            Raycaster.SetCastOrigin('X', xDirection);
            Raycaster.OffsetRayOrigin('X', Raycaster.HorizontalRaySpacing * i);

            RaycastHit2D hit = new RaycastHit2D();
            if (drawRaycasts)
            {
                hit = Raycaster.FireAndDrawCastAndReturnFirstResult('X', xDirection, Color.red, RAYCAST_DRAW_TIME, collisionMask);
            }
            else
            {
                hit = Raycaster.FireCastAndReturnFirstResult('X', xDirection, collisionMask);
            }

            if (hit)
            {
                if (hit.collider.gameObject.layer == GlobalData.OBSTACLE_LAYER)
                {
                    if (hit.distance == 0)
                    {
                        velocity.x = 0;

                        if (ejectFromCollider)
                        {
                            Vector3 pointOnCollider = StaticTools.ClosestPoint(hit.collider, hit.point);
                            Vector2 directionOfPoint = new Vector2(pointOnCollider.x, pointOnCollider.y) - hit.point;

                            objectToControl.transform.Translate(new Vector3((pointOnCollider.x - hit.point.x) + X_EJECT_DISTANCE * Mathf.Sign(directionOfPoint.x), 0, 0), Space.World);
                        }

                        continue;
                    }

                    float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                    //We normally only want to do this check once, however, in the case of subtle angle changes when travelling at high velocity (e.g. dash), a recalculation may be necessary
                    //if we detect a different angle in the middle of the collision check.
                    if ((!climbSlopeCalculated || (climbSlopeCalculated && slopeAngle != currentSlopeAngle)) && slopeAngle <= maxAngle)
                    {
                        if (groundCollisions.DescendingSlope)
                        {
                            groundCollisions.DescendingSlope = false;
                            velocity = Collisions.StartingVelocity;
                        }

                        float distanceToSlopeStart = 0;
                        if (slopeAngle != groundCollisions.PreviousSlopeAngle)
                        {
                            distanceToSlopeStart = hit.distance - CollisionCaster2D.SKIN_WIDTH;
                            velocity.x -= distanceToSlopeStart * xDirection;
                        }
                        ClimbSlope(ref velocity, slopeAngle, hit.collider.gameObject);
                        velocity.x += distanceToSlopeStart * xDirection;

                        climbSlopeCalculated = true;
                        currentSlopeAngle = slopeAngle;
                    }

                    if (!groundCollisions.ClimbingSlope || slopeAngle > maxAngle)
                    {
                        if (hit.distance < shortestHitDistance)
                        {
                            velocity.x = (hit.distance - raycastDistanceExtension - CollisionCaster2D.SKIN_WIDTH) * xDirection;
                            shortestHitDistance = hit.distance;

                            if (slopeAngle <= 90)
                            {
                                raycastDistanceExtension = 0.01f;
                            }
                        }
                        Raycaster.SetCastLength(hit.distance + raycastDistanceExtension);

                        if (groundCollisions.ClimbingSlope)
                        {
                            velocity.y = Mathf.Tan(groundCollisions.SlopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x);
                        }

                        if (xDirection == -1)
                        {
                            Collisions.Left.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                        }
                        else
                        {
                            Collisions.Right.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                        }
                    }
                }
            }
        }
    }
    #endregion

    #region Vertical Collision Check
    /// <summary>
    /// Checks for any collision on the Y-axis. This version contains handling for slopes.
    /// </summary>
    /// <param name="velocity">The current velocity of the object.</param>
    protected override void VerticalCollisions(ref Vector3 velocity, bool ejectFromCollider, bool checkForPlatform, bool drawRaycasts, LayerMask? collisionMask = null)
    {
        //Mark: I can't remember why I wanted a '0' for the yDirection in cases where there was no vertical velocity, but it interferes with edge of platform check for object's when they are on a moving platform. Hopefully, whatever reason I wanted that '0' for is no longer relevant.
        float yDirection = Mathf.Sign(velocity.y);//velocity.y != 0 ? Mathf.Sign(velocity.y) : 0;

        /* Mark: I changed the xDirection to only take into account the base object velocity for the x-axis, instead of the object's overall x-axis velocity,
         * because taking into account the overall velocity presented issues if the object was on a side to side moving platform where they object could be moving against the platform's velocity,
         * but the speed of the platform meant that the overall velocity was still in the direction of the platform. This would cause the raycasts to begin from the wrong side of the collider.
         * Hopefully, doing things this way will not present any issues, but if they do, we can go back to the old way, and I'll try to find a different way to account for the described case. */
        float xDirection = Mathf.Sign(objectToControl.GetVelocityComponentVelocity().x);//Mathf.Sign(velocity.x);

        float raySpacing = xDirection == 1 ? -Raycaster.VerticalRaySpacing : Raycaster.VerticalRaySpacing;

        //Platform platformBelow = null;
        //Transform lastTransformHit = null;

        ShouldTurnToNotFallOffPlatfrom = false;

        float shortestHitDistance = 999f;
        int verticalRaysHit = 0;

        var groundCollisions = Collisions as GroundCollisionInfo;

        Raycaster.SetCastLength(CollisionCaster2D.SKIN_WIDTH + Mathf.Abs(velocity.y) + 0.05f);
        for (int i = 0; i < Raycaster.VerticalRayCount; ++i)
        {
            Raycaster.SetCastOrigin(new Vector2(xDirection, yDirection));
            Raycaster.OffsetRayOrigin('Y', raySpacing * i + velocity.x);

            var hit = new RaycastHit2D();
            if (drawRaycasts)
            {
                hit = Raycaster.FireAndDrawCastAndReturnFirstResult('Y', yDirection, Color.red, RAYCAST_DRAW_TIME, collisionMask);
            }
            else
            {
                hit = Raycaster.FireCastAndReturnFirstResult('Y', yDirection, collisionMask);
            }

            if (hit)
            {
                bool setVelocity = false;

                verticalRaysHit++;
                if (hit.distance < shortestHitDistance)
                {
                    shortestHitDistance = hit.distance;
                    setVelocity = true;
                }
                Raycaster.SetCastLength(hit.distance);

                if (hit.distance == 0)
                {
                    if (!hit.collider.isTrigger)
                    {
                        velocity = Vector3.zero;

                        if (ejectFromCollider && TheGame.GetRef.gameState != TheGame.GameState.TRANSITION)
                        {
                            Vector3 pointOnCollider = StaticTools.ClosestPoint(hit.collider, hit.point);
                            Vector2 directionOfPoint = new Vector2(pointOnCollider.x, pointOnCollider.y) - hit.point;

                            objectToControl.transform.Translate(new Vector3(0, pointOnCollider.y - hit.point.y + Y_EJECT_DISTANCE * Mathf.Sign(directionOfPoint.y), 0));
                        }
                    }
                    else
                    {
                        return;
                    }
                }

                if (setVelocity)
                {
                    //Only set the velocity and collisions if this hit is the shortest distance one we've found yet (indicating this is the new minimum distance the object can move).
                    if (groundCollisions.ClimbingSlope)
                    {
                        velocity.x = velocity.y / Mathf.Tan(groundCollisions.SlopeAngle * Mathf.Deg2Rad) * Mathf.Sign(velocity.x);
                    }

                    if (yDirection == -1)
                    {
                        Collisions.Below.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                    }
                    else if (yDirection == 1)
                    {
                        Collisions.Above.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                    }

                    if (Collisions.IsCollidingBelow)
                    {
                        Collisions.BelowSurfaceNormal = hit.normal;
                    }

                    float newYVelocity = (hit.distance - CollisionRaycaster2D.SKIN_WIDTH) * yDirection;
                    if (TheGame.GetRef.gameState == TheGame.GameState.TRANSITION && newYVelocity > 0)
                    {
                        //If the game is going through an area transition, don't respond to Y velocities greater that 0 because it's likely
                        //a result of the player walked through a surface during the transition, and we don't want the weird collision issues
                        //that can accompany that.
                        newYVelocity = 0;
                    }
                    velocity.y = newYVelocity;
                }
            }

            //Also check if we're colliding in the direction we aren't travelling (which may happen if the object is on a platform, or being pushed by something).
            Raycaster.SetCastOrigin(new Vector2(xDirection, -yDirection));
            Raycaster.OffsetRayOrigin('Y', raySpacing * i + velocity.x);
            Raycaster.SetCastLength(0.05f);

            var otherDirectionHit = new RaycastHit2D();
            if (drawRaycasts)
            {
                otherDirectionHit = Raycaster.FireAndDrawCastAndReturnFirstResult('Y', -yDirection, Color.blue, RAYCAST_DRAW_TIME, collisionMask);
            }
            else
            {
                otherDirectionHit = Raycaster.FireCastAndReturnFirstResult('Y', -yDirection, collisionMask);
            }

            if (otherDirectionHit)
            {
                ++verticalRaysHit;
            }

            if (i == 0 && ((!hit && yDirection == Mathf.Sign(objectToControl.CurrentActiveGravityState.GravityVelocity.normalized.y)) || (!otherDirectionHit && yDirection != Mathf.Sign(objectToControl.CurrentActiveGravityState.GravityVelocity.normalized.y))))
            {
                //First ray was not a hit in one of the directions so we need to check if we're about to go off a platform
                ShouldTurnToNotFallOffPlatfrom = true;
                OnShouldTurnAroundFromPlatfrom?.Invoke();
            }
        }

        if (checkForPlatform && velocity.y < PLATFORM_SCAN_DISTANCE)
        {
            CastForPlatformBelow(ref velocity, drawRaycasts);
        }

        if (ShouldTurnToNotFallOffPlatfrom && verticalRaysHit == 0) //No hits means we are in the air
        {
            ShouldTurnToNotFallOffPlatfrom = false;
        }

        if (groundCollisions.ClimbingSlope)
        {
            //If we are climbing a slope, we want to check at the Y value that we will be if there is a slope with a different angle than we are currently on.
            //If so, we need to handle that.
            Raycaster.SetCastLength(velocity.x);
            Raycaster.SetCastOrigin('X', xDirection);
            Raycaster.OffsetRayOrigin('X', velocity.y);
            var hit = Raycaster.FireCastAndReturnFirstResult('X', xDirection);

            if (hit)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle != groundCollisions.SlopeAngle)
                {
                    velocity.x = (hit.distance - CollisionRaycaster2D.SKIN_WIDTH) * xDirection;
                    groundCollisions.SlopeAngle = slopeAngle;
                }
            }
        }
    }
    #endregion

    #region Slope Calculations
    /// <summary>
    /// Calculates the required velocity so that the desired move distance is achieved
    /// when climbing up slopes.
    /// </summary>
    /// <param name="velocity">The current velocity of the object.</param>
    /// <param name="slopeAngle">The angle of the slope the object is currently on.</param>
    protected void ClimbSlope(ref Vector3 velocity, float slopeAngle, GameObject hitCollider)
    {
        float moveDistance = Mathf.Abs(velocity.x);
        float yClimbingVelocity = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

        if (velocity.y <= yClimbingVelocity)
        {
            velocity.y = yClimbingVelocity;
            velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);

            Collisions.Below.SetColliding(GlobalData.OBSTACLE_LAYER, hitCollider);

            var groundCollisions = Collisions as GroundCollisionInfo;
            groundCollisions.ClimbingSlope = true;
            groundCollisions.SlopeAngle = slopeAngle;
        }
    }

    /// <summary>
    /// Calculates the required velocity so that the desired move distance is achieved
    /// when descending a slope while remaining in contact with the slope (i.e. gravity doesn't
    /// cause the object to keep dropping down the slope).
    /// </summary>
    /// <param name="velocity">The current velocity of the object.</param>
    /// <param name="slopeAngle">The angle of the slope the object is currently on.</param>
    /// <returns></returns>
    protected void DescendSlope(ref Vector3 velocity, float slopeAngle)
    {
        float moveDistance = Mathf.Abs(velocity.x);
        float yDescendingVelocity = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
        yDescendingVelocity = -Mathf.Abs(yDescendingVelocity); //Guarantee that the descending velocity is negative.

        if (yDescendingVelocity < velocity.y)
        {
            velocity.y = yDescendingVelocity;
            velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);

            var groundCollisions = Collisions as GroundCollisionInfo;
            groundCollisions.DescendingSlope = true;
            groundCollisions.SlopeAngle = slopeAngle;
        }
    }

    /// <summary>
    /// Calculates and returns the signed slope angle of the platform beneath the object.
    /// </summary>
    /// <returns>The signed angle of the platform beneath the object, 
    /// or 0 if the platform is flat or the object is in mid-air.</returns>
    protected float CalculateSignedSlopeAngleBelowObject(Vector3 velocity, bool checkBothSides)
    {
        bool firstRaycastFromBottomLeft = false;

        Raycaster.CalculateCastLength(velocity.y);

        if (Collisions.FaceDir == 1)
        {
            if (velocity.x >= 0)
            {
                Raycaster.SetCastOrigin(Raycaster.Origins.BottomLeft);
                firstRaycastFromBottomLeft = true;
            }
            else
            {
                Raycaster.SetCastOrigin(Raycaster.Origins.BottomRight);
            }
        }
        else
        {
            if (velocity.x <= 0)
            {
                Raycaster.SetCastOrigin(Raycaster.Origins.BottomRight);
            }
            else
            {
                Raycaster.SetCastOrigin(Raycaster.Origins.BottomLeft);
                firstRaycastFromBottomLeft = true;
            }
        }
        var hit = Raycaster.FireCastAndReturnFirstResult('Y', -1);

        float signedAngle = 0;
        if (hit)
        {
            signedAngle = Vector2.SignedAngle(hit.normal, Vector2.up);
            Collisions.Below.SetColliding(GlobalData.OBSTACLE_LAYER, hit.collider.gameObject);
        }

        if (checkBothSides && signedAngle == 0)
        {
            if (firstRaycastFromBottomLeft)
            {
                Raycaster.SetCastOrigin(Raycaster.Origins.BottomRight);
            }
            else
            {
                Raycaster.SetCastOrigin(Raycaster.Origins.BottomLeft);
            }

            hit = Raycaster.FireCastAndReturnFirstResult('Y', -1);
            if (hit)
            {
                signedAngle = Vector2.SignedAngle(hit.normal, Vector2.up);
                Collisions.Below.SetColliding(GlobalData.OBSTACLE_LAYER, hit.collider.gameObject);
            }
        }

        return signedAngle;
    }
    #endregion

    #region Other
    protected void CastForPlatformBelow(ref Vector3 velocity, bool drawRaycasts)
    {
        var groundCollisions = Collisions as GroundCollisionInfo;

        //If platformBelow is still null after this check, then we know the player is not colliding with a platform below them anymore.
        Boxcaster.UpdateCastOrigins();
        //We only want to add the y velocity to the cast origin if the object's y velocity is negative (i.e. it's going down). Adding a positive y velocity will cause the origin to raise into the object's collider, causing the object to possible sink into the platform before the cast detects a valid platform collision.
        //Boxcaster.SetCastOrigin(new Vector2(Boxcaster.Origins.Center.x + velocity.x, velocity.y < 0 ? Boxcaster.Origins.BottomRight.y + velocity.y : Boxcaster.Origins.BottomRight.y));
        Boxcaster.SetCastOrigin(new Vector2(Boxcaster.Origins.Center.x + velocity.x, Boxcaster.Origins.BottomRight.y));

        Boxcaster.SetBoxSize(new Vector2(Boxcaster.Collider.bounds.size.x, CollisionCaster2D.SKIN_WIDTH)); //Set the box size and positioning to right along the bottom of the object collider.
        Boxcaster.SetCastLength(velocity.y < 0 ? PLATFORM_SCAN_DISTANCE + Mathf.Abs(velocity.y) : PLATFORM_SCAN_DISTANCE);

        var hit = new RaycastHit2D();
        if (drawRaycasts)
        {
            hit = Boxcaster.FireAndDrawCastAndReturnFirstResult('Y', -1, Color.cyan, RAYCAST_DRAW_TIME);
        }
        else
        {
            hit = Boxcaster.FireCastAndReturnFirstResult('Y', -1);
        }

        if (hit)
        {
            var platformBelow = hit.transform.GetComponentInChildren<Platform>();
            if (platformBelow == null)
            {
                platformBelow = hit.transform.GetComponentInParent<Platform>();
            }

            if (platformBelow != null)
            {
                bool isNewRegistration = platformBelow.RegisterPassenger(objectToControl);
                groundCollisions.RegisteredPlatform = platformBelow;

                if (isNewRegistration)
                {
                    var yVelocityToMeetPlatform = 0f;
                    if (platformBelow.CurrentVelocityDirection.y > 0)
                    {
                        yVelocityToMeetPlatform = (platformBelow.DistanceTravelledThisFrame * platformBelow.CurrentVelocityDirection.y) - hit.distance;
                        if (platformBelow.DistanceTravelledThisFrame != 0)
                        {
                            velocity.y = Mathf.Abs(yVelocityToMeetPlatform);
                        }
                        else
                        {
                            //This feels messy, but it's the only thing I can find that satisfies both cases in which the platform landed on is already moving, and cases where the platform doesn't start moving until an object lands on it.
                            velocity.y = yVelocityToMeetPlatform;
                        }
                    }
                    else if (platformBelow.CurrentVelocityDirection.y < 0)
                    {
                        yVelocityToMeetPlatform = (platformBelow.DistanceTravelledThisFrame * platformBelow.CurrentVelocityDirection.y) + hit.distance;
                        if (platformBelow.DistanceTravelledThisFrame != 0)
                        {
                            velocity.y = -Mathf.Abs(yVelocityToMeetPlatform);
                        }
                        else
                        {
                            velocity.y = yVelocityToMeetPlatform;
                        }
                    }
                }
            }
        }
        else if (groundCollisions.RegisteredPlatform != null)
        {
            groundCollisions.RegisteredPlatform.DeregisterPassenger(objectToControl);
            groundCollisions.RegisteredPlatform = null;
        }

        //Check if there are any platforms beneath us that aren't close enough to be registered with, but close enough that we should add a passenger component marker to be prepared.
        Boxcaster.SetCastLength(velocity.y < 0 ? PLATFORM_COMPONENT_MARKER_SCAN_DISTANCE + Mathf.Abs(velocity.y) : PLATFORM_COMPONENT_MARKER_SCAN_DISTANCE);

        hit = new RaycastHit2D();
        if (drawRaycasts)
        {
            hit = Boxcaster.FireAndDrawCastAndReturnFirstResult('Y', -1, Color.yellow, RAYCAST_DRAW_TIME);
        }
        else
        {
            hit = Boxcaster.FireCastAndReturnFirstResult('Y', -1);
        }

        if (hit)
        {
            var platformBelow = hit.transform.GetComponentInChildren<Platform>();
            if (platformBelow == null)
            {
                platformBelow = hit.transform.GetComponentInParent<Platform>();
            }

            if (platformBelow != null && objectToControl.gameObject.GetComponent<ObjectIsPassenger>() == null)
            {
                objectToControl.gameObject.AddComponent<ObjectIsPassenger>();
            }
        }
    }
    #endregion
}
