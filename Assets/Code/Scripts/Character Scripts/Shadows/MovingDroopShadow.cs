﻿using UnityEngine;
using System.Collections;

public class MovingDroopShadow : MonoBehaviour
{
    public delegate void ReportRotation(float zRotation);
    public event ReportRotation Rotation;

    /// <summary>
    /// Transform of the traked GameObject 
    /// </summary>
    public Transform TrakingGameObject;
    /// <summary>
    /// The Material with the shader
    /// </summary>
    public Material ShadowShader;
    /// <summary>
    ///  The Transform of the Shadow quad
    /// </summary>
    public Transform ShadowTransfrom;
    /// <summary>
    /// The Max distance the shadow is shows also affects scaling
    /// </summary>
    public float MaxDisctance = 10;
    /// <summary>
    /// The default Full scale
    /// </summary>
    private Vector3 _startingScale;
    /// <summary>
    /// The Angle to apply half rotaions 2 
    /// </summary>
    private const float MIN_Y_DISTIANCE_FOR_ADJUSRMENT = 0.01f;
    private const float MAX_Y_DISTIANCE_FOR_ADJUSRMENT = 0.75f;

    private const int OFFSET_LEFT_RAY = 0;
    private const int MAIN_RAY = 1;
    private const int OFFSET_RIGHT_RAY = 2;

    private RaycastHit2D[] hit2Ds = new RaycastHit2D[3];

    private const string ALPHA_PRARAM_STRING = "_Alpha";
    //Last used angel 
    private Vector3 _lastAngle = Vector3.zero;

    private MaterialPropertyBlock _materialPropertyBlock;
    private Renderer _renderRef;

    private bool _objectOnScreen = false;


    // Use this for initialization
    void Start()
    {
        _renderRef = GetComponent<Renderer>();
        _materialPropertyBlock = new MaterialPropertyBlock();
        _startingScale = transform.localScale;//Hold The scale at the start
    }

    // Update is called once per frame
    void Update()
    {
        if (_objectOnScreen)
        {
            hit2Ds[OFFSET_LEFT_RAY] = Physics2D.Raycast(TrakingGameObject.transform.position + (Vector3.left / 2), Vector3.down, MaxDisctance, GlobalData.OBSTACLE_LAYER_SHIFTED);
            hit2Ds[MAIN_RAY] = Physics2D.Raycast(TrakingGameObject.transform.position, Vector3.down, MaxDisctance, GlobalData.OBSTACLE_LAYER_SHIFTED);
            hit2Ds[OFFSET_RIGHT_RAY] = Physics2D.Raycast(TrakingGameObject.transform.position + (Vector3.right / 2), Vector3.down, MaxDisctance, GlobalData.OBSTACLE_LAYER_SHIFTED);

            if (hit2Ds[OFFSET_LEFT_RAY] && hit2Ds[MAIN_RAY])
            {
                //Calc the alpha from the distice of the ray so that it is alway semi trasparent since it is alway off the gound 
                float Alpha = hit2Ds[MAIN_RAY].distance / MaxDisctance;
                //salce the shadow with the invers of the deistace
                Vector3 scale = _startingScale * (1 - (hit2Ds[MAIN_RAY].distance / MaxDisctance));
                ///Apply scale and alpha 
                _materialPropertyBlock.SetFloat(ALPHA_PRARAM_STRING, Alpha);
                ShadowTransfrom.localScale = scale;

                ShadowTransfrom.transform.position = hit2Ds[MAIN_RAY].point;
                float DiffA = Mathf.Abs(hit2Ds[OFFSET_LEFT_RAY].point.y - hit2Ds[MAIN_RAY].point.y);
                float DiffB = Mathf.Abs(hit2Ds[OFFSET_RIGHT_RAY].point.y - hit2Ds[MAIN_RAY].point.y);
                //Debug.Log("DiffA: " + DiffA);
                // Debug.Log("DiffB: " + DiffB);
                if ((DiffA < MAX_Y_DISTIANCE_FOR_ADJUSRMENT && DiffA > MIN_Y_DISTIANCE_FOR_ADJUSRMENT) || (DiffB < MAX_Y_DISTIANCE_FOR_ADJUSRMENT && DiffB > MIN_Y_DISTIANCE_FOR_ADJUSRMENT))
                {
                    float angleA = Vector2.SignedAngle(hit2Ds[OFFSET_LEFT_RAY].point - hit2Ds[MAIN_RAY].point, hit2Ds[OFFSET_LEFT_RAY].point - new Vector2(hit2Ds[OFFSET_LEFT_RAY].point.x, hit2Ds[MAIN_RAY].point.y));
                    float angleB = Vector2.SignedAngle(hit2Ds[OFFSET_RIGHT_RAY].point - hit2Ds[MAIN_RAY].point, hit2Ds[OFFSET_RIGHT_RAY].point - new Vector2(hit2Ds[OFFSET_RIGHT_RAY].point.x, hit2Ds[MAIN_RAY].point.y));

                    float angle = angleA;


                    //Use The angle that changed a hack not to need a  facing directions 
                    if (angleA != angleB)
                    {
                        if (_lastAngle.z == angleA && _lastAngle.x != angleA && _lastAngle.y != angleB)
                        {
                            angle = angleB;
                        }
                        if (!StaticTools.ThresholdApproximately(DiffA, DiffB, MAX_Y_DISTIANCE_FOR_ADJUSRMENT))// The rays are at a sing distance aprat
                        {
                            if (DiffA > DiffB)// The A Angle Ray is off the edge of somting 
                            {
                                angle = angleB;
                            }

                            if (DiffA < DiffB)// the B Angle Ray is off the edge 
                            {
                                angle = angleA;
                            }
                        }
                    }

                    //The Angel is 90 Degrees off 
                    //Debug.Log("Found Angle: "+angle);
                    _lastAngle.x = angleA;
                    _lastAngle.y = angleB;
                    _lastAngle.z = angle;
                    if (angle > 0)
                    {
                        angle = 90 - angle;
                    }
                    else if (angle < 0)
                    {
                        angle = -(angle + 90);// Fight against gimble lock
                    }

                    ///adds some X scale to strech while on an angel 
                    float offset = 0.5f;
                    ///Closer to 180 = More offset
                    offset *= Mathf.Abs(angle) / 90;

                    //Debug.Log("Calced Angle: " + angle);



                    ShadowTransfrom.transform.parent.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                    ShadowTransfrom.transform.parent.transform.localScale = new Vector3(1 + offset, 1, 1);


                    /*Debug.DrawLine(TrakingGameObject.transform.position, hit2Ds[MAIN_RAY].point, Color.red, 133);
                    Debug.DrawLine(TrakingGameObject.transform.position + (Vector3.left / 2), hit2Ds[OFFSET_RAY].point, Color.yellow, 133);
                    Debug.DrawLine(hit2Ds[OFFSET_RAY].point, new Vector2(hit2Ds[MAIN_RAY].point.x, hit2Ds[OFFSET_RAY].point.y), Color.green, 33);
                    Debug.DrawLine(hit2Ds[OFFSET_RAY].point, hit2Ds[MAIN_RAY].point, Color.cyan, 33);
                    Debug.DrawLine(new Vector2(hit2Ds[MAIN_RAY].point.x, hit2Ds[OFFSET_RAY].point.y), hit2Ds[MAIN_RAY].point, Color.blue, 33);*/

                    Rotation?.Invoke(ShadowTransfrom.transform.parent.transform.eulerAngles.z);
                    return;


                }

                // no angle
                ShadowTransfrom.transform.parent.transform.localScale = new Vector3(1, 1, 1);
                ShadowTransfrom.transform.parent.transform.rotation = Quaternion.Euler(0, 0, 0);
                ShadowTransfrom.transform.position = hit2Ds[MAIN_RAY].point;

                _renderRef.SetPropertyBlock(_materialPropertyBlock);
                Rotation?.Invoke(0);
            }
        }
    }

    private void OnBecameVisible()
    {
        _objectOnScreen = true;
    }

    private void OnBecameInvisible()
    {
        _objectOnScreen = false;
    }
}
