﻿using UnityEngine;
using System.Collections;

public class MakePlayerDynamicTrigger : MonoBehaviour
{
    /// <summary>
    /// This is to make sure only the zone setting can unset
    /// </summary>
    private static MakePlayerDynamicTrigger _contrllingZone = null;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == GlobalData.PLAYER_TAG)
        {
            if (!_contrllingZone)// if a zone is not already  controlling  this 
            {
                GlobalData.Player.Rigidbody2D.isKinematic = false;
               _contrllingZone = this;
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == GlobalData.PLAYER_TAG)
        {
            if (_contrllingZone == this)//only the trigger that set it can release it 
            {
                GlobalData.Player.Rigidbody2D.isKinematic = true;
                _contrllingZone = null;
            }
            else if (GlobalData.Player.Rigidbody2D.isKinematic == false)//look for external changes IE a MakePlayerDynamicActivator or other script
            {
                _contrllingZone = null;
            }
        }
    
    }
}
