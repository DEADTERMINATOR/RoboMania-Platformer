﻿using UnityEngine;
using System.Collections;

public class MakePlayerDynamicActivator : MonoBehaviour, IActivatable
{
    public bool Active { get { return !GlobalData.Player.Rigidbody2D.isKinematic; } }

    public void Activate(GameObject activator)
    {
        GlobalData.Player.Rigidbody2D.isKinematic = false;
    }

    public void Deactivate(GameObject activator)
    {
        GlobalData.Player.Rigidbody2D.isKinematic = true;
    }
}
