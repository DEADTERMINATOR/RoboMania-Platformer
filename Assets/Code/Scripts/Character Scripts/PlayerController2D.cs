﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;
using System;
using UnityEngine.UIElements;
using LevelComponents.Platforms;
using Characters;

public class PlayerController2D : GroundController2D
{
    private const float GRAB_DISTANCE = 1f;

    #region Collision Info
    public class PlayerCollisionInfo : GroundCollisionInfo
    {
        /// <summary>
        /// The percentage of raycasts fired out horizontally that collide with an object.
        /// </summary>
        public float PercentageCollidingHorizontal;

        /// <summary>
        /// The game object that this object is colliding with horizontally.
        /// </summary>
        public GameObject HorizontalCollidingObject;

        /// <summary>
        /// The percentage of raycasts fired out vertically that collide with an object.
        /// </summary>
        public float PercentageCollidingVertical;

        /// <summary>
        /// The game object that this object is colliding with vertically.
        /// </summary>
        public GameObject VerticalCollidingObject;

        /// <summary>
        /// The count of the first horizontal ray to collide with an object.
        /// </summary>
        public int StartingRaycastColliding;

        /// <summary>
        /// The position of the first horizontal ray to collide with an object.
        /// </summary>
        public Vector3 StartingRaycastingCollidingPosition;

        /// <summary>
        /// The count of the last horizontal ray to collide with an object.
        /// </summary>
        public int EndingRaycastColliding;


        public override void Reset()
        {
            base.Reset();

            PercentageCollidingVertical = 0f;
            PercentageCollidingHorizontal = 0f;

            VerticalCollidingObject = null;
            HorizontalCollidingObject = null;
        }

        /// <summary>
        /// Checks if the bottom area of the object's horizontal collider is colliding with an object. To be used in conjunction with a check on how much of the horizontal collider is colliding.
        /// </summary>
        /// <param name="totalHorizontalRaycastCount">The total number of rays the controller fires horizontally.</param>
        /// <returns></returns>
        public bool IsBottomCollidingHorizontally(int totalHorizontalRaycastCount, CollisionRaycaster2D raycaster)
        {
            //Use the top most rays to check if we are clear to climb up. This covers cases where the clamber may trigger because the wall collision isn't perfectly straight.
            //Check the top most ray.
            raycaster.SetCastOrigin(FaceDir == 1 ? raycaster.Origins.TopRight : raycaster.Origins.TopLeft);
            raycaster.SetCastLength(0.5f);
            int topRayResultCount = raycaster.FireCast('X', FaceDir);

            if (topRayResultCount != 0)
            {
                //The top ray hit something, so we don't clamber.
                return false;
            }

            //Check the lowest ray that doesn't count for clambering.
            raycaster.SetCastOrigin(FaceDir == 1 ? raycaster.Origins.TopRight : raycaster.Origins.TopLeft);
            raycaster.OffsetRayOrigin('X', raycaster.HorizontalRaySpacing * (Mathf.Floor(totalHorizontalRaycastCount / 2) - 1));
            int lowerRayResultCount = raycaster.FireCast('X', FaceDir);

            if (lowerRayResultCount != 0)
            {
                //The lower ray hit something, so we don't clamber.
                return false;
            }

            //If the ray tests passed, then we continue on and check if the rest of the collision meets our criteria.

            //We get the total number of rays that should be colliding based on the calculated horizontal collision percentage and the total number of rays fired.
            float expectedTotalRaysColliding = Mathf.Ceil(totalHorizontalRaycastCount * PercentageCollidingHorizontal);

            //If the first ray to collide is in the bottom half of rays fired, and the total number of collided rays added to this starting ray does not exceed the max number of rays fired,
            //then we can assume that we have a continuous section of rays on the bottom portion colliding. 
            return StartingRaycastColliding >= Mathf.Floor(totalHorizontalRaycastCount / 2) && StartingRaycastColliding + expectedTotalRaysColliding <= totalHorizontalRaycastCount;
        }

        /// <summary>
        /// Checks if the given raycast is colliding horizontally.
        /// </summary>
        /// <param name="raycastNumToTest">The number of the raycast to test.</param>
        /// <param name="raycaster">The raycaster used to do the raycasting.</param>
        /// <param name="rayLength">The length the horizontal ray should be for the test. If the parameter is negative, the currnet ray length is used.</param>
        /// <returns>True if the raycast is colliding, false otherwise.</returns>
        public bool IsGivenRayCollidingHorizontally(int raycastNumToTest, CollisionRaycaster2D raycaster, float rayLength = -1)
        {
            if (raycastNumToTest > raycaster.HorizontalRayCount)
            {
                return false;
            }

            raycaster.SetCastOrigin(FaceDir == 1 ? Vector2.right : Vector2.left);
            raycaster.OffsetRayOrigin('X', raycaster.HorizontalRaySpacing * raycastNumToTest - 1);

            if (rayLength >= 0)
            {
                raycaster.SetCastLength(rayLength);
            }

            int rayTest = raycaster.FireCast('X', FaceDir);
            return rayTest != 0;
        }
    }
    #endregion


    /// <summary>
    /// Getter for the collision info property cast to its actual class.
    /// </summary>
    public PlayerCollisionInfo PlayerCollisions { get { return (PlayerCollisionInfo)Collisions; } }


    [SerializeField]
    private LayerMask _noVelocityCollisionMask;

    private PlayerMaster _playerRef;


    protected override void Awake()
    {
        base.Awake();
        Collisions = new PlayerCollisionInfo();
	}

    private void Start()
    {
        _playerRef = GlobalData.Player;
    }

    #region Move
    public override void Move(ref Vector3 velocity, char performCollisionCheckOnProvidedAxis = 'B', char ejectFromColliderOnProvidedAxis = 'N', bool moveImmediately = false, Space translateRelativeTo = Space.World, LayerMask? collisionMask = null)
    {
        var moveableObject = _playerRef.State.GrabbedObject as PlayerMoveableObject;
        var canPush = false;
        var canPull = false;
        float xVelocityDir = Mathf.Sign(velocity.x);

        if (velocity.x != 0 && _playerRef.State.Grabbing && moveableObject != null)
        {
            //If the player is grabbing onto a moveable object, check which direction that object is in to determine whether it's a push or a pull based on the player's desired velocity.
            if (CheckForGrabbableObject(xVelocityDir) != null)
            {
                canPush = true;
            }
            else
            {
                if (CheckForGrabbableObject(-xVelocityDir) != null)
                {
                    canPull = true;
                }
            }

            if (!canPush && !canPull)
            {
                //If the player is not in range to push or pull the object, cancel the grab.
                _playerRef.State.SetGrabbing(false);
            }
        }

        if (canPush)
        {
            //If the player is trying to push a moveable object, we need to do this before moving the player.
            moveableObject.Push(new Vector2(xVelocityDir, 0));
        }

        CalculateMove(ref velocity, performCollisionCheckOnProvidedAxis, ejectFromColliderOnProvidedAxis);

        if (!Collisions.PreviousBelow && Collisions.IsCollidingBelow)
        {
            _playerRef.NotifyCollidedBelow(velocity);
        }

        if (!_playerRef.State.InsideCameraMovementAreaOverrideZone)
        {
            //Create a test game object modeled like the player at the position that they will be if this move is executed and check if they are still within a camera movement area.
            //If they aren't, don't let them move.
            GameObject predictedPlayerPositionGO = new GameObject();
            predictedPlayerPositionGO.transform.position = GlobalData.Player.transform.position;
            predictedPlayerPositionGO.transform.Translate(velocity);
            predictedPlayerPositionGO.layer = GlobalData.PLAYER_LAYER;
            BoxCollider2D predictedPlayerPositionCollider = predictedPlayerPositionGO.AddComponent<BoxCollider2D>();
            predictedPlayerPositionCollider.size = GlobalData.Player.MovementCollider.size;
            predictedPlayerPositionCollider.offset = GlobalData.Player.MovementCollider.offset;

            RaycastHit2D hit = Physics2D.BoxCast(predictedPlayerPositionGO.transform.position, predictedPlayerPositionCollider.size, 0, Vector2.right, 0, GlobalData.CAMERA_MOVEMENT_SHIFTED);
            if (hit.collider != null)
            {
                if (moveImmediately)
                {
                    objectToControl.transform.Translate(velocity, translateRelativeTo);
                }
                else
                {
                    if (translateRelativeTo == Space.Self)
                    {
                        var newPosition = _rigidbody2D.position + (Vector2)objectToControl.transform.TransformDirection(velocity);
                        _rigidbody2D.MovePosition(newPosition);
                    }
                    else
                    {
                        _rigidbody2D.MovePosition(_rigidbody2D.position + (Vector2)velocity);
                    }
                }
            }

            if (canPull && velocity.x != 0 && Collisions.IsCollidingBelow)
            {
                //If the player is trying to pull a moveable object, we need to do this after moving the player.
                //We need to check the x velocity and whether they are colliding below a second time because it's possible the player was unable to move anywhere, or they are now falling after moving.
                bool pullSuccess = moveableObject.Pull(new Vector2(xVelocityDir, 0));
                if (!pullSuccess)
                {
                    //If the pull fails for whatever reason, we don't want the player moving away from the object like their pulling it without the pulled object moving with them. So undo the movement that was just done.
                    objectToControl.transform.Translate(-velocity, translateRelativeTo);
                }
            }

            Destroy(predictedPlayerPositionGO);
        }
        else
        {
            if (moveImmediately)
            {
                objectToControl.transform.Translate(velocity, translateRelativeTo);
            }
            else
            {
                if (translateRelativeTo == Space.Self)
                {
                    var newPosition = _rigidbody2D.position + (Vector2)objectToControl.transform.TransformDirection(velocity);
                    _rigidbody2D.MovePosition(newPosition);
                }
                else
                {
                    _rigidbody2D.MovePosition(_rigidbody2D.position + (Vector2)velocity);
                }
            }
        }

        if (!Collisions.IsCollidingAbove && !Collisions.IsCollidingBelow && !Collisions.IsCollidingRight && !Collisions.IsCollidingLeft)
        {
            _playerRef.NotifyNoCollision(velocity);
        }
    }
    #endregion

    #region Grabbing
    /// <summary>
    /// Checks if there is any object within GRAB_DISTANCE that is grabbable, and grabs it if so. Alternatively, if the player is already grabbing something,
    /// the methods ensures the player is still within an acceptable range of the object to continue grabbing it, and releases it if they are not.
    /// </summary>
    public void TryOrMaintainGrab(float direction)
    {
        var playerIsGrabbing = _playerRef.State.Grabbing;
        var iGrabbable = CheckForGrabbableObject(direction, playerIsGrabbing ? GRAB_DISTANCE + CollisionCaster2D.SKIN_WIDTH : GRAB_DISTANCE);

        if (!playerIsGrabbing && !_playerRef.Weapon.IsAiming)
        {
            if (iGrabbable != null)
            {
                _playerRef.State.SetGrabbing(true, iGrabbable);
            }
        }
        else
        {
            if (iGrabbable == null)
            {
                _playerRef.State.SetGrabbing(false);
            }
        }
    }

    /// <summary>
    /// Fires out a boxcast in front of the player to see if anything within the provided distance is grabbable.
    /// </summary>
    private IGrabbable CheckForGrabbableObject(float direction, float distance = GRAB_DISTANCE)
    {
        Boxcaster.UpdateCastOrigins();

        Boxcaster.SetCastOrigin(Boxcaster.Origins.Center);
        Boxcaster.SetBoxSize(new Vector2(GRAB_DISTANCE, PlayerMaster.PLAYER_VERTICAL_EXTENTS));
        var hitCount = Boxcaster.FireCast('X', direction);

        for (int i = 0; i < hitCount; ++i)
        {
            var iGrabbable = Boxcaster.Hits[i].collider.GetComponent<IGrabbable>();
            if (iGrabbable != null)
            {
                return iGrabbable;
            }
        }

        return null;
    }
    #endregion

    #region Horizontal Collision Check
    /// <summary>
    /// Checks for any collision on the X-axis. This version contains handling for sloped surfaces.
    /// </summary>
    /// <param name="velocity">The current velocity of the object.</param>
    protected override void HorizontalCollisions(ref Vector3 velocity, bool ejectFromCollider, bool drawRaycasts, LayerMask? collisionMask = null)
    {
        PlayerCollisionInfo playerCollisions = (PlayerCollisionInfo)Collisions;

        int horizontalRaysHit = 0;

        float lastHitDistance = Mathf.Abs(velocity.x) + CollisionRaycaster2D.SKIN_WIDTH;
        float shortestHitDistance = 999f;

        //Holds the current extension of the raycast distance that is done to ensure that an accurate count of the number of raycasts actually colliding with a surface is counted.
        //But, since we don't want to apply this extension to any velocity modification, we need to subtract it off any modification.
        //We start it at 0 since we don't want to apply the extension until we have at least 1 confirmed hit.
        float raycastDistanceExtension = 0;

        bool climbSlopeCalculated = false;
        float currentSlopeAngle = 0;

        float xDirection = Mathf.Sign(velocity.x);

        playerCollisions.StartingRaycastColliding = Raycaster.HorizontalRayCount + 1;
        playerCollisions.EndingRaycastColliding = -1;

        for (int i = 0; i < Raycaster.HorizontalRayCount; ++i)
        {
            Raycaster.SetCastLength(lastHitDistance);

            Raycaster.SetCastOrigin('X', xDirection);
            Raycaster.OffsetRayOrigin('X', Raycaster.HorizontalRaySpacing * i);

            RaycastHit2D hit;
            if (velocity.x != 0)
            {
                if (drawRaycasts)
                {
                    hit = Raycaster.FireAndDrawCastAndReturnFirstResult('X', xDirection, Color.yellow, RAYCAST_DRAW_TIME, collisionMask);
                }
                else
                {
                    hit = Raycaster.FireCastAndReturnFirstResult('X', xDirection, collisionMask);
                }
            }
            else
            {
                if (drawRaycasts)
                {
                    hit = Raycaster.FireAndDrawCastAndReturnFirstResult('X', xDirection, Color.yellow, RAYCAST_DRAW_TIME, _noVelocityCollisionMask);
                }
                else
                {
                    hit = Raycaster.FireCastAndReturnFirstResult('X', xDirection, _noVelocityCollisionMask);
                }
            }

            if (hit)
            {
                ++horizontalRaysHit;

                lastHitDistance = hit.distance + 0.01f;

                //The raycasting starts from the bottom and goes up. But for counting the raycasts, we want to go the other way (top to bottom).
                //So all the checks need to be inverted.
                if (Raycaster.HorizontalRayCount - i - 1 < playerCollisions.StartingRaycastColliding)
                {
                    playerCollisions.StartingRaycastColliding = Raycaster.HorizontalRayCount - i - 1;
                    playerCollisions.StartingRaycastingCollidingPosition = hit.point;
                }
                if (Raycaster.HorizontalRayCount - i - 1 > playerCollisions.EndingRaycastColliding)
                {
                    playerCollisions.EndingRaycastColliding = Raycaster.HorizontalRayCount - i - 1;
                }

                playerCollisions.HorizontalCollidingObject = hit.collider.gameObject;

                if (hit.distance == 0)
                {
                    velocity.x = 0;

                    if (ejectFromCollider)
                    {
                        Vector3 pointOnCollider = StaticTools.ClosestPoint(hit.collider, hit.point);
                        Vector2 directionOfPoint = new Vector2(pointOnCollider.x, pointOnCollider.y) - hit.point;

                        objectToControl.transform.Translate(new Vector3(pointOnCollider.x - hit.point.x + X_EJECT_DISTANCE * Mathf.Sign(directionOfPoint.x), pointOnCollider.y - hit.point.y + Y_EJECT_DISTANCE * Mathf.Sign(directionOfPoint.y), 0), Space.World);
                    }

                    continue;
                }

                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                //We normally only want to do this check once, however, in the case of subtle angle changes when travelling at high velocity (e.g. dash), a recalculation may be necessary
                //if we detect a different angle in the middle of the collision check.
                if ((!climbSlopeCalculated || (climbSlopeCalculated && slopeAngle != currentSlopeAngle)) && slopeAngle <= maxAngle)
                {
                    if (playerCollisions.DescendingSlope)
                    {
                        playerCollisions.DescendingSlope = false;
                        velocity = Collisions.StartingVelocity;
                    }

                    float distanceToSlopeStart = 0;
                    if (slopeAngle != playerCollisions.PreviousSlopeAngle)
                    {
                        distanceToSlopeStart = hit.distance - CollisionCaster2D.SKIN_WIDTH;
                        velocity.x -= distanceToSlopeStart * xDirection;
                    }
                    ClimbSlope(ref velocity, slopeAngle, hit.collider.gameObject);
                    velocity.x += distanceToSlopeStart * xDirection;

                    climbSlopeCalculated = true;
                    currentSlopeAngle = slopeAngle;
                }

                if (!playerCollisions.ClimbingSlope || Mathf.Round(slopeAngle) > maxAngle)
                {
                    //TODO: Put some sort of flag here to start a sliding animation.
                    if (hit.distance < shortestHitDistance)
                    {
                        velocity.x = (hit.distance - raycastDistanceExtension - CollisionCaster2D.SKIN_WIDTH) * xDirection;
                        shortestHitDistance = hit.distance;

                        if (slopeAngle <= 90)
                        {
                            raycastDistanceExtension = 0.01f;
                        }
                    }
                    Raycaster.SetCastLength(hit.distance + raycastDistanceExtension);

                    if (playerCollisions.ClimbingSlope)
                    {
                        velocity.y = Mathf.Tan(playerCollisions.SlopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x);
                    }

                    if (xDirection == -1)
                    {
                        Collisions.Left.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                    }
                    else
                    {
                        Collisions.Right.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                    }
                }
            }

            //Also check if we're colliding in the direction we aren't travelling (which may happen if we are being pushed by something).
            Raycaster.SetCastOrigin('X', -xDirection);
            Raycaster.OffsetRayOrigin('X', Raycaster.HorizontalRaySpacing * i);
            Raycaster.SetCastLength(CollisionCaster2D.SKIN_WIDTH * 2);

            hit = Raycaster.FireCastAndReturnFirstResult('X', -xDirection, _noVelocityCollisionMask);

            if (hit)
            {
                if (-xDirection == -1)
                {
                    Collisions.Left.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                }
                else
                {
                    Collisions.Right.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                }
            }
        }

        playerCollisions.PercentageCollidingHorizontal = (float)horizontalRaysHit / (float)Raycaster.HorizontalRayCount;
    }
    #endregion

    #region Vertical Collision Check
    /// <summary>
    /// Checks for any collision on the Y-axis. This version contains handling for slopes.
    /// </summary>
    /// <param name="velocity">The current velocity of the object.</param>
    protected override void VerticalCollisions(ref Vector3 velocity, bool ejectFromCollider, bool checkForPlatform, bool drawRaycasts, LayerMask? collisionMask = null)
    {
        PlayerCollisionInfo playerCollisions = (PlayerCollisionInfo)Collisions;

        float lastHitDistance = Mathf.Abs(velocity.y) + CollisionRaycaster2D.SKIN_WIDTH;
        float yDirection = Mathf.Sign(velocity.y);

        int verticalRaysHit = 0;

        for (int i = 0; i < Raycaster.VerticalRayCount; ++i)
        {
            Raycaster.SetCastLength(lastHitDistance);

            Raycaster.SetCastOrigin('Y', yDirection);
            Raycaster.OffsetRayOrigin('Y', Raycaster.VerticalRaySpacing * i + velocity.x);

            RaycastHit2D hit = new RaycastHit2D();
            if (drawRaycasts)
            {
                hit = Raycaster.FireAndDrawCastAndReturnFirstResult('Y', yDirection, Color.cyan, RAYCAST_DRAW_TIME, collisionMask);
            }
            else
            {
                hit = Raycaster.FireCastAndReturnFirstResult('Y', yDirection, collisionMask);
            }

            if (hit)
            {
                ++verticalRaysHit;

                lastHitDistance = hit.distance;
                playerCollisions.VerticalCollidingObject = hit.collider.gameObject;

                if (playerCollisions.ClimbingSlope)
                {
                    velocity.x = velocity.y / Mathf.Tan(playerCollisions.SlopeAngle * Mathf.Deg2Rad) * Mathf.Sign(velocity.x);
                }

                if (hit.distance == 0)
                {
                    if (!hit.collider.isTrigger)
                    {
                        velocity.y = 0;

                        if (ejectFromCollider && TheGame.GetRef.gameState != TheGame.GameState.TRANSITION)
                        {
                            Vector3 pointOnCollider = StaticTools.ClosestPoint(hit.collider, hit.point);
                            Vector2 directionOfPoint = new Vector2(pointOnCollider.x, pointOnCollider.y) - hit.point;

                            objectToControl.transform.Translate(new Vector3(pointOnCollider.x - hit.point.x + X_EJECT_DISTANCE * Mathf.Sign(directionOfPoint.x), pointOnCollider.y - hit.point.y + Y_EJECT_DISTANCE * Mathf.Sign(directionOfPoint.y), 0));
                        }

                        continue;
                    }
                    else
                    {
                        return;
                    }

                }

                if (yDirection == -1)
                {
                    Collisions.Below.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                }
                else
                {
                    Collisions.Above.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                }

                if (Collisions.IsCollidingBelow)
                {
                    Collisions.BelowSurfaceNormal = hit.normal;
                }

                var newYVelocity = (hit.distance - CollisionRaycaster2D.SKIN_WIDTH) * yDirection;

                if (TheGame.GetRef.gameState == TheGame.GameState.TRANSITION && newYVelocity > 0)
                {
                    //If the game is going through an area transition, don't respond to Y velocities greater that 0 because it's likely
                    //a result of the player walked through a surface during the transition, and we don't want the weird collision issues
                    //that can accompany that.
                    newYVelocity = 0;
                }
                velocity.y = newYVelocity;
            }

            //Also check if we're colliding in the direction we aren't travelling (which may happen if we are being pushed by something).
            Raycaster.SetCastOrigin('Y', -yDirection);
            Raycaster.OffsetRayOrigin('Y', Raycaster.VerticalRaySpacing * i + velocity.x);
            Raycaster.SetCastLength(CollisionCaster2D.SKIN_WIDTH * 2);

            hit = Raycaster.FireCastAndReturnFirstResult('Y', -yDirection, _noVelocityCollisionMask);

            if (hit)
            {
                if (-yDirection == -1)
                {
                    Collisions.Below.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                }
                else
                {
                    Collisions.Above.SetColliding(hit.collider.gameObject.layer, hit.collider.gameObject);
                }
            }
        }

        playerCollisions.PercentageCollidingVertical = (float)verticalRaysHit / (float)Raycaster.VerticalRayCount;

        if (checkForPlatform && velocity.y < PLATFORM_SCAN_DISTANCE)
        {
            CastForPlatformBelow(ref velocity, drawRaycasts);
        }

        if (playerCollisions.ClimbingSlope)
        {
            //If we are climbing a slope, we want to check at the Y value that we will be if there is a slope with a different angle than we are currently on.
            //If so, we need to handle that.
            float xDirection = Mathf.Sign(velocity.x);

            Raycaster.SetCastLength(velocity.x);
            Raycaster.SetCastOrigin('X', xDirection);
            Raycaster.OffsetRayOrigin('X', velocity.y);
            RaycastHit2D hit = Raycaster.FireCastAndReturnFirstResult('X', xDirection);

            if (hit)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle != playerCollisions.SlopeAngle)
                {
                    velocity.x = (hit.distance - CollisionRaycaster2D.SKIN_WIDTH) * xDirection;
                    playerCollisions.SlopeAngle = slopeAngle;
                }
            }
        }
    }
    #endregion

    #region Internal Collision Check
    public float CheckInternalCollision()
    {
        int lastRaycastHit = -1;
        float distanceToEject = -999;

        Raycaster.SetCastLength(Raycaster.VerticalRaySpacing * (Raycaster.VerticalRayCount - 1));

        for (int i = 0; i < Raycaster.HorizontalRayCount; ++i)
        {
            Raycaster.SetCastOrigin(Raycaster.Origins.BottomLeft);
            Raycaster.OffsetRayOrigin('X', Raycaster.HorizontalRaySpacing * i);
            int hitResutCount = Raycaster.FireCast('X', 1.0f);

            if (hitResutCount != 0 && i > lastRaycastHit)
            {
                lastRaycastHit = i;
            }
        }

        if (lastRaycastHit != -1)
        {
            distanceToEject = Raycaster.HorizontalRaySpacing * lastRaycastHit;
        }

        return distanceToEject;
    }
    #endregion
}
