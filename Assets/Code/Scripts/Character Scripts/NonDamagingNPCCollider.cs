﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonDamagingNPCCollider : EnemyCollider
{
	// Use this for initialization
	protected override void Start()
    {
        base.Start();	
	}


    public override void TakeDamage(IDamageGiver giver, float amount, GlobalData.DamageType type, Vector3 hitPoint)
    {
        base.TakeDamage(giver, 0, type,hitPoint);
    }
}
