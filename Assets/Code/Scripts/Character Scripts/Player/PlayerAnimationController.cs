﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;
using Spine;
using Spine.Unity;
using static StaticTools;
using LevelSystem;
using UnityEngine.UIElements;

namespace Characters.Player
{
    public class PlayerAnimationController
    {
        #region Constant Values
        /// <summary>
        /// The damage flash color converted from 0-255 RGBA value to 0-1 RGBA.
        /// </summary>
        private Vector4 DAMAGE_FLASH_COLOUR = new Vector4(1, 50f / 255f, 0, 1);//new Vector4(1, 125f / 255f, 0, 1);

        private const float RUN_TIMESCALE_MULTIPLIER = 1.5f;
        private const float JUMP_TIMESCALE_MULTIPLIER = 1.5f;
        private const float MULTI_JUMP_TIMESCALE_MULTIPLIER = 2f;
        private const float SPRITE_ROTATION_ASCENDING_DESCENDING_MULTIPLIER = 0.75f;

        private const string EVERYTHING_ELSE_ANIMATION_SUFFIX = " (Everything Else)";
        private const string ARMS_AND_HEAD_ANIMATION_SUFFIX = " (Right Arm + Head)";
        private const string ARM_ONLY_SUFFIX = " (Right Arm)";
        private const string HEAD_ONLY_SUFFIX = " (Head)";

        private const int MAIN_ANIMATION_TRACK = 0;
        private const int SECONDARY_ANIMATION_TRACK = 1;
        private const int TERTIARY_ANIMATION_TRACK = 2;
        private const int ARMS_AND_HEAD_ANIMATION_PRIMARY_TRACK_NUMBER = 3;
        private const int HEAD_ONLY_ANIMATION_TRACK_NUMBER = 4;

        private const int INVULNERABILITY_FLASHES = 20;

        #region Spine Animation Slot Values
        /* These are the indices of the various Player slots as Spine stores them.
         * Since there does not appear to be any obvious rhyme or reason to them, they are stored here for reference. */
        private const int LEFT_FOREARM = 0;
        private const int LEFT_HAND = 1;
        private const int LEFT_UPPER_ARM = 2;
        private const int LEFT_FOREARM_WALL_SLIDING = 3;
        private const int LEFT_JETBOOT_FIRE = 4;
        private const int LEFT_FOOT = 5;
        private const int LEFT_UPPER_LEG = 6;
        private const int LEFT_LEG = 7;
        private const int PELVIS = 8;
        private const int TORSO_AND_NECK = 9;
        private const int RIGHT_FOREARM_WALL_GRIPPING = 10;
        private const int RIGHT_UPPER_ARM_WALL_GRIPPING = 11;
        private const int HEAD = 12;
        private const int SIDE_EYE = 13;
        private const int LEFT_FRONT_EYE = 14;
        private const int RIGHT_FRONT_EYE = 15;
        private const int RIGHT_UPPER_LEG = 16;
        private const int RIGHT_JETBOOT_FIRE = 17;
        private const int RIGHT_FOOT = 18;
        private const int RIGHT_LEG = 19;
        private const int HOLSTERED_RIFLE = 20;
        private const int RIFLE = 21;
        private const int RIGHT_HAND = 22;
        private const int RIGHT_UPPER_ARM = 23;
        private const int RIGHT_FOREARM = 24;
        private const int RIGHT_FOREARM_LASER_AIMER = 25;
        #endregion
        #endregion

        #region Structs + Enums
        public struct PlayingAnimation
        {
            public enum HeadAndArmsAnimationMode { None, HeadAndArmsSameAnimation, HeadAndArmsSeparateAnimations, HeadOnly, ArmsOnly }
            public TrackEntry Animation { get; private set; }
            public string AnimationName { get; private set; }
            public Animations AnimationEnumName { get; private set; }
            public int TrackNumber { get; private set; }
            public bool ShouldLoop { get; private set; }
            public HeadAndArmsAnimationMode HeadAndArmsMode { get; private set; }
            public TrackEntry ArmsAnimation { get; private set; }
            public TrackEntry HeadAnimation { get; private set; }

            public PlayingAnimation(string animationName, Animations animationEnumName, bool shouldLoop, HeadAndArmsAnimationMode headAndArmsMode)
                => (AnimationName, AnimationEnumName, TrackNumber, ShouldLoop, HeadAndArmsMode, Animation, ArmsAnimation, HeadAnimation) = (animationName, animationEnumName, -1, shouldLoop, headAndArmsMode, null, null, null);

            public void SetAnimation(TrackEntry animation = null, int trackNumber = -1)
            {
                Animation = animation;
                TrackNumber = trackNumber;
            }

            public void SetArmsAnimation(TrackEntry armsAnimation = null)
            {
                ArmsAnimation = armsAnimation;
            }

            public void SetHeadAnimation(TrackEntry headAnimation = null)
            {
                HeadAnimation = headAnimation;
            }
        }

        public enum Animations { Idle, Walk, Run, Jump, MultiJump, Fall, WallSlide, WallGripFire, Hover, Fire, FireMediumRecoil, FireHeavyRecoil, GroundDeath, AirDeath, EnterShip, Push, PushIdle, Pull, PullIdle, HorizontalClimb, HorizontalClimbIdle };
        #endregion

        #region Public Properties
        public SkeletonAnimation SkeletonAnimation;

        /// <summary>
        /// The state machine that handles the player's current animation state and transitions to any other animation as necessary.
        /// </summary>
        public StateMachine AnimationStateMachine { get; private set; }

        /// <summary>
        /// Reference to the coroutine that handles the effect when the player takes damage.
        /// </summary>
        public CoroutineHandle DamageFlashHandle { get; private set; }

        /// <summary>
        /// Reference to the coroutine that handles the effect when the player is in their invulnerability period after taking damage.
        /// </summary>
        public CoroutineHandle InvulnerabilityFlashHandle { get; private set; }

        /// <summary>
        /// Reference to the game object reprsenting the main player sprite.
        /// </summary>
        public GameObject MainSprite { get; private set; }

        /// <summary>
        /// Reference to the main sprite's mesh renderer.
        /// </summary>
        public MeshRenderer MainSpriteRenderer { get; private set; }

        public Bone RootBone { get { return _rootBone; } }
        public Bone FullBodyBone { get { return _fullBodyBone; } }
        public Bone RightUpperArmBone { get { return _rightUpperArmBone; } }
        public Bone RightForearmBone { get { return _rightForearmBone; } }
        public Bone HeadBone { get { return _headBone; } }
        public Bone AimingPoint { get { return _aimingPoint; } }
        public Bone FiringPoint { get { return _firingPoint; } }
        public Bone LightPoint { get { return _lightPoint; } }

        public PlayingAnimation MainAnimation { get { return _mainAnimation; } }
        public PlayingAnimation SideAnimation { get { return _sideAnimation; } }
        #endregion

        #region Private Variables
        /* Commonly accessed skeletal bones. */
        private Bone _rootBone;
        private Bone _fullBodyBone;
        private Bone _rightUpperArmBone;
        private Bone _rightForearmBone;
        private Bone _headBone;
        private Bone _aimingPoint;
        private Bone _firingPoint;
        private Bone _lightPoint;

        private PlayingAnimation _mainAnimation;
        private PlayingAnimation _sideAnimation;

        /// <summary>
        /// Reference to the player's main collider.
        /// </summary>
        private BoxCollider2D _playerCollider;

        /// <summary>
        /// Reference to the player's master class.
        /// </summary>
        private PlayerMaster _playerRef;

        private bool _playerOnGroundAfterDeath = false;

#pragma warning disable 649 //value is never assigned to and will always be false
        private bool _walk;
#pragma warning restore 649
        private bool _jump;
        private bool _multiJump;
        private bool _enteringShip;

        /* Shortcut variable for getting to the commonly used Skeleton property of SkeletonAnimation. */
        private readonly Skeleton _skeleton;
        /* Shortcut variable for getting to the commonly used AnimationState property of SkeletonAnimation. */
        private readonly Spine.AnimationState _animationState;
        #endregion

        #region Instantiation
        // Use this for initialization
        public PlayerAnimationController(BoxCollider2D playerCollider, PlayerMaster playerRef)
        {
            MainSprite = playerRef.transform.Find("Character").gameObject;

            MovingDroopShadow dropShadow = playerRef.transform.Find("ShadowBlobParent/ShadowQuad").GetComponent<MovingDroopShadow>();
            dropShadow.Rotation += new MovingDroopShadow.ReportRotation((float zRotation) =>
            {
                float signedZRotation = zRotation > 240 ? zRotation - 360 : zRotation;
                float faceDir = playerRef.Collisions.FaceDir;

                if (playerRef.Collisions.IsCollidingBelow || CollisionRaycaster2D.PointDistanceFromGround(new Vector2(playerRef.transform.position.x, playerRef.transform.position.y - playerRef.MovementCollider.bounds.extents.y)) < 1.0f)
                {
                    if ((faceDir == 1 && signedZRotation > 0) || (faceDir == -1 && signedZRotation < 0)) //Ascending Slope
                {
                        playerRef.Sprite.transform.rotation = Quaternion.Euler(playerRef.Sprite.transform.eulerAngles.x, playerRef.Sprite.transform.eulerAngles.y,
                            faceDir == -1 ? signedZRotation + Mathf.Abs(signedZRotation) * SPRITE_ROTATION_ASCENDING_DESCENDING_MULTIPLIER : signedZRotation - Mathf.Abs(signedZRotation) * SPRITE_ROTATION_ASCENDING_DESCENDING_MULTIPLIER);
                    }
                    else if ((faceDir == -1 && signedZRotation > 0) || (faceDir == 1 && signedZRotation < 0)) //Descending Slope
                {
                        playerRef.Sprite.transform.rotation = Quaternion.Euler(playerRef.Sprite.transform.eulerAngles.x, playerRef.Sprite.transform.eulerAngles.y,
                            faceDir == -1 ? signedZRotation - Mathf.Abs(signedZRotation) * SPRITE_ROTATION_ASCENDING_DESCENDING_MULTIPLIER : signedZRotation + Mathf.Abs(signedZRotation) * SPRITE_ROTATION_ASCENDING_DESCENDING_MULTIPLIER);
                    }
                    else
                        playerRef.Sprite.transform.rotation = Quaternion.Euler(playerRef.Sprite.transform.eulerAngles.x, playerRef.Sprite.transform.eulerAngles.y, zRotation);
                }
                else
                {
                    playerRef.Sprite.transform.rotation = Quaternion.identity;
                }
            });

            SkeletonAnimation = MainSprite.GetComponent<SkeletonAnimation>();
            _skeleton = SkeletonAnimation.Skeleton;
            _animationState = SkeletonAnimation.AnimationState;

            MainSpriteRenderer = MainSprite.GetComponent<MeshRenderer>();

            _rootBone = _skeleton.FindBone("Root");
            _fullBodyBone = _skeleton.FindBone("Full Body");
            _rightUpperArmBone = _skeleton.FindBone("Right Upper Arm");
            _rightForearmBone = _skeleton.FindBone("Right Forearm");
            _headBone = _skeleton.FindBone("Head");
            _aimingPoint = _skeleton.FindBone("Aiming Point");
            _firingPoint = _skeleton.FindBone("Fire Point");
            _lightPoint = _skeleton.FindBone("Light Point");

            State start = new State("Start");
            State idle = new State("Idle");
            State walk = new State("Walk");
            State run = new State("Run");
            State jump = new State("Jump");
            State multiJump = new State("Multi Jump", true);
            State fall = new State("Fall");
            State wallSlide = new State("Wall Slide");
            State wallGripFire = new State("Wall Grip Fire");
            State hover = new State("Hover");
            State dead = new State("Dead");
            State enterShip = new State("Enter Ship");
            State push = new State("Push");
            State pull = new State("Pull");
            State horizontalClimb = new State("Horizontal Climb");
            //TODO: ANIMATION STATES
            //Dash State
            //Climb Chain State
            //Push on Wall State

            AnimationStateMachine = new StateMachine(start, true);

            idle.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                StartAnimation(Animations.Idle, true);
                return true;
            })));

            walk.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                StartAnimation(Animations.Walk, true);
                return true;
            })));

            run.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                StartAnimation(Animations.Run, true, RUN_TIMESCALE_MULTIPLIER);
                return true;
            })));

            jump.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                StartAnimation(Animations.Jump, false, JUMP_TIMESCALE_MULTIPLIER);
                return true;
            })));

            jump.AddStateAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                if (_mainAnimation.Animation.IsComplete)
                {
                    _jump = false;
                }
                return true;
            })));

            jump.AddExitAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                _jump = false;
                return true;
            })));

            multiJump.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                StartAnimation(Animations.MultiJump, false, MULTI_JUMP_TIMESCALE_MULTIPLIER);
                return true;
            })));

            multiJump.AddStateAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                if (_mainAnimation.Animation.IsComplete)
                {
                    _multiJump = false;
                }
                return true;
            })));

            multiJump.AddExitAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                _multiJump = false;
                return true;
            })));

            fall.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                StartAnimation(Animations.Fall, false);
                return true;
            })));

            hover.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                StartAnimation(Animations.Hover, true);
                return true;
            })));

            fall.AddStateAction(new FunctionPointerAction(new Func<bool>(() =>
            {
            //if (GlobalData.Player.Controller.Collisions.Below)
            //Set landed condition variable here.
            return true;
            })));

            wallSlide.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                StartAnimation(Animations.WallSlide, true);
                _animationState.SetEmptyAnimation(ARMS_AND_HEAD_ANIMATION_PRIMARY_TRACK_NUMBER, 0);
                _animationState.SetEmptyAnimation(HEAD_ONLY_ANIMATION_TRACK_NUMBER, 0);

                return true;
            })));

            wallGripFire.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                StartAnimation(Animations.WallGripFire, false);
                return true;
            })));

            wallGripFire.AddExitAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                _skeleton.SetSlotAttachmentsToSetupPose();
                return true;
            })));

            dead.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                if (playerRef.Collisions.IsCollidingBelow)
                {
                    StartAnimation(Animations.GroundDeath, false);
                }
                else
                {
                    StartAnimation(Animations.AirDeath, false);
                }

                return true;
            })));

            dead.AddStateAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                if (_mainAnimation.AnimationEnumName == Animations.AirDeath && !_playerOnGroundAfterDeath)
                {
                    if (playerRef.Collisions.IsCollidingBelow)
                    {
                        _playerOnGroundAfterDeath = true;
                        StartAnimation(Animations.AirDeath, false); //This second call to StartAnimation triggers the landing portion of the air death animation.
                    }
                }
                return true;
            })));

            dead.AddStateAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                if (_mainAnimation.AnimationEnumName == Animations.GroundDeath || (_mainAnimation.AnimationEnumName == Animations.AirDeath && _playerOnGroundAfterDeath))
                {
                    if (_mainAnimation.Animation.IsComplete)
                    {
                        Level.CurentLevel.ReloadLevel();
                        AnimationStateMachine.Active = false;
                    }
                }
                return true;
            })));

            enterShip.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                StartAnimation(Animations.EnterShip, false);
                return true;
            })));

            push.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                if (!Mathf.Approximately(_playerRef.MovementInput.x, 0))
                {
                    StartAnimation(Animations.Push, true);
                }
                else
                {
                    StartAnimation(Animations.PushIdle, true);
                }
                return true;
            })));
            push.AddStateAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                if (_mainAnimation.AnimationEnumName == Animations.Push && Mathf.Approximately(_playerRef.MovementInput.x, 0))
                {
                    StartAnimation(Animations.PushIdle, true);
                }
                else if (_mainAnimation.AnimationEnumName == Animations.PushIdle && !Mathf.Approximately(_playerRef.MovementInput.x, 0))
                {
                    StartAnimation(Animations.Push, true);
                }
                return true;
            })));
            push.AddExitAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                _skeleton.SetSlotAttachmentToSetupPose(RIGHT_HAND);
                _skeleton.SetSlotAttachmentToSetupPose(LEFT_HAND);

                if (AnimationStateMachine.CurrentState != pull)
                {
                    _skeleton.SetSlotAttachmentToSetupPose(RIFLE);
                    _skeleton.SetSlotAttachmentToSetupPose(HOLSTERED_RIFLE);
                }
                return true;
            })));

            pull.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                if (!Mathf.Approximately(_playerRef.MovementInput.x, 0))
                {
                    StartAnimation(Animations.Pull, true);
                }
                else
                {
                    StartAnimation(Animations.PullIdle, true);
                }
                return true;
            })));
            pull.AddStateAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                if (_mainAnimation.AnimationEnumName == Animations.Pull && Mathf.Approximately(_playerRef.MovementInput.x, 0))
                {
                    StartAnimation(Animations.PullIdle, true);
                }
                else if (_mainAnimation.AnimationEnumName == Animations.PullIdle && !Mathf.Approximately(_playerRef.MovementInput.x, 0))
                {
                    StartAnimation(Animations.Pull, true);
                }
                return true;
            })));
            pull.AddExitAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                _skeleton.SetSlotAttachmentToSetupPose(HEAD);

                _skeleton.SetSlotAttachmentToSetupPose(SIDE_EYE);
                _skeleton.SetSlotAttachmentToSetupPose(LEFT_FRONT_EYE);
                _skeleton.SetSlotAttachmentToSetupPose(RIGHT_FRONT_EYE);

                _skeleton.SetSlotAttachmentToSetupPose(RIGHT_HAND);
                _skeleton.SetSlotAttachmentToSetupPose(LEFT_HAND);

                if (AnimationStateMachine.CurrentState != push)
                {
                    _skeleton.SetSlotAttachmentToSetupPose(RIFLE);
                    _skeleton.SetSlotAttachmentToSetupPose(HOLSTERED_RIFLE);
                }
                return true;
            })));

            horizontalClimb.AddEntryAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                if (!Mathf.Approximately(_playerRef.MovementInput.x, 0))
                {
                    StartAnimation(Animations.HorizontalClimb, true);
                }
                else
                {
                    StartAnimation(Animations.HorizontalClimbIdle, true);
                }
                return true;
            })));
            horizontalClimb.AddStateAction(new FunctionPointerAction(new Func<bool>(() =>
            {
                if (_mainAnimation.AnimationEnumName == Animations.HorizontalClimb && Mathf.Approximately(_playerRef.MovementInput.x, 0))
                {
                    StartAnimation(Animations.HorizontalClimbIdle, true);
                }
                else if (_mainAnimation.AnimationEnumName == Animations.HorizontalClimbIdle && !Mathf.Approximately(_playerRef.MovementInput.x, 0))
                {
                    StartAnimation(Animations.HorizontalClimb, true);
                }
                return true;
            })));

            Condition isIdle = new FunctionPointerCondition(new Func<bool>(() => { return playerRef.MovementInput.x == 0 && !playerRef.Input.JustReleasedMovementInput; }));
            Condition atWalkSpeed = new FunctionPointerCondition(new Func<bool>(() => { return _walk; }));
            Condition isMoving = new FunctionPointerCondition(new Func<bool>(() => { return playerRef.MovementInput.x != 0; }));
            Condition onGround = new FunctionPointerCondition(new Func<bool>(() => { return playerRef.Collisions.IsCollidingBelow; }));
            Condition isDead = new FunctionPointerCondition(new Func<bool>(() => { return playerRef.State.Dead && TheGame.GetRef.gameState != TheGame.GameState.RESPAWNING; }));
            Condition didJump = new FunctionPointerCondition(new Func<bool>(() => { return _jump; }));
            Condition didMultiJump = new FunctionPointerCondition(new Func<bool>(() => { return _multiJump; }));
            Condition isWallSliding = new FunctionPointerCondition(new Func<bool>(() => { return playerRef.State.WallSliding; }));
            Condition isWallGripping = new FunctionPointerCondition(new Func<bool>(() => { return playerRef.State.WallGripping; }));
            Condition isHovering = new FunctionPointerCondition(new Func<bool>(() => { return playerRef.State.Hovering; }));
            Condition isEnteringShip = new FunctionPointerCondition(new Func<bool>(() => { return _enteringShip; }));
            Condition runningIntoWall = new FunctionPointerCondition(new Func<bool>(() => { return (playerRef.Collisions.Right.CollidingLayer == GlobalData.OBSTACLE_LAYER || playerRef.Collisions.Left.CollidingLayer == GlobalData.OBSTACLE_LAYER) && playerRef.MovementInput.x != 0; }));
            Condition isAiming = new FunctionPointerCondition(new Func<bool>(() => { return playerRef.Weapon.IsAiming; }));
            Condition isGrabbing = new FunctionPointerCondition(new Func<bool>(() => { return playerRef.State.Grabbing; }));
            Condition isPushing = new FunctionPointerCondition(new Func<bool>(() => 
            {
                var grabbedObjectAsMoveableObject = playerRef.State.GrabbedObject as PlayerMoveableObject;
                if (grabbedObjectAsMoveableObject != null)
                {
                    return grabbedObjectAsMoveableObject.LastMoveType == PlayerMoveableObject.MoveType.Push || grabbedObjectAsMoveableObject.LastMoveType == PlayerMoveableObject.MoveType.None;
                }

                return false;
            }));
            Condition isPulling = new FunctionPointerCondition(new Func<bool>(() =>
            {
                var grabbedObjectAsMoveableObject = playerRef.State.GrabbedObject as PlayerMoveableObject;
                if (grabbedObjectAsMoveableObject != null)
                {
                    return grabbedObjectAsMoveableObject.LastMoveType == PlayerMoveableObject.MoveType.Pull;
                }

                return false;
            }));
            Condition isHorizontalClimbing = new FunctionPointerCondition(new Func<bool>(() => { return playerRef.State.Climbing && playerRef.State.CurrentActiveClimbableObject?.TravelDirection == ClimbableObject.TravelDirections.Horizontal; }));
            Condition isDashing = new FunctionPointerCondition(new Func<bool>(() => { return playerRef.State.Dashing; }));

            GlobalTransition toIdle = new GlobalTransition(idle, new AndCondition(isIdle, onGround, new NotCondition(isGrabbing)), dead, enterShip);
            GlobalTransition toWalk = new GlobalTransition(walk, new AndCondition(atWalkSpeed, onGround, new NotCondition(isGrabbing)), dead, enterShip);
            GlobalTransition toRun = new GlobalTransition(run, new AndCondition(isMoving, onGround, new NotCondition(runningIntoWall), new NotCondition(isGrabbing)), dead, enterShip);
            GlobalTransition toJump = new GlobalTransition(jump, didJump, dead, multiJump, fall, enterShip);
            GlobalTransition toFall = new GlobalTransition(fall, new AndCondition(new NotCondition(didJump), new NotCondition(didMultiJump), new NotCondition(onGround), new NotCondition(isWallSliding), new NotCondition(isHovering), new NotCondition(isHorizontalClimbing)), dead, enterShip);
            GlobalTransition toDeath = new GlobalTransition(dead, isDead, enterShip);
            GlobalTransition toEnterShip = new GlobalTransition(enterShip, isEnteringShip);
            GlobalTransition toPush = new GlobalTransition(push, new AndCondition(isGrabbing, isPushing), pull);
            GlobalTransition toPull = new GlobalTransition(pull, new AndCondition(isGrabbing, isPulling), push);
            GlobalTransition toHorizontalClimbing = new GlobalTransition(horizontalClimb, isHorizontalClimbing);

            Transition runToIdle = new Transition(idle, runningIntoWall);

            Transition jumpToMultiJump = new Transition(multiJump, didMultiJump);
            Transition jumpToWallSlide = new Transition(wallSlide, isWallSliding);
            Transition jumpToHover = new Transition(hover, isHovering);

            Transition fallToMultiJump = new Transition(multiJump, new AndCondition(didMultiJump, new NotCondition(onGround)));
            Transition fallToWallSlide = new Transition(wallSlide, new AndCondition(isWallSliding, new NotCondition(isDashing)));
            Transition fallToHover = new Transition(hover, isHovering);

            Transition multiJumpToWallSlide = new Transition(wallSlide, isWallSliding);
            Transition multiJumpToHover = new Transition(hover, isHovering);

            Transition wallSlideToMultiJump = new Transition(multiJump, didMultiJump);
            Transition wallSlideToFall = new Transition(fall, new AndCondition(new NotCondition(isWallSliding), new NotCondition(onGround)));
            Transition wallSlideToWallGripFire = new Transition(wallGripFire, new AndCondition(isWallGripping, isAiming));
            Transition wallSlideToDash = new Transition(fall, new AndCondition(isWallSliding, isDashing));

            Transition hoverToMultiJump = new Transition(multiJump, didMultiJump);

            Transition wallGripFireToWallSlide = new Transition(wallSlide, new AndCondition(new NotCondition(isWallGripping), isWallSliding));

            Transition pushToPull = new Transition(pull, isPulling);

            Transition pullToPush = new Transition(push, isPushing);

            AnimationStateMachine.AddGlobalTransition(toEnterShip, true);
            AnimationStateMachine.AddGlobalTransition(toDeath, true);
            AnimationStateMachine.AddGlobalTransition(toIdle, false);
            AnimationStateMachine.AddGlobalTransition(toWalk, false);
            AnimationStateMachine.AddGlobalTransition(toRun, false);
            AnimationStateMachine.AddGlobalTransition(toJump, false);
            AnimationStateMachine.AddGlobalTransition(toFall, false);
            AnimationStateMachine.AddGlobalTransition(toPush, false);
            AnimationStateMachine.AddGlobalTransition(toPull, false);
            AnimationStateMachine.AddGlobalTransition(toHorizontalClimbing, false);

            run.AddTransition(runToIdle);

            jump.AddTransition(jumpToMultiJump);
            jump.AddTransition(jumpToWallSlide);
            jump.AddTransition(jumpToHover);

            fall.AddTransition(fallToMultiJump);
            fall.AddTransition(fallToWallSlide);
            fall.AddTransition(fallToHover);

            multiJump.AddTransition(multiJumpToWallSlide);
            multiJump.AddTransition(multiJumpToHover);

            wallSlide.AddTransition(wallSlideToMultiJump);
            wallSlide.AddTransition(wallSlideToFall);
            //wallSlide.AddTransition(wallSlideToWallGrip);
            wallSlide.AddTransition(wallSlideToWallGripFire);
            wallSlide.AddTransition(wallSlideToDash);

            hover.AddTransition(hoverToMultiJump);

            //wallGrip.AddTransition(wallGripToWallSlide);
            //wallGrip.AddTransition(wallGripToWallGripFire);

            //wallGripFire.AddTransition(wallGripFireToWallGrip);
            wallGripFire.AddTransition(wallGripFireToWallSlide);

            push.AddTransition(pushToPull);

            pull.AddTransition(pullToPush);

            _playerCollider = playerCollider;

            string playerRefGOName = playerRef.transform.root.gameObject.name; //We need to store this name, because re-instantiations of the player after death may change the name of the player object, and thus the load path.

            playerRef.Respawned += OnRespawn;
            playerRef.Stunned += OnStunned;
            playerRef.Hovering += OnHover;
            playerRef.Climbing += OnLadder;

            playerRef.DamageReceived += OnDamageReceived;
            playerRef.Dead += OnDead;

            playerRef.CollisionBelow += OnCollisionBelow;

            playerRef.EnteringShip += OnEnteringShip;

            playerRef.FacingDirectionChanged += OnFacingDirectionChanged;

            _playerRef = playerRef;
        }
        #endregion

        #region Update
        public void AnimationUpdate()
        {
            AnimationStateMachine.RunUpdate(false);

            if ((_playerRef.Weapon.CurrentAimState == PlayerWeaponHandler.AimState.DOWN || _playerRef.State.LightsOn)
                && (!_playerRef.State.Climbing || (_playerRef.State.Climbing && _playerRef.State.CurrentActiveClimbableObject.TravelDirection == ClimbableObject.TravelDirections.Horizontal)) 
                && !_playerRef.State.Hovering && !_playerRef.State.WallSliding && !_playerRef.State.Dashing
                && _mainAnimation.AnimationEnumName != Animations.MultiJump && PushOrPullAnimationNotPlaying())
            {
                SetSpriteDirection();
            }

            if (_playerRef.Collisions.FaceDir == -1)
            {
                MainSprite.transform.localPosition = new Vector3(0, 1.0f, 0);
            }

            _fullBodyBone.UpdateWorldTransform();
            _rightUpperArmBone.UpdateWorldTransform();
            _rightForearmBone.UpdateWorldTransform();
            _headBone.UpdateWorldTransform();
            _aimingPoint.UpdateWorldTransform();
            _firingPoint.UpdateWorldTransform();

            if (!_playerRef.Collisions.IsCollidingBelow)
            {
                SetOnGround(false);
            }
        }

        public void AnimationLateUpdate()
        {
            if (_playerRef.Collisions.FaceDir == 1)
            {
                MainSprite.transform.localPosition = Vector3.zero;
            }
            else
            {
                MainSprite.transform.localPosition = new Vector3(_playerCollider.offset.x, 0, 0);
            }
        }
        #endregion

        #region Sprite Manipulation
        /// <summary>
        /// Checks the character's input and calls a co-routine to set the appropriate facing direction.
        /// The player has it's own version because it's base orientation is the opposite of other characters,
        /// so the scale flipping is opposite.
        /// </summary>
        public bool SetSpriteDirection(bool isAiming = false)
        {
            bool didSwitchOccur = false;

            if (_playerRef.MovementInput.x < 0 && _playerRef.Collisions.FaceDir == 1)
            {
                _playerRef.Collisions.FaceDir = -1;
                _skeleton.ScaleX = -1.0f;

                didSwitchOccur = true;
            }
            else if (_playerRef.MovementInput.x > 0 && _playerRef.Collisions.FaceDir == -1)
            {
                _playerRef.Collisions.FaceDir = 1;
                _skeleton.ScaleX = 1.0f;

                didSwitchOccur = true;
            }

            if (didSwitchOccur)
            {
                _playerRef.NotifyFacingDirectionChanged(_playerRef.Collisions.FaceDir);
            }

            return didSwitchOccur;
        }

        /// <summary>
        /// Calls a co-routine to set the specified facing direction. This version has an optional safety check that ensures the sprite is not already
        /// facing the direction the sprite has been requested to switch to. The player has it's own version because it's base orientation
        /// is the opposite of other characters, so the scale flipping is opposite.
        /// </summary>
        /// <param name="switchToRight">Whether the sprite should be swtiching to face the right.</param>
        /// <param name="safetyCheck">Should the function check to ensure the sprite is not trying to switch to the direction it is already facing.</param>
        public bool SetSpriteDirection(bool switchToRight, bool safetyCheck = true, bool isAiming = false)
        {
            bool didSwitchOccur = false;
            if ((safetyCheck && switchToRight && _playerRef.Collisions.FaceDir == -1) || (!safetyCheck && switchToRight)) //The character is facing left and we want it to face right.
            {
                _playerRef.Collisions.FaceDir = 1;
                _skeleton.ScaleX = 1.0f;

                didSwitchOccur = true;
            }
            else if ((safetyCheck && !switchToRight && _playerRef.Collisions.FaceDir == 1) || (!safetyCheck && !switchToRight)) //The character is facing right and we want it to face left.
            {
                _playerRef.Collisions.FaceDir = -1;
                _skeleton.ScaleX = -1.0f;

                didSwitchOccur = true;
            }

            if (didSwitchOccur)
            {
                _playerRef.NotifyFacingDirectionChanged(_playerRef.Collisions.FaceDir);
            }

            return didSwitchOccur;
        }

        private IEnumerator<float> ApplyDamageFlash()
        {
            //TODO: RE-IMPLEMENT TO WORK WITH MESH RENDERER AND SPINE
            /*
            if (colourFlash != null)
            {
                colourFlash.UpdateAll(Color.white);
                yield return Timing.WaitForSeconds(0.1f);
                colourFlash.UpdateAll(DAMAGE_FLASH_COLOUR);
                yield return Timing.WaitForSeconds(0.1f);
                colourFlash.UpdateAll(Color.white);
            }
            */

            yield return 0f;
        }

        private IEnumerator<float> ApplyInvulnerabiltyFlash()
        {
            int percentComplete = 0;
            float damageInvulnerabilityTimeInterval = PlayerConstants.DAMAGE_INVULNERABILITY_FLASH_TIME / INVULNERABILITY_FLASHES;

            while (percentComplete < 100)
            {
                if (MainSpriteRenderer.enabled)
                {
                    MainSpriteRenderer.enabled = false;
                }
                else
                {
                    MainSpriteRenderer.enabled = true;
                }

                yield return Timing.WaitForSeconds(damageInvulnerabilityTimeInterval);
                percentComplete += 5;
            }

            MainSpriteRenderer.enabled = true;
            yield return 0f;
        }
        #endregion

        public void StartAnimation(Animations animation, bool loop, float timeScale = 1.0f, bool queueAnimation = false)
        {
            switch (animation)
            {
                case Animations.Idle:
                    _mainAnimation = new PlayingAnimation("Idle", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSameAnimation);
                    break;
                case Animations.Walk:
                    _mainAnimation = new PlayingAnimation("Walk", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSameAnimation);
                    break;
                case Animations.Run:
                    _mainAnimation = new PlayingAnimation("Run", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSameAnimation);
                    break;
                case Animations.Jump:
                    _mainAnimation = new PlayingAnimation("Jump", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSameAnimation);
                    break;
                case Animations.MultiJump:
                    if (_playerRef.MovementInput.x > 0 || (_playerRef.MovementInput.x == 0 && _playerRef.Collisions.FaceDir == 1))
                    {
                        _mainAnimation = new PlayingAnimation("Multi Jump (Right)", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                        _mainAnimation.SetAnimation(_animationState.SetAnimation(MAIN_ANIMATION_TRACK, _mainAnimation.AnimationName, _mainAnimation.ShouldLoop), MAIN_ANIMATION_TRACK);
                        SetSpriteDirection(true, true, false);
                    }
                    else
                    {
                        _mainAnimation = new PlayingAnimation("Multi Jump (Left)", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                        _mainAnimation.SetAnimation(_animationState.SetAnimation(MAIN_ANIMATION_TRACK, _mainAnimation.AnimationName, _mainAnimation.ShouldLoop), MAIN_ANIMATION_TRACK);
                        SetSpriteDirection(false, true, false);
                    }
                    _mainAnimation.Animation.TimeScale = timeScale;

                    return;
                case Animations.Fall:
                    _mainAnimation = new PlayingAnimation("Fall", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSameAnimation);
                    break;
                case Animations.WallSlide:
                    _mainAnimation = new PlayingAnimation("Wall Slide", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    break;
                case Animations.WallGripFire:
                    _mainAnimation = new PlayingAnimation("Wall Grip", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSeparateAnimations);

                    _mainAnimation.SetAnimation(_animationState.SetAnimation(MAIN_ANIMATION_TRACK, "Wall Grip (Everything Else)", false), MAIN_ANIMATION_TRACK);
                    _mainAnimation.SetArmsAnimation(_animationState.SetAnimation(ARMS_AND_HEAD_ANIMATION_PRIMARY_TRACK_NUMBER, "Wall Grip (Right Arm)", false));
                    _mainAnimation.SetHeadAnimation(_animationState.SetAnimation(HEAD_ONLY_ANIMATION_TRACK_NUMBER, "Wall Grip (Head)", false));

                    _mainAnimation.Animation.TimeScale = timeScale;
                    return;
                case Animations.Hover:
                    _mainAnimation = new PlayingAnimation("Hover", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSameAnimation);
                    break;
                case Animations.Fire:
                    if (_mainAnimation.AnimationEnumName != Animations.MultiJump)
                    {
                        var sideTrack = _animationState.SetAnimation(SECONDARY_ANIMATION_TRACK, "Fire", loop);
                        sideTrack.TimeScale = timeScale;

                        _sideAnimation = new PlayingAnimation("Fire", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    }

                    return;
                case Animations.FireMediumRecoil:
                    if (_mainAnimation.AnimationEnumName != Animations.MultiJump)
                    {
                        var sideTrack = _animationState.SetAnimation(SECONDARY_ANIMATION_TRACK, "Fire (Medium Recoil)", loop);
                        sideTrack.TimeScale = timeScale;

                        _sideAnimation = new PlayingAnimation("Fire", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    }

                    return;
                case Animations.FireHeavyRecoil:
                    if (_mainAnimation.AnimationEnumName != Animations.MultiJump)
                    {
                        var sideTrack = _animationState.SetAnimation(SECONDARY_ANIMATION_TRACK, "Fire (Heavy Recoil)", loop);
                        sideTrack.TimeScale = timeScale;

                        _sideAnimation = new PlayingAnimation("Fire", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    }

                    return;
                case Animations.GroundDeath:
                    _mainAnimation = new PlayingAnimation("Death (Ground)", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    break;
                case Animations.AirDeath:
                    if (!_playerOnGroundAfterDeath)
                    {
                        _mainAnimation = new PlayingAnimation("Death (Air - Launch)", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    }
                    else
                    {
                        _mainAnimation = new PlayingAnimation("Death (Air - Land)", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    }
                    break;
                case Animations.EnterShip:
                    _mainAnimation = new PlayingAnimation("Enter Ship (Ground)", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    break;
                case Animations.Push:
                    _mainAnimation = new PlayingAnimation("Push", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    break;
                case Animations.PushIdle:
                    _mainAnimation = new PlayingAnimation("Push (Idle)", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    break;
                case Animations.Pull:
                    _mainAnimation = new PlayingAnimation("Pull", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    break;
                case Animations.PullIdle:
                    _mainAnimation = new PlayingAnimation("Pull (Idle)", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    break;
                case Animations.HorizontalClimb:
                    _mainAnimation = new PlayingAnimation("Horizontal Climb", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.None);
                    break;
                case Animations.HorizontalClimbIdle:
                    _mainAnimation = new PlayingAnimation("Horizontal Climb (Idle)", animation, loop, PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSameAnimation);
                    break;
            }

            if (queueAnimation)
            {
                _mainAnimation.SetAnimation(_animationState.AddAnimation(MAIN_ANIMATION_TRACK, _mainAnimation.HeadAndArmsMode != PlayingAnimation.HeadAndArmsAnimationMode.None ? _mainAnimation.AnimationName + EVERYTHING_ELSE_ANIMATION_SUFFIX : _mainAnimation.AnimationName, _mainAnimation.ShouldLoop, 0), MAIN_ANIMATION_TRACK);
            }
            else
            {
                _mainAnimation.SetAnimation(_animationState.SetAnimation(MAIN_ANIMATION_TRACK, _mainAnimation.HeadAndArmsMode != PlayingAnimation.HeadAndArmsAnimationMode.None ? _mainAnimation.AnimationName + EVERYTHING_ELSE_ANIMATION_SUFFIX : _mainAnimation.AnimationName, _mainAnimation.ShouldLoop), MAIN_ANIMATION_TRACK);
            }

            _mainAnimation.Animation.TimeScale = timeScale;

            if (_mainAnimation.HeadAndArmsMode != PlayingAnimation.HeadAndArmsAnimationMode.None && !_playerRef.Weapon.IsAiming)
            {
                switch (_mainAnimation.HeadAndArmsMode)
                {
                    case PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSameAnimation:
                        TrackEntry headAndArmsAnimation = _animationState.SetAnimation(ARMS_AND_HEAD_ANIMATION_PRIMARY_TRACK_NUMBER, _mainAnimation.AnimationName + ARMS_AND_HEAD_ANIMATION_SUFFIX, _mainAnimation.ShouldLoop);
                        headAndArmsAnimation.TimeScale = timeScale;

                        _mainAnimation.SetArmsAnimation(headAndArmsAnimation);
                        _mainAnimation.SetHeadAnimation(headAndArmsAnimation);

                        _animationState.SetEmptyAnimation(HEAD_ONLY_ANIMATION_TRACK_NUMBER, 0);

                        break;
                }
            }
            else
            {
                _animationState.SetEmptyAnimation(ARMS_AND_HEAD_ANIMATION_PRIMARY_TRACK_NUMBER, 0);
            }
        }

        #region Action Animations
        /// <summary>
        /// Sets the parameters for the player sprite to play the jump animation.
        /// </summary>
        public void SetJumpAnimation(bool wallSliding = false)
        {
            _playerRef.Sound?.PlayJumpSound();
            _jump = true;
        }

        /// <summary>
        /// Sets the parameters for the player sprite to play the multi jump animation (i.e. the "Air Flip").
        /// </summary>
        public void SetMultiJumpAnimation()
        {
            _jump = false;
            _multiJump = true;

            if (AnimationStateMachine.CurrentState.CompareStates("Multi Jump"))
            {
                StartAnimation(Animations.MultiJump, false, MULTI_JUMP_TIMESCALE_MULTIPLIER);
            }
        }
        #endregion

        #region State Values
        /// <summary>
        /// Sets the on ground value of the animator to the provided value.
        /// Also plays the appropriate sound effect (if the player is in fact on the ground).
        /// </summary>
        /// <param name="onGround">Whether the player is currently on the ground (i.e. colliding from below).</param>
        public void SetOnGround(bool onGround)
        {
            _playerRef.Sound?.PlayLandOnGroundSound(onGround);
        }
        #endregion

        #region Callbacks
        /// <summary>
        /// Sets the hover value of the animator to the provided value.
        /// </summary>
        /// <param name="hover">The value the hover state value should be set to.</param>
        public void OnHover(bool hover)
        {
            _jump = false;
            _multiJump = false;
            //TODO: Set Hover animation parameters here.
        }

        /// <summary>
        /// Sets the stunned value of the animator to the provided value.
        /// </summary>
        /// <param name="stunned">The value the stunned state value should be set to.</param>
        public void OnStunned(bool stunned)
        {
            //TODO: Set Stunned animation parameters here.
        }

        /// <summary>
        /// Sets the parameters for the player sprite to play the animation of getting on a ladder
        /// (and in turn the animation of them actually climbing or idling on the ladder).
        /// </summary>
        public void OnLadder(bool onLadder)
        {
            //TODO: Put ladder animation logic here.
        }

        /// <summary>
        /// Performs the animation specific tasks that should occur when the player takes damage.
        /// </summary>
        /// <param name="type">The type of damage received.</param>
        public void OnDamageReceived(Vector3 position)
        {
            OnHover(false);
            StopJetAnimation("Legs");

            DamageFlashHandle = Timing.RunCoroutine(ApplyDamageFlash());
            InvulnerabilityFlashHandle = Timing.RunCoroutine(ApplyInvulnerabiltyFlash());
        }

        /// <summary>
        /// Sets the dead trigger of the animator and begins the death co-routine.
        /// </summary>
        /// <param name="type">The type of damage received.</param>
        public void OnDead()
        {
            _playerOnGroundAfterDeath = false;
            GlobalData.Timekeeper.StartTimer(() => { _playerOnGroundAfterDeath = true; }, 3.0f, "Override Player On Ground");
        }

        private void OnRespawn(bool positive)
        {
            SetSpriteDirection(true, true, false);
        }

        /// <summary>
        /// Performs the animation specific tasks that should occur when the player is colliding from below.
        /// </summary>-
        /// <param name="velocity"></param>
        public void OnCollisionBelow(Vector2 velocity)
        {
            StopJetAnimation("Legs");
            SetOnGround(true);
        }

        private void OnEnteringShip()
        {
            _jump = false;
            _multiJump = false;

            _enteringShip = true;
        }

        private void OnFacingDirectionChanged(int newFaceDir)
        {
            if (_mainAnimation.AnimationEnumName == Animations.MultiJump)
            {
                float currentAnimationTrackTime = _mainAnimation.Animation.TrackTime;
                StartAnimation(Animations.MultiJump, false, MULTI_JUMP_TIMESCALE_MULTIPLIER);
                _mainAnimation.Animation.TrackTime = currentAnimationTrackTime;
            }
        }
        #endregion

        #region Cancelling Animations
        public void CancelHoverAnimation()
        {
            //TODO: Cancel hover state here
            StopJetAnimation("Legs");
        }

        /// <summary>
        /// Cancels the parameter that controls whether any ladder related animations should play.
        /// </summary>
        public void GetOffLadder()
        {
            //TODO: Cancel ladder state here.
        }

        /// <summary>
        /// Stops the jetboot animation for each foot and stops the sound effect.
        /// </summary>
        public void StopJetAnimation(string limb)
        {
            //AUDIO: Stop Jetboot sound
            _playerRef.Sound?.StopJetbootSound();
        }
        #endregion

        #region Helper Methods
        public void StopArmsAndHeadAnimation()
        {
            if (_mainAnimation.AnimationEnumName != Animations.MultiJump)
            {
                switch (_mainAnimation.HeadAndArmsMode)
                {
                    case PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSameAnimation:
                        _animationState.SetEmptyAnimation(ARMS_AND_HEAD_ANIMATION_PRIMARY_TRACK_NUMBER, 0);
                        break;
                    case PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSeparateAnimations:
                        _animationState.SetEmptyAnimation(ARMS_AND_HEAD_ANIMATION_PRIMARY_TRACK_NUMBER, 0);
                        _animationState.SetEmptyAnimation(HEAD_ONLY_ANIMATION_TRACK_NUMBER, 0);
                        break;
                    case PlayingAnimation.HeadAndArmsAnimationMode.ArmsOnly:
                        _animationState.SetEmptyAnimation(ARMS_AND_HEAD_ANIMATION_PRIMARY_TRACK_NUMBER, 0);
                        break;
                    case PlayingAnimation.HeadAndArmsAnimationMode.HeadOnly:
                        _animationState.SetEmptyAnimation(HEAD_ONLY_ANIMATION_TRACK_NUMBER, 0);
                        break;
                }

                _mainAnimation.SetArmsAnimation(null);
                _mainAnimation.SetHeadAnimation(null);
            }
        }

        public void MatchArmsAndHeadToMainAnimation()
        {
            switch (_mainAnimation.HeadAndArmsMode)
            {
                case PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSameAnimation:
                    TrackEntry headAndArmsAnimation = _animationState.SetAnimation(ARMS_AND_HEAD_ANIMATION_PRIMARY_TRACK_NUMBER, _mainAnimation.AnimationName + ARMS_AND_HEAD_ANIMATION_SUFFIX, _mainAnimation.ShouldLoop);
                    headAndArmsAnimation.TrackTime = _mainAnimation.Animation.TrackTime;
                    headAndArmsAnimation.TimeScale = _mainAnimation.Animation.TimeScale;

                    _mainAnimation.SetArmsAnimation(headAndArmsAnimation);
                    _mainAnimation.SetHeadAnimation(headAndArmsAnimation);

                    break;
                case PlayingAnimation.HeadAndArmsAnimationMode.HeadAndArmsSeparateAnimations:
                    _mainAnimation.SetArmsAnimation(_animationState.SetAnimation(ARMS_AND_HEAD_ANIMATION_PRIMARY_TRACK_NUMBER, _mainAnimation.AnimationName + ARM_ONLY_SUFFIX, _mainAnimation.ShouldLoop));
                    _mainAnimation.SetHeadAnimation(_animationState.SetAnimation(HEAD_ONLY_ANIMATION_TRACK_NUMBER, _mainAnimation.AnimationName + HEAD_ONLY_SUFFIX, _mainAnimation.ShouldLoop));

                    _mainAnimation.ArmsAnimation.TrackTime = _mainAnimation.Animation.TrackTime;
                    _mainAnimation.ArmsAnimation.TimeScale = _mainAnimation.Animation.TimeScale;

                    _mainAnimation.HeadAnimation.TrackTime = _mainAnimation.Animation.TrackTime;
                    _mainAnimation.HeadAnimation.TimeScale = _mainAnimation.Animation.TimeScale;

                    break;
                case PlayingAnimation.HeadAndArmsAnimationMode.ArmsOnly:
                    _mainAnimation.SetArmsAnimation(_animationState.SetAnimation(ARMS_AND_HEAD_ANIMATION_PRIMARY_TRACK_NUMBER, _mainAnimation.AnimationName + ARM_ONLY_SUFFIX, _mainAnimation.ShouldLoop));
                    _mainAnimation.ArmsAnimation.TrackTime = _mainAnimation.Animation.TrackTime;
                    _mainAnimation.ArmsAnimation.TimeScale = _mainAnimation.Animation.TimeScale;

                    break;
                case PlayingAnimation.HeadAndArmsAnimationMode.HeadOnly:
                    _mainAnimation.SetHeadAnimation(_animationState.SetAnimation(HEAD_ONLY_ANIMATION_TRACK_NUMBER, _mainAnimation.AnimationName + HEAD_ONLY_SUFFIX, _mainAnimation.ShouldLoop));
                    _mainAnimation.HeadAnimation.TrackTime = _mainAnimation.Animation.TrackTime;
                    _mainAnimation.HeadAnimation.TimeScale = _mainAnimation.Animation.TimeScale;

                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Determines whether the main animation currently playing is not one of the push or pull animations.
        /// </summary>
        /// <returns>True if the main animation is not one of the push or pull animations, false otherwise.</returns>
        private bool PushOrPullAnimationNotPlaying()
        {
            return _mainAnimation.AnimationEnumName != Animations.Push && _mainAnimation.AnimationEnumName != Animations.PushIdle && _mainAnimation.AnimationEnumName != Animations.Pull && _mainAnimation.AnimationEnumName != Animations.PullIdle;
        }
        #endregion
    }
}
