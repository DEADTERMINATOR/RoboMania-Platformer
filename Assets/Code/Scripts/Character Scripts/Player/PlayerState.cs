﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using LevelSystem;
using static GlobalData;

namespace Characters.Player
{
    public class PlayerState
    {
        #region Constants
        /// <summary>
        /// The percentage interval that sprite visibility should switch on during damage invulnerability.
        /// </summary>
        private const int DAMAGE_SPRITE_VISIBILITY_PERCENTAGE_INTERVAL = 5;
        #endregion

        #region Public Properties
        /// <summary>
        /// Whether the player is currently dead.
        /// </summary>
        public bool Dead { get; private set; }

        /// <summary>
        /// Whether the player has entered a state of invulnerability due to damage (or other potential cause).
        /// </summary>
        public bool Invulnerable { get; private set; }

        /// <summary>
        /// Whether the player is currently stunned.
        /// </summary>
        public bool Stunned { get; private set; }

        /// <summary>
        /// Whether the player is currently dashing.
        /// </summary>
        public bool Dashing { get; private set; }

        /// <summary>
        /// Whether the player is currently running.
        /// </summary>
        public bool Running { get; private set; }

        /// <summary>
        /// Whether the player is currently hovering (or raising).
        /// </summary>
        public bool Hovering { get; private set; }

        /// <summary>
        /// Whether the player is wall sliding.
        /// </summary>
        public bool WallSliding { get; private set; }

        /// <summary>
        /// Whether the player is currently making use of the wall grip enhancement while wall sliding.
        /// </summary>
        public bool WallGripping { get; private set; }

        /// <summary>
        /// Whether the player is currently in the middle of a wall jump off of a wall (as opposed to wall jumping into a wall to climb it).
        /// </summary>
        public bool WallJumpingOffWall { get; private set; }

        /// <summary>
        /// Whether the player is inside of an air gust.
        /// </summary>
        public bool InsideAirGust { get; private set; }

        /// <summary>
        /// Whether the player has recently landed on a bouncy platform that they can boost from.
        /// </summary>
        public bool Bouncing { get; private set; }

        /// <summary>
        /// Whether the player has recently landed on a bouncy platform that they cannot boost from (i.e. the platform has full control over their velocity).
        /// </summary>
        public bool Bumped { get; private set; }

        /// <summary>
        /// Whether the player is currently grabbing onto something (may happen in conjunction with pulling).
        /// </summary>
        public bool Grabbing { get; private set; }

        /// <summary>
        /// The object the player is currently grabbing onto.
        /// </summary>
        public IGrabbable GrabbedObject { get; private set; }

        /// <summary>
        /// Whether the player is current climbing on something.
        /// </summary>
        public bool Climbing { get; private set; }

        /// <summary>
        /// The climbable object the player is currently climbing on.
        /// </summary>
        public ClimbableObject CurrentActiveClimbableObject { get; private set; }

        /// <summary>
        /// Whether the player is following a path while climbing (e.g. climbing a vine that's moving, so the direction of "up" on the vine keeps changing).
        /// </summary>
        public bool ShouldFollowClimbingPath { get; private set; }

        /// <summary>
        /// The position target the player should be moving towards if they are climbing up or to the right.
        /// </summary>
        public Vector2 NextClimbingPathTarget { get; private set; }

        /// <summary>
        /// The position target the player should be moving towards if they are climbing down or to the left.
        /// </summary>
        public Vector2 PreviousClimbingPathTarget { get; private set; }

        /// <summary>
        /// Whether the player is currently submerged in lava.
        /// </summary>
        public bool InLava { get; private set; }

        /// <summary>
        /// Whether the player is current sinking in mud.
        /// </summary>
        public bool InMud { get; private set; }

        /// <summary>
        /// How long the player has been on the ground (i.e. colliding from the bottom on a obstacle surface).
        /// </summary>
        public float TimeOnGround { get; private set; }

        /// <summary>
        /// How long the player has been in the air (i.e. not colliding from the bottom).
        /// </summary>
        public float TimeInAir { get; private set; }

        /// <summary>
        /// Whether the player's flashlight and personal light should be on.
        /// </summary>
        public bool LightsOn { get; private set; }

        /// <summary>
        /// Whether the player is inside a zone that overrides the logic that should otherwise stop the player from exiting a camera movement area.
        /// </summary>
        public bool InsideCameraMovementAreaOverrideZone { get; private set; }

        /// <summary>
        /// Public getter for the reference to the player's aiming cone effect.
        /// </summary>
        public Light2D AimingLight { get { return _aimingLight; } }

        /// <summary>
        /// Public getter for the reference to the player's flashlight effect.
        /// </summary>
        public Light2D Flashlight { get { return _flashlight; } }
        #endregion

        #region Private Variables
        private float _currentStunAmount = 0f;
        private bool _rechargeEnergyCells = false;
        private float _bouncingTime = 0f;
        private Light2D _aimingLight;
        private Light2D _flashlight;
        private Light2D _personalLight;
        private PlayerMaster _playerRef;
        #endregion

        #region Constructor
        public PlayerState(PlayerMaster playerRef)
        {
            if (TheGame.GetRef.gameState != TheGame.GameState.RESPAWNING)
            {
                Dead = false;
            }
            else
            {
                Dead = true;
            }

            Stunned = false;
            Hovering = false;
            WallSliding = false;
            Climbing = false;

            _aimingLight = playerRef.transform.Find("Lights/Aiming Light").GetComponent<Light2D>();
            _aimingLight.enabled = false;

            _flashlight = playerRef.transform.Find("Lights/Original Flashlight").GetComponent<Light2D>();
            _personalLight = playerRef.transform.Find("Lights/Personal Light").GetComponent<Light2D>();

            if (LightsOn)
            {
                _flashlight.enabled = true;
                _personalLight.enabled = true;
            }
            else
            {
                _flashlight.enabled = false;
                _personalLight.enabled = false;
            }

            playerRef.CollisionBelow += OnCollideBelow;
            playerRef.EnergyCellMoveUsed += OnEnergyCellMoveUsed;
            playerRef.Respawned += OnRespawn;
            playerRef.GravityChange += SetEnvironmentalState;

            _playerRef = playerRef;
        }
        #endregion

        #region Update
        public void StateUpdate()
        {
            if (_playerRef.Controller.Collisions.IsCollidingBelow && !_playerRef.Input.BlockInput)
            {
                TimeOnGround += Time.deltaTime;

                if (_rechargeEnergyCells)
                {
                    _playerRef.Data.InstantlyRechargeEnergyCells();
                    _rechargeEnergyCells = false;
                }
            }
            else
            {
                TimeInAir += Time.deltaTime;
                TimeOnGround = 0f;

                if (InLava)
                {
                    TimeInAir = 0;
                    if (_rechargeEnergyCells)
                    {
                        _playerRef.Data.InstantlyRechargeEnergyCells();
                        _rechargeEnergyCells = false;
                    }
                }
            }

            if (Bouncing)
            {
                _bouncingTime += Time.deltaTime;
            }
        }
        #endregion

        #region Damage
        /// <summary>
        /// Handles the application of any incoming damage to the player. This version requires a damage type to be provided.
        /// </summary>
        /// <param name="giver">The source of the damage.</param>
        /// <param name="amount">The amount of damage.</param>
        /// <param name="type">The type of damage.</param>
        public void TakeDamage(IDamageGiver giver, float amount, DamageType type)
        {
            if (TheGame.GetRef.gameState != TheGame.GameState.TRANSITION && _playerRef.Data.Health > 0)
            {
                if (!(giver is PlayerMaster))
                {
                    if (Invulnerable && type != DamageType.ENVIROMENT && type != DamageType.CONSTANT)
                    {
                        return;
                    }
                }

                if (DamageType.SHIELD == type)//If the Damage is coming From a shield then make sure the player does not go below 1 
                {
                    if (amount > _playerRef.Data.Health)
                    {
                        amount = _playerRef.Data.Health - 1;
                    }
                }
                _playerRef.Data.TakeHealth(amount, type);
                _playerRef.NotifyDamageReceived(_playerRef.transform.position);

                SetClimbing(false, null);

                if (_playerRef.Data.Health <= 0 && !Dead)
                {
                    SetDead(true);

                    Dashing = false;
                    Stunned = false;
                    Hovering = false;

                    SetWallSliding(false);
                    SetWallGripping(false);
                    SetWallJumpingOffWall(false);
                    SetGrabbing(false);

                    if (type == DamageType.ENVIROMENT || type == DamageType.GUARDIANS || type == DamageType.NULL)
                    {
                        _playerRef.Animation.AnimationStateMachine.Active = false;
                        _playerRef.Effects.PlayDestructionEffect();
                    }
                }
                else
                {
                    Invulnerable = true;
                    GlobalData.Timekeeper.StartTimer(new System.Action(() => { Invulnerable = false; }), PlayerConstants.DAMAGE_INVULNERABILITY_TIME);
                }
            }
        }
        #endregion

        #region State Setters
        /// <summary>
        /// Sets the Dead property of the player's state, and calls the appropriate notifier (for death or respawning).
        /// </summary>
        /// <param name="dead">Whether the player is dead.</param>
        /// <param name="type">The type of damage received by the player (only relevant in death).</param>
        public void SetDead(bool dead)
        {
            Dead = dead;
            if (dead)
            {
                GlobalData.Player.NotifyDead();
            }
            else
            {
                GlobalData.Player.NotifyRespawned();
            }
        }

        /// <summary>
        /// Apply the stun power of the projectile to the Player, and stun them if this causes them to
        /// cross their stun threshold.
        /// </summary>
        /// <param name="stunPower">The amount of stun power to apply.</param>
        /// <param name="stunTime">How long the character should be stunned for if they cross their stun threshold.</param>
        public void SetStunned(float stunPower, IDamageGiver giver, GameObject owner)
        {
            _currentStunAmount += stunPower;

            if (_currentStunAmount >= GlobalData.Player.Data.Health)
            {
                Stunned = true;
                _playerRef.NotifyStunned(true);
            }
        }

        public void SetDashing(bool dashing)
        {
            Dashing = dashing;
        }

        public void SetHovering(bool hovering)
        {
            if (Hovering != hovering) //If the current hovering state does not equal the hovering state being set.
            {
                Hovering = hovering;
                _playerRef.NotifyHovering(hovering);
            }

            if (!Hovering)
            {
                GlobalData.Timekeeper.StartTimer(() => _rechargeEnergyCells = true, PlayerConstants.ENERGY_CELL_RECHARGE_TIME_DELAY);
            }
        }

        public void SetInsideAirGust(bool isInGust)
        {
            InsideAirGust = isInGust;
            if (InsideAirGust)
            {
                SetHovering(false);
            }
        }

        public void SetBouncing(bool isBouncing)
        {
            Bouncing = isBouncing;

            if (isBouncing)
            {
                GlobalData.Timekeeper.StartTimer(() => SetBouncing(false), PlayerConstants.BOUNCE_TIME);
            }
            else
            {
                _bouncingTime = 0f;
            }
        }

        public void SetBumped(bool wasBumped)
        {
            Bumped = wasBumped;

            if (wasBumped)
            {
                GlobalData.Timekeeper.StartTimer(() => SetBumped(false), PlayerConstants.BOUNCE_TIME);
            }
        }

        public void SetGrabbing(bool grabbing, IGrabbable grabbedObject = null)
        {
            Grabbing = grabbing;
            if (grabbing)
            {
                GrabbedObject = grabbedObject;
                if (grabbedObject == null)
                {
                    Debug.LogWarning("A call to set the player's Grabbing state to true was made, but the grabbed object provided was null (or no object was provided).");
                }
                else
                {
                    grabbedObject.Grab();
                }
            }
            else
            {
                GrabbedObject?.Release();
                GrabbedObject = null;
            }
        }

        public void SetWallSliding(bool wallSliding)
        {
            if (wallSliding != WallSliding)
            {
                WallSliding = wallSliding;
                _playerRef.NotifyWallSliding(wallSliding);
            }
        }

        public void SetWallGripping(bool wallGripping)
        {
            WallGripping = wallGripping;
            _playerRef.NotifyWallGripping(wallGripping);
        }

        public void SetWallJumpingOffWall(bool wallJumpingOffWall)
        {
            WallJumpingOffWall = wallJumpingOffWall;
            if (wallJumpingOffWall)
            {
                GlobalData.Timekeeper.StartTimer(() => WallJumpingOffWall = false, PlayerConstants.WALL_JUMP_OFF_WALL_TIME, "Wall Jump off Wall", true);
            }
            else
            {
                GlobalData.Timekeeper.EndTimer("Wall Jump off Wall");
            }
        }

        public void SetClimbing(bool climbing, ClimbableObject climbableObject, bool playerShouldFollowClimbingPath = false, Vector2? nextPathTarget = null, Vector2? previousPathTarget = null)
        {
            if (climbing != Climbing)
            {
                Climbing = climbing;

                if (climbing)
                {
                    CurrentActiveClimbableObject = climbableObject;

                    if (playerShouldFollowClimbingPath)
                    {
                        ShouldFollowClimbingPath = true;

                        NextClimbingPathTarget = nextPathTarget.GetValueOrDefault();
                        PreviousClimbingPathTarget = previousPathTarget.GetValueOrDefault();
                    }
                }
                else
                {
                    CurrentActiveClimbableObject = null;
                    ShouldFollowClimbingPath = false;

                    NextClimbingPathTarget = Vector2.zero;
                    PreviousClimbingPathTarget = Vector2.zero;
                }

                _playerRef.NotifyClimbing(climbing);
            }
        }

        public void SetClimbableObject(ClimbableObject climbableObject)
        {
            CurrentActiveClimbableObject = climbableObject;
        }

        public void SetClimbingPathTargets(Vector2 nextPathTarget, Vector2 previousPathTarget)
        {
            NextClimbingPathTarget = nextPathTarget;
            PreviousClimbingPathTarget = previousPathTarget;
        }

        public void SetLightsOn(bool lightsOn)
        {
            LightsOn = lightsOn;
            if (lightsOn)
            {
                _flashlight.enabled = true;
                _personalLight.enabled = true;
            }
            else
            {
                _flashlight.enabled = false;
                _personalLight.enabled = false;
            }
        }

        public void SetEnvironmentalState(GravityType type)
        {
            //POSSIBLE IMPROVEMENT: Is separate state variables for each substance type really necessary?
            if (type == GravityType.Lava)
            {
                InMud = false;
                InLava = true;
            }
            else if (type == GravityType.Mud)
            {
                InLava = false;
                InMud = true;
            }
            else if (type == GravityType.Base)
            {
                InLava = false;
                InMud = false;
            }
        }

        public void SetInsideCameraMovementAreaOverrideZone(bool insideZone)
        {
            InsideCameraMovementAreaOverrideZone = insideZone;
        }
        #endregion

        #region Callbacks
        /// <summary>
        /// Performs any actions required any time the player respawns.
        /// </summary>
        private void OnRespawn(bool positive)
        {
            Dead = false;
            InLava = false;
        }

        public void OnCollideBelow(Vector2 velocity)
        {
            TimeInAir = 0f;
        }

        public void OnEnergyCellMoveUsed()
        {
            if (!Hovering)
            {
                GlobalData.Timekeeper.StartTimer(() => _rechargeEnergyCells = true, PlayerConstants.ENERGY_CELL_RECHARGE_TIME_DELAY);
            }
        }
        #endregion
    }
}
