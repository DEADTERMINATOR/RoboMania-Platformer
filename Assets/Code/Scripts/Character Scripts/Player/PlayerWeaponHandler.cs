﻿using MovementEffects;
using Spine;
using Spine.Unity;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using Inputs;
using static GlobalData;
using Weapons.Player;
using Delaunay.Geo;
using UnityEngine.Rendering;
using Weapons.Player.Forms;

namespace Characters.Player
{
    public class PlayerWeaponHandler
    {
        #region Constants
        /* The maximum rotation the head can achieve before the head appears detached from the body. */
        private const float MAX_HEAD_ROTATION = 63f;
        /* The maximum rotation the head can achieve if they are looking down while wall gripping before the head appears detached from the body.
         * Don't ask me why this is an edge case. */
        private const float MAX_WALL_GRIPPING_DOWNWARD_HEAD_ROTATION = 35;

        /* Indicated no aiming is occuring */
        private const float NO_FIRE_AIM_ANGLE = -999f;

        /* The maximum number of degrees the player's arm is allowed to rotate in a second when lerping their aim to the initial position. */
        private const float MAX_DEGREES_PER_SECONDS = 240;
        /* The maximum amount of time the entire initial arm lerp is allowed to take. This value supersedes the maximum number of degrees. */
        private const float MAX_ARM_LERP_TIME = 0.1f;

        /// <summary>
        /// Spine animation defines straight down as 0 degrees, while Unity2D defines right as 0 degrees.
        /// So when converting between Spine's rotations and Unity's rotations, we need to add or subtract 90 degrees.
        /// </summary>
        private const float HEAD_ROTATION_OFFSET = 90;

        /* If the player presses the fire key without aiming, they will fire forward. This is how long the player's arm should stay raised
         * for further firing before automatically lowering it */
        private const float TIME_TO_CONTINUE_AIMING_FORWARD_WITHOUT_FIRING = 0.5f;
        #endregion

        #region Enums
        /// <summary>
        /// An enum containing the possible aiming states the player's weapon can be in.
        /// </summary>
        public enum AimState { GOING_UP, UP, GOING_DOWN, DOWN }

        /// <summary>
        /// An enum containing the possible types of aiming the player could be performing.
        /// </summary>
        public enum AimType { PRECISION, FORWARD, NO_AIM }
        #endregion

        #region Public Properties
        /// <summary>
        /// Reference to the player's weapon that contains the different weapon forms they possess and the ability to fire them.
        /// </summary>
        public PlayerWeapon PlayerWeapon;

        /// <summary>
        /// The layers that the precision aimer will respond if they are hit.
        /// </summary>
        public LayerMask PrecisionAimCollideSurfaces = OBSTACLE_LAYER_SHIFTED | NPC_LAYER_SHIFTED | ENEMY_LAYER_SHIFTED;

        /// <summary>
        /// Whether the player is currently pressing a firing input.
        /// </summary>
        public bool IsShooting { get; private set; }

        /// <summary>
        /// Whether the player is currently aiming but not shooting.
        /// </summary>
        public bool IsAiming { get; private set; }


        /// <summary>
        /// The current state the player's gun is in with regards to aiming.
        /// </summary>
        public AimState CurrentAimState { get; private set; }
        #endregion

        #region Private Properties
        /// <summary>
        /// The Light2D that represents the aiming cone.
        /// </summary>
        private Light2D _aimingLight;

        private AimType _aimType = AimType.NO_AIM;

        /// <summary>
        /// The current aim angle the lerp function is targeting.
        /// </summary>
        private float _currentArmAngle;

        /// <summary>
        /// The most recent valid angle for projectiles to be fired at.
        /// </summary>
        private float _lastValidFireAngle;

        /// <summary>
        /// Whether the player has fired at least once since aiming began.
        /// </summary>
        private bool _hasFiredSinceAiming = false;

        /// <summary>
        /// Stored reference to the player master due to frequent access.
        /// </summary>
        private PlayerMaster _playerRef;
        private PlayerInput _playerInput;

        /* References to the commonly used bones for weapon handling */
        private Bone _firingPointBoneRef;

        /* The world positions of the above bones */
        private Vector3 _firingPointWorldPosition;
        private Vector3 _aimingPointWorldPosition;

        /* How much longer the player's arm will continue aiming without any fire input (forward firing only) */
        private float _remainingAimTime = 0;

        private float _currentWeaponFormSpread = 0;
        #endregion

        #region Constructor
        public PlayerWeaponHandler(PlayerMaster playerRef)
        {
            string playerRefGOName = playerRef.transform.root.gameObject.name;

            _aimingLight = playerRef.State.AimingLight;

            CurrentAimState = AimState.DOWN;

            playerRef.Dead += OnDead;
            playerRef.WallSliding += OnEndWallSliding;
            playerRef.DashPerformed += OnDashPerformed;

            _playerRef = playerRef;
            PlayerWeapon = playerRef.GetComponent<PlayerWeapon>();
            PlayerWeapon.PlayerRef = playerRef;

            PlayerWeapon.WeaponSwitch += OnWeaponSwitch;       
        }

        public void LateInit()
        {
            _firingPointBoneRef = _playerRef.Animation.FiringPoint;
            _playerInput = _playerRef.Input;
        }
        #endregion

        #region Update
        public void WeaponUpdate()
        {
            /* Animation update has already run by the time we get here, so we don't need to update the bone transforms before getting their world position. */
            _firingPointWorldPosition = _playerRef.Animation.FiringPoint.GetWorldPosition(_playerRef.transform);
            _aimingPointWorldPosition = _playerRef.Animation.AimingPoint.GetWorldPosition(_playerRef.transform);

            if (TheGame.GetRef.gameState != TheGame.GameState.TRANSITION)
            {
                HandleFiring();
            }

            if (PlayerWeapon.ActiveForm is MachineGunWeaponForm)
            {
                var machineGun = PlayerWeapon.ActiveForm as MachineGunWeaponForm;
                _currentWeaponFormSpread = machineGun.CurrentSpread;
            }
        }
        #endregion

        #region Firing
        /// <summary>
        /// Handles the direction the currently equipped weapon fires in (if the player possesses one).
        /// </summary>
        private void HandleFiring()
        {
            bool shouldFire = false;
            float currentFireAngle = NO_FIRE_AIM_ANGLE;

            if (CurrentAimState != AimState.GOING_DOWN
                && (!_playerRef.State.WallSliding || _playerRef.State.WallGripping)
                && (!_playerRef.State.Climbing || (_playerRef.State.Climbing && StaticTools.ThresholdApproximately(_playerRef.MovementInput, Vector2.zero, 0.05f))))
            {
                /* Check if the player is precision aiming and calculate their aim angle */
                if (_playerInput.InputDictionary["Precision Aim"].IsDownOrHeld && (_aimType == AimType.NO_AIM || (_aimType == AimType.FORWARD && CurrentAimState == AimState.UP) || _aimType == AimType.PRECISION))
                {
                    _remainingAimTime = 0;
                    if (_playerRef.State.WallGripping)
                    {
                        if (_playerRef.Animation.MainAnimation.AnimationEnumName == PlayerAnimationController.Animations.WallGripFire && _playerRef.Animation.MainAnimation.Animation.IsComplete)
                        {
                            if (InputManager.Instance.CurrentDeviceType == InputManager.DeviceType.KeyboardMouse)
                            {
                                currentFireAngle = CheckPrecisionFireDirectionKeyboardMouse();
                            }
                            else if (InputManager.Instance.CurrentDeviceType == InputManager.DeviceType.Controller)
                            {
                                currentFireAngle = CheckPrecisionFireDirectionController();
                            }
                        }
                        else if (!IsAiming)
                        {
                            //When wall gripping, we need to set IsAiming up here instead of inside of SetupAiming because IsAiming is what triggers the change in animation to the wall grip animation,
                            //but we don't want to perform any aiming setup or arm rotations until that wall grip animation has run to completion.
                            IsAiming = true;
                        }
                    }
                    else
                    {
                        if (InputManager.Instance.CurrentDeviceType == InputManager.DeviceType.KeyboardMouse)
                        {
                            currentFireAngle = CheckPrecisionFireDirectionKeyboardMouse();
                        }
                        else if (InputManager.Instance.CurrentDeviceType == InputManager.DeviceType.Controller)
                        {
                            currentFireAngle = CheckPrecisionFireDirectionController();
                        }
                    }
                }
                else if (IsAiming && _aimType == AimType.PRECISION)
                {
                    currentFireAngle = CancelPrecisionAim();
                    EndAiming();
                }

                if (currentFireAngle == NO_FIRE_AIM_ANGLE)
                {
                    if (!CheckForwardFire(ref shouldFire))
                    {
                        if (IsAiming)
                        {
                            _remainingAimTime -= Time.deltaTime;

                            if (_remainingAimTime <= 0)
                            {
                                EndAiming();
                            }
                        }
                    }
                }
                else
                {
                    //The player pressed the fire key, and a fire angle was found through precision aiming.
                    _lastValidFireAngle = currentFireAngle;

                    if (PlayerWeapon.CanFire && !_playerInput.BlockWeaponFire && (_playerInput.InputDictionary["Fire"].GetTheState() == PlayerInput.ButtonState.State.DOWN || (PlayerWeapon.ActiveForm.HoldToFireAllowed && _playerInput.InputDictionary["Fire"].IsDownOrHeld)))
                    {
                        shouldFire = true;
                    }
                }

                if (PlayerWeapon != null && (shouldFire || (PlayerWeapon.ActiveForm is ChargeRifleWeaponForm && _playerInput.InputDictionary["Fire"].IsReleased)))
                {
                    //If the active weapon is the charge rifle, we need to call Fire one last time because the fire key release may be the signal that allows the weapon to be fired.
                    PlayerWeapon.FireWeapon(_firingPointWorldPosition, new Vector3(0, 0, _lastValidFireAngle));
                    _hasFiredSinceAiming = true;
                }
            }
            else if (IsAiming)
            {
                EndAiming();
            }
        }
        #endregion

        #region Forward Aiming
        /// <summary>
        /// Checks and handles if the player is firing directly forward of them by pressing the fire button without aiming. Handles both the setup
        /// and execution if this is the player's intention.
        /// </summary>
        /// <returns>True if the player is either setting up a forward fire, or is in the process of firing. False otherwise.</returns>
        private bool CheckForwardFire(ref bool shouldFire)
        {
            if ((_playerInput.InputDictionary["Fire"].IsDownOrHeld || (_aimType == AimType.FORWARD && !_hasFiredSinceAiming)) && PlayerWeapon.CanFire)
            {
                //The player is not aiming but they have pressed the fire button. So just fire forward.

                //We don't want the forward projectile to fire until the player's gun is fully aimed up, so if the aim state is currently down or going down,
                //start the lerp to bring it back up.
                if (CurrentAimState == AimState.DOWN || CurrentAimState == AimState.GOING_DOWN)
                {
                    _aimType = AimType.FORWARD;
                    SetupAiming(90, 0);

                    if (_playerRef.State.WallGripping)
                    {
                        //If the player is wall gripping, then their facing direction is the opposite of what it visually appears to be. So we need to do the reverse of what we'd normally do.
                        _lastValidFireAngle = _playerRef.Collisions.FaceDir == 1 ? 180 : 0;
                    }
                    else
                    {
                        _lastValidFireAngle = _playerRef.Collisions.FaceDir == 1 ? 0 : 180;
                    } 
                }
                /* The player's gun is fully aimed up, but we need to make sure we either haven't fired once this aim cycle (pressing a quick fire key forces
                 * at least one projectile fire), or the player is still holding fire. */
                else if (CurrentAimState == AimState.UP && ((_playerInput.InputDictionary["Fire"].GetTheState() == PlayerInput.ButtonState.State.DOWN || (_playerInput.InputDictionary["Fire"].IsDownOrHeld && PlayerWeapon.ActiveForm.HoldToFireAllowed)) || !_hasFiredSinceAiming))
                {
                    shouldFire = true;
                    _remainingAimTime = TIME_TO_CONTINUE_AIMING_FORWARD_WITHOUT_FIRING;
                }

                return true;
            }

            return false;
        }
        #endregion

        #region Precision Aiming
        private float CheckPrecisionFireDirectionKeyboardMouse()
        {
            IsShooting = false;

            Vector3 mousePosition = Input.mousePosition; //Returns the mouse position on the screen in pixels.
            Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(mousePosition); //Converts the pixel coordinates to a world position.
            mouseWorldPosition.z = 0;

            Vector3 worldDirection = mouseWorldPosition - _playerRef.transform.position; //Get the direction vector from the player to the world position mouse coordinates.
            worldDirection.Normalize();

            float aimingDirectionSign = Mathf.Sign(mouseWorldPosition.x - _playerRef.transform.position.x);

            if (_playerRef.State.WallGripping)
            {
                //Flipping the sign of the y-axis of the world direction when aiming while wall-gripping is needed to make it work. Not entirely sure why.
                SetForearmAndHeadRotationForPrecisionAiming(new Vector3(worldDirection.x, -worldDirection.y), aimingDirectionSign);
            }
            else
            {
                SetForearmAndHeadRotationForPrecisionAiming(worldDirection, aimingDirectionSign);
            }

            float fireAngle = CalculateFireAngle();
            ConstructAimingLightPoints(worldDirection, fireAngle);

            return fireAngle;
        }

        private float CheckPrecisionFireDirectionController()
        {
            IsShooting = false;

            Vector3 worldDirection = Vector3.zero;
            float aimingDirectionSign = 0;

            if (_playerInput.PrecisionAimInput.magnitude > CONTROLLER_STICK_DEADZONE)
            {
                worldDirection = _playerInput.PrecisionAimInput;
                aimingDirectionSign = Mathf.Sign(_playerInput.PrecisionAimInput.x);
            }
            else
            {
                return CancelPrecisionAim();
            }

            if (_playerRef.State.WallGripping)
            {
                SetForearmAndHeadRotationForPrecisionAiming(new Vector3(worldDirection.x, -worldDirection.y), aimingDirectionSign);
            }
            else
            {
                SetForearmAndHeadRotationForPrecisionAiming(worldDirection, aimingDirectionSign);
            }

            float fireAngle = CalculateFireAngle();
            ConstructAimingLightPoints(worldDirection, fireAngle);

            return fireAngle;
        }

        /// <summary>
        /// Performs all actions required to cancel an active precision aim.
        /// </summary>
        /// <returns></returns>
        private float CancelPrecisionAim()
        {
            if (_aimType == AimType.PRECISION)
            {
                IsAiming = false;
            }

            _aimingLight.enabled = false;
            return NO_FIRE_AIM_ANGLE;
        }

        /// <summary>
        /// Calculates and sets the rotations of the player's forearm and head bones based on the desired mouse aiming direction.
        /// </summary>
        /// <param name="worldDirection">The direction vector of the player's forearm bone to the world location of the mouse.</param>
        /// <param name="aimingDirectionSign">The sign (positive or negative) that the player's root bone is currently facing.</param>
        /// <returns>Whether the player's facing direction was changed by the setting of the rotation.</returns>
        private bool SetForearmAndHeadRotationForPrecisionAiming(Vector3 worldDirection, float aimingDirectionSign)
        {
            if (CurrentAimState == AimState.UP && _aimType == AimType.FORWARD)
            {
                //It's possible to transition straight from a forward aim to a precision aim if the player triggers precision aim while their gun is still up from a forward aim. So convert the aim type in this case.
                _aimType = AimType.PRECISION;
                _aimingLight.enabled = true;
            }

            float calculatedAimAngle = Vector2.SignedAngle(Vector2.down, worldDirection);
            if (calculatedAimAngle < 0)
            {
                calculatedAimAngle += 360;
            }

            //Angles are relative to the orientation of the Spine skeleton. So when we change the facing direction of the player, we need to calculate what the current aim angle would be relative to their current orientation.
            //Essentially, for the arm to always be facing forward of the character, the arm can only be rotated between 0 and 180 degrees regardless of which direction the character is facing.
            float calculatedAimAngleAdjustedForLeftFacingPlayer = calculatedAimAngle;
            if (worldDirection.x < 0)
            {
                calculatedAimAngleAdjustedForLeftFacingPlayer = Vector2.Angle(Vector2.down, worldDirection);
            }

            bool isWallGripping = _playerRef.State.WallGripping;
            float newHeadAimAngle = calculatedAimAngle > 180 ? calculatedAimAngleAdjustedForLeftFacingPlayer - HEAD_ROTATION_OFFSET : calculatedAimAngle - HEAD_ROTATION_OFFSET;
            if (!isWallGripping && newHeadAimAngle > MAX_HEAD_ROTATION)
            {
                newHeadAimAngle = MAX_HEAD_ROTATION;
            }
            else if (newHeadAimAngle < -MAX_HEAD_ROTATION)
            {
                newHeadAimAngle = -MAX_HEAD_ROTATION;
            }
            else if (isWallGripping && newHeadAimAngle > MAX_WALL_GRIPPING_DOWNWARD_HEAD_ROTATION)
            {
                newHeadAimAngle = MAX_WALL_GRIPPING_DOWNWARD_HEAD_ROTATION;
            }

            bool playerFaceDirChanged = false;
            if (!_playerRef.State.WallSliding)
            {
                if (calculatedAimAngle >= 0 && calculatedAimAngle <= 180 && aimingDirectionSign > 0 && _playerRef.Collisions.FaceDir == -1)
                {
                    _playerRef.Animation.SetSpriteDirection(true, true, true);
                    playerFaceDirChanged = true;
                }
                else if (calculatedAimAngle > 180 && calculatedAimAngle <= 360 && aimingDirectionSign < 0 && _playerRef.Collisions.FaceDir == 1)
                {
                    _playerRef.Animation.SetSpriteDirection(false, true, true);
                    playerFaceDirChanged = true;
                }
            }

            float newArmAimAngle = calculatedAimAngle > 180 ? calculatedAimAngleAdjustedForLeftFacingPlayer : calculatedAimAngle; //Angles over 180 degrees will have the player facing left. Since we flip the skeleton in this case, the actual rotation angle for the arm will be back to between 0 and 180.
            if (CurrentAimState != AimState.UP)
            {
                if (CurrentAimState == AimState.DOWN || CurrentAimState == AimState.GOING_DOWN)
                {
                    _aimType = AimType.PRECISION;
                    SetupAiming(newArmAimAngle, newHeadAimAngle);
                }
                else
                {
                    if (Mathf.Abs(newArmAimAngle - _currentArmAngle) > 10)
                    {
                        SetupAiming(newArmAimAngle, newHeadAimAngle);
                    }
                }
            }
            else
            {
                if (!isWallGripping && ((calculatedAimAngle >= 0 && calculatedAimAngle <= 180 && _playerRef.Collisions.FaceDir == 1) || (calculatedAimAngle > 180 && calculatedAimAngle <= 360 && _playerRef.Collisions.FaceDir == -1)))
                {
                    _playerRef.Animation.RightForearmBone.Rotation = newArmAimAngle;
                    _playerRef.Animation.HeadBone.Rotation = newHeadAimAngle;
                }
                else if (isWallGripping && ((calculatedAimAngle >= 0 && calculatedAimAngle <= 180 && _playerRef.Collisions.FaceDir == -1) || (calculatedAimAngle > 180 && calculatedAimAngle <= 360 && _playerRef.Collisions.FaceDir == 1)))
                {
                    if (Mathf.Abs(calculatedAimAngleAdjustedForLeftFacingPlayer - _currentArmAngle) >= 90)
                    {
                        SetupAiming(calculatedAimAngleAdjustedForLeftFacingPlayer, newHeadAimAngle);
                    }
                    else
                    {
                        _playerRef.Animation.RightUpperArmBone.Rotation = calculatedAimAngleAdjustedForLeftFacingPlayer;
                        _playerRef.Animation.HeadBone.Rotation = newHeadAimAngle;
                    }
                }
            }

            return playerFaceDirChanged;
        }

        private void ConstructAimingLightPoints(Vector3 worldDirection, float fireAngle)
        {
            var point1 = (Vector2)_aimingPointWorldPosition + (0.05f * Vector2.Perpendicular(worldDirection.normalized));
            Vector2 point2;
            Vector2 point3;
            var point4 = (Vector2)_aimingPointWorldPosition - (0.05f * Vector2.Perpendicular(worldDirection.normalized));

            if (_currentWeaponFormSpread == 0)
            {
                Vector2 endAimerOffset = StaticTools.Vector2FromAngle(fireAngle) * 100f;
                point2 = point1 + endAimerOffset;
                point3 = point4 + endAimerOffset;
            }
            else
            {
                point2 = point1 + StaticTools.Vector2FromAngle(fireAngle + _currentWeaponFormSpread) * 100f;
                point3 = point4 + StaticTools.Vector2FromAngle(fireAngle - _currentWeaponFormSpread) * 100f;
            }

            //Points on a freeform light are stored as an offset from the base light position, so we need to subtract the position of the light from each point.
            var lightPoints = new Vector3[] { point1 - (Vector2)_aimingLight.transform.position, point2 - (Vector2)_aimingLight.transform.position, point3 - (Vector2)_aimingLight.transform.position, point4 - (Vector2)_aimingLight.transform.position };
            _aimingLight.DefineFreeformLightShape(lightPoints);
        }

        /// <summary>
        /// Calculates the angle along which a fired projectile will actually travel if it is to be fired "forward" from the gun.
        /// </summary>
        /// <returns>The angle a fired projectile will travel.</returns>
        private float CalculateFireAngle()
        {
            // Calculate the angle the projectile should travel based on direction the muzzle of the gun is actually facing.
            float fireCalculationAngle = _playerRef.Collisions.FaceDir == 1 ? _firingPointBoneRef.WorldRotationX + HEAD_ROTATION_OFFSET : _firingPointBoneRef.WorldRotationX + HEAD_ROTATION_OFFSET + 180;
            if (_playerRef.State.WallGripping)
            {
                fireCalculationAngle += 180;
            }

            Vector2 fireVector = StaticTools.RotateVector2(Vector2.right, fireCalculationAngle);
            float fireAngle = Vector2.Angle(Vector2.right, fireVector);
            if (fireVector.y < 0)
            {
                fireAngle = 360 - fireAngle;
            }
            return fireAngle;
        }
        #endregion

        #region Aim Lerping
        private IEnumerator<float> LerpArmAndHead(float desiredArmRotation, float desiredHeadRotation)
        {
            Bone rightUpperArmBoneRef = _playerRef.Animation.RightUpperArmBone;
            Bone rightForearmBoneRef = _playerRef.Animation.RightForearmBone;
            Bone headBoneRef = _playerRef.Animation.HeadBone;

            float startingUpperArmRotation = rightUpperArmBoneRef.Rotation;
            float startingForearmArmRotation = rightForearmBoneRef.Rotation;
            float startingHeadRotation = headBoneRef.Rotation;

            float percentComplete = 0f;

            CurrentAimState = AimState.GOING_UP;
            if (_aimType == AimType.PRECISION)
            {
                _aimingLight.enabled = true;
            }

            bool isWallGripping = _playerRef.State.WallGripping;

            float degreesToRotate = isWallGripping == true ? Mathf.Abs(desiredArmRotation - rightUpperArmBoneRef.Rotation) : Mathf.Abs(desiredArmRotation - rightForearmBoneRef.Rotation);
            float estimatedTime = degreesToRotate / MAX_DEGREES_PER_SECONDS;

            if (estimatedTime > MAX_ARM_LERP_TIME)
            {
                estimatedTime = MAX_ARM_LERP_TIME;
            }

            while (percentComplete < 1f)
            {
                percentComplete += Time.deltaTime / estimatedTime;

                if (isWallGripping)
                {
                    rightUpperArmBoneRef.Rotation = Mathf.LerpAngle(startingUpperArmRotation, desiredArmRotation, percentComplete);
                }
                else
                {
                    rightForearmBoneRef.Rotation = Mathf.LerpAngle(startingForearmArmRotation, desiredArmRotation, percentComplete);
                }

                headBoneRef.Rotation = Mathf.LerpAngle(startingHeadRotation, desiredHeadRotation, percentComplete);

                yield return 0f;
            }

            CurrentAimState = AimState.UP;
            yield return 0f;
        }
        #endregion

        #region Support Functions
        private void SetupAiming(float desiredArmRotation, float desiredHeadRotation)
        {
            if (!_playerRef.State.WallGripping)
            {
                _playerRef.Animation.StopArmsAndHeadAnimation();
            }

            IsAiming = true;
            _currentArmAngle = desiredArmRotation;

            _playerRef.State.SetGrabbing(false);

            Timing.KillCoroutines("LerpArmAndHead");
            Timing.RunCoroutine(LerpArmAndHead(desiredArmRotation, desiredHeadRotation), "LerpArmAndHead");
        }

        private void EndAiming()
        {
            Timing.KillCoroutines("LerpArmAndHead");

            _playerRef.Animation.MatchArmsAndHeadToMainAnimation();

            _hasFiredSinceAiming = false;
            _currentArmAngle = NO_FIRE_AIM_ANGLE;

            _aimingLight.enabled = false;

            CurrentAimState = AimState.DOWN;
            _aimType = AimType.NO_AIM;
            IsAiming = false;
        }
        #endregion

        #region Callbacks
        private void OnDead()
        {
            PlayerWeapon.Pool?.CleanThePool();
        }

        private void OnTransitionOrReload()
        {
            PlayerWeapon.Pool?.CleanThePool();
        }

        private void OnEndWallSliding(bool positive)
        {
            if (!positive)
            {
                EndAiming();
            }
        }

        private void OnDashPerformed()
        {
            EndAiming();
        }

        private void OnWeaponSwitch(WeaponForm newForm)
        {
            switch (newForm.WeaponFormType)
            {
                case WeaponFormProperties.WeaponForms.MachineGun:
                    var machineGun = newForm as MachineGunWeaponForm;
                    _currentWeaponFormSpread = machineGun.CurrentSpread;
                    break;
                case WeaponFormProperties.WeaponForms.Shotgun:
                    var shotgun = newForm as ShotgunWeaponForm;
                    _currentWeaponFormSpread = shotgun.Spread / 2;
                    break;
                default:
                    _currentWeaponFormSpread = 0;
                    break;
            }
        }
        #endregion
    }
}
