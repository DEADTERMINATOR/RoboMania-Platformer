﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace Characters.Player
{
    public class PlayerRepair
    {
        #region PublicVars
        [HideInInspector]
        public float secondsToRepair = 3.5f;
        public float costforRepair = 50;
        public float MaxHealtherPreRepair = 50;
        #endregion

        #region PrivateVars
        public float timeElapsed;
        private bool _isRepairing;

        private float _amountOfHealing;
        private float _costPerPoint;
        #endregion

        // Use this for initialization
        public PlayerRepair()
        {
            _costPerPoint = costforRepair / MaxHealtherPreRepair;
        }

        public void RepairUpdate()
        {
            if (_isRepairing)
            {
                timeElapsed += Time.deltaTime;

                if (timeElapsed >= secondsToRepair)
                {
                    _isRepairing = false;
                    GlobalData.Player.Data.Heal(_amountOfHealing);
                    GlobalData.Player.NotifyRepair(false);
                }

            }
            else if (Input.GetButtonDown("Repair"))
                StartRepair();
        }


        private void StartRepair()
        {

            ///IF the player has 1 or less scraps Give them a waring 
            if (GlobalData.Player.Data.TotalScrap <= 1)
            {
                GlobalData.Player.NotifyRepairFailed(true);
                return;//fail state
            }

            timeElapsed = 0;
            //dose the player have any scap to use
            if (GlobalData.Player.Controller.Collisions.IsCollidingBelow && GlobalData.Player.Data.TotalScrap > 0)
            {
                //How Much Healing is needed 
                float healingNeeded = GlobalData.Player.Data.MaxHealth - GlobalData.Player.Data.Health;

                // is it more then the max
                if (healingNeeded > MaxHealtherPreRepair)
                {
                    healingNeeded = MaxHealtherPreRepair;
                }

                //Whats the cost of a full heal
                float cost = Mathf.Ceil(_costPerPoint * healingNeeded);

                //Can the player afford the heal 
                if (cost <= GlobalData.Player.Data.TotalScrap)
                    //set the healing to full amount
                    _amountOfHealing = healingNeeded;
                else // the player is too poor
                {
                    //set the cost to use all the metal it can
                    cost = GlobalData.Player.Data.TotalScrap;
                    // how much healing can be done at that cost
                    _amountOfHealing = cost / _costPerPoint;
                }

                // remove the scrap from the player 
                GlobalData.Player.Data.RequestScrap((int)cost);

                //start the reparing
                _isRepairing = true;

                GlobalData.Player.NotifyRepair(true);
            }
            else
            {
                GlobalData.Player.NotifyRepairFailed(true);
            }

        }
    }
}
