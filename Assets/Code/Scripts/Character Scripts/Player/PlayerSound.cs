﻿using UnityEngine;
using System.Collections;
using Characters.Sound;

namespace Characters.Player
{
    public class PlayerSound : CharacterSounds
    {
        public AudioClip JetBootSound;
        public AudioClip ScrapPickUpSound;
        public AudioClip PickUpSound;
        public AudioClip StepSound;
        public AudioClip LandOnGroundSound;
        public AudioClip JumpSound;
        public AudioClip DashSound;


        private bool _wasOnGround = true;
        private bool _isPlayingJetPackSound = false;


        public PlayerSound(PlayerMaster playerRef)
        {
            AudioSource = playerRef.GetComponent<AudioSource>();

            //Assets/Resources/
            HitSound = Resources.Load("Sounds/Player Sounds/Hit Sounds/player_hitsound_1") as AudioClip;
            DeathSound = Resources.Load("Sounds/Weapon Sounds/110115__ryansnook__small-explosion") as AudioClip;
            JetBootSound = Resources.Load("Sounds/Player Sounds/Jetboot Sounds/THRUSTER_Flickering_Flame_loop_mono") as AudioClip;
            StepSound = Resources.Load("Sounds/Player Sounds/Footsteps/Rock + Gravel Step") as AudioClip;
            //StepSoundA = Resources.Load("Sounds/Player Sounds/Walk/sterp1.wav") as AudioClip;
            //StepSoundB = Resources.Load("Sounds/Player Sounds/Walk/sterp2.wav") as AudioClip;
            LandOnGroundSound = Resources.Load("Sounds/FX/jumpland") as AudioClip;
            JumpSound = Resources.Load("Sounds/FX/Woosh-Mark_DiAngelo-4778593") as AudioClip;
            DashSound = Resources.Load("Sounds/Player Sounds/Dash") as AudioClip;

            playerRef.DashPerformed += new PlayerMaster.ActionEvent(PlayDashSound);
            playerRef.Hovering += new PlayerMaster.StateChange(HandleJetbootSound);
        }


        public void PlayLandOnGroundSound(bool onGround)
        {
            if (!_wasOnGround && onGround)
            {
                PlaySoundRandomPitch(LandOnGroundSound, 0.9f, 1.2f, 0.5f, "Player Actions");
            }
            _wasOnGround = onGround;
        }

        public void PlayDashSound()
        {
            PlaySoundRandomPitch(DashSound, 0.9f, 1.2f, 0.65f, "Player Actions");
        }

        public void PlayJumpSound()
        {
            PlaySoundRandomPitch(JumpSound, 0.9f, 1.2f, 0.5f, "Player Actions");
        }

        public void PlayJetbootSound(bool isContinuous)
        {
            if (!_isPlayingJetPackSound)
            {
                _isPlayingJetPackSound = true;

                if (isContinuous)
                    PlaySoundLoop(JetBootSound, 0.25f, "Player Actions");
                else
                    PlaySoundOnce(JetBootSound, 0.25f, "Player Actions");
            }
        }

        public void StopJetbootSound()
        {
            StopSoundLoop(JetBootSound);
            _isPlayingJetPackSound = false;
        }

        public void PlayFootstepSound()
        {
            PlaySoundRandomPitch(StepSound, 0.5f, 1.5f, 0.15f, "Player Actions");
        }

        private void HandleJetbootSound(bool isHovering)
        {
            if (isHovering)
                PlayJetbootSound(true);
            else
                StopJetbootSound();
        }
    }
}
