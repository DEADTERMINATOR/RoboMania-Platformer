﻿using Characters.Player;
using LevelSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickupHandler : MonoBehaviour
{
    #region Delegates
    /// <summary>
    /// 
    /// </summary>
    /// <param name="txt"></param>
    /// <param name="pos"></param>
    public delegate void RequestMessageUI(string txt, Vector3 pos);


    /// <summary>
    /// 
    /// </summary>
    public delegate void RequestHideMessageUI();

    /// <summary>
    /// 
    /// </summary>
    public event RequestMessageUI OnRequestMessageUI;

    /// <summary>
    /// 
    /// </summary>
    public event RequestHideMessageUI OnRequestHideMessageUI;
   
    #endregion

    /// <summary>
    /// Public getter for _pickUpItem.
    /// </summary>
    public PickUp PickUpItem { get { return _pickUpItem; } }

    public static int GunChipToReplace = 1;
    /// <summary>
    /// The point pick up weapons are attached.
    /// </summary>
    private Vector3 _attachPoint = new Vector3(-1f, -0.361f, 0);

    /// <summary>
    /// 
    /// </summary>
    private PickUp _pickUpItem;


    // Use this for initialization
    private void Start()
    {
        
	}
	
	// Update is called once per frame
	private void LateUpdate ()
    {
        HandlePickUps();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    /// <param name="ammoAmount"></param>
    public void PickUpGunChip(GunChipPickUp gunChip, int ammoAmount)
    {
        if (OnRequestHideMessageUI != null)
            OnRequestHideMessageUI();

        //GlobalData.Player.Weapon.TheGun.AddChipToGun(gunChip.Gunchip);
        //Level.CurentLevel.LevelState.SetState(gunChip.ID);
    }

    /// <summary>
    /// Handles the pick up of a shield. Return the one that was drooped
    /// </summary>
    /// <param name="shieldPickUp">The shield to pick up.</param>
    public Shield PickUpShield(ShieldPickUp shieldPickUp)
    {
        
        //Level.CurentLevel.LevelState.SetState(shieldPickUp.ID,true);
        return GlobalData.Player.Shield.AttachShield(shieldPickUp.Shield);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    public void EnterPickUp(PickUp item)
    {
        _pickUpItem = item;

        
        if (item.Type == PickUp.PickUpType.GUNCHIP)
        {
           /* GunChipPickUpUIManager.Instance.transform.position = Camera.main.WorldToScreenPoint(item.transform.position + (Vector3.up*3.2f) ) ;
            GunChipPickUpUIManager.Instance.Show(((GunChipPickUp)item).Gunchip);*/
        }
        

        ///SHIELD FIX NEEDED
        if (item.Type == PickUp.PickUpType.SHIELD)
        {
            ShieldPickUpUI.Instance.transform.position = Camera.main.WorldToScreenPoint(item.transform.position + (Vector3.up * 3.2f));
            ShieldPickUpUI.Instance.Show(((ShieldPickUp)item).Shield);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void ExitPickUp()
    {
        _pickUpItem = null;
        //Hide All to save a flag
        
        if (GunChipPickUpUI.Instance)
        {
            GunChipPickUpUIManager.Instance?.Hide();
        }
        
        if (ShieldPickUpUI.Instance)
        {
            ShieldPickUpUI.Instance?.Hide();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg"></param>
    /// <param name="time"></param>
    /// <param name="pos"></param>
    public void ReqestUiMsg(string msg, float time, Vector3 pos)
    {
        OnRequestMessageUI(msg, pos);
    }

    /// <summary>
    /// 
    /// </summary>
    public void ReqestUiMsgHide()
    {
        OnRequestHideMessageUI();
    }


    /// <summary>
    /// 
    /// </summary>
    private void HandlePickUps()
    {
        // No item to pick up so do nothing
        if (_pickUpItem == null)
            return;
        if (Input.GetButtonDown("SwitchChipPickup"))
        {
            GunChipToReplace =   (GunChipToReplace == 1)? 2 : 1;
        }
        if (Input.GetButtonDown("Action"))
        {
            _pickUpItem.PickUpItem(GlobalData.Player);
        }
    }
}
