﻿using GameMaster;
using LevelSystem;
using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Characters.Player
{
    public class PlayerShield
    {
        /// <summary>
        /// The current Shield May be null
        /// </summary>
        public Shield Current { get; protected set; }
        /// <summary>
        /// Where to attach the Shield 
        /// </summary>
        public GameObject AttchPoint;
        public Vector3 ShieldScale = new Vector3(2, 2.5f, 2);
        public const string SHIELDPOWERAMOUNT = "Shield-Power-Amount";
        public const string SHIELDMAXPOWERAMOUNT = "Shield-Max-Power-Amount";
        public const string SHIELDRARITYAMOUNT = "Shield-Rarity-Amount";

        /// <summary>
        /// Attaches a shield to a player and return the ref to the old one
        /// </summary>
        /// <param name="newShield">The New Shield to attach</param>
        /// <returns>the last shield or null</returns>
        public Shield AttachShield(Shield newShield)
        {
            Shield old = Current;

            newShield.transform.parent = AttchPoint.transform;
            newShield.transform.position = GlobalData.Player.Animation.RootBone.GetWorldPosition(GlobalData.Player.transform);
            //newShield.transform.localScale = ShieldScale;
            newShield.ShieldHolder = GlobalData.Player;
            Current = newShield;
            return old;
        }
        /// <summary>
        /// Save the player current shield 
        /// </summary>
        /// <param name="dataFileName"></param>
        public void Save(string dataFileName)
        {
        
            if(Current ==  null)
            {
                ES3.Save<float>(SHIELDPOWERAMOUNT, -1f, dataFileName);
                return;
            }


            ES3.Save<int>(SHIELDRARITYAMOUNT, (int)Current.Rarity, dataFileName);
            ES3.Save<float>(SHIELDPOWERAMOUNT, Current.Power, dataFileName);
            ES3.Save<float>(SHIELDMAXPOWERAMOUNT, Current.MaxPower, dataFileName);

        }

        public void Load(string dataFileName)
        {
            float power = ES3.Load<float>(SHIELDPOWERAMOUNT, dataFileName, -1);// -1 is a lack of shield to load
            if (power != -1)
            {
                Rarity rarity = (Rarity)ES3.Load<int>(SHIELDRARITYAMOUNT, dataFileName, 0);
                float maxPower = ES3.Load<float>(SHIELDMAXPOWERAMOUNT, dataFileName, -1);// -1 is a lack of shield to load


                 GameManager.Instance.SetMasterAsActive();
                Shield theShield = GameObject.Instantiate<Shield>(ShieldAssetManager.GetTemplateFromRarity(rarity), GlobalData.VEC3_OFFSCREEN, Quaternion.identity, null);
                theShield.transform.localScale = ShieldScale;
                theShield.Load(power, maxPower, rarity);
                AttachShield(theShield);
            }

        }
    }
}

