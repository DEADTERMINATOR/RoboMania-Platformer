﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;
using static GlobalData;
using Weapons.Player;
using System;

namespace Characters.Player
{
    public class PlayerMaster : Character, IDamageGiver, IDamageReceiver, IStunnable, IGravityObject
    {
        #region Player Gravity State Class
        sealed public class PlayerGravityState : GravityState
        {
            public float JumpVelocity { get { return _jumpVelocity; } }
            public float MaximumDownwardYVelocity { get { return _maximumDownwardYVelocity; } }
            public float BaseMoveSpeed { get { return _baseMoveSpeed; } }
            public bool DashingAllowed { get { return _dashingAllowed; } }
            public bool WallSlidingAllowed { get { return _wallSlidingAllowed; } }

            private float _jumpVelocity;

            private float _maximumDownwardYVelocity;
            private float _baseMoveSpeed;

            private bool _dashingAllowed;
            private bool _wallSlidingAllowed;


            public PlayerGravityState(GravityType type, PlayerMaster playerRef = null)
            {
                _gravityType = type;
                switch (type)
                {
                    case GravityType.Base:
                        _gravityVelocity = _baseGravityVelocity = new Vector2(0, -0.46f); //new Vector2(0, -55f);
                        _jumpVelocity = 23f;
                        _maximumDownwardYVelocity = -98f; //-98f
                        _gravityReductionValue = 0f;
                        _baseMoveSpeed = playerRef == null ? GlobalData.Player.Data.BaseMoveSpeed : playerRef.Data.BaseMoveSpeed;
                        _dashingAllowed = true;
                        _wallSlidingAllowed = true;
                        _baseGravityScale = 1;
                        break;
                    case GravityType.Lava:
                        _gravityVelocity = _baseGravityVelocity = new Vector2(0, -7.5f);
                        _jumpVelocity = 5.0f;
                        _maximumDownwardYVelocity = -15f;
                        _gravityReductionValue = -2f;
                        _baseMoveSpeed = 3.5f;
                        _dashingAllowed = true;
                        _wallSlidingAllowed = false;
                        _baseGravityScale = 1;
                        break;
                    case GravityType.Mud:
                        _gravityVelocity = _baseGravityVelocity = new Vector2(0, -1.25f);
                        _jumpVelocity = 2.25f;
                        _maximumDownwardYVelocity = -2.5f;
                        _gravityReductionValue = 0f;
                        _baseMoveSpeed = 1.5f;
                        _dashingAllowed = false;
                        _wallSlidingAllowed = false;
                        _baseGravityScale = 1;
                        break;
                    default:
                        _gravityVelocity = Vector2.zero;
                        _jumpVelocity = 0;
                        _maximumDownwardYVelocity = 0;
                        _gravityReductionValue = 0f;
                        _baseMoveSpeed = 0;
                        _dashingAllowed = true;
                        _wallSlidingAllowed = true;
                        _baseGravityScale = 1;
                        break;
                }
            }
        }
        #endregion

        //TODO: Add OnTransitionOrReload event
        #region State Change Delegates
        public event StateChange Respawned;
        public event StateChange Invulnerable;
        public event StateChange Stunned;
        public event StateChange WallSliding;
        public event StateChange WallGripping;
        public event StateChange Hovering;
        public event StateChange Climbing;
        public event StateChange OnRepair;
        public event StateChange OnRepairFailed;
        public event StateChange OnShieldRemoved;
        #endregion

        #region Collision Delegates
        public delegate void MovementCollisionEvent(Vector2 velocity);
        public event MovementCollisionEvent CollisionBelow;
        public event MovementCollisionEvent NoCollision;
        public event MovementCollisionEvent KnockbackReceived;
        #endregion

        #region Gravity Change Delegates
        public delegate void GravityChangeEvent(GlobalData.GravityType type);
        public event GravityChangeEvent GravityChange;
        #endregion

        #region Action Delegates
        public delegate void ActionEvent();
        public event ActionEvent EnergyCellMoveUsed;
        public event ActionEvent EnteringShip;
        public event ActionEvent DashPerformed;
        #endregion

        #region Sprite Delegates
        public delegate void SpriteDirectionChangeEvent(int newFaceDir);
        public event SpriteDirectionChangeEvent FacingDirectionChanged;
        #endregion

        #region Constants
        public const float PLAYER_HORIZONTAL_EXTENTS = 0.4f;
        public const float PLAYER_VERTICAL_EXTENTS = 0.825f;

        public const int AIR_GUST_VELOCITY_COMPONENT = 2;
        #endregion

        #region Player Components
        public PlayerState State;
        public PlayerInput Input;
        public PlayerMovementController Movement;
        public PlayerAnimationController Animation;
        public PlayerWeaponHandler Weapon;
        public PlayerShield Shield;
        public PlayerData Data;
        public PlayerRepair Repair;
        public PlayerSound Sound;
        [HideInInspector]
        public PlayerEffectHandler Effects;
        #endregion

        #region Public Fields + Accessors
        /// <summary>
        /// Should the player attempt to load its data from a save
        /// </summary>
        public bool LoadOnStart = false;

        /// <summary>
        /// The string for the name of the file that the player should save to and load from.
        /// </summary>
        public string SaveAndLoadFileNameString;

        /// <summary>
        /// The number of energy cells the player should start with.
        /// </summary>
        public int StartingEnergyCells = 1;

        /// <summary>
        /// Whether the player's lights should be turned on.
        /// </summary>
        public bool LightsOn = false;

        //public override Vector3 Velocity { get { return Movement.Velocity; } }
        public override Vector2 MovementInput { get { return Input.MovementInput; } }
        public override Controller2D Controller { get { return controller; } }
        public override Controller2D.CollisionInfo Collisions { get { return controller.Collisions; } }
        public override BoxCollider2D MovementCollider { get { return _movementCollider; } }
        public override GravityState CurrentActiveGravityState { get { return Movement.CurrentActiveGravityState; } }

        public bool IsStunned { get { return State.Stunned; } }

        public GameObject Sprite { get { return _sprite; } }

        public GameObject BlobShadow { get; private set; }

        public Dictionary<GravityType, PlayerGravityState> GravityStates { get { return _gravityStates; } }

        public PlayerWeapon PlayerWeapon { get { return Weapon.PlayerWeapon; } }

        public Rigidbody2D Rigidbody2D { get { return _rigidbody2D; } }

        /// <summary>
        /// Gets the transform position for the top of the Player character.
        /// </summary>
        public Vector3 TopOfPlayerTransform { get { return new Vector3(transform.position.x, transform.position.y + PLAYER_VERTICAL_EXTENTS * 2, transform.position.z); } }
        #endregion

        #region DebugVars 
        public bool StartWithDash = false;
        public bool StartWithWallClimb = false;
        #endregion

        #region Private Variables
        private PlayerController2D controller;
        private BoxCollider2D _movementCollider;

        private GameObject _sprite;

        private Guid _lockID;
        private Rigidbody2D _rigidbody2D;

        /// <summary>
        /// Holds all the possible gravity states, and their associated values, that the player could encounter.
        /// </summary>
        private readonly Dictionary<GravityType, PlayerMaster.PlayerGravityState> _gravityStates = new Dictionary<GravityType, PlayerGravityState>();
        #endregion

        #region Unity Methods
        private void Awake()
        {
            _lockID = GameMaster.LoadingLocks.Lock();
            
            _gravityStates.Add(GravityType.Base, new PlayerGravityState(GravityType.Base, this));
            _gravityStates.Add(GravityType.Lava, new PlayerGravityState(GravityType.Lava));
            _gravityStates.Add(GravityType.Mud, new PlayerGravityState(GravityType.Mud));

            AddVelocityComponent(AIR_GUST_VELOCITY_COMPONENT, Vector2.zero, false);

            _movementCollider = GetComponent<BoxCollider2D>();
            controller = GetComponent<PlayerController2D>();
            State = new PlayerState(this);
            Weapon = new PlayerWeaponHandler(this);
            Shield = new PlayerShield();

            if (GlobalData.IsANewGame || !LoadOnStart)
            {
                Data = new PlayerData(100, 100, 0, StartingEnergyCells, this, SaveAndLoadFileNameString);
                GlobalData.IsANewGame = false;
            }
            else
            {
                Load();
            }

            Input = new PlayerInput(this);
            Movement = new PlayerMovementController(this);
            Animation = new PlayerAnimationController(MovementCollider, this);
            Repair = new PlayerRepair();
            Sound = new PlayerSound(this);

            Effects = GetComponent<PlayerEffectHandler>();

            _sprite = transform.Find("Character").gameObject;
            BlobShadow = transform.Find("ShadowBlobParent/ShadowQuad").gameObject;
            State.SetLightsOn(LightsOn);

            _rigidbody2D = GetComponent<Rigidbody2D>();
            _rigidbody2D.isKinematic = true;///start dynamic but gain Kinematic control
        }

        private void Start()
        {
            if (LoadOnStart)
            {   
                Load();
            }

            Weapon.LateInit();

            controller.Collisions.FaceDir = 1;
            Effects.Init(this);

            GameMaster.LoadingLocks.Unlock(_lockID);
        }

        private void Update()
        {
            if (!GameMaster.LoadingLocks.IsLocked())
            {
                if (!State.Dead || !State.Stunned)
                {
                    Input.ReceivePlayerInput(Time.deltaTime);
                }
                State.StateUpdate();
                Movement.MovementUpdate();
                Animation.AnimationUpdate();
               // Shield.PlayerShieldUpdate();

                //Zero out the player's rigidbody velocity in case any forces have been applied to the body as a result of being a dynamic body in the physics simulation.
                _rigidbody2D.velocity = Vector2.zero;
                //Zero out the rotation for the same reason as above.
                transform.rotation = Quaternion.identity;
            }

            if (UnityEngine.Input.GetKeyDown(KeyCode.M))
            {
                Save();
            }
        }

        private void FixedUpdate()
        {
            if (!GameMaster.LoadingLocks.IsLocked())
            {
                Movement.FixedMovementUpdate();
            }
        }

        private void LateUpdate()
        {
            if (!GameMaster.LoadingLocks.IsLocked())
            {
                Animation.AnimationLateUpdate();
                Weapon.WeaponUpdate();
                Repair.RepairUpdate();
            }
        }

        private void OnDestroy()
        {
            Timing.KillCoroutines(Animation.DamageFlashHandle);
            Timing.KillCoroutines(Animation.InvulnerabilityFlashHandle);
        }
        #endregion

        #region Interface Methods
        public override void TakeDamage(IDamageGiver giver, float amount, GlobalData.DamageType type, Vector3 hitPoint)
        {
            /*if (Shield.Current != null)
            {
                Shield.Current.TakeDamage(giver, amount, type, hitPoint);
            }
            else
            {
                State.TakeDamage(giver, amount, type);
            }*/

            State.TakeDamage(giver, amount, type);
        }

        public void SetVelocityOnOneAxis(char axis, float axisVelocity)
        {
            Movement.SetVelocity(axis, axisVelocity);
        }

        public void ChangeGravityType(GravityType type)
        {
            Movement.ChangeGravityType(type);
        }

        public void SetStunned(float stunPower, IDamageGiver giver, GameObject owner)
        {
            State.SetStunned(stunPower, giver, owner);
        }
        #endregion

        public void SetLights(bool lightsStatus)
        {
            LightsOn = lightsStatus;
        }


        #region Saving + Loading
        /// <summary>
        /// Player Data To be Saved at checkpoints and death
        /// </summary>
        public void Save()
        {
            Data.Save();
            Weapon.PlayerWeapon.SaveWeaponData(SaveAndLoadFileNameString);
            Shield.Save(SaveAndLoadFileNameString);
        }
        /// <summary>
        /// Player Data To be Saved at checkpoints and death
        /// </summary>
        public void Load()
        {
            Data = new PlayerData(this, SaveAndLoadFileNameString);
            Shield.Load(SaveAndLoadFileNameString);
        }
        #endregion

        #region Delegate + Other Notifiers
        public void NotifyRespawned()
        {
            Respawned?.Invoke(true);
        }

        public void NotifyInvulnerable(bool invulnerable)
        {
            Invulnerable?.Invoke(invulnerable);
        }

        public void NotifyStunned(bool stunned)
        {
            Stunned?.Invoke(stunned);
        }

        public void NotifyWallSliding(bool wallSliding)
        {
            WallSliding?.Invoke(wallSliding);
        }

        public void NotifyWallGripping(bool wallGripping)
        {
            WallGripping?.Invoke(wallGripping);
        }

        public void NotifyShieldRemoved(bool wallSliding)
        {
            OnShieldRemoved?.Invoke(wallSliding);
        }

        public void NotifyHovering(bool hovering)
        {
            Hovering?.Invoke(hovering);
        }

        public void NotifyClimbing(bool climbing)
        {
            Climbing?.Invoke(climbing);
        }

        public void NotifyRepair(bool positive)
        {
            OnRepair?.Invoke(positive);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="positive">True = not enough scrap False = blocked by the Game  </param>
        public void NotifyRepairFailed(bool positive)
        {
            OnRepairFailed?.Invoke(positive);
        }

        public void NotifyCollidedBelow(Vector2 velocity)
        {
            CollisionBelow?.Invoke(velocity);
        }

        public void NotifyNoCollision(Vector2 velocity)
        {
            NoCollision?.Invoke(velocity);
        }

        public void NotifyKnockbackReceived(Vector2 velocity)
        {
            KnockbackReceived?.Invoke(velocity);
        }

        public void NotifyGravityChange(GlobalData.GravityType type)
        {
            GravityChange?.Invoke(type);
        }

        public void NotifyEnergyCellMoveUsed()
        {
            EnergyCellMoveUsed?.Invoke();
        }

        public void NotifyEnteringShip()
        {
            EnteringShip?.Invoke();
        }

        public void NotifyDashPerformed()
        {
            DashPerformed?.Invoke();
        }

        public void NotifyFacingDirectionChanged(int newFaceDir)
        {
            FacingDirectionChanged?.Invoke(newFaceDir);
        }
        #endregion
    }
}
