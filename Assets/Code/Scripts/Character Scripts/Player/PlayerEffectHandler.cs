﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelSystem;
using Spine;
using Spine.Unity;
using System;

namespace Characters.Player
{
    public class PlayerEffectHandler : MonoBehaviour
    {
        private class InstantiatedEffect
        {
            public GameObject Instantiation;
            public int InstantiatedFacingDirection;
            public bool DirectionSwitchAllowed;
            public ParticleSystem Particles;
            public bool EndEffect;

            public InstantiatedEffect(GameObject instantiation, int instantiatedFacingDirection, bool directionSwitchAllowed, ParticleSystem particles = null) => (Instantiation, InstantiatedFacingDirection, DirectionSwitchAllowed, Particles, EndEffect) = (instantiation, instantiatedFacingDirection, directionSwitchAllowed, particles, false);
            public InstantiatedEffect() => (Instantiation, InstantiatedFacingDirection, DirectionSwitchAllowed, Particles, EndEffect) = (null, 0, false, null, true);
        }

        private readonly Vector3 DASH_RIGHT_PLAYER_OFFSET = new Vector3(5.5f, 0.5f);
        private readonly Vector3 DASH_LEFT_PLAYER_OFFSET = new Vector3(5.5f, -0.5f);

        private readonly Quaternion MOVING_RIGHT_EFFECT_ROTATION = Quaternion.Euler(0, -90, 0);
        private readonly Quaternion MOVING_LEFT_EFFECT_ROTATION = Quaternion.Euler(0, 90, 0);

        /// <summary>
        /// The base scale for the explosion effect.
        /// </summary>
        private const float BASE_EXPLOSION_SCALE = 5f;

        private const float DASH_DAMPEN_ON_STOP_VALUE = 0.9f;


        public GameObject MultiJumpEffect;
        public GameObject DashEffect;
        public GameObject ExplosionEffect;
        public GameObject WallSlideEffect;

        public Explodable Fragmentation { get; private set; }

        private InstantiatedEffect _instantiatedDashEffect = new InstantiatedEffect();

        private InstantiatedEffect _instantiatedHandWallSlideEffect = new InstantiatedEffect();
        private InstantiatedEffect _instantiatedFootWallSlideEffect = new InstantiatedEffect();

        private Bone _handWallSlideEffectBone;
        private Bone _footWallSlideEffectBone;

        private PlayerMaster _playerRef;
        private Guid _lockID;


        /// <summary>
        /// An initialization function to be run during the execution of the PlayerMaster's awake function.
        /// Essentially the "constructor" for the effects controller.
        /// </summary>
        /// <param name="playerRef">Reference to the player master.</param>
        internal void Init(PlayerMaster playerRef)
        {
            _lockID = GameMaster.LoadingLocks.Lock();

            Fragmentation = playerRef.transform.Find("Destruction").GetComponent<Explodable>();
            Vector2 destructionOffset = Fragmentation.transform.localPosition;
            Fragmentation.transform.localPosition = Vector3.zero;

            //NOTE: This generation calls the non-coroutine version of the function, as this constructor is called from Awake and I have learned that Awake cannot function as a coroutine.
            //The unlock for the lock above occurs at the end of this method.
            Fragmentation.generateFragments(false, false, false, false, null, _lockID);

            Fragmentation.transform.localPosition = destructionOffset;

            _handWallSlideEffectBone = playerRef.Animation.SkeletonAnimation.Skeleton.FindBone("Left Hand Wall Slide Effect");
            _footWallSlideEffectBone = playerRef.Animation.SkeletonAnimation.Skeleton.FindBone("Right Foot Wall Slide Effect");

            _playerRef = playerRef;
        }

        private void Start()
        {
            GlobalData.Player.DashPerformed += new PlayerMaster.ActionEvent(PlayDashEffect);
        }

        private void Update()
        {
            if (_instantiatedHandWallSlideEffect.Instantiation != null)
            {
                if (!_instantiatedHandWallSlideEffect.EndEffect)
                {
                    SetWallSlideEffectsPosition();
                }
                else if (_instantiatedHandWallSlideEffect.EndEffect && !_instantiatedFootWallSlideEffect.Particles.isPlaying)
                {
                    DestroyWallSlideEffects();
                }
            }
        }

        public void PlayMultiJumpEffect()
        {
            Instantiate(MultiJumpEffect, transform.position, Quaternion.Euler(-90, 0, 0));
        }

        public void PlayDashEffect()
        {
            int playerFaceDir = GlobalData.Player.Controller.Collisions.FaceDir;
            var instantiatedDash = Instantiate(DashEffect, playerFaceDir == 1 ? transform.position + DASH_RIGHT_PLAYER_OFFSET : transform.position - DASH_LEFT_PLAYER_OFFSET, playerFaceDir == 1 ? MOVING_RIGHT_EFFECT_ROTATION : MOVING_LEFT_EFFECT_ROTATION);
            var dashEffectParticles = instantiatedDash.GetComponent<ParticleSystem>();

            _instantiatedDashEffect = new InstantiatedEffect(instantiatedDash, playerFaceDir, false, dashEffectParticles);
        }

        public void StopDashEffect()
        {
            //This could be called after the dash effect has already been destroyed, so we need to ensure it still exists.
            if (_instantiatedDashEffect.Instantiation != null)
            {
                //The glow should be the first sub-child of the dash effect. Since the first element will be the parent's particle system, get the second element.
                var dashGlowEffect = _instantiatedDashEffect.Instantiation.GetComponentsInChildren<ParticleSystem>()[1];
                var mainEffectLimitVelocityModule = _instantiatedDashEffect.Particles.limitVelocityOverLifetime;
                var glowLimitVelocityModule = dashGlowEffect.limitVelocityOverLifetime;

                mainEffectLimitVelocityModule.dampen = DASH_DAMPEN_ON_STOP_VALUE;
                glowLimitVelocityModule.dampen = DASH_DAMPEN_ON_STOP_VALUE;
            }

            //If the dash effect hasn't been replaced with a default instantiation yet, then EndEffect will be false.
            if (!_instantiatedDashEffect.EndEffect)
            {
                _instantiatedDashEffect = new InstantiatedEffect();
            }
        }

        public void PlayDestructionEffect()
        {
            GameObject instantiatedExplosionEffect = Instantiate(ExplosionEffect, new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z), Quaternion.identity);
            instantiatedExplosionEffect.transform.localScale = new Vector3(BASE_EXPLOSION_SCALE, BASE_EXPLOSION_SCALE, BASE_EXPLOSION_SCALE);
            Fragmentation.explode(0);
            GlobalData.Timekeeper.StartTimer(() => { Level.CurentLevel.ReloadLevel(); }, 2.0f, "Player Went Boom", true);

            _playerRef.Sprite.SetActive(false);
            _playerRef.BlobShadow.SetActive(false);
            _playerRef.MovementCollider.enabled = false;

            _playerRef.Movement.Stop();
        }

        public void PlayWallSlideEffects()
        {
            _handWallSlideEffectBone.UpdateWorldTransform();
            _footWallSlideEffectBone.UpdateWorldTransform();

            var playerTransform = _playerRef.transform;
            var playerFaceDir = _playerRef.Collisions.FaceDir;

            var instantiatedHandWallSlide = Instantiate(WallSlideEffect, _handWallSlideEffectBone.GetWorldPosition(playerTransform), Quaternion.identity);
            var instantiatedFootWallSlide = Instantiate(WallSlideEffect, _footWallSlideEffectBone.GetWorldPosition(playerTransform), Quaternion.identity);

            var handParticles = instantiatedHandWallSlide.GetComponent<ParticleSystem>();
            var footParticles = instantiatedFootWallSlide.GetComponent<ParticleSystem>();

            _instantiatedHandWallSlideEffect = new InstantiatedEffect(instantiatedHandWallSlide, playerFaceDir, false, handParticles);
            _instantiatedFootWallSlideEffect = new InstantiatedEffect(instantiatedFootWallSlide, playerFaceDir, false, footParticles);
        }

        public void StopWallSlideEffects(bool destroyImmediately)
        {
            if (_instantiatedHandWallSlideEffect.Particles != null)
            {
                if (destroyImmediately)
                {
                    DestroyWallSlideEffects();
                }
                else
                {
                    var handParticlesMain = _instantiatedHandWallSlideEffect.Particles.main;
                    var footParticlesMain = _instantiatedFootWallSlideEffect.Particles.main;

                    handParticlesMain.loop = false;
                    footParticlesMain.loop = false;

                    _instantiatedHandWallSlideEffect.EndEffect = true;
                    _instantiatedFootWallSlideEffect.EndEffect = true;
                }
            }
        }

        private void SetWallSlideEffectsPosition()
        {
            _handWallSlideEffectBone.UpdateWorldTransform();
            _footWallSlideEffectBone.UpdateWorldTransform();

            _instantiatedHandWallSlideEffect.Instantiation.transform.position = _handWallSlideEffectBone.GetWorldPosition(_playerRef.transform);
            _instantiatedFootWallSlideEffect.Instantiation.transform.position = _footWallSlideEffectBone.GetWorldPosition(_playerRef.transform);
        }

        private void DestroyWallSlideEffects()
        {
            Destroy(_instantiatedHandWallSlideEffect.Instantiation);
            Destroy(_instantiatedFootWallSlideEffect.Instantiation);

            _instantiatedHandWallSlideEffect = new InstantiatedEffect();
            _instantiatedFootWallSlideEffect = new InstantiatedEffect();
        }
    }
}
