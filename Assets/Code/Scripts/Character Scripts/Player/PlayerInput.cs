﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Inputs;
using static GlobalData;
using UnityEngine.UIElements;
using Weapons.Player;
using static Weapons.Player.WeaponFormProperties;
using System;
using Weapons.Player.Forms;

namespace Characters.Player
{
    public class PlayerInput
    {
        public delegate void UpdateEvent();
        public event UpdateEvent OnUpdate;

        #region Button State
        public class ButtonState
        {
            public enum State { UP, DOWN, HELD, RELEASED }

            public bool IsDown { get { return TheState == State.DOWN; } }
            public bool IsDownOrHeld { get { return TheState == State.DOWN || TheState == State.HELD; } }
            public bool IsReleased { get { return TheState == State.RELEASED; } }
            public bool IsUpOrReleased { get { return TheState == State.UP || TheState == State.RELEASED; } }

            private State TheState;


            public ButtonState(State buttonState, PlayerInput input)
            {
                TheState = buttonState;
                input.OnUpdate += ConvertDownToHeld;
            }


            /// <summary>
            /// Gets the current state of the button.
            /// </summary>
            /// <returns></returns>
            public State GetTheState()
            {
                return TheState;
            }

            /// <summary>
            /// Sets the current state of the button. Whether the button goes into a held or
            /// released state is handled internally by comparing the current state to the previous state
            /// - only whether the button is currently pressed is needed.
            /// </summary>
            /// <param name="buttonIsPressed">Whether the button is currently pressed.</param>
            public void SetTheState(bool buttonIsPressed)
            {
                if (buttonIsPressed)
                {
                    if (TheState == State.DOWN || TheState == State.HELD)
                    {
                        TheState = State.HELD;
                    }
                    else
                    {
                        TheState = State.DOWN;
                    }
                }
                else
                {
                    if (TheState == State.DOWN || TheState == State.HELD)
                    {
                        TheState = State.RELEASED;
                    }
                    else
                    {
                        TheState = State.UP;
                    }
                }
            }

            public void ClearState()
            {
                TheState = State.UP;
            }

            private void ConvertDownToHeld()
            {
                if (TheState == State.DOWN)
                {
                    TheState = State.HELD;
                }
                else if (TheState == State.RELEASED)
                {
                    TheState = State.UP;
                }
            }

            public override string ToString()
            {
                switch (TheState)
                {
                    case State.UP:
                        return "Up";
                    case State.DOWN:
                        return "Down";
                    case State.HELD:
                        return "Held";
                    case State.RELEASED:
                        return "Released";
                    default:
                        return "None";
                }
            }
        }
        #endregion

        #region Constants
        private const float INPUT_RELEASED_GRACE_PERIOD = 0.1f;
        private const float TIME_BETWEEN_WEAPON_SWAPS = 0.15f;
        #endregion

        #region Delegates
        public delegate void OnWeaponSwitch(WeaponForms form);
        public delegate void InputStatus();
        
        public event OnWeaponSwitch Pistol;
        public event OnWeaponSwitch MachineGun;
        public event OnWeaponSwitch Shotgun;
        public event OnWeaponSwitch RocketLauncher;
        public event OnWeaponSwitch ChargeRifle;

        public event InputStatus FireInputPressed;
        public event InputStatus FireInputReleased;
        #endregion

        #region Public Properties
        /// <summary>
        /// The raw X and Y-axis movement received from the player.
        /// </summary>
        public Vector2 MovementInput = Vector2.zero;

        /// <summary>
        /// A dictionary containing all button inputs that tracks which ones have been pressed/held this frame.
        /// </summary>
        public Dictionary<string, ButtonState> InputDictionary = new Dictionary<string, ButtonState>();

        /// <summary>
        /// Whether all input should be ignored.
        /// </summary>
        public bool BlockInput { get { return _blockInput; } set { _blockInput = value; if (value) ClearInputDictionary(); } }

        /// <summary>
        /// A probably temporary variable to get around the issue of repeated inputs as a result of BlockInput until a permanent solution can be implemented.
        /// </summary>
        public bool BlockInputWithoutClearingState = false;

        /// <summary>
        /// Whether only special movement input (i.e. jumping, dashing, and hovering) should be blocked.
        /// </summary>
        public bool BlockSpecialInput = false;

        /// <summary>
        /// Public getter for whether weapon fire input should be ignored.
        /// </summary>
        public bool BlockWeaponFire { get { return _blockWeaponFire; } }

        public Vector2 PrecisionAimInput { get; private set; } = Vector2.zero;

        /// <summary>
        /// Whether the movement input was very recently released (within INPUT_RELEASED_GRACE_PERIOD).
        /// </summary>
        public bool JustReleasedMovementInput { get; private set; } = false;
        #endregion

        #region Private Variables
        private bool _blockInput = false;

        private bool _blockWeaponFire = false;
        private int _currentPlayerWeaponForm = 1;

        private float _remainingTimeBetweenWeaponSwitches = 0;

        private InputManager _inputManagerInstanceRef;
        private GameInputs.InGameActions _actions;
        private Dictionary<WeaponForms, ButtonState> _weaponDictionary = new Dictionary<WeaponForms, ButtonState>();
        private PlayerMaster _playerRef;
        #endregion

        #region Constructor
        public PlayerInput(PlayerMaster playerRef)
        {
            _inputManagerInstanceRef = InputManager.Instance;
            _actions = _inputManagerInstanceRef.InGameActions;
            _actions.Enable();

            InputDictionary.Add("Jump", new ButtonState(ButtonState.State.UP, this));
            InputDictionary.Add("Dash", new ButtonState(ButtonState.State.UP, this));
            InputDictionary.Add("Hover", new ButtonState(ButtonState.State.UP, this));
            InputDictionary.Add("Raise", new ButtonState(ButtonState.State.UP, this));
            InputDictionary.Add("Wall Grip", new ButtonState(ButtonState.State.UP, this));
            InputDictionary.Add("Fire", new ButtonState(ButtonState.State.UP, this));
            InputDictionary.Add("Precision Aim", new ButtonState(ButtonState.State.UP, this));
            InputDictionary.Add("Repair", new ButtonState(ButtonState.State.UP, this));
            InputDictionary.Add("Action", new ButtonState(ButtonState.State.UP, this));
            InputDictionary.Add("Next Form", new ButtonState(ButtonState.State.UP, this));
            InputDictionary.Add("Previous Form", new ButtonState(ButtonState.State.UP, this));
           
            
            _weaponDictionary.Add(WeaponForms.Pistol, new ButtonState(ButtonState.State.UP, this));
            _weaponDictionary.Add(WeaponForms.MachineGun, new ButtonState(ButtonState.State.UP, this));
            _weaponDictionary.Add(WeaponForms.Shotgun, new ButtonState(ButtonState.State.UP, this));
            _weaponDictionary.Add(WeaponForms.RocketLauncher, new ButtonState(ButtonState.State.UP, this));
            _weaponDictionary.Add(WeaponForms.ChargeRifle, new ButtonState(ButtonState.State.UP, this));

            playerRef.Stunned += OnStunned;
            playerRef.Dead += OnDead;
            playerRef.Respawned += OnRespawn;
            playerRef.OnRepair += OnRepair;
            playerRef.EnteringShip += OnEnteringShip;

            if (TheGame.GetRef.gameState == TheGame.GameState.RESPAWNING)
            {
                BlockInput = true;
            }
            else
            {
                BlockInput = false;
            }

            _playerRef = playerRef;
        }
        #endregion

        #region Update
        /// <summary>
        /// Checks the various inputs the player can make (moving, jumping, wall sliding, dashing) and handles what should happen
        /// if any of the inputs are made.
        /// </summary>
        public void ReceivePlayerInput(float updateTime)
        {
            OnUpdate?.Invoke();
            if (!BlockInput && !BlockInputWithoutClearingState)
            {
                Vector2 previousMovementInput = MovementInput;
                MovementInput = Vector2.zero;

                var readMovementInput = _actions.Move.ReadValue<Vector2>();
                if (readMovementInput.magnitude > CONTROLLER_STICK_DEADZONE)
                {
                    MovementInput = readMovementInput;
                }

                if (previousMovementInput.x != 0 && MovementInput.x == 0)
                {
                    JustReleasedMovementInput = true;
                    GlobalData.Timekeeper.StartTimer(new System.Action(() => { JustReleasedMovementInput = false; }), INPUT_RELEASED_GRACE_PERIOD, "JustReleasedInput", true);
                }

                if (_inputManagerInstanceRef.CurrentDeviceType == InputManager.DeviceType.KeyboardMouse && _actions.PrecisionAimTrigger.ReadValue<float>() != 0)
                {
                    InputDictionary["Precision Aim"].SetTheState(true);
                    PrecisionAimInput = _actions.PrecisionAimMouse.ReadValue<Vector2>().normalized;
                }
                else if (_inputManagerInstanceRef.CurrentDeviceType == InputManager.DeviceType.Controller && (PrecisionAimInput = _actions.PrecisionAimGamepad.ReadValue<Vector2>()).magnitude > CONTROLLER_STICK_DEADZONE)
                {
                    InputDictionary["Precision Aim"].SetTheState(true);
                }
                else
                {
                    InputDictionary["Precision Aim"].SetTheState(false);
                    PrecisionAimInput = Vector2.zero;
                }

                if (_inputManagerInstanceRef.CurrentDeviceType == InputManager.DeviceType.KeyboardMouse)
                {
                    if (_actions.PistolKeyboard.ReadValue<float>() != 0 && _currentPlayerWeaponForm != (int)WeaponForms.Pistol)
                    {
                        _weaponDictionary[WeaponForms.Pistol].SetTheState(true);
                        if (_weaponDictionary[WeaponForms.Pistol].IsDown)
                        {
                            Pistol?.Invoke(WeaponForms.Pistol);
                        }

                        _currentPlayerWeaponForm = (int)WeaponForms.Pistol;

                        _blockWeaponFire = true;
                        FireInputReleased?.Invoke();
                    }
                    else
                    {
                        _weaponDictionary[WeaponForms.Pistol].SetTheState(false);
                    }

                    if (_actions.MachineGunKeyboard.ReadValue<float>() != 0 && _currentPlayerWeaponForm != (int)WeaponForms.MachineGun)
                    {
                        _weaponDictionary[WeaponForms.MachineGun].SetTheState(true);
                        if (_weaponDictionary[WeaponForms.MachineGun].IsDown)
                        {
                            MachineGun?.Invoke(WeaponForms.MachineGun);
                        }

                        _currentPlayerWeaponForm = (int)WeaponForms.MachineGun;

                        _blockWeaponFire = true;
                        FireInputReleased?.Invoke();
                    }
                    else
                    {
                        _weaponDictionary[WeaponForms.MachineGun].SetTheState(false);
                    }

                    if (_actions.ShotgunKeyboard.ReadValue<float>() != 0 && _currentPlayerWeaponForm != (int)WeaponForms.Shotgun)
                    {
                        _weaponDictionary[WeaponForms.Shotgun].SetTheState(true);
                        if (_weaponDictionary[WeaponForms.Shotgun].IsDown)
                        {
                            Shotgun?.Invoke(WeaponForms.Shotgun);
                        }

                        _currentPlayerWeaponForm = (int)WeaponForms.Shotgun;

                        _blockWeaponFire = true;
                        FireInputReleased?.Invoke();
                    }
                    else
                    {
                        _weaponDictionary[WeaponForms.Shotgun].SetTheState(false);
                    }

                    if (_actions.RocketLauncherKeyboard.ReadValue<float>() != 0 && _currentPlayerWeaponForm != (int)WeaponForms.RocketLauncher)
                    {
                        _weaponDictionary[WeaponForms.RocketLauncher].SetTheState(true);
                        if (_weaponDictionary[WeaponForms.RocketLauncher].IsDown)
                        {
                            RocketLauncher?.Invoke(WeaponForms.RocketLauncher);
                        }

                        _currentPlayerWeaponForm = (int)WeaponForms.RocketLauncher;

                        _blockWeaponFire = true;
                        FireInputReleased?.Invoke();
                    }
                    else
                    {
                        _weaponDictionary[WeaponForms.RocketLauncher].SetTheState(false);
                    }

                    if (_actions.ChargeRifleKeyboard.ReadValue<float>() != 0 && _currentPlayerWeaponForm != (int)WeaponForms.ChargeRifle)
                    {
                        _weaponDictionary[WeaponForms.ChargeRifle].SetTheState(true);
                        if (_weaponDictionary[WeaponForms.ChargeRifle].IsDown)
                        {
                            ChargeRifle?.Invoke(WeaponForms.ChargeRifle);
                        }

                        _currentPlayerWeaponForm = (int)WeaponForms.ChargeRifle;

                        _blockWeaponFire = true;
                        FireInputReleased?.Invoke();
                    }
                    else
                    {
                        _weaponDictionary[WeaponForms.ChargeRifle].SetTheState(false);
                    }

                    _remainingTimeBetweenWeaponSwitches -= updateTime;
                    var scrollInput = _actions.CycleWeaponFormKeyboard.ReadValue<Vector2>();

                    if (scrollInput.y != 0 && _remainingTimeBetweenWeaponSwitches <= 0)
                    {
                        _currentPlayerWeaponForm = (int)_playerRef.PlayerWeapon.CycleForms(scrollInput.y < 0 ? true : false);
                        _remainingTimeBetweenWeaponSwitches = TIME_BETWEEN_WEAPON_SWAPS;
                    }
                }
                else if (_inputManagerInstanceRef.CurrentDeviceType == InputManager.DeviceType.Controller)
                {
                    InputDictionary["Next Form"].SetTheState(_actions.NextWeaponFormController.ReadValue<float>() != 0);
                    if (InputDictionary["Next Form"].IsDown)
                    {
                        _playerRef.Weapon.PlayerWeapon.CycleForms(false);
                    }

                    InputDictionary["Previous Form"].SetTheState(_actions.PreviousWeaponFormController.ReadValue<float>() != 0);
                    if (InputDictionary["Previous Form"].IsDown)
                    {
                        _playerRef.Weapon.PlayerWeapon.CycleForms(true);
                    }
                }

                InputDictionary["Fire"].SetTheState(_actions.Fire.ReadValue<float>() != 0);
                if (InputDictionary["Fire"].IsDown && !_blockWeaponFire)
                {
                    FireInputPressed?.Invoke();
                }
                else if (InputDictionary["Fire"].IsReleased)
                {
                    FireInputReleased?.Invoke();
                    _blockWeaponFire = false;
                }

                InputDictionary["Action"].SetTheState(_actions.Action.ReadValue<float>() != 0);
                
                if (!BlockSpecialInput)
                {
                    if (InputDictionary["Action"].IsDown)
                    {
                        if (!_playerRef.State.Grabbing)
                        {
                            var playerController = _playerRef.Controller as PlayerController2D;
                            playerController.TryOrMaintainGrab(_playerRef.Collisions.FaceDir);
                        }
                        else
                        {
                            _playerRef.State.SetGrabbing(false);
                        }
                    }

                    InputDictionary["Jump"].SetTheState(_actions.Jump.ReadValue<float>() != 0);
                    InputDictionary["Dash"].SetTheState(_actions.Dash.ReadValue<float>() != 0);

                    InputDictionary["Hover"].SetTheState(_actions.Hover.ReadValue<float>() != 0);
                    if (InputDictionary["Hover"].IsDownOrHeld)
                    {
                        InputDictionary["Raise"].SetTheState(_actions.Raise.ReadValue<float>() > CONTROLLER_STICK_DEADZONE);
                    }

                    InputDictionary["Repair"].SetTheState(_actions.Repair.ReadValue<float>() != 0);
                }

                if (_playerRef.State.WallSliding)
                {
                    InputDictionary["Wall Grip"].SetTheState(_actions.WallGrip.ReadValue<float>() != 0);
                }

                //DebugPrintAllInputStates();
            }
        }
        #endregion

        #region Helper Methods
        private void ClearInputDictionary()
        {
            foreach (ButtonState button in InputDictionary.Values)
            {
                button.ClearState();
            }
        }

        private void DebugPrintAllInputStates()
        {
            string inputs = "";
            foreach (string state in InputDictionary.Keys)
            {
                inputs += state + ": " + InputDictionary[state] + "\n";
            }

            Debug.Log(inputs);
        }
        #endregion

        #region Callback Methods
        private void OnStunned(bool positive)
        {
            BlockInput = positive;
        }

        private void OnDead()
        {
            BlockInput = true;
        }

        private void OnRespawn(bool positive)
        {
            BlockInput = false;
        }

        private void OnRepair(bool positive)
        {
            if (positive)
            {
                BlockInput = true;
            }
            else
            {
                BlockInput = false;
            }
        }

        private void OnEnteringShip()
        {
            BlockInput = true;
        }
        #endregion
    }
}
