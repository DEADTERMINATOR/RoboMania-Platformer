﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Player.Modules
{
    public class HoverModule : PlayerUpgradeModule
    {
        protected override void Start()
        {
            base.Start();
            EnergyCost = _playerRef.Data.EnergyDrainPerSecondOfHover;
        }

        public override bool StartModule()
        {
            if (_playerRef.Data.RequestEnergy(EnergyCost * (Time.deltaTime / 1)))
            {
                _playerRef.State.SetHovering(true);

                _playerRef.GravityScale = 0;

                _playerRef.SetVelocityOnOneAxis('X', 0);
                _playerRef.SetVelocityOnOneAxis('Y', 0);

                return true;
            }

            return false;
        }

        public override bool ContinueModule()
        {
            if (_playerRef.Data.RequestEnergy(EnergyCost * (Time.deltaTime / 1)))
            {
                return true;
            }

            FinishModule();
            return false;
        }

        public override bool FinishModule()
        {
            _playerRef.State.SetHovering(false);

            _playerRef.SetVelocityOnOneAxis('Y', 0);

            if (!_playerRef.State.Dashing)
            {
                //We don't want to reset the gravity scale if the hover was cancelled by a dash, since that would conflict with the gravity scale change of the dash.
                _playerRef.GravityScale = _playerRef.Movement.CurrentActiveGravityState.BaseGravityScale;
            }

            return true;
        }
    }
}
