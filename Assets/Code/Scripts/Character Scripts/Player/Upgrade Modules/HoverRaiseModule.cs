﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Player.Modules
{
    public class HoverRaiseModule : PlayerUpgradeModule
    {
        protected override void Start()
        {
            base.Start();
            EnergyCost = _playerRef.Data.EnergyDrainPerSecondOfHover * PlayerConstants.HOVER_RAISE_ENERGY_DRAIN_MULTIPLIER;
        }

        public override bool StartModule()
        {
            if (_playerRef.Data.RequestEnergy(EnergyCost * (Time.deltaTime / 1)))
            {
                _playerRef.SetVelocityOnOneAxis('Y', PlayerConstants.HOVER_RAISE_AMOUNT_PER_SECOND * (Time.deltaTime / 1));
                return true;
            }

            FinishModule();
            return false;
        }

        public override bool ContinueModule()
        {
            return StartModule();
        }

        public override bool FinishModule()
        {
            _playerRef.SetVelocityOnOneAxis('Y', 0);
            return true;
        }
    }
}