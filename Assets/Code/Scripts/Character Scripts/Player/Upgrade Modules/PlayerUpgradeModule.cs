﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player;

namespace Characters.Player.Modules
{
    public abstract class PlayerUpgradeModule : MonoBehaviour
    {
        public float EnergyCost { get; protected set; }

        protected PlayerMaster _playerRef;

        protected virtual void Start()
        {
            _playerRef = GlobalData.Player;
        }

        public abstract bool StartModule();
        public virtual bool ContinueModule() { return false; }
        public virtual bool FinishModule() { return false; }
    }
}
