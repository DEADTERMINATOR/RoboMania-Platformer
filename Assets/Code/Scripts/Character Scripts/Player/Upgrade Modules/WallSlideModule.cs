﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Player.Modules
{
    public class WallSlideModule : PlayerUpgradeModule
    {
        /// <summary>
        /// The amount of time the player should be wall sliding for before playing the wall slide effects.
        /// </summary>
        private const float PLAY_WALL_SLIDE_EFFECT_AFTER_PERIOD_OF_TIME = 0.25f;


        /// <summary>
        /// How long the player has been wall sliding for.
        /// </summary>
        protected float _howLongWallSliding = 0;

        /// <summary>
        /// Whether the wall slide effects are currently allowed to play;
        /// </summary>
        protected bool _wallSlideEffectsAllowedToPlay = true;


        /// <summary>
        /// How long remaining until the player unsticks from the wall.
        /// </summary>
        private float _timeToWallUnstick;

        /// <summary>
        /// How long remaining until the player pushes off from the wall (the player pushed off the wall by providing movement input in the direction opposite to that of the wall).
        /// </summary>
        private float _timeToWallPushoff;

        /// <summary>
        /// The direction the player should travel on the next wall jump. -1 for left, 1 for right.
        /// </summary>
        private int _nextWallJumpDirection = 0;

        /// <summary>
        /// Whether the player has attempted the next wall jump. If they attempt to wall jump before
        /// they are wall sliding on the opposite wall, their wall jump will be cancelled and they will have to
        /// start again.
        /// </summary>
        private bool _nextWallJumpAttempted = false;

        /// <summary>
        /// Whether the facing direction of the player should be flipped on the termination of the wall slide.
        /// </summary>
        private bool _flipPlayerDirectionOnFinish = false;

        /// <summary>
        /// Whether the wall sliding effects are currently playing.
        /// </summary>
        private bool _wallSlideEffectPlaying = false;


        protected override void Start()
        {
            base.Start();
            EnergyCost = _playerRef.Data.WallClimbCost;

            _playerRef.DamageReceived += OnDamageReceived;
            _playerRef.GravityChange += OnGravityTypeChanged;
            _playerRef.Hovering += OnHoverStarted;
        }

        #region Module Methods
        public override bool StartModule()
        {
            PlayerController2D playerController = (PlayerController2D)_playerRef.Controller;
            Vector3 playerVelocity = _playerRef.Velocity;

            int wallXDirection = 0;
            PlatformType _platformType = null;
            if (_playerRef.Collisions.IsCollidingRight)
            {
                wallXDirection = 1;
                _platformType = _playerRef.Collisions.Right.CollidingObject.GetComponent<PlatformType>();
            }
            else if (_playerRef.Collisions.IsCollidingLeft)
            {
                wallXDirection = -1;
                _platformType = _playerRef.Collisions.Left.CollidingObject.GetComponent<PlatformType>();
            }

            if (!_playerRef.State.WallSliding
                && (Mathf.Sign(_playerRef.MovementInput.x) == Mathf.Sign(wallXDirection) || (_playerRef.State.Dashing && playerController.Collisions.FaceDir == wallXDirection))
                && playerVelocity.y <= 0
                && !playerController.Collisions.IsCollidingBelow
                && !_playerRef.State.Bumped
                && CollisionRaycaster2D.PointDistanceFromGround(playerController.Collisions.FaceDir == 1 ? playerController.Raycaster.Origins.BottomLeft : playerController.Raycaster.Origins.BottomRight) >= 3.0f //Raycast from the bottom back of the player.
                && playerController.PlayerCollisions.PercentageCollidingHorizontal >= PlayerConstants.MINIMUM_HORIZONTAL_COLLISION_PERCENTAGE_FOR_WALL_SLIDE
                && (_nextWallJumpDirection != 0 || (_nextWallJumpDirection == 0 && wallXDirection != 0))
                && _playerRef.Movement.CurrentActiveGravityState.GravityType == GlobalData.GravityType.Base
                && (_platformType == null || _platformType.Type == PlatformType.PlatformTypes.Normal))
            {
                _playerRef.State.SetWallSliding(true);

                _nextWallJumpDirection = _playerRef.Collisions.IsCollidingRight ? -1 : 1;
                _nextWallJumpAttempted = false;

                if (playerVelocity.y < -PlayerConstants.MAXIMUM_WALL_SLIDE_SPEED)
                {
                    _playerRef.SetVelocityOnOneAxis('Y', -PlayerConstants.MAXIMUM_WALL_SLIDE_SPEED);
                }
                else if (playerVelocity.y > 0)
                {
                    _playerRef.SetVelocityOnOneAxis('Y', 0);
                }

                _howLongWallSliding = 0;

                return true;
            }

            return false;
        }

        public override bool ContinueModule()
        {
            PlayerController2D playerController = (PlayerController2D)_playerRef.Controller;

            int wallXDirection = 0;
            if (_playerRef.Collisions.IsCollidingRight)
            {
                wallXDirection = 1;
            }
            else if (_playerRef.Collisions.IsCollidingLeft)
            {
                wallXDirection = -1;
            }

            if ((_playerRef.Velocity.y < 0 || (_playerRef.State.Dashing && playerController.Collisions.FaceDir == wallXDirection) || _playerRef.State.WallGripping) && _timeToWallUnstick < PlayerConstants.MAXIMUM_WALL_STICK_TIME)
            {
                Vector2 playerMovementInput = _playerRef.MovementInput;
                float playerMovementInputSign = Mathf.Sign(playerMovementInput.x);
                float playerFaceDirSign = Mathf.Sign(_playerRef.Collisions.FaceDir);

                if (playerMovementInputSign == playerFaceDirSign || playerMovementInput.x == 0 || (playerMovementInputSign != playerFaceDirSign && _timeToWallPushoff < PlayerConstants.MAXIMUM_WALL_PUSHOFF_TIME))
                {
                    _playerRef.SetVelocityOnOneAxis('X', 0);
                    _nextWallJumpAttempted = false;

                    if (_playerRef.Velocity.y < -PlayerConstants.MAXIMUM_WALL_SLIDE_SPEED)
                    {
                        _playerRef.SetVelocityOnOneAxis('Y', -PlayerConstants.MAXIMUM_WALL_SLIDE_SPEED);
                    }

                    if (_wallSlideEffectsAllowedToPlay && !_wallSlideEffectPlaying)
                    {
                        _howLongWallSliding += Time.deltaTime;
                        if (_howLongWallSliding >= PLAY_WALL_SLIDE_EFFECT_AFTER_PERIOD_OF_TIME)
                        {
                            _playerRef.Effects.PlayWallSlideEffects();
                            _wallSlideEffectPlaying = true;
                        }
                    }

                    //We use this wall pushoff value to give the player a little bit of leeway if they're a little fast in pushing their movement input in the direction of the wall they want to jump to.
                    //Without it, if they're even a frame early with the movement input change, the wall jump would fail, which can feel punishing.
                    if (playerMovementInputSign != playerFaceDirSign)
                    {
                        _timeToWallPushoff += Time.deltaTime;
                    }
                    else
                    {
                        _timeToWallPushoff = 0;
                    }

                    PlayerController2D.PlayerCollisionInfo castedCollisions = (PlayerController2D.PlayerCollisionInfo)_playerRef.Collisions;

                    //Looking for the raycast 1/4ths of the way up from the bottom.
                    //If the ray is not colliding, we are nearing a gap in the wall, so start a timer until we automatically dismount.
                    if (!castedCollisions.IsGivenRayCollidingHorizontally(playerController.Raycaster.HorizontalRayCount / 4, playerController.Raycaster, PlayerConstants.CHECK_FOR_WALL_SLIDE_DISTANCE))
                    {
                        _timeToWallUnstick += Time.deltaTime;
                    }
                    else
                    {
                        _timeToWallUnstick = 0;
                    }

                    if (!_playerRef.State.WallGripping)
                    {
                        float frameTimePercentOfASecond = Time.deltaTime / 1.0f;
                        _playerRef.Data.RechargeEnergyCells(PlayerConstants.WALL_SLIDING_ENERGY_CELL_RECHARGE_RATE * frameTimePercentOfASecond);
                    }
                }
                else
                {
                    _flipPlayerDirectionOnFinish = false;
                    FinishModule();
                }
            }
            else
            {
                if (_nextWallJumpDirection == 0)
                {
                    _flipPlayerDirectionOnFinish = false;
                }
                else if (_playerRef.Velocity.y == 0 && !_playerRef.State.Dashing)
                {
                    _flipPlayerDirectionOnFinish = true;
                }

                FinishModule();
            }

            return true;
        }

        public override bool FinishModule()
        {
            _playerRef.State.SetWallSliding(false);

            _timeToWallPushoff = 0;
            _timeToWallUnstick = 0;

            _nextWallJumpAttempted = false;
            _nextWallJumpDirection = 0;

            if (_flipPlayerDirectionOnFinish)
            {
                _playerRef.Animation.SetSpriteDirection(_playerRef.Collisions.FaceDir == 1 ? false : true, true, false);
            }

            EndWallSlidingEffects();

            return true;
        }
        #endregion

        #region Wall Jump
        public void WallJump(bool overrideWallJumpDirection = false, int newWallJumpDirection = 1)
        {
            if (!_nextWallJumpAttempted)
            {
                if (_playerRef.Data.RequestEnergy(EnergyCost))
                {
                    var wallJumpVelocity = PlayerConstants.WALL_JUMP;

                    if (overrideWallJumpDirection)
                    {
                        _playerRef.SetVelocity(new Vector2(wallJumpVelocity.x * newWallJumpDirection, wallJumpVelocity.y));
                    }
                    else
                    {
                        _playerRef.SetVelocity(new Vector2(wallJumpVelocity.x * _nextWallJumpDirection, wallJumpVelocity.y));
                    }

                    _playerRef.State.SetWallJumpingOffWall(true);
                    _playerRef.Animation.SetMultiJumpAnimation();

                    _playerRef.NotifyEnergyCellMoveUsed();

                    _nextWallJumpAttempted = true;
                    FinishModule();
                }
            }
            else
            {
                FinishModule();
            }
        }
        #endregion

        #region Callbacks and Support Methods
        protected void EndWallSlidingEffects()
        {
            _playerRef.Effects.StopWallSlideEffects(false);
            _wallSlideEffectPlaying = false;
            _howLongWallSliding = 0;
        }

        private void OnGravityTypeChanged(GlobalData.GravityType type)
        {
            _flipPlayerDirectionOnFinish = false;
            FinishModule();
        }

        protected virtual void OnDashPerformed()
        {
            _flipPlayerDirectionOnFinish = true;
            FinishModule();
        }

        private void OnDamageReceived(Vector3 position)
        {
            _flipPlayerDirectionOnFinish = true;
            FinishModule();
        }

        private void OnHoverStarted(bool started)
        {
            if (started)
            {
                FinishModule();
            }
        }
        #endregion
    }
}