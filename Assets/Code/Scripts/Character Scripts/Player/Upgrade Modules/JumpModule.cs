﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelComponents.Platforms;

namespace Characters.Player.Modules
{
    public class JumpModule : PlayerUpgradeModule
    {
        public BouncyPlatform BouncyPlatformUnderneath;

        public override bool StartModule()
        {
            float currentPlayerYVelocity = _playerRef.Velocity.y;

            if ((_playerRef.Collisions.IsCollidingBelow && _playerRef.State.TimeOnGround >= PlayerConstants.MINIMUM_GROUND_TIME_BETWEEN_JUMP)
                || _playerRef.State.TimeInAir <= PlayerConstants.AIR_TIME_JUMP_LEEWAY
                || (!_playerRef.Collisions.IsCollidingBelow && _playerRef.State.Dashing)
                || _playerRef.State.Climbing
                || _playerRef.Movement.CurrentActiveGravityState.GravityType != GlobalData.GravityType.Base)
            {
                _playerRef.Animation.SetJumpAnimation();

                if (_playerRef.State.Bouncing)
                {
                    _playerRef.SetVelocityOnOneAxis('Y', currentPlayerYVelocity *= 1.5f);
                }
                else if (BouncyPlatformUnderneath != null && BouncyPlatformUnderneath.PlayerCanBoostVelocityWithTimedJump)
                {
                    _playerRef.SetVelocityOnOneAxis('Y', BouncyPlatformUnderneath.JumpVelocity * 1.5f);
                }
                else if (currentPlayerYVelocity < _playerRef.Movement.JumpVelocity)
                {
                    _playerRef.SetVelocityOnOneAxis('Y', _playerRef.Movement.JumpVelocity);
                }

                _playerRef.GravityScale = _playerRef.Movement.CurrentActiveGravityState.BaseGravityScale;

                _playerRef.Input.BlockSpecialInput = true;
                GlobalData.Timekeeper.StartTimer(() => _playerRef.Input.BlockSpecialInput = false, PlayerConstants.JUMP_INPUT_LOCK_TIME);

                _playerRef.State.SetGrabbing(false);
                _playerRef.State.SetClimbing(false, null);

                return true;
            }

            return false;
        }
    }
}
