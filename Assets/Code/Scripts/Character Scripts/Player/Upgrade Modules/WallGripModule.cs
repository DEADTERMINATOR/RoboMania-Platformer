﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Inputs;

namespace Characters.Player.Modules
{
    public class WallGripModule : WallSlideModule
    {
        public bool HoldToEnable = true;

        private bool _shouldWallGrip = false;

        protected override void Start()
        {
            base.Start();

            _playerRef.DashPerformed += OnDashPerformed;
            _playerRef.WallSliding += OnWallSlidingFinished;

            InputManager.Instance.RegisterSpecificActionStarted("Wall Grip", () =>
            {
                if (_playerRef.State.WallSliding)
                {
                    if (!HoldToEnable && _shouldWallGrip)
                    {
                        DisableGrip();
                        _playerRef.SetVelocityOnOneAxis('Y', -0.01f); //We set the velocity to just under zero so the conditions for continuing wall sliding can be met.
                    }
                    else
                    {
                        EnableGrip();
                    }
                }
            });
            InputManager.Instance.RegisterSpecificActionCancelled("Wall Grip", () =>
            {
                if (HoldToEnable && _shouldWallGrip)
                {
                    DisableGrip();
                    _playerRef.SetVelocityOnOneAxis('Y', -0.01f); //We set the velocity to just under zero so the conditions for continuing wall sliding can be met.
                }
            });
        }

        public override bool ContinueModule()
        {
            base.ContinueModule();
            if (_shouldWallGrip)
            {
                if ((Mathf.Sign(_playerRef.MovementInput.x) == Mathf.Sign(_playerRef.Collisions.FaceDir) || _playerRef.MovementInput.x == 0))
                {
                    _playerRef.SetVelocityOnOneAxis('Y', 0);
                }
                else
                {
                    DisableGrip();
                }
            }

            return true;
        }

        private void EnableGrip()
        {
            _shouldWallGrip = true;
            _playerRef.State.SetWallGripping(true);
            EndWallSlidingEffects();
            _wallSlideEffectsAllowedToPlay = false;
        }

        private void DisableGrip()
        {
            _shouldWallGrip = false;
            _playerRef.State.SetWallGripping(false);
            _wallSlideEffectsAllowedToPlay = true;

            if (_playerRef.State.WallSliding)
            {
                _howLongWallSliding = 0;
            }
        }

        protected override void OnDashPerformed()
        {
            if (_shouldWallGrip)
            {
                base.FinishModule();
                DisableGrip();
            }
        }

        private void OnWallSlidingFinished(bool positive)
        {
            if (!positive)
            {
                DisableGrip();
            }
        }
    }
}
