﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

namespace Characters.Player.Modules
{
    public class MultiJumpModule : PlayerUpgradeModule
    {
        protected override void Start()
        {
            base.Start();
            EnergyCost = PlayerConstants.MULTI_JUMP_ENERGY_USAGE;
        }

        public override bool StartModule()
        {
            if (_playerRef.Data.RequestEnergy(EnergyCost))
            {
                _playerRef.Animation.SetMultiJumpAnimation();
                _playerRef.NotifyEnergyCellMoveUsed();

                float newYVelocity = _playerRef.Movement.JumpVelocity / PlayerConstants.MULTI_JUMP_POWER_REDUCTION;
                _playerRef.GravityScale = PlayerConstants.MULTI_JUMP_GRAVITY_SCALE;
                _playerRef.MoveSpeed = _playerRef.Movement.BaseMoveSpeed;

                if (_playerRef.Velocity.y < newYVelocity)
                {
                    _playerRef.SetVelocityOnOneAxis('Y', newYVelocity);
                }

                _playerRef.Input.BlockSpecialInput = true;
                GlobalData.Timekeeper.StartTimer(() => _playerRef.Input.BlockSpecialInput = false, PlayerConstants.MULTI_JUMP_INPUT_LOCK_TIME);

                _playerRef.Effects.PlayMultiJumpEffect();

                return true;
            }

            return false;
        }
    }
}
