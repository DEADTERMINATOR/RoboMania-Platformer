﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters.Player.Modules
{
    public class DashModule : PlayerUpgradeModule
    {
        public bool DashEasingActive { get; private set; }


        private float _dashDirection;

        private Vector2 _totalDashVelocityToDistribute;
        private Vector2 _remainingDashVelocityToDistribute;

        private bool _dashMarkedFinished = false;

        private float _dashVelocitySmoothing = 0;
        private float _dashEasingTime = 0;

        protected override void Start()
        {
            base.Start();
            EnergyCost = GlobalData.Player.Data.DashEnergyCost;
        }

        public override bool StartModule()
        {
            float dashDirection = 0;

            if (!_playerRef.State.WallSliding)
            {
                if (_playerRef.MovementInput.x != 0)
                {
                    /* Round up to 1 or down to -1. */
                    dashDirection = _playerRef.MovementInput.x > 0 ? Mathf.Ceil(_playerRef.MovementInput.x) : Mathf.Floor(_playerRef.MovementInput.x);
                }
                else
                {
                    dashDirection = _playerRef.Collisions.FaceDir;
                }
            }
            else
            {
                dashDirection = -_playerRef.Collisions.FaceDir;
            }

            if (_playerRef.Movement.CurrentActiveGravityState.DashingAllowed
                && !(_playerRef.Collisions.IsCollidingRight && dashDirection == 1)
                && !(_playerRef.Collisions.IsCollidingLeft && dashDirection == -1)
                && _playerRef.Data.RequestEnergy(EnergyCost))
            {
                _playerRef.State.SetDashing(true);
                _dashMarkedFinished = false;

                _playerRef.SetVelocityOnOneAxis('X', _playerRef.MoveSpeed);
                _playerRef.SetVelocityOnOneAxis('Y', 0);

                _playerRef.GravityScale = 0;
                _playerRef.Animation.SetSpriteDirection(dashDirection == 1 ? true : false);
                _dashDirection = dashDirection;

                /* Note: _totalDash and _remainingDash have been made Vector2s so dashing could be more easily extended to include dashing on the Y-axis if later desired */
                _totalDashVelocityToDistribute = _remainingDashVelocityToDistribute = new Vector2(PlayerConstants.DASH_VELOCITY, 0);

                _playerRef.Input.BlockInput = true;
                _playerRef.Input.BlockSpecialInput = true;
                _playerRef.State.SetGrabbing(false);

                _playerRef.NotifyEnergyCellMoveUsed();
                _playerRef.NotifyDashPerformed();

                return true;
            }

            return false;
        }

        public override bool ContinueModule()
        {
            bool isDashFinished = false;
            float amountOfDashToResolve = _totalDashVelocityToDistribute.x * (Time.deltaTime / PlayerConstants.DESIRED_TIME_TO_RESOLVE_DASH);
            if (_remainingDashVelocityToDistribute.x <= amountOfDashToResolve)
            {
                _remainingDashVelocityToDistribute = Vector2.zero;
                isDashFinished = true;
            }
            else
            {
                _remainingDashVelocityToDistribute.x -= amountOfDashToResolve;
            }

            if (((_playerRef.Collisions.IsCollidingRight && _dashDirection == 1) || (_playerRef.Collisions.IsCollidingLeft && _dashDirection == -1))
                && _playerRef.Collisions.IsCollidingBelow)
            {
                isDashFinished = true;
            }

            _playerRef.SetVelocityOnOneAxis('X', amountOfDashToResolve * _dashDirection);

            if (isDashFinished)
            {
                _playerRef.State.SetDashing(false);
                DashEasingActive = true;

                /* We may have terminated early, missing the 75% check below. */
                if (!_dashMarkedFinished)
                {
                    FinishModule();
                }
                _playerRef.Effects.StopDashEffect();

                return true;
            }
            else
            {
                float percentageOfDashToRemainingToDistribute = _remainingDashVelocityToDistribute.x / _totalDashVelocityToDistribute.x;
                if (1.0f - PlayerConstants.DASH_RESOLVED_PERCENTAGE_TO_RESTORE_GRAVITY_AND_INPUT >= percentageOfDashToRemainingToDistribute)
                {
                    if (!_dashMarkedFinished)
                    {
                        FinishModule();
                    }

                    return false;
                }
            }

            return false;
        }

        public override bool FinishModule()
        {
            _playerRef.GravityScale = _playerRef.Movement.CurrentActiveGravityState.BaseGravityScale;

            _playerRef.Input.BlockInput = false;
            _playerRef.Input.BlockSpecialInput = false;

            _dashMarkedFinished = true;
            return true;
        }

        public void EaseOutDash(float desiredXMovementInputVelocity)
        {
            _dashEasingTime += Time.deltaTime;

            if (desiredXMovementInputVelocity == 0)
            {
                _playerRef.SetVelocityOnOneAxis('X', Mathf.SmoothDamp(_playerRef.Velocity.x, _playerRef.MoveSpeed * _dashDirection, ref _dashVelocitySmoothing, PlayerConstants.DESIRED_TIME_TO_EASE_OUT_DASH_NO_INPUT));
                if (_dashEasingTime >= PlayerConstants.DESIRED_TIME_TO_EASE_OUT_DASH_NO_INPUT)
                {
                    DashEasingActive = false;
                    _dashEasingTime = 0;
                }
            }
            else
            {
                _playerRef.SetVelocityOnOneAxis('X', Mathf.SmoothDamp(_playerRef.Velocity.x, desiredXMovementInputVelocity, ref _dashVelocitySmoothing, PlayerConstants.DESIRED_TIME_TO_EASE_OUT_DASH_WITH_INPUT));
                if (_dashEasingTime >= PlayerConstants.DESIRED_TIME_TO_EASE_OUT_DASH_WITH_INPUT)
                {
                    DashEasingActive = false;
                    _dashEasingTime = 0;
                }
            }
        }
    }
}
