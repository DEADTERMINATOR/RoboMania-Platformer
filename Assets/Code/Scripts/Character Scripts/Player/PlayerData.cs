﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Player.Modules;
using System.Reflection;
using PlayerDynamicModule = GlobalData.PlayerDynamicModules;
using System.Runtime.InteropServices;

namespace Characters.Player
{
    [Serializable]
    public class PlayerData
    {
        #region Ammo Class
        public class Ammo
        {
            /// <summary>
            /// The maximum amount of bullet ammo the player can have.
            /// </summary>
            private const float BASE_MAX_BULLET_COST = 1f / 750f;

            /// <summary>
            /// The maximum amount of energy ammo the player can have.
            /// </summary>
            private const float BASE_MAX_ENERGY_COST = 1f / 75f;

            /// <summary>
            /// The maximum amount of rocket ammo the player can have.
            /// </summary>
            private const float BASE_MAX_ROCKET_COST = 1f / 25f;

            /// <summary>
            /// The maximum amount of PENETRATION ammo the player can have.
            /// </summary>
            private const float BASE_MAX_PENETRATION_COST = 1f / 40f;

            /// <summary>
            /// The minimum amount of ammo of any projectile type the player can have.
            /// </summary>
            private const int MINIMUM_MAX_AMMO = 1;

            /// <summary>
            /// The different types of ammo the player can possess.
            /// </summary>
            public enum AmmoTypes { Bullet, Energy, Rocket, Penetration, Count }

            /// <summary>
            /// The price per percentage of ammo.
            /// </summary>
            public int[] AmmoBuyCosts { get; private set; } = { 1, 2, 2, 3, 5, 10 };

            /// <summary>
            /// The percentage of each type of ammo currently possessed.
            /// </summary>
            public float[] AmmoPercentageAmounts { get; private set; } = new float[(int)AmmoTypes.Count];


            /// <summary>
            /// The multipliers applied to the base cost of a single shot for a given ammo type to find the final cost.
            /// </summary>
            private float[] _ammoCostMultipliers = { 1f, 1f, 1f, 1f };


            /// <summary>
            /// Instantiates with each ammo type receiving the specified starting ammo percentage.
            /// </summary>
            /// <param name="startingAmmoPercentage">The percentage of ammo each ammo type will begin with.</param>
            public Ammo(int startingAmmoPercentage)
            {
                Mathf.Clamp(startingAmmoPercentage, 0, 100);
                for (int i = 0; i < AmmoPercentageAmounts.Length; ++i)
                {
                    AmmoPercentageAmounts[i] = startingAmmoPercentage / 100f; //Convert the percentage to a value between 0 and 1. Makes calculating accurate projectile costs easier.
                }
            }

            /// <summary>
            /// Instantiates with each ammo type receiving the amount specified in the array. Logs a warning if the provided array's size does not match the total number of projectile types.
            /// </summary>
            /// <param name="ammoAmounts">An array containing the percentages for each ammo type.</param>
            public Ammo(float[] ammoAmounts)
            {
                AmmoPercentageAmounts = ammoAmounts;
                if (ammoAmounts.Length != (int)AmmoTypes.Count)
                {
                    Debug.LogWarning("Player's ammo inventory was instantiated with an ammo count array whose length did not match the number of ammo types the player can have.");
                }
            }

            /// <summary>
            /// Only for serialization
            /// </summary>
            public Ammo()
            {

            }

            /// <summary>
            /// Get the cost of the amount of bullets of the specified type
            /// </summary>
            /// <param name="ammoType">The type of ammo.</param>
            /// <param name="percentage">The percentage of the player's total ammo.</param>
            /// <returns>The price for the requested percentage of ammo.</returns>
            public int GetPriceOfAmmo(AmmoTypes ammoType, int percentage)
            {
                if (percentage > 0)
                {
                    return AmmoBuyCosts[(int)ammoType] * percentage;
                }
                return 0;
            }

            /// <summary>
            /// Gets the cost of a single shot for a given ammo type factoring in any current cost multipliers.
            /// </summary>
            /// <param name="ammoType">The type of ammo.</param>
            /// <returns>The cost of a single shot of the provided ammo type.</returns>
            public float GetAmmoCost(AmmoTypes ammoType)
            {
                switch (ammoType)
                {
                    case AmmoTypes.Bullet:
                    {
                        return BASE_MAX_BULLET_COST * _ammoCostMultipliers[(int)ammoType];
                    }
                    case AmmoTypes.Energy:
                    {
                        return BASE_MAX_ENERGY_COST * _ammoCostMultipliers[(int)ammoType];
                    }
                    case AmmoTypes.Rocket:
                    {
                        return BASE_MAX_ROCKET_COST * _ammoCostMultipliers[(int)ammoType];
                    }
                    case AmmoTypes.Penetration:
                    {
                        return BASE_MAX_PENETRATION_COST * _ammoCostMultipliers[(int)ammoType];
                    }
                }
                return 0;
            }

            /// <summary>
            /// Adds the specified percentage of ammo (or as much as possible) to the specified
            /// ammo types total.
            /// </summary>
            /// <param name="ammoType">The type of ammo being added.</param>
            /// <param name="percentageOfTotal">The percentage of the player's allowed total amount of ammo being added.</param>
            public void AddAmmo(AmmoTypes ammoType, int percentageOfTotal)
            {
                float percentageBetween0And1 = percentageOfTotal / 100f;
                if (AmmoPercentageAmounts[(int)ammoType] + percentageBetween0And1 > 1f)
                {
                    AmmoPercentageAmounts[(int)ammoType] = 1f;
                }
                else
                {
                    AmmoPercentageAmounts[(int)ammoType] += percentageBetween0And1;
                }
            }

            /// <summary>
            /// Calculates and returns the number of shots the player current has for a given ammo type based on the current cost of a shot and
            /// their remaining percentage of that ammo type.
            /// </summary>
            /// <param name="ammoType">The type of ammo.</param>
            /// <returns>The number of shots the player currently has.</returns>
            public int GetCurrentAmmoCount(AmmoTypes ammoType)
            {
                return Mathf.FloorToInt(AmmoPercentageAmounts[(int)ammoType] / GetAmmoCost(ammoType));
            }

            /// <summary>
            /// Calculates and returns the maximum number of shots the player can carry for a given ammo type based on the current cost of a shot.
            /// </summary>
            /// <param name="ammoType">The type of ammo.</param>
            /// <returns>The maximum number of shots the player can carry.</returns>
            public int GetMaxAmmoCount(AmmoTypes ammoType)
            {
                return Mathf.FloorToInt(1f / GetAmmoCost(ammoType));
            }

            /// <summary>
            /// Adds the provided multiplier to the provided ammo type's cost multiplier. Provide a negative multiplier to subtract from an ammo type's cost multiplier.
            /// </summary>
            /// <param name="ammoType">The type of ammo.</param>
            /// <param name="additionalMultiplier">The amount of multiplier being added/subtracted.</param>
            public void AddToAmmoMultiplier(AmmoTypes ammoType, float additionalMultiplier)
            {
                _ammoCostMultipliers[(int)ammoType] += additionalMultiplier;
            }

            /// <summary>
            /// Gets the cost of a shot if available, all the remaining energy if less than the required cost amount is available, or none if no ammo is available.
            /// Decrements amount from total ammo percentage.
            /// </summary>
            /// <param name="ammoType">The type of ammo being retrieved.</param>
            public float GetAmmoEvenIfNotEnough(AmmoTypes ammoType)
            {
                //if the type is not valid 
                if ((int)ammoType >= (int)AmmoTypes.Count || (int)ammoType < 0)
                {
                    return -1;
                }

                float fullCost = GetAmmoCost(ammoType);
                if (AmmoPercentageAmounts[(int)ammoType] >= fullCost)
                {
                    AmmoPercentageAmounts[(int)ammoType] -= fullCost;
                    return fullCost;
                }
                else
                {
                    float returnVal = AmmoPercentageAmounts[(int)ammoType];
                    AmmoPercentageAmounts[(int)ammoType] = 0;
                    return returnVal;
                }
            }

            /// <summary>
            /// Tries to get the percentage of ammo required for a single shot. If the necessary amount can be retrieved, that amount is decremented from the total. Otherwise, nothing happens.
            /// </summary>
            /// <param name="ammoType">The type of ammo being retrieved.</param>
            /// <returns>True if the player possessed the amount required, false otherwise.</returns>
            public bool TryGetAmmo(AmmoTypes ammoType)
            {
                //if the type is not valid 
                if ((int)ammoType >= (int)AmmoTypes.Count || (int)ammoType < 0)
                {
                    return false;
                }

                float fullCost = GetAmmoCost(ammoType);
                if (AmmoPercentageAmounts[(int)ammoType] >= fullCost)
                {
                    AmmoPercentageAmounts[(int)ammoType] -= fullCost;
                    return true;
                }

                return false;
            }
        }
        #endregion

        #region Constants
        /// <summary>
        /// The amount of energy that a single energy cell contains.
        /// </summary>
        public const int ENERGY_PER_CELL = 100;

        /// <summary>
        /// The amount of energy a single dash uses without any upgrades.
        /// </summary>
        private const float INITIAL_DASH_ENERGY_USAGE = 80f;

        /// <summary>
        /// The amount of energy a single wall climb jump uses without any upgrades.
        /// </summary>
        private const float INITIAL_WALL_CLIMB_ENERGY_USAGE = 20f;

        /// <summary>
        /// The amount of energy a second of hovering uses without any upgrades.
        /// </summary>
        private const float INITIAL_HOVER_ENERGY_USAGE_PER_SECOND = 20f;
        #endregion

        #region Public Properties
        /// <summary>
        /// The amount of health the player currently has.
        /// </summary>
        public float Health { get; private set; }

        /// <summary>
        /// The amount of health the player can have at maximum.
        /// </summary>
        public float MaxHealth { get; private set; }

        /// <summary>
        /// The maximum move speed the player can achieved without dashing.
        /// </summary>
        public float BaseMoveSpeed { get; private set; }

        /// <summary>
        /// The player's inventory of ammo.
        /// </summary>
        public Ammo AmmoInventory;

        /// <summary>
        /// The total amount of scrap the player has.
        /// </summary>
        public int TotalScrap { get; private set; }

        /// <summary>
        /// The total amount of energy cell energy the player has.
        /// </summary>
        public float TotalEnergyCellEnergy { get; private set; }

        /// <summary>
        /// The total amount of free energy cell energy.
        /// </summary>
        public float FreeCellEnergy { get { return TotalEnergyCellEnergy - _currentUsedEnergyCellEnergy; } }

        /// <summary>
        /// The total number of energy cells the player has.
        /// </summary>
        public int TotalEnergyCells { get { return (int)(TotalEnergyCellEnergy / ENERGY_PER_CELL); } }

        /// <summary>
        /// The amount of energy from the player's reserves a single dash costs.
        /// </summary>
        public float DashEnergyCost { get; private set; }

        /// <summary>
        /// The amount of energy from the player's reserves a wall climb jump costs.
        /// </summary>
        public float WallClimbCost { get; private set; }

        /// <summary>
        /// The amount of energy from the player's reserves that is drained per second of hovering.
        /// </summary>
        public float EnergyDrainPerSecondOfHover { get; private set; }
        #endregion


        /// <summary>
        /// The amount of energy cell energy currently being used.
        /// </summary>
        private float _currentUsedEnergyCellEnergy = 0;

        /// <summary>
        /// The amount of energy used the last time the player requested energy usage. Stored in case a "refund" is required.
        /// </summary>
        private float _lastUsedAmountOfEnergy = 0;

        private Dictionary<string, PlayerUpgradeModule> _modulesPossessed = new Dictionary<string, PlayerUpgradeModule>();

        private string _fileToSaveLoadFrom = "Save.es3";

        private PlayerMaster _playerRef;


        #region Instantiation
        //TODO: This constructor reconstructs the PlayerData from save files or apply default values.
        public PlayerData(PlayerMaster playerRef, string fileToSaveLoadFrom)
        {
            _fileToSaveLoadFrom = fileToSaveLoadFrom;
            Health = MaxHealth = 100;

            if (ES3.KeyExists("TotalScrap", fileToSaveLoadFrom))
            {
                TotalScrap = ES3.Load<int>("Total Scrap", _fileToSaveLoadFrom);
            }
            else
            {
                TotalScrap = 0;
            }

            if (ES3.KeyExists("Total Energy", fileToSaveLoadFrom))
            {
                TotalEnergyCellEnergy = ES3.Load<float>("Total Energy", _fileToSaveLoadFrom, 100);
            }
            else
            {
                TotalEnergyCellEnergy = 1 * ENERGY_PER_CELL;
            }


            BaseMoveSpeed = 8.5f;

            if (ES3.KeyExists("Dash Upgrade Owned", fileToSaveLoadFrom))
            {
                DashEnergyCost = INITIAL_DASH_ENERGY_USAGE / 2;
            }
            else
            {
                DashEnergyCost = INITIAL_DASH_ENERGY_USAGE;
            }

            bool wallClimbUpgrade1Owned = ES3.KeyExists("Wall Climb Upgrade 1 Owned", fileToSaveLoadFrom);
            bool wallClimbUpgrade2Owned = ES3.KeyExists("Wall Climb Upgrade 2 Owned", fileToSaveLoadFrom);
            if (wallClimbUpgrade1Owned && wallClimbUpgrade2Owned)
            {
                WallClimbCost = 0;
            }
            else if (wallClimbUpgrade1Owned || wallClimbUpgrade2Owned)
            {
                WallClimbCost = INITIAL_WALL_CLIMB_ENERGY_USAGE / 2;
            }
            else
            {
                WallClimbCost = INITIAL_WALL_CLIMB_ENERGY_USAGE;
            }

            EnergyDrainPerSecondOfHover = INITIAL_HOVER_ENERGY_USAGE_PER_SECOND;

            playerRef.Respawned += new PlayerMaster.StateChange(OnRespawn);
            playerRef.Climbing += new PlayerMaster.StateChange(OnClimbingChange);
            playerRef.GravityChange += new PlayerMaster.GravityChangeEvent(OnGravityChange);

            if (ES3.KeyExists("Last Checkpoint", fileToSaveLoadFrom))
            {
                playerRef.transform.position = ES3.Load<Vector3>("Last Checkpoint", fileToSaveLoadFrom);
                int CameraMovmentIndex = ES3.Load<int>("CameraMovmentIndex", fileToSaveLoadFrom, 0);
            }

            if (ES3.KeyExists("Ammo Percentages", _fileToSaveLoadFrom))
            {
                float[] loadedAmmoPercentages = ES3.Load<float[]>("Ammo Percentages", _fileToSaveLoadFrom);
                AmmoInventory = new Ammo(loadedAmmoPercentages);
                /*
                for (int i = 0; i < loadedAmmoPercentages.Length; ++i)
                {
                    AmmoInventory.SetUnmodifiedAmmoCount((Ammo.AmmoTypes)i, loadedAmmoPercentages[i]);
                }
                */
            }
            else
            {
                AmmoInventory = new Ammo(100);
            }

            string jumpName = PlayerDynamicModule.JUMP;
            string multiJumpName = PlayerDynamicModule.MULTI_JUMP;
            string dashName = PlayerDynamicModule.DASH;
            string hoverName = PlayerDynamicModule.HOVER;
            string hoverRaiseName = PlayerDynamicModule.HOVER_RAISE;
            string wallSlideName = PlayerDynamicModule.WALL_SLIDE;
            string wallGripName = PlayerDynamicModule.WALL_GRIP;

            /* These do the initial write of the module information to the save file, so the information as to whether the player possesses a given module exists in the save file from the get go.
             * The function also writes another field to the save file that denotes whether the module is currently enabled. This allows the enabled state of the module to be persisted,
             * in case a checkpoint/save occurs during a point where one or more modules are possessed but disabled. The function does a check to see if the key already exists, so this will not overwrite an existing status.
             * Eventually, this code should be moved to only be run when the player starts a new campaign */
            WriteModuleToSaveFile(jumpName, true);
            WriteModuleToSaveFile(multiJumpName, true);
            WriteModuleToSaveFile(dashName, false);
            WriteModuleToSaveFile(hoverName, true);
            WriteModuleToSaveFile(hoverRaiseName, true);
            WriteModuleToSaveFile(wallSlideName, true);
            WriteModuleToSaveFile(wallGripName, false);

            //Add and load the states of the default modules (the ones the player has from the start).
            var jump = playerRef.gameObject.AddComponent<JumpModule>();
            var multiJump = playerRef.gameObject.AddComponent<MultiJumpModule>();
            var hover = playerRef.gameObject.AddComponent<HoverModule>();
            var hoverRaise = playerRef.gameObject.AddComponent<HoverRaiseModule>();
            var wallSlide = playerRef.gameObject.AddComponent<WallSlideModule>();

            _modulesPossessed.Add(jumpName, jump);
            _modulesPossessed.Add(multiJumpName, multiJump);
            _modulesPossessed.Add(hoverName, hover);
            _modulesPossessed.Add(hoverRaiseName, hoverRaise);
            _modulesPossessed.Add(wallSlideName, wallSlide);

            ChangeModuleActiveState(jumpName, ES3.Load<bool>(jumpName + " enabled", _fileToSaveLoadFrom));
            ChangeModuleActiveState(multiJumpName, ES3.Load<bool>(multiJumpName + " enabled", _fileToSaveLoadFrom));
            ChangeModuleActiveState(hoverName, ES3.Load<bool>(hoverName + " enabled", _fileToSaveLoadFrom));
            ChangeModuleActiveState(hoverRaiseName, ES3.Load<bool>(hoverRaiseName + " enabled", _fileToSaveLoadFrom));
            ChangeModuleActiveState(wallSlideName, ES3.Load<bool>(wallSlideName + " enabled", _fileToSaveLoadFrom));

            //Add and load the states of the modules that the player must collect over the course of the game.
            if (ES3.Load<bool>(dashName, _fileToSaveLoadFrom) || playerRef.StartWithDash)
            {
                var dash = playerRef.gameObject.AddComponent<DashModule>();
                _modulesPossessed.Add(dashName, dash);
                ChangeModuleActiveState(dashName, playerRef.StartWithDash == true ? true : ES3.Load<bool>(dashName + " enabled", _fileToSaveLoadFrom));
            }
            if (ES3.Load<bool>(wallGripName, _fileToSaveLoadFrom) || playerRef.StartWithWallClimb)
            {
                var wallGrip = playerRef.gameObject.AddComponent<WallGripModule>();
                _modulesPossessed.Add(wallGripName, wallGrip);
                ChangeModuleActiveState(wallGripName, playerRef.StartWithWallClimb == true ? true : ES3.Load<bool>(wallGripName + " enabled", _fileToSaveLoadFrom));
            }

            _playerRef = playerRef;
        }

        //This is a constructor that allows manual setting of key data values and overrides any save file data.
        public PlayerData(float maxHealth, int startingAmmoPercentage, int totalScrap, int startingEnergyCells, PlayerMaster playerRef, string fileToSaveLoadFrom)
        {
            _fileToSaveLoadFrom = fileToSaveLoadFrom;

            Health = MaxHealth = maxHealth;
            AmmoInventory = new Ammo(startingAmmoPercentage);
            TotalScrap = totalScrap;
            TotalEnergyCellEnergy = startingEnergyCells * ENERGY_PER_CELL;

            BaseMoveSpeed = 7;

            DashEnergyCost = INITIAL_DASH_ENERGY_USAGE;
            WallClimbCost = INITIAL_WALL_CLIMB_ENERGY_USAGE;
            EnergyDrainPerSecondOfHover = 20;

            playerRef.Respawned += new PlayerMaster.StateChange(OnRespawn);
            playerRef.Climbing += new PlayerMaster.StateChange(OnClimbingChange);
            playerRef.GravityChange += new PlayerMaster.GravityChangeEvent(OnGravityChange);

            JumpModule jump = playerRef.gameObject.AddComponent<JumpModule>();
            MultiJumpModule multiJump = playerRef.gameObject.AddComponent<MultiJumpModule>();
            DashModule dash = playerRef.gameObject.AddComponent<DashModule>();
            HoverModule hover = playerRef.gameObject.AddComponent<HoverModule>();
            HoverRaiseModule hoverRaise = playerRef.gameObject.AddComponent<HoverRaiseModule>();
            WallSlideModule wallSlide = playerRef.gameObject.AddComponent<WallSlideModule>();
            WallGripModule wallGrip = playerRef.gameObject.AddComponent<WallGripModule>();

            ///Default modules
            _modulesPossessed.Add(PlayerDynamicModule.JUMP, jump);
            _modulesPossessed.Add(PlayerDynamicModule.MULTI_JUMP, multiJump);
            _modulesPossessed.Add(PlayerDynamicModule.DASH, dash);
            _modulesPossessed.Add(PlayerDynamicModule.HOVER, hover);
            _modulesPossessed.Add(PlayerDynamicModule.HOVER_RAISE, hoverRaise);
            _modulesPossessed.Add(PlayerDynamicModule.WALL_SLIDE, wallSlide);
            _modulesPossessed.Add(PlayerDynamicModule.WALL_GRIP, wallGrip);

            ChangeModuleActiveState(PlayerDynamicModule.DASH, playerRef.StartWithDash);
            ChangeModuleActiveState(PlayerDynamicModule.WALL_GRIP, playerRef.StartWithWallClimb);
            
        }
        /// <summary>
        /// For Serialization ONLY!!!
        /// </summary>
        public PlayerData()
        {
            BaseMoveSpeed = 7;
            EnergyDrainPerSecondOfHover = 20;
        }
        #endregion

        #region Health Functions
        public void Heal(float amountToHeal)
        {
            if (Health + amountToHeal > MaxHealth)
                Health = MaxHealth;
            else
                Health += amountToHeal;
        }

        /// <summary>
        /// Reduces the player's current health by the specified amount.
        /// </summary>
        /// <param name="amountToTake">The amount of health to remove from the player's total.</param>
        public void TakeHealth(float amountToTake, GlobalData.DamageType type)
        {
            if (Health - amountToTake <= 0)
                Health = 0;
            else
                Health -= amountToTake;
        }

        /// <summary>
        /// Increases the maximum amount of health the player can have by the specified amount.
        /// </summary>
        /// <param name="amountToAdd"></param>
        public void IncreaseMaxHealth(float amountToAdd)
        {
            MaxHealth += amountToAdd;
            Health = MaxHealth;
        }
        #endregion

        #region Scrap Functions
        /// <summary>
        /// Lose a random amount of scrap
        /// </summary>
        public void DropSomeScrap(float MaxPercentageToKeep, float MinPercentageToKeep = .5f)
        {
            if (TotalScrap > 0)
            {
                int newScrapCount = (int)(TotalScrap * UnityEngine.Random.Range(MinPercentageToKeep, MaxPercentageToKeep));
                TotalScrap = Mathf.Max(1, newScrapCount);
            }
        }

        /// <summary>
        /// Add to the players scrap 
        /// </summary>
        /// <param name="amount"></param>
        public void AddScrapAmount(int amount)
        {
            if (amount > 0)
                TotalScrap += amount;
        }

        /// <summary>
        /// Provides the request amount of scrap to heal the player, or as much as is possible.
        /// </summary>
        /// <param name="amountOfScrap">How much scrap the player is requesting.</param>
        /// <returns>An amount of scrap equal to or less than the requested amount of scrap.</returns>
        public int RequestScrap(int amountOfScrap)
        {
            if (TotalScrap >= amountOfScrap)
            {
                TotalScrap -= amountOfScrap;
                return amountOfScrap;
            }
            else
            {
                int remainingScrap = TotalScrap;
                TotalScrap = 0;
                return remainingScrap;
            }
        }

        /// <summary>
        /// tray Spend The amount of Scrap.
        /// </summary>
        /// <param name="amountOfScrap">How much scrap the player is requesting.</param>
        /// <returns>if the player was able to spend the amount true = scrap spent false = could not spend so no scrap lost</returns>
        public bool TryAndSpendScrap(int amountOfScrap)
        {
            if (TotalScrap >= amountOfScrap)
            {
                TotalScrap -= amountOfScrap;
                return true;
            }
            return false;
        }

        #endregion

        #region Energy Functions
        /// <summary>
        /// Checks if the player has another energy cell that can be used for a maneuver.
        /// </summary>
        /// <returns>True if there is another energy cell available, false if not.</returns>
        public bool RequestEnergy(float amount)
        {
            if (_currentUsedEnergyCellEnergy + amount <= TotalEnergyCellEnergy)
            {
                _currentUsedEnergyCellEnergy += amount;
                _lastUsedAmountOfEnergy = amount;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Refunds the last used amount of energy.
        /// </summary>
        public void RefundEnergy()
        {
            if (_currentUsedEnergyCellEnergy - _lastUsedAmountOfEnergy > 0)
            {
                _currentUsedEnergyCellEnergy -= _lastUsedAmountOfEnergy;
            }
            else
            {
                _currentUsedEnergyCellEnergy = 0;
            }
        }

        /// <summary>
        /// Instantly restores the player's energy cells back to max.
        /// </summary>
        public void InstantlyRechargeEnergyCells()
        {
            _currentUsedEnergyCellEnergy = 0;
        }

        /// <summary>
        /// Recharges a portion of the player's energy cells.
        /// </summary>
        /// <param name="energyCellAmount"></param>
        public void RechargeEnergyCells(float energyCellAmount)
        {
            _currentUsedEnergyCellEnergy = _currentUsedEnergyCellEnergy - energyCellAmount > 0 ? _currentUsedEnergyCellEnergy -= energyCellAmount : _currentUsedEnergyCellEnergy = 0;
        }

        /// <summary>
        /// Adds 100 energy (the amount of a single cell) to the player's energy total.
        /// </summary>
        public void AddEnergyCell(bool saveAdditionToSaveFile = true)
        {
            TotalEnergyCellEnergy += ENERGY_PER_CELL;

            if (saveAdditionToSaveFile)
                ES3.Save<float>("Total Energy", TotalEnergyCellEnergy, _fileToSaveLoadFrom);
        }

        public void SetEnergyCells(int numCellsToAdd)
        {
            TotalEnergyCellEnergy = ENERGY_PER_CELL * numCellsToAdd;
        }
        #endregion

        #region Module Functions
        public void AddNewModule(string moduleName, PlayerUpgradeModule newModule)
        {
            if (!_modulesPossessed.ContainsKey(moduleName))
            {
                _modulesPossessed.Add(moduleName, newModule);
                GlobalData.Player.Movement.AttemptRegisterPlayerModule(moduleName);
                ES3.Save<bool>(moduleName, true, _fileToSaveLoadFrom);
            }
        }

        public void AddNewModule(string newModule)
        {
            PlayerUpgradeModule module = null;
            switch (newModule)
            {
                case PlayerDynamicModule.JUMP:
                    module = _playerRef.gameObject.AddComponent<JumpModule>();
                    break;
                case PlayerDynamicModule.MULTI_JUMP:
                    module = _playerRef.gameObject.AddComponent<MultiJumpModule>();
                    break;
                case PlayerDynamicModule.DASH:
                    module = _playerRef.gameObject.AddComponent<DashModule>();
                    break;
                case PlayerDynamicModule.HOVER:
                    module = _playerRef.gameObject.AddComponent<HoverModule>();
                    break;
                case PlayerDynamicModule.HOVER_RAISE:
                    module = _playerRef.gameObject.AddComponent<HoverRaiseModule>();
                    break;
                case PlayerDynamicModule.WALL_SLIDE:
                    module = _playerRef.gameObject.AddComponent<WallSlideModule>();
                    break;
                case PlayerDynamicModule.WALL_GRIP:
                    module = _playerRef.gameObject.AddComponent<WallGripModule>();
                    break;
            }

            if (module != null)
            {
                AddNewModule(newModule, module);
            }
            else
            {
                Debug.LogError("Tried to add non-existent upgrade module\n" + System.Environment.StackTrace);
            }
        }

        public PlayerUpgradeModule GetModuleIfPossessed(string moduleName)
        {
            if (_modulesPossessed.ContainsKey(moduleName))
            {
                return _modulesPossessed[moduleName];
            }

            return null;
        }

        public void ChangeModuleActiveState(string moduleName, bool newActiveState)
        {
            if (_modulesPossessed.ContainsKey(moduleName))
            {
                _modulesPossessed[moduleName].enabled = newActiveState;
            }
        }

        public bool GetModuleActiveState(string moduleName)
        {
            if (_modulesPossessed.ContainsKey(moduleName))
            {
                return _modulesPossessed[moduleName].enabled;
            }

            Debug.LogError("Tried to check the active state of a non-existent module\n" + System.Environment.StackTrace);
            return false;
        }

        public void ChangeMultipleModulesActiveState(bool newActiveState, params string[] moduleNames)
        {
            for (int i = 0; i < moduleNames.Length; ++i)
            {
                _modulesPossessed[moduleNames[i]].enabled = newActiveState;
            }
        }

        public void ChangeAllModulesActiveState(bool newActiveState)
        {
            foreach (PlayerUpgradeModule module in _modulesPossessed.Values)
            {
                module.enabled = newActiveState;
            }
        }

        public void WriteModuleToSaveFile(string moduleName, bool initialModuleState)
        {
            if (!ES3.KeyExists(moduleName, _fileToSaveLoadFrom))
            {
                ES3.Save<bool>(moduleName, initialModuleState, _fileToSaveLoadFrom);
                ES3.Save<bool>(moduleName + " enabled", initialModuleState, _fileToSaveLoadFrom);
            }
        }

        public bool CheckModuleEnabledStatusInSaveFile(string moduleName)
        {
            if (ES3.KeyExists(moduleName, _fileToSaveLoadFrom))
            {
                return ES3.Load<bool>(moduleName + " enabled", _fileToSaveLoadFrom);
            }
            return false;
        }
        #endregion

        #region Callbacks
        private void OnRespawn(bool respawn)
        {
            DropSomeScrap(0.75f);
            Health = MaxHealth;
        }

        private void OnClimbingChange(bool climbing)
        {
            if (climbing)
            {
                InstantlyRechargeEnergyCells();
            }
        }

        private void OnGravityChange(GlobalData.GravityType type)
        {
            InstantlyRechargeEnergyCells();
        }
        #endregion

        public void Save()
        {
            ES3.Save<int>("Total Scrap", TotalScrap, _fileToSaveLoadFrom);
            ES3.Save<float[]>("Ammo Percentages", AmmoInventory.AmmoPercentageAmounts, _fileToSaveLoadFrom);
            ES3.Save<bool>(PlayerDynamicModule.DASH + " enabled", GetModuleActiveState(PlayerDynamicModule.DASH), _fileToSaveLoadFrom);
            ES3.Save<bool>(PlayerDynamicModule.WALL_GRIP + " enabled", GetModuleActiveState(PlayerDynamicModule.WALL_GRIP), _fileToSaveLoadFrom);
        }
    }
}
