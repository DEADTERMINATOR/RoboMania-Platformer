﻿using Inputs;
using LevelComponents.Platforms;
using MovementEffects;
using UnityEngine;
using Characters.Player.Modules;
using static GlobalData;
using static MoveableObject;

namespace Characters.Player
{
    public class PlayerMovementController
    {
        #region Enums
        public enum CollisionCheck { Both = 'B', Horizontal = 'X', Vertical = 'Y', None = 'N' }
        #endregion

        #region Public Properties
        /// <summary>
        /// The velocity that will be applied on a first normal jump.
        /// </summary>
        public float JumpVelocity { get; private set; }

        /// <summary>
        /// Public getter for the player's current gravity velocity value.
        /// </summary>
        public Vector2 Gravity { get { return _currentActiveGravityState.GravityVelocity; } }

        public Vector2 Knockback { get { return _currentNonDamageKnockback; } }

        public Vector2 DamageKnockback { get { return _currentDamageKnockback; } }

        /// <summary>
        /// Determines which axes will be checked during collision.
        /// </summary>
        public CollisionCheck CurrentCollisionCheck = CollisionCheck.Both;

        public float BaseMoveSpeed { get { return _currentActiveGravityState.BaseMoveSpeed; } }

        public PlayerMaster.PlayerGravityState CurrentActiveGravityState { get { return _currentActiveGravityState; } }
        #endregion

        #region Private Variables
        /// <summary>
        /// The amount of knockback remaining to apply to the player that came from a non-damaging source.
        /// </summary>
        private Vector2 _currentNonDamageKnockback;

        /// <summary>
        /// The total amount of knockback that is to be applied to the player from a non-damaging souce.
        /// Used to calculate the percentage of non-damaging knockback that has yet to be applied so we can
        /// figure out when the player's input should be restored.
        /// </summary>
        private Vector2 _totalNonDamageKnockback;

        /// <summary>
        /// The amount of knockback remaining to apply to the player that came from a damaging source.
        /// </summary>
        private Vector2 _currentDamageKnockback;

        /// <summary>
        /// Whether the player has left the ground following the application of some knockback.
        /// </summary>
        private bool _playerOffGroundAfterKnockback;

        /// <summary>
        /// Holds the current horizontal velocity that specifically attributable to player input.
        /// </summary>
        private float _xInputVelocity = 0;

        /// <summary>
        /// Holds the current horizontal velocity of the smoothing function.
        /// </summary>
        private float _xVelocitySmoothing;

        /// <summary>
        /// Whether the player has used their first jump (i.e. the one that doesn't require an energy cell).
        /// </summary>
        private bool _hasUsedFirstJump;

        /// <summary>
        /// The remaining amount of time the player has to wait before performing another dash.
        /// </summary>
        private float _remainingDashTimeout = 0;

        /// <summary>
        /// Reference to the player master stored locally due to frequent use.
        /// </summary>
        private PlayerMaster _playerRef;

        /// <summary>
        /// Reference to the player state stored locally due to frequent use.
        /// </summary>
        private PlayerState _playerState;

        /// <summary>
        /// Holds the gravity state the player is currently experiencing.
        /// </summary>
        private PlayerMaster.PlayerGravityState _currentActiveGravityState;

        /// <summary>
        /// When the player jumps (double jumps not included here), a raycast is fired below the player to determine if they are trying to jump on a Bouncy platform.
        /// To determine if the platform is a bouncy platform, we get the platform's PlatformType component.
        /// But if the player is trying to jump on the same platform over and over, checking over and over and wasteful.
        /// Hence, we cache the last platform we detected to compare against before we even try a GetComponent.
        /// </summary>
        private PlatformType _lastDetectedPlatformOnJump;

        /// <summary>
        /// Same logic as above, except for cases when there is a bouncy platform beneath the player, and they are trying to bounce on it repeatedly.
        /// </summary>
        private BouncyPlatform _lastDetectedBouncyPlatform;

        private JumpModule _jump;
        private MultiJumpModule _multiJump;
        private DashModule _dash;
        private HoverModule _hover;
        private HoverRaiseModule _hoverRaise;
        private WallSlideModule _wallSlide;
        #endregion

        public PlayerMovementController(PlayerMaster playerRef)
        {
            _playerRef = playerRef;
            _playerState = _playerRef.State;

            _currentActiveGravityState = playerRef.GravityStates[GravityType.Base];
            JumpVelocity = _currentActiveGravityState.JumpVelocity;

            AttemptRegisterPlayerModule(PlayerDynamicModules.JUMP);
            AttemptRegisterPlayerModule(PlayerDynamicModules.MULTI_JUMP);
            AttemptRegisterPlayerModule(PlayerDynamicModules.DASH);
            AttemptRegisterPlayerModule(PlayerDynamicModules.HOVER);
            AttemptRegisterPlayerModule(PlayerDynamicModules.HOVER_RAISE);

            bool playerHasGrip = AttemptRegisterPlayerModule(PlayerDynamicModules.WALL_GRIP);
            if (!playerHasGrip)
            {
                AttemptRegisterPlayerModule(PlayerDynamicModules.WALL_SLIDE);
            }

            playerRef.MoveSpeed = _currentActiveGravityState.BaseMoveSpeed;

            playerRef.Respawned += OnRespawn;
            playerRef.Stunned += OnStunned;
            playerRef.OnRepair += OnRepair;
            playerRef.Hovering += OnHoverStateChanged;
            playerRef.Climbing += OnClimbingStateChanged;
            playerRef.GravityChange += OnGravityChange;
            playerRef.Dead += OnDead;
            playerRef.WallGripping += OnWallGrip;

            playerRef.EnteringShip += OnEnteringShip;
            playerRef.DashPerformed += OnDashPerformed;

            playerRef.CollisionBelow += OnCollisionBelow;

            InputManager instance = InputManager.Instance;

            instance.RegisterSpecificActionStarted("Dash", () =>
            {
                if (_dash != null && _dash.enabled && !playerRef.Input.BlockInput && !playerRef.Input.BlockSpecialInput && _remainingDashTimeout <= 0 && !_playerState.Climbing)
                {
                    _playerRef.State.SetDashing(_dash.StartModule());
                }
            });

            instance.RegisterSpecificActionStarted("Hover", () =>
            {
                if (!playerRef.Input.BlockInput && !playerRef.Input.BlockSpecialInput && !playerRef.Collisions.IsCollidingBelow && !playerRef.State.InsideAirGust && !playerRef.State.Climbing)
                {
                    if (_hover != null && _hover.enabled)
                    {
                        _hover.StartModule();
                    }

                    _playerRef.NotifyEnergyCellMoveUsed();
                }
            });
            instance.RegisterSpecificActionCancelled("Hover", () =>
            {
                if (_hover != null && _hover.enabled && _playerState.Hovering)
                {
                    _hover.FinishModule();
                }
            });
        }

        #region Update
        public void MovementUpdate()
        {
            var currentPlayerPlatformVelocity = _playerRef.GetVelocityComponentVelocity(PLATFORM_VELOCITY_COMPONENT);
            if (_playerRef.Collisions.IsCollidingBelow)
            {
                var collidingBelowIsPlatform = _playerRef.Collisions.Below.CollidingObject.GetComponent<Platform>();
                if (!collidingBelowIsPlatform && currentPlayerPlatformVelocity != Vector2.zero)
                {
                    currentPlayerPlatformVelocity = Vector2.zero;
                    _playerRef.SetVelocity(currentPlayerPlatformVelocity, PLATFORM_VELOCITY_COMPONENT);
                }
            }

            //The player is now free failing after some type of up movement with non standard gravity
            if (!_playerState.Dashing && !_playerState.Hovering && !_playerState.WallGripping && !_playerState.Climbing && _playerRef.GravityScale != _currentActiveGravityState.BaseGravityScale && !_playerRef.Collisions.IsCollidingBelow && _playerRef.Velocity.y <= 0)
            {
                _playerRef.GravityScale = _currentActiveGravityState.BaseGravityScale;
            }

            _remainingDashTimeout -= Time.deltaTime;

            if (!_playerRef.Input.BlockInput) //We don't want to accept player movement input if they are in the middle of a full input lock.
            {
                float targetXVelocity = _playerRef.MovementInput.x * _playerRef.MoveSpeed;

                if (_playerState.Climbing) //Handle movement if the player is climbing on something.
                {
                    HandleClimbing(new Vector2(0, _playerRef.MovementInput.y * PlayerConstants.CLIMBING_SPEED_MULTIPLIER));

                    if (_playerState.CurrentActiveClimbableObject?.TravelDirection == ClimbableObject.TravelDirections.Horizontal && _playerRef.MovementInput.y < 0)
                    {
                        _playerState.SetClimbing(false, null);
                    }

                    return; //Do not run the rest of the code;
                }

                if (_playerRef.Collisions.IsCollidingBelow)
                {
                    if ((_currentNonDamageKnockback.x != 0 || _currentNonDamageKnockback.y != 0 || _currentDamageKnockback.x != 0 || _currentDamageKnockback.y != 0) && _playerOffGroundAfterKnockback)
                    {
                        _currentNonDamageKnockback = Vector2.zero;
                        _currentDamageKnockback = Vector2.zero;

                        _playerOffGroundAfterKnockback = false;
                    }
                }
                else
                {
                    _playerState.SetGrabbing(false);
                    if (_currentNonDamageKnockback.x != 0 || _currentNonDamageKnockback.y != 0 || _currentDamageKnockback.x != 0 || _currentDamageKnockback.y != 0)
                    {
                        _playerOffGroundAfterKnockback = true;
                    }
                }

                PlayerController2D.PlayerCollisionInfo castedCollisionInfo = (PlayerController2D.PlayerCollisionInfo)_playerRef.Collisions;
                if (castedCollisionInfo.PercentageCollidingHorizontal >= PlayerConstants.MINIMUM_HORIZONTAL_COLLISION_PERCENTAGE_FOR_STOPPING_KNOCKBACK || castedCollisionInfo.IsCollidingAbove)
                {
                    //The zero-ing out of the knockback velocities here is intended to handled the case where the knockback throws the player into a wall or ceiling.
                    _currentDamageKnockback = Vector2.zero;
                    _currentNonDamageKnockback = Vector2.zero;
                    _playerRef.Input.BlockInput = false;
                }

                //Handle wall sliding if the player isn't hovering.
                if (!_playerState.Hovering)
                {
                    if (_wallSlide != null && _wallSlide.enabled && _currentActiveGravityState.WallSlidingAllowed) //Added a check for if the module is enabled 
                    {
                        if (!_playerState.WallSliding)
                        {
                            //We don't need to account for whether the player possesses the wall grip upgrade here because there is no difference in how the two start their modules.
                            //We do need to account for a disabled module.
                            _wallSlide.StartModule();
                        }
                        else
                        {
                            //If the player possesses the wall grip upgrade, call continue on that module instead of the wall slide module as wall grip encompasses the logic of both the grip and slide.
                            var wallGrip = _wallSlide as WallGripModule;
                            if (wallGrip != null)
                            {
                                wallGrip.ContinueModule();
                            }
                            else
                            {
                                _wallSlide.ContinueModule();
                            }

                            _xVelocitySmoothing = 0;
                        }
                    }

                    if (!_playerState.Dashing && !_playerState.WallSliding && !_playerState.WallJumpingOffWall)
                    {
                        //Apply X-input movement.
                        _xInputVelocity = Mathf.Clamp(Mathf.SmoothDamp(_xInputVelocity, targetXVelocity, ref _xVelocitySmoothing,
                            _playerRef.Collisions.IsCollidingBelow ? PlayerConstants.ACCELERATION_TIME_GROUNDED : PlayerConstants.ACCELERATION_TIME_AIRBORNE), -_playerRef.MoveSpeed, _playerRef.MoveSpeed);

                        var currentPlayerVelocity = _playerRef.GetVelocityComponentVelocity(OBJECT_VELOCITY_COMPONENT);
                        _playerRef.SetVelocity(new Vector2(_xInputVelocity, currentPlayerVelocity.y), OBJECT_VELOCITY_COMPONENT);
                    }

                    if (!_playerRef.Input.BlockSpecialInput)
                    {
                        if (_playerRef.Input.InputDictionary["Jump"].GetTheState() == PlayerInput.ButtonState.State.DOWN)
                        {
                            if (!_playerState.WallSliding)
                            {
                                bool didJump = false;
                                if (_jump != null && _jump.enabled)
                                {
                                    _jump.BouncyPlatformUnderneath = IsBouncyPlatformBeneath();

                                    if (!_hasUsedFirstJump || _jump.BouncyPlatformUnderneath != null || _playerState.Bouncing)
                                    {
                                        bool success = _jump.StartModule();

                                        if (success)
                                        {
                                            _hasUsedFirstJump = true;
                                            didJump = true;
                                        }
                                    }
                                }

                                if (!didJump)
                                {
                                    
                                    var playerController = (PlayerController2D)_playerRef.Controller;
                                    if (_wallSlide != null && _wallSlide.enabled && castedCollisionInfo.IsGivenRayCollidingHorizontally(playerController.Raycaster.HorizontalRayCount / 2, playerController.Raycaster, PlayerConstants.CHECK_FOR_WALL_SLIDE_DISTANCE))
                                    {
                                        //The player is close enough to a wall that we can just consider this a wall climb jump, provided the player has the wall climb ability.
                                        _wallSlide.WallJump(true, -_playerRef.Collisions.FaceDir);
                                    }
                                    else if (_multiJump != null && _multiJump.enabled)
                                    {
                                        _multiJump.StartModule();
                                    }
                                }
                            }
                            else
                            {
                                _wallSlide.WallJump();
                            }
                        }
                    }
                }
            }

            if (_playerRef.Input.InputDictionary["Jump"].GetTheState() == PlayerInput.ButtonState.State.RELEASED && !_playerState.Bumped && !_playerState.Bouncing)
            {
                var currentPlayerVelocity = _playerRef.GetVelocityComponentVelocity(OBJECT_VELOCITY_COMPONENT);
                if (currentPlayerVelocity.y > JumpVelocity / 2)
                {
                    currentPlayerVelocity.y = JumpVelocity / 2;
                    _playerRef.SetVelocity(currentPlayerVelocity, OBJECT_VELOCITY_COMPONENT);
                }
            }
        }

        public void FixedMovementUpdate()
        {
            var currentPlayerVelocity = _playerRef.GetVelocityComponentVelocity(OBJECT_VELOCITY_COMPONENT);
            var damageKnockbackThisFrame = Vector2.zero;
            var regularKnockbackThisFrame = Vector2.zero;

            if (DamageKnockback.x != 0 || DamageKnockback.y != 0)
            {
                damageKnockbackThisFrame = HandleDamageKnockback(currentPlayerVelocity);
            }
            if (Knockback.x != 0 || Knockback.y != 0)
            {
                regularKnockbackThisFrame = HandleRegularKnockback();
            }

            if (_playerRef.State.Hovering)
            {
                if (_playerRef.Input.InputDictionary["Raise"].IsDownOrHeld)
                {
                    bool canRaise = _hoverRaise.StartModule(); //Start and continue module function the same in this case.
                    if (!canRaise)
                    {
                        _hover.FinishModule(); //Hover raise already calls finish if energy runs out, so we only need to finish the base hover.
                    }
                }
                else
                {
                    _hoverRaise.FinishModule();
                    _hover.ContinueModule();
                }
            }
    
            if (!(_playerRef.RegisteredPlatform != null && _playerRef.RegisteredPlatform.CurrentVelocityDirection.y > 0) || _hasUsedFirstJump)
            {
                //If this condition is true, then we got into this conditional via the player jumping. If so, we want to deregister them from an upward moving platform so that their object Y-velocity isn't zeroed out
                //(which is what happens when a player is standing on an upward moving platform).
                if (_playerRef.RegisteredPlatform != null && _playerRef.RegisteredPlatform.CurrentVelocityDirection.y > 0)
                {
                    _playerRef.RegisteredPlatform.DeregisterPassenger(_playerRef);
                }

                //Apply gravity to the Y-velocity.
                _playerRef.AddToVelocity(Gravity * _playerRef.GravityScale, OBJECT_VELOCITY_COMPONENT);
            }
            

            if (currentPlayerVelocity.y < _currentActiveGravityState.MaximumDownwardYVelocity)
            {
                currentPlayerVelocity.y = _currentActiveGravityState.MaximumDownwardYVelocity;
                _playerRef.SetVelocity(currentPlayerVelocity, OBJECT_VELOCITY_COMPONENT);
            }

            if (_playerRef.State.Dashing)
            {
                bool dashFinished = _dash.ContinueModule();

                if (dashFinished && _playerRef.Collisions.IsCollidingBelow)
                {
                    _remainingDashTimeout = PlayerConstants.DASH_TIMEOUT;
                }
            }

            var finalVelocity = _playerRef.CalculateFrameVelocity();

            finalVelocity += (Vector3)damageKnockbackThisFrame;
            finalVelocity += (Vector3)regularKnockbackThisFrame;

            if (StaticTools.ThresholdApproximately(finalVelocity.x, 0, 0.01f))
            {
                finalVelocity.x = 0;
            }

            //Check if player is registered with a platform. If so, don't eject on the Y.
            _playerRef.Controller.Move(ref finalVelocity, (char)CurrentCollisionCheck, 'B', false, _playerRef.MoveCharacterInLocalSpace == true ? Space.Self : Space.World);

            //We need the movement to occur before we check if we are still on the ground (otherwise the ground check cancels any jump movement), hence the repetition of the Dead check here.
            if (!_playerRef.State.Dead && (_playerRef.Collisions.IsCollidingBelow || _playerRef.Collisions.IsCollidingAbove))
            {
                _playerRef.ZeroOutVelocityOnOneAxis('Y'); //Zero out y velocity when we collide either from above or below to prevent a buildup.
            }
        }
        #endregion

        #region Gravity
        /// <summary>
        /// Recalculates the player's gravity and jump velocity given the current gravity type they are experiencing.
        /// Also recalculates the player's minimum jump height.
        /// </summary>
        /// <param name="gravityType">The type of gravity the player is experiencing.</param>
        public void ChangeGravityType(GravityType gravityType)
        {
            switch (gravityType)
            {
                case GravityType.Base:
                    _currentActiveGravityState = _playerRef.GravityStates[GravityType.Base];
                    break;
                case GravityType.Lava:
                    _currentActiveGravityState = _playerRef.GravityStates[GravityType.Lava];
                    break;
                case GravityType.Mud:
                    _currentActiveGravityState = _playerRef.GravityStates[GravityType.Mud];
                    break;
            }

            JumpVelocity = _currentActiveGravityState.JumpVelocity;

            var currentPlayerVelocity = _playerRef.GetVelocityComponentVelocity(OBJECT_VELOCITY_COMPONENT);
            if (currentPlayerVelocity.y < _currentActiveGravityState.GravityReductionValue)
            {
                currentPlayerVelocity.y = _currentActiveGravityState.GravityReductionValue;
                _playerRef.SetVelocity(currentPlayerVelocity, OBJECT_VELOCITY_COMPONENT);
            }

            _playerRef.NotifyGravityChange(gravityType);
        }
        #endregion

        #region Knockback
        /// <summary>
        /// Calculates and applies the correct amount of existing damage knockback for the frame.
        /// </summary>
        /// <returns>A Vector2 representing the damage knockback for the frame.</returns>
        private Vector2 HandleDamageKnockback(Vector2 currentPlayerVelocity)
        {
            Vector2 damageKnockbackThisFrame = DamageKnockback * Time.fixedDeltaTime;

            _currentDamageKnockback.x -= damageKnockbackThisFrame.x;
            _currentDamageKnockback.y -= damageKnockbackThisFrame.y;

            if (_playerOffGroundAfterKnockback && currentPlayerVelocity.y <= 0)
            {
                damageKnockbackThisFrame.y = 0;
                _currentDamageKnockback.y = 0;
            }

            return damageKnockbackThisFrame;
        }

        /// <summary>
        /// Calculates and applies the correct amount of existing regular knockback for the frame.
        /// </summary>
        /// <returns>A Vector2 representing the regular knockback for the frame.</returns>
        private Vector2 HandleRegularKnockback()
        {
            if (Mathf.Abs(_currentNonDamageKnockback.x) > 0)
            {
                Vector2 regularKnockbackThisFrame = _currentNonDamageKnockback * Time.fixedDeltaTime;

                float percentageOfKnockbackRemaining = (Mathf.Abs(_currentNonDamageKnockback.x) + Mathf.Abs(_currentNonDamageKnockback.y)) / (Mathf.Abs(_totalNonDamageKnockback.x) + Mathf.Abs(_totalNonDamageKnockback.y));
                if (percentageOfKnockbackRemaining <= PlayerConstants.PERCENTAGE_OF_KNOCKBACK_REMAINING_BEFORE_INPUT_LOCK_REMOVAL && !_playerRef.State.Dead)
                {
                    _playerRef.Input.BlockInput = false;
                }

                _currentNonDamageKnockback -= regularKnockbackThisFrame;
                return regularKnockbackThisFrame;
            }
            else
            {
                _totalNonDamageKnockback = _currentNonDamageKnockback = Vector2.zero;

                if (!_playerRef.State.Dead)
                {
                    _playerRef.Input.BlockInput = false;
                }

                return Vector2.zero;
            }
        }

        /// <summary>
        /// Sets the damage knockback to be applied the player's velocity. Override of the base set damage knockback function for player specific functions.
        /// </summary>
        /// <param name="knockback">The direction and magnitude of the knockback.</param>
        public void SetDamageKnockback(Vector2 knockback)
        {
            _playerRef.ZeroOutAllVelocities();
            _currentDamageKnockback = knockback;

            _hasUsedFirstJump = true;
            _playerRef.GravityScale = _currentActiveGravityState.BaseGravityScale;

            _playerRef.State.SetClimbing(false, null);

            _playerRef.Input.BlockInput = true;
            GlobalData.Timekeeper.StartTimer(() => { if (!_playerRef.State.Dead) _playerRef.Input.BlockInput = false; }, PlayerConstants.DAMAGE_INVULNERABILITY_TIME / 4);
            _playerRef.NotifyKnockbackReceived(knockback);
        }

        /// <summary>
        /// Sets the non-damage based knockback to be applied the player's velocity. Override of the base set knockback function.
        /// </summary>
        /// <param name="knockback">The direction and magnitude of the knockback.</param>
        public void SetNonDamageKnockback(Vector2 knockback)
        {
            _playerRef.ZeroOutAllVelocities();
            _totalNonDamageKnockback = _currentNonDamageKnockback = knockback;

            _hasUsedFirstJump = true;
            _playerRef.GravityScale = _currentActiveGravityState.BaseGravityScale;

            _playerRef.State.SetClimbing(false, null);

            _playerRef.Input.BlockInput = true;
            _playerRef.NotifyKnockbackReceived(knockback);
        }

        /// <summary>
        /// Adds to the non-damage based knockback to be applied to the player's velocity. Override of the base add knockback function.
        /// </summary>
        /// <param name="knockback"></param>
        public void AddNonDamageKnockback(Vector2 knockback)
        {
            _playerRef.ZeroOutAllVelocities();
            _currentNonDamageKnockback += knockback;

            _hasUsedFirstJump = true;
            _playerRef.Input.BlockInput = true;
            _playerRef.NotifyKnockbackReceived(_currentNonDamageKnockback);
        }
        #endregion

        #region Velocity Manipulation
        /// <summary>
        /// Sets the velocity along the provided axis by the provided value.
        /// </summary>
        /// <param name="axis">The axis to set the velocity along.</param>
        /// <param name="axisVelocity">The value to be set as the velocity along the provided axis.</param>
        public void SetVelocity(char axis, float axisVelocity)
        {
            var currentPlayerVelocity = _playerRef.GetVelocityComponentVelocity(OBJECT_VELOCITY_COMPONENT);
            switch (axis)
            {
                case 'X':
                    currentPlayerVelocity.x = axisVelocity;
                    break;
                case 'Y':
                    currentPlayerVelocity.y = axisVelocity;
                    break;
            }
            _playerRef.SetVelocity(currentPlayerVelocity, OBJECT_VELOCITY_COMPONENT);
        }

        /// <summary>
        /// Stops all player movement, blocks input and sets the player's animation to idle.
        /// </summary>
        public void Stop()
        {
            _playerRef.ZeroOutAllVelocities();
            _playerRef.Input.BlockInput = true;
        }

        /// <summary>
        /// Re-enables to player abilty to move
        /// </summary>
        public void Restart()
        {
            _playerRef.ZeroOutAllVelocities();
            _playerRef.Input.BlockInput = false;
        }
        #endregion

        #region Helper Methods
        private BouncyPlatform IsBouncyPlatformBeneath()
        {
            Ray2D rayBeneath = new Ray2D(new Vector2(_playerRef.transform.position.x, _playerRef.transform.position.y), Vector2.down);
            RaycastHit2D hit = CollisionRaycaster2D.RaycastBeneath(new Vector2(_playerRef.transform.position.x, _playerRef.transform.position.y), 0.5f, OBSTACLE_LAYER_SHIFTED);

            if (hit)
            {
                if (_lastDetectedPlatformOnJump != null)
                {
                    if (!GameObject.ReferenceEquals(_lastDetectedPlatformOnJump.gameObject, hit.collider.gameObject))
                    {
                        _lastDetectedPlatformOnJump = hit.collider.GetComponent<PlatformType>();
                        if (_lastDetectedPlatformOnJump != null && _lastDetectedPlatformOnJump.Type == PlatformType.PlatformTypes.Bouncy)
                        {
                            _lastDetectedBouncyPlatform = hit.collider.GetComponent<BouncyPlatform>();
                            return _lastDetectedBouncyPlatform;
                        }
                    }
                    else
                    {
                        if (_lastDetectedPlatformOnJump.Type == PlatformType.PlatformTypes.Bouncy)
                        {
                            return _lastDetectedBouncyPlatform;
                        }
                    }
                }
                else
                {
                    _lastDetectedPlatformOnJump = hit.collider.GetComponent<PlatformType>();
                }
            }

            return null;
        }

        private void HandleClimbing(Vector2 velocity)
        {
            if (!_playerRef.State.ShouldFollowClimbingPath)
            {
                _playerRef.SetVelocity(velocity, OBJECT_VELOCITY_COMPONENT);
            }
            else
            {
                char axis = _playerRef.State.CurrentActiveClimbableObject.TravelDirection == ClimbableObject.TravelDirections.Horizontal ? 'X' : 'Y';
                if ((axis == 'Y' && _playerRef.MovementInput.y != 0) || (axis == 'X' && _playerRef.MovementInput.x != 0))
                {
                    Vector2 nextTravelDirection = (_playerRef.State.NextClimbingPathTarget - (Vector2)_playerRef.TopOfPlayerTransform).normalized;
                    Vector2 previousTravelDirection = (_playerRef.State.PreviousClimbingPathTarget - (Vector2)_playerRef.TopOfPlayerTransform).normalized;
                    float playerMovementSign = axis == 'X' ? Mathf.Sign(_playerRef.MovementInput.x) : Mathf.Sign(_playerRef.MovementInput.y);

                    bool movingToNextPoint = false;
                    bool movingToPreviousPoint = false;

                    switch (axis)
                    {
                        case 'X':
                            if (playerMovementSign == Mathf.Sign(nextTravelDirection.x))
                            {
                                movingToNextPoint = true;
                            }
                            else if (playerMovementSign == Mathf.Sign(previousTravelDirection.x))
                            {
                                movingToPreviousPoint = true;
                            }
                            break;
                        case 'Y':
                            if (playerMovementSign == Mathf.Sign(nextTravelDirection.y))
                            {
                                movingToNextPoint = true;
                            }
                            else if (playerMovementSign == Mathf.Sign(previousTravelDirection.y))
                            {
                                movingToPreviousPoint = true;
                            }
                            break;
                    }

                    if (movingToNextPoint)
                    {
                        _playerRef.SetVelocity(PlayerConstants.CLIMBING_SPEED_MULTIPLIER * nextTravelDirection, OBJECT_VELOCITY_COMPONENT);
                    }
                    else if (movingToPreviousPoint)
                    {
                        _playerRef.SetVelocity(PlayerConstants.CLIMBING_SPEED_MULTIPLIER * previousTravelDirection, OBJECT_VELOCITY_COMPONENT);
                    }
                }
                else
                {
                    _playerRef.ZeroOutVelocity(OBJECT_VELOCITY_COMPONENT);
                }
                
            }

            if (_jump != null && !_playerRef.Input.BlockSpecialInput && _playerRef.Input.InputDictionary["Jump"].GetTheState() == PlayerInput.ButtonState.State.DOWN)
            {
                _jump.StartModule();
            }
        }
        #endregion

        public bool AttemptRegisterPlayerModule(string newModule)
        {
            switch (newModule)
            {
                case PlayerDynamicModules.JUMP:
                    _jump = (JumpModule)_playerRef.Data.GetModuleIfPossessed(PlayerDynamicModules.JUMP);
                    if (_jump != null)
                    {
                        return true;
                    }
                    break;
                case PlayerDynamicModules.MULTI_JUMP:
                    _multiJump = (MultiJumpModule)_playerRef.Data.GetModuleIfPossessed(PlayerDynamicModules.MULTI_JUMP);
                    if (_multiJump != null)
                    {
                        return true;
                    }
                    break;
                case PlayerDynamicModules.DASH:
                    _dash = (DashModule)_playerRef.Data.GetModuleIfPossessed(PlayerDynamicModules.DASH);
                    if (_dash != null)
                    {
                        return true;
                    }
                    break;
                case PlayerDynamicModules.HOVER:
                    _hover = (HoverModule)_playerRef.Data.GetModuleIfPossessed(PlayerDynamicModules.HOVER);
                    if (_hover != null)
                    {
                        return true;
                    }
                    break;
                case PlayerDynamicModules.HOVER_RAISE:
                    _hoverRaise = (HoverRaiseModule)_playerRef.Data.GetModuleIfPossessed(PlayerDynamicModules.HOVER_RAISE);
                    if (_hoverRaise != null)
                    {
                        return true;
                    }
                    break;
                case PlayerDynamicModules.WALL_SLIDE:
                    _wallSlide = (WallSlideModule)_playerRef.Data.GetModuleIfPossessed(PlayerDynamicModules.WALL_SLIDE);
                    if (_wallSlide != null)
                    {
                        return true;
                    }
                    break;
                case PlayerDynamicModules.WALL_GRIP:
                    _wallSlide = (WallGripModule)_playerRef.Data.GetModuleIfPossessed(PlayerDynamicModules.WALL_GRIP);
                    if (_wallSlide != null)
                    {
                        return true;
                    }
                    break;
            }

            return false;
        }

        #region Callbacks
        private void OnDead()
        {
            _playerRef.ZeroOutAllVelocities();
        }

        private void OnRespawn(bool positive)
        {
            _playerRef.GravityScale = _currentActiveGravityState.BaseGravityScale;
            ChangeGravityType(GravityType.Base);
        }

        private void OnStunned(bool positive)
        {
            _playerRef.ZeroOutAllVelocities();
        }

        private void OnHoverStateChanged(bool hoverStarted)
        {
            if (hoverStarted)
            {
                _currentNonDamageKnockback = Vector2.zero;
                _currentDamageKnockback = Vector2.zero;
            }
        }

        private void OnDashPerformed()
        {
            if (_playerRef.State.Hovering)
            {
                _hover.FinishModule();
            }
        }

        private void OnClimbingStateChanged(bool climbing)
        {
            if (climbing)
            {
                if (_playerState.Hovering)
                {
                    _hover.FinishModule();
                }

                _playerRef.GravityScale = 0;
                _playerRef.ZeroOutAllVelocities();

                _hasUsedFirstJump = false;
                _playerRef.MoveCharacterInLocalSpace = true;
            }
            else
            {
                _playerRef.GravityScale = _currentActiveGravityState.BaseGravityScale;
                _playerRef.MoveCharacterInLocalSpace = false;
            }
        }

        private void OnGravityChange(GlobalData.GravityType type)
        {
            if (type != GlobalData.GravityType.Base)
            {
                _currentNonDamageKnockback = Vector2.zero;
                _currentDamageKnockback = Vector2.zero;
            }

            _hasUsedFirstJump = false;
        }

        private void OnRepair(bool positive)
        {
            if (positive)
            {
                _playerRef.ZeroOutVelocity(OBJECT_VELOCITY_COMPONENT);
            }
        }

        private void OnEnteringShip()
        {
            _playerRef.ZeroOutAllVelocities();
        }

        private void OnCollisionBelow(Vector2 velocity)
        {
            _hasUsedFirstJump = false;
        }

        private void OnWallGrip(bool positive)
        {
            if (positive)
            {
                _playerRef.GravityScale = 0;
            }
            else
            {
                _playerRef.GravityScale = _currentActiveGravityState.BaseGravityScale;
            }
        }
        #endregion   
    }
}
