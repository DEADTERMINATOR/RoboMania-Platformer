﻿using System.Collections;
using UnityEngine;
using MovementEffects;
using Spine.Unity;

namespace Characters.Player
{
    public abstract class PlayerTutorial : MonoBehaviour, IActivatable
    {
        public bool Active { get { return _tutorialStarted; } }

        protected enum MovementType { Distance, Position }

        protected delegate void MoveComplete();
        protected event MoveComplete _atPosition;
        protected event MoveComplete _distanceMoved;
        protected event MoveComplete _jumpPeaked;
        protected event MoveComplete _multiJumpPeaked;

        protected delegate void CollisionEvent();
        protected event CollisionEvent _collidedRight;
        protected event CollisionEvent _collidedLeft;

        protected bool _tutorialStarted = false;

        protected Vector3 _velocity;
        protected float _moveSpeed;
        protected Vector2 _gravity;
        protected float _gravityScale = 1;

        protected MovementType _moveType;
        protected Vector2 _desiredMoveToPosition;
        protected float _desiredXDistanceToTraverse = 0;
        protected float _xDistanceTraversed = 0;

        protected bool _hasActiveMoveOrder = false;
        protected bool _didJump = false;
        protected bool _didMultiJump = false;
        protected bool _isWallSliding = false;

        protected bool _terminateMovementOnCollision = false;

        protected GameObject _sprite;
        protected PlayerController2D _controller;
        protected SkeletonAnimation _skeletonAnimation;
        protected PlayerEffectHandler _playerEffects;

        protected CoroutineHandle _gravityScaleTimerHandle;


        // Start is called before the first frame update
        protected virtual void Start()
        {
            _moveSpeed = GlobalData.Player.Movement.CurrentActiveGravityState.BaseMoveSpeed;
            _gravity = GlobalData.Player.Movement.CurrentActiveGravityState.GravityVelocity;

            _sprite = transform.GetChild(0).gameObject;
            _controller = GetComponent<PlayerController2D>();
            _skeletonAnimation = _sprite.GetComponent<SkeletonAnimation>();
            _playerEffects = GetComponent<PlayerEffectHandler>();
        }

        protected virtual void Update()
        {
            if (_tutorialStarted)
            {
                StartCoroutine(RunTutorial());
                _tutorialStarted = false;
            }
        }

        // Update is called once per frame
        protected virtual void FixedUpdate()
        {
            if (!_isWallSliding)
            {
                _velocity += (Vector3)_gravity * _gravityScale * Time.deltaTime;
                if (_velocity.y <= 0)
                {
                    if (_didJump)
                    {
                        _jumpPeaked?.Invoke();
                        _didJump = false;
                    }
                    else if (_didMultiJump)
                    {
                        _multiJumpPeaked?.Invoke();
                        _didMultiJump = false;
                    }
                }
            }
            else
                _velocity.y = -PlayerConstants.MAXIMUM_WALL_SLIDE_SPEED;

            Vector3 finalVelocity = _velocity * Time.deltaTime;
            _controller.Move(ref finalVelocity);

            if (_controller.Collisions.IsCollidingRight)
                _collidedRight?.Invoke();
            else if (_controller.Collisions.IsCollidingLeft)
                _collidedLeft?.Invoke();

            if (_hasActiveMoveOrder)
                EvaluateMove(finalVelocity);
        }

        public abstract IEnumerator RunTutorial();
        public abstract void CleanupTutorial();

        public void Activate(GameObject activator)
        {
            transform.parent.gameObject.SetActive(true);
            _tutorialStarted = true;
        }

        public void Deactivate(GameObject deactivator)
        {
            CleanupTutorial();
            transform.parent.gameObject.SetActive(false);
            _tutorialStarted = false;
        }

        protected void MoveRight(float distance, bool terminateOnCollision = false)
        {
            _moveType = MovementType.Distance;

            float xVelocity = transform.right.x * GlobalData.Player.Movement.CurrentActiveGravityState.BaseMoveSpeed;
            if (_velocity.x == 0 || Mathf.Sign(_velocity.x) != Mathf.Sign(xVelocity) || _velocity.x < xVelocity)
                _velocity.x = xVelocity;

            SetSpriteDirectionRight();

            _skeletonAnimation.AnimationState.SetAnimation(0, "Run (Everything Else)", true);
            _skeletonAnimation.AnimationState.SetAnimation(1, "Run (Right Arm + Head)", true);

            _hasActiveMoveOrder = true;
        }

        protected void MoveLeft(float distance, bool terminateOnCollision = false)
        {
            _moveType = MovementType.Distance;

            float xVelocity = -transform.right.x * GlobalData.Player.Movement.CurrentActiveGravityState.BaseMoveSpeed;
            if (_velocity.x == 0 || Mathf.Sign(_velocity.x) != Mathf.Sign(xVelocity) || _velocity.x > xVelocity)
                _velocity.x = xVelocity;

            SetSpriteDirectionLeft();

            _skeletonAnimation.AnimationState.SetAnimation(0, "Run (Everything Else)", true);
            _skeletonAnimation.AnimationState.SetAnimation(1, "Run (Right Arm + Head)", true);

            _hasActiveMoveOrder = true;
        }

        protected void MoveToPosition(Vector2 position, bool terminateOnCollision = false)
        {
            _moveType = MovementType.Position;
            _desiredMoveToPosition = position;

            Vector2 directionToPosition = position - (Vector2)transform.position;
            float xVelocity = Mathf.Sign(directionToPosition.x) > 0 ? GlobalData.Player.Movement.CurrentActiveGravityState.BaseMoveSpeed : -GlobalData.Player.Movement.CurrentActiveGravityState.BaseMoveSpeed;

            if (_velocity.x == 0 || Mathf.Sign(_velocity.x) != Mathf.Sign(xVelocity) || _velocity.x < xVelocity)
                _velocity.x = xVelocity;

            if (Mathf.Sign(directionToPosition.x) > 0)
                SetSpriteDirectionRight();
            else
                SetSpriteDirectionLeft();

            _skeletonAnimation.AnimationState.SetAnimation(0, "Run (Everything Else)", true);
            _skeletonAnimation.AnimationState.SetAnimation(1, "Run (Right Arm + Head)", true);

            _hasActiveMoveOrder = true;
        }

        protected void Jump(bool isMultiJump)
        {
            if (isMultiJump)
            {
                _velocity.y = GlobalData.Player.Movement.CurrentActiveGravityState.JumpVelocity / PlayerConstants.MULTI_JUMP_POWER_REDUCTION;
                _gravityScale /= PlayerConstants.ENERGY_CELL_MOVE_GRAVITY_DIVISOR;

                if (_gravityScaleTimerHandle != null)
                    Timing.KillCoroutines(_gravityScaleTimerHandle);

                _gravityScaleTimerHandle = GlobalData.Timekeeper.StartTimer(() => _gravityScale = 1, PlayerConstants.DOUBLE_JUMP_PAUSE_TIME);

                _skeletonAnimation.AnimationState.SetAnimation(0, _controller.Collisions.FaceDir == 1 ? "Multi Jump (Right)" : "Multi Jump (Left)", false);
                _playerEffects.PlayMultiJumpEffect();

                _didMultiJump = true;
            }
            else
            {
                _velocity.y = GlobalData.Player.Movement.CurrentActiveGravityState.JumpVelocity;

                _skeletonAnimation.AnimationState.SetAnimation(0, "Jump (Everything Else)", false);
                _skeletonAnimation.AnimationState.SetAnimation(1, "Jump (Right Arm + Head)", false);

                _didJump = true;
            }
        }

        protected void Dash()
        {
            _velocity.y = 0;

            if (_gravityScaleTimerHandle != null)
                Timing.KillCoroutines(_gravityScaleTimerHandle);

            _gravityScale = 0;
            _gravityScaleTimerHandle = GlobalData.Timekeeper.StartTimer(new Timekeeper.TimedAction(() => _gravityScale = 1 / PlayerConstants.ENERGY_CELL_MOVE_GRAVITY_DIVISOR, PlayerConstants.AIR_DASH_PAUSE_TIME / 2),
                new Timekeeper.TimedAction(() => _gravityScale = 1, PlayerConstants.AIR_DASH_PAUSE_TIME / 2));

            if (!_isWallSliding)
            {
                if (_controller.Collisions.FaceDir == 1)
                    _velocity.x += PlayerConstants.DASH_VELOCITY;
                else if (_controller.Collisions.FaceDir == -1)
                    _velocity.x -= PlayerConstants.DASH_VELOCITY;
            }
            else
            {
                if (_controller.Collisions.FaceDir == 1)
                    _velocity.x -= PlayerConstants.DASH_VELOCITY;
                else
                    _velocity.x += PlayerConstants.DASH_VELOCITY;
            }

            /*
            if (GlobalData.Player.Input.MovementInput.x != 0 && GlobalData.Player.Controller.Collisions.IsCollidingBelow)
                _moveSpeed = GlobalData.Player.Movement.CurrentActiveGravityState.BaseMoveSpeed * PlayerConstants.RUNNING_MOVE_SPEED_MUTLIPLER;
            */

            _playerEffects.PlayDashEffect();
        }

        protected void WallSlideRight()
        {
            _isWallSliding = true;

            if (_velocity.y < -PlayerConstants.MAXIMUM_WALL_SLIDE_SPEED)
                _velocity.y = -PlayerConstants.MAXIMUM_WALL_SLIDE_SPEED;
            else if (_velocity.y > 0)
                _velocity.y = 0;
        }

        protected void WallSlideLeft()
        {
            _isWallSliding = true;

            if (_velocity.y < -PlayerConstants.MAXIMUM_WALL_SLIDE_SPEED)
                _velocity.y = -PlayerConstants.MAXIMUM_WALL_SLIDE_SPEED;
            else if (_velocity.y > 0)
                _velocity.y = 0;
        }

        protected void Stop()
        {
            _velocity = Vector2.zero;
            _isWallSliding = false;
            _moveSpeed = GlobalData.Player.Movement.CurrentActiveGravityState.BaseMoveSpeed;
        }

        protected void SetSpriteDirectionRight()
        {
            _sprite.transform.localScale = new Vector3(Mathf.Abs(_sprite.transform.localScale.x), _sprite.transform.localScale.y, _sprite.transform.localScale.z);
            _controller.Collisions.FaceDir = 1;
        }

        protected void SetSpriteDirectionLeft()
        {
            _sprite.transform.localScale = new Vector3(-Mathf.Abs(_sprite.transform.localScale.x), _sprite.transform.localScale.y, _sprite.transform.localScale.z);
            _controller.Collisions.FaceDir = -1;
        }

        private void EvaluateMove(Vector3 movedVelocity)
        {
            if (_moveType == MovementType.Distance)
            {
                _xDistanceTraversed += movedVelocity.x;
                if (Mathf.Abs(_xDistanceTraversed) >= _desiredXDistanceToTraverse)
                {
                    _velocity.x = 0;
                    _distanceMoved?.Invoke();
                    _hasActiveMoveOrder = false;
                }
                else if (_terminateMovementOnCollision && ((Mathf.Sign(movedVelocity.x) == 1 && _controller.Collisions.IsCollidingRight) || (Mathf.Sign(movedVelocity.x) == -1 && _controller.Collisions.IsCollidingLeft)))
                {
                    _velocity.x = 0;
                    _distanceMoved?.Invoke();
                    _hasActiveMoveOrder = false;
                }
            }
            else
            {
                if (Mathf.Abs(transform.position.x - _desiredMoveToPosition.x) <= 0.1f)
                {
                    _velocity.x = 0;
                    _atPosition?.Invoke();
                    _hasActiveMoveOrder = false;
                }
                else if (_terminateMovementOnCollision && ((Mathf.Sign(movedVelocity.x) == 1 && _controller.Collisions.IsCollidingRight) || (Mathf.Sign(movedVelocity.x) == -1 && _controller.Collisions.IsCollidingLeft)))
                {
                    _velocity.x = 0;
                    _atPosition?.Invoke();
                    _hasActiveMoveOrder = false;
                }
            }
        }
    }
}
