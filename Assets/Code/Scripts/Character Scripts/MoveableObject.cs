﻿using System.Collections.Generic;
using UnityEngine;
using LevelComponents.Platforms;
using static GlobalData;
using Characters.Player;

public abstract class MoveableObject : MonoBehaviour
{
    public abstract class GravityState
    {
        public GravityType GravityType { get { return _gravityType; } }
        public Vector2 GravityVelocity { get { return _gravityVelocity; } }
        public float BaseGravityScale { get { return _baseGravityScale; } }

        /// <summary>
        /// The value the object's Y velocity will be set to upon entering a gravity trigger (provided their current velocity is negative and exceeds this value).
        /// </summary>
        public float GravityReductionValue { get { return _gravityReductionValue; } }

        protected GravityType _gravityType;
        protected Vector2 _gravityVelocity;

        protected float _baseGravityScale;
        protected Vector2 _baseGravityVelocity;

        protected float _gravityReductionValue;


        /// <summary>
        /// Changes the gravity velocity applied to this character for this gravity state to the given magnitude applied along the suppled direction.
        /// </summary>
        /// <param name="gravityMagnitude">The magnitude of the new gravity value.</param>
        /// <param name="normalizedGravityDirection">The direction gravity should be applied in, as a normalized vector.</param>
        public void ChangeGravity(float gravityMagnitude, Vector2 normalizedGravityDirection)
        {
            _gravityVelocity = normalizedGravityDirection * gravityMagnitude;
        }

        /// <summary>
        /// Changes the direction gravity is applied to this character for this gravity state without changing the magnitude of the gravitational velocity.
        /// </summary>
        /// <param name="normalizedGravityDirection">The direction gravity should be applied in, as a normalized vector.</param>
        public void ChangeGravityDirection(Vector2 normalizedGravityDirection)
        {
            var baseGravityMagnitude = Mathf.Abs(_baseGravityVelocity.magnitude);
            _gravityVelocity = baseGravityMagnitude * normalizedGravityDirection;
        }

        /// <summary>
        /// Restores the magnitude and direction of the applied gravity to the base for this gravity state.
        /// </summary>
        public void RestoreBaseGravity()
        {
            _gravityVelocity = _baseGravityVelocity;
        }
    }

    public struct VelocityComponent
    {
        /// <summary>
        /// The current velocity for this component.
        /// </summary>
        public Vector2 Velocity;

        /// <summary>
        /// Whether any applied velocity for this component is constant, or should be treated as a one time force
        /// </summary>
        public readonly bool VelocityIsConstant;

        /// <summary>
        /// Reference to the key that this component is stored in the dictionary under. Allows changes to be made to the component and stored in situations when all the components are being handled at once and the key for the current component would otherwise be unknown.
        /// </summary>
        public readonly int DictionaryKey;


        public VelocityComponent(Vector2 initialVelocity, bool velocityIsConstant, int dictionaryKey) => (Velocity, VelocityIsConstant, DictionaryKey) = (initialVelocity, velocityIsConstant, dictionaryKey);
    }


    public const int OBJECT_VELOCITY_COMPONENT = 0;
    public const int PLATFORM_VELOCITY_COMPONENT = 1;


    /// <summary>
    /// The current overall velocity (factoring in all velocity components) of the object.
    /// </summary>
    public Vector2 Velocity
    {
        get
        {
            var totalVelocity = Vector2.zero;
            foreach (VelocityComponent velocity in _velocityComponentDictionary.Values)
            {
                totalVelocity += velocity.Velocity;
            }
            return totalVelocity;
        }
    }

    /// <summary>
    /// Reference to the object's controller, which performs movement and collision related functionality.
    /// </summary>
    public abstract Controller2D Controller { get; }

    /// <summary>
    /// Reference to the object's collisions info, which provides information about the character's current collision status.
    /// Can be gotten through Controller, this is merely a shortcut for convenience.
    /// </summary>
    public abstract Controller2D.CollisionInfo Collisions { get; }

    /// <summary>
    /// The currently active gravity state for the object.
    /// </summary>
    public abstract GravityState CurrentActiveGravityState { get; }

    /// <summary>
    /// The platform the object is currently registered with (if they are registered with one).
    /// </summary>
    public Platform RegisteredPlatform { get; private set; }


    /// <summary>
    /// The value that determines how much of the current gravity value is applied to the object's velocity.
    /// </summary>
    [HideInInspector]
    public float GravityScale = 1;

    /// <summary>
    /// Whether the object should be moved relative to world space, or relative to their own space.
    /// </summary>
    public bool MoveCharacterInLocalSpace = false;


    /// <summary>
    /// Contains the valu
    /// </summary>
    private Dictionary<int, VelocityComponent> _velocityComponentDictionary = new Dictionary<int, VelocityComponent>
    {
        { OBJECT_VELOCITY_COMPONENT, new VelocityComponent(Vector2.zero, true, OBJECT_VELOCITY_COMPONENT) },
        { PLATFORM_VELOCITY_COMPONENT, new VelocityComponent(Vector2.zero, true, PLATFORM_VELOCITY_COMPONENT) }
    };

    private ObjectIsPassenger _passengerMarker;


    /// <summary>
    /// Directly overrides the velocity of the specified component with the provided velocity vector. Will overwrite the base object velocity component by default.
    /// </summary>
    /// <param name="velocity">The new velocity vector.</param>
    /// <param name="velocityComponent">Int representing which component of the velocity to set.</param>
    public virtual void SetVelocity(Vector2 newVelocity, int velocityComponent = OBJECT_VELOCITY_COMPONENT)
    {
        if (_velocityComponentDictionary.ContainsKey(velocityComponent))
        {
            var component = _velocityComponentDictionary[velocityComponent];
            component.Velocity = newVelocity;
            _velocityComponentDictionary[velocityComponent] = component;

            return;
        }
        throw new KeyNotFoundException();
    }

    /// <summary>
    /// Directly override the velocity of the specified axis of the specified component with the provided value. Will overwrite the base object velocity component by default.
    /// </summary>
    /// <param name="axis">The axis to override.</param>
    /// <param name="newAxisVelocity">The new axis velocity.</param>
    /// <param name="velocityComponent">Int representing which component of the velocity to set.</param>
    public virtual void SetVelocityOnOneAxis(char axis, float newAxisVelocity, int velocityComponent = OBJECT_VELOCITY_COMPONENT)
    {
        if (_velocityComponentDictionary.ContainsKey(velocityComponent))
        {
            var component = _velocityComponentDictionary[velocityComponent];
            switch (axis)
            {
                case 'X':
                    component.Velocity.x = newAxisVelocity;
                    break;
                case 'Y':
                    component.Velocity.y = newAxisVelocity;
                    break;
            }
            _velocityComponentDictionary[velocityComponent] = component;

            return;
        }
        throw new KeyNotFoundException();
    }

    /// <summary>
    /// Adds the provided velocity to the specified velocity component for the object. Will add to the base object velocity component by default.
    /// </summary>
    /// <param name="addedVelocity">The velocity to add to the specified velocity component</param>
    /// <param name="velocityComponent">The value representing the velocity component to retrieve.</param>
    /// <returns>The velocity of the component with the added velocity.</returns>
    public virtual Vector2 AddToVelocity(Vector2 addedVelocity, int velocityComponent = OBJECT_VELOCITY_COMPONENT)
    {
        if (_velocityComponentDictionary.ContainsKey(velocityComponent))
        {
            var component = _velocityComponentDictionary[velocityComponent];
            component.Velocity += addedVelocity;
            _velocityComponentDictionary[velocityComponent] = component;

            return component.Velocity;
        }
        throw new KeyNotFoundException();
    }

    /// <summary>
    /// Adds the provided value to the specified axis of the specified velocity component for the object. Will add to the base object velocity component by default.
    /// </summary>
    /// <param name="axis">The axis to add to.</param>
    /// <param name="addedAxisVelocity">The velocity to add.</param>
    /// <param name="velocityComponent">Int representing which component of the velocity to set.</param>
    /// <returns></returns>
    public virtual Vector2 AddToVelocityOnOneAxis(char axis, float addedAxisVelocity, int velocityComponent = OBJECT_VELOCITY_COMPONENT)
    {
        if (_velocityComponentDictionary.ContainsKey(velocityComponent))
        {
            var component = _velocityComponentDictionary[velocityComponent];
            switch (axis)
            {
                case 'X':
                    component.Velocity.x = addedAxisVelocity;
                    break;
                case 'Y':
                    component.Velocity.y = addedAxisVelocity;
                    break;
            }
            _velocityComponentDictionary[velocityComponent] = component;

            return component.Velocity;
        }
        throw new KeyNotFoundException();
    }

    /// <summary>
    /// Zeroes out the specified velocity component for the object.
    /// </summary>
    /// <param name="velocityComponent"></param>
    public virtual void ZeroOutVelocity(int velocityComponent = OBJECT_VELOCITY_COMPONENT)
    {
        var component = _velocityComponentDictionary[velocityComponent];
        component.Velocity = Vector2.zero;
        _velocityComponentDictionary[velocityComponent] = component;
    }

    /// <summary>
    /// Zeroes out the specified axis for the specified velocity component for the object.
    /// </summary>
    /// <param name="axis">The axis to zero out.</param>
    /// <param name="velocityComponent">Int representing which component of the velocity to set.</param>
    public virtual void ZeroOutVelocityOnOneAxis(char axis, int velocityComponent = OBJECT_VELOCITY_COMPONENT)
    {
        var component = _velocityComponentDictionary[velocityComponent];
        switch (axis)
        {
            case 'X':
                component.Velocity.x = 0f;
                break;
            case 'Y':
                component.Velocity.y = 0f;
                break;
        }
        _velocityComponentDictionary[velocityComponent] = component;
    }

    /// <summary>
    /// Zeroes out all velocity components for the object, bringing it to a total standstill.
    /// </summary>
    public virtual void ZeroOutAllVelocities()
    {
        var keys = new List<int>(_velocityComponentDictionary.Keys);
        foreach (int key in keys)
        {
            var component = _velocityComponentDictionary[key];
            component.Velocity = Vector2.zero;
            _velocityComponentDictionary[key] = component;
        }
    }

    /// <summary>
    /// Returns the whole specified velocity component. Note, this will throw a KeyNotFoundException if the component is not found.
    /// </summary>
    /// <param name="velocityComponent">The value representing the velocity component to retrieve.</param>
    /// <returns>The velocity component.</returns>
    public VelocityComponent GetVelocityComponent(int velocityComponent = OBJECT_VELOCITY_COMPONENT)
    {
        if (_velocityComponentDictionary.ContainsKey(velocityComponent))
        {
            return _velocityComponentDictionary[velocityComponent];
        }
        throw new KeyNotFoundException();
    }

    /// <summary>
    /// Returns all the velocity components the object possesses.
    /// </summary>
    /// <returns>A List containing all velocity components the object possesses.</returns>
    public List<VelocityComponent> GetAllVelocityComponents()
    {
        return new List<VelocityComponent>(_velocityComponentDictionary.Values);
    }

    /// <summary>
    /// Returns the velocity value stored in the specified velocity component. Note, this will throw a KeyNotFoundException if the component is not found.
    /// </summary>
    /// <param name="velocityComponent">The value representing the velocity component to retrieve.</param>
    /// <returns>The velocity stored in the velocity component.</returns>
    public Vector2 GetVelocityComponentVelocity(int velocityComponent = OBJECT_VELOCITY_COMPONENT)
    {
        if (_velocityComponentDictionary.ContainsKey(velocityComponent))
        {
            return _velocityComponentDictionary[velocityComponent].Velocity;
        }
        throw new KeyNotFoundException();
    }

    /// <summary>
    /// Adds a new velocity component to the object's velocity component dictionary.
    /// </summary>
    /// <param name="velocityComponent">The value representing the new component.</param>
    /// <param name="initialVelocity">The initial velocity for the component.</param>
    /// <param name="velocityIsConstant">Whether any applied velocity for this new component is constant, or should be treated as a one time force.</param>
    /// <returns>True if the addition was successful. False if the value already exists, and thus the addition was a failure.</returns>
    public bool AddVelocityComponent(int velocityComponent, Vector2 initialVelocity, bool velocityIsConstant)
    {
        if (!_velocityComponentDictionary.ContainsKey(velocityComponent))
        {
            _velocityComponentDictionary.Add(velocityComponent, new VelocityComponent(initialVelocity, velocityIsConstant, velocityComponent));
            return true;
        }
        //The value for the velocity component addition already exists in the dictionary.
        return false;
    }

    /// <summary>
    /// Stores the platform the object is currently registered as a passenger on.
    /// </summary>
    /// <param name="registeredPlatform">The platform the object is a passenger on.</param>
    public void HasBeenRegistered(Platform registeredPlatform)
    {
        RegisteredPlatform = registeredPlatform;
    }

    /// <summary>
    /// Denotes that the object is no longer registered as a platform passenger by nulling the RegisteredPlatform, and removes the marker indicating this object was a passenger of a platform.
    /// </summary>
    public void HasBeenDeregistered()
    {
        RegisteredPlatform = null;

        var passengerMarker = gameObject.GetComponent<ObjectIsPassenger>();
        if (passengerMarker != null)
        {
            Destroy(passengerMarker);
        }
    }

    /// <summary>
    /// Calculates the velocity for this frame based on all the velocities active on the object.
    /// </summary>
    /// <returns>A Vector3 containing the velocity for this frame.</returns>
    public Vector3 CalculateFrameVelocity()
    {
        Vector3 finalVelocity = Vector3.zero;
        var allVelocities = GetAllVelocityComponents();
        foreach (VelocityComponent velocity in allVelocities)
        {
            Vector3 addedVelocity = velocity.Velocity * Time.fixedDeltaTime;
            if (!velocity.VelocityIsConstant)
            {
                SetVelocity(velocity.Velocity - (Vector2)addedVelocity, velocity.DictionaryKey);
            }
            finalVelocity += addedVelocity;
        }
        return finalVelocity;
    }
}
