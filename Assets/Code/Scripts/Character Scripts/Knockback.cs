﻿using UnityEngine;
using System.Collections;

public abstract class Knockback : MonoBehaviour
{
    /// <summary>
    /// The possible options for knockback on the X-axis.
    /// </summary>
    public enum XKnockbackDirection { EITHER, SAME_DIRECTION_AS_COLLIDER_VELOCITY, OPPOSITE_DIRECTION_OF_COLLIDER_VELOCITY, SAME_DIRECTION_AS_PLAYER_VELOCITY, OPPOSITE_DIRECTION_OF_PLAYER_VELOCITY, BACK, FORWARD, GLOBAL_LEFT, GLOBAL_RIGHT }

    /// <summary>
    /// The possible options for knockback on the Y-axis.
    /// </summary>
    public enum YKnockbackDirection { EITHER, SAME_DIRECTION_AS_VELOCITY, OPPOSITE_DIRECTION_OF_VELOCITY, UP, DOWN }


    /// <summary>
    /// Whether the true direction of the collision should be factored into the application of knockback (only used when either X or Y knockback is set to EITHER).
    /// </summary>
    [SerializeField]
    protected bool usePositionalAwareness = false;

    /// <summary>
    /// The amount of knockback the player should receive.
    /// </summary>
    [SerializeField]
    protected Vector2 knockbackBaseValue;

    /// <summary>
    /// The desired knockback direction on the X-axis.
    /// </summary>
    [SerializeField]
    protected XKnockbackDirection desiredXKnockbackDirection = XKnockbackDirection.EITHER;

    /// <summary>
    /// The desired knockback direction on the Y-axis.
    /// </summary>
    [SerializeField]
    protected YKnockbackDirection desiredYKnockbackDirection = YKnockbackDirection.EITHER;

    /// <summary>
    /// How much damage should be dealt to the whatever is receiving the knockback (if it can receive damage).
    /// </summary>
    [SerializeField]
    protected float damage;


    /// <summary>
    /// Reference to the collider this is attached to.
    /// </summary>
    protected new Collider2D collider;

    /// <summary>
    /// The velocity of the collider.
    /// </summary>
    protected Vector3 velocity;

    /// <summary>
    /// The position of the collider on the last frame.
    /// </summary>
    protected Vector3 _previousPosition;


    private void Start()
    {
        knockbackBaseValue = new Vector2(Mathf.Abs(knockbackBaseValue.x), Mathf.Abs(knockbackBaseValue.y));
    }

    /// Calculates the direction of the knockback (accounting for any desired knockback direction) and returns the knockback with that direction applied. It dose not account for if knockback back should be applied
    /// </summary>
    /// <param name="CollidingObjectConter">The Center of the Object to apply knockback to (Should be the transform.position for no player objects) </param>
    /// <returns>The knockback with direction applied.</returns>
    protected virtual Vector2 CalculateKnockback(Vector3 collidingObjectCenter)
    {
        Vector3 knockbackToApply = knockbackBaseValue;
        if (desiredXKnockbackDirection == XKnockbackDirection.EITHER || desiredYKnockbackDirection == YKnockbackDirection.EITHER)
        {
            Vector3 directionToColliderExit = collidingObjectCenter - collider.transform.position;
            directionToColliderExit.Normalize();
            if (!usePositionalAwareness)
            {
                if (desiredXKnockbackDirection == XKnockbackDirection.EITHER)
                {
                    if (directionToColliderExit.x < 0)
                    {
                        knockbackToApply.x *= -1;
                    }
                }
                if (desiredYKnockbackDirection == YKnockbackDirection.EITHER)
                {
                    if (directionToColliderExit.y < 0)
                    {
                        knockbackToApply.y *= -1;
                    }
                }
            }
            else
            {
                if (desiredXKnockbackDirection == XKnockbackDirection.EITHER)
                {
                    knockbackToApply.x = knockbackBaseValue.x * directionToColliderExit.x;
                }
                if (desiredYKnockbackDirection == YKnockbackDirection.EITHER)
                {
                    knockbackToApply.y = knockbackBaseValue.y * directionToColliderExit.y;
                }
            }
        }
        if (desiredXKnockbackDirection == XKnockbackDirection.SAME_DIRECTION_AS_COLLIDER_VELOCITY || desiredXKnockbackDirection == XKnockbackDirection.OPPOSITE_DIRECTION_OF_COLLIDER_VELOCITY
            || desiredXKnockbackDirection == XKnockbackDirection.SAME_DIRECTION_AS_PLAYER_VELOCITY || desiredXKnockbackDirection == XKnockbackDirection.OPPOSITE_DIRECTION_OF_PLAYER_VELOCITY)
        {
            float xVelocitySign = 0;
            if (desiredXKnockbackDirection == XKnockbackDirection.SAME_DIRECTION_AS_COLLIDER_VELOCITY || desiredXKnockbackDirection == XKnockbackDirection.OPPOSITE_DIRECTION_OF_COLLIDER_VELOCITY)
            {
                xVelocitySign = Mathf.Sign(velocity.x);
            }
            else
            {
                xVelocitySign = Mathf.Sign(GlobalData.Player.Velocity.x);
            }

            if (desiredXKnockbackDirection == XKnockbackDirection.SAME_DIRECTION_AS_COLLIDER_VELOCITY || desiredXKnockbackDirection == XKnockbackDirection.SAME_DIRECTION_AS_PLAYER_VELOCITY)
            {
                knockbackToApply.x *= xVelocitySign;
            }
            else
            {
                knockbackToApply.x *= -xVelocitySign;
            }
        }
        else if (desiredXKnockbackDirection == XKnockbackDirection.BACK)
        {
            if (StaticTools.ThresholdApproximately(velocity.x, 0, 0.01f))
            {
                Vector3 directionToColliderCenter = collider.transform.position - collidingObjectCenter; //Get the direction vector to the center of the collider.
                if (directionToColliderCenter.x > 0) //If the center of the collider is to the right of the player, then back is to the left.
                {
                    knockbackToApply.x *= -1;
                }
            }
            else
            {
                if (velocity.x > 0) //If the X-velocity of the collider is positive, it is going to the right, so back is to the left.
                {
                    knockbackToApply.x *= -1;
                }
            }
        }
        else if (desiredXKnockbackDirection == XKnockbackDirection.FORWARD)
        {
            if (StaticTools.ThresholdApproximately(velocity.x, 0, 0.01f))
            {
                Vector3 directionToColliderCenter = collider.transform.position - collidingObjectCenter; //Get the direction vector to the center of the collider.
                if (directionToColliderCenter.x < 0) //If the center of the collider is to the left of the player, then forward is in that direction.
                {
                    knockbackToApply.x *= -1;
                }
            }
            else
            {
                if (velocity.x < 0) //If the X-velocity of the collider is negative, it is going to the left, so forward is in that direction.
                {
                    knockbackToApply.x *= -1;
                }
            }
        }
        else if (desiredXKnockbackDirection == XKnockbackDirection.GLOBAL_LEFT)
        {
            knockbackToApply.x *= -1;
        }
        //We don't need to handle GLOBAL_RIGHT, since that is already handled by the knockback values being positive.

        if (desiredYKnockbackDirection == YKnockbackDirection.SAME_DIRECTION_AS_VELOCITY || desiredYKnockbackDirection == YKnockbackDirection.OPPOSITE_DIRECTION_OF_VELOCITY)
        {
            float yVelocitySign = Mathf.Sign(velocity.y);

            if (desiredYKnockbackDirection == YKnockbackDirection.SAME_DIRECTION_AS_VELOCITY)
            {
                knockbackToApply.y *= yVelocitySign;
            }
            else
            {
                knockbackToApply.y *= -yVelocitySign;
            }
        }
        else if (desiredYKnockbackDirection == YKnockbackDirection.DOWN)
        {
            knockbackToApply.y *= -1;
        }
        //We don't need to handle UP, since that is already handled by the knockback values being positive.

        return new Vector2(knockbackToApply.x, knockbackToApply.y);
    }
}
