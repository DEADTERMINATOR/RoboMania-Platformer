﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalData;

public class VariableDamageNPCCollider : EnemyCollider
{
    /// <summary>
    /// The percentage of the base damage this collider will receive.
    /// </summary>
    [Range(0.25f, 3f)]
    public float DamagePercentage;


    public override void TakeDamage(IDamageGiver giver, float amount, DamageType type, Vector3 hitPoint)
    {
        if (DamagePercentage < 1)
        {
            base.TakeDamage(giver, amount* DamagePercentage, DamageType.REDUCED,hitPoint);
        }
        else
        {
            base.TakeDamage(giver, amount* DamagePercentage, DamageType.INCREASED,hitPoint);
        }
    }
}
