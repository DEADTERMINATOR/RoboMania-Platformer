﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters.Enemies;

public class EnemyCollider : MonoBehaviour, IDamageReceiver
{
    /// <summary>
    /// Reference to the npc script to get information about the NPC the colider is attached to 
    /// </summary>
    public Enemy Enemy;

    /// <summary>
    /// Reference to the body part's collider.
    /// </summary>
    protected Collider2D _collider;


    // Use this for initialization
    protected virtual void Start()
    {
        if (Enemy == null)
        {
            Enemy = transform.GetComponentInParent<Enemy>();
        }
        if (Enemy == null)
        {
            Debug.Log(name);
        }
        _collider = GetComponent<Collider2D>();
    }

    /*
    protected virtual void Update()
    {
        if (Enemy.Activated)
        {
            //Set the rotation and position of the collider to match that of the sprite.
            _collider.transform.rotation = Quaternion.Euler(_collider.transform.rotation.eulerAngles.x, _collider.transform.rotation.eulerAngles.y, Enemy.CharacterSpriteCollection.transform.rotation.eulerAngles.z);
            _collider.transform.position = new Vector3(Enemy.CharacterSpriteCollection.transform.position.x, Enemy.CharacterSpriteCollection.transform.position.y, 0);
        }
    }
    */

    public virtual void TakeDamage(IDamageGiver giver, float amount, GlobalData.DamageType type, Vector3 hitPoint)
    {
        Enemy.TakeDamage(giver, amount, type,hitPoint);
    }
}
