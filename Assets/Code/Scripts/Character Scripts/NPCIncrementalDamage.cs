﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Characters.Enemies;
using Characters;

public class NPCIncrementalDamage : MonoBehaviour
{
    /// <summary>
    /// First Lelve of Dmage
    /// </summary>
    public List<GameObject> LowLevelDamage;

    /// <summary>
    /// Second level of Dmage
    /// </summary>
    public List<GameObject> MidLevelDamage;

    /// <summary>
    /// Second level of Dmage
    /// </summary>
    public List<GameObject> MaxLevelDamage;

    /// <summary>
    /// Bit That are spaened When hit
    /// </summary>
    public List<GameObject> PicesToDroop;

    /// <summary>
    /// A sparl FX to Run on a hit
    /// </summary>
    public ParticleSystem SparkFX;

    /// <summary>
    /// The NPC this Script affects
    /// </summary>
    public Enemy TheNPC;

    //
    public float MidLevelPoint = 0.66f;


    public float MaxLevelPoint = 0.33f;

    public float NPCHealthPrecetage { get { return TheNPC.Health / TheNPC.MaxHealth; } }


    public void TakeHit(Vector3 pos)
    {

        //Pick the decal List based on health
        List<GameObject> list = LowLevelDamage;
        if(NPCHealthPrecetage < MidLevelPoint)
        {
            list = MidLevelDamage;
            if (NPCHealthPrecetage < MaxLevelPoint)
            {
                list = MaxLevelDamage;
            }
        }

        float dist = float.MaxValue;
        GameObject closest= null;
        foreach (GameObject go in list)
        {
            if(!go.activeInHierarchy && Vector3.Distance(pos,go.transform.position) < dist)
            {
                closest = go;
                dist = Vector3.Distance(pos, go.transform.position);
            }                      
        }
        closest?.SetActive(true);
        GameObject Part = Instantiate(PicesToDroop[UnityEngine.Random.Range(0, PicesToDroop.Count)], pos, Quaternion.identity);
        Part.SetActive(true);
       /// Part.GetComponent<Rigidbody2D>().AddForce(new Vector3(UnityEngine.Random.Range(20,50),106,0));
    }

    // Use this for initialization
    void Start()
    {
        SparkFX.Stop();
        if (TheNPC == null)// a Fail safe
        {
            TheNPC = GetComponent<Enemy>();
        }

        if(TheNPC == null)// a Fail safe
        {
            throw new Exception("NPCIncrementalDamage Can not Find an NPC ");
        }

        TheNPC.DamageReceived += TookDamageEvent;

        foreach (GameObject go in LowLevelDamage)
        {
            go.SetActive(false);
        }
        foreach (GameObject go in MidLevelDamage)
        {
            go.SetActive(false);
        }
        foreach (GameObject go in MaxLevelDamage)
        {
            go.SetActive(false);
        }
        foreach (GameObject go in PicesToDroop)
        {
            go.SetActive(false);
        }

    }

    public void TookDamageEvent(Vector3 pos)
    {
        SparkFX.transform.position = pos;
        SparkFX.Play();
        TakeHit(pos);
    }
}
