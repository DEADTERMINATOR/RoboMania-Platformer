﻿using Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Direction = Controller2D.CollisionInfo.CollisionDirectionInfo.Direction;

//[RequireComponent(typeof(CollisionBoxcaster2D))]
//[RequireComponent(typeof(MoveableObject))]
public class Controller2D : MonoBehaviour
{
    #region Collision Info
    /// <summary>
    /// A struct containing information about whether the object this controller handles is currently colliding.
    /// </summary>
    public class CollisionInfo
    {
        public class CollisionDirectionInfo
        {
            public delegate void CollisionEvent(CollisionDirectionInfo info);
            public event CollisionEvent CollisionOccurred;

            public enum Direction { Above, Below, Left, Right }

            public readonly Direction CollisionDirection;

            public bool IsColliding { get; private set; }
            public int CollidingLayer { get; private set; }
            public GameObject CollidingObject { get; private set; }

            public CollisionDirectionInfo(Direction collisionDirection, bool isColliding, int collidingLayer, GameObject collidingObject) => (CollisionDirection, IsColliding, CollidingLayer, CollidingObject) = (collisionDirection, isColliding, collidingLayer, collidingObject);

            public void SetColliding(int collidingLayer, GameObject collidingObject = null)
            {
                IsColliding = true;
                CollidingLayer = collidingLayer >= 0 && collidingLayer <= 31 ? collidingLayer : 0;
                CollidingObject = collidingObject;

                CollisionOccurred?.Invoke(this);
            }

            public void Reset()
            {
                IsColliding = false;
                CollidingLayer = -1;
                CollidingObject = null;
            }
        }

        public CollisionDirectionInfo Above;

        public CollisionDirectionInfo Below;

        public CollisionDirectionInfo Left;

        public CollisionDirectionInfo Right;

        /// <summary>
        /// Whether the object is colliding with something above it.
        /// </summary>
        public bool IsCollidingAbove { get { return Above.IsColliding; } }

        /// <summary>
        /// Whether the object is colliding with something below it.
        /// </summary>
        public bool IsCollidingBelow { get { return Below.IsColliding; } }

        /// <summary>
        /// Whether the object is colliding with something to the left of it.
        /// </summary>
        public bool IsCollidingLeft { get { return Left.IsColliding; } }

        /// <summary>
        /// Whether the object is colliding with something to the right of it.
        /// </summary>
        public bool IsCollidingRight { get { return Right.IsColliding; } }

        /// <summary>
        /// Whether the object was colliding with something below it on the last frame.
        /// </summary>
        public bool PreviousBelow;

        /// <summary>
        /// The surface normal of the object the controller owner is colliding with on the bottom.
        /// </summary>
        public Vector2 BelowSurfaceNormal;

        /// <summary>
        /// The direction the object this controller handles is currently facing. 1 for right and -1 for left.
        /// </summary>
        public int FaceDir;

        /// <summary>
        /// The direction the object this controller handles faced last frame. 1 for right and -1 for left.
        /// </summary>
        public int PreviousFaceDir = 1;

        /// <summary>
        /// The velocity at the start of the collision check.
        /// </summary>
        public Vector3 StartingVelocity;

        public CollisionInfo()
        {
            Above = new CollisionDirectionInfo(Direction.Above, false, -1, null);
            Below = new CollisionDirectionInfo(Direction.Below, false, -1, null);
            Left = new CollisionDirectionInfo(Direction.Left, false, -1, null);
            Right = new CollisionDirectionInfo(Direction.Right, false, -1, null);

            BelowSurfaceNormal = new Vector2(0, 1);
        }

        /// <summary>
        /// Resets all collision variables to their base values.
        /// </summary>
        public virtual void Reset()
        {
            PreviousBelow = IsCollidingBelow;

            Above.Reset();
            Below.Reset();
            Left.Reset();
            Right.Reset();

            BelowSurfaceNormal = new Vector2(0, 1);
            PreviousFaceDir = FaceDir;
        }

        public void DrawLineToCollisions(Vector3 startPosition, float duration = 0.2f, bool printCollidingObjectName = false)
        {
            if (Above.IsColliding)
            {
                DrawLine(startPosition, Above.CollidingObject, duration, printCollidingObjectName, "Above: " + Above.CollidingObject.name);
            }
            if (Below.IsColliding)
            {
                DrawLine(startPosition, Below.CollidingObject, duration, printCollidingObjectName, "Below: " + Below.CollidingObject.name);
            }
            if (Left.IsColliding)
            {
                DrawLine(startPosition, Left.CollidingObject, duration, printCollidingObjectName, "Left: " + Left.CollidingObject.name);
            }
            if (Right.IsColliding)
            {
                DrawLine(startPosition, Right.CollidingObject, duration, printCollidingObjectName, "Right: " + Right.CollidingObject.name);
            }
        }

        private void DrawLine(Vector3 startPosition, GameObject collidingObject, float duration, bool printDebug, string debugMessage)
        {
            Debug.DrawLine(startPosition, collidingObject.transform.position, Color.red, duration);
            if (printDebug)
            {
                Debug.Log(debugMessage);
            }
        }
    }
    #endregion

    #region Constants
    /// <summary>
    /// The additional distance on the X-axis the character will be ejected if the character ends up inside a collider (should the eject from collider option be true).
    /// </summary>
    public const float X_EJECT_DISTANCE = 0.01f;

    /// <summary>
    /// The additional distance on the Y-axis the character will be ejected if the character ends up inside a collider (should the eject from collider option be true).
    /// </summary>
    public const float Y_EJECT_DISTANCE = 0.01f;

    /// <summary>
    /// The time a raycast should be drawn for (if drawRaycasts in the collision methods is true).
    /// </summary>
    protected const float RAYCAST_DRAW_TIME = 0.001f;
    #endregion

    #region Public Variables
    /// <summary>
    /// Instance of the CollisionInfo struct to hold information about collisions for the object.
    /// </summary>
    public CollisionInfo Collisions { get; protected set; }

    /// <summary>
    /// Reference to the boxcaster attached to the object this controller handles.
    /// </summary>
    public CollisionBoxcaster2D Boxcaster { get; private set; }

    /// <summary>
    /// Event fired when The object the controller controls 
    /// </summary>
    [HideInInspector]
    public UnityEvent OnShouldTurnAroundFromPlatfrom;

    /// <summary>
    /// Whether the object is standing on something with a Platform component that is currently moving up.
    /// </summary>
    [HideInInspector]
    public bool StandingOnUpwardMovingPlatform;
    #endregion

    /// <summary>
    /// The game object that this controller manages.
    /// </summary>
    protected MoveableObject objectToControl;

    protected Rigidbody2D _rigidbody2D;


    protected virtual void Awake()
    {
        objectToControl = gameObject.GetComponent<MoveableObject>();
        _rigidbody2D = objectToControl.GetComponent<Rigidbody2D>();
        Boxcaster = GetComponent<CollisionBoxcaster2D>();
        Collisions = new CollisionInfo();
    }


    #region Move
    /// <summary>
    /// Calculates what the velocity of the controlled object should be based on their desried velocity and any other objects they may collide with when trying to achieve that velocity.
    /// </summary>
    /// <param name="velocity">The velocity of the object prior to collision detection.</param>
    /// <param name="performCollisionCheckOnProvidedAxis">Whether the movement logic should perform a collision check to ensure the movement doesn't enter a collider.</param>
    /// <param name="ejectFromColliderOnX">Whether the horizontal collision check logic should attempt to eject the character should it detect that the character is potentially inside a collider.</param>
    /// <param name="ejectFromColliderOnY">Whether the vertical collision check logic should attempt to eject the character should it detect that the character is potentially inside a collider.</param>
    public virtual void Move(ref Vector3 velocity, char performCollisionCheckOnProvidedAxis = 'B', char ejectFromColliderOnProvidedAxis = 'N', bool moveImmediately = false, Space translateRelativeTo = Space.World, LayerMask? collisionMask = null)
    {
        CalculateMove(ref velocity, performCollisionCheckOnProvidedAxis, ejectFromColliderOnProvidedAxis, collisionMask);

        if (moveImmediately)
        {
            objectToControl.transform.Translate(velocity, translateRelativeTo);
        }
        else
        {
            if (translateRelativeTo == Space.Self)
            {
                var newPosition = _rigidbody2D.position + (Vector2)objectToControl.transform.TransformDirection(velocity);
                _rigidbody2D.MovePosition(newPosition);
            }
            else
            {
                _rigidbody2D.MovePosition(_rigidbody2D.position + (Vector2)velocity);
            }
        }
    }

    /// <summary>
    /// This move the object to a world position with looking for collision use with caution as Move is like what should be used.NOTE The physic system will still kick in on a rigidbody and Velocity will still be resolved 
    /// </summary>
    /// <param name="worldPosition">The Position to move to in world space</param>
    public virtual void MoveToPositionWithOutCollisions(Vector3 worldPosition)
    {
        _rigidbody2D.MovePosition(worldPosition);
    }

    /// <summary>
    /// Checks for collisions on the horizontal and vertical axes and determines the appropriate velocity considering.
    /// </summary>
    /// <param name="velocity">The velocity of the object prior to collision detection.</param>
    /// <param name="standingOnPlatform">Whether the character is standing on a platform that moves (i.e. has a controller)</param>
    /// <param name="performCollisionCheckOnProvidedAxis">Whether the movement logic should perform a collision check to ensure the movement doesn't enter a collider.</param>
    /// <param name="ejectFromColliderOnX">Whether the horizontal collision check logic should attempt to eject the character should it detect that the character is potentially inside a collider.</param>
    /// <param name="ejectFromColliderOnY">Whether the vertical collision check logic should attempt to eject the character should it detect that the character is potentially inside a collider.</param>
    protected virtual void CalculateMove(ref Vector3 velocity, char performCollisionCheckOnProvidedAxis = 'B', char ejectFromColliderOnProvidedAxis = 'N', LayerMask? collisionMask = null)
    {
        Boxcaster.UpdateCastOrigins();

        Collisions.Reset();
        Collisions.StartingVelocity = velocity;

        if (performCollisionCheckOnProvidedAxis == 'B' || performCollisionCheckOnProvidedAxis == 'X')
        {
            HorizontalCollisions(ref velocity, ejectFromColliderOnProvidedAxis == 'B' || ejectFromColliderOnProvidedAxis == 'X', true, collisionMask);
        }

        if (performCollisionCheckOnProvidedAxis == 'B' || performCollisionCheckOnProvidedAxis == 'Y')
        {
            VerticalCollisions(ref velocity, ejectFromColliderOnProvidedAxis == 'B' || ejectFromColliderOnProvidedAxis == 'Y', true, true, collisionMask);
        }

        if (StandingOnUpwardMovingPlatform && !Collisions.IsCollidingBelow)
        {
            Collisions.Below.SetColliding(GlobalData.OBSTACLE_LAYER, objectToControl.RegisteredPlatform.gameObject);
        }
    }
    #endregion

    #region Horizontal Collision Check
    /// <summary>
    /// Checks for any collision on the X-axis.
    /// </summary>
    /// <param name="velocity">The current velocity of the object.</param>
    protected virtual void HorizontalCollisions(ref Vector3 velocity, bool ejectFromCollider, bool drawRaycasts, LayerMask? collisionMask = null)
    {
        float xDirection = Mathf.Sign(velocity.x);

        Boxcaster.CalculateCastLength(velocity.x);
        Boxcaster.SetCastOrigin(Boxcaster.Origins.Center);

        var hitCount = 0;
        if (drawRaycasts)
        {
            hitCount = Boxcaster.FireAndDrawCast('X', xDirection, true, Color.red, RAYCAST_DRAW_TIME, collisionMask);

        }
        else
        {
            hitCount = Boxcaster.FireCastAll('X', xDirection, collisionMask);
        }

        if (hitCount > 0)
        {
            var hits = Boxcaster.Hits;
            var shortestHit = 999f;

            for (int i = 0; i < hits.Length; ++i)
            {
                if (hits[i].collider.gameObject.GetComponent<ObjectIsPassenger>() == null && hits[i].distance < shortestHit)
                {
                    shortestHit = hits[i].distance;

                    if (hits[i].distance == 0)
                    {
                        velocity.x = 0;

                        if (ejectFromCollider)
                        {
                            Vector3 pointOnCollider = StaticTools.ClosestPoint(hits[i].collider, hits[i].point);
                            Vector2 directionOfPoint = new Vector2(pointOnCollider.x, pointOnCollider.y) - hits[i].point;

                            objectToControl.transform.Translate(new Vector3((pointOnCollider.x - hits[i].point.x) + X_EJECT_DISTANCE * Mathf.Sign(directionOfPoint.x), 0, 0), Space.World);
                        }

                        return;
                    }

                    velocity.x = (hits[i].distance - CollisionCaster2D.SKIN_WIDTH) * xDirection;

                    if (xDirection == -1)
                    {
                        Collisions.Left.SetColliding(hits[i].collider.gameObject.layer, hits[i].collider.gameObject);
                    }
                    else
                    {
                        Collisions.Right.SetColliding(hits[i].collider.gameObject.layer, hits[i].collider.gameObject);
                    }
                }
            }
        }
    }
    #endregion

    #region Vertical Collision Check
    /// <summary>
    /// Checks for any collision on the Y-axis. This version contains handling for slopes.
    /// </summary>
    /// <param name="velocity">The current velocity of the object.</param>
    protected virtual void VerticalCollisions(ref Vector3 velocity, bool ejectFromCollider, bool checkForPlatform, bool drawRaycasts, LayerMask? collisionMask = null)
    {
        float yDirection = Mathf.Sign(velocity.y);

        Boxcaster.SetCastLength(CollisionCaster2D.SKIN_WIDTH + Mathf.Abs(velocity.y) + 0.05f);
        Boxcaster.SetCastOrigin(Boxcaster.Origins.Center);

        var hitCount = 0;
        if (drawRaycasts)
        {
            hitCount = Boxcaster.FireAndDrawCast('Y', yDirection, true, Color.red, RAYCAST_DRAW_TIME, collisionMask);
        }
        else
        {
            hitCount = Boxcaster.FireCastAll('Y', yDirection, collisionMask);
        }

        if (hitCount > 0)
        {
            var hits = Boxcaster.Hits;
            var shortestHit = 999f;

            for (int i = 0; i < hits.Length; ++i)
            {
                if (hits[i].collider.gameObject.GetComponent<ObjectIsPassenger>() == null && hits[i].distance < shortestHit)
                {
                    shortestHit = hits[i].distance;

                    if (hits[i].distance == 0)
                    {
                        if (!hits[i].collider.isTrigger)
                        {
                            velocity = Vector3.zero;

                            if (ejectFromCollider && TheGame.GetRef.gameState != TheGame.GameState.TRANSITION)
                            {
                                Vector3 pointOnCollider = StaticTools.ClosestPoint(hits[i].collider, hits[i].point);
                                Vector2 directionOfPoint = new Vector2(pointOnCollider.x, pointOnCollider.y) - hits[i].point;

                                objectToControl.transform.Translate(new Vector3(0, pointOnCollider.y - hits[i].point.y + Y_EJECT_DISTANCE * Mathf.Sign(directionOfPoint.y), 0));
                            }
                        }
                        else
                        {
                            return;
                        }
                    }

                    velocity.y = (hits[i].distance - CollisionRaycaster2D.SKIN_WIDTH) * yDirection;

                    if (yDirection == -1)
                    {
                        Collisions.Below.SetColliding(hits[i].collider.gameObject.layer, hits[i].collider.gameObject);
                    }
                    else if (yDirection == 1)
                    {
                        Collisions.Above.SetColliding(hits[i].collider.gameObject.layer, hits[i].collider.gameObject);
                    }

                    if (Collisions.IsCollidingBelow)
                    {
                        Collisions.BelowSurfaceNormal = hits[i].normal;
                    }
                }
            }
        }
    }
    #endregion
}

public class AboutToHitEvent : UnityEvent<GameObject>
{

}