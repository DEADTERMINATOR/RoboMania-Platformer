﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecallDestroyerFist : MonoBehaviour
{
    private const float RECALL_TIME = 25.0f;

    public delegate void RecallRequired();
    public event RecallRequired Recall;

    [HideInInspector]
    public bool FistFired = false;

    private float _fistActiveTime = 0f;

	
	private void Update()
    {
        if (FistFired)
            _fistActiveTime += Time.deltaTime;

        if (_fistActiveTime >= RECALL_TIME)
            InvokeRecall();
	}

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == GlobalData.OBSTACLE_LAYER)
            InvokeRecall();
    }

    private void OnColliderEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.layer == GlobalData.OBSTACLE_LAYER)
            InvokeRecall();
    }

    private void InvokeRecall()
    {
        Recall?.Invoke();
        FistFired = false;
        _fistActiveTime = 0f;
    }
}
