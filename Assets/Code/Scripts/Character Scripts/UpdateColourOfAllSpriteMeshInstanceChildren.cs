﻿using UnityEngine;
using System.Collections;
using Anima2D;

public class UpdateColourOfAllSpriteMeshInstanceChildren : MonoBehaviour
{
    public static Color AllWhite = new Color32(255,255,255,255);
    public static Color AllWhiteClear = new Color32(255, 255, 255, 0);

    [HideInInspector]
    public bool isClear;

    private SpriteMeshInstance[] PartList;


    private void Awake()
    {
        PartList = GetComponentsInChildren<SpriteMeshInstance>();
    }

    private void Start()
    {
        isClear = false;
    }


    public void UpdateAll(Color color)
    {
        isClear = false;

        foreach (SpriteMeshInstance piece in PartList)
            piece.color = color;
    }

    public void SetClear()
    {
        UpdateAll(AllWhiteClear);
        isClear = true;
    }

    public void SetWhite()
    {
        UpdateAll(AllWhite);
    }
}
