﻿using MovementEffects;
using System.Collections.Generic;
using UnityEngine;
using Characters.Sound;
using Characters.Enemies;
using Characters.Player;
using static GlobalData;
using System.Xml.Schema;
using UnityEngine.UIElements;

namespace Characters
{
    /// <summary>
    /// A component that handles behaviours specific to non-player characters.
    /// </summary>
    [RequireComponent(typeof(Controller2D))]
    [System.Serializable]
    public class NPC : Character, IDamageGiver, IActivatable, IGravityObject
    {
        #region NPC Gravity State
        protected class NPCGravityState : GravityState
        {
            public NPCGravityState(GravityType gravityType)
            {
                _gravityType = gravityType;
                switch (gravityType)
                {
                    case GravityType.Base:
                        _gravityVelocity = _baseGravityVelocity = new Vector2(0, -0.46f);//new Vector2(0, -55f);
                        _gravityReductionValue = 0f;
                        break;
                    case GravityType.Lava:
                        _gravityVelocity = _baseGravityVelocity = new Vector2(0, -7.5f);
                        _gravityReductionValue = -2f;
                        break;
                    case GravityType.Mud:
                        _gravityVelocity = _baseGravityVelocity = new Vector2(0, -5.0f);
                        _gravityReductionValue = 0f;
                        break;
                    default:
                        _gravityVelocity = _baseGravityVelocity = Vector2.zero;
                        _gravityReductionValue = 0f;
                        break;
                }
            }
        }
        #endregion

        #region Delegates
        public delegate void ActionEvent();
        public event ActionEvent SpriteSwitch;
        #endregion

        #region Constants
        /// <summary>
        /// The damage flash color converted from 0-255 RGBA value to 0-1 RGBA.
        /// </summary>
        protected Vector4 DAMAGE_FLASH_COLOUR = new Vector4(1, 50f / 255f, 0, 1);//new Vector4(1, 125f / 255f, 0, 1);

        /// <summary>
        /// The desired minimum distance from a target vector to be to consider itself as having arrived at the target.
        /// </summary>
        protected const float ARRIVAL_DISTANCE = 0.05f;
        #endregion

        public enum MovementType { Ground, Air };
        public enum StartingFacingDirection : int { Right = 1, Left = -1 }

        #region Public Field + Properties
        public bool ActivateOnStartDebug = false;

        public float BaseMoveSpeed = 6;

        /// <summary>
        /// The NPC current health.
        /// </summary>
        public float Health { get; protected set; }

        /// <summary>
        /// The maximum amount of health the NPC can have.
        /// </summary>
        public float MaxHealth;

        /// <summary>
        /// The direction the NPC should initially face when they spawn.
        /// </summary>
        [HideInInspector]
        public StartingFacingDirection StartFaceDir = StartingFacingDirection.Left;

        /// <summary>
        /// The target that the NPC is travelling towards.
        /// </summary>
        [HideInInspector]
        public Vector3 TargetVector;

        /// <summary>
        /// Whether the NPC is ready for a new waypoint.
        /// </summary>
        [HideInInspector]
        public bool ReadyForNewWaypoint;

        /// <summary>
        /// A flag to indicate whether and on what axis the charater's collision should be checked on.
        /// 'B' for both, 'X' and 'Y' for X and Y-axis only respectively, and 'N' for neither.
        /// </summary>
        [HideInInspector]
        public char DoCollisionCheckOnSpecifiedAxis = 'B';

        /// <summary>
        /// The waypoints the NPC will traverse between.
        /// </summary>
        [HideInInspector]
        public EditableWayPoints Waypoints;

        /// <summary>
        /// The area that the NPC can operate within.
        /// </summary>
        [HideInInspector]
        public Rect ActiveArea = Rect.zero;

        /// <summary>
        /// Whether the NPC has constructed a platform bounding box for the platform they're currently on.
        /// </summary>
        public bool HasPlatformBoundingBox { get; private set; } = false;

        /// <summary>
        /// The NPC's current movement represented as a virtual input.
        /// </summary>
        public override Vector2 MovementInput { get { return movementInput; } }

        public override Controller2D Controller { get { return controller; } }
        public override Controller2D.CollisionInfo Collisions { get { return controller.Collisions; } }
        public override BoxCollider2D MovementCollider { get { return movementCollider; } }
        public override GravityState CurrentActiveGravityState { get { return currentActiveGravityState; } }


        /// <summary>
        /// Public getter/setter for whether the NPC is activated.
        /// </summary>
        public bool Active { get { return activated; } set { activated = value; if (value == true) OnActivationSetup(); } }

        /// <summary>
        /// Shortcut for getting the facing direction of the NPC.
        /// </summary>
        public int FaceDir { get { return controller.Collisions.FaceDir; } }
        /// <summary>
        /// The game object that contains all the sprites that make up this character (if the character has only one sprite, this would be it).
        /// </summary>
        public GameObject CharacterSpriteCollection { get { return _characterSpriteCollection; } }

        /// <summary>
        /// The state machine that controls the NPCs behaviour.
        /// </summary>
        public StateMachine BehaviourStateMachine { get { return _behaviourStateMachine; } }

        /// <summary>
        /// A rectangle that represents the navigable area of the platform the NPC is currently on.
        /// </summary>
        public Rect PlatformBoundingBox { get { return platformBoundingBox; } }

        /// <summary>
        /// Whether the character has died (or are in the middle of their death animation).
        /// </summary>
        public bool IsDead { get; protected set; }


        public NPCShieldContainer Shield;
        #endregion

        #region Protected Fields + Properties
        /// <summary>
        /// Holds all the possible gravity states, and their associated values, that an NPC could encounter.
        /// </summary>
        protected Dictionary<GravityType, NPCGravityState> _gravityStates = new Dictionary<GravityType, NPCGravityState>();

        /// <summary>
        /// Whether this NPC moves in the air or is limited to ground movement.
        /// </summary>
        [SerializeField]
        protected MovementType _movementType;

        [SerializeField]
        protected bool _memberOfNPCGroup;

        [SerializeField]
        protected GameObject _characterSpriteCollection;

        protected StateMachine _behaviourStateMachine;

        /// <summary>
        /// Whether all operations for the NPC are "frozen" (i.e they perform none of their functionality).
        /// An example of the use for this would be to pause NPCs for effect when the player dies.
        /// </summary>
        protected bool frozen = false;

        /// <summary>
        /// Whether the NPC is activated.
        /// </summary>
        protected bool activated = false;

        /// <summary>
        /// Reference to the box collider used for movement collisions.
        /// </summary>
        protected BoxCollider2D movementCollider;

        /// <summary>
        /// A vector that represents the direction the NPC is currrently travelling in as a "virtual input".
        /// </summary>
        protected Vector2 movementInput;

        /// <summary>
        /// Holds the co-routine handle for the damage flash. Used to test if there is a flash already in effect before we start another one.
        /// </summary>
        protected CoroutineHandle damageFlashHandle;

        /// <summary>
        /// Reference to the NPC's animation controller.
        /// </summary>
        protected INPCAnimationController npcAnim;

        /// <summary>
        /// Reference to the controller for the NPC.
        /// </summary>
        protected Controller2D controller;

        /// <summary>
        /// Reference to the animator for the NPC.
        /// </summary>
        protected Animator animator;

        /// <summary>
        /// Reference to the sound managers for the NPC.
        /// </summary>
        protected CharacterSounds soundManager;

        /// <summary>
        /// Holds the gravity state the NPC is currently experiencing.
        /// </summary>
        protected NPCGravityState currentActiveGravityState;

        /// <summary>
        /// A rect that encompasses the bounds of the platform the NPC is currently on.
        /// </summary>
        protected Rect platformBoundingBox;

        /// <summary>
        /// The NPCs controller cast as a GroundController2D, if the NPC is a ground type.
        /// </summary>
        protected GroundController2D groundController;
        #endregion

        #region Private Variables
        [SerializeField]
        private bool _canFallOffPlatforms = false;

        /// <summary>
        /// Whether the NPC should analyze the platform they are on to find the edges and produce a platform bounding box.
        /// </summary>
        [SerializeField]
        private bool _shouldAnalyzePlatform = false;

        /// <summary>
        /// The maximum distance the NPC should analyze a platform for. Platforms larger than this value will have their bounding boxes set at the edge of this distance.
        /// </summary>
        [SerializeField]
        private float _maxPlatformAnalysisDistance = 50f;

        /// <summary>
        /// Holds the sign of the movement collider offset so it can be reapplied when the collider is switched on a sprite flip.
        /// </summary>
        private float _colliderXOffsetSign;
        #endregion

        #region Unity Methods
        protected virtual void Awake()
        {
            _gravityStates.Add(GravityType.Base, new NPCGravityState(GravityType.Base));
            _gravityStates.Add(GravityType.Lava, new NPCGravityState(GravityType.Lava));
            _gravityStates.Add(GravityType.Mud, new NPCGravityState(GravityType.Mud));

            currentActiveGravityState = _gravityStates[GravityType.Base];

            controller = GetComponent<Controller2D>();
            if (_movementType == MovementType.Ground)
            {
                groundController = (GroundController2D)controller;
                groundController.ShouldTurnToNotFallOffPlatfrom = _canFallOffPlatforms;
            }

            if (!Waypoints)// Only look for waypoints if it is null 
            {
                Waypoints = GetComponent<EditableWayPoints>();
            }

            if(CharacterSpriteCollection == null)
            {
                Debug.Log(name);
            }
            animator = CharacterSpriteCollection?.GetComponent<Animator>();
            movementCollider = GetComponent<BoxCollider2D>();
            _colliderXOffsetSign = Mathf.Sign(movementCollider.offset.x);

            if (ActivateOnStartDebug)
            {
                Active = true;
            }

            InstantiateStateMachine();
        }

        protected virtual void Start()
        {
            SetSpriteDirection((int)StartFaceDir == 1 ? true : false, false);

            BaseMoveSpeed = Mathf.Abs(BaseMoveSpeed);
            MoveSpeed = BaseMoveSpeed;

            ReadyForNewWaypoint = true;

            npcAnim = GetComponentInChildren<INPCAnimationController>();

            if (_shouldAnalyzePlatform)
            {
                AnalyzePlatform(true);
            }

            GlobalData.Player.Dead += OnPlayerDead;
        }

        protected virtual void FixedUpdate()
        {
            if (!frozen && !IsDead)
            {
                if (!(RegisteredPlatform != null && RegisteredPlatform.CurrentVelocityDirection.y > 0))
                {
                    //Apply gravity to the Y-velocity.
                    AddToVelocity(currentActiveGravityState.GravityVelocity * GravityScale);
                }

                var finalVelocity = CalculateFrameVelocity();

                Controller.Move(ref finalVelocity, DoCollisionCheckOnSpecifiedAxis, 'N', false, MoveCharacterInLocalSpace == true ? Space.Self : Space.World);
                if (Controller.Collisions.IsCollidingAbove || Controller.Collisions.IsCollidingBelow)
                {
                    ZeroOutVelocityOnOneAxis('Y'); //Zero out y velocity when we collide either from above or below to prevent a buildup.
                }

                if (_shouldAnalyzePlatform && (platformBoundingBox.width == 0 || !platformBoundingBox.Contains(transform.position)))
                {
                    AnalyzePlatform(true);
                }

                Enemy enemy = this as Enemy;
                if (activated && !enemy)
                {
                    BehaviourStateMachine?.RunUpdate();
                }
            }
        }
        #endregion

        public virtual void InstantiateStateMachine() { }

        #region Sprite Manipulation
        /// <summary>
        /// Override to perform any additional functionality required by NPCs when flipping the sprite.
        /// </summary>
        public virtual bool SetSpriteDirection()
        {
            bool didSwitchOccur = false;
            if (!Controller.Collisions.IsCollidingLeft && !Controller.Collisions.IsCollidingRight)
            {
                if (MovementInput.x < 0 && Controller.Collisions.FaceDir == 1) //Facing right and we want to face left.
                {
                    Controller.Collisions.FaceDir = -1;
                    CharacterSpriteCollection.transform.localScale = new Vector3(Mathf.Abs(CharacterSpriteCollection.transform.localScale.x), CharacterSpriteCollection.transform.localScale.y, CharacterSpriteCollection.transform.localScale.z);
                    movementCollider.offset = new Vector2(Mathf.Abs(movementCollider.offset.x) * _colliderXOffsetSign, movementCollider.offset.y);
                    didSwitchOccur = true;
                }
                else if (MovementInput.x > 0 && Controller.Collisions.FaceDir == -1) //Facing left and we want to face right.
                {
                    Controller.Collisions.FaceDir = 1;
                    CharacterSpriteCollection.transform.localScale = new Vector3(-Mathf.Abs(CharacterSpriteCollection.transform.localScale.x), CharacterSpriteCollection.transform.localScale.y, CharacterSpriteCollection.transform.localScale.z);
                    movementCollider.offset = new Vector2(-Mathf.Abs(movementCollider.offset.x) * _colliderXOffsetSign, movementCollider.offset.y);
                    didSwitchOccur = true;
                }
            }

            if (didSwitchOccur)
            {
                SpriteSwitch?.Invoke();
            }

            return didSwitchOccur;
        }

        /// <summary>
        /// Override to perform any additional functionality required by NPCs when flipping the sprite to a specified direction.
        /// </summary>
        /// <param name="switchToRight"></param>
        public virtual bool SetSpriteDirection(bool switchToRight, bool safetyCheck = true, bool isAiming = false)
        {
            bool didSwitchOccur = false;
            if ((safetyCheck && switchToRight && Controller.Collisions.FaceDir == -1) || (!safetyCheck && switchToRight)) //The character is facing left and we want it to face right.
            {
                Controller.Collisions.FaceDir = 1;
                CharacterSpriteCollection.transform.localScale = new Vector3(-Mathf.Abs(CharacterSpriteCollection.transform.localScale.x), CharacterSpriteCollection.transform.localScale.y, CharacterSpriteCollection.transform.localScale.z);
                movementCollider.offset = new Vector2(-Mathf.Abs(movementCollider.offset.x) * _colliderXOffsetSign, movementCollider.offset.y);
                didSwitchOccur = true;
            }
            else if ((safetyCheck && !switchToRight && Controller.Collisions.FaceDir == 1) || (!safetyCheck && !switchToRight)) //The character is facing right and we want it to face left.
            {
                Controller.Collisions.FaceDir = -1;
                CharacterSpriteCollection.transform.localScale = new Vector3(Mathf.Abs(CharacterSpriteCollection.transform.localScale.x), CharacterSpriteCollection.transform.localScale.y, CharacterSpriteCollection.transform.localScale.z);
                movementCollider.offset = new Vector2(Mathf.Abs(movementCollider.offset.x) * _colliderXOffsetSign, movementCollider.offset.y);
                didSwitchOccur = true;
            }

            if (didSwitchOccur)
            {
                SpriteSwitch?.Invoke();
            }

            return didSwitchOccur;
        }

        protected IEnumerator<float> ApplyDamageFlash()
        {
            UpdateColourOfAllSpriteMeshInstanceChildren meshController = CharacterSpriteCollection?.GetComponent<UpdateColourOfAllSpriteMeshInstanceChildren>();

            meshController?.UpdateAll(DAMAGE_FLASH_COLOUR);
            yield return Timing.WaitForSeconds(0.1f);
            meshController?.UpdateAll(UpdateColourOfAllSpriteMeshInstanceChildren.AllWhite);
        }
        #endregion

        #region Damage
        /// <summary>
        /// An ovveride to inject a call to the SoudManager
        /// </summary>
        /// <param name="giver"></param>
        /// <param name="amount"></param>
        /// <param name="type"></param>
        public override void TakeDamage(IDamageGiver giver, float amount, DamageType type, Vector3 hitPoint)
        {
            soundManager?.PlayHitSound();
            NotifyDamageReceived(hitPoint);
        }

        public void SetDead(bool dead)
        {
            IsDead = dead;
            if (dead)
                NotifyDead();
        }
        #endregion

        #region Getters + Setters
        public bool GetActivated()
        {
            return activated;
        }

        public void SetActivated(bool activeStatus)
        {
            activated = activeStatus;

            if (BehaviourStateMachine != null)
                BehaviourStateMachine.Active = activeStatus;
        }

        public bool GetFrozen()
        {
            return frozen;
        }

        public void SetFrozen(bool frozenStatus)
        {
            frozen = frozenStatus;
        }

        /// <summary>
        /// Sets what the equivalent input would be based on the NPC's velocity (e.g. (1, 0) if they are moving to the right).
        /// </summary>
        /// <param name="input">The new input value for the NPC.</param>
        public void SetInput(Vector2 input)
        {
            movementInput = input;
        }

        /// <summary>
        /// Sets what the equivalent input would be based on NPC's movement (e.g. (1, 0) if they are moving to the right).
        /// This version allows the setting of each axis individually.
        /// </summary>
        /// <param name="axis">The axis whose input is to be set. Either X or Y in 2D.</param>
        /// <param name="value">The value the axis is to be set to.</param>
        public void SetInputOnOneAxis(char axis, float value)
        {
            switch (axis)
            {
                case 'X':
                    movementInput.x = value;
                    break;
                case 'Y':
                    movementInput.y = value;
                    break;
            }
        }
        #endregion

        #region Other
        /// <summary>
        /// Recalculates the character's gravity and jump velocity given the current gravity type they are experiencing.
        /// </summary>
        /// <param name="gravityType">The type of gravity the character is experiencing.</param>
        public virtual void ChangeGravityType(GravityType gravityType)
        {
            if (_gravityStates.Count != 0) //TODO:Temp fix for empty _gravityStates
            {
                switch (gravityType)
                {
                    case GravityType.Base:
                        currentActiveGravityState = _gravityStates[GravityType.Base];
                        break;
                    case GravityType.Lava:
                        currentActiveGravityState = _gravityStates[GravityType.Lava];
                        break;
                    case GravityType.Mud:
                        currentActiveGravityState = _gravityStates[GravityType.Mud];
                        break;
                }
            }
        }

        /// <summary>
        /// For set up after reciving spawner data 
        /// </summary>
        public virtual void PostSpawnSetup()
        {

        }

        /// <summary>
        /// Handles anything that needs to be performed once the NPC is set to actually appear on the screen.
        /// </summary>
        public virtual void OnActivationSetup()
        {

        }

        protected void ResetMovementParameters()
        {
            movementInput = Vector2.zero;
            ZeroOutVelocity();
        }

        private void OnPlayerDead()
        {
            SetFrozen(true);
            GlobalData.Player.Dead -= OnPlayerDead;
        }

        public virtual void Activate(GameObject activator)
        {
            Active = true;
        }

        public virtual void Deactivate(GameObject deactivator)
        {
            Active = false;
        }

        /// <summary>
        ///  how MoveOnPlatfrom decides to turn around default will turn at wall and platform edges 
        /// </summary>
        /// <returns></returns>
        public virtual bool ShouldTurnAround()
        {
            if (HasPlatformBoundingBox)
            {
                if ((Controller.Collisions.FaceDir == -1 && MovementCollider.bounds.min.x <= PlatformBoundingBox.xMin) || (Controller.Collisions.FaceDir == 1 && MovementCollider.bounds.max.x >= PlatformBoundingBox.xMax))
                {
                    return true;
                }
                return false;
            }
            else if (groundController != null)
            {
                return groundController.ShouldTurnToNotFallOffPlatfrom || (controller.Collisions.Left.IsColliding && Controller.Collisions.FaceDir == -1) || (controller.Collisions.Right.IsColliding && Controller.Collisions.FaceDir == 1);
            }

            return false;
        }
        /// <summary>
        /// moves the NPC on a platform by the MoveSpeed and will turn around based on ShouldTrunAround
        /// </summary>
        /// <returns>True(for state machine actions)</returns>
        public bool MoveOnPlatfrom()
        {
            float XDir = Mathf.Sign(Velocity.x);
            if (ShouldTurnAround())
            {
                SetSpriteDirection(Controller.Collisions.FaceDir != 1, true);
            }

            if (Controller.Collisions.FaceDir == -1)
            {
                SetVelocityOnOneAxis('X', MoveSpeed * -1f);
            }
            else
            {
                SetVelocityOnOneAxis('X', MoveSpeed);
            }
            return true;
        }
        #endregion

        #region Behaviours

        #region Movement Behaviours
        /// <summary>
        /// Checks if the NPC has reached its target, and if it hasn't, moves it closer to it.
        /// </summary>
        /// <param name="axis">The char representing the axis the move towards operates on. 'X' for x-axix, 'Y' for y-axis, 'B' for both axes.</param>
        /// <param name="returnSuccessOnlyOnCompletion">If true, the function will only return true if the NPC has reached its target.</param>
        /// <returns>Always true if this is not treated as a continued action. Otherwise, false if the NPC doesn't reach its target, and true if it does.</returns>
        public virtual bool MoveTowardsOnOneAxis(object axis, object returnSuccessOnlyOnCompletion)
        {
            char castedAxis = (char)axis;
            bool castedReturnSuccess = (bool)returnSuccessOnlyOnCompletion;

            Vector3 targetDirection = Vector3.zero;

            //Check if we've reached the target.
            if (castedAxis == 'X')
            {
                if (Mathf.Abs(transform.position.x - TargetVector.x) <= ARRIVAL_DISTANCE)
                {
                    return true;
                }

                targetDirection.x = TargetVector.x - transform.position.x < 0 ? -1 : 1;
            }
            else if (castedAxis == 'Y')
            {
                if (Mathf.Abs(transform.position.y - TargetVector.y) <= ARRIVAL_DISTANCE)
                {
                    return true;
                }

                targetDirection.y = TargetVector.y - transform.position.y < 0 ? -1 : 1;
            }
            else
            {
                if (Vector3.Distance(transform.position, TargetVector) <= ARRIVAL_DISTANCE)
                {
                    return true;
                }

                targetDirection = TargetVector - transform.position;
            }

            //We haven't reached the target, so keep moving towards it.
            Vector3 frameVelocity = targetDirection.normalized * MoveSpeed;

            //Set the NPC input and velocity based on the target direction and the selected axes of movement.
            if (castedAxis == 'X' || castedAxis == 'B')
            {
                if (frameVelocity.x < 0)
                {
                    movementInput.x = -1;
                }
                else if (frameVelocity.x > 0)
                {
                    movementInput.x = 1;
                }
                else
                {
                    movementInput.x = 0;
                }

                SetVelocityOnOneAxis('X', frameVelocity.x);
            }

            if (castedAxis == 'Y' || castedAxis == 'B')
            {
                if (frameVelocity.y < 0)
                {
                    movementInput.y = -1;
                }
                else if (frameVelocity.y > 0)
                {
                    movementInput.y = 1;
                }
                else
                {
                    movementInput.y = 0;
                }

                SetVelocityOnOneAxis('Y', frameVelocity.y);
            }

            if (castedReturnSuccess)
                return false;
            return true;
        }
        #endregion

        #region Target Behaviours
        /// <summary>
        /// If the NPC is in need of another waypoint, advances the NPCs target to the next waypoint it should travel towards.
        /// </summary>
        public virtual bool SetWaypoint()
        {
            WayPointsComponent simpleWaypoints = Waypoints as WayPointsComponent;
            if (ReadyForNewWaypoint)
            {
                TargetVector = simpleWaypoints.WayPoints.Increment();
                ReadyForNewWaypoint = false;
            }
            return true;
        }
        #endregion

        #endregion

        protected virtual bool AnalyzePlatform(bool drawDebugInfo = false)
        {
            if (groundController != null)
            {
                groundController.Raycaster.SetCastOrigin(groundController.Raycaster.Origins.BottomCenter);
                groundController.Raycaster.SetCastLength(0.15f);

                var checkForGroundBelow = groundController.Raycaster.FireAndDrawCastAndReturnFirstResult('Y', -1, Color.magenta, 10.0f, OBSTACLE_LAYER_SHIFTED);
                if (checkForGroundBelow)
                {
                    groundController.Raycaster.SetCastOrigin(groundController.Raycaster.Origins.BottomLeft);
                    groundController.Raycaster.SetCastLength(50.0f);
                    var leftObstacle = groundController.Raycaster.FireAndDrawCastAndReturnFirstResult('X', -1, Color.blue, 10.0f, OBSTACLE_LAYER_SHIFTED);

                    groundController.Raycaster.SetCastOrigin(groundController.Raycaster.Origins.BottomRight);
                    var rightObstacle = groundController.Raycaster.FireAndDrawCastAndReturnFirstResult('X', 1, Color.blue, 10.0f, OBSTACLE_LAYER_SHIFTED);

                    bool leftEdgeFound = false;
                    bool rightEdgeFound = false;

                    RaycastHit2D leftEdgeHit = new RaycastHit2D();
                    RaycastHit2D rightEdgeHit = new RaycastHit2D();

                    float offsetCount = 1;

                    groundController.Raycaster.SetCastLength(0.25f);
                    while (!leftEdgeFound || !rightEdgeFound)
                    {
                        if (!leftEdgeFound)
                        {
                            groundController.Raycaster.SetCastOrigin(groundController.Raycaster.Origins.BottomCenter);
                            groundController.Raycaster.OffsetRayOrigin('Y', -0.5f * offsetCount);
                            var currentLeftHit = groundController.Raycaster.FireAndDrawCastAndReturnFirstResult('Y', -1, Color.red, 10.0f, OBSTACLE_LAYER_SHIFTED);
                            if (!currentLeftHit || (leftObstacle && currentLeftHit.point.x <= leftObstacle.point.x))
                            {
                                leftEdgeFound = true;
                            }
                            else
                            {
                                leftEdgeHit = currentLeftHit;
                            }
                        }

                        if (!rightEdgeFound)
                        {
                            groundController.Raycaster.SetCastOrigin(groundController.Raycaster.Origins.BottomCenter);
                            groundController.Raycaster.OffsetRayOrigin('Y', 0.5f * offsetCount);
                            var currentRightHit = groundController.Raycaster.FireAndDrawCastAndReturnFirstResult('Y', -1, Color.red, 10.0f, OBSTACLE_LAYER_SHIFTED);
                            if (!currentRightHit || (rightObstacle && currentRightHit.point.x >= rightObstacle.point.x))
                            {
                                rightEdgeFound = true;
                            }
                            else
                            {
                                rightEdgeHit = currentRightHit;
                            }
                        }

                        ++offsetCount;
                    }

                    if (drawDebugInfo)
                    {
                        VisualDebug.DrawDebugPoint(leftEdgeHit.point, Color.yellow);
                        VisualDebug.DrawDebugPoint(rightEdgeHit.point, Color.yellow);
                    }

                    platformBoundingBox = new Rect(leftEdgeHit.point.x, leftEdgeHit.point.y, rightEdgeHit.point.x - leftEdgeHit.point.x, 5.0f);

                    if (drawDebugInfo)
                    {
                        VisualDebug.DrawBox(platformBoundingBox.center, new Vector2(platformBoundingBox.size.x / 2, platformBoundingBox.size.y / 2), Quaternion.identity, Color.cyan, 10.0f);
                    }

                    HasPlatformBoundingBox = true;
                    return true;
                }
            }

            HasPlatformBoundingBox = false;
            return false;
        }
    }
}
