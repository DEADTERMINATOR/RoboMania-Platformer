﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;

public class NPCGroup : MonoBehaviour
{
    /// <summary>
    /// The NPCs that will be part of this group.
    /// </summary>
    public NPC[] NPCs;
}
