﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class Timekeeper : MonoBehaviour
{
    public class TimedAction
    {
        public Action ActionToExecute;
        public float WaitTime;
        public string ActionName;
        public bool KillPreviousActionsOfSameName;

        public TimedAction(Action actionToExecute, float waitTime, string actionName = "", bool killPreviousActionsOfSameName = false)
        {
            ActionToExecute = actionToExecute;
            WaitTime = waitTime;
            ActionName = actionName;
            KillPreviousActionsOfSameName = killPreviousActionsOfSameName;
        }
    }


    public CoroutineHandle StartTimer(Action actionToExecute, float waitTime, string actionName = "", bool killPreviousActionsOfSameName = false)
    {
        if (killPreviousActionsOfSameName)
            Timing.KillCoroutines(actionName);
        return Timing.RunCoroutine(RunTimer(actionToExecute, waitTime), actionName);
    }

    public CoroutineHandle StartTimer(params TimedAction[] actionsToExecute)
    {
        foreach (TimedAction action in actionsToExecute)
        {
            if (action.KillPreviousActionsOfSameName)
                Timing.KillCoroutines(action.ActionName);
        }
        return Timing.RunCoroutine(RunTimer(actionsToExecute));
    }

    public void EndTimer(string actionName = "", float waitTime = 0)
    {
        if (waitTime == 0)
            Timing.KillCoroutines(actionName);
        else
            Timing.RunCoroutine(RunTimer(() => { EndTimer(actionName, 0); }, waitTime));
    }


    private IEnumerator<float> RunTimer(Action actionToExecute, float waitTime)
    {
        yield return Timing.WaitForSeconds(waitTime);
        actionToExecute.Invoke();
    }

    private IEnumerator<float> RunTimer(params TimedAction[] actionsToExecute)
    {
        foreach (TimedAction action in actionsToExecute)
        {
            yield return Timing.WaitForSeconds(action.WaitTime);
            action.ActionToExecute.Invoke();
        }
    }
}
