﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBoxcaster2D : CollisionCaster2D
{
    /// <summary>
    /// The desired size for the next boxcast.
    /// </summary>
    private Vector2 _boxSize;


    protected override void Awake()
    {
        base.Awake();
        SetBoxSize(Collider.bounds.size);
    }

    /*
    /// <summary>
    /// Sets the boxcast origin to the correct position based on the axis and the direction of travel on that axis.
    /// </summary>
    /// <param name="axis">The axis the boxcast will occur on. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction of travel along the chosen axis. Positive or negative.</param>
    public override void SetCastOrigin(char axis, float axisDirection)
    {
        //TODO: This method currently places the origins incorrectly. It needs to be fixed. Until it is fixed (and this message removed), use SetCastOrigin(Origins.Center). The default size of the box is equivalent to the size of the collider, so set
        switch (axis)
        {
            case 'X':
                CastOrigin = axisDirection == 1 ? new Vector2(Origins.TopRight.x, Origins.Center.y) : new Vector2(Origins.TopLeft.x, Origins.Center.y);
                break;
            case 'Y':
                CastOrigin = axisDirection == 1 ? new Vector2(Origins.Center.x, Origins.TopRight.y) : new Vector2(Origins.Center.x, Origins.BottomRight.y);
                break;
        }
    }
    */

    /// <summary>
    /// Offsets the origin of the boxcast by the specified offset amount.
    /// </summary>
    /// <param name="offset">The offset to apply to the origin.</param>
    public void OffsetCastOrigin(Vector2 offset)
    {
        CastOrigin += offset;
    }

    /// <summary>
    /// Sets the size of the box cast.
    /// </summary>
    /// <param name="size">The size of the box cast.</param>
    public void SetBoxSize(Vector2 size)
    {
        _boxSize = size;
    }

    /// <summary>
    /// Fires a box cast from the set position with the set size. Only gets the first collision result.
    /// </summary>
    /// <param name="axis">The axis the box cast should be fired along. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction the box cast will travel along the specified axis. Either 1 for position or -1 for negative.</param>
    /// <param name="collisionMask">The layer mask to use to determine which layers the box cast should respond to. Uses the member layer mask if null.</param>
    /// <returns>The number of objects the cast collided with. Will be 1 or 0.</returns>
    public override int FireCast(char axis, float axisDirection, LayerMask? collisionMask = null)
    {
        int resultCount = 0;
        _filter.layerMask = collisionMask ?? CollisionMask;

        switch (axis)
        {
            case 'X':
                resultCount = Physics2D.BoxCast(CastOrigin, _boxSize, 0, axisDirection == 1 ? Vector2.right : Vector2.left, _filter, _hits, _castLength);
                break;
            case 'Y':
                resultCount = Physics2D.BoxCast(CastOrigin, _boxSize, 0, axisDirection == 1 ? Vector2.up : Vector2.down, _filter, _hits, _castLength);
                break;
        }
        return resultCount;
    }

    /// <summary>
    /// Fires a box cast from the set position with the set size. Gets as many collision results as possible, up to MaximumAllowedNumbedOfCollisionsPerCast.
    /// </summary>
    /// <param name="axis">The axis the box cast should be fired along. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction the box cast will travel along the specified axis. Either 1 for position or -1 for negative.</param>
    /// <param name="collisionMask">The layer mask to use to determine which layers the box cast should respond to. Uses the member layer mask if null.</param>
    /// <returns>The number of objects the cast collided with. Will be anywhere from 0 to MaximumAllowedNumberOfCollisionsAllowed.</returns>
    public override int FireCastAll(char axis, float axisDirection, LayerMask? collisionMask = null)
    {
        int resultCount = 0;
        LayerMask collisionMaskToUse = collisionMask ?? CollisionMask;

        switch (axis)
        {
            case 'X':
                resultCount = Physics2D.BoxCastNonAlloc(CastOrigin, _boxSize, 0, axisDirection == 1 ? Vector2.right : Vector2.left, _hits, _castLength, collisionMaskToUse);
                break;
            case 'Y':
                resultCount = Physics2D.BoxCastNonAlloc(CastOrigin, _boxSize, 0, axisDirection == 1 ? Vector2.up : Vector2.down, _hits, _castLength, collisionMaskToUse);
                break;
        }
        return resultCount;
    }

    /// <summary>
    /// Fires a box cast from the set position with the set size. Directly returns the first collision result instead of the number of collision results.
    /// </summary>
    /// <param name="axis">The axis the box cast should be fired along. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction the box cast will travel along the specified axis. Either 1 for position or -1 for negative.</param>
    /// <param name="collisionMask">The layer mask to use to determine which layers the box cast should respond to. Uses the member layer mask if null.</param>
    /// <returns>The results of the raycast as a RaycastHit2D.</returns>
    public override RaycastHit2D FireCastAndReturnFirstResult(char axis, float axisDirection, LayerMask? collisionMask = null)
    {
        int resultCount = FireCast(axis, axisDirection, collisionMask);
        if (resultCount != 0)
        {
            return _hits[0];
        }

        return _emptyHit;
    }

    /// <summary>
    /// Draws the upcoming box cast in the scene view of the editor before firing the cast itself.
    /// </summary>
    /// <param name="axis">The axis the box cast should be fired along. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction the box cast will travel along the specified axis. Either 1 for position or -1 for negative.</param>
    /// <param name="shouldCastAll">Whether the box cast method that captures multiple collisions in one cast should be used.</param>
    /// <param name="color">The color the raycast will be drawn in.</param>
    /// <param name="duration">An optional duration for the raycast to remain drawn in the scene view.</param>
    /// <param name="collisionMask">The layer mask to use to determine which layers the raycast should respond to. Uses the member layer mask if null.</param> 
    /// <returns>The number of objects the cast collided with. Depending on whether shouldCastAll is true, this can be a maximum of 1 or up to MaximumAllowedNumberOfCollisionsAllowed.</returns>
    public override int FireAndDrawCast(char axis, float axisDirection, bool shouldCastAll, Color color, float duration = 0, LayerMask? collisionMask = null)
    {
        Vector2 castDirection = Vector2.zero;
        switch (axis)
        {
            case 'X':
                castDirection = axisDirection == 1 ? Vector2.right : Vector2.left;
                break;
            case 'Y':
                castDirection = axisDirection == 1 ? Vector2.up : Vector2.down;
                break;
        }

        VisualDebug.DrawBoxCast2D(CastOrigin, _boxSize, 0, castDirection, _castLength, color, duration);

        if (shouldCastAll)
        {
            return FireCastAll(axis, axisDirection, collisionMask);
        }
        return FireCast(axis, axisDirection, collisionMask);
    }

    /// <summary>
    /// Draws the upcoming box cast in the scene view of the editor before firing the cast itself. Directly returns the first collision result instead of the number of collision results.
    /// </summary>
    /// <param name="axis">The axis the box cast should be fired along. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction the box cast will travel along the specified axis. Either 1 for position or -1 for negative.</param>
    /// <param name="shouldCastAll">Whether the box cast method that captures multiple collisions in one cast should be used.</param>
    /// <param name="color">The color the raycast will be drawn in.</param>
    /// <param name="duration">An optional duration for the raycast to remain drawn in the scene view.</param>
    /// <param name="collisionMask">The layer mask to use to determine which layers the raycast should respond to. Uses the member layer mask if null.</param> 
    /// <returns>The results of the raycast as a RaycastHit2D.</returns>
    public override RaycastHit2D FireAndDrawCastAndReturnFirstResult(char axis, float axisDirection, Color color, float duration = 0, LayerMask? collisionMask = null)
    {
        int resultCount = FireAndDrawCast(axis, axisDirection, false, color, duration, collisionMask);
        if (resultCount != 0)
        {
            return _hits[0];
        }

        return _emptyHit;
    }
}
