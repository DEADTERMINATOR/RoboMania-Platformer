﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalData;

[RequireComponent(typeof(Collider2D))]
public class CollisionRaycaster2D : CollisionCaster2D
{
    #region Constants
    /// <summary>
    /// The minimum desired distance between rays on either the X or Y axis.
    /// </summary>
    private const float DESIRED_DISTANCE_BETWEEN_RAYS = 0.15f;

    /// <summary>
    /// The minimum number of rays that should be present on an object with a raycaster.
    /// </summary>
    private const int MINIMUM_RAY_COUNT = 4;
    #endregion

    #region Public Variables
    /// <summary>
    /// Whether the raycasts should be fired according to the normal of the attached game object
    /// (e.g. the game object is rotated 45 degrees, so the raycasts are fired at 45 degrees), or the global normal.
    /// </summary>
    public bool RaycastAlongGameObjectNormal = false;

    /// <summary>
    /// The number of rays that will be fired out horizontally.
    /// </summary>
    [HideInInspector]
    public int HorizontalRayCount = 4;

    /// <summary>
    /// The number of rays that will be fired out vertically.
    /// </summary>
    [HideInInspector]
    public int VerticalRayCount = 4;

    /// <summary>
    /// The amount of vertical spacing that should be in between each ray being fired horizontally.
    /// </summary>
    [HideInInspector]
    public float HorizontalRaySpacing;

    /// <summary>
    /// The amount of horizontal spacing that should be in between each ray being fired vertically.
    /// </summary>
    [HideInInspector]
    public float VerticalRaySpacing;

    /// <summary>
    /// Whether a switch in the raycounts is needed (e.g. The collider has rotated, so the horizontal count should become the vertical count and vice versa).
    /// </summary>
    [HideInInspector]
    public bool RayCountSwitchNeeded;
    #endregion

    #region Unity Methods
    protected override void Awake()
    {
        base.Awake();
        CalculateRaySpacing();
    }

    private void Update()
    {
        if (RayCountSwitchNeeded)
        {
            int tempHorizontalCount = HorizontalRayCount;
            HorizontalRayCount = VerticalRayCount;
            VerticalRayCount = tempHorizontalCount;

            RayCountSwitchNeeded = false;
        }
    }
    #endregion

    #region Public Methods

    #region Ray Length + Positioning
    /// <summary>
    /// Sets the ray origin to the correct raycast origin based on the axis and the direction of travel on that axis.
    /// </summary>
    /// <param name="axis">The axis the raycast will occur on. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction of travel along the chosen axis. Positive or negative.</param>
    public void SetCastOrigin(char axis, float axisDirection)
    {
        switch (axis)
        {
            case 'X':
                CastOrigin = axisDirection == -1 ? Origins.BottomLeft : Origins.BottomRight;
                break;
            case 'Y':
                CastOrigin = axisDirection == -1 ? Origins.BottomLeft : Origins.TopLeft;
                break;

        }
    }
    /// <summary>
    /// Sets the ray origin to the correct raycast origin based on the axis and the direction of travel on that axis.
    /// </summary>
    /// <param name="normalizedVelocity">uses a unity vector of the velocity.</param>
 
    public override void SetCastOrigin(Vector2 normalizedVelocity)
    {
        if (normalizedVelocity.y > 0)
        {
            if (normalizedVelocity.x > 0)
            {
                CastOrigin = Origins.TopRight;
                return;
            }
            else if (normalizedVelocity.x < 0)
            {
                CastOrigin = Origins.TopLeft;
                return;
            }
            CastOrigin = Origins.TopLeft;
        }
        else 
        {
            if (normalizedVelocity.x > 0)
            {
                CastOrigin = Origins.BottomRight;
                return;
            }
            else 
            {
                CastOrigin = Origins.BottomLeft;
                return;
            }
          
        }
    }
    /// <summary>
    /// Offsets the ray origin along the chosen axis by the provided offset value.
    /// </summary>
    /// <param name="axis">The axis the raycast will occur on. Either X or Y in 2D.</param>
    /// <param name="offset">The offset that will be appllied to the ray's base origin point.</param>
    public void OffsetRayOrigin(char axis, float offset)
    {
        switch (axis)
        {
            case 'X':
                if (RaycastAlongGameObjectNormal)
                {
                    CastOrigin += (Vector2)transform.up * offset;
                }
                else
                {
                    CastOrigin += Vector2.up * offset;
                }
                break;
            case 'Y':
                if (RaycastAlongGameObjectNormal)
                {
                    CastOrigin += (Vector2)transform.right * offset;
                }
                else
                {
                    CastOrigin += Vector2.right * offset;
                }
                break;
        }
    }
    #endregion

    #region Fire Raycast
    /// <summary>
    /// Fires the raycast along the provided axis with the currently set ray origin and length.
    /// </summary>
    /// <param name="axis">The axis the raycast will occur on. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction of travel along the chosen axis. Positive or negative.</param>
    /// <param name="collisionMask">The layer mask to use to determine which layers the raycast should respond to. Uses the member layer mask if null.</param>
    /// <returns>The number of colliders the raycast hit, either 0 or 1.</returns>
    public override int FireCast(char axis, float axisDirection, LayerMask? collisionMask = null)
    {
        int resultCount = 0;
        _filter.layerMask = collisionMask ?? CollisionMask;

        switch (axis)
        {
            case 'X':
                if (RaycastAlongGameObjectNormal)
                {
                    resultCount = Physics2D.Raycast(CastOrigin, transform.right * axisDirection, _filter, _hits, _castLength);
                }
                else
                {
                    resultCount = Physics2D.Raycast(CastOrigin, Vector2.right * axisDirection, _filter, _hits, _castLength);
                }
                break;
            case 'Y':
                if (RaycastAlongGameObjectNormal)
                {
                    resultCount = Physics2D.Raycast(CastOrigin, transform.up * axisDirection, _filter, _hits, _castLength);
                }
                else
                {
                    resultCount = Physics2D.Raycast(CastOrigin, Vector2.up * axisDirection, _filter, _hits, _castLength);
                }
                break;
        }
        return resultCount;
    }

    /// <summary>
    /// Fires the raycast along the provided axis with the current set ray origin and length.
    /// Returns all results hit along the raycast, instead of just the first.
    /// </summary>
    /// <param name="axis">The axis the raycast will occur on. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction of travel along the chosen axis. Positive or negative.</param>
    /// <param name="collisionMask">The layer mask to use to determine which layers the raycast should respond to. Uses the member layer mask if null.</param>
    /// <returns>The number of colliders the raycast hit.</returns>
    public override int FireCastAll(char axis, float axisDirection, LayerMask? collisionMask = null)
    {
        int resultCount = 0;
        LayerMask collisionMaskToUse = collisionMask ?? CollisionMask;

        switch (axis)
        {
            case 'X':
                if (RaycastAlongGameObjectNormal)
                {
                    resultCount = Physics2D.RaycastNonAlloc(CastOrigin, transform.right * axisDirection, _hits, _castLength, collisionMaskToUse);
                }
                else
                {
                    resultCount = Physics2D.RaycastNonAlloc(CastOrigin, Vector2.right * axisDirection, _hits, _castLength, collisionMaskToUse);
                }
                break;
            case 'Y':
                if (RaycastAlongGameObjectNormal)
                {
                    resultCount = Physics2D.RaycastNonAlloc(CastOrigin, transform.up * axisDirection, _hits, _castLength, collisionMaskToUse);
                }
                else
                {
                    resultCount = Physics2D.RaycastNonAlloc(CastOrigin, Vector2.up * axisDirection, _hits, _castLength, collisionMaskToUse);
                }
                break;
        }
        return resultCount;
    }

    /// <summary>
    /// Fires the raycast along the provided axis with the currently set ray origin and length. Directly returns the first collision result instead of the number of collisions.
    /// </summary>
    /// <param name="axis">The axis the raycast will occur on. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction of travel along the chosen axis. Positive or negative.</param>
    /// <param name="collisionMask">The layer mask to use to determine which layers the raycast should respond to. Uses the member layer mask if null.</param>
    /// <returns>The results of the raycast as a RaycastHit2D.</returns>
    public override RaycastHit2D FireCastAndReturnFirstResult(char axis, float axisDirection, LayerMask? collisionMask = null)
    {
        int resultCount = FireCast(axis, axisDirection, collisionMask);
        if (resultCount != 0)
        {
            return _hits[0];
        }

        return _emptyHit;
    }

    /// <summary>
    /// Fires the raycast along the provided Vector2 direction with the currently set ray origin and length.
    /// </summary>
    /// <param name="direction">The direction the ray should be fired in.</param>
    /// <returns>The number of colliders the raycast hit, either 0 or 1.</returns>
    public int FireAngledRaycast(Vector2 direction)
    {
        int resultCount = 0;
        _filter.layerMask = CollisionMask;

        resultCount = Physics2D.Raycast(CastOrigin, direction, _filter, _hits, _castLength);
        return resultCount;
    }

    /// <summary>
    /// Fires the raycast at the provided angle with the currently set ray origin and length.
    /// </summary>
    /// <param name="angle">The angle the ray should be fired at.</param>
    /// <param name="axis">The axis the raycast will occur on. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction of travel along the chosen axis. Positive or negative/</param>
    /// <returns>The number of colliders the raycast hit, either 0 or 1.</returns>
    public int FireAngledRaycast(char axis, float axisDirection, float angle)
    {
        int resultCount = 0;
        Vector2 angleVector = Vector2.zero;
        _filter.layerMask = CollisionMask;

        switch (axis)
        {
            case 'X':
                if (axisDirection == 1)
                {
                    angleVector = StaticTools.Vector2FromAngle(angle);
                }
                else if (axisDirection == -1)
                {
                    angleVector = StaticTools.Vector2FromAngle(180 + angle);
                }
                break;
            case 'Y':
                if (axisDirection == 1)
                {
                    angleVector = StaticTools.Vector2FromAngle(90 + angle);
                }
                else if (axisDirection == -1)
                {
                    angleVector = StaticTools.Vector2FromAngle(-90 + angle);
                }
                break;
        }

        resultCount = Physics2D.Raycast(CastOrigin, angleVector, _filter, _hits, _castLength);
        return resultCount;
    }
    #endregion

    #region Fire + Draw Raycast
    /// <summary>
    /// Fires the raycast along the provided axis with the currently set ray origin and length.
    /// Also draws the raycast in the scene view for debugging purposes.
    /// </summary>
    /// <param name="axis">The axis the raycast will occur on. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction of travel along the chosen axis. Positive or negative/</param>
    /// <param name="shouldCastAll">Whether the cast should be able to hit multiple colliders.</param>
    /// <param name="color">The color the raycast will be drawn in.</param>
    /// <param name="duration">An optional duration for the raycast to remain drawn in the scene view.</param>
    /// <param name="collisionMask">The layer mask to use to determine which layers the raycast should respond to. Uses the member layer mask if null.</param> 
    /// <returns>The number of objects the cast collided with. Depending on whether shouldCastAll is true, this can be a maximum of 1 or up to MaximumAllowedNumberOfCollisionsAllowed.</returns>
    public override int FireAndDrawCast(char axis, float axisDirection, bool shouldCastAll, Color color, float duration = 0, LayerMask? collisionMask = null)
    {
        switch (axis)
        {
            case 'X':
                if (RaycastAlongGameObjectNormal)
                {
                    Debug.DrawRay(CastOrigin, transform.right * axisDirection * _castLength, color, duration);
                }
                else
                {
                    Debug.DrawRay(CastOrigin, Vector2.right * axisDirection * _castLength, color, duration);
                }
                break;
            case 'Y':
                if (RaycastAlongGameObjectNormal)
                {
                    Debug.DrawRay(CastOrigin, transform.up * axisDirection * _castLength, color, duration);
                }
                else
                {
                    Debug.DrawRay(CastOrigin, Vector2.up * axisDirection * _castLength, color, duration);
                }
                break;
        }

        if (shouldCastAll)
        {
            return FireCastAll(axis, axisDirection, collisionMask);
        }
        return FireCast(axis, axisDirection, collisionMask);
    }

    /// <summary>
    /// Fires the raycast along the provided axis with the currently set ray origin and length.
    /// Also draws the raycast in the scene view for debugging purposes.
    /// </summary>
    /// <param name="axis">The axis the raycast will occur on. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction of travel along the chosen axis. Positive or negative/</param>
    /// <param name="color">The color the raycast will be drawn in.</param>
    /// <param name="duration">An optional duration for the raycast to remain drawn in the scene view.</param>
    /// <param name="collisionMask">The layer mask to use to determine which layers the raycast should respond to. Uses the member layer mask if null.</param> 
    /// <returns>The results of the raycast as a RaycastHit2D.</returns>
    public override RaycastHit2D FireAndDrawCastAndReturnFirstResult(char axis, float axisDirection, Color color, float duration = 0, LayerMask? collisionMask = null)
    {
        int resultCount = FireAndDrawCast(axis, axisDirection, false, color, duration, collisionMask);
        if (resultCount != 0)
        {
            return _hits[0];
        }

        return _emptyHit;
    }

    /// <summary>
    /// Fires the raycast along the provided axis with the currently set ray origin and length.
    /// Also draws the raycast in the scene view for debugging purposes.
    /// </summary>
    /// <param name="axis">The axis the raycast will occur on. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction of travel along the chosen axis. Positive or negative/</param>
    /// <param name="color">The color the raycast will be drawn in.</param>
    /// <param name="duration">An optional duration for the raycast to remain drawn in the scene view.</param>
    /// <param name="collisionMask">The layer mask to use to determine which layers the raycast should respond to. Uses the member layer mask if null.</param> 
    /// <returns>The results of the raycast as a RaycastHit2D.</returns>
    public  RaycastHit2D FireAndDrawCastAndReturnFirstResult(char axis, float axisDirection,GameObject ignor ,Color color, float duration = 0, LayerMask? collisionMask = null)
    {
        
        int resultCount = FireAndDrawCast(axis, axisDirection, false, color, duration, collisionMask);
        if (resultCount != 0)
        {
            if (resultCount == 1 && _hits[0].collider.gameObject != ignor)
            {
                return _hits[0];
            }
            else
            {
                foreach(RaycastHit2D hit in Hits)
                {
                    if(hit.collider != null && hit.collider.gameObject != ignor)
                    {
                        return hit;
                    }
                    else
                    {
                        Debug.Log("Ignored Ray");
                    }
                }
            }
        }

        return _emptyHit;
    }

    /// <summary>
    /// Fires the raycast at the provided angle with the currently set ray origin and length.
    /// Also draws the raycast in the scene view for debugging purposes.
    /// </summary>
    /// <param name="angle">The angle the ray should be fired at.</param>
    /// <param name="axis">The axis the raycast will occur on. Either X or Y in 2D.</param>
    /// <param name="axisDirection">The direction of travel along the chosen axis. Positive or negative</param>
    /// <param name="color">The color the raycast will be drawn in.</param>
    /// <param name="duration">An optional duration for the raycast to remain drawn in the scene view.</param>
    /// <returns>The number of colliders the raycast hit, either 0 or 1.</returns>
    public int FireAndDrawAngledRaycast(float angle, char axis, float axisDirection, Color color, float duration = 0)
    {
        Vector2 angleVector = Vector2.zero;
        switch (axis)
        {
            case 'X':
                if (axisDirection == 1)
                {
                    angleVector = StaticTools.Vector2FromAngle(angle);
                }
                else if (axisDirection == -1)
                {
                    angleVector = StaticTools.Vector2FromAngle(180 + angle);
                }
                break;
            case 'Y':
                if (axisDirection == 1)
                {
                    angleVector = StaticTools.Vector2FromAngle(90 + angle);
                }
                else if (axisDirection == -1)
                {
                    angleVector = StaticTools.Vector2FromAngle(-90 + angle);
                }
                break;
        }
        Debug.DrawRay(CastOrigin, angleVector * _castLength, color, duration);
        return FireAngledRaycast(angleVector);
    }
    #endregion

    #endregion

    #region Private Methods
    /// <summary>
    /// Calculates the spacing between rays on both the X and Y axes
    /// </summary>
    public void CalculateRaySpacing(bool startRaycastOutsideOfObject = false)
    {
        Bounds bounds = Collider.bounds;

        if (!startRaycastOutsideOfObject)
        {
            bounds.Expand(SKIN_WIDTH * -2f);
        }
        else
        {
            bounds.Expand(0.01f);
        }

        if (UseColliderBounds)
        {
            HorizontalRayCount = Mathf.RoundToInt(bounds.size.y / DESIRED_DISTANCE_BETWEEN_RAYS);
            VerticalRayCount = Mathf.RoundToInt(bounds.size.x / DESIRED_DISTANCE_BETWEEN_RAYS);

            if (HorizontalRayCount < MINIMUM_RAY_COUNT)
            {
                HorizontalRayCount = MINIMUM_RAY_COUNT;
            }
            if (VerticalRayCount < MINIMUM_RAY_COUNT)
            {
                VerticalRayCount = MINIMUM_RAY_COUNT;
            }

            HorizontalRaySpacing = bounds.size.y / (HorizontalRayCount - 1);
            VerticalRaySpacing = bounds.size.x / (VerticalRayCount - 1);
        }
        else
        {
            float height = Vector2.Distance(Origins.BottomRight, Origins.TopRight);
            float width = Vector2.Distance(Origins.TopLeft, Origins.TopRight);

            HorizontalRayCount = Mathf.RoundToInt(height / DESIRED_DISTANCE_BETWEEN_RAYS);
            VerticalRayCount = Mathf.RoundToInt(width / DESIRED_DISTANCE_BETWEEN_RAYS);

            HorizontalRaySpacing = height / (HorizontalRayCount - 1);
            VerticalRaySpacing = width / (VerticalRayCount - 1);
        }
    }

    /// <summary>
    /// Re-calculates the ray spacing. Used if the horizontal or vertical ray count changes.
    /// </summary>
    private void RecalculateRaySpacing()
    {
        Bounds bounds = Collider.bounds;
        bounds.Expand(SKIN_WIDTH * -2f);

        if (UseColliderBounds)
        {
            HorizontalRaySpacing = bounds.size.y / (HorizontalRayCount - 1);
            VerticalRaySpacing = bounds.size.x / (VerticalRayCount - 1);
        }
        else
        {
            float height = Vector2.Distance(Origins.BottomRight, Origins.TopRight);
            float width = Vector2.Distance(Origins.TopLeft, Origins.TopRight);

            HorizontalRaySpacing = height / (HorizontalRayCount - 1);
            VerticalRaySpacing = width / (VerticalRayCount - 1);
        }
    }
    #endregion

    #region Static Methods
    /// <summary>
    /// Fires a raycast downward and returns the results of the hit. A static method for objects that don't have a collision caster
    /// (or for a quick check where properly setting up the caster is unnecessary).
    /// </summary>
    /// <param name="raycastOriginPoint">The point the raycast should originate from.</param>
    /// <param name="distance">The distance the ray should travel.</param>
    /// <param name="layerMask">The layer mask to filter out results, if this is desired.</param>
    /// <returns></returns>
    public static RaycastHit2D RaycastBeneath(Vector2 raycastOriginPoint, float distance, LayerMask? layerMask = null)
    {
        Ray2D rayBeneath = new Ray2D(raycastOriginPoint, Vector2.down);
        if (layerMask.HasValue)
        {
            return Physics2D.Raycast(rayBeneath.origin, rayBeneath.direction, distance, layerMask.Value);
        }
        else
        {
            return Physics2D.Raycast(rayBeneath.origin, rayBeneath.direction, distance);
        }
    }

    /// <summary>
    /// Checks how far the ground is from a given location. A static method for objects that don't have a collision caster
    /// (or for a quick check where properly setting up the caster is unnecessary).
    /// </summary>
    /// <param name="raycastOriginPoint">The point the ray test should originate from (i.e. the point distance you want to test).</param>
    /// <returns>The distance from the ground the point sits.</returns>
    public static float PointDistanceFromGround(Vector2 raycastOriginPoint)
    {
        RaycastHit2D rayTest = RaycastBeneath(raycastOriginPoint, 50f, OBSTACLE_LAYER_SHIFTED);
        return rayTest.distance == 0 ? Mathf.Infinity : rayTest.distance;
    }
    #endregion
}
