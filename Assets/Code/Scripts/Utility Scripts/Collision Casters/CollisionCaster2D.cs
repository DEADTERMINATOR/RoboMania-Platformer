﻿using UnityEngine;

public abstract class CollisionCaster2D : MonoBehaviour
{
    /// <summary>
    /// A struct containing the various origin points that may be used when raycasting.
    /// </summary>
    public struct RaycastOrigins
    {
        /// <summary>
        /// The vector representing the center of the collider.
        /// </summary>
        public Vector2 Center;

        /// <summary>
        /// The vector representing the bottom center of the collider.
        /// </summary>
        public Vector2 BottomCenter;

        /// <summary>
        /// The vector representing the top left of collider.
        /// </summary>
        public Vector2 TopLeft;

        /// <summary>
        /// The vector representing the top right of collider.
        /// </summary>
        public Vector2 TopRight;

        /// <summary>
        /// The vector representing the bottom left of collider.
        /// </summary>
        public Vector2 BottomLeft;

        /// <summary>
        /// The vector representing the bottom right of collider.
        /// </summary>
        public Vector2 BottomRight;


        public override string ToString()
        {
            return "Center: (" + Center.x + " " + Center.y
                + ")\nTop Left: (" + TopLeft.x + " " + TopLeft.y 
                + ")\nTop Right : (" + TopRight.x + " " + TopRight.y 
                + ")\nBottom Left: (" + BottomLeft.x + " " + BottomLeft.y 
                + ")\nBottom Right: (" + BottomRight.x + " " + BottomRight.y + ")";
        }
    }


    /// <summary>
    /// The distance into the collider that raycasts will begin, so raycasts fired when the object
    /// is right against something still have some distance to travel and register the collision.
    /// </summary>
    public const float SKIN_WIDTH = 0.015f;


    /// <summary>
    /// Reference to the collider that the casts will be fired from.
    /// </summary>
    public Collider2D Collider;

    /// <summary>
    /// A layer mask that holds the layers we want to consider when raycasting for collisions.
    /// </summary>
    public LayerMask CollisionMask;

    /// <summary>
    /// The maximum number of collisions that can be captured in a single cast. Values of more than one will force the use of an "all" variant of the raycast functions.
    /// </summary>
    public int MaximumAllowedNumberOfCollisionsPerCast = 1;

    /// <summary>
    /// Whether the bounds of the collider should be used to calculate the origin points. Only works for objects whose colliders are axis aligned and will never rotate (and is more efficient in these cases).
    /// </summary>
    [Tooltip("Uncheck this only if the collider on the object is not axis aligned or it will rotate during gameplay or the bounds Unity is producing don't make any sense.")]
    public bool UseColliderBounds = true;

    /// <summary>
    /// Instance of the raycast origins struct to store the origins of the collider.
    /// </summary>
    public RaycastOrigins Origins;

    /// <summary>
    /// Public getter for the array of raycast hits from the last cast.
    /// </summary>
    public RaycastHit2D[] Hits { get { return _hits; } }

    /// <summary>
    /// The chosen origin for the next raycast.
    /// </summary>
    public Vector2 CastOrigin { get; protected set; }


    /// <summary>
    /// A re-usable array for raycast hits.
    /// </summary>
    protected RaycastHit2D[] _hits;

    /// <summary>
    /// An empty raycast hit, to return when no collision results were found but a returned result is required.
    /// </summary>
    protected RaycastHit2D _emptyHit;

    /// <summary>
    /// A filter to pass into the cast calls to specify the types of results that are wanted from the cast.
    /// </summary>
    protected ContactFilter2D _filter;

    /// <summary>
    /// The desired length for the next raycast.
    /// </summary>
    protected float _castLength;


    protected virtual void Awake()
    {
        if (Collider == null)
        {
            Collider = GetComponent<BoxCollider2D>();
            if (Collider == null)
            {
                Debug.LogError("CollisionCaster2D component on game object " + gameObject.name + " doesn't have a collider assigned, and one could not be found on the object");
            }
        }
        UpdateCastOrigins();

        if (MaximumAllowedNumberOfCollisionsPerCast < 1)
        {
            MaximumAllowedNumberOfCollisionsPerCast = 1;
        }

        //It's possible that another Start method has already called for a raycast. In which case this array would already have been instantiated.
        _hits = new RaycastHit2D[MaximumAllowedNumberOfCollisionsPerCast];

        _emptyHit = new RaycastHit2D();
        _filter = new ContactFilter2D();
        _filter.useLayerMask = true;
    }

    /// <summary>
    /// Sets the next ray length using the velocity of the provided axis
    /// </summary>
    /// <param name="axisVelocity">The velocity on one of the axis.</param>
    public void CalculateCastLength(float axisVelocity)
    {
        _castLength = Mathf.Abs(axisVelocity) + SKIN_WIDTH;
    }

    /// <summary>
    /// Directly sets the next ray length.
    /// </summary>
    /// <param name="castLength">The desired ray length.</param>
    public void SetCastLength(float castLength)
    {
        _castLength = castLength;
    }

    /// <summary>
    /// Sets the ray origin to the specified vector.
    /// </summary>
    /// <param name="origin">The vector representing the origin point for the raycast.</param>
    public virtual void SetCastOrigin(Vector2 origin)
    {
        CastOrigin = origin;
    }

    /// <summary>
    /// Adds the provided layer for the raycaster to consider in collisions.
    /// </summary>
    /// <param name="layerToAdd">The layer to add to the collision mask.</param>
    public void AddLayerToCollisionMask(int layerToAdd)
    {
        Debug.Assert(layerToAdd >= 0 && layerToAdd <= 31);
        CollisionMask |= 1 << layerToAdd;
    }

    /// <summary>
    /// Removes the provided layer from the raycaster's consideration during collisions.
    /// </summary>
    /// <param name="layerToAdd">The layer to remove from the collision mask.</param>
    public void RemoveLayerFromCollisionMask(int layerToAdd)
    {
        Debug.Assert(layerToAdd >= 0 && layerToAdd <= 31);
        CollisionMask &= ~(1 << layerToAdd);
    }

    /// <summary>
    /// Updates the positions of the cast origins to reflect the current position of the casting object.
    /// </summary>
    public virtual void UpdateCastOrigins(bool startRaycastOutsideOfObject = false, bool showDebug = false)
    {
        if (Collider != null)
        {
            Origins.Center = Collider.bounds.center;

            if (UseColliderBounds)
            {
                Bounds bounds = Collider.bounds;
                if (!startRaycastOutsideOfObject)
                {
                    bounds.Expand(SKIN_WIDTH * -2f);
                }
                else
                {
                    bounds.Expand(0.01f);
                }

                Origins.BottomCenter = new Vector2(bounds.center.x, bounds.min.y);
                Origins.BottomLeft = new Vector2(bounds.min.x, bounds.min.y);
                Origins.BottomRight = new Vector2(bounds.max.x, bounds.min.y);
                Origins.TopLeft = new Vector2(bounds.min.x, bounds.max.y);
                Origins.TopRight = new Vector2(bounds.max.x, bounds.max.y);

                if (showDebug)
                {
                    Debug.Log("Object : " + gameObject.name + "\nBounds: " + Collider.bounds + "\nOrigins:  " + Origins);
                    VisualDebug.DrawBox(bounds.center, bounds.extents, Quaternion.identity, Color.gray);
                }
            }
            else
            {
                float halfWidth = 0;
                float halfHeight = 0;

                if (Collider is BoxCollider2D)
                {
                    BoxCollider2D boxCollider = Collider as BoxCollider2D;
                    halfWidth = boxCollider.size.x / 2f * transform.lossyScale.x;
                    halfHeight = boxCollider.size.y / 2f * transform.lossyScale.y;
                }
                else if (Collider is CapsuleCollider2D)
                {
                    CapsuleCollider2D capsuleCollider = Collider as CapsuleCollider2D;
                    halfWidth = capsuleCollider.size.x / 2f * transform.lossyScale.x;
                    halfHeight = capsuleCollider.size.y / 2f * transform.lossyScale.y;
                }

                Vector3 scaledColliderOffset = Collider.offset * transform.lossyScale;
                float currentRotationAsRad = transform.eulerAngles.z * Mathf.Deg2Rad;

                Origins.BottomCenter = new Vector2(transform.position.x + scaledColliderOffset.x * Mathf.Cos(currentRotationAsRad), transform.position.y + (-halfWidth + scaledColliderOffset.x) * Mathf.Sin(currentRotationAsRad) + (-halfHeight + scaledColliderOffset.y) * Mathf.Cos(currentRotationAsRad));
                Origins.BottomLeft = new Vector2(transform.position.x + (-halfWidth + scaledColliderOffset.x) * Mathf.Cos(currentRotationAsRad) - (-halfHeight + scaledColliderOffset.y) * Mathf.Sin(currentRotationAsRad), transform.position.y + (-halfWidth + scaledColliderOffset.x) * Mathf.Sin(currentRotationAsRad) + (-halfHeight + scaledColliderOffset.y) * Mathf.Cos(currentRotationAsRad));
                Origins.BottomRight = new Vector2(transform.position.x + (halfWidth + scaledColliderOffset.x) * Mathf.Cos(currentRotationAsRad) - (-halfHeight + scaledColliderOffset.y) * Mathf.Sin(currentRotationAsRad), transform.position.y + (halfWidth + scaledColliderOffset.x) * Mathf.Sin(currentRotationAsRad) + (-halfHeight + scaledColliderOffset.y) * Mathf.Cos(currentRotationAsRad));
                Origins.TopLeft = new Vector2(transform.position.x + (-halfWidth + scaledColliderOffset.x) * Mathf.Cos(currentRotationAsRad) - (halfHeight + scaledColliderOffset.y) * Mathf.Sin(currentRotationAsRad), transform.position.y + (-halfWidth + scaledColliderOffset.x) * Mathf.Sin(currentRotationAsRad) + (halfHeight + scaledColliderOffset.y) * Mathf.Cos(currentRotationAsRad));
                Origins.TopRight = new Vector2(transform.position.x + (halfWidth + scaledColliderOffset.x) * Mathf.Cos(currentRotationAsRad) - (halfHeight + scaledColliderOffset.y) * Mathf.Sin(currentRotationAsRad), transform.position.y + (halfWidth + scaledColliderOffset.x) * Mathf.Sin(currentRotationAsRad) + (halfHeight + scaledColliderOffset.y) * Mathf.Cos(currentRotationAsRad));
            }
        }
    }

    public abstract int FireCast(char axis, float axisDirection, LayerMask? collisionMask = null);
    public abstract int FireCastAll(char axis, float axisDirection, LayerMask? collisionMask = null);
    public abstract int FireAndDrawCast(char axis, float axisDirection, bool shouldCastAll, Color color, float duration = 0.25f, LayerMask? collisionMask = null);
    public abstract RaycastHit2D FireCastAndReturnFirstResult(char axis, float axisDirection, LayerMask? collisionMask = null);
    public abstract RaycastHit2D FireAndDrawCastAndReturnFirstResult(char axis, float axisDirection, Color color, float duration = 0.25f, LayerMask? collisionMask = null);
    //public abstract void SetCastOrigin(char axis, float axisDirection);

    protected void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(Origins.BottomLeft, 0.01f);
        Gizmos.DrawSphere(Origins.BottomRight, 0.01f);
        Gizmos.DrawSphere(Origins.TopLeft, 0.01f);
        Gizmos.DrawSphere(Origins.TopRight, 0.01f);
    }
}
