using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[DisallowMultipleComponent]
public class MatchShadowCasterPointsForAllTerrain : MonoBehaviour
{
    [SerializeField]
    private bool _addMissingShadowCasters = true;

    public void ProcessAllTerrain()
    {
        var watch = new System.Diagnostics.Stopwatch();
        Debug.Log("Starting Terrain Processing");
        watch.Start();

        var allTerrain = FindObjectsOfType<Ferr2DT_PathTerrain>();
        foreach (Ferr2DT_PathTerrain terrain in allTerrain)
        {
            var shadowCaster = terrain.GetComponent<ShadowCaster2D>();
            if (shadowCaster == null && _addMissingShadowCasters)
            {
                shadowCaster = terrain.gameObject.AddComponent<ShadowCaster2D>();
            }

            if (shadowCaster != null)
            {
                shadowCaster.GenerateShapePathFromPoints(terrain.PathData.GetPoints(0));
            }
        }

        watch.Stop();
        Debug.Log("Terrain Finished Processing in " + watch.ElapsedMilliseconds + "ms");
    }
}
