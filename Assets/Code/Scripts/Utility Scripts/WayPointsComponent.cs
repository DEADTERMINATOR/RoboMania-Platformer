﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Hold a set of waypoints in a Component
/// </summary>
public class WayPointsComponent : EditableWayPoints
{
    public WayPoints WayPoints;

    private Vector3 offset;


    private void OnDrawGizmos()
    {
        if(WayPoints== null || WayPoints.Count ==0)
        {
            Debug.LogWarning("Empty Way point system");
            return;
        }
            offset = Vector3.zero;
        ///The last point of the laster inner loop
        Vector3 lastEND = Vector3.zero;
        /// The First point
        Vector3 first = Vector3.zero;
        int i = 0;
        Vector3 last = Vector3.zero;
        foreach (Vector3 point in WayPoints)
        {
            GizmoHelper.drawString(i.ToString(), point + offset, 0, 1, Color.white);
            Gizmos.color = Color.black;
            Gizmos.DrawSphere(point + offset, 0.1f);


            if (last != Vector3.zero)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(last + offset, point + offset);
            }
            last = point;
            i++;
        }
        Gizmos.color = Color.blue;
        if (WayPoints.IsLooping || WayPoints.Count == 2)
        {
            Gizmos.DrawLine(WayPoints.First + offset, WayPoints.Last + offset);
        }
    }
}



