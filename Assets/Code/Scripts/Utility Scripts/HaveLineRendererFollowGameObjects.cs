﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HaveLineRendererFollowGameObjects : MonoBehaviour
{
    public bool UpdatePositions = true;
    public LineRenderer LineRenderer;
    public List<GameObject> GameObjectsToFollow = new List<GameObject>();

    // Start is called before the first frame update
    private void Start()
    {
        if (LineRenderer == null)
        {
            LineRenderer = GetComponent<LineRenderer>();
            if (LineRenderer == null)
            {
                Debug.LogWarning("HaveLineRendererFollowGameObjects component on " + gameObject.name + " does not have a LineRenderer assigned, and one could not be found on the object.");
            }
        }

        if (GameObjectsToFollow.Count == 0)
        {
            Debug.LogWarning("No game objects were assigned in the HaveLineRendererFollowGameObjects component on " + gameObject.name);
        }

        /* We need to loop through through the game objects to follow list because it's possible some were removed in the specific instance of the object with this script attached
         * (e.g. a horizontal vine attached to a hitch doesn't need points travelling along the entire length of the vine). So instead of manually handling the points for each
         * game object in the scene, this will just clean up any unnecessary ones. */
        for (int i = GameObjectsToFollow.Count - 1; i >= 0; --i)
        {
            if (GameObjectsToFollow[i] == null)
            {
                GameObjectsToFollow.RemoveAt(i);
            }
        }

        LineRenderer.positionCount = GameObjectsToFollow.Count;
        SetPositions();
    }

    private void Update()
    {
        if (UpdatePositions)
        {
            SetPositions();
        }
    }

    private void SetPositions()
    {
        for (int i = 0; i < GameObjectsToFollow.Count; ++i)
        {
            LineRenderer.SetPosition(i, GameObjectsToFollow[i].transform.position);
        }
    }
}
