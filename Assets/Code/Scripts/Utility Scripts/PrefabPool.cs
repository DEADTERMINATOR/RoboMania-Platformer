﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PrefabPool : MonoBehaviour
{
    private class SpawnedItem
    {
        public GameObject Item;
        public bool MarkedForDeletion;

        public SpawnedItem(GameObject spawnedItem, bool markedForDeletion = false) => (Item, MarkedForDeletion) = (spawnedItem, markedForDeletion);
    }

    public GameObject PoolPrefab;
    public Guid PoolID = Guid.NewGuid();
    public string PoolDebugName = "Name not set";
    private List<SpawnedItem> _alive;
    private List<SpawnedItem> _dead;
    public int NumberAlive { get { return _alive.Count; } }



    public void NotifyOfObjectDeath(GameObject it)
    {
        int index = _alive.FindIndex(0, x => x.Item == it);
        it.transform.position = GlobalData.VEC3_OFFSCREEN;

        if (index >= 0)
        {
            //get and hold the ref to the OBJ being returned 
            SpawnedItem item = _alive[index];

            //remove the Ref from the alive list
            _alive.RemoveAt(index);

            //Add back to the dead list
            _dead.Add(item);
        }
     }

    // Use this for initialization
    void Start()
    {
        _dead = new List<SpawnedItem>();
        _alive = new List<SpawnedItem>();
    }

    public void CleanThePool()
    {
        foreach (SpawnedItem obj in _dead)
        {
            Destroy(obj.Item);
        }
        _dead.Clear();

        foreach (SpawnedItem obj in _alive)
        {
            Destroy(obj.Item);
        }
        _alive.Clear();
    }

    public void PreparePoolForPrefabSwitch()
    {
        foreach (SpawnedItem obj in _dead)
        {
            Destroy(obj.Item);
        }
        _dead.Clear();

        for (int i = 0; i < _alive.Count; ++i)
        {
            _alive[i].MarkedForDeletion = true;
        }
    }

    /// <summary>
    ///    
    /// </summary>
    /// <returns></returns>
    public GameObject Spawn()
    {
        bool wasNew = false;
        SpawnedItem spawnedItem;

        if (_dead.Count > 0)
        {
            spawnedItem = _dead[_dead.Count - 1];
            _dead.RemoveAt(_dead.Count - 1);

            //Fail safe
            if (!spawnedItem.Item || spawnedItem.MarkedForDeletion)
            {
                spawnedItem = new SpawnedItem(Instantiate(PoolPrefab));
                wasNew = true;
            }
        }
        else
        {
            spawnedItem = new SpawnedItem(Instantiate(PoolPrefab));
            wasNew = true;
        }

        if (spawnedItem.Item)
        {
            PooledItem pooled = spawnedItem.Item.GetComponent<PooledItem>();
            if (pooled)
            {
                pooled.ManagingPool = this;
            }

            _alive.Add(spawnedItem);
            spawnedItem.Item.SetActive(true); 
            return spawnedItem.Item;
        }
        else
        {
            print("Error Getting OBJ for pool");
            if (wasNew)
            {
                print("Was newly spawned");
            }

            return null;
        }
    }

    /// <summary>
    ///    
    /// </summary>
    /// <returns></returns>
    public GameObject Spawn(Vector3 spawnPos, Quaternion spawnRotation)
    {
        bool wasNew = false;
        SpawnedItem spawnedItem;

        if (_dead.Count > 0)
        {
            spawnedItem = _dead[_dead.Count - 1];
            _dead.RemoveAt(_dead.Count - 1);

            //Fail safe
            if (!spawnedItem.Item || spawnedItem.MarkedForDeletion)
            {
                spawnedItem = new SpawnedItem(Instantiate(PoolPrefab, spawnPos, spawnRotation));
                wasNew = true;
            }
        }
        else
        {
            spawnedItem = new SpawnedItem(Instantiate(PoolPrefab, spawnPos, spawnRotation));
            wasNew = true;
        }

        if (spawnedItem.Item)
        {
            PooledItem pooled = spawnedItem.Item.GetComponent<PooledItem>();
            if (pooled)
            {
                pooled.ManagingPool = this;
            }

            _alive.Add(spawnedItem);
            spawnedItem.Item.SetActive(true);

            if (wasNew)
            {
                Debug.Log(String.Format("{2} Pool with ID {0} was crated a new Item With ID {1}", PoolID, pooled.Itemguid, PoolDebugName));
            }
            else
            {
                Debug.Log(String.Format("{2} Pool with ID {0} was spawend an Item With ID {1}", PoolID, pooled.Itemguid, PoolDebugName));
            }
            return spawnedItem.Item;
        }
        else
        {
            print("Error Geting OBJ for pool");
            if (wasNew)
            {
                print("Was newly spawned");
            }

            return null;
        }
    }
}
