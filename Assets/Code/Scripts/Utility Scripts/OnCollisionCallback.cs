﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollisionCallback : MonoBehaviour
{
    /// <summary>
    /// The GameObject to receive the callback.
    /// </summary>
    public GameObject Receiver;

    /// <summary>
    /// The layers to on which collisions should be responded.
    /// </summary>
    public List<int> LayersToRespondToCollisions;

    /// <summary>
    /// The name of the method to call when a collision enter event is triggered.
    /// </summary>
    public string CollisionEnterCallbackMethodName;

    /// <summary>
    /// The name of the method to call when a collision exit event is triggered.
    /// </summary>
    public string CollisionExitCallbackMethodName;

    /// <summary>
    /// Whether an error should be raised if the method does not exist in the target MonoBehavior.
    /// </summary>
    public bool RaiseErrorOnCallFailure = true;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (CollisionEnterCallbackMethodName != "" && CollisionOccuredOnValidLayer(collision.collider))
            Receiver.SendMessage(CollisionEnterCallbackMethodName, collision, RaiseErrorOnCallFailure == true ? SendMessageOptions.RequireReceiver : SendMessageOptions.DontRequireReceiver);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (CollisionEnterCallbackMethodName != "" && CollisionOccuredOnValidLayer(collider))
            Receiver.SendMessage(CollisionEnterCallbackMethodName, collider, RaiseErrorOnCallFailure == true ? SendMessageOptions.RequireReceiver : SendMessageOptions.DontRequireReceiver);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (CollisionExitCallbackMethodName != "" && CollisionOccuredOnValidLayer(collision.collider))
            Receiver.SendMessage(CollisionExitCallbackMethodName, collision, RaiseErrorOnCallFailure == true ? SendMessageOptions.RequireReceiver : SendMessageOptions.DontRequireReceiver);
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (CollisionExitCallbackMethodName != "" && CollisionOccuredOnValidLayer(collider))
            Receiver.SendMessage(CollisionExitCallbackMethodName, collider, RaiseErrorOnCallFailure == true ? SendMessageOptions.RequireReceiver : SendMessageOptions.DontRequireReceiver);
    }

    private bool CollisionOccuredOnValidLayer(Collider2D collider)
    {
        foreach (int layer in LayersToRespondToCollisions)
        {
            if (collider.gameObject.layer == layer)
                return true;
        }
        return false;
    }
}
