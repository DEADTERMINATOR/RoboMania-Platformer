﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PooledItem : MonoBehaviour
{
    public PrefabPool ManagingPool { get; set; }
    public Guid Itemguid = Guid.NewGuid();

    public void NotifyOfObjectDeath()
    {
        if (ManagingPool)
        {
            ManagingPool.NotifyOfObjectDeath(this.gameObject);
        }
    }

}
