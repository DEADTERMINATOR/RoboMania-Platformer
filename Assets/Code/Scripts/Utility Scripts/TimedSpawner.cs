﻿using UnityEngine;
using System.Collections;

public class TimedSpawner : MonoBehaviour, IActivatable
{
    /// <summary>
    /// The prefab to spawn;
    /// </summary>
    public GameObject objPrefab;
    /// <summary>
    /// Time between each spawn
    /// </summary>
    public float timeOut;
    /// <summary>
    /// The point to spawn the obj Relative To The spawner
    /// </summary>
    public Vector3 spawnPoint = Vector3.zero;
    /// <summary>
    /// Spawn on start
    /// </summary>
    public bool StartSpawing;
    private float _time;
    public bool _run;

    public bool Active { get { return _run; } }


    // Use this for initialization
    void Start()
    {
        //move to world space
        spawnPoint += transform.position;
        if(StartSpawing)
        {
            _time = timeOut;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_run)
        {
            _time += Time.deltaTime;
            if (_time >= timeOut)
            {
                _time = 0;
                Instantiate(objPrefab, spawnPoint, new Quaternion());
            }
        }
    }

    public void Activate(GameObject activator)
    {
        _run = true; 
    }

    public void Deactivate(GameObject activator)
    {
        _run = false;
    }
}
