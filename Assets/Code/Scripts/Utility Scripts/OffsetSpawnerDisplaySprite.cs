﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used to apply an offset to the position of the display sprite (in case the sprite doesn't properly line up with the spawner position or the position of the NPC's movement collider) that is used with NPC spawners to indicate what type of NPC they are spawning.
/// </summary>
public class OffsetSpawnerDisplaySprite : MonoBehaviour
{
    public delegate void TransformEvent();
    public event TransformEvent Moved;


    public Vector2 Offset { get { return _offset; } }
    public bool OffsetCollisionBoxDraw { get { return _offsetCollisionBoxDraw; } }

    [SerializeField]
    private Vector2 _offset;
    [SerializeField]
    private bool _offsetCollisionBoxDraw;
    private Vector2 _lastOffset;


    private void OnDrawGizmos()
    {
        if (_offset != _lastOffset)
        {
            Moved?.Invoke();
        }
        _lastOffset = _offset;
    }
}
