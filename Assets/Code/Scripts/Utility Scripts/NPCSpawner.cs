﻿using Characters;
using Characters.Enemies;
using LevelSystem;
using LevelSystem.SateData;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class NPCSpawner : MonoBehaviour, IActivatable
{
    private const string DISPLAY_SPRITE_GONAME = "Display Sprite";

    /// <summary>
    /// THe NPC Prefab to be spawned
    /// </summary>
    public GameObject PrefabToSpawn;
    /// <summary>
    /// 
    /// </summary>
    public bool RespawnOnDeath = true;


    public bool NPCActive { get { return _spawnedNPC != null && _spawnedNPC.activeInHierarchy; } }
    /// <summary>
    /// This NPC Will only spawn once per level after killed 
    /// </summary>
    public bool IsAUniqueSpawn = false;
    private LevelStateBool _didUniqueSpawn;


    [HideInInspector]
    public bool WasKilled = false;
    /// <summary>
    /// The spawnerData  
    /// </summary>
    public NPCSpawnerData SpawnerData;

    public bool SpawnerDefaultsApplied = false;

    [HideInInspector]
    public String SPAWNERID = "";

    public Difficulty Difficulty = Difficulty.Values.EASY;

    public bool Active { get { return _active; } }


    /// <summary>
    /// The sprite to be displayed at the spawner's position to more easily identify what NPC type this spawner spawns.
    /// </summary>
    [SerializeField]
    private Sprite _displaySprite = null;

    /// <summary>
    /// Whether the scale of the display sprite needs to be overriden.
    /// </summary>
    [SerializeField]
    private bool _overrideDisplaySpriteScale;

    /// <summary>
    /// The overriden display sprite scale.
    /// </summary>
    [SerializeField]
    private Vector3 _displaySpriteScale;

    /// <summary>
    /// Whether we need to force a bounds regeneration for displaying the collider bounds and sprite of the NPC being spawner (functionality only relevant in the editor).
    /// </summary>
    [SerializeField]
    private bool _forceBoundsRegeneration;

    /// <summary>
    /// The NPC reference that is crated on a scene load 
    /// </summary>
    private GameObject _spawnedNPC;

    /// <summary>
    /// The NPC reference that is used to spawn the NPC and is crated or loaded 
    /// </summary>
    private GameObject _NPCLoadedPrefab;

    private Guid _lockID;

    private bool _active = false;

    private List<Renderer> _prefabMeshes = new List<Renderer>();

    private BoxCollider2D _prefabCollider;
    private Bounds _prefabBounds;
    private bool _boundsFound = false;

    private SpriteRenderer _displaySpriteRenderer = null;
    private OffsetSpawnerDisplaySprite _displaySpriteOffset = null;
    private Sprite _previousSprite = null;

    private Vector3 _offScreen = new Vector3(-9999, 9999, -56);


    private void Awake()
    {
        if (_displaySprite != null)
        {
            var displaySpriteTransform = transform.Find(DISPLAY_SPRITE_GONAME);
            if (displaySpriteTransform != null)
            {
                displaySpriteTransform.gameObject.SetActive(false);
            }
        }

        if (Difficulty > Level.CurentLevel.CurentDifficulty)
        {
            Destroy(gameObject);
        }

        if (SpawnerData == null)
        {
            SpawnerData = GetComponent<NPCSpawnerData>();
        }

        if (SpawnerData.WaypointsMode == NPCSpawnerData.WaypointsSelectionMode.AttachedSystem)
        {
            SpawnerData.WaypointSystem = GetComponent<EditableWayPoints>();
        }

        if (SpawnerData.BoundsMode == NPCSpawnerData.BoundsSelectionMode.AttachedCollider)
        {
            BoxCollider2D activeArea = GetComponent<BoxCollider2D>();
            SpawnerData.ActiveBounds = new Rect(activeArea.bounds.min.x, activeArea.bounds.min.y, activeArea.bounds.size.x, activeArea.bounds.size.y);
            Destroy(activeArea);
        }

        if (string.IsNullOrEmpty(SPAWNERID))
        {
            Debug.Log("Making New NPCID" + name);
            SPAWNERID = Guid.NewGuid().ToString();
        }

        if (IsAUniqueSpawn)
        {
            _didUniqueSpawn = Level.CurentLevel.LevelState.LoadLevelStateBool(SPAWNERID + "_UniqueSpawn", false);
        }

        SpawnNPCOnLoad();
    }

    /// <summary>
    /// Set Up Sapaner data
    /// </summary>
    /// <param name="FirstTime">Is the First Run </param>
    private void SetUpSpawnerData(bool FirstTime)
    {
        NPC npcRef = _spawnedNPC.GetComponentInChildren<NPC>();

        npcRef.ChangeGravityType(SpawnerData.InitialGravityState);

        npcRef.GravityScale = SpawnerData.GravityScale;
        npcRef.StartFaceDir = SpawnerData.StartFaceDir;

        if (SpawnerData.WaypointSystem != null)
        {
            npcRef.Waypoints = SpawnerData.WaypointSystem;
        }

        if (SpawnerData.ActiveBounds != null)
        {
            npcRef.ActiveArea = SpawnerData.ActiveBounds;
        }
        npcRef.PostSpawnSetup();

        Enemy npcAsEnemy = npcRef as Enemy;
        if (npcAsEnemy)
        {
            npcAsEnemy.MaxHealth = ((EnemySpawnerData)SpawnerData).MaxHealth;
            npcAsEnemy.GunChipSpawnChance = ((EnemySpawnerData)SpawnerData).GunChipSpawnChance;
            npcAsEnemy.ShieldSpawnChance = ((EnemySpawnerData)SpawnerData).ShieldSpawnChance;
        }
        if (npcRef is Dragonfly)
        {
            ((Dragonfly)npcAsEnemy).PathTileMap = SpawnerData.AreaTiler.Map;
        }
        else if (npcRef is Bat)
        {
            ((Bat)npcAsEnemy).PathTileMap = SpawnerData.AreaTiler.Map;
        }
        else if (npcRef is Sniper)
        {
            ((Sniper)npcAsEnemy).RobotSniperActiveZones = ((SniperSpawnerData)SpawnerData).ActiveZones;
        }
        else if (npcRef is Cockroach)
        {
            ((Cockroach)npcAsEnemy).UpSideDown = ((CockroachSpawnerData)SpawnerData).StartUpsideDown;
            ((Cockroach)npcAsEnemy).CanBeOnRoof = ((CockroachSpawnerData)SpawnerData).CanBeOnRoof;
        }
        else if (npcRef is Wasp)
        {
            ((Wasp)npcAsEnemy).FaceStartingDirectionWheneverPossible = ((WaspSpawnerData)SpawnerData).FaceStartingDirectionWheneverPossible;
            ((Wasp)npcAsEnemy).PathTilemap = SpawnerData.AreaTiler;
        }
        else if (npcRef is Spiny)
        {
            ((Spiny)npcAsEnemy).AllowedMovementArea = ((SpinySpawnerData)SpawnerData).AllowedMovementArea;
            ((Spiny)npcAsEnemy).StartingPlatformSide = ((SpinySpawnerData)SpawnerData).StartingPlatformSide;
        }

        if (!FirstTime && npcRef)
        {
            npcRef.Dead += WhenDead;
        }
    }

    /// <summary>
    /// Spawn on load;
    /// </summary>
    private void SpawnNPCOnLoad()
    {
        _NPCLoadedPrefab = Instantiate(PrefabToSpawn, _offScreen, Quaternion.identity);
        _NPCLoadedPrefab.gameObject.SetActive(false);
        SceneManager.MoveGameObjectToScene(_NPCLoadedPrefab, SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage.MasterScene));
    }

    /// <summary>
    /// Activate The NPC and Move it in to the World
    /// </summary>
    /// <param name="activator"></param>
    public void Activate(GameObject activator)
    {
        TryAndSpawnNPC();
        _active = true;
    }

    /// <summary>
    /// Try and Spawn an nPC with the Spawner
    /// </summary>
    /// <returns></returns>
    public GameObject TryAndSpawnNPC()
    {
        if(IsAUniqueSpawn && _didUniqueSpawn)
        {
            return null; 
        }

        SceneManager.SetActiveScene(SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage[0].Path));
        if (_NPCLoadedPrefab == null && _spawnedNPC == null)
        {
            if (RespawnOnDeath)
            {
                Debug.LogError("Tried to spawn a spawner that was not activated");
            }

            return null;
        }
        else if (_NPCLoadedPrefab != null && !_spawnedNPC)
        {
            _spawnedNPC = Instantiate(_NPCLoadedPrefab, _offScreen, Quaternion.identity);
        }

        _spawnedNPC.transform.position = transform.position;
        //move the ref to the world from the master
        SceneManager.MoveGameObjectToScene(_spawnedNPC, SceneManager.GetSceneByPath(GameMaster.GameManager.Instance.LoadedDatatpackage[0].Path));
        Enemy npcAsEnemy = _spawnedNPC.GetComponentInChildren<Enemy>();

        if (npcAsEnemy != null)
        {
            npcAsEnemy.Activated = true;
        }
        SetUpSpawnerData(false);

        _spawnedNPC.SetActive(true);
        _spawnedNPC.gameObject.SetActive(true);

        return _spawnedNPC;
    }

    private void WhenDead()
    {
        WasKilled = true;
        if (IsAUniqueSpawn)
        {
            _didUniqueSpawn.Value = true;
        }

        if (RespawnOnDeath && PrefabToSpawn != null)
        {
            _spawnedNPC = Instantiate(_NPCLoadedPrefab, _offScreen, Quaternion.identity);
            
            SceneManager.MoveGameObjectToScene(_spawnedNPC, gameObject.scene);
            SetUpSpawnerData(false);

            WasKilled = false;
            _spawnedNPC.SetActive(false);

        }
    }

    public void Deactivate(GameObject activator)
    {
        if (_spawnedNPC)
        {
            Enemy npcAsEnemy = _spawnedNPC.GetComponentInChildren<Enemy>();
            if (npcAsEnemy != null)
            {
                npcAsEnemy.Activated = false;
            }

            _spawnedNPC.SetActive(false);
            _spawnedNPC.transform.position = _offScreen;
        }

        _active = false;
    }

    private void OnDestroy()
    {
        if (_spawnedNPC)
        {
            Destroy(_spawnedNPC);
        }
    }

    private void OnDrawGizmos()
    {
        //Draw marker and line back to the activator.
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.1f);

        Gizmos.color = Color.black;
        float size = 0.3f;

        Gizmos.DrawLine(transform.position - Vector3.up * size, transform.position + Vector3.up * size);
        Gizmos.DrawLine(transform.position - Vector3.left * size, transform.position + Vector3.left * size);

        if (SpawnerData != null && SpawnerData.ActiveBounds != null)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireCube(SpawnerData.ActiveBounds.center, SpawnerData.ActiveBounds.size);
        }

        Transform displaySpriteTransform = null;
        //Setup the sprite renderer to display the display sprite for this spawner (if there is one).
        if (_displaySprite != null)
        {
            displaySpriteTransform = transform.Find(DISPLAY_SPRITE_GONAME);
            if (displaySpriteTransform == null)
            {
                var displaySpriteGO = new GameObject(DISPLAY_SPRITE_GONAME);
                displaySpriteGO.transform.parent = transform;
                displaySpriteTransform = displaySpriteGO.transform;
            }

            _displaySpriteRenderer = displaySpriteTransform.GetComponent<SpriteRenderer>();
            if (_displaySpriteRenderer == null)
            {
                //Regenerate the bounds so that we'll loop through the meshes again and find the scale the sprite should be set to.
                _boundsFound = false;

                _displaySpriteRenderer = displaySpriteTransform.gameObject.AddComponent<SpriteRenderer>();
                _displaySpriteRenderer.sprite = _previousSprite = _displaySprite;
            }
            else
            {
                if (Application.isPlaying)
                {
                    _displaySpriteRenderer.enabled = false;
                }
                else
                {
                    _displaySpriteRenderer.enabled = true;
                    if (_displaySpriteOffset == null)
                    {
                        _displaySpriteOffset = _displaySpriteRenderer.GetComponent<OffsetSpawnerDisplaySprite>();
                        if (_displaySpriteOffset != null)
                        {
                            _displaySpriteOffset.Moved += SpriteTransformHasChanged;
                        }
                    }
                }
            }
        }

        //Have we gotten and stored the box collider off the NPC already?
        if (_prefabCollider == null && PrefabToSpawn != null)
        {
            _prefabCollider = PrefabToSpawn.GetComponent<BoxCollider2D>();
        }

        //Has the transform been modified in anyway, necessitating a regeneration of the collider bounds. This is also where we do a quick check to make sure the display is still the same one, and reasssign if it is not.
        if (transform.hasChanged || (_displaySpriteRenderer != null && _previousSprite != _displaySprite))
        {
            ResetBounds();
            if (_displaySpriteRenderer != null)
            {
                _displaySpriteRenderer.sprite = _previousSprite = _displaySprite;
            }
        }

        //Runs a depth first search to compile a list of all the meshes in the NPC so that we can find the bounds and center position of the actual NPC this spawner will spawn.
        //We need to do this so that we can accurately position the drawn collider and display sprite to DO WE THOUGH????
        if (_prefabMeshes.Count == 0 && PrefabToSpawn != null)
        {
            for (int i = 0; i < PrefabToSpawn.transform.childCount; ++i)
            {
                FindMesh(PrefabToSpawn.transform.GetChild(i));
            }
        }

        var minBounds = Vector2.positiveInfinity;
        var maxBounds = Vector2.negativeInfinity;
        var meshScale = _displaySpriteScale;

        if (_prefabMeshes.Count != 0 && (!_boundsFound || _forceBoundsRegeneration) && _prefabCollider != null)
        {
            /* We use the meshes of the NPC to figure out the bounds and the center of the NPC visuals because Unity apparently doesn't fill in the values of a collider's bounds until runtime,
                * meaning it's not possible to get the actual bounds or center of the collider in the editor. */
            foreach (Renderer mesh in _prefabMeshes)
            {
                if (meshScale == Vector3.zero)
                {
                    //This is bad. This will just grab the scale of the first mesh it finds. But we don't currently have any NPCs that have different scaling values for different meshes, so it'll work for now.
                    meshScale = mesh.transform.lossyScale;
                }

                if (mesh.bounds.min.x < minBounds.x)
                {
                    minBounds.x = mesh.bounds.min.x;
                }
                if (mesh.bounds.max.x > maxBounds.x)
                {
                    maxBounds.x = mesh.bounds.max.x;
                }

                if (mesh.bounds.min.y < minBounds.y)
                {
                    minBounds.y = mesh.bounds.min.y;
                }
                if (mesh.bounds.max.y > maxBounds.y)
                {
                    maxBounds.y = mesh.bounds.max.y;
                }
            }
        }

        if ((!_boundsFound || _forceBoundsRegeneration) && _prefabCollider != null)
        {
            if (_prefabMeshes.Count != 0)
            {
                SetUpBounds(minBounds + (Vector2)transform.position, maxBounds + (Vector2)transform.position);
            }
            else
            {
                SetUpBounds((Vector2)transform.position - _prefabCollider.size / 2f + _prefabCollider.offset, (Vector2)transform.position + _prefabCollider.size / 2f + _prefabCollider.offset);
            }

            if (_displaySpriteRenderer != null)
            {
                //If the spawner has an offset applied to its display (possibly to correct some misalignment due to the way the prefab is structured, or the positions calculated from the meshes),
                //we want to apply that offset to the setting of the display sprite's position.
                Vector2 spriteOffset = Vector2.zero;
                if (_displaySpriteOffset != null)
                {
                    spriteOffset = _displaySpriteOffset.Offset;
                }

                _displaySpriteRenderer.gameObject.transform.position = (Vector2)_prefabBounds.center + spriteOffset;
                if (meshScale != Vector3.zero)
                {
                    _displaySpriteRenderer.gameObject.transform.localScale = meshScale;
                }
            }

            _forceBoundsRegeneration = false;
        }

        if (_prefabBounds != null && !Application.isPlaying)
        {
            //We need to apply any offset that is a part of the movement collider to the drawing of the collider to accurately reflect its position.
            var movementColliderOffset = Vector2.zero;
            var prefabMovementCollider = PrefabToSpawn.GetComponent<BoxCollider2D>();
            if (prefabMovementCollider != null)
            {
                movementColliderOffset = prefabMovementCollider.offset;
            }

            //If the spawner sprite has an offset applied to its display, optionally that offset can be applied to the drawing of the collider if it is necessary to ensure everything lines up.
            //Here we check if that is the case.
            var displayOffset = Vector2.zero;
            if (_displaySpriteOffset != null && _displaySpriteOffset.OffsetCollisionBoxDraw)
            {
                displayOffset = _displaySpriteOffset.Offset;
            }

            //Draw the movement collider for the NPC.
            VisualDebug.DrawBoxGizmos((Vector2)_prefabBounds.center + movementColliderOffset + displayOffset, _prefabBounds.size / 2f, Quaternion.identity);
        }
    }

    /// <summary>
    /// A recursive function that will search a transform and any of its children (and any of the children's children etc) for any MeshRenderer or SkinnedMeshRenderer components to add to the collection.
    /// </summary>
    /// <param name="t">The transform currently being checked.</param>
    private void FindMesh(Transform t)
    {
        Renderer mesh = t.GetComponent<MeshRenderer>();
        if (mesh != null)
        {
            _prefabMeshes.Add(mesh);
        }
        else
        {
            mesh = t.GetComponent<SkinnedMeshRenderer>();
            if (mesh != null)
            {
                _prefabMeshes.Add(mesh);
            }
        }
        
        if (t.childCount > 0)
        {
            for (int i = 0; i < t.childCount; ++i)
            {
                FindMesh(t.GetChild(i));
            }
        }
    }

    /// <summary>
    /// Creates the bounds of the prefab by taking the size of the prefab's collider and calculating the center of the collider from the minimum and maximum points found on the meshes.
    /// </summary>
    /// <param name="minBounds">The minimum point on the display sprite's mesh.</param>
    /// <param name="maxBounds">The maximum point on the display sprite's mesh.</param>
    private void SetUpBounds(Vector2 minBounds, Vector2 maxBounds)
    {
        var boundsSize = maxBounds - minBounds;
        var center = new Vector2(minBounds.x + boundsSize.x / 2f, minBounds.y + boundsSize.y / 2f);
        _prefabBounds = new Bounds(center, _prefabCollider.size);

        _boundsFound = true;
    }

    /// <summary>
    /// Sets the variables required to trigger a recalculation of the prefab's bounds.
    /// </summary>
    private void ResetBounds()
    {
        _prefabMeshes.Clear();

        _boundsFound = false;
        transform.hasChanged = false;
    }

    private void SpriteTransformHasChanged()
    {
        transform.hasChanged = true;
    }
}
