﻿using System.Collections.Generic;
using UnityEngine;

public class HasTagInTrigger : MonoBehaviour
 {
    public string TAG;
    public bool IsInTrigger = false;
    public delegate void OnEnterEvent(GameObject gameObject);
    public event OnEnterEvent OnEnter;
    public Collider2D Thecollider2D;
    public GameObject Target;

    public void Start()
    {
        Thecollider2D = GetComponent<Collider2D>();
        Target = GameObject.FindGameObjectWithTag(tag);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == TAG)
        {
            IsInTrigger = true;
            Target = collision.gameObject;
            if (OnEnter != null)
            {
                OnEnter(collision.gameObject);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == TAG)
            IsInTrigger = false;
    }
  /// <summary>
  /// Can the GameObject at the Passes in pos see the taged OBj;
  /// </summary>
  /// <param name="fromObj"></param>
  /// <param name="Layers"></param>
  /// <returns></returns>
    public bool CanGameObjectSeeTagInTrigger(Transform fromObj, int Layers = 1 << 9)
    {
        if (!IsInTrigger)
        {
            return false;
        }
        ///Target = GameObject.FindGameObjectWithTag(tag);
        RaycastHit2D hit = Physics2D.Raycast(fromObj.position, Target.transform.position - transform.position, Vector3.Distance(Target.transform.position, fromObj.position), 1 << 9);

        if (hit)
        {
            Debug.DrawLine(fromObj.position, hit.point,Color.red,22);
            return false;
        }
        Debug.DrawLine(fromObj.position, Target.transform.position, Color.green,22);
        return true;
    }
}

