﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalData;

//TODO: Create gravity states for the relevant gravity objects using this script and phase out this script.
public class ModifyRigidbodyForGravity : MonoBehaviour, IGravityObject
{
    /// <summary>
    /// The gravity types whose values should be set manually and not to the defaults.
    /// </summary>
    [HideInInspector]
    public int OverrideGravityTypes;


    /// <summary>
    /// The specified value for the linear drag if the gravity is normal.
    /// </summary>
    public float BaseLinearDrag = -1;

    /// <summary>
    /// The specified value for the angular drag if the gravity is normal.
    /// </summary>
    public float BaseAngularDrag = -1;

    /// <summary>
    /// The specified value for the gravity scale if the gravity is normal.
    /// </summary>
    public float BaseGravityScale = -999;

    /// <summary>
    /// The specified value for the linear drag if the gravity is that of lava.
    /// </summary>
    public float LavaLinearDrag = -1;

    /// <summary>
    /// The specified value for the angular drag if the gravity is that of lava.
    /// </summary>
    public float LavaAngularDrag = -1;

    /// <summary>
    /// The specified value for the gravity scale if the gravity is that of lava.
    /// </summary>
    public float LavaGravityScale = -999;


    /// <summary>
    /// Reference to the rigidbody component being modified.
    /// </summary>
    private Rigidbody2D _rigidbody;


    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        Debug.Assert(_rigidbody != null, "Cannot find a rigidbody on " + gameObject.name);
    }


    /// <summary>
    /// Modifies the appropriate values of the rigidbody so it responds correctly to the current gravitational environment.
    /// </summary>
    /// <param name="gravityType">The type of gravity the rigidbody is entering.</param>
    public void ChangeGravityType(GravityType gravityType)
    {
        if (_rigidbody != null)
        {
            switch (gravityType)
            {
                case GravityType.Base:
                    if (BaseLinearDrag == -1)
                        _rigidbody.drag = RigidbodyGravityDefaults.BASE_LINEAR_DRAG;
                    else
                        _rigidbody.drag = BaseLinearDrag;


                    if (BaseAngularDrag == -1)
                        _rigidbody.angularDrag = RigidbodyGravityDefaults.BASE_ANGULAR_DRAG;
                    else
                        _rigidbody.angularDrag = BaseAngularDrag;

                    if (BaseGravityScale == -999)
                        _rigidbody.gravityScale = RigidbodyGravityDefaults.BASE_GRAVITY_SCALE;
                    else
                        _rigidbody.gravityScale = BaseGravityScale;
                    break;
                case GravityType.Lava:
                    if (LavaLinearDrag == -1)
                        _rigidbody.drag = RigidbodyGravityDefaults.LAVA_LINEAR_DRAG;
                    else
                        _rigidbody.drag = LavaLinearDrag;

                    if (LavaAngularDrag == -1)
                        _rigidbody.angularDrag = RigidbodyGravityDefaults.LAVA_ANGULAR_DRAG;
                    else
                        _rigidbody.angularDrag = LavaAngularDrag;

                    if (LavaGravityScale == -999)
                        _rigidbody.gravityScale = RigidbodyGravityDefaults.LAVA_GRAVITY_SCALE;
                    else
                        _rigidbody.gravityScale = LavaGravityScale;
                    break;
                    //TODO: case GravityType.Mud:
            }
        }
    }
}
