﻿using Anima2D;
using System.Collections.Generic;
using UnityEngine;
using Characters.Enemies;

/// <summary>
/// A script for Controlling a group of RobotBody Shaders 
/// </summary>
public class DefaultGroupRobotBodyShaderController : MonoBehaviour
{
    /// <summary>
    /// The List of renderer to be affected 
    /// </summary>
    public List<SpriteMeshInstance> RobotRendres;
    /// <summary>
    /// How Much Color Saturation is applied 
    /// </summary>
    public float Saturation;
    /// <summary>
    /// THe hue offset Applied to all sprite IE Color change 
    /// </summary>
    public float Hue;
    /// <summary>
    /// 
    /// </summary>
    public float EmisstionPower;


    private List<SpriteMeshInstance> emisstionShaders;


    private float EmisstionPowerLast;
    private float LastHue =11; // 11 is outside the valid range and forces the update on start
    private float LastSaturation = 11;
    private Enemy enemy;
    void Start()
    {
        RobotRendres =  new List<SpriteMeshInstance>(gameObject.GetComponentsInChildren<SpriteMeshInstance>());
        emisstionShaders = new List<SpriteMeshInstance>();
        //RobotBodyWithEmission

        foreach (SpriteMeshInstance rendredRef in RobotRendres)
        {
            if(rendredRef.sharedMaterial.shader.name == "robomania/RobotBodyWithEmission")
            {
                emisstionShaders.Add(rendredRef);
                rendredRef.sharedMaterial.SetFloat("_EmmissionPower", EmisstionPower);
            }

            rendredRef.sharedMaterial.SetFloat("_Saturation", Saturation);
            rendredRef.sharedMaterial.SetFloat("_Hue", Hue);
        }

        enemy = gameObject.GetComponent<Enemy>();
    }
    private void LateUpdate()
    {
        if (enemy != null)// if the Enemy ref is set then use it 
            Saturation = Mathf.Clamp( enemy.Health / enemy.MaxHealth, 0, 1);
        if (LastSaturation != Saturation)// if the value has changed 
            UpdateAllSaturationValues();
        if (LastHue != Hue)// if the value has changed 
            UpdateAllHueValues();
        if(EmisstionPowerLast != EmisstionPower)
            UpdateAllEmisstionPowerValues();
        LastHue = Hue;
        LastSaturation = Saturation;
        EmisstionPowerLast = EmisstionPower;
    }
    private void UpdateAllSaturationValues()
    {
        Saturation = Mathf.Clamp(Saturation, 0, 5);
        if (RobotRendres.Count ==0)
        {
            return;//fail sate 
        }
        foreach(SpriteMeshInstance rendredRef in RobotRendres)
        {
            
            rendredRef.sharedMaterial.SetFloat("_Saturation", Saturation); 
        }
    }
    private void UpdateAllEmisstionPowerValues()
    {
        Saturation = Mathf.Clamp(Saturation, -1, 1);
        if (emisstionShaders.Count == 0)
        {
            return;//fail sate 
        }
        foreach (SpriteMeshInstance rendredRef in emisstionShaders)
        {

            rendredRef.sharedMaterial.SetFloat("_EmmissionPower", EmisstionPower);
        }
    }
    private void UpdateAllHueValues()
    {
        Hue = Mathf.Clamp(Hue, 0, 5);
        if (RobotRendres.Count == 0)
        {
            return;//fail sate 
        }
        foreach (SpriteMeshInstance rendredRef in RobotRendres)
        {
            rendredRef.sharedMaterial.SetFloat("_Hue", Hue);
        }
    }


}