﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BendAtPositionShaderController : MonoBehaviour
{
    /* The amount of time the lerp back effect after the shader is deactivated should take. */
    private float DIP_DISTANCE_LERP_BACK_TIME = 0.75f;


    /// <summary>
    /// The game object whose position will be used to determine where the bend should occur.
    /// If this is null, it will default to the Player game object.
    /// </summary>
    [SerializeField]
    [Tooltip("You can leave this null if the intended game object is the Player, as that is selected as the default game object.")]
    private GameObject _gameObjectPositionToUse;

    /// <summary>
    /// The climbable object that is employing this shader effect.
    /// </summary>
    [SerializeField]
    private ClimbableObject _associatedClimbableObject;

    /* Reference to the material that has the shader applied to it. */
    private Material _bendMaterial;

    private bool _shaderEffectActivated;
    private float _maxDipDistance;

    /* Whether the lerp back effect, where the dip position lerps back to it's initial position after the shader effect is deactivated, is currently active. */
    private bool _lerpBackActive = false;

    private float _currentBounceBackTime = 0;


    // Start is called before the first frame update
    private void Start()
    {
        _bendMaterial = GetComponent<Renderer>()?.material;
        if (_bendMaterial == null)
        {
            Debug.LogWarning("BendAtPositionShaderController attached to game object " + gameObject.name + " doesn't have a renderer with material attached");
            return;
        }

        if (!_bendMaterial.HasProperty("BendPosition"))
        {
            Debug.LogWarning("The main material attached to game object " + gameObject.name + " doesn't have the BendPosition property");
            return;
        }

        _maxDipDistance = _bendMaterial.GetFloat("MaxDipDistance");

        if (_gameObjectPositionToUse == null)
        {
            /* If no game object was assigned, default to the player. It was probably the inability to assign references across scenes that prevented the Player from being assigned in the first place. */
            _gameObjectPositionToUse = GlobalData.Player.gameObject;
        }

        if (_associatedClimbableObject == null)
        {
            _associatedClimbableObject = GetComponent<ClimbableObject>();
            if (_associatedClimbableObject == null)
            {
                Debug.LogWarning("BendAtPositionShaderController attached to game object " + gameObject.name + " doesn't have an associated climbable object assigned, and one could not be found.");
                return;
            }
        }

        _associatedClimbableObject.OnClimbableObject += EnableShaderEffect;
        _associatedClimbableObject.OffClimbableObject += DisableShaderEffect;
    }

    // Update is called once per frame
    private void Update()
    {
        if (_shaderEffectActivated)
        {
            _bendMaterial.SetVector("BendPosition", _gameObjectPositionToUse.transform.position - transform.position);
        }
        else if (_lerpBackActive)
        {
            float currentDipDistance = 0;
            _currentBounceBackTime += Time.deltaTime;

            if (_currentBounceBackTime >= DIP_DISTANCE_LERP_BACK_TIME)
            {
                _currentBounceBackTime = DIP_DISTANCE_LERP_BACK_TIME;

                _lerpBackActive = false;
                _currentBounceBackTime = 0;
            }
            else
            {
                currentDipDistance = _maxDipDistance * (1 - (_currentBounceBackTime / DIP_DISTANCE_LERP_BACK_TIME));
            }

            _bendMaterial.SetFloat("MaxDipDistance", currentDipDistance);
        }
        else
        {
            _bendMaterial.SetVector("BendPosition", GlobalData.VEC3_OFFSCREEN);

        }
    }

    private void EnableShaderEffect()
    {
        _shaderEffectActivated = true;
        _bendMaterial.SetVector("BendPosition", _gameObjectPositionToUse.transform.position - transform.position);
        _bendMaterial.SetFloat("MaxDipDistance", _maxDipDistance);
    }

    private void DisableShaderEffect()
    {
        _shaderEffectActivated = false;
        _lerpBackActive = true;
    }
}
