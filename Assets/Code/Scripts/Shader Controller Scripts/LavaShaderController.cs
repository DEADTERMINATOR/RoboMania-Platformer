﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class LavaShaderController : MonoBehaviour
{
    public Vector2 TopTiling;
    public Vector2 BottomTilling;
    public float TopCellSize = 15;
    public List<MeshRenderer> Meshs = new List<MeshRenderer>();
    public float MinDispaceWaveOffset = -0.5f;
    public float MaxDispaceWaveOffset = 0.5f;
    public float WaveNoiseScale = 10;
    public Vector2 LavaWaveMovmentSpeed = new Vector2(0.01f, 0.01f);
    public bool ObstialPass = true;
    /// <summary>
    /// Asses to the block for other components 
    /// </summary>
    public MaterialPropertyBlock MaterialPropertyBlock { get{return _materialPropertyBlock;} }
    private MaterialPropertyBlock _materialPropertyBlock;

    private int _topCellDesncity = Shader.PropertyToID("_topCellDesncity");
    private int _LavaTopTiling = Shader.PropertyToID("_LavaTopTiling");
    private int _lavaBottomTiling = Shader.PropertyToID("_lavaBottomTiling");
    private int _minwaveoffset = Shader.PropertyToID("_minwaveoffset");
    private int _maxWaveOffset = Shader.PropertyToID("_maxWaveOffset");
    private int _waveScale = Shader.PropertyToID("_waveScale");
    private int _lavaWaveSpeed = Shader.PropertyToID("_lavaWaveSpeed");
    private int _obbpass = Shader.PropertyToID("_obsPass");


    // Use this for initialization
    void Start()
    {
        _materialPropertyBlock = new MaterialPropertyBlock();
        _materialPropertyBlock.SetFloat(_topCellDesncity, TopCellSize);
        _materialPropertyBlock.SetVector(_LavaTopTiling, TopTiling);
        _materialPropertyBlock.SetVector(_lavaBottomTiling, BottomTilling);
        _materialPropertyBlock.SetFloat(_minwaveoffset, MinDispaceWaveOffset);
        _materialPropertyBlock.SetFloat(_maxWaveOffset, MaxDispaceWaveOffset);
        _materialPropertyBlock.SetFloat(_waveScale, WaveNoiseScale);
        _materialPropertyBlock.SetVector(_lavaWaveSpeed, LavaWaveMovmentSpeed);
        //  _materialPropertyBlock.SetFloat("_preview", 1f);


        // Top.SetPropertyBlock(_materialPropertyBlock);
        foreach (MeshRenderer mesh in Meshs)
        {
            mesh.SetPropertyBlock(_materialPropertyBlock);
        }
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (_materialPropertyBlock != null )
        {
            _materialPropertyBlock.SetFloat(_topCellDesncity, TopCellSize);
            _materialPropertyBlock.SetVector(_LavaTopTiling, TopTiling);
            _materialPropertyBlock.SetVector(_lavaBottomTiling, BottomTilling);
            _materialPropertyBlock.SetFloat(_minwaveoffset, MinDispaceWaveOffset);
            _materialPropertyBlock.SetFloat(_maxWaveOffset, MaxDispaceWaveOffset);
            _materialPropertyBlock.SetFloat(_waveScale, WaveNoiseScale);
          //  _materialPropertyBlock.SetInt(_obbpass, ObstialPass);
            // _materialPropertyBlock.SetFloat("_preview", 0f);
            // Top.SetPropertyBlock(_materialPropertyBlock);
            foreach (MeshRenderer mesh in Meshs)
            {
                mesh.SetPropertyBlock(_materialPropertyBlock);
            }
        }
#endif
    }

    private void OnDestroy()
    {
        // _materialPropertyBlock.SetFloat("_preview", 0f);


        // Top.SetPropertyBlock(_materialPropertyBlock);
        foreach (MeshRenderer mesh in Meshs)
        {
            mesh.SetPropertyBlock(_materialPropertyBlock);
        }
    }
}
