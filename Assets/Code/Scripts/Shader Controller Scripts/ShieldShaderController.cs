﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class ShieldShaderController : MonoBehaviour
{
    #region ExposedVars
    [SerializeField]
    private Color _MaiaColor;
    [SerializeField]
    private Color _AltColor;
    [SerializeField]
    private float _FadeAmount;
    #endregion;
    private int _MaiaColorID;
    private int _AltColorID;
    private int _FadeAmountID;

    
    private MeshRenderer _renderer;
    private MaterialPropertyBlock _propBlock;
    // Use this for initialization
    void Start()
    {
        _propBlock = new MaterialPropertyBlock();
        _renderer = gameObject.GetComponent<MeshRenderer>();
        _renderer.enabled = true;
        _renderer.GetPropertyBlock(_propBlock);
        _MaiaColorID = Shader.PropertyToID("_MainColour");
        _AltColorID = Shader.PropertyToID("_AltColour");
        _FadeAmountID = Shader.PropertyToID("_FadeAmount");
        UpdateValues();
    }

    public void UpdateValues()
    {
        _renderer.GetPropertyBlock(_propBlock);
        _propBlock.SetColor(_MaiaColorID, _MaiaColor);
        _propBlock.SetColor(_AltColorID, _AltColor);
        _propBlock.SetFloat(_FadeAmountID, _FadeAmount);
        _renderer.SetPropertyBlock(_propBlock);
    }

    private MeshRenderer GetRenderREf()
    {
        if (_renderer)
        {
            return _renderer;
        }
        _renderer = gameObject.GetComponent<MeshRenderer>();
        return _renderer;

    }

}
