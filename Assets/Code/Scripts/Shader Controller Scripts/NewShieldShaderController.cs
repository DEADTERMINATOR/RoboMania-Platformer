﻿using System.Collections.Generic;
using UnityEngine;


public class NewShieldShaderController :MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    

    public void SetRingAlpha(float amount)
    {
        amount = Mathf.Clamp(amount, 0, 1);
        spriteRenderer.material.SetFloat("_outterAlpha", amount);
    }
    public void SetInnerAlphaMax(float amount)
    {
        amount = Mathf.Clamp(amount, 0, 1);
        spriteRenderer.material.SetFloat("_AlphaLimit", amount);
    }

    public void IncreaseAlphaSFXTileAmount(float amount)
    {
        amount = Mathf.Clamp(amount, 1, 100);
        StartCoroutine(TileOverTime(amount));
    }

    public void SetAlphaSFXTileAmount(float amount)
    {
        amount = Mathf.Clamp(amount, 1, 100);
        spriteRenderer.material.SetFloat("_alphaDistorstTile", amount);
    }
    public void SetAlphaSFXAmount(float amount)
    {
        amount = Mathf.Clamp(amount, 0, 1);
        spriteRenderer.material.SetFloat("_alphaDestortionInflinace", amount);
    }

    public void SetDmageSeed()
    {
       
        spriteRenderer.material.SetFloat("_DamagFxSeed", Random.Range(0.001f,0.989f));
    }
    public void SetDamage(float amount)
    {
        amount = Mathf.Clamp(amount, 0, 1);
        spriteRenderer.material.SetFloat("_damageLerp", amount);
    }
    

    public void SetAlphSFSTextuer(Sprite color)
    {
        spriteRenderer.material.SetTexture("_alphaSFXTex", color.texture);
    }
    public void SetMainColor(Color color)
    {
        spriteRenderer.material.SetColor("_MainColor", color);
    }

    public void SetDamageColor(Color color)
    {
        spriteRenderer.material.SetColor("_DamageColor", color);
    }
    public void SetRingColor(Color color)
    {
        spriteRenderer.material.SetColor("_ringColor", color);
    }
    public void SetDamgeValue(float amount)
    {
        amount = Mathf.Clamp(amount, 0, 1);
        spriteRenderer.material.SetFloat("_colorLerp", EaseInOut(amount));
    }

    float EaseInOut(float aTime)
    {
        return -.5f * (Mathf.Cos(Mathf.PI * aTime / 1f) - 1);
    }

    private IEnumerator<object> TileOverTime(float endVal)
    {
        float startVal = spriteRenderer.material.GetFloat("_alphaDistorstTile");
        // have the value applied over 25 frams 
        for(int i =0; i < 25; i++)
        {
            spriteRenderer.material.SetFloat("_alphaDistorstTile", Mathf.Lerp(startVal,endVal,i/25));
            yield return new WaitForEndOfFrame();
        }

        spriteRenderer.material.SetFloat("_alphaDistorstTile", endVal);
    }
}

