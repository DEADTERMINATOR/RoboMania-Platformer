﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

[CreateAssetMenu(menuName = "Shield/ShieldAssetManager")]
public class ShieldAssetManager : ScriptableObject
{
    public GameObject CommonShieldVisualTemplate;
    public GameObject UncommonShieldVisualTemplate;
    public GameObject RareShieldVisualTemplate;
    public GameObject EpicShieldVisualTemplate;
    public ShieldPickUp ShieldPickUpTemplate;

    public static ShieldAssetManager Instance { get;  set; }

    private void Awake()
    {
        Instance = this;
    }


    /// <summary>
    /// A short cut for getting a template
    /// </summary>
    /// <param name="rarity"></param>
    /// <returns></returns>
    public static Shield GetTemplateFromRarity(Rarity rarity)
    {
        switch (rarity)
        {
            case Rarity.UNCOMMON:
                return Instantiate<GameObject>(Instance.UncommonShieldVisualTemplate).GetComponent<Shield>();
            case Rarity.RARE:
                return Instantiate<GameObject>(Instance.RareShieldVisualTemplate).GetComponent<Shield>();
            case Rarity.EPIC:
                return Instantiate<GameObject>(Instance.EpicShieldVisualTemplate).GetComponent<Shield>();
            default:
                return Instantiate<GameObject>(Instance.CommonShieldVisualTemplate).GetComponent<Shield>();

        }
    }



}
