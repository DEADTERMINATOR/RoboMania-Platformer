﻿using UnityEngine;
using System.Collections;
using LevelSystem;
using Characters.Player;
using Characters;

[RequireComponent(typeof(NPC))]
public class NPCShieldContainer : MonoBehaviour
{

    /*/// <summary>
    /// The current Shield May be null
    /// </summary>
    public Shield Current { get; protected set; }
    /// <summary>
    /// Where to attach the Shield 
    /// </summary>
    ///
    [Tooltip("Can be left NULL if a GameObject has the tag ShieldAttachPoint")]
    public GameObject AttchPoint;
    /// <summary>
    /// is there an active Shield 
    /// </summary>
    /// 
    public bool StartFacingLeft = true;
    public bool IsActive { get { return (Current != null); } }
    public bool StartAttached = false;
    public ShieldAsset shieldAssest;
    public NPC NPCReferance;

    public void Start()
    {
        if (AttchPoint == null)
        {
            AttchPoint = GameObject.FindGameObjectWithTag("ShieldAttachPoint");
        }
        if(NPCReferance)
        {
            NPCReferance = GetComponent<NPC>();
            NPCReferance.Shield = this;
        }
        if(StartAttached)
        {
            AttachShield(shieldAssest.ConvertToShield());
        }
    }

    public Shield RemoveCurrent()
    {
        if (Current != null)
        {
            Shield old = Current;
            Current = null;
            old.Active = false;
            old.gameObject.SetActive(false);
            old.gameObject.transform.position = GlobalData.VEC3_OFFSCREEN;
            old.gameObject.transform.parent = null;
            old.gameObject.transform.localScale = Vector3.one;
            return old;
        }
        return null;
    }

    public void AttachShield(Shield shield)
    {
        Current = shield;
        shield.Owner = NPCReferance.gameObject;

        shield.gameObject.tag = "Shield";
        shield.transform.parent = AttchPoint.transform;
        shield.transform.localPosition = Vector3.zero;
        int faceDir = GlobalData.Player.Controller.Collisions.FaceDir;
        if (faceDir != 1 && faceDir != -1)///Must be A load from file 
        {
            faceDir = 1;
        }
        shield.transform.localScale = new Vector3(shield.transform.localScale.x * faceDir, shield.transform.localScale.y, shield.transform.localScale.z);
        shield.gameObject.SetActive(true);
        shield.Active = true;

    }

    public void Update()
    {
        if (Current != null && (Current.Power <= 0))
        {

            GameObject.Destroy(Current.gameObject);
            Current = null;
        }
    }
    */
  
}
