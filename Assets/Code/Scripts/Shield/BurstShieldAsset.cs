﻿using UnityEngine;
using System.Collections;
[CreateAssetMenu(menuName = "Shield/BurstShieldAssest")]
public class BurstShieldAsset : ShieldAsset
{

    /// <summary>
    /// How many burst the shiled has this splits the over all power by the number of bursts 
    /// </summary>
    public int NumberOfBursts;
    /// <summary>
    /// How long the player must wait with outh being hit for the damage to rest;
    /// </summary>
    public float RechargeTime;

   

}
