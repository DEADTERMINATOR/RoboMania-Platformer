﻿using UnityEngine;
using System.Collections;
[CreateAssetMenu(menuName = "Shield/ShieldAssest")]
public class ShieldAsset : ScriptableObject
{
    public Mesh Mesh;
    public Collider2D Collider2D;
    
}
