﻿using UnityEngine;
using UnityEditor;

public class EditComplexWayPointGameObject : EditWayPointGameObject
{

    public bool IsPartrolPoint = true;
    public bool IsConnectionPoint = false;
    /// <summary>
    /// The index of the conecction and the AreaId of the connection 
    /// </summary>
    public int ConectedAreaID;
    /// <summary>
    /// The area the Conncetion is part of 
    /// </summary>
    public int OwningArea;
    

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    

    }