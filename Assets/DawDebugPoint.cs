﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DawDebugPoint : MonoBehaviour {
    public FillTriggerIcons icon = FillTriggerIcons.NoIcon;
    public Color FillColour = Color.yellow;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = FillColour;
        Gizmos.DrawSphere(transform.position, 0.5f);
        Gizmos.color = Color.red;
        float size = 0.3f;
        Vector3 globalWaypointPos =  transform.position;
        Gizmos.DrawLine(globalWaypointPos - Vector3.up * size, globalWaypointPos + Vector3.up * size);
        Gizmos.DrawLine(globalWaypointPos - Vector3.left * size, globalWaypointPos + Vector3.left * size);
        if (icon != FillTriggerIcons.NoIcon)
        {
            Gizmos.DrawIcon(transform.position, icon + ".png", false);
        }

    }
}
